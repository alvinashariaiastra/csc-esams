$(document).ready(function(){
	$('#px-status-kerawanan-peta-form-peta-perusahaan').change(function() {
		var perusahaan_id = this.value;

        $("#px-status-kerawanan-peta-form-peta-instalasi option").remove();
        $('#px-status-kerawanan-peta-form-peta-instalasi').append($('<option>', {
            value: '0',
            text : '-- Pilih Perusahaan --'
        }));

        $.ajax({
            url: 'admin_status_kerawanan/ajax_get_instalasi',
            type: 'POST',
            dataType: 'json',
            data: {'id': perusahaan_id},
            success : function(result){
                var data = result.result;
                //console.log(data);
                if (result.status == 'ok') {
                    $.each(data, function (i, item) {
                        $('#px-status-kerawanan-peta-form-peta-instalasi').append($('<option>', {
                            value: item.id,
                            text : item.name
                        }));
                    });
                    $('#px-status-kerawanan-form-peta-jenisbisnis-id').val('');
					$('#px-status-kerawanan-form-peta-jenisbisnis-nm').val('');
					$('#px-status-kerawanan-form-peta-instalasi-alamat').html('')
                };  
            },
            error : function(jqXHR, textStatus, errorThrown) {
                alert(textStatus, errorThrown);
            }

        })
	});
	$('#px-status-kerawanan-peta-form-peta-instalasi').change(function() {
		$.ajax({
			url: 'admin_status_kerawanan/get_instalasi_detail',
			type: 'POST',
			dataType: 'json',
			data: {id:this.value},
			success: function(result){
				$('#px-status-kerawanan-form-peta-jenisbisnis-id').val(result.jenis_bisnis_id);
				$('#px-status-kerawanan-form-peta-jenisbisnis-nm').val(result.jenis_bisnis_nm);
				$('#px-status-kerawanan-form-peta-instalasi-alamat').html(result.instalasi_alamat)
			}
		})
	});

	$('#px-status-kerawanan-form').validate({
		rules: {                                            
			perusahaan_id: {
				required: true
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-status-kerawanan-form .alert-warning').removeClass('hidden');
			$('#px-status-kerawanan-form .alert-success').addClass('hidden');
			$('#px-status-kerawanan-form .alert-danger').addClass('hidden');
			$('.px-summernote').each(function() {
				$(this).val($(this).code());
			});
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					$('#px-status-kerawanan-form .alert-warning').addClass('hidden');
					if(response.status == 'ok'){
						$('#px-status-kerawanan-form .alert-success').removeClass('hidden').children('span').text(response.msg);
						window.location.href = response.redirect;
					}
					else
						$('#px-status-kerawanan-form .alert-danger').removeClass('hidden').children('span').text(response.msg);	
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
	$('#px-status-kerawanan-form-peta-content').summernote({
		toolbar: [
			['style', ['style']],
			['font', ['bold', 'italic', 'underline', 'clear']],
			['fontname', ['fontname']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']],
			['table', ['table']],
			['insert', ['link', 'picture', 'hr']],
			['view', ['fullscreen', 'codeview']],
			['help', ['help']]
		],
		height: '300px',
		onImageUpload: function(files, editor, welEditable) {
            sendFile(files[0], editor, welEditable);
        }
	});
    function sendFile(file, editor, welEditable) {
        data = new FormData();
        data.append('image', file);
        $.ajax({
            data: data,
            type: 'post',
            url: 'upload/image',
            cache: false,
            contentType: false,
            processData: false,
            success: function(url) {
                editor.insertImage(welEditable, url);
                $('#px-status-kerawanan-form .panel-body').after('<input type="hidden" name="images[]" value="'+url+'">');
            }
        });
    }
})