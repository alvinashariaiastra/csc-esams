$(document).ready(function(){
	var mapDash = new GMaps({
		div: '#peta-status-kerawanan',
		lat: -3.409856,
		lng: 119.9383623,
		zoom : 4
	});
	$(".select2-dropdown").select2().on("select2:select", function (e) {
		e.preventDefault();
		GMaps.geocode({
			address: this.value,
			callback: function(results, status) {
				if (status == 'OK') {
					if (results[0].address_components[0].long_name == 'Indonesia') {
						mapDash.setCenter(-3.409856, 119.9383623);
						mapDash.setZoom(4);
					}else{
						var latlng = results[0].geometry.location;
						mapDash.setCenter(latlng.lat(), latlng.lng());
						mapDash.setZoom(9);
					}
				}
			}
		});
	});

	$.ajax({
		url: 'admin_status_kerawanan/get_all_location',
		dataType: 'json',
		success: function(data) {
			$.each(data, function(i, val){
				var mark = mapDash.addMarker({
					lat: val.latitude,
					lng:  val.longitude,
					title: val.instalasi_nm,
					infoWindow: {
						content: '<h4><img src="assets/backend_assets/img/'+val.icon_status+'"> '+val.instalasi_nm+'</h4><div>'
						+'<p>'+val.instalasi_alamat+'</p><br>'
						+'<button class="btn btn-default btn-show-status-kerawanan-berita" data-target-id="'+val.instalasi_id+'">Lihat Berita Terkini</button>'
						+'</div>',
						maxWidth: 200
					},
					icon : 'assets/backend_assets/img/'+val.icon_status
				});
			})
			//console.log(data);
		}
	});
        
        $('body').delegate('.change-mark','click',function(){
            mapDash.removeMarkers();
            var status = $(this).attr('data-id');
            $.ajax({
                    
                    url: 'admin_status_kerawanan/get_location_by_status',
                    dataType: 'json',
                    type : 'POST',
                    data: {status:status},
                    success: function(data) {
                            $('#dropdown-kuningmerah').html('');
                            var values = '';
                            $.each(data, function(i, val){
                                    var mark = mapDash.addMarker({
                                            lat: val.latitude,
                                            lng:  val.longitude,
                                            title: val.instalasi_nm,
                                            infoWindow: {
                                                    content: '<h4><img src="assets/backend_assets/img/'+val.icon_status+'"> '+val.instalasi_nm+'</h4><div>'
                                                    +'<p>'+val.instalasi_alamat+'</p><br>'
                                                    +'<button class="btn btn-default btn-show-status-kerawanan-berita" data-target-id="'+val.instalasi_id+'">Lihat Berita Terkini</button>'
                                                    +'</div>',
                                                    maxWidth: 200
                                            },
                                            icon : 'assets/backend_assets/img/'+val.icon_status
                                    });
                                    values += '<option>'+val.instalasi_nm+'</option>';
                            });
                            var dropdown = '<select class="form-control">'+values+'</select>';
                            if(status == 2 || status == 3)
                            $('#dropdown-kuningmerah').html(dropdown);
                    }
            });
        });

	$('body').delegate('.btn-show-status-kerawanan-berita','click',function(){
		var id = $(this).attr('data-target-id');
		var news = ' ';
		$.ajax({
			url : 'admin_status_kerawanan/get_all_news',
			type : 'POST',
			dataType : 'Json',
			data : {'id' : id },
			success : function(result){
				$('#px-dashboard-status-kerawanan-new-modal .modal-body').html(' ');
				if(result.status == 'ok'){
                    if(result.data){
    					$.each(result.data, function(i, data){
    						var bg = 'bg-success';
    						if(data.status == 1)
    							bg = 'bg-success';
    						else if(data.status == 2)
    							bg = 'bg-warning';
    						else if(data.status == 3)
    							bg = 'bg-danger';
    						news = news+'<div data-target-id="'+data.id+'" class="list-group-item '+bg+' btn-show-detail-news-kerawanan-berita">'
    							+'<div class="media-body">'
    								+'<span class="text-muted">'+data.date_created+'</span><br>'
    								+data.title
    							+'</div>'
    						+'</div>'
    					});
                    }
                    $('#px-dashboard-status-kerawanan-new-modal .modal-body').html(news);
				}
                else {
                    $('#px-dashboard-status-kerawanan-new-modal .modal-body').html('Belum ditemukan berita untuk area ini');
                }
			}
		})
		$('#px-dashboard-status-kerawanan-new-modal').modal('show');
	});
});

