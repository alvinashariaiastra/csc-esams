$(document).ready(function(){
	$('#form-approval').validate({
		ignore: [],
		rules: {                                            
			id: {
				required: true
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('.alert-warning').removeClass('hidden');
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					if(response.status == 'ok'){
						$('.alert-warning').addClass('hidden');
						$('.alert-success').removeClass('hidden');
						window.location.href = response.redirect;
					}
					else{
						$('.alert-warning').addClass('hidden');
						$('.alert-danger').removeClass('hidden');
					}
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
})