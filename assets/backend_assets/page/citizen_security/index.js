var table;

table = $('#sc-table').DataTable({
	"processing": true, //Feature control the processing indicator.
	"serverSide": true, //Feature control DataTables' server-side processing mode.
	"order": [[ 1, "desc" ]],
	// Load data for the table's content from an Ajax source
	"ajax": {
		"url": "admin_citizen_security/ajax_index",
		"type": "POST"
	},
	//Set column definition initialisation properties.
	"columnDefs": [{ 
		"targets": [0, 4, 5, 6 ], //last column
		"orderable": false //set not orderable
	}]
});

function reload_table()
{
  table.ajax.reload(null,false); //reload datatable ajax 
}

$(document).ready(function () {
	$('.form_datetime').datetimepicker({format: 'yyyy-mm-dd hh:ii', autoclose: true, todayBtn: true, todayHighlight: true, startDate: '2018-01-01 00:00'});	
	
	$('#sc-datetime-filter-apply').click(function () {
		var table = $('#sc-table').DataTable();
		table.destroy();
		$('#sc-table').DataTable({
			"processing": true, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			"order": [[ 1, "desc" ]],
			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": "admin_citizen_security/ajax_index",
				"type": "POST",
				"data": function(d) {
					d.startdatetime = $('#sc-datetime-filter-start').val();
					d.enddatetime = $('#sc-datetime-filter-end').val();
				}
			},
			//Set column definition initialisation properties.
			"columnDefs": [{ 
				"targets": [0, 4, 5, 6 ], //last column
				"orderable": false //set not orderable
			}]
		});;
	});
});