$(document).ready(function(){
	$('#px-system-maintain-email-form').validate({
		ignore: [],
		rules: {
			transaksi: {
				required: true
			},
			admin_id: {
				required : true
			}
		},
		messages: {
			transaksi:
			{
				required: "Please enter your transaction."
			},
			admin_id:
			{
				required: "Please enter your admin user.",
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-system-maintain-email-form .alert-warning').removeClass('hidden');
			$('#px-system-maintain-email-form .alert-success').addClass('hidden');
			$('#px-system-maintain-email-form .alert-danger').addClass('hidden');
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					$('#px-system-maintain-email-form .alert-warning').addClass('hidden');
					if(response.status == 'ok'){
						$('#px-system-maintain-email-form .alert-success').removeClass('hidden').children('span').text(response.msg);
						window.location.href = response.redirect;
					}
					else
						$('#px-system-maintain-email-form .alert-danger').removeClass('hidden').children('span').text(response.msg);	
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
	$('#px-system-maintain-email-message-form').validate({
		ignore: [],
		rules: {                                            
			id: {
				required: true
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-system-maintain-email-message-form .msg-status').text('Deleting data');
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					if(response.status == 'ok'){
						$('#px-system-maintain-email-message-form .msg-status').text('Delete Success...');
						window.location.href = response.redirect;
					}
					else
						$('#px-system-maintain-email-message-form .msg-status').text('Delete Failed');		
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
	$('.btn-add').click(function(){
		var target_form = $(this).attr('data-target-form');
		$('#px-system-maintain-email-form').attr('action',target_form);
		$('#px-system-maintain-email-form-id').val('');
		$('#px-system-maintain-email-form-email').val('');
		$('#px-system-maintain-email-form-transaksi option').removeAttr('selected');
		$('#px-system-maintain-email-form-admin-id option').removeAttr('selected');
		$('#px-system-maintain-email-modal').modal('show');
	});
	$('body').delegate('.btn-edit','click',function(){
		var target_form = $(this).attr('data-target-form');
		var target = $(this).attr('data-target-get');
		var id = $(this).attr('data-target-id');
		$('#px-system-maintain-email-form').attr('action',target_form);
		$.ajax({
			url : target,
			type : 'POST',
			dataType : 'json',
			data : {'id':id},
			success : function(response){
				$('#px-system-maintain-email-form-id').val(id);
				$('#px-system-maintain-email-form-email').val(response.data.row.email);
				$('#px-system-maintain-email-form-transaksi option[value="'+response.data.row.transaksi+'"]').prop('selected',true);
				$('#px-system-maintain-email-form-admin-id option[value="'+response.data.row.admin_id+'"]').prop('selected',true);
				$('#px-system-maintain-email-modal').modal('show');
			},
			error : function(jqXHR, textStatus, errorThrown) {
				alert(textStatus, errorThrown);
			}
		});
	});
	$('body').delegate('.btn-delete','click',function(){
		var id = $(this).attr('data-target-id');
		$('#px-system-maintain-email-message-form input[name="id"]').val('');
		$('#px-system-maintain-email-message-form input[name="id"]').val(id);
		$('#px-system-maintain-email-message-box').addClass('open');
	});
	$('#px-system-maintain-email-modal').on('hidden.bs.modal', function (e) {
		$('#px-system-maintain-email-form').validate().resetForm();
	});
	$('#px-system-maintain-email-modal').on('shown.bs.modal', function (e) {
		$('#px-system-maintain-email-form').validate().resetForm();
	});
})