    //datatables function
var save_method; //for save method string
var table;

table = $('.table').DataTable({
  "processing": true, //Feature control the processing indicator.
  "serverSide": true, //Feature control DataTables' server-side processing mode.
  
  // Load data for the table's content from an Ajax source
  "ajax": {
      "url": "admin_visitor_history/visitor_history_ajax",
      "type": "POST"
  },

  //Set column definition initialisation properties.
  "columnDefs": [
  {
    "sClass": "text-center",
    "aTargets": [0,1,2,3,4,5],
  },
  ]

});

function reload_table()
{
  table.ajax.reload(null,false); //reload datatable ajax 
}