$(document).ready(function(){
    var mapDash = new GMaps({
		div: '#peta-status-kerawanan-dash',
		lat: -3.409856,
		lng: 119.9383623,
		zoom : 4
	});
	$("#select2-dashboard-year").select2().on("select2:select", function (e) {
		e.preventDefault();
        $.ajax({
            url : 'admin/chart_hasil_asesmen_per_grup_tahun',
            type : 'POST',
            data: {year:this.value},
            dataType : 'Json',
            success : function(result){
                chart_hasil_asesmen_per_grup_tahun(result.category,result.series);
            }
        });
	});

    $(".select2-dropdown").select2().on("select2:select", function (e) {
        e.preventDefault();
        GMaps.geocode({
            address: this.value,
            callback: function(results, status) {
                if (status == 'OK') {
                    if (results[0].address_components[0].long_name == 'Indonesia') {
                        mapDash.setCenter(-3.409856, 119.9383623);
                        mapDash.setZoom(4);
                    }else{
                        var latlng = results[0].geometry.location;
                        mapDash.setCenter(latlng.lat(), latlng.lng());
                        mapDash.setZoom(9);
                    }
                }
            }
        });
    });

	$.ajax({
		url: 'admin_status_kerawanan/get_all_location',
		dataType: 'json',
		success: function(data) {
			$.each(data, function(i, val){
				var mark = mapDash.addMarker({
					lat: val.latitude,
					lng:  val.longitude,
					title: val.instalasi_nm,
					infoWindow: {
						content: '<h4><img src="assets/backend_assets/img/'+val.icon_status+'"> '+val.instalasi_nm+'</h4><div>'
						+'<p>'+val.instalasi_alamat+'</p><br>'
						+'<button class="btn btn-default btn-show-status-kerawanan-berita" data-target-id="'+val.instalasi_id+'">Lihat Berita Terkini</button>'
						+'</div>',
						maxWidth: 200
					},
					icon : 'assets/backend_assets/img/'+val.icon_status
				});
			})
		}
	});
        $('body').delegate('.change-mark','click',function(){
            mapDash.removeMarkers();
            var status = $(this).attr('data-id');
            $.ajax({
                    
                    url: 'admin_status_kerawanan/get_location_by_status',
                    dataType: 'json',
                    type : 'POST',
                    data: {status:status},
                    success: function(data) {
                            $('#dropdown-kuningmerah').html('');
                            var values = '';
                            $.each(data, function(i, val){
                                    var mark = mapDash.addMarker({
                                            lat: val.latitude,
                                            lng:  val.longitude,
                                            title: val.instalasi_nm,
                                            infoWindow: {
                                                    content: '<h4><img src="assets/backend_assets/img/'+val.icon_status+'"> '+val.instalasi_nm+'</h4><div>'
                                                    +'<p>'+val.instalasi_alamat+'</p><br>'
                                                    +'<button class="btn btn-default btn-show-status-kerawanan-berita" data-target-id="'+val.instalasi_id+'">Lihat Berita Terkini</button>'
                                                    +'</div>',
                                                    maxWidth: 200
                                            },
                                            icon : 'assets/backend_assets/img/'+val.icon_status
                                    });
                                    values += '<option>'+val.instalasi_nm+'</option>';
                            });
                            var dropdown = '<select class="form-control">'+values+'</select>';
                            if(status == 2 || status == 3)
                            $('#dropdown-kuningmerah').html(dropdown);
                    }
            });
        });

	$('body').delegate('.btn-show-status-kerawanan-berita','click',function(){
		var id = $(this).attr('data-target-id');
		var news = ' ';
		$.ajax({
			url : 'admin_status_kerawanan/get_all_news',
			type : 'POST',
			dataType : 'Json',
			data : {'id' : id },
			success : function(result){
				$('#px-dashboard-status-kerawanan-new-modal .modal-body').html(' ');
				if(result.status == 'ok'){
                    if(result.data){
    					$.each(result.data, function(i, data){
    						var bg = 'bg-success';
                            if(data.status == 1)
                                bg = 'bg-success';
                            else if(data.status == 2)
                                bg = 'bg-warning';
                            else if(data.status == 3)
                                bg = 'bg-danger';
                            news = news+'<div data-target-id="'+data.id+'" class="list-group-item '+bg+' btn-show-detail-news-kerawanan-berita">'
                                +'<div class="media-body">'
                                    +'<span class="text-muted">'+data.date_created+'</span><br>'
                                    +data.title
                                +'</div>'
                            +'</div>'
    					});
                    }
                    $('#px-dashboard-status-kerawanan-new-modal .modal-body').html(news);
				}
                else {
                    $('#px-dashboard-status-kerawanan-new-modal .modal-body').html('Belum ditemukan berita untuk area ini');
                }
			}
		})
		$('#px-dashboard-status-kerawanan-new-modal').modal('show');
	});
    $('body').delegate('.btn-show-detail-news-kerawanan-berita','click',function(){
        var id = $(this).attr('data-target-id');
        $.ajax({
            url : 'admin_status_kerawanan/get_detail_news',
            type : 'POST',
            dataType : 'Json',
            data : {'id' : id },
            success : function(result){
                $('#px-dashboard-status-kerawanan-news-detail-modal .modal-header h4').html(' ');
                $('#px-dashboard-status-kerawanan-news-detail-modal .modal-body .content').html(' ');
                if(result.status == 'ok'){
                    // $('#px-dashboard-status-kerawanan-new-modal').modal('hide');
                    $('#px-dashboard-status-kerawanan-news-detail-modal .modal-header h4').html(result.data.title);
                    $('#px-dashboard-status-kerawanan-news-detail-modal .modal-body .content').html(result.data.content);
                }
            }
        })
        $('#px-dashboard-status-kerawanan-news-detail-modal').modal('show');
    });

	// chart
	$.ajax({
		url : 'admin/chart_jumlah_instalasi',
		type : 'POST',
		dataType : 'Json',
		success : function(result){
			
			chart_jumlah_instalasi(result.category,result.series);
		}
	});
	$.ajax({
		url : 'admin/chart_jumlah_satpam',
		type : 'POST',
		dataType : 'Json',
		success : function(result){
			chart_jumlah_satpam(result.category,result.series);
		}
	});
    $.ajax({
        url : 'admin/chart_jumlah_asesmen',
        type : 'POST',
        dataType : 'Json',
        success : function(result){
            
            chart_jumlah_asesmen(result.category,result.series);
        }
    });
    $.ajax({
        url : 'admin/chart_hasil_asesmen_per_grup',
        type : 'POST',
        dataType : 'Json',
        success : function(result){
            
            chart_hasil_asesmen_per_grup(result.category,result.series);
        }
    });
    $.ajax({
        url : 'admin/chart_hasil_asesmen_per_instalasi',
        type : 'POST',
        dataType : 'Json',
        success : function(result){
            
            chart_hasil_asesmen_per_instalasi(result.category,result.series);
        }
    });
    $.ajax({
            url : 'admin/chart_hasil_asesmen_per_grup_tahun',
            type : 'POST',
            data: {year: 0},
            dataType : 'Json',
            success : function(result){
                chart_hasil_asesmen_per_grup_tahun(result.category,result.series);
            }
        });
    $.ajax({
            url : 'admin/chart_hasil_asesmen2004_per_instalasi',
            type : 'POST',
            data: {year: 0},
            dataType : 'Json',
            success : function(result){
                chart_hasil_asesmen2004_per_instalasi(result.category,result.series);
            }
        });
    $.ajax({
            url : 'admin/chart_hasil_asesmen2015_per_instalasi',
            type : 'POST',
            data: {year: 0},
            dataType : 'Json',
            success : function(result){
                chart_hasil_asesmen2015_per_instalasi(result.series);
            }
        });
     $.ajax({
            url : 'admin/chart_kompetensi_per_grup',
            type : 'POST',
            dataType : 'Json',
            success : function(result){
                chart_kompetensi_per_grup(result.category,result.series);
            }
        });
     $.ajax({
            url : 'admin/chart_training_per_grup',
            type : 'POST',
            dataType : 'Json',
            success : function(result){
                chart_training_per_grup(result.category,result.series);
            }
        });
     $.ajax({
            url : 'admin/chart_stc_per_grup',
            type : 'POST',
            dataType : 'Json',
            success : function(result){
                chart_stc_per_grup(result.category,result.series);
            }
        });

    $.ajax({
        url : 'admin/chart_stc_perinstalasi',
        type : 'POST',
        dataType : 'Json',
        success : function(result){
            chart_stc_per_instalasi(result.data,result.nama_instalasi);
            console.log(result.data);
        }
    });

    $('#standar-kompetensi-per-instalasi-menu').change(function(){
        var id = $(this).val();
        $.ajax({
            url : 'admin/chart_stc_perinstalasi/'+id,
            type : 'POST',
            dataType : 'Json',
            success : function(result){
                chart_stc_per_instalasi(result.data,result.nama_instalasi);
                console.log(result.data);
            }
        });
    });
	function chart_jumlah_instalasi(categories_data,series_data){
	    $('#instalasi-highchart').highcharts({
	        chart: {
	            type: 'column',
                    options3d: {
                    enabled: true,
                    alpha: 15,
                    beta: 15,
                    depth: 50,
                    viewDistance: 25
                }
	        },
	        title: {
	            text: 'Jumlah Perusahaan Grup Astra'
	        },
	        xAxis: {
	            categories: categories_data,
	            crosshair: true
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Jumlah Perusahaan'
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                '<td style="padding:0"><b>{point.y}</b></td></tr>',
	            footerFormat: '</table>',
	            shared: true,
	            useHTML: true
	        },
	        plotOptions: {
	            column: {
	                pointPadding: 0.2,
	                borderWidth: 0
	            }
	        },
	        series: [series_data]
	    });
	}
	function chart_jumlah_satpam(categories_data,series_data){
	    $('#satpam-highchart').highcharts({
	        chart: {
	            type: 'bar',
                    options3d: {
                    enabled: true,
                    alpha: 15,
                    beta: 15,
                    depth: 50,
                    viewDistance: 25
                }
	        },
	        title: {
	            text: 'Jumlah Anggota Satpam Grup Astra'
	        },
	        xAxis: {
	            categories: categories_data,
	            crosshair: true
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Jumlah Satpam (orang)'
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                '<td style="padding:0"><b>{point.y}</b></td></tr>',
	            footerFormat: '</table>',
	            shared: true,
	            useHTML: true
	        },
	        legend: {
	            layout: 'vertical',
	            align: 'right',
	            verticalAlign: 'middle',
	            borderWidth: 0
	        },
	        plotOptions: {
	            column: {
	                pointPadding: 0.2,
	                borderWidth: 0
	            }
	        },
	        series: series_data
	    });
	}
    function chart_jumlah_asesmen(categories_data,series_data) {
        $('#jumlah-asesmen-highchart').highcharts({
            title: {
                text: 'Jumlah Asesmen Grup Astra'
            },
            xAxis: {
                categories: categories_data
            },
            yAxis: {
                title: {
                    text: 'Jumlah Asesmen'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: ''
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: series_data
        });
    }
	function chart_hasil_asesmen_per_grup(categories_data,series_data) {
        $('#hasil-asesmen-per-grup-highchart').highcharts({
        chart: {
            type: 'column',
                options3d: {
                    enabled: true,
                    alpha: 15,
                    beta: 15,
                    depth: 50,
                    viewDistance: 25
                }
        },
        title: {
            text: 'Grafik Hasil Asesmen per Grup Bisnis'
        },
        xAxis: {
            categories: categories_data,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Jumlah Status'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: series_data,
        colors: ['#D4AF37', '#23B14D', '#417EC1', '#ED1A23', '#000']
        });
    }
    function chart_hasil_asesmen_per_instalasi(categories_data,series_data) {
        $('#jumlah-asesmen-per-instalasi-highchart').highcharts({
            title: {
                text: 'Jumlah Asesmen Grup Astra per Instalasi'
            },
            xAxis: {
                categories: categories_data
            },
            yAxis: {
                title: {
                    text: 'Jumlah Asesmen'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: ''
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: series_data
        });
    }
    function chart_hasil_asesmen_per_grup_tahun(categories_data,series_data) {
        $('#hasil-asesmen-per-tahun-highchart').highcharts({
        chart: {
            type: 'column',
                options3d: {
                    enabled: true,
                    alpha: 15,
                    beta: 15,
                    depth: 50,
                    viewDistance: 25
                }
        },
        title: {
            text: 'Grafik Hasil Asesmen per Grup Bisnis Periode'
        },
        xAxis: {
            categories: categories_data,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Jumlah Status'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: series_data,
        colors: ['#D4AF37', '#23B14D', '#417EC1', '#ED1A23', '#000']
        });
    }
    function chart_hasil_asesmen2004_per_instalasi(categories_data,series_data) {
        $('#hasil-asesmen-2004-per-instalasi-highchart').highcharts({
            chart: {
                type: 'column',
                    options3d: {
                        enabled: true,
                        alpha: 15,
                        beta: 15,
                        depth: 50,
                        viewDistance: 25
                    }
            },
            title: {
                text: 'Auto 2000'
            },
            subtitle: {
                text: 'Grafik Hasil Asesmen 2004 per Instalasi'
            },
            xAxis: {
                categories: categories_data,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Nilai Asesmen'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: series_data,
            colors: ['#417EC1']
        });
    }
    function chart_hasil_asesmen2015_per_instalasi(series_data) {
        $('#hasil-asesmen-2015-per-instalasi-highchart').highcharts({
            chart: {
                type: 'column',
                    options3d: {
                        enabled: true,
                        alpha: 15,
                        beta: 15,
                        depth: 50,
                        viewDistance: 25
                    }
            },
            title: {
                text: 'Auto 2000'
            },
            subtitle: {
                text: 'Grafik Hasil Asesmen 2015 per Instalasi'
            },
            xAxis: {
                categories: ['Persentase Pemenuhan System','Performance','Reliability','CSI'],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Nilai (%)'

                },
                plotLines: [{
                    value: 90,
                    width: 2,
                    zIndex: 1000,
                    color: '#D4AF37',
                    label : {
                        text : 'Emas'
                    }
                },{
                    value: 76,
                    width: 2,
                    zIndex: 1000,
                    color: '#23B14D',
                    label : {
                        text : 'Hijau'
                    }
                },{
                    value: 51,
                    width: 2,
                    zIndex: 1000,
                    color: '#417EC1',
                    label : {
                        text : 'Biru'
                    }
                },{
                    value: 30,
                    width: 2,
                    zIndex: 1000,
                    color: '#ED1A23',
                    label : {
                        text : 'Merah'
                    }
                },{
                    value: 0,
                    width: 2,
                    zIndex: 1000,
                    color: '#000',
                    label : {
                        text : 'Hitam'
                    }
                }]
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: series_data,
            colors: ['#ED1A23']
        });
    }
    function chart_kompetensi_per_grup(categories_data,series_data) {
        $('#kompetensi-grup-bisnis').highcharts({
            chart: {
                type: 'column',
                    options3d: {
                        enabled: true,
                        alpha: 15,
                        beta: 15,
                        depth: 50,
                        viewDistance: 25
                    }
            },
            title: {
                text: 'Kompetensi Grup Bisnis'
            },
            xAxis: {
                categories: categories_data,
                crosshair: true
            },
            yAxis: {
                min: 0, 
                title: {
                    text: 'Grup Bisnis'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: series_data
        });
    }
    function chart_training_per_grup(categories_data,series_data) {
        //chart static
        $('#training-grup-bisnis').highcharts({
            chart: {
                type: 'column',
                    options3d: {
                        enabled: true,
                        alpha: 15,
                        beta: 15,
                        depth: 50,
                        viewDistance: 25
                    }
            },
            title: {
                text: 'Training Grup Bisnis'
            },
            xAxis: {
                categories: categories_data,
                crosshair: true
            },
            yAxis: {
                min: 0, 
                title: {
                    text: 'Grup Bisnis'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: series_data
        });
    }
    function chart_stc_per_grup(categories_data,series_data) {
        $('#stc-grup-bisnis').highcharts({
            chart: {
                type: 'column',
                    options3d: {
                        enabled: true,
                        alpha: 15,
                        beta: 15,
                        depth: 50,
                        viewDistance: 25
                    }
            },
            title: {
                text: 'STC Grup Bisnis'
            },
            xAxis: {
                categories: categories_data,
                crosshair: true
            },
            yAxis: {
                min: 0, 
                title: {
                    text: 'Grup Bisnis'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: series_data
        });
    }
    function chart_stc_per_instalasi(data_series,data_name){
        $('#hasil-stc-per-instalasi').highcharts({
            chart: {
                polar: true,
                type: 'area'
            },

            title: {
                text: 'Hasil STC Instalasi <br>'+data_name,
                x: -80
            },

            pane: {
                size: '80%'
            },

            xAxis: {
                categories: ['STC 1','STC 2','STC 3','STC 4'],
                tickmarkPlacement: 'on',
                lineWidth: 0
            },

            yAxis: {
                gridLineInterpolation: 'polygon',
                lineWidth: 0,
                min: 0
            },

            tooltip: {
                shared: true,
                pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f} %</b><br/>'
            },

            legend: {
                align: 'right',
                verticalAlign: 'top',
                y: 70,
                layout: 'vertical'
            },

            series: data_series
        });
    }
    // static chart
    $('#kompetensi-per-orang').highcharts({
        title: {
            text: 'Kompetensi Per Orang'
        },
        xAxis: {
            categories: ['Rules1','Rules2','Rules3','Rules3'],
            title: {
                text: 'Kompetensi'
            },
        },
        yAxis: {
            min: 0, 
            max: 4,
            title: {
                text: 'Score'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            color: '#FF0000',
            name: 'Actual',
            data: [3,4,2,2]
        },{
            color: '#000000',
            name: 'Std',
            data: [4,4,4,4]
        },{
            color: '#008FBF',
            name: 'Avg',
            data: [3,3,3,3]
        }]
    });
    // static chart
    $('#training-per-orang').highcharts({
        title: {
            text: 'Training Per Orang'
        },
        xAxis: {
            categories: ['Rules1','Rules2','Rules3','Rules3'],
            title: {
                text: 'Training'
            },
        },
        yAxis: {
            min: 0, 
            max: 4,
            title: {
                text: 'Score'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            color: '#FF0000',
            name: 'Actual',
            data: [2,4,4,3]
        },{
            color: '#000000',
            name: 'Std',
            data: [4,4,4,4]
        },{
            color: '#008FBF',
            name: 'Avg',
            data: [3,3,3,3]
        }]
    });
	$('#stc-per-orang').highcharts({
        chart: {
            type: 'column',
                options3d: {
                    enabled: true,
                    alpha: 15,
                    beta: 15,
                    depth: 50,
                    viewDistance: 25
                }
        },
        title: {
            text: 'STC per Orang'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: ['STC1','STC2','STC3','STC4'],
            crosshair: true
        },
        yAxis: {
            min: 0, 
            max: 100,
            title: {
                text: 'Persentase'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            },
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}%'
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [
            {
                'name' : 'STC',
                'data' : [90,80,100,20]
            }
        ],
        colors: ['#417EC1']
    });
    // static chart
    $('#kompetensi-per-instalasi-m').highcharts({
        title: {
            text: 'Kompetensi Per Instalasi'
        },
        subtitle: {
            text: 'Level Management'
        },
        xAxis: {
            categories: ['Rules1','Rules2','Rules3','Rules3'],
            title: {
                text: 'Training'
            },
        },
        yAxis: {
            min: 0, 
            max: 4,
            title: {
                text: 'Score'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            color: '#FF0000',
            name: 'Actual',
            data: [2,4,2,3]
        },{
            color: '#000000',
            name: 'Std',
            data: [4,4,4,4]
        }]
    });
    // static chart
    $('#kompetensi-per-instalasi-mm').highcharts({
        title: {
            text: 'Kompetensi Per Instalasi'
        },
        subtitle: {
            text: 'Level Middle Management'
        },
        xAxis: {
            categories: ['Rules1','Rules2','Rules3','Rules3'],
            title: {
                text: 'Training'
            },
        },
        yAxis: {
            min: 0, 
            max: 4,
            title: {
                text: 'Score'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            color: '#FF0000',
            name: 'Actual',
            data: [2,4,2,3]
        },{
            color: '#000000',
            name: 'Std',
            data: [4,4,4,4]
        }]
    });
    // static chart
    $('#kompetensi-per-instalasi-o').highcharts({
        title: {
            text: 'Kompetensi Per Instalasi'
        },
        subtitle: {
            text: 'Level Operasional'
        },
        xAxis: {
            categories: ['Rules1','Rules2','Rules3','Rules3'],
            title: {
                text: 'Training'
            },
        },
        yAxis: {
            min: 0, 
            max: 4,
            title: {
                text: 'Score'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            color: '#FF0000',
            name: 'Actual',
            data: [2,4,2,3]
        },{
            color: '#000000',
            name: 'Std',
            data: [4,4,4,4]
        }]
    });
    // static chart
    $('#training-per-instalasi-m').highcharts({
        title: {
            text: 'Training Per Instalasi'
        },
        subtitle: {
            text: 'Level Management'
        },
        xAxis: {
            categories: ['Rules1','Rules2','Rules3','Rules3'],
            title: {
                text: 'Training'
            },
        },
        yAxis: {
            min: 0, 
            max: 4,
            title: {
                text: 'Score'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            color: '#FF0000',
            name: 'Actual',
            data: [2,4,2,3]
        },{
            color: '#000000',
            name: 'Std',
            data: [4,4,4,4]
        }]
    });
    // static chart
    $('#training-per-instalasi-mm').highcharts({
        title: {
            text: 'Training Per Instalasi'
        },
        subtitle: {
            text: 'Level Middle Management'
        },
        xAxis: {
            categories: ['Rules1','Rules2','Rules3','Rules3'],
            title: {
                text: 'Training'
            },
        },
        yAxis: {
            min: 0, 
            max: 4,
            title: {
                text: 'Score'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            color: '#FF0000',
            name: 'Actual',
            data: [2,4,2,3]
        },{
            color: '#000000',
            name: 'Std',
            data: [4,4,4,4]
        }]
    });
    // static chart
    $('#training-per-instalasi-o').highcharts({
        title: {
            text: 'Training Per Instalasi'
        },
        subtitle: {
            text: 'Level Operasional'
        },
        xAxis: {
            categories: ['Rules1','Rules2','Rules3','Rules3'],
            title: {
                text: 'Training'
            },
        },
        yAxis: {
            min: 0, 
            max: 4,
            title: {
                text: 'Score'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            color: '#FF0000',
            name: 'Actual',
            data: [2,4,2,3]
        },{
            color: '#000000',
            name: 'Std',
            data: [4,4,4,4]
        }]
    });
    $('#stc-per-level').highcharts({
        chart: {
            type: 'column',
                options3d: {
                    enabled: true,
                    alpha: 15,
                    beta: 15,
                    depth: 50,
                    viewDistance: 25
                }
        },
        title: {
            text: 'STC'
        },
        xAxis: {
            categories: ['STC1','STC2','STC3','STC4'],
            crosshair: true
        },
        yAxis: {
            min: 0, 
            max: 100,
            title: {
                text: 'Jumlah Status'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            },
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}%'
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [
            {
                'name' : 'Management',
                'data' : [80,56,23,40]
            },
            {
                'name' : 'Middle Management',
                'data' : [34,12,89,50]
            },
            {
                'name' : 'Operasional',
                'data' : [34,78,98,65]
            }
        ],
        colors: ['#D4AF37', '#23B14D', '#417EC1', '#ED1A23', '#000']
    });

    /*$('#nilai-chart').highcharts({
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: 'Perolehan Nilai Asesmen 2004'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [{
                name: 'Emas',
                y: 67
            }, {
                name: 'Hijau',
                y: 34,
                sliced: true,
                selected: true
            }, {
                name: 'Biru',
                y: 12
            }, {
                name: 'Merah',
                y: 8
            }, {
                name: 'Hitam',
                y: 4
            }]
        }],
        colors: ['#D4AF37', '#23B14D', '#417EC1', '#ED1A23', '#000']
    });
    $('#nilai-2015-chart').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Perolehan Nilai Asesmen 2015'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [{
                name: 'Emas',
                y: 89
            }, {
                name: 'Hijau',
                y: 74,
                sliced: true,
                selected: true
            }, {
                name: 'Biru',
                y: 15
            }, {
                name: 'Merah',
                y: 3
            }, {
                name: 'Hitam',
                y: 1
            }]
        }],
        colors: ['#D4AF37', '#23B14D', '#417EC1', '#ED1A23', '#000']
    });
    var chart = new Highcharts.Chart({
        chart: {
            renderTo: 'stc-chart',
            type: 'column',
            options3d: {
                enabled: true,
                alpha: 15,
                beta: 15,
                depth: 50,
                viewDistance: 25
            }
        },
        title: {
            text: 'Perolehan Nilai STC Anda'
        },
        subtitle: {
            text: 'STC'
        },
        plotOptions: {
            column: {
                depth: 25
            }
        },
        series: [{
            data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
        }]
    });*/
})