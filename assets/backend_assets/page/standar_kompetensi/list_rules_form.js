$(document).ready(function(){
	$('#px-standarkompetensi-listrules-form').validate({
		rules: {                                            
			rules: {
				required: true
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-standarkompetensi-listrules-form .alert-warning').removeClass('hidden');
			$('#px-standarkompetensi-listrules-form .alert-success').addClass('hidden');
			$('#px-standarkompetensi-listrules-form .alert-danger').addClass('hidden');
			$('.px-summernote').each(function() {
				$(this).val($(this).code());
			});
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					$('#px-standarkompetensi-listrules-form .alert-warning').addClass('hidden');
					if(response.status == 'ok'){
						$('#px-standarkompetensi-listrules-form .alert-success').removeClass('hidden').children('span').text(response.msg);
						window.location.href = response.redirect;
					}
					else
						$('#px-standarkompetensi-listrules-form .alert-danger').removeClass('hidden').children('span').text(response.msg);	
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
	$("#file-upload").fileupload({
		dataType: 'text',
		autoUpload: false,
		acceptFileTypes: /(\.|\/)(doc|docx|xls|xlsx|pdf)$/i,
		maxFileSize: 10000000 // 5 MB
		}).on('fileuploadadd', function(e, data) {
			data.process();
		}).on('fileuploadprocessalways', function (e, data) {
		if (data.files.error) {
			data.abort();
			alert('Document file must be doc,docx,xls,xlsx or pdf with less than 5MB');
		} else {
			data.submit();
		}
		}).on('fileuploadprogressall', function (e, data) {
			var progress = parseInt(data.loaded / data.total * 100, 10);
			var target = $('#target-file').val();
			$('#px-site-content-'+target+'-form-file-upload-progress').text('Upload Progress '+progress+'%');
		}).on('fileuploaddone', function (e, data) {
			var target = $('#target-file').val();
			var preview_template = '<div class="per-preview-document">'
										+'<input class="form-control" type="hidden" name="document[]" value="'+data.result+'">'
										+'<i class="fa fa-file-o"></i> <span>'+baseName(data.result)+'</span>'
										+'<a href="'+data.result+'"><i class="fa fa-download btn-download" title="Download"></i></a>'
										+'<i class="fa fa-times btn-delete-file" title="Delete"></i>'
									+'</div>';	
			$('#preview-'+target).append(preview_template);
		}).on('fileuploadfail', function (e, data) {
			alert('File upload failed.');
		}).on('fileuploadalways', function() {
	});
	$('.btn-upload').click(function(){
		var target = $(this).attr('data-target');
		var old_temp = $('[name="'+target+'"]').val();
		$('#file-upload #target-file').val(target);
		$('#file-upload #old-file').val(old_temp);
	});
	$('body').delegate('.btn-delete-file','click',function(){
		$(this).parent().remove();
	});
	function baseName(str)
	{
	   var base = new String(str).substring(str.lastIndexOf('/') + 1); 
	    if(base.lastIndexOf(".") != -1)       
	        base = base.substring(0, base.lastIndexOf("."));
	   return base;
	}
})