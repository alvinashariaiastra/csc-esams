$(document).ready(function(){
	$('#px-standarkompetensi-training-form').validate({
		rules: {                                            
			rules: {
				required: true
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-standarkompetensi-training-form .alert-warning').removeClass('hidden');
			$('#px-standarkompetensi-training-form .alert-success').addClass('hidden');
			$('#px-standarkompetensi-training-form .alert-danger').addClass('hidden');
			$('.px-summernote').each(function() {
				$(this).val($(this).code());
			});
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					$('#px-standarkompetensi-training-form .alert-warning').addClass('hidden');
					if(response.status == 'ok'){
						$('#px-standarkompetensi-training-form .alert-success').removeClass('hidden').children('span').text(response.msg);
						window.location.href = response.redirect;
					}
					else
						$('#px-standarkompetensi-training-form .alert-danger').removeClass('hidden').children('span').text(response.msg);	
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
})