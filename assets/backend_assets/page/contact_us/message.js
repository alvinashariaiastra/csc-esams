$(document).ready(function(){
	$("#message_form").submit(function(e) {
	    var url = "admin_contact_us/ajax_message_save"; // the script where you handle the form input.
	    $.ajax({
	           type: "POST",
	           url: url,
	           data: $("#message_form").serialize(), // serializes the form's elements.
	           success: function(data)
	           {
	           	$('.message_txt').val('');
	           	$('.messages').append(data);
	           }
	         });

	    e.preventDefault(); // avoid to execute the actual submit of the form.
	});
});