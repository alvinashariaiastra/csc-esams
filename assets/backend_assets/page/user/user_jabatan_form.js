$(document).ready(function(){
	$('#px-admin-user-user-jabatan-form').validate({
		ignore: [],
		rules: {                                            
			realname: {
				required: true
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-admin-user-user-jabatan-form .alert-warning').removeClass('hidden');
			$('#px-admin-user-user-jabatan-form .alert-success').addClass('hidden');
			$('#px-admin-user-user-jabatan-form .alert-danger').addClass('hidden');
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					$('#px-admin-user-user-jabatan-form .alert-warning').addClass('hidden');
					if(response.status == 'ok'){
						$('#px-admin-user-user-jabatan-form .alert-success').removeClass('hidden').children('span').text(response.msg);
						window.location.href = response.redirect;
					}
					else
						$('#px-admin-user-user-jabatan-form .alert-danger').removeClass('hidden').children('span').text(response.msg);	
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
})