$(document).ready(function(){
	$('#px-admin-user-user-list-form').validate({
		ignore: [],
		rules: {                                            
			username: {
				required: true,
				remote:
				{
					url: 'admin_user/admin_user_check_username',
					type: "post",
					data:
					{
						username: function()
						{
							return $('#px-admin-user-user-list-form #px-admin-user-user-list-form-user-list-username').val();
						},
						id : function()
						{
							return $('#px-admin-user-user-list-form #px-admin-user-user-list-form-user-list-id').val();
						}
					}
				}
			},
			realname: {
				required: true
			},
			password: {
				required: true
			},
			confirm_password: {
				equalTo: '#px-admin-user-user-list-form-user-list-password'
			},
			email: {
				required: true,
				remote:
				{
					url: 'admin_user/admin_user_check_email',
					type: "post",
					data:
					{
						email: function()
						{
							return $('#px-admin-user-user-list-form #px-admin-user-user-list-form-user-list-email').val();
						},
						id : function()
						{
							return $('#px-admin-user-user-list-form #px-admin-user-user-list-form-user-list-id').val();
						}
					}
				}
			}
		},
		messages: {
			email:
			{
				required: "Please enter your email address.",
				email: "Please enter a valid email address.",
				remote: jQuery.validator.format("{0} is already taken.")
			},
			username:
			{
				required: "Please enter your username.",
				remote: jQuery.validator.format("{0} is already taken.")
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-admin-user-user-list-form .alert-warning').removeClass('hidden');
			$('#px-admin-user-user-list-form .alert-success').addClass('hidden');
			$('#px-admin-user-user-list-form .alert-danger').addClass('hidden');
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					$('#px-admin-user-user-list-form .alert-warning').addClass('hidden');
					if(response.status == 'ok'){
						$('#px-admin-user-user-list-form .alert-success').removeClass('hidden').children('span').text(response.msg);
						window.location.href = response.redirect;
					}
					else
						$('#px-admin-user-user-list-form .alert-danger').removeClass('hidden').children('span').text(response.msg);	
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
	$("#file-upload").fileupload({
		dataType: 'text',
		autoUpload: false,
		acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
		maxFileSize: 5000000 // 5 MB
		}).on('fileuploadadd', function(e, data) {
			data.process();
		}).on('fileuploadprocessalways', function (e, data) {
		if (data.files.error) {
			data.abort();
			alert('Image file must be jpeg/jpg, png or gif with less than 5MB');
		} else {
			data.submit();
		}
		}).on('fileuploadprogressall', function (e, data) {
			var progress = parseInt(data.loaded / data.total * 100, 10);
			var target = $('#target-file').val();
			$('#px-admin-user-user-list-fileupload-'+target+'-progress').text(progress+'%');
			$('#px-admin-user-user-list-fileupload-'+target+'-upload-button').attr('disabled',true);
		}).on('fileuploaddone', function (e, data) {
			var target = $('#target-file').val();
			$('#px-admin-user-user-list-fileupload-'+target+'-upload-button').removeAttr('disabled');
			$('#preview-'+target).removeClass('hidden');
			$('[name="'+target+'"]').val(data.result);
			$('#image-original-previews').html('<img src="'+data.result+'" alt="preview" id="original-image">');
			$('#image-crop-previews').html('<img src="'+data.result+'" alt="preview" id="crop-image">');
			$('#image').val(data.result);
			origImageVal();
			$('#original-image').Jcrop({
				onChange: showPreview,
				onSelect: showPreview,
				aspectRatio: 1 / 1,
				setSelect:  [ 0, 12, 569, 227.6 ]
			});
		}).on('fileuploadfail', function (e, data) {
			alert('File upload failed.');
			$('#px-admin-user-user-list-fileupload-upload-button').removeAttr('disabled');
		}).on('fileuploadalways', function() {
	});
	$('.btn-upload').click(function(){
		var target = $(this).attr('data-target');
		var old_temp = $('[name="'+target+'"]').val();
		$('#file-upload #target-file').val(target);
		$('#file-upload #old-file').val(old_temp);
	});
        if($('#original-image').length > 0){
		origImageVal();
		var imgwidth = $('#original-image').width();
		var imgheight = $('#original-image').height();
		$('#fakeheight').val(imgheight);
		$('#fakewidth').val(imgwidth);
		var ratios = 1 / 1;
		$('#original-image').Jcrop({
			onChange: showPreview,
			onSelect: showPreview,
			aspectRatio: ratios
		});
	}
	function origImageVal(){
		var origImg = new Image();
		origImg.src = $('#original-image').attr('src');
		origImg.onload = function() {
			var origheight = origImg.height;
			var origwidth = origImg.width;
			$('#origheight').val(origheight);
			$('#origwidth').val(origwidth);
		}
	}
	function showPreview(coords)
	{
		var image_asli = $('#original-image').attr('src');
		var imgwidth = $('#original-image').width();
		var imgheight = $('#original-image').height();
		$('#fakeheight').val(imgheight);
		$('#fakewidth').val(imgwidth);
		var rx = 100 / coords.w;
		var ry = 100 / coords.h;

		$('#crop-image').attr('src',image_asli).css({
			width: Math.round(rx * imgwidth) + 'px',
			height: Math.round(ry * imgheight) + 'px',
			marginLeft: '-' + Math.round(rx * coords.x) + 'px',
			marginTop: '-' + Math.round(ry * coords.y) + 'px'
		});
		$('#x').val(coords.x);
		$('#y').val(coords.y);
		$('#w').val(coords.w);
		$('#h').val(coords.h);
	}
	$('.btn-show-pass').click(function(){
		var status = $(this).attr('data-status');
		if(status == 'hidden'){
			$(this).attr('data-status','visible');
			$(this).removeClass('fa-eye-slash').addClass('fa-eye');
			$('#px-admin-user-user-list-form-user-list-password').attr('type','text');
			$('#px-admin-user-user-list-form-user-list-confirm-password').attr('type','text');
		}
		else{
			$(this).attr('data-status','hidden');
			$(this).removeClass('fa-eye').addClass('fa-eye-slash');
			$('#px-admin-user-user-list-form-user-list-password').attr('type','password');
			$('#px-admin-user-user-list-form-user-list-confirm-password').attr('type','password');
		}
	});
	$('#px-admin-user-user-list-form-user-list-perusahaan').change(function() {
		var perusahaan_id = this.value;

		$("#px-admin-user-user-list-form-user-list-instalasi option").remove();
		$('#px-admin-user-user-list-form-user-list-instalasi').append($('<option>', {
			value: '0',
			text : '-- Pilih Instalasi --'
		}));

		$.ajax({
			url: 'admin_user/ajax_get_instalasi',
			type: 'POST',
			dataType: 'json',
			data: {'id': perusahaan_id},
			success : function(result){
				var data = result.result;
				//console.log(data);
				if (result.status == 'ok') {
					$.each(data, function (i, item) {
		                $('#px-admin-user-user-list-form-user-list-instalasi').append($('<option>', {
		                    value: item.id,
		                    text : item.name
		                }));
	            	});
				};	
			},
			error : function(jqXHR, textStatus, errorThrown) {
				alert(textStatus, errorThrown);
			}

		})
	});
})