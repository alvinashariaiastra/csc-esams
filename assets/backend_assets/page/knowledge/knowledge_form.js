$(document).ready(function(){
	$('#px-knowledge-knowledge-form').validate({
		rules: {                                            
			title: {
				required: true
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-knowledge-knowledge-form .alert-warning').removeClass('hidden');
			$('#px-knowledge-knowledge-form .alert-success').addClass('hidden');
			$('#px-knowledge-knowledge-form .alert-danger').addClass('hidden');
			$('.px-summernote').each(function() {
				$(this).val($(this).code());
			});
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					$('#px-knowledge-knowledge-form .alert-warning').addClass('hidden');
					if(response.status == 'ok'){
						$('#px-knowledge-knowledge-form .alert-success').removeClass('hidden').children('span').text(response.msg);
						window.location.href = response.redirect;
					}
					else
						$('#px-knowledge-knowledge-form .alert-danger').removeClass('hidden').children('span').text(response.msg);	
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
    $('#file-upload').fileupload({
		dataType: 'text',
		autoUpload: false,
		maxFileSize: 100000000 // 5 MB
		}).on('fileuploadadd', function(e, data) {
			data.process();
		}).on('fileuploadprocessalways', function (e, data) {
		if (data.files.error) {
			data.abort();
			alert('File must be less than 100MB');
		} else {
			data.submit();
		}
		}).on('fileuploadprogressall', function (e, data) {
			var progress = parseInt(data.loaded / data.total * 100, 10);
			var target = $('#target-file').val();
			$('#px-knowledge-knowledge-fileupload-'+target+'-progress').text(progress+'%');
			$('#px-knowledge-knowledge-fileupload-'+target+'-upload-button').attr('disabled',true);
		}).on('fileuploaddone', function (e, data) {
			var target = $('#target-file').val();
			var arr = data.result.split('/');
			$('#px-knowledge-knowledge-fileupload-'+target+'-upload-button').removeAttr('disabled');
			$('#preview_'+target+' a').attr('href',data.result).val(data.result);
			$('#preview_'+target+' span').html(arr[4]);
			$('[name="file_src"]').val(data.result);
			$('[name="'+target+'"]').val(arr[4]);
		}).on('fileuploadfail', function (e, data) {
			alert('File upload failed.');
			$('#px-knowledge-knowledge-fileupload-upload-button').removeAttr('disabled');
		}).on('fileuploadalways', function() {
	});
	$('.btn-upload').click(function(){
		var target = $(this).attr('data-target');
		var old_temp = $('[name="'+target+'"]').val();
		$('#file-upload #target-file').val(target);
		$('#file-upload #old-file').val(old_temp);
	});
})