$(document).ready(function(){
  $('#px-esams-masterdata-group-detail-form').validate({
    ignore: [],
    rules: {                                            
      id: {
        required: true
      }
    },
    submitHandler: function(form) {
      var target = $(form).attr('action');
      $('#px-esams-masterdata-group-detail-form .msg-status').text('Deleting data');
      $.ajax({
        url : target,
        type : 'POST',
        dataType : 'json',
        data : $(form).serialize(),
        success : function(response){
          if(response.status == 'ok'){
            $('#px-esams-masterdata-group-detail-form .msg-status').text('Delete Success...');
            window.location.href = response.redirect;
          }
          else
            $('#px-esams-masterdata-group-detail-form .msg-status').text('Delete Failed');    
        },
        error : function(jqXHR, textStatus, errorThrown) {
          alert(textStatus, errorThrown);
        }
      });
    }
  });
  $('body').delegate('.btn-delete','click',function(){
    var id = $(this).attr('data-target-id');
    $('#px-esams-masterdata-group-detail-form input[name="id"]').val('');
    $('#px-esams-masterdata-group-detail-form input[name="id"]').val(id);
    $('#px-esams-masterdata-group-detail-box').addClass('open');
  });
  $('#px-esams-masterdata-group-detail-modal').on('hidden.bs.modal', function (e) {
    $('#px-esams-masterdata-group-detail-form').validate().resetForm();
  });
  $('#px-esams-masterdata-group-detail-modal').on('shown.bs.modal', function (e) {
    $('#px-esams-masterdata-group-detail-form').validate().resetForm();
  });
})