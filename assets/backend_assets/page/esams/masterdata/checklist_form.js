$(document).ready(function(){
	$('#px-esams-masterdata-checklist-form').validate({
		rules: {},
		submitHandler: function(form) {
			var target = $(form).attr('action');
            /*alert(target);
            return false;*/
			$('#px-esams-masterdata-checklist-form .alert-warning').removeClass('hidden');
			$('#px-esams-masterdata-checklist-form .alert-success').addClass('hidden');
			$('#px-esams-masterdata-checklist-form .alert-danger').addClass('hidden');
			$('.px-summernote').each(function() {
				$(this).val($(this).code());
			});
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					$('#px-esams-masterdata-checklist-form .alert-warning').addClass('hidden');
					if(response.status == 'ok'){
						$('#px-esams-masterdata-checklist-form .alert-success').removeClass('hidden').children('span').text(response.msg);
						window.location.href = response.redirect;
					}
					else
						$('#px-esams-masterdata-checklist-form .alert-danger').removeClass('hidden').children('span').text(response.msg);	
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
	// Add Checklist Value
	let iEventChecklistValue = 0
    $("#addchecklistvalue").click(function() {
    	set_tbody_checklist_value(iEventChecklistValue)
        iEventChecklistValue += 1
    })
    // Delete Checklist Value Current
    $('.delcurrchecklistvalue').click(function(ev) {
        if (ev.type == 'click') {
            let str_id = $('#str_id_delete_checklistvalue').val()
            let row = $(this).attr('row')
            $('.current-checklistvalue-'+row).fadeOut()
            $('.current-checklistvalue-'+row).remove()
            $('#delete_checklistvalue').modal('hide')
            $('#divdeletechecklistvalue').append('<input type="hidden" name="checklist_value_delete[]" value="'+str_id+'">')
        }   
    })
	// Select2 Lib
    $('.select2ins').select2()
})

function set_tbody_checklist_value(iEventChecklistValue, data=null) {
	$('#tbodyaddchecklistvalue').append(tbody_checklist_value(iEventChecklistValue, data))
    $('.delchecklistvalue').click(function(ev){
        if (ev.type == 'click') {
            $(this).parents('.records').fadeOut()
            $(this).parents('.records').remove()
        }   
    })
}

$('.delchecklistvalue').click(function(ev){
    if (ev.type == 'click') {
        $(this).parents('.records').fadeOut()
        $(this).parents('.records').remove()
    }   
})

function tbody_checklist_value(iEventChecklistValue, data=null) {
	return `
		<tr class="records">
            <td>
                <div class="form-group row">
                    <div class="col-md-8">
                        <input type="text" name="value[]" class="form-control m-input" autocomplete="off" _i="`+iEventChecklistValue+`" value="`+(data?data.value:'')+`">
                        <sup>Value</sup>
                    </div>
                    <div class="col-md-4">
                    	<input type="hidden" name="checklist_detail_id[]" value="`+(data?data.id:'')+`">
	                    <input type="hidden" name="checklist_value[]" value="`+iEventChecklistValue+`">
	                    <a class="btn btn-danger btn-sm delchecklistvalue" href="javascript:;"><i class="icon mdi mdi-delete"></i> Delete</a>
                    </div>
                </div>
            </td>
        </tr>
	`
}

// Get Building
function get_building_by_instalasi(select, instalasi_id) {
    $.ajax({  
        type    : 'get',  
        url     : base_url+'esams/masterdata/get_building_by_instalasi/'+instalasi_id,  
        cache   : false, 
        beforeSend: function() {
        },                  
        success: function(response) {
            // console.log(response)
            if (response.length > 0) {
            	$(select).append('<option value="" selected="" disabled=""></option>')
            	for (let i in response)
            		$(select).append('<option value="'+response[i].id+'">'+response[i].building_name+'</option>')
            }
        },
        error: function(response) {
            // console.log(response.responseJSON)
            if (typeof response.responseJSON === 'object') {
                alert('error something happened on our response api')
                return false
            }
            alert(response.responseJSON)
        },
        complete: function(response) {
        }
    })   
}

// Get Detail Building
function get_detail_building_by_building(select, building_id) {
    $.ajax({  
        type    : 'get',  
        url     : base_url+'esams/masterdata/get_detail_building_by_building/'+building_id,  
        cache   : false, 
        beforeSend: function() {
        },                  
        success: function(response) {
            // console.log(response)
            if (response.length > 0) {
            	$(select).append('<option value="" selected="" disabled=""></option>')
            	for (let i in response)
            		$(select).append('<option value="'+response[i].id+'">Floor '+response[i].floor+'</option>')
            }
        },
        error: function(response) {
            // console.log(response.responseJSON)
            if (typeof response.responseJSON === 'object') {
                alert('error something happened on our response api')
                return false
            }
            alert(response.responseJSON)
        },
        complete: function(response) {
        }
    })   
}

// Get Checkpoint
function get_checkpoint_by_detail_building(select, building_detail_id) {
    $.ajax({  
        type    : 'get',  
        url     : base_url+'esams/masterdata/get_checkpoint_by_detail_building/'+building_detail_id,  
        cache   : false, 
        beforeSend: function() {
        },                  
        success: function(response) {
            // console.log(response)
            if (response.length > 0) {
            	$(select).append('<option value="" selected="" disabled=""></option>')
            	for (let i in response)
            		$(select).append('<option value="'+response[i].id+'">'+response[i].checkpoint_name+'</option>')
            }
        },
        error: function(response) {
            // console.log(response.responseJSON)
            if (typeof response.responseJSON === 'object') {
                alert('error something happened on our response api')
                return false
            }
            alert(response.responseJSON)
        },
        complete: function(response) {
        }
    })   
}

// Select Option Instalasi
$('#px-system-usergroup-form-instalasi_id').on('change', function() {
    let instalasi_id = $(this).val()
    if (instalasi_id=='')
    	return false
    $('#px-system-usergroup-form-building_id').empty()
    $('#px-system-usergroup-form-building_id').val('').trigger('change')
    $('#px-system-usergroup-form-building_detail_id').empty()
    $('#px-system-usergroup-form-building_detail_id').val('').trigger('change')
    $('#px-system-usergroup-form-checkpoint_id').empty()
    $('#px-system-usergroup-form-checkpoint_id').val('').trigger('change')
    get_building_by_instalasi('#px-system-usergroup-form-building_id', instalasi_id)
})

// Select Option Building
$('#px-system-usergroup-form-building_id').on('change', function() {
    let building_id = $(this).val()
    if (building_id=='')
    	return false
    $('#px-system-usergroup-form-building_detail_id').empty()
    $('#px-system-usergroup-form-building_detail_id').val('').trigger('change')
    $('#px-system-usergroup-form-checkpoint_id').empty()
    $('#px-system-usergroup-form-checkpoint_id').val('').trigger('change')
    get_detail_building_by_building('#px-system-usergroup-form-building_detail_id', building_id)
})

// Select Option Detail Building
$('#px-system-usergroup-form-building_detail_id').on('change', function() {
    let building_detail_id = $(this).val()
    if (building_detail_id=='')
    	return false
    $('#px-system-usergroup-form-checkpoint_id').empty()
    $('#px-system-usergroup-form-checkpoint_id').val('').trigger('change')
    get_checkpoint_by_detail_building('#px-system-usergroup-form-checkpoint_id', building_detail_id)
})