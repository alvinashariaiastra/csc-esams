$(document).ready(function(){
	$('#px-esams-masterdata-detail_pattern-form').validate({
		rules: {                                            
			pattern_id: {
				required: true
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-esams-masterdata-detail_pattern-form .alert-warning').removeClass('hidden');
			$('#px-esams-masterdata-detail_pattern-form .alert-success').addClass('hidden');
			$('#px-esams-masterdata-detail_pattern-form .alert-danger').addClass('hidden');
			$('.px-summernote').each(function() {
				$(this).val($(this).code());
			});
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					$('#px-esams-masterdata-detail_pattern-form .alert-warning').addClass('hidden');
					if(response.status == 'ok'){
						$('#px-esams-masterdata-detail_pattern-form .alert-success').removeClass('hidden').children('span').text(response.msg);
						window.location.href = response.redirect;
					}
					else
						$('#px-esams-masterdata-detail_pattern-form .alert-danger').removeClass('hidden').children('span').text(response.msg);	
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
    // Add Detail Pattern
	let iEventDetailPattern = 0
    $("#adddetailpattern").click(function() {
    	set_tbody_detail_pattern(iEventDetailPattern)
        iEventDetailPattern += 1
    })
    // Delete Detail Pattern Current
    $('.delcurrdetailpattern').click(function(ev) {
        if (ev.type == 'click') {
            let str_id = $('#str_id_delete_detailpattern').val()
            let row = $(this).attr('row')
            $('.current-detailpattern-'+row).fadeOut()
            $('.current-detailpattern-'+row).remove()
            $('#delete_detailpattern').modal('hide')
            $('#divdeletedetailpattern').append('<input type="hidden" name="detail_pattern_delete[]" value="'+str_id+'">')
        }   
    })
    // Select2 Lib
    $('.select2ins').select2()
    // Get Detail pattern for edit
    // let pattern_id = $('input[name="id"]').val()
    // if (pattern_id!='')
    // 	get_detail_pattern(pattern_id)
})

function tbody_detail_pattern(iEventDetailPattern, data=null) {
	return `
		<tr class="records">
            <td>
                <div class="form-group row">
                    <div class="col-md-8">
                        <select class="form-control m-input" name="instalasi[]" id="select-instalasi-`+iEventDetailPattern+`" _i="`+iEventDetailPattern+`">
                            <option value="" disabled="" selected=""></option>
                        </select>
                        <sup>Instalasi</sup>
                    </div>
                    <div class="col-md-8">
                        <select class="form-control m-input" name="building[]" id="select-building-`+iEventDetailPattern+`" _i="`+iEventDetailPattern+`">
                            <option value="" disabled="" selected=""></option>
                        </select>
                        <sup>Building</sup>
                    </div>
                    <div class="col-md-8">
                        <select class="form-control m-input" name="detail_building[]" id="select-detail-building-`+iEventDetailPattern+`" _i="`+iEventDetailPattern+`">
                            <option value="" disabled="" selected=""></option>
                        </select>
                        <sup>Detail Building</sup>
                    </div>
                    <div class="col-md-6">
                        <select class="form-control m-input" name="checkpoint[]" id="select-checkpoint-`+iEventDetailPattern+`" _i="`+iEventDetailPattern+`">
                            <option value="" disabled="" selected=""></option>
                        </select>
                        <sup>Checkpoint</sup>
                    </div>
                    <div class="col-md-3">
                        <input type="text" name="sequence[]" class="form-control m-input" autocomplete="off" _i="`+iEventDetailPattern+`" value="`+(data?data.sequence:'')+`">
                        <sup>Sequence</sup>
                    </div>
                    <div class="col-md-3">
                    	<input type="hidden" name="detail_pattern_id[]" value="`+(data?data.id:'')+`">
	                    <input type="hidden" name="detail_pattern[]" value="`+iEventDetailPattern+`">
	                    <a class="btn btn-danger btn-sm deldetailpattern" href="javascript:;"><i class="icon mdi mdi-delete"></i> Delete</a>
                    </div>
                </div>
            </td>
        </tr>
	`
}

function set_tbody_detail_pattern(iEventDetailPattern, data=null) {
	$('#tbodyadddetailpattern').append(tbody_detail_pattern(iEventDetailPattern, data))
    $('.deldetailpattern').click(function(ev){
        if (ev.type == 'click') {
            $(this).parents('.records').fadeOut()
            $(this).parents('.records').remove()
        }   
    })
    // Setup select option
    $('#select-instalasi-'+iEventDetailPattern).select2({ placeholder : 'Select Instalasi' })
    $('#select-building-'+iEventDetailPattern).select2({ placeholder : 'Select Building' })
    $('#select-detail-building-'+iEventDetailPattern).select2({ placeholder : 'Select Detail Building' })
    $('#select-checkpoint-'+iEventDetailPattern).select2({ placeholder : 'Select Checkpoint' })
    // get_instalasi('#select-instalasi-'+iEventDetailPattern, (data?data.instalasi_id:null))
    /* Not Used
    if (data) {
        let xtime = 500*_xtime;
        if (data.instalasi_id) {
            get_instalasi('#select-instalasi-'+iEventDetailPattern, data.instalasi_id)
        }
        if (data.instalasi_id && data.building_id) {
            setTimeout(function(){
                get_building_by_instalasi(data.instalasi_id, '#select-building-'+iEventDetailPattern, data.building_id)
            }, xtime*1)
        }
        if (data.building_id && data.building_detail_id) {
            setTimeout(function(){
                get_detail_building_by_building(data.building_id, '#select-detail-building-'+iEventDetailPattern, data.building_detail_id)
            }, xtime*2)
        }
        if (data.building_detail_id && data.checkpoint_id) {
            setTimeout(function(){ 
                get_checkpoint_by_detail_building(data.building_detail_id, '#select-checkpoint-'+iEventDetailPattern, data.checkpoint_id)
            }, xtime*3)
        }
    } else 
        get_instalasi('#select-instalasi-'+iEventDetailPattern)
        */
    get_instalasi('#select-instalasi-'+iEventDetailPattern)
    // Select Option Instalasi
    $('#select-instalasi-'+iEventDetailPattern).on('change', function() {
        let instalasi_id = $(this).val()
        if (instalasi_id=='') return false
        $('#select-building-'+iEventDetailPattern).empty()
        $('#select-detail-building-'+iEventDetailPattern).empty()
        $('#select-checkpoint-'+iEventDetailPattern).empty()
        $('#select-building-'+iEventDetailPattern).val('').trigger('change')
        $('#select-detail-building-'+iEventDetailPattern).val('').trigger('change')
        $('#select-checkpoint-'+iEventDetailPattern).val('').trigger('change')
        get_building_by_instalasi(instalasi_id, '#select-building-'+iEventDetailPattern)
    })
    // Select Option Building
    $('#select-building-'+iEventDetailPattern).on('change', function() {
        let building_id = $(this).val()
        if (building_id=='') return false
        $('#select-detail-building-'+iEventDetailPattern).empty()
        $('#select-checkpoint-'+iEventDetailPattern).empty()
        $('#select-detail-building-'+iEventDetailPattern).val('').trigger('change')
        $('#select-checkpoint-'+iEventDetailPattern).val('').trigger('change')
        get_detail_building_by_building(building_id, '#select-detail-building-'+iEventDetailPattern)
    })
    // Select Option Detail Building
    $('#select-detail-building-'+iEventDetailPattern).on('change', function() {
        let detail_building_id = $(this).val()
        if (detail_building_id=='') return false
        $('#select-checkpoint-'+iEventDetailPattern).empty()
        $('#select-checkpoint-'+iEventDetailPattern).val('').trigger('change')
        get_checkpoint_by_detail_building(detail_building_id, '#select-checkpoint-'+iEventDetailPattern)
    })
    // Select Option Checkpoint
    // get_checkpoint('#select-checkpoint-'+iEventDetailPattern, (data?data.checkpoint_id:null))
}

// Select Option Instalasi
$('.select-instalasi').on('change', function() {
    let instalasi_id = $(this).val()
    if (instalasi_id=='') return false
    $('.select-building').empty()
    $('.select-detail-building').empty()
    $('.select-checkpoint').empty()
    $('.select-building').val('').trigger('change')
    $('.select-detail-building').val('').trigger('change')
    $('.select-checkpoint').val('').trigger('change')
    get_building_by_instalasi(instalasi_id, '.select-building')
})
// Select Option Building
$('.select-building').on('change', function() {
    let building_id = $(this).val()
    if (building_id=='') return false
    $('.select-detail-building').empty()
    $('.select-checkpoint').empty()
    $('.select-detail-building').val('').trigger('change')
    $('.select-checkpoint').val('').trigger('change')
    get_detail_building_by_building(building_id, '.select-detail-building')
})
// Select Option Detail Building
$('.select-detail-building').on('change', function() {
    let detail_building_id = $(this).val()
    if (detail_building_id=='') return false
    $('.select-checkpoint').empty()
    $('.select-checkpoint').val('').trigger('change')
    get_checkpoint_by_detail_building(detail_building_id, '.select-checkpoint')
})
// Delete Row Record
$('.deldetailpattern').click(function(ev){
    if (ev.type == 'click') {
        $(this).parents('.records').fadeOut()
        $(this).parents('.records').remove()
    }   
})

// Get Detail Pattern // Not Used
function get_detail_pattern(pattern_id) {
    $.ajax({  
        type    : 'get',  
        url     : base_url+'esams/masterdata/get_detail_pattern/'+pattern_id,  
        cache   : false, 
        beforeSend: function() {
        },                  
        success: function(response) {
            // console.log(response)
            if (response.length > 0) {
            	for (let i in response)
                    set_tbody_detail_pattern(('0'+i), response[i])
            }
        },
        error: function(response) {
            // console.log(response.responseJSON)
            if (typeof response.responseJSON === 'object') {
                alert('error something happened on our response api')
                return false
            }
            alert(response.responseJSON)
        },
        complete: function(response) {
        }
    })
}

// Get Instalasi
function get_instalasi(select, selected=null) {
    $.ajax({  
        type    : 'get',  
        url     : base_url+'esams/masterdata/get_instalasi',  
        cache   : false, 
        beforeSend: function() {
        },                  
        success: function(response) {
            // console.log(response)
            if (response.length > 0) {
                for (let i in response)
                    $(select).append('<option value="'+response[i].id+'">'+response[i].name+'</option>')
                if (selected)
                    $(select).val(selected).trigger('change')
            }
        },
        error: function(response) {
            // console.log(response.responseJSON)
            if (typeof response.responseJSON === 'object') {
                alert('error something happened on our response api')
                return false
            }
            alert(response.responseJSON)
        },
        complete: function(response) {
        }
    })   
}

// Get Building
function get_building_by_instalasi(instalasi_id, select, selected=null) {
    $.ajax({  
        type    : 'get',  
        url     : base_url+'esams/masterdata/get_building_by_instalasi/'+instalasi_id,  
        cache   : false, 
        beforeSend: function() {
        },                  
        success: function(response) {
            // console.log(response)
            if (response.length > 0) {
                // $(select).append('<option value=""></option>')
                for (let i in response)
                    $(select).append('<option value="'+response[i].id+'">'+response[i].building_name+'</option>')
                if (selected)
                    $(select).val(selected).trigger('change')
            }
        },
        error: function(response) {
            // console.log(response.responseJSON)
            if (typeof response.responseJSON === 'object') {
                alert('error something happened on our response api')
                return false
            }
            alert(response.responseJSON)
        },
        complete: function(response) {
        }
    })   
}

// Get Detail Building
function get_detail_building_by_building(building_id, select, selected=null) {
    $.ajax({  
        type    : 'get',  
        url     : base_url+'esams/masterdata/get_detail_building_by_building/'+building_id,  
        cache   : false, 
        beforeSend: function() {
        },                  
        success: function(response) {
            // console.log(response)
            if (response.length > 0) {
                // $(select).append('<option value=""></option>')
                for (let i in response)
                    $(select).append('<option value="'+response[i].id+'">Floor '+response[i].floor+'</option>')
                if (selected)
                    $(select).val(selected).trigger('change')
            }
        },
        error: function(response) {
            // console.log(response.responseJSON)
            if (typeof response.responseJSON === 'object') {
                alert('error something happened on our response api')
                return false
            }
            alert(response.responseJSON)
        },
        complete: function(response) {
        }
    })   
}

// Get Checkpoint
function get_checkpoint_by_detail_building(detail_building_id, select, selected=null) {
    $.ajax({  
        type    : 'get',  
        url     : base_url+'esams/masterdata/get_checkpoint_by_detail_building/'+detail_building_id,  
        cache   : false, 
        beforeSend: function() {
        },                  
        success: function(response) {
            // console.log(response)
            if (response.length > 0) {
                // $(select).append('<option value=""></option>')
                for (let i in response)
                    $(select).append('<option value="'+response[i].id+'">'+response[i].checkpoint_name+'</option>')
                if (selected)
                    $(select).val(selected).trigger('change')
            }
        },
        error: function(response) {
            // console.log(response.responseJSON)
            if (typeof response.responseJSON === 'object') {
                alert('error something happened on our response api')
                return false
            }
            alert(response.responseJSON)
        },
        complete: function(response) {
        }
    })   
}
function get_checkpoint(select, selected=null) {
    $.ajax({  
        type    : 'get',  
        url     : base_url+'esams/masterdata/get_checkpoint',  
        cache   : false, 
        beforeSend: function() {
        },                  
        success: function(response) {
            // console.log(response)
            if (response.length > 0) {
            	for (let i in response)
            		$(select).append('<option value="'+response[i].id+'">'+response[i].checkpoint_name+'</option>')
            	if (selected)
          			$(select).val(selected).trigger('change')
            }
        },
        error: function(response) {
            // console.log(response.responseJSON)
            if (typeof response.responseJSON === 'object') {
                alert('error something happened on our response api')
                return false
            }
            alert(response.responseJSON)
        },
        complete: function(response) {
        }
    })   
}