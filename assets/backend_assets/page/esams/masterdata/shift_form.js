$(document).ready(function(){
	$('#px-esams-masterdata-shift-form').validate({
		rules: {                                            
			shift_name: {
				required: true
			},
			start_time: {
				required: true
			},
			end_time: {
				required: true
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-esams-masterdata-shift-form .alert-warning').removeClass('hidden');
			$('#px-esams-masterdata-shift-form .alert-success').addClass('hidden');
			$('#px-esams-masterdata-shift-form .alert-danger').addClass('hidden');
			$('.px-summernote').each(function() {
				$(this).val($(this).code());
			});
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					$('#px-esams-masterdata-shift-form .alert-warning').addClass('hidden');
					if(response.status == 'ok'){
						$('#px-esams-masterdata-shift-form .alert-success').removeClass('hidden').children('span').text(response.msg);
						window.location.href = response.redirect;
					}
					else
						$('#px-esams-masterdata-shift-form .alert-danger').removeClass('hidden').children('span').text(response.msg);	
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
	// Add by Alvin (atsalvin0017), 2019-09-12 13.30 PM
	let timepicker = new TimePicker(['px-esams-masterdata-shift-form-start-time','px-esams-masterdata-shift-form-end-time'], {
		lang: 'en',
	  	theme: 'dark'
	})
	timepicker.on('change', function(evt) {
	  	let value = (evt.hour || '00') + ':' + (evt.minute || '00')
	  	evt.element.value = value
	})
	// 
})