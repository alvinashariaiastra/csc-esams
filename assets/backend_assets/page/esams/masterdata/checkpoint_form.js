$(document).ready(function(){
	$('#px-esams-masterdata-checkpoint-form').validate({
		rules: {},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-esams-masterdata-checkpoint-form .alert-warning').removeClass('hidden');
			$('#px-esams-masterdata-checkpoint-form .alert-success').addClass('hidden');
			$('#px-esams-masterdata-checkpoint-form .alert-danger').addClass('hidden');
			$('.px-summernote').each(function() {
				$(this).val($(this).code());
			});
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					$('#px-esams-masterdata-checkpoint-form .alert-warning').addClass('hidden');
					if(response.status == 'ok'){
						$('#px-esams-masterdata-checkpoint-form .alert-success').removeClass('hidden').children('span').text(response.msg);
						window.location.href = response.redirect;
					}
					else
						$('#px-esams-masterdata-checkpoint-form .alert-danger').removeClass('hidden').children('span').text(response.msg);	
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
	// Select2 Lib
    $('.select2ins').select2()
})

// Get Building
function get_building_by_instalasi(select, instalasi_id) {
    $.ajax({  
        type    : 'get',  
        url     : base_url+'esams/masterdata/get_building_by_instalasi/'+instalasi_id,  
        cache   : false, 
        beforeSend: function() {
        },                  
        success: function(response) {
            // console.log(response)
            if (response.length > 0) {
            	$(select).append('<option value="" selected="" disabled=""></option>')
            	for (let i in response)
            		$(select).append('<option value="'+response[i].id+'">'+response[i].building_name+'</option>')
            }
        },
        error: function(response) {
            // console.log(response.responseJSON)
            if (typeof response.responseJSON === 'object') {
                alert('error something happened on our response api')
                return false
            }
            alert(response.responseJSON)
        },
        complete: function(response) {
        }
    })   
}

// Get Detail Building
function get_detail_building_by_building(select, building_id) {
    $.ajax({  
        type    : 'get',  
        url     : base_url+'esams/masterdata/get_detail_building_by_building/'+building_id,  
        cache   : false, 
        beforeSend: function() {
        },                  
        success: function(response) {
            // console.log(response)
            if (response.length > 0) {
            	$(select).append('<option value="" selected="" disabled=""></option>')
            	for (let i in response)
            		$(select).append('<option value="'+response[i].id+'">Floor '+response[i].floor+'</option>')
            }
        },
        error: function(response) {
            // console.log(response.responseJSON)
            if (typeof response.responseJSON === 'object') {
                alert('error something happened on our response api')
                return false
            }
            alert(response.responseJSON)
        },
        complete: function(response) {
        }
    })   
}

// Select Option Instalasi
$('#px-system-usergroup-form-instalasi_id').on('change', function() {
    let instalasi_id = $(this).val()
    if (instalasi_id=='')
    	return false
    $('#px-system-usergroup-form-building_id').empty()
    $('#px-system-usergroup-form-building_id').val('').trigger('change')
    $('#px-system-usergroup-form-building_detail_id').empty()
    $('#px-system-usergroup-form-building_detail_id').val('').trigger('change')
    get_building_by_instalasi('#px-system-usergroup-form-building_id', instalasi_id)
})

// Select Option Building
$('#px-system-usergroup-form-building_id').on('change', function() {
    let building_id = $(this).val()
    if (building_id=='')
    	return false
    $('#px-system-usergroup-form-building_detail_id').empty()
    $('#px-system-usergroup-form-building_detail_id').val('').trigger('change')
    get_detail_building_by_building('#px-system-usergroup-form-building_detail_id', building_id)
})