$(document).ready(function(){
	$('#px-esams-masterdata-detail_building-form').validate({
		rules: {},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-esams-masterdata-detail_building-form .alert-warning').removeClass('hidden');
			$('#px-esams-masterdata-detail_building-form .alert-success').addClass('hidden');
			$('#px-esams-masterdata-detail_building-form .alert-danger').addClass('hidden');
			$('.px-summernote').each(function() {
				$(this).val($(this).code());
			});
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					$('#px-esams-masterdata-detail_building-form .alert-warning').addClass('hidden');
					if(response.status == 'ok'){
						$('#px-esams-masterdata-detail_building-form .alert-success').removeClass('hidden').children('span').text(response.msg);
						window.location.href = response.redirect;
					}
					else
						$('#px-esams-masterdata-detail_building-form .alert-danger').removeClass('hidden').children('span').text(response.msg);	
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
	// Select2 Lib
    $('.select2ins').select2()
})

// Get Building
function get_building_by_instalasi(select, instalasi_id) {
    $.ajax({  
        type    : 'get',  
        url     : base_url+'esams/masterdata/get_building_by_instalasi/'+instalasi_id,  
        cache   : false, 
        beforeSend: function() {
        },                  
        success: function(response) {
            // console.log(response)
            if (response.length > 0) {
            	$(select).append('<option value="" selected="" disabled=""></option>')
            	for (let i in response)
            		$(select).append('<option value="'+response[i].id+'">'+response[i].building_name+'</option>')
            }
        },
        error: function(response) {
            // console.log(response.responseJSON)
            if (typeof response.responseJSON === 'object') {
                alert('error something happened on our response api')
                return false
            }
            alert(response.responseJSON)
        },
        complete: function(response) {
        }
    })   
}

// Select Option Instalasi
$('#px-system-usergroup-form-instalasi_id').on('change', function() {
    let instalasi_id = $(this).val()
    $('#px-system-usergroup-form-building_id').empty()
    $('#px-system-usergroup-form-building_id').val('').trigger('change')
    get_building_by_instalasi('#px-system-usergroup-form-building_id', instalasi_id)
})