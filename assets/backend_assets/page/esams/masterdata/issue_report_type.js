$(document).ready(function(){
	$('#px-esams-masterdata-issue_report_type-form').validate({
		ignore: [],
		rules: {                                            
			id: {
				required: true
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-esams-masterdata-issue_report_type-form .msg-status').text('Deleting data');
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					if(response.status == 'ok'){
						$('#px-esams-masterdata-issue_report_type-form .msg-status').text('Delete Success...');
						window.location.href = response.redirect;
					}
					else
						$('#px-esams-masterdata-issue_report_type-form .msg-status').text('Delete Failed');		
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
	$('body').delegate('.btn-delete','click',function(){
		var id = $(this).attr('data-target-id');
		$('#px-esams-masterdata-issue_report_type-form input[name="id"]').val('');
		$('#px-esams-masterdata-issue_report_type-form input[name="id"]').val(id);
		$('#px-esams-masterdata-issue_report_type-box').addClass('open');
	});
	$('#px-esams-masterdata-issue_report_type-modal').on('hidden.bs.modal', function (e) {
		$('#px-esams-masterdata-issue_report_type-form').validate().resetForm();
	});
	$('#px-esams-masterdata-issue_report_type-modal').on('shown.bs.modal', function (e) {
		$('#px-esams-masterdata-issue_report_type-form').validate().resetForm();
	});
})