$(document).ready(function(){
	$('#px-esams-masterdata-pattern-form').validate({
		rules: {},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-esams-masterdata-pattern-form .alert-warning').removeClass('hidden');
			$('#px-esams-masterdata-pattern-form .alert-success').addClass('hidden');
			$('#px-esams-masterdata-pattern-form .alert-danger').addClass('hidden');
			$('.px-summernote').each(function() {
				$(this).val($(this).code());
			});
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					$('#px-esams-masterdata-pattern-form .alert-warning').addClass('hidden');
					if(response.status == 'ok'){
						$('#px-esams-masterdata-pattern-form .alert-success').removeClass('hidden').children('span').text(response.msg);
						window.location.href = response.redirect;
					}
					else
						$('#px-esams-masterdata-pattern-form .alert-danger').removeClass('hidden').children('span').text(response.msg);	
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
	// Select2 Lib
    $('.select2ins').select2()
})