$(document).ready(function(){
    $('#px-esams-patrol-form').validate({
        rules: {},
        submitHandler: function(form) {
            let id = $('input[name="id"]').val()
            $('#span-dialog').text((id==''?'add':'edit'))
            $('#modal-confirm').modal('show')
            return false
        }
    });
    // Add Detail Pattern
    let iEventDetailPattern = 0
    $("#adddetailpattern").click(function() {
        set_tbody_patrol_pattern(iEventDetailPattern)
        iEventDetailPattern += 1

        //get checkpoint
        /*var checkpoint_id = $('#checkpoint_id').val();
        var get_id = $('#get_id').val();
        if (get_id != "") {
            checkpoint_id = get_id+","+checkpoint_id;
        }
        document.getElementById("get_id").value = checkpoint_id;*/
    })
    // Delete Detail Pattern Current
    $('.delcurrdetailpattern').click(function(ev) {
        if (ev.type == 'click') {
            let str_id = $('#str_id_delete_detailpattern').val()
            let row = $(this).attr('row')
            $('.current-detailpattern-'+row).fadeOut()
            $('.current-detailpattern-'+row).remove()
            $('#delete_detailpattern').modal('hide')
            $('#divdeletedetailpattern').append('<input type="hidden" name="patrol_pattern_delete[]" value="'+str_id+'">')
        }   
    })
    // Select2 lib
    $('.select2ins').select2()
    // Datepicker lib
    $('.datepicker').datepicker()
    // Get Detail pattern for edit
    let patrol_id = $('input[name="id"]').val()
    if (patrol_id!='')
        get_patrol_pattern(patrol_id)

    
})

function submitFormMain() {
    let form = '#px-esams-patrol-form'
    let id = $('input[name="id"]').val()
    var target = $(form).attr('action');
    $('#px-esams-patrol-form .alert-warning').removeClass('hidden');
    $('#px-esams-patrol-form .alert-success').addClass('hidden');
    $('#px-esams-patrol-form .alert-danger').addClass('hidden');
    $('.px-summernote').each(function() {
        $(this).val($(this).code());
    });
    $.ajax({
        url : target,
        type : 'POST',
        dataType : 'json',
        data : $(form).serialize(),
        beforeSend: function() {
            $('#modal-confirm').modal('hide')
        },
        success : function(response){
            $('#px-esams-patrol-form .alert-warning').addClass('hidden');
            if(response.status == 'ok') {
                $('#px-esams-patrol-form .alert-success').removeClass('hidden').children('span').text(response.msg);
                window.location.href = response.redirect;
            } else
                $('#px-esams-patrol-form .alert-danger').removeClass('hidden').children('span').text(response.msg);  
        },
        error : function(jqXHR, textStatus, errorThrown) {
            alert(textStatus, errorThrown);
        }
    });
}

function tbody_patrol_pattern(iEventDetailPattern, data=null) {
    let trnow = document.getElementById('tableadddetailpattern').getElementsByTagName('tr').length
    let disabled = ''
    let tab = $('input[name="tab"]').val()
    if (tab!=undefined) {
        if (tab=='inprogress') disabled = 'disabled="readonly"'
    }
    return `
        <tr class="records">
            <td>
                <input type="text" name="sequence[]" class="form-control m-input" autocomplete="off" _i="`+iEventDetailPattern+`" value="`+(data?data.sequence:trnow)+`" `+disabled+` style="color: #666666;">
            </td>
            <td>
                <select class="form-control m-input" name="checkpoint[]" id="select-detailpattern-level-`+iEventDetailPattern+`" _i="`+iEventDetailPattern+`" `+disabled+`>
                    <option value="" disabled="" selected=""></option>
                </select>
            </td>
            <td>
            `+(disabled==''?`
                <input type="hidden" name="patrol_pattern_id[]" value="`+(data?data.id:'')+`">
                <input type="hidden" name="patrol_pattern[]" value="`+iEventDetailPattern+`">
                <a class="btn btn-danger btn-sm deldetailpattern" href="javascript:;"><i class="fa fa-trash-o"></i> Delete</a>
            `:`
                <button class="btn btn-success btn-sm" type="button" onclick="formDialog('`+(data?data.id:'')+`')" `+(data?data.reason_manual_checkin==null?'':'disabled':'')+`><i class="fa fa-check"></i> Verify</button>
            `)+`
            </td>
            <td>
                `+(data?data.reason_manual_checkin==null?'':data.reason_manual_checkin:'')+`
            </td>
        </tr>
    `
}


function set_tbody_patrol_pattern(iEventDetailPattern, data=null) {
    $('#tbodyadddetailpattern').append(tbody_patrol_pattern(iEventDetailPattern, data))
    $('.deldetailpattern').click(function(ev){
        if (ev.type == 'click') {
            $(this).parents('.records').fadeOut()
            $(this).parents('.records').remove()
        }   
    })
    $('#select-detailpattern-level-'+iEventDetailPattern).select2({ placeholder : 'Select Checkpoint' })
    get_checkpoint('#select-detailpattern-level-'+iEventDetailPattern, (data?data.checkpoint_id:null))

    //get id checkpoint
    /*$('#select-detailpattern-level-'+iEventDetailPattern).on('change',function(){
        //alert($('#select-detailpattern-level-'+iEventDetailPattern).val())
        //var checkpoint_old = $('#checkpoint_id').val();
        var data = $('#select-detailpattern-level-'+iEventDetailPattern).val();
        //if (checkpoint_old != "") {
        //    data = checkpoint_old+","+data;
        //}
        document.getElementById("checkpoint_id").value = data;
        
    });*/
}

// Get Building
function get_building_by_instalasi(select, instalasi_id) {
    $.ajax({  
        type    : 'get',  
        url     : base_url+'esams/masterdata/get_building_by_instalasi/'+instalasi_id,  
        cache   : false, 
        beforeSend: function() {
        },                  
        success: function(response) {
            // console.log(response)
            if (response.length > 0) {
                $(select).append('<option value="" selected="" disabled=""></option>')
                for (let i in response)
                    $(select).append('<option value="'+response[i].id+'">'+response[i].building_name+'</option>')
            }
        },
        error: function(response) {
            // console.log(response.responseJSON)
            if (typeof response.responseJSON === 'object') {
                alert('error something happened on our response api')
                return false
            }
            alert(response.responseJSON)
        },
        complete: function(response) {
        }
    })   
}

// Select Option Instalasi
$('#px-esams-patrol-form-instalasi_id').on('change', function() {
    let instalasi_id = $(this).val()
    if (instalasi_id=='')
        return false
    $('#px-esams-patrol-form-building_id').empty()
    $('#px-esams-patrol-form-building_id').val('').trigger('change')
    get_building_by_instalasi('#px-esams-patrol-form-building_id', instalasi_id)
})

// Get Checkpoint
function get_checkpoint(select, selected=null) {
    let building_id = $('select[name="building_id"]').val()
    $.ajax({  
        type    : 'get',  
        // url     : base_url+'esams/masterdata/get_checkpoint',  
        url     : base_url+'esams/masterdata/get_checkpoint_by_building/'+building_id,  
        cache   : false, 
        beforeSend: function() {
        },                  
        success: function(response) {
            // console.log(response)
            if (response.length > 0) {
                for (let i in response){
                    /*var get_id = $('#get_id').val();
                    get_id = get_id.split(",");
                    checkpoint = get_id.indexOf(response[i].id)
                    if (checkpoint<0) {
                        $(select).append('<option value="'+response[i].id+'">'+response[i].checkpoint_name+'</option>')
                    }*/
                    $(select).append('<option value="'+response[i].id+'">'+response[i].checkpoint_name+'</option>')                    
                }  
                if (selected)
                    $(select).val(selected).trigger('change')
            }
        },
        error: function(response) {
            // console.log(response.responseJSON)
            if (typeof response.responseJSON === 'object') {
                alert('error something happened on our response api')
                return false
            }
            alert(response.responseJSON)
        },
        complete: function(response) {
        }
    })   
}

// Select Option Pattern
$('#px-esams-patrol-form-pattern_id').on('change', function() {
    let pattern_id = $(this).val()
    if (pattern_id=='')
        return false
    get_pattern_detail_by_pattern(pattern_id)
})

// Get Pattern Detail
function get_pattern_detail_by_pattern(pattern_id) {
    $.ajax({  
        type    : 'get',  
        url     : base_url+'esams/masterdata/get_pattern_detail_by_pattern/'+pattern_id,  
        cache   : false, 
        beforeSend: function() {
        },                  
        success: function(response) {
            // console.log(response)
            if (response.length > 0) {
                $('#tbodyadddetailpattern').empty()
                for (let i in response)
                    set_tbody_patrol_pattern(('0'+i), response[i])
            }
        },
        error: function(response) {
            // console.log(response.responseJSON)
            if (typeof response.responseJSON === 'object') {
                alert('error something happened on our response api')
                return false
            }
            alert(response.responseJSON)
        },
        complete: function(response) {
        }
    })   
}

// Get Detail Pattern
function get_patrol_pattern(patrol_id) {
    $.ajax({  
        type    : 'get',  
        url     : base_url+'esams/patrol/get_patrol_pattern/'+patrol_id,  
        cache   : false, 
        beforeSend: function() {
        },                  
        success: function(response) {
            // console.log(response)
            if (response.length > 0) {
                $('#tbodyadddetailpattern').empty()
                for (let i in response)
                    set_tbody_patrol_pattern(('0'+i), response[i])
            }
        },
        error: function(response) {
            // console.log(response.responseJSON)
            if (typeof response.responseJSON === 'object') {
                alert('error something happened on our response api')
                return false
            }
            alert(response.responseJSON)
        },
        complete: function(response) {
        }
    })   
}

function formDialog(patrol_pattern_id) {
    console.log(location)
    $('input[name="patrol_pattern_id"]').val(patrol_pattern_id)
    $('#modal-verify').modal('show')
}

function submitForm(url) {
    let patrol_pattern_id = $('input[name="patrol_pattern_id"]').val()
    let reason = $('textarea[name="reason"]').val()
    if (reason=='') {
        alert('Please complete reason')
        return false
    }
    $('#px-esams-patrol-form .alert-warning').removeClass('hidden')
    $('#px-esams-patrol-form .alert-success').addClass('hidden')
    $('#px-esams-patrol-form .alert-danger').addClass('hidden')
    $.ajax({
        url : url+'_verify',
        type : 'POST',
        dataType : 'json',
        data : {
            patrol_pattern_id: patrol_pattern_id,
            reason: reason
        },
        beforeSend : function() {
            $('#modal-verify').modal('hide')
        },
        success : function(response){
            $('#px-esams-patrol-form .alert-warning').addClass('hidden');
            if(response.status == 'ok'){
                $('#px-esams-patrol-form .alert-success').removeClass('hidden').children('span').text(response.msg);
                let patrol_id = $('input[name="id"]').val()
                if (patrol_id!='')
                    get_patrol_pattern(patrol_id)
            }
            else
                $('#px-esams-patrol-form .alert-danger').removeClass('hidden').children('span').text(response.msg);   
        },
        error : function(jqXHR, textStatus, errorThrown) {
            alert(textStatus, errorThrown);
        },
        complete : function() {
            document.getElementById('px-esams-patrol-form').scrollIntoView(false)
            $('#px-esams-patrol-form .alert-success').addClass('hidden');
            $('textarea[name="reason"]').val('')
            $('#modal-verify').modal('hide')
        }
    });
}