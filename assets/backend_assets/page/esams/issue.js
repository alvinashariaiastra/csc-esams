$(document).ready(function(){
    $('#px-esams-issue-form').validate({
        ignore: [],
        rules: {                                            
            id: {
                required: true
            }
        },
        submitHandler: function(form) {
            var target = $(form).attr('action');
            $('#px-esams-issue-form .msg-status').text('Deleting data');
            $.ajax({
                url : target,
                type : 'POST',
                dataType : 'json',
                data : $(form).serialize(),
                success : function(response){
                    if(response.status == 'ok'){
                        $('#px-esams-issue-form .msg-status').text('Delete Success...');
                        window.location.href = response.redirect;
                    }
                    else
                        $('#px-esams-issue-form .msg-status').text('Delete Failed');        
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    alert(textStatus, errorThrown);
                }
            });
        }
    });
    $('body').delegate('.btn-delete','click',function(){
        var id = $(this).attr('data-target-id');
        $('#px-esams-issue-form input[name="id"]').val('');
        $('#px-esams-issue-form input[name="id"]').val(id);
        $('#px-esams-issue-box').addClass('open');
    });
    $('#px-esams-issue-modal').on('hidden.bs.modal', function (e) {
        $('#px-esams-issue-form').validate().resetForm();
    });
    $('#px-esams-issue-modal').on('shown.bs.modal', function (e) {
        $('#px-esams-issue-form').validate().resetForm();
    });
})

function openTab(param) {
     location.href=param
}