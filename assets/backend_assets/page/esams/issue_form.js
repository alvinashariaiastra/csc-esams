$(document).ready(function(){
    $('#px-esams-issue-form').validate({
        rules: {
        },
        submitHandler: function(form) {
            var target = $(form).attr('action');
            $('#px-esams-issue-form .alert-warning').removeClass('hidden');
            $('#px-esams-issue-form .alert-success').addClass('hidden');
            $('#px-esams-issue-form .alert-danger').addClass('hidden');
            $('.px-summernote').each(function() {
                $(this).val($(this).code());
            });
            $.ajax({
                url : target,
                type : 'POST',
                dataType : 'json',
                data : $(form).serialize(),
                success : function(response){
                    $('#px-esams-issue-form .alert-warning').addClass('hidden');
                    if(response.status == 'ok') {
                        $('#px-esams-issue-form .alert-success').removeClass('hidden').children('span').text(response.msg);
                        window.location.href = response.redirect;
                    } else
                        $('#px-esams-issue-form .alert-danger').removeClass('hidden').children('span').text(response.msg);  
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    alert(textStatus, errorThrown);
                }
            });
        }
    });
    $('.px-summernote').summernote({
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline', 'clear']],
            ['fontname', ['fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['table', ['table']],
            ['insert', ['link', 'hr']],
            ['view', ['fullscreen', 'codeview']],
            ['help', ['help']]
        ],
        height: '200px',
        width: '700px',
        onImageUpload: function(files, editor, welEditable) {
            sendFile(files[0], editor, welEditable);
        }
    });

    let select_action = $('select[name="select_action"]').val()
    if (select_action == "dispatch") {
        $('#div-assign-personel').attr('hidden', false)
    }
    $('select[name="select_action"]').on('change', function(e) {
        $('#div-assign-personel').attr('hidden', true)
        let value = $(this).val()
        if (value=='dispatch') $('#div-assign-personel').attr('hidden', false)
        if (value=='escalate') $('#div-assign-personel').attr('hidden', true)
    })

    // Select2 lib
    $('.select2ins').select2()
    // Datepicker lib
    $('.datepicker').datepicker()
    $('.js-example-basic-multiple').select2()
    
})
$('#btn-close').on('click', function(){
    //alert('ok')
    $.ajax({
        url : $(this).attr('href'),
        type : 'GET',
        dataType : 'json',
        success : function(response){
            $('#px-esams-issue-form .alert-warning').addClass('hidden');
            if(response.status == 'ok') {
                $('#px-esams-issue-form .alert-success').removeClass('hidden').children('span').text(response.msg);
                window.location.href = response.redirect;
            } else
                $('#px-esams-issue-form .alert-danger').removeClass('hidden').children('span').text(response.msg);  
        },
        error : function(jqXHR, textStatus, errorThrown) {
            alert(textStatus, errorThrown);
        }
    });
    return false
})

function formDialog() {
    let issue_id = $('input[name="id"]').val()
    //alert(issue_id)
    $('input[name="issue_id"]').val(issue_id)
    $('#modal-verify').modal('show')
}

function submitForm(url) {
    let issue_id = $('input[name="issue_id"]').val()
    let reason = $('textarea[name="reason"]').val()
    if (reason=='') {
        alert('Please complete reason')
        return false
    }
    console.log(url)
    $('#px-esams-issue-form .alert-warning').removeClass('hidden')
    $('#px-esams-issue-form .alert-success').addClass('hidden')
    $('#px-esams-issue-form .alert-danger').addClass('hidden')
    $.ajax({
        /*url : location.href+'_close_ticket',*/
        url : url+'_close_ticket',
        type : 'POST',
        dataType : 'json',
        data : {
            issue_id: issue_id,
            reason: reason
        },
        beforeSend : function() {
            $('#modal-verify').modal('hide')
        },
        success : function(response){
            $('#px-esams-issue-form .alert-warning').addClass('hidden');
            if(response.status == 'ok') {
                $('#px-esams-issue-form .alert-success').removeClass('hidden').children('span').text(response.msg);
                window.location.href = response.redirect;
            } else
                $('#px-esams-issue-form .alert-danger').removeClass('hidden').children('span').text(response.msg);  
        },
        error : function(jqXHR, textStatus, errorThrown) {
            alert(textStatus, errorThrown);
        },
        complete : function() {
            document.getElementById('px-esams-issue-form').scrollIntoView(false)
            $('#px-esams-issue-form .alert-success').addClass('hidden');
            $('textarea[name="reason"]').val('')
            $('#modal-verify').modal('hide')
        }
    });
}
