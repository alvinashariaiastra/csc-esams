$(document).ready(function(){
  $('#px-esams-assignment-form').validate({
    ignore: [],
    rules: {                                            
      id: {
        required: true
      }
    },
    submitHandler: function(form) {
      var target = $(form).attr('action');
      $('#px-esams-assignment-form .msg-status').text('Deleting data');
      $.ajax({
        url : target,
        type : 'POST',
        dataType : 'json',
        data : $(form).serialize(),
        success : function(response){
          if(response.status == 'ok'){
            $('#px-esams-assignment-form .msg-status').text('Delete Success...');
            window.location.href = response.redirect;
          }
          else
            $('#px-esams-assignment-form .msg-status').text('Delete Failed');    
        },
        error : function(jqXHR, textStatus, errorThrown) {
          alert(textStatus, errorThrown);
        }
      });
    }
  });
  $('body').delegate('.btn-delete','click',function(){
    var id = $(this).attr('data-target-id');
    $('#px-esams-assignment-form input[name="id"]').val('');
    $('#px-esams-assignment-form input[name="id"]').val(id);
    $('#px-esams-assignment-box').addClass('open');
  });
  $('#px-esams-assignment-modal').on('hidden.bs.modal', function (e) {
    $('#px-esams-assignment-form').validate().resetForm();
  });
  $('#px-esams-assignment-modal').on('shown.bs.modal', function (e) {
    $('#px-esams-assignment-form').validate().resetForm();
  });


})
function openTab(param) {
     location.href=param
}
/*function openTab(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontentesams");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}*/

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();