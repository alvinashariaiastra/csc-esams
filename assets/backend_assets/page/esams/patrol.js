$(document).ready(function(){
    $('#px-esams-patrol-form').validate({
        ignore: [],
        rules: {                                            
            id: {
                required: true
            }
        },
        submitHandler: function(form) {
            var target = $(form).attr('action');
            $('#px-esams-patrol-form .msg-status').text('Deleting data');
            $.ajax({
                url : target,
                type : 'POST',
                dataType : 'json',
                data : $(form).serialize(),
                success : function(response){
                    if(response.status == 'ok'){
                        $('#px-esams-patrol-form .msg-status').text('Delete Success...');
                        window.location.href = response.redirect;
                    }
                    else
                        $('#px-esams-patrol-form .msg-status').text('Delete Failed');        
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    alert(textStatus, errorThrown);
                }
            });
        }
    });
    $('body').delegate('.btn-delete','click',function(){
        var id = $(this).attr('data-target-id');
        $('#px-esams-patrol-form input[name="id"]').val('');
        $('#px-esams-patrol-form input[name="id"]').val(id);
        $('#px-esams-patrol-box').addClass('open');
    });
    $('#px-esams-patrol-modal').on('hidden.bs.modal', function (e) {
        $('#px-esams-patrol-form').validate().resetForm();
    });
    $('#px-esams-patrol-modal').on('shown.bs.modal', function (e) {
        $('#px-esams-patrol-form').validate().resetForm();
    });
})

function openTab(param) {
     location.href=param
}