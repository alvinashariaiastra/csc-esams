$(document).ready(function(){
    $('#px-esams-assignment-form').validate({
        rules: {
            // image_name: {
            //     required: true
            // },
            assignment_title: {
                required: true
            },
            assignment_type: {
                required: true
            }
            // start_time_single: {
            //     required: true
            // },
            // end_time_single: {
            //     required: true
            // }
        },
        submitHandler: function(form) {
            let id = $('input[name="id"]').val()
            $('#span-dialog').text((id==''?'add':'edit'))
            $('#modal-confirm').modal('show')
            return false
            
        }
    });

    // Add by Alvin (atsalvin0017), 2019-09-12 13.30 PM
    $('input[name="assigned"]').on('change', function(e) {
        $('#div-assign-personel').attr('hidden', true)
        $('#div-assign-group').attr('hidden', true)
        let value = $(this).val()
        if (value=='Presonel') $('#div-assign-personel').attr('hidden', false)
        if (value=='Group') $('#div-assign-group').attr('hidden', false)
    })

    // Add by Idzni (agridzni6587), 2019-09-19 13.30 PM
    $('input[name="assignment_type"]').on('change', function(e) {
        $('#div_task').attr('hidden', false)
        $('#div-assign-subtask').attr('hidden', true)
        $('#div-assign-title').attr('hidden', true)
        $('#div-assign-subtask-data').attr('hidden', true)
        $('#div-assign-image-single').attr('hidden', false)
        let value = $(this).val()
        if (value=='series') {
            $('.div-task-series').css('display', 'none')
            $('#div-assign-subtask').attr('hidden', false)
            $('#div-assign-title').attr('hidden', false)
            $('#div-assign-subtask-data').attr('hidden', false)
            $('#div-assign-image-single').attr('hidden', true)
        } else
        if (value=='single') {
            $('.div-task-series').css('display', '')
        }
    })

    // Add by Alvin (atsalvin0017), 2019-09-12 13.30 PM
    $('.px-summernote').summernote({
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline', 'clear']],
            ['fontname', ['fontname']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['table', ['table']],
            ['insert', ['link', 'hr']],
            ['view', ['fullscreen', 'codeview']],
            ['help', ['help']]
        ],
        height: '200px',
        width: '700px',
        onImageUpload: function(files, editor, welEditable) {
            sendFile(files[0], editor, welEditable);
        }
    });
    $('.datetimepicker').datetimepicker()
    $('.js-example-basic-multiple').select2()
})

function submitForm() {
    let form = '#px-esams-assignment-form'
    let id = $('input[name="id"]').val()
    var target = $(form).attr('action');
    $('#px-esams-assignment-form .alert-warning').removeClass('hidden');
    $('#px-esams-assignment-form .alert-success').addClass('hidden');
    $('#px-esams-assignment-form .alert-danger').addClass('hidden');
    $('.px-summernote').each(function() {
        $(this).val($(this).code());
        //console.log($(this).code(), "summernote");
    });
    $('.px-summernotes').each(function() {
        $(this).val($(this).code());
        //console.log($(this).code(), "summernotes");
    });
    // add by Alvin (atsalvin0017), Date: 2019-09-18 11:40 AM
    // ::start for add post file
    let formData = new FormData();
    let type_val = $('input[name="assignment_type"]:checked').val()
    if (type_val=='single') {
        let end_time = $('input[name="end_time_single"]').val()
        if (end_time=='') {
            alert('please complete form, end time field')
            $('#px-esams-assignment-form .alert-warning').addClass('hidden');
            $('#modal-confirm').modal('hide')
            return false
        }
        let location = $('input[name="location_single"]').val()
        if (location=='') {
            alert('please complete form, location field')
            $('#px-esams-assignment-form .alert-warning').addClass('hidden');
            $('#modal-confirm').modal('hide')
            return false
        }
        let radius = $('input[name="radius_single"]').val()
        if (radius=='') {
            alert('please complete form, radius field')
            $('#px-esams-assignment-form .alert-warning').addClass('hidden');
            $('#modal-confirm').modal('hide')
            return false
        }
        let formImage = $('input[name="image_name"]').prop('files')
        if (formImage.length==0 && id=='') {
            alert('at least 1 image must be uploaded')
            $('#px-esams-assignment-form .alert-warning').addClass('hidden');
            $('#modal-confirm').modal('hide')
            return false
        }
        for (let i in formImage)
            formData.append('image_name[]', formImage[i]);
    } else
    if (type_val=='series') {
        let task = $('input[name="task_series[]"]')
        if (task.length==0) {
            alert('please add sub task')
            $('#px-esams-assignment-form .alert-warning').addClass('hidden');
            $('#modal-confirm').modal('hide')
            return false
        }
        for (let i = 0; i < task.length; i++) {
            let task = $('#task_title_'+i).val()
            if (task=='') {
                alert('please complete form, task title field for each form')
                $('#px-esams-assignment-form .alert-warning').addClass('hidden');
                $('#modal-confirm').modal('hide')
                return false
            }
            let start_time = $('#start_time_'+i).val()
            if (start_time=='') {
                alert('please complete form, start time field for each form')
                $('#px-esams-assignment-form .alert-warning').addClass('hidden');
                $('#modal-confirm').modal('hide')
                return false
            }
            let end_time = $('#end_time_'+i).val()
            if (end_time=='') {
                alert('please complete form, end time field for each form')
                $('#px-esams-assignment-form .alert-warning').addClass('hidden');
                $('#modal-confirm').modal('hide')
                return false
            }
            let location = $('#location_'+i).val()
            if (location=='') {
                alert('please complete form, location field for each form')
                $('#px-esams-assignment-form .alert-warning').addClass('hidden');
                $('#modal-confirm').modal('hide')
                return false
            }
            let radius = $('#radius_'+i).val()
            if (radius=='') {
                alert('please complete form, radius field for each form')
                $('#px-esams-assignment-form .alert-warning').addClass('hidden');
                $('#modal-confirm').modal('hide')
                return false
            }
            let formImage = $('input[name="image_name_'+i+'"]').prop('files')
            if (formImage.length==0 && id=='') {
                alert('at least 1 image must be uploaded for each form')
                $('#px-esams-assignment-form .alert-warning').addClass('hidden');
                $('#modal-confirm').modal('hide')
                return false
            }
            for (let j in formImage)
                formData.append('image_name_'+i+'[]', formImage[j]);
        }
    }
    let formSerialize = $(form).serializeArray();
    for (let i in formSerialize)
        formData.append(formSerialize[i].name, formSerialize[i].value);
    // ::end
    $.ajax({
        url : target,
        type : 'POST',
        // dataType : 'json',
        // data : $(form).serialize(),
        data : formData,
        contentType: false,
        cache: false,
        processData:false,
        beforeSend: function() {
            $('#modal-confirm').modal('hide')
        },    
        success : function(response){
            response = JSON.parse(response)
            $('#px-esams-assignment-form .alert-warning').addClass('hidden');
            if(response.status == 'ok'){
                $('#px-esams-assignment-form .alert-success').removeClass('hidden').children('span').text(response.msg);
                window.location.href = response.redirect;
            }
            else
                $('#px-esams-assignment-form .alert-danger').removeClass('hidden').children('span').text(response.msg); 
        },
        error : function(jqXHR, textStatus, errorThrown) {
            alert(textStatus, errorThrown);
        }
    });
}

function initialize() {
    var current_latitude = $('#latitude').val()
    if (current_latitude=='') current_latitude = '-2.2744545'
    var current_longitude = $('#longitude').val()
    if (current_longitude=='') current_longitude = '99.3550328'
    var latlng = new google.maps.LatLng(parseFloat(current_latitude),parseFloat(current_longitude))
    // var map = new google.maps.Map(document.getElementById('map'), {
    var class_name = document.getElementsByClassName('map')[0]
    // var class_name = $('.map')
    var map = new google.maps.Map(class_name, {
        center: latlng,
        zoom: 13
    })
    var marker = new google.maps.Marker({
        map: map,
        position: latlng,
        draggable: true,
        anchorPoint: new google.maps.Point(0, -29)
    })
    // var input = document.getElementById('searchInput')
    var input = document.getElementsByClassName('searchInput')[0]
    // var input = $('.searchInput')
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input)
    var geocoder = new google.maps.Geocoder()
    var autocomplete = new google.maps.places.Autocomplete(input)
        autocomplete.bindTo('bounds', map)
    var infowindow = new google.maps.InfoWindow()   
        autocomplete.addListener('place_changed', function() {
            infowindow.close()
            marker.setVisible(false)
            var place = autocomplete.getPlace()
            if (!place.geometry) {
                window.alert("Autocomplete's returned place contains no geometry")
                return
            }
            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport)
            } else {
                map.setCenter(place.geometry.location)
                map.setZoom(17)
            }
        
            marker.setPosition(place.geometry.location)
            marker.setVisible(true)          
        
            bindDataToForm(place.formatted_address,place.geometry.location.lat(),place.geometry.location.lng())
            infowindow.setContent(place.formatted_address)
            infowindow.open(map, marker)    
        })
    // this function will work on marker move event into map 
    google.maps.event.addListener(marker, 'dragend', function() {
        geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {        
                    bindDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng())
                    infowindow.setContent(results[0].formatted_address)
                    infowindow.open(map, marker)
                }
            }
        })
    })
}

function bindDataToForm(location,latitude,longitude){
    $('#location').val(location)
    $('#latitude').val(latitude)
    $('#longitude').val(longitude)
}
google.maps.event.addDomListener(window, 'load', initialize)

let task = $('input[name="task_series[]"]').length
let i = 0;
    if (task>0) i = task
$('#save-task').on('click', function() {
    $('.div-task-series').css('display', 'none')
    let divdata = `
        <div class="records" style="padding: 15px; border-left: 2px solid #ccc; border-top: 1px solid #ccc;">
            <h4>Task `+(i+1)+`</h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Task Title <sup class="mandatory">*</sup></label>
                        <div class="col-md-8 col-xs-12">
                            <input type="text" class="form-control" name="task_title_series[]" id="task_title_`+i+`">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Start Time <sup class="mandatory">*</sup></label>
                        <div class="col-md-8 col-xs-12">
                            <input type="text" class="form-control datetimepickers" style="padding: 5px 12px;" name="start_time_series[]" id="start_time_`+i+`">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">End Time <sup class="mandatory">*</sup></label>
                        <div class="col-md-8 col-xs-12">
                            <input type="text" class="form-control datetimepickers" style="padding: 5px 12px;" name="end_time_series[]" id="end_time_`+i+`">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Description <sup class="mandatory">*</sup></label>
                        <div class="col-md-8 col-xs-12">
                            <textarea class="form-control px-summernotes" name="description_series[]"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Map</label>
                        <div class="col-md-6 col-xs-12">
                            <button class="btn btn-success" type="button" onclick="formMap('modal', '`+i+`')">Set Location Map</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Location <sup class="mandatory">*</sup></label>
                        <div class="col-md-8 col-xs-12">
                            <input type="text" class="form-control" name="location_series[]" id="location_`+i+`" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Latitude <sup class="mandatory">*</sup></label>
                        <div class="col-md-8 col-xs-12">
                            <input type="text" class="form-control" name="latitude_series[]" id="latitude_`+i+`" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Longitude <sup class="mandatory">*</sup></label>
                        <div class="col-md-8 col-xs-12">
                            <input type="text" class="form-control" name="longitude_series[]" id="longitude_`+i+`" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Radius <sup class="mandatory">*</sup></label>
                        <div class="col-md-2 col-xs-12">
                            <input type="text" class="form-control" name="radius_series[]" id="radius_`+i+`"> Meter
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label">Image <sup class="mandatory">*</sup></label>
                        <div class="col-md-10 col-xs-12">
                            <input type="file" name="image_name_`+i+`" multiple="multiple">
                        </div>
                    </div>
                    <hr/>
                    <div class="form-group">
                        <label class="col-md-2 col-xs-12 control-label"></label>
                        <div class="col-md-10 col-xs-12">
                            <input type="hidden" name="task_series[]" value="`+i+`">
                            <input type="hidden" name="detailid_series[]">
                            <a class="btn btn-danger btn-sm delsubtask" href="javascript:;">Delete</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $('.px-summernotes').summernote({
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'italic', 'underline', 'clear']],
                    ['fontname', ['fontname']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['table', ['table']],
                    ['insert', ['link', 'hr']],
                    ['view', ['fullscreen', 'codeview']],
                    ['help', ['help']]
                ],
                height: '200px',
                width: '700px',
                onImageUpload: function(files, editor, welEditable) {
                    sendFile(files[0], editor, welEditable);
                }
            });
            $('.datetimepickers').datetimepicker()
        </script>
    `
    $('#div-data-series').append(divdata)
    $('.delsubtask').click(function(ev){
        if (ev.type == 'click') {
            $(this).parents('.records').fadeOut()
            $(this).parents('.records').remove()
        }   
        i --
    })
    i ++
})

function formMap(dialog, param=null) {
    if (dialog=='modal') {
        $('.searchInput').val('')
        $('#param_loc').val(param)
        $('#location').val('')
        $('#latitude').val('')
        $('#longitude').val('')
        $('#maps').attr('hidden', false)
    }
    if (dialog=='set') {
        let param_loc = $('#param_loc').val()
        let location = $('#location').val()
        $('#location_'+param_loc).val(location)
        let latitude = $('#latitude').val()
        $('#latitude_'+param_loc).val(latitude)
        let longitude = $('#longitude').val()
        $('#longitude_'+param_loc).val(longitude)
        $('#maps').attr('hidden', true)
    }
    if (dialog=='cancel') {
        $('#maps').attr('hidden', true)
    }
}
