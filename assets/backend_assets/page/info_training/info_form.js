$(document).ready(function(){
	$('#px-info-training-form').validate({
		rules: {                                            
			title: {
				required: true
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-info-training-form .alert-warning').removeClass('hidden');
			$('#px-info-training-form .alert-success').addClass('hidden');
			$('#px-info-training-form .alert-danger').addClass('hidden');
			$('.px-summernote').each(function() {
				$(this).val($(this).code());
			});
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					$('#px-info-training-form .alert-warning').addClass('hidden');
					if(response.status == 'ok'){
						$('#px-info-training-form .alert-success').removeClass('hidden').children('span').text(response.msg);
						window.location.href = response.redirect;
					}
					else
						$('#px-info-training-form .alert-danger').removeClass('hidden').children('span').text(response.msg);	
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
	$('#px-info-training-form-berita-content').summernote({
		toolbar: [
			['style', ['style']],
			['font', ['bold', 'italic', 'underline', 'clear']],
			['fontname', ['fontname']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']],
			['table', ['table']],
			['insert', ['link', 'picture', 'hr']],
			['view', ['fullscreen', 'codeview']],
			['help', ['help']]
		],
		height: '300px',
		onImageUpload: function(files, editor, welEditable) {
			sendFile(files[0], editor, welEditable);
		}
	});
	function sendFile(file, editor, welEditable) {
		data = new FormData();
		data.append('image', file);
		$.ajax({
			data: data,
			type: 'post',
			url: 'upload/image',
			cache: false,
			contentType: false,
			processData: false,
			success: function(url) {
				editor.insertImage(welEditable, url);
				$('#px-info-training-form .panel-body').after('<input type="hidden" name="images[]" value="'+url+'">');
			}
		});
	}                        
})