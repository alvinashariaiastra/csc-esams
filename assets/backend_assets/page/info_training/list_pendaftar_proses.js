$(document).ready(function(){
    $('body').delegate('.btn-processing','click',function(){
            var id = $(this).attr('data-target-id');
            var process = $(this).attr('data-process');
            if(process == 2)
            {
                $('#warning-message').html('Apakah kamu yakin ingin Menolak Pendaftar ini?');
                $('#button-message').addClass('btn-danger');
                $('.msg-status').html('Data yang sudah ditolak tidak dapat dikembalikan');
                $('#button-message').html('Reject');
            }
            else
            {
                $('#warning-message').html('Apakah kamu yakin ingin Menyetujui Pendaftar ini?');
                $('#button-message').addClass('btn-primary');
                $('.msg-status').html('Pastikan seluruh data yang diinput sudah benar. Data yang telah diinput tidak dapat diedit kembali');
                $('#button-message').html('Approve');
            }
            var name = $('#px-pendaftaran-training-form-name').val();
            var company = $('#px-pendaftaran-training-form-company').val();
            var position = $('#px-pendaftaran-training-form-position').val();
            var phone = $('#px-pendaftaran-training-form-phone').val();
            var email = $('#px-pendaftaran-training-form-email').val();
            var supervisor_name = $('#px-pendaftaran-training-form-supervisor_name').val();
            $('#px-info-training-message-box').addClass('open');
            $('#px-info-training-message-form input[name="id"]').val('');
            $('#px-info-training-message-form input[name="id"]').val(id);
            $('#px-info-training-message-form input[name="name"]').val('');
            $('#px-info-training-message-form input[name="name"]').val(name);
            $('#px-info-training-message-form input[name="company"]').val('');
            $('#px-info-training-message-form input[name="company"]').val(company);
            $('#px-info-training-message-form input[name="position"]').val('');
            $('#px-info-training-message-form input[name="position"]').val(position);
            $('#px-info-training-message-form input[name="phone"]').val('');
            $('#px-info-training-message-form input[name="phone"]').val(phone);
            $('#px-info-training-message-form input[name="email"]').val('');
            $('#px-info-training-message-form input[name="email"]').val(email);
            $('#px-info-training-message-form input[name="supervisor_name"]').val('');
            $('#px-info-training-message-form input[name="supervisor_name"]').val(supervisor_name);
            $('#px-info-training-message-form input[name="status"]').val('');
            $('#px-info-training-message-form input[name="status"]').val(process);
    });
    
    $('#px-info-training-message-form').validate({
            ignore: [],
            rules: {                                            
                    id: {
                            required: true
                    }
            },
            submitHandler: function(form) {
                    var target = $(form).attr('action');
                    $('#px-info-training-message-form .msg-status').text('Updating data');
                    $.ajax({
                            url : target,
                            type : 'POST',
                            dataType : 'json',
                            data : $(form).serialize(),
                            success : function(response){
                                    if(response.status == 'ok'){
                                            $('#px-info-training-message-form .msg-status').text('Update Success...');
                                            window.location.href = response.redirect;
                                    }
                                    else
                                            $('#px-info-training-message-form .msg-status').text('Update Failed');
                            },
                            error : function(jqXHR, textStatus, errorThrown) {
                                    alert(textStatus, errorThrown);
                            }
                    });
            }
    });
});