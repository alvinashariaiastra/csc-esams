//datatables function
var save_method; //for save method string
var table;

table = $('.table').DataTable({
  
  "processing": true, //Feature control the processing indicator.
  "serverSide": true, //Feature control DataTables' server-side processing mode.
  
  // Load data for the table's content from an Ajax source
  "ajax": {
      "url": "admin_info_training/ajax_pendaftaran_training_list",
      "type": "POST"
  },

  //Set column definition initialisation properties.
  "columnDefs": [
  { 
    "targets": [0, -1 ], //last column
    "orderable": false, //set not orderable
  },
  ]

});

function reload_table()
{
  table.ajax.reload(null,false); //reload datatable ajax 
}