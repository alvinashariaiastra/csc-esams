//datatables function
var save_method; //for save method string
var table;

table = $('.table').DataTable({
  
  "processing": true, //Feature control the processing indicator.
  "serverSide": true, //Feature control DataTables' server-side processing mode.
  "order": [[ 1, "asc" ]],
  
  // Load data for the table's content from an Ajax source
  "ajax": {
      "url": "admin_info_training/ajax_info_list",
      "type": "POST"
  },

  //Set column definition initialisation properties.
  "columnDefs": [
  { 
    "targets": [0, -1 ], //last column
    "orderable": false, //set not orderable
  },
  ]

});

function reload_table()
{
  table.ajax.reload(null,false); //reload datatable ajax 
}
$(document).ready(function(){
	$('#px-info-training-message-form').validate({
		ignore: [],
		rules: {                                            
			id: {
				required: true
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-info-training-message-form .msg-status').text('Deleting data');
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					if(response.status == 'ok'){
						$('#px-info-training-message-form .msg-status').text('Delete Success...');
						window.location.href = response.redirect;
					}
					else
						$('#px-info-training-message-form .msg-status').text('Delete Failed');
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
	$('body').delegate('.btn-delete','click',function(){
		$('#px-info-training-message-box').addClass('open');
		var id = $(this).attr('data-target-id');
		$('#px-info-training-message-form input[name="id"]').val('');
		$('#px-info-training-message-form input[name="id"]').val(id);
	});
	var fullCalendarInfoTraining = function(){
            
        var calendar = function(){
            
            if($("#calendar-info-training").length > 0){
                
                var date = new Date();
                var d = date.getDate();
                var m = date.getMonth();
                var y = date.getFullYear();

                var calendar = $('#calendar-info-training').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay'
                    },
                    editable: true,
                    eventSources: {url: "admin_info_training/get_info_training"},
                    droppable: false,
                    selectable: false,
                    selectHelper: false,
                    select: function(start, end, allDay) {
                        var title = prompt('Event Title:');
                        if (title) {
                            calendar.fullCalendar('renderEvent',
                            {
                                title: title,
                                start: start,
                                end: end,
                                allDay: allDay
                            },
                            true
                            );
                        }
                        calendar.fullCalendar('unselect');
                    },
                    drop: function(date, allDay) {

                        var originalEventObject = $(this).data('eventObject');

                        var copiedEventObject = $.extend({}, originalEventObject);

                        copiedEventObject.start = date;
                        copiedEventObject.allDay = allDay;

                        $('#calendar-info-training').fullCalendar('renderEvent', copiedEventObject, true);


                        if ($('#drop-remove').is(':checked')) {
                            $(this).remove();
                        }

                    },
                    eventClick: function(calEvent, jsEvent, view) {
                        var id = calEvent.id;
                        
                        if (calEvent.className == 'blue') {
                            $.ajax({
                                url : 'admin_info_training/check_calendar_event',
                                type : 'POST',
                                dataType : 'json',
                                data : {id: calEvent.id},
                                success : function(response){
                                    if(response.status == 'ok'){
                                        $.ajax({
                                            url : 'admin_info_training/get_training_detail_calendar',
                                            type : 'POST',
                                            dataType : 'json',
                                            data : { training_info_id: id },
                                            success : function(data){
                                              $('.modal-title').html(data.title);
                                              $('.modal-body').html(data.content);
                                              $('.btn-link').attr('href','admin_info_training/pendaftaran_training_form/'+id);
                                            },
                                            error : function(jqXHR, textStatus, errorThrown) {
                                              console.log(textStatus, errorThrown);
                                            }
                                        });
                                    $('#detail-training-modal').modal('show');
                                    }else{
                                        $.ajax({
                                            url : 'admin_info_training/get_training_detail_calendar',
                                            type : 'POST',
                                            dataType : 'json',
                                            data : { training_info_id: id },
                                            success : function(data){
                                              $('.modal-title').html(data.title);
                                              $('.modal-body').html(data.content);
                                              $('.btn-link').attr('style','display:none;');
                                            },
                                            error : function(jqXHR, textStatus, errorThrown) {
                                              console.log(textStatus, errorThrown);
                                            }
                                        });
                                        $('#detail-training-modal').modal('show');
                                    }
                                },
                                error : function(jqXHR, textStatus, errorThrown) {
                                    alert(textStatus, errorThrown);
                                }
                            });
                        };
                        /*if (calEvent.className == 'blue') {
                            $.ajax({
                                url : 'admin_info_training/check_calendar_event',
                                type : 'POST',
                                dataType : 'json',
                                data : {id: calEvent.id},
                                success : function(response){
                                    if(response.status == 'ok'){
                                        window.location.href = response.redirect;
                                    }
                                },
                                error : function(jqXHR, textStatus, errorThrown) {
                                    alert(textStatus, errorThrown);
                                }
                            });
                        };*/
                    }
                });
                
            }            
        }
        
        return {
            init: function(){
                calendar();
            }
        }
    }();
    fullCalendarInfoTraining.init();
})