$(document).ready(function(){
	$('#px-company-assessment2015-form').validate({
		rules: {                                            
			instalasi_id: {
				required: true
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-company-assessment2015-form .alert-warning').removeClass('hidden');
			$('#px-company-assessment2015-form .alert-success').addClass('hidden');
			$('#px-company-assessment2015-form .alert-danger').addClass('hidden');
			$('.px-summernote').each(function() {
				$(this).val($(this).code());
			});
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					$('#px-company-assessment2015-form .alert-warning').addClass('hidden');
					if(response.status == 'ok'){
						$('#px-company-assessment2015-form .alert-success').removeClass('hidden').children('span').text(response.msg);
						window.location.href = response.redirect;
					}
					else
						$('#px-company-assessment2015-form .alert-danger').removeClass('hidden').children('span').text(response.msg);	
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
	$('#px-company-assessment2015-form-assessment2015-content').summernote({
		toolbar: [
			['style', ['style']],
			['font', ['bold', 'italic', 'underline', 'clear']],
			['fontname', ['fontname']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']],
			['table', ['table']],
			['insert', ['link', 'picture', 'hr']],
			['view', ['fullscreen', 'codeview']],
			['help', ['help']]
		],
		height: '300px',
		onImageUpload: function(files, editor, welEditable) {
            sendFile(files[0], editor, welEditable);
        }
	});
   	$('#px-company-assessment2015-form-assessment2015-rules1').rules('add', {
		number : true,
		required : true
	});
	$('#px-company-assessment2015-form-assessment2015-rules2').rules('add', {
		number : true,
		required : true
	});
	$('#px-company-assessment2015-form-assessment2015-rules3').rules('add', {
		number : true,
		required : true
	});
	$('#px-company-assessment2015-form-assessment2015-rules4').rules('add', {
		number : true,
		required : true
	});
	$('#px-company-assessment2015-form-assessment2015-security_performance').rules('add', {
		number : true,
		required : true
	});
	$('#px-company-assessment2015-form-assessment2015-people').rules('add', {
		number : true,
		required : true
	});
	$('#px-company-assessment2015-form-assessment2015-device_and_infrastructure').rules('add', {
		number : true,
		required : true
	});
	$('#px-company-assessment2015-form-assessment2015-csi').rules('add', {
		number : true,
		required : true
	});

	// perhitungan nilai pada form
	// nilai pemenuhan system (rata rata element 1-4)
	hitung_rules();
	$('.rules-element').keyup(function(){
		hitung_rules();
		hitung_nilai_akhir();
    	cek_warna(nilai_akhir);
	});
	// nilai security reliability performance
	hitung_security_reliability();
	$('.rules-security-reliability').keyup(function(){
		hitung_security_reliability();
		hitung_nilai_akhir();
    	cek_warna(nilai_akhir);
	});
	// nilai akhir (nilai terendah dari pemenuhan system, security performance,security reliability dan csi)
	hitung_nilai_akhir();
    $('.rules-last-value').change(function(){
    	hitung_nilai_akhir();
    	cek_warna(nilai_akhir);
    });
    $('.rules-last-value').keyup(function(){
    	hitung_nilai_akhir();
    	cek_warna(nilai_akhir);
    });
    function hitung_rules(){
    	var element_1 = parseFloat($('#px-company-assessment2015-form-assessment2015-rules1').val());
		var element_2 = parseFloat($('#px-company-assessment2015-form-assessment2015-rules2').val());
		var element_3 = parseFloat($('#px-company-assessment2015-form-assessment2015-rules3').val());
		var element_4 = parseFloat($('#px-company-assessment2015-form-assessment2015-rules4').val());
		if(!element_1)
			element_1 = 0;
		if(!element_2)
			element_2 = 0;
		if(!element_3)
			element_3 = 0;
		if(!element_4)
			element_4 = 0;
		var pemenuhan_system = (parseFloat(element_1) + parseFloat(element_2) + parseFloat(element_3) + parseFloat(element_4)) / 4;
		$('#px-company-assessment2015-form-assessment2015-pemenuhan_system').val(pemenuhan_system.toFixed(2));
    }
    function hitung_security_reliability(){
    	var security_reliablity_people = parseFloat($('#px-company-assessment2015-form-assessment2015-people').val());
		var security_reliablity_device_and_infrastructure = parseFloat($('#px-company-assessment2015-form-assessment2015-device_and_infrastructure').val());
		if(!security_reliablity_people)
			security_reliablity_people = 0;
		if(!security_reliablity_device_and_infrastructure)
			security_reliablity_device_and_infrastructure = 0;
		var security_reliability = (parseFloat(security_reliablity_people) + parseFloat(security_reliablity_device_and_infrastructure)) / 2;
		$('#px-company-assessment2015-form-assessment2015-security_reliability').val(security_reliability.toFixed(2));
    }
    function hitung_nilai_akhir(){
    	var last_values = [];
    	$('.rules-last-value').each(function(){
    		var value = parseFloat($(this).val());
    		if(!value)
    			last_values.push(0);
    		else
    			last_values.push(value);
    	});
    	if(last_values.length > 0)
			nilai_akhir = Math.min.apply( null, last_values );
		else
			nilai_akhir = 0;
    	$('#px-company-assessment2015-form-assessment2015-nilai_akhir').val(nilai_akhir.toFixed(2));
    }
    function cek_warna(nilai){
    	$.ajax({
    		url : 'admin_company/assessment_2015_check_colour',
    		type : 'POST',
    		dataType : 'json',
    		data : {'nilai' : nilai},
    		success : function(response){	
    			if(response.warna != 'putih')
    				$('#nilai-akhir-colour').html(response.warna);
    			else
    				$('#nilai-akhir-colour').html(' ');
    		}
    	});
    }
    $('#px-company-assessment2015-form-assessment2015-critical_point').summernote({
		toolbar: [
			['style', ['style']],
			['font', ['bold', 'italic', 'underline', 'clear']],
			['fontname', ['fontname']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']],
			['table', ['table']],
			['insert', ['link', 'hr']],
			['view', ['fullscreen', 'codeview']],
			['help', ['help']]
		],
		height: '300px',
		onImageUpload: function(files, editor, welEditable) {
            sendFile(files[0], editor, welEditable);
        }
	});
    function sendFile(file, editor, welEditable) {
        data = new FormData();
        data.append('image', file);
        $.ajax({
            data: data,
            type: 'post',
            url: 'upload/image',
            cache: false,
            contentType: false,
            processData: false,
            success: function(url) {
                editor.insertImage(welEditable, url);
                $('#px-company-assessment2015-form .panel-body').after('<input type="hidden" name="images[]" value="'+url+'">');
            }
        });
    }
    $('.select2ins').select2();
})