//datatables function
var save_method; //for save method string
var table;

table = $('.table').DataTable({
  
  "processing": true, //Feature control the processing indicator.
  "serverSide": true, //Feature control DataTables' server-side processing mode.
  "order": [[ 1, "asc" ]],
  // Load data for the table's content from an Ajax source
  "ajax": {
      "url": "admin_company/ajax_company_list_list",
      "type": "POST"
  },

  //Set column definition initialisation properties.
  "columnDefs": [
  { 
    "targets": [0,3, -1 ], //last column
    "orderable": false, //set not orderable
  },
  ]

});

function reload_table()
{
  table.ajax.reload(null,false); //reload datatable ajax 
}

$(document).ready(function(){
  $('#px-company-companylist-message-form').validate({
    ignore: [],
    rules: {                                            
      id: {
        required: true
      }
    },
    submitHandler: function(form) {
      var target = $(form).attr('action');
      $('#px-company-companylist-message-form .msg-status').text('Deleting data');
      $.ajax({
        url : target,
        type : 'POST',
        dataType : 'json',
        data : $(form).serialize(),
        success : function(response){
          if(response.status == 'ok'){
            $('#px-company-companylist-message-form .msg-status').text('Delete Success...');
            window.location.href = response.redirect;
          }
          else
            $('#px-company-companylist-message-form .msg-status').text('Delete Failed');  
        },
        error : function(jqXHR, textStatus, errorThrown) {
          alert(textStatus, errorThrown);
        }
      });
    }
  });
  $('body').delegate('.btn-delete','click',function(){
    $('#px-company-companylist-message-box').addClass('open');
    var id = $(this).attr('data-target-id');
    $('#px-company-companylist-message-form input[name="id"]').val('');
    $('#px-company-companylist-message-form input[name="id"]').val(id);
  });
})