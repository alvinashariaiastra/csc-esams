$(document).ready(function(){
	$('#px-company-instalasi-form').validate({
		rules: {                                            
			
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-company-instalasi-form .alert-warning').removeClass('hidden');
			$('#px-company-instalasi-form .alert-success').addClass('hidden');
			$('#px-company-instalasi-form .alert-danger').addClass('hidden');
			$('.px-summernote').each(function() {
				$(this).val($(this).code());
			});
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					$('#px-company-instalasi-form .alert-warning').addClass('hidden');
					if(response.status == 'ok'){
						$('#px-company-instalasi-form .alert-success').removeClass('hidden').children('span').text(response.msg);
						window.location.href = response.redirect;
					}
					else
						$('#px-company-instalasi-form .alert-danger').removeClass('hidden').children('span').text(response.msg);	
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
	$('#px-company-instalasi-form-instalasi-content').summernote({
		toolbar: [
			['style', ['style']],
			['font', ['bold', 'italic', 'underline', 'clear']],
			['fontname', ['fontname']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']],
			['table', ['table']],
			['insert', ['link', 'picture', 'hr']],
			['view', ['fullscreen', 'codeview']],
			['help', ['help']]
		],
		height: '300px',
		onImageUpload: function(files, editor, welEditable) {
            sendFile(files[0], editor, welEditable);
        }
	});
    function sendFile(file, editor, welEditable) {
        data = new FormData();
        data.append('image', file);
        $.ajax({
            data: data,
            type: 'post',
            url: 'upload/image',
            cache: false,
            contentType: false,
            processData: false,
            success: function(url) {
                editor.insertImage(welEditable, url);
                $('#px-company-instalasi-form .panel-body').after('<input type="hidden" name="images[]" value="'+url+'">');
            }
        });
    };
    $('#px-company-instalasi-form-instalasi-businesstype').change(function() {
        var jenis_bisnis_id = this.value;

        $("#px-company-instalasi-form-instalasi-perusahaan option").remove();
        $('#px-company-instalasi-form-instalasi-perusahaan').append($('<option>', {
            value: '0',
            text : '-- Pilih Perusahaan --'
        }));

        $.ajax({
            url: 'admin_company/ajax_get_perusahaan',
            type: 'POST',
            dataType: 'json',
            data: {'id': jenis_bisnis_id},
            success : function(result){
                var data = result.result;
                //console.log(data);
                if (result.status == 'ok') {
                    $.each(data, function (i, item) {
                        $('#px-company-instalasi-form-instalasi-perusahaan').append($('<option>', {
                            value: item.id,
                            text : item.name
                        }));
                    });
                };  
            },
            error : function(jqXHR, textStatus, errorThrown) {
                alert(textStatus, errorThrown);
            }

        })
    });
});