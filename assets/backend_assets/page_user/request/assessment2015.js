//datatables function
var save_method; //for save method string
var table;

// var loc = location.pathname
//     loc = loc.indexOf('user_request')
// var status = loc>=0?2:0

table = $('.table').DataTable({
  
  "processing": true, //Feature control the processing indicator.
  "serverSide": true, //Feature control DataTables' server-side processing mode.
  "order": [[ 9, "desc" ]],
  
  // Load data for the table's content from an Ajax source
  "ajax": {
      "url": "user_request/ajax_assessment2015_list",
      "type": "POST",
      // "data": {
      //   "status": status
      // },
  },

  //Set column definition initialisation properties.
  "columnDefs": [
  { 
    "targets": [0, 3, 5, 8, 10], //last column
    "orderable": false, //set not orderable
  },
  ]

});

function reload_table()
{
  table.ajax.reload(null,false); //reload datatable ajax 
}

$(document).ready(function(){
  $('#px-company-assessment2015-message-form').validate({
    ignore: [],
    rules: {                                            
      id: {
        required: true
      }
    },
    submitHandler: function(form) {
      var target = $(form).attr('action');
      $('#px-company-assessment2015-message-form .msg-status').text('Deleting data');
      $.ajax({
        url : target,
        type : 'POST',
        dataType : 'json',
        data : $(form).serialize(),
        success : function(response){
          if(response.status == 'ok'){
            $('#px-company-assessment2015-message-form .msg-status').text('Delete Success...');
            window.location.href = response.redirect;
          }
          else
            $('#px-company-assessment2015-message-form .msg-status').text('Delete Failed');  
        },
        error : function(jqXHR, textStatus, errorThrown) {
          alert(textStatus, errorThrown);
        }
      });
    }
  });
  $('body').delegate('.btn-delete','click',function(){
    $('#px-company-assessment2015-message-box').addClass('open');
    var id = $(this).attr('data-target-id');
    $('#px-company-assessment2015-message-form input[name="id"]').val('');
    $('#px-company-assessment2015-message-form input[name="id"]').val(id);
  });
})