$(document).ready(function() {
	$('#px-pendaftaran-training-form-training').change(function(){
		var id = this.value;
		$.ajax({
	        url : 'admin_info_training/get_training_detail',
	        type : 'POST',
	        dataType : 'json',
	        data : { training_info_id: id },
	        success : function(data){
	          $('input[name="title"]').val(data.result.title);
	          $('input[name="pendaftaran_start"]').val(data.result.pendaftaran_start);
	          $('input[name="pendaftaran_end"]').val(data.result.pendaftaran_end);
	          $('input[name="date_start"]').val(data.result.date_start);
	          $('input[name="date_end"]').val(data.result.date_end);
	        },
	        error : function(jqXHR, textStatus, errorThrown) {
	          console.log(textStatus, errorThrown);
	        }
      	});
	});
	$('#px-info-training-form-content').summernote({
		toolbar: [
			['style', ['style']],
			['font', ['bold', 'italic', 'underline', 'clear']],
			['fontname', ['fontname']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']],
			['table', ['table']],
			['insert', ['link', 'picture', 'hr']],
			['view', ['fullscreen', 'codeview']],
			['help', ['help']]
		],
		height: '300px',
		onImageUpload: function(files, editor, welEditable) {
            sendFile(files[0], editor, welEditable);
        }
	});
    function sendFile(file, editor, welEditable) {
        data = new FormData();
        data.append('image', file);
        $.ajax({
            data: data,
            type: 'post',
            url: 'upload/image',
            cache: false,
            contentType: false,
            processData: false,
            success: function(url) {
                editor.insertImage(welEditable, url);
                $('#px-security-analysis-form .panel-body').after('<input type="hidden" name="images[]" value="'+url+'">');
            }
        });
    }
});