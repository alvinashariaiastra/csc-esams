$(document).ready(function(){
	$('#banner-slider').owlCarousel({
        loop:true,
        autoplay : 1000,
        stopOnHover : false,
        nav : true,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    });
    var mapDash = new GMaps({
		div: '#peta-status-kerawanan-dash',
		lat: -3.409856,
		lng: 119.9383623,
		zoom : 4
	});
	$(".select2-dropdown").select2().on("select2:select", function (e) {
		e.preventDefault();
		GMaps.geocode({
			address: this.value,
			callback: function(results, status) {
				if (status == 'OK') {
					if (results[0].address_components[0].long_name == 'Indonesia') {
						mapDash.setCenter(-3.409856, 119.9383623);
						mapDash.setZoom(4);
					}else{
						var latlng = results[0].geometry.location;
						mapDash.setCenter(latlng.lat(), latlng.lng());
						mapDash.setZoom(9);
					}
				}
			}
		});
	});

	$.ajax({
		url: 'user_status_kerawanan/get_all_location',
		dataType: 'json',
		success: function(data) {
			$.each(data, function(i, val){
				var mark = mapDash.addMarker({
					lat: val.latitude,
					lng:  val.longitude,
					title: val.instalasi_nm,
					infoWindow: {
						content: '<h4><img src="assets/backend_assets/img/'+val.icon_status+'"> '+val.instalasi_nm+'</h4><div>'
						+'<p>'+val.instalasi_alamat+'</p><br>'
						+'<button class="btn btn-default btn-show-status-kerawanan-berita" data-target-id="'+val.instalasi_id+'">Lihat Berita Terkini</button>'
						+'</div>',
						maxWidth: 200
					},
					icon : 'assets/backend_assets/img/'+val.icon_status
				});
			})
		}
	});

	$('body').delegate('.btn-show-status-kerawanan-berita','click',function(){
		var id = $(this).attr('data-target-id');
		var news = ' ';
		$.ajax({
			url : 'user_status_kerawanan/get_all_news',
			type : 'POST',
			dataType : 'Json',
			data : {'id' : id },
			success : function(result){
				$('#px-dashboard-status-kerawanan-new-modal .modal-body').html(' ');
				if(result.status == 'ok'){
                    if(result.data){
    					$.each(result.data, function(i, data){
    						var bg = 'bg-success';
                            if(data.status == 1)
                                bg = 'bg-success';
                            else if(data.status == 2)
                                bg = 'bg-warning';
                            else if(data.status == 3)
                                bg = 'bg-danger';
                            news = news+'<div data-target-id="'+data.id+'" class="list-group-item '+bg+' btn-show-detail-news-kerawanan-berita">'
                                +'<div class="media-body">'
                                    +'<span class="text-muted">'+data.date_created+'</span><br>'
                                    +data.title
                                +'</div>'
                            +'</div>'
    					});
                    }
                    $('#px-dashboard-status-kerawanan-new-modal .modal-body').html(news);
				}
                else {
                    $('#px-dashboard-status-kerawanan-new-modal .modal-body').html('Belum ditemukan berita untuk area ini');
                }
			}
		})
		$('#px-dashboard-status-kerawanan-new-modal').modal('show');
	});
    $('body').delegate('.btn-show-detail-news-kerawanan-berita','click',function(){
        var id = $(this).attr('data-target-id');
        $.ajax({
            url : 'user_status_kerawanan/get_detail_news',
            type : 'POST',
            dataType : 'Json',
            data : {'id' : id },
            success : function(result){
                $('#px-dashboard-status-kerawanan-news-detail-modal .modal-header h4').html(' ');
                $('#px-dashboard-status-kerawanan-news-detail-modal .modal-body .content').html(' ');
                if(result.status == 'ok'){
                    // $('#px-dashboard-status-kerawanan-new-modal').modal('hide');
                    $('#px-dashboard-status-kerawanan-news-detail-modal .modal-header h4').html(result.data.title);
                    $('#px-dashboard-status-kerawanan-news-detail-modal .modal-body .content').html(result.data.content);
                }
            }
        })
        $('#px-dashboard-status-kerawanan-news-detail-modal').modal('show');
    });

	// chart
	$.ajax({
		url : 'user/chart_jumlah_instalasi',
		type : 'POST',
		dataType : 'Json',
		success : function(result){
			chart_jumlah_instalasi(result.category,result.series);
		}
	});
        $.ajax({
		url : 'user/chart_jumlah_perusahaan',
		type : 'POST',
		dataType : 'Json',
		success : function(result){
			chart_jumlah_perusahaan(result.category,result.series);
		}
	});
	$.ajax({
		url : 'user/chart_jumlah_satpam',
		type : 'POST',
		dataType : 'Json',
		success : function(result){
			chart_jumlah_satpam(result.category,result.series);
		}
	});
	function chart_jumlah_instalasi(categories_data,series_data){
	    $('#instalasi-highchart').highcharts({
	        chart: {
	            type: 'column',
                    options3d: {
                    enabled: true,
                    alpha: 15,
                    beta: 15,
                    depth: 50,
                    viewDistance: 25
                }
	        },
	        title: {
	            text: 'Jumlah Instalasi Grup Astra'
	        },
	        xAxis: {
	            categories: categories_data,
	            crosshair: true
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Jumlah Instalasi'
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                '<td style="padding:0"><b>{point.y}</b></td></tr>',
	            footerFormat: '</table>',
	            shared: true,
	            useHTML: true
	        },
	        plotOptions: {
	            column: {
	                pointPadding: 0.2,
	                borderWidth: 0
	            }
	        },
	        series: [series_data]
	    });
	}
        function chart_jumlah_perusahaan(categories_data,series_data){
	    $('#perusahaan-highchart').highcharts({
	        chart: {
	            type: 'column',
                    options3d: {
                    enabled: true,
                    alpha: 15,
                    beta: 15,
                    depth: 50,
                    viewDistance: 25
                }
	        },
	        title: {
	            text: 'Jumlah Perusahaan Grup Astra'
	        },
	        xAxis: {
	            categories: categories_data,
	            crosshair: true
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Jumlah Perusahaan'
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                '<td style="padding:0"><b>{point.y}</b></td></tr>',
	            footerFormat: '</table>',
	            shared: true,
	            useHTML: true
	        },
	        plotOptions: {
	            column: {
	                pointPadding: 0.2,
	                borderWidth: 0
	            }
	        },
	        series: [series_data]
	    });
	}
	function chart_jumlah_satpam(categories_data,series_data){
	    $('#satpam-highchart').highcharts({
	        chart: {
	            type: 'bar',
                    options3d: {
                    enabled: true,
                    alpha: 15,
                    beta: 15,
                    depth: 50,
                    viewDistance: 25
                }
	        },
	        title: {
	            text: 'Jumlah Anggota Satpam Grup Astra'
	        },
	        xAxis: {
	            categories: categories_data,
	            crosshair: true
	        },
	        yAxis: {
	            min: 0,
	            title: {
	                text: 'Jumlah Satpam (orang)'
	            }
	        },
	        tooltip: {
	            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	                '<td style="padding:0"><b>{point.y}</b></td></tr>',
	            footerFormat: '</table>',
	            shared: true,
	            useHTML: true
	        },
	        legend: {
	            layout: 'vertical',
	            align: 'right',
	            verticalAlign: 'middle',
	            borderWidth: 0
	        },
	        plotOptions: {
	            column: {
	                pointPadding: 0.2,
	                borderWidth: 0
	            }
	        },
	        series: series_data
	    });
	}
	// static chart
    $.ajax({
        url : 'user/chart_asesment_2004',
        type : 'POST',
        dataType : 'Json',
        success : function(result){
            chart_asesment_2004(result.nama_instalasi,result.nilai);
        }
    });
    function chart_asesment_2004(name,data){
        $('#nilai-chart').highcharts({
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            title: {
                text: 'Perolehan Nilai Asesmen 2004 '+name
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}% ({point.y:,.0f} Kali)</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: 'Emas',
                    y: data.emas
                }, {
                    name: 'Hijau',
                    y: data.hijau,
                    sliced: true,
                    selected: true
                }, {
                    name: 'Biru',
                    y: data.biru
                }, {
                    name: 'Merah',
                    y: data.merah
                }, {
                    name: 'Hitam',
                    y: data.hitam
                }]
            }],
            colors: ['#D4AF37', '#23B14D', '#417EC1', '#ED1A23', '#000']
        });
    }
    $.ajax({
        url : 'user/chart_asesment_2015',
        type : 'POST',
        dataType : 'Json',
        success : function(result){
            chart_asesment_2015(result.nama_instalasi,result.nilai);
        }
    });
    function chart_asesment_2015(name,data){
        $('#nilai-2015-chart').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Perolehan Nilai Asesmen 2015 '+name
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}% ({point.y:,.0f} Kali)</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: 'Emas',
                    y: data.emas
                }, {
                    name: 'Hijau',
                    y: data.hijau,
                    sliced: true,
                    selected: true
                }, {
                    name: 'Biru',
                    y: data.biru
                }, {
                    name: 'Merah',
                    y: data.merah
                }, {
                    name: 'Hitam',
                    y: data.hitam
                }]
            }],
            colors: ['#D4AF37', '#23B14D', '#417EC1', '#ED1A23', '#000']
        });
    }
    $.ajax({
        url : 'user/chart_hasil_asesment_2004',
        type : 'POST',
        dataType : 'Json',
        success : function(result){
            chart_hasil_asesment_2004(result.name,result.category,result.series,result.warna);
        }
    });
    function chart_hasil_asesment_2004(name,categories_data,series_data,warna){
        $('#hasil-asesmen-2004-per-instalasi-highchart').highcharts({
            chart: {
                type: 'column',
                    options3d: {
                        enabled: true,
                        alpha: 15,
                        beta: 15,
                        depth: 50,
                        viewDistance: 25
                    }
            },
            title: {
                text: name
            },
            subtitle: {
                text: 'Grafik Hasil Asesmen 2004'
            },
            xAxis: {
                categories: categories_data,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Nilai Asesmen'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0,
                    colorByPoint: true
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [series_data],
            colors: warna
        });
    }
    $.ajax({
        url : 'user/chart_hasil_asesment_2015',
        type : 'POST',
        dataType : 'Json',
        success : function(result){
            chart_hasil_asesment_2015(result.name,result.category,result.series,result.plotline,result.warna);
        }
    });
    function chart_hasil_asesment_2015(name,categories_data,series_data,plotline,warna){
        $('#hasil-asesmen-2015-per-instalasi-highchart').highcharts({
            chart: {
                type: 'column',
                    options3d: {
                        enabled: true,
                        alpha: 15,
                        beta: 15,
                        depth: 50,
                        viewDistance: 25
                    }
            },
            title: {
                text: name
            },
            subtitle: {
                text: 'Grafik Hasil Asesmen 2015'
            },
            xAxis: {
                categories: categories_data,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Nilai Asesmen'
                }
                // plotLines: [{
                //     value: plotline.Emas,
                //     width: 2,
                //     zIndex: 1000,
                //     color: '#D4AF37',
                //     label : {
                //         text : 'Emas'
                //     }
                // },{
                //     value: plotline.Hijau,
                //     width: 2,
                //     zIndex: 1000,
                //     color: '#23B14D',
                //     label : {
                //         text : 'Hijau'
                //     }
                // },{
                //     value: plotline.Biru,
                //     width: 2,
                //     zIndex: 1000,
                //     color: '#417EC1',
                //     label : {
                //         text : 'Biru'
                //     }
                // },{
                //     value: plotline.Merah,
                //     width: 2,
                //     zIndex: 1000,
                //     color: '#ED1A23',
                //     label : {
                //         text : 'Merah'
                //     }
                // },{
                //     value: plotline.Hitam,
                //     width: 2,
                //     zIndex: 1000,
                //     color: '#000',
                //     label : {
                //         text : 'Hitam'
                //     }
                // }]
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0,
                    colorByPoint: true
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [series_data],
            colors: warna
        });
    }
    $('#hasil-asesmen-2004-menu').change(function(){
        var id = $(this).val();
        $.ajax({
            url : 'user/chart_hasil_asesment_2004',
            type : 'POST',
            dataType : 'Json',
            data : {'id':id},
            success : function(result){
                chart_hasil_asesment_2004(result.name,result.category,result.series,result.warna);
            }
        });
    });
    $('#hasil-asesmen-2015-menu').change(function(){
        var id = $(this).val();
        $.ajax({
            url : 'user/chart_hasil_asesment_2015',
            type : 'POST',
            dataType : 'Json',
            data : {'id':id},
            success : function(result){
                chart_hasil_asesment_2015(result.name,result.category,result.series,result.plotline,result.warna);
            }
        });
    });
    $.ajax({
        url : 'user/chart_stc_perorangan',
        type : 'POST',
        dataType : 'Json',
        success : function(result){
            chart_stc_per_orang(result.data);
            console.log(result.data);
        }
    });
    function chart_stc_per_orang(data_series){
        $('#hasil-stc-per-orang-highchart').highcharts({
            chart: {
                polar: true,
                type: 'line'
            },

            title: {
                text: 'Hasil STC per Individu',
                x: -80
            },

            pane: {
                size: '80%'
            },

            xAxis: {
                categories: ['STC 1','STC 2','STC 3','STC 4'],
                tickmarkPlacement: 'on',
                lineWidth: 0
            },

            yAxis: {
                gridLineInterpolation: 'polygon',
                lineWidth: 0,
                min: 0
            },

            tooltip: {
                shared: true,
                pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f} %</b><br/>'
            },

            legend: {
                align: 'right',
                verticalAlign: 'top',
                y: 70,
                layout: 'vertical'
            },

            series: data_series
        });
    }
    $('#standar-kompetensi-per-orang-menu').change(function(){
        var id = $(this).val();
        $.ajax({
            url : 'user/chart_stc_perorangan',
            type : 'POST',
            dataType : 'Json',
            data : {'id':id},
            success : function(result){
                chart_stc_per_orang(result.data);
                console.log(result.data);
            }
        });
    });
    $.ajax({
        url : 'user/chart_stc_perinstalasi',
        type : 'POST',
        dataType : 'Json',
        success : function(result){
            chart_stc_per_instalasi(result.data);
            console.log(result.data);
        }
    });
    function chart_stc_per_instalasi(data_series){
        $('#hasil-stc-per-instalasi-highchart').highcharts({
            chart: {
                polar: true,
                type: 'area'
            },

            title: {
                text: 'Hasil STC per Instalasi',
                x: -80
            },

            pane: {
                size: '80%'
            },

            xAxis: {
                categories: ['STC 1','STC 2','STC 3','STC 4'],
                tickmarkPlacement: 'on',
                lineWidth: 0
            },

            yAxis: {
                gridLineInterpolation: 'polygon',
                lineWidth: 0,
                min: 0
            },

            tooltip: {
                shared: true,
                pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f} %</b><br/>'
            },

            legend: {
                align: 'right',
                verticalAlign: 'top',
                y: 70,
                layout: 'vertical'
            },

            series: data_series
        });
    }
})