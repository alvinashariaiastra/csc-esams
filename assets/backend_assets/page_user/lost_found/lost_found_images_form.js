$(document).ready(function () {
    $('#px-admin-lost_found-lost_found_images-form').validate({
        ignore: [],
        rules: {},
        submitHandler: function (form) {
            var target = $(form).attr('action');
            $('#px-admin-lost_found-lost_found_images-form .alert-warning').removeClass('hidden');
            $('#px-admin-lost_found-lost_found_images-form .alert-success').addClass('hidden');
            $('#px-admin-lost_found-lost_found_images-form .alert-danger').addClass('hidden');
            $.ajax({
                url: target,
                type: 'POST',
                dataType: 'json',
                data: $(form).serialize(),
                success: function (response) {
                    $('#px-admin-lost_found-lost_found_images-form .alert-warning').addClass('hidden');
                    if (response.status == 'ok') {
                        $('#px-admin-lost_found-lost_found_images-form .alert-success').removeClass('hidden').children('span').text(response.msg);
                        window.location.href = response.redirect;
                    } else
                        $('#px-admin-lost_found-lost_found_images-form .alert-danger').removeClass('hidden').children('span').text(response.msg);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(textStatus, errorThrown);
                }
            });
        }
    });
    $("#file-upload").fileupload({
        dataType: 'text',
        autoUpload: false,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        maxFileSize: 5000000 // 5 MB
    }).on('fileuploadadd', function (e, data) {
        data.process();
    }).on('fileuploadprocessalways', function (e, data) {
        if (data.files.error) {
            data.abort();
            alert('Image file must be jpeg/jpg, png or gif with less than 5MB');
        } else {
            data.submit();
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        var target = $('#target-file').val();
        $('#px-admin-lost_found-lost_found_images-fileupload-' + target + '-progress').text(progress + '%');
        $('#px-admin-lost_found-lost_found_images-fileupload-' + target + '-upload-button').attr('disabled', true);
    }).on('fileuploaddone', function (e, data) {
        var target = $('#target-file').val();
        $('#px-admin-lost_found-lost_found_images-fileupload-' + target + '-upload-button').removeAttr('disabled');
        $('#preview_' + target + ' a').attr('href', data.result);
        $('#preview_' + target + ' img').attr('src', data.result);
        $('[name="' + target + '"]').val(data.result);
    }).on('fileuploadfail', function (e, data) {
        alert('File upload failed.');
        $('#px-admin-lost_found-lost_found_images-fileupload-upload-button').removeAttr('disabled');
    }).on('fileuploadalways', function () {
    });
    $('.btn-upload').click(function () {
        var target = $(this).attr('data-target');
        var old_temp = $('[name="' + target + '"]').val();
        $('#file-upload #target-file').val(target);
        $('#file-upload #old-file').val(old_temp);
    });
})