$(document).ready(function(){
	$('#px-lost_found-form').validate({
		rules: {                                            
			title: {
				required: true
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-lost_found-form .alert-warning').removeClass('hidden');
			$('#px-lost_found-form .alert-success').addClass('hidden');
			$('#px-lost_found-form .alert-danger').addClass('hidden');
			$('.px-summernote').each(function() {
				$(this).val($(this).code());
			});
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					$('#px-lost_found-form .alert-warning').addClass('hidden');
					if(response.status == 'ok'){
						$('#px-lost_found-form .alert-success').removeClass('hidden').children('span').text(response.msg);
						window.location.href = response.redirect;
					}
					else
						$('#px-lost_found-form .alert-danger').removeClass('hidden').children('span').text(response.msg);	
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
})