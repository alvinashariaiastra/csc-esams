$(document).ready(function(){
	$('#px-company-assessment2004-form').validate({
		rules: {                                            
			instalasi_id: {
				required: true
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-company-assessment2004-form .alert-warning').removeClass('hidden');
			$('#px-company-assessment2004-form .alert-success').addClass('hidden');
			$('#px-company-assessment2004-form .alert-danger').addClass('hidden');
			$('.px-summernote').each(function() {
				$(this).val($(this).code());
			});
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					$('#px-company-assessment2004-form .alert-warning').addClass('hidden');
					if(response.status == 'ok'){
						$('#px-company-assessment2004-form .alert-success').removeClass('hidden').children('span').text(response.msg);
						window.location.href = response.redirect;
					}
					else
						$('#px-company-assessment2004-form .alert-danger').removeClass('hidden').children('span').text(response.msg);	
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
	$('#px-company-assessment2004-form-assessment2004-rules1').rules('add', {
		number : true,
		required : true
	});
	$('#px-company-assessment2004-form-assessment2004-rules2').rules('add', {
		number : true,
		required : true
	});
	$('#px-company-assessment2004-form-assessment2004-rules3').rules('add', {
		number : true,
		required : true
	});
	$('#px-company-assessment2004-form-assessment2004-rules4').rules('add', {
		number : true,
		required : true
	});
	$('#px-company-assessment2004-form-assessment2004-nilai_akhir').rules('add', {
		number : true,
		required : true
	});
	hitung_nilai_akhir()
	$('.rules-element').keyup(function(){
		hitung_nilai_akhir();
		cek_warna(nilai_akhir);
	});
	$('#px-company-assessment2004-form-assessment2004-nilai_akhir').change(function(){
		hitung_nilai_akhir();
		cek_warna(nilai_akhir);
	})
	function hitung_nilai_akhir(){
		var element_1 = parseFloat($('#px-company-assessment2004-form-assessment2004-rules1').val());
		var element_2 = parseFloat($('#px-company-assessment2004-form-assessment2004-rules2').val());
		var element_3 = parseFloat($('#px-company-assessment2004-form-assessment2004-rules3').val());
		var element_4 = parseFloat($('#px-company-assessment2004-form-assessment2004-rules4').val());
		if(!element_1)
			element_1 = 0;
		if(!element_2)
			element_2 = 0;
		if(!element_3)
			element_3 = 0;
		if(!element_4)
			element_4 = 0;
		nilai_akhir = (parseFloat(element_1) + parseFloat(element_2) + parseFloat(element_3) + parseFloat(element_4)) / 4;
		$('#px-company-assessment2004-form-assessment2004-nilai_akhir').val(nilai_akhir.toFixed(2));
	}
	function cek_warna(nilai){
		if(!nilai)
			nilai = 0;
    	$.ajax({
    		url : 'user_company/assessment_2004_check_colour',
    		type : 'POST',
    		dataType : 'json',
    		data : {'nilai' : nilai},
    		success : function(response){	
    			if(response.warna != 'putih')
    				$('#nilai-akhir-colour').html(response.warna);
    			else
    				$('#nilai-akhir-colour').html(' ');
    		}
    	});
    }
    $('#px-company-assessment2004-form-assessment2004-critical_point').summernote({
		toolbar: [
			['style', ['style']],
			['font', ['bold', 'italic', 'underline', 'clear']],
			['fontname', ['fontname']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']],
			['table', ['table']],
			['insert', ['link', 'hr']],
			['view', ['fullscreen', 'codeview']],
			['help', ['help']]
		],
		height: '300px',
		onImageUpload: function(files, editor, welEditable) {
            sendFile(files[0], editor, welEditable);
        }
	});
    function sendFile(file, editor, welEditable) {
        data = new FormData();
        data.append('image', file);
        $.ajax({
            data: data,
            type: 'post',
            url: 'upload/image',
            cache: false,
            contentType: false,
            processData: false,
            success: function(url) {
                editor.insertImage(welEditable, url);
                $('#px-company-assessment2004-form .panel-body').after('<input type="hidden" name="images[]" value="'+url+'">');
            }
        });
    }
})