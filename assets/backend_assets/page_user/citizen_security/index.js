function popPic1()
{
	$('#picture1modal').modal('show');
}

function popPic2()
{
	$('#picture2modal').modal('show');
}

function popPic3()
{
	$('#picture3modal').modal('show');
}

function changeCursor1()
{
	$("#px-citizen-security-report-form-picture1-img").css("cursor", "pointer");
}

function changeCursor2()
{
	$("#px-citizen-security-report-form-picture2-img").css("cursor", "pointer");
}

function changeCursor3()
{
	$("#px-citizen-security-report-form-picture3-img").css("cursor", "pointer");
}

$(document).ready(function(){
	jQuery.validator.addMethod("min1pic", function(value, element) {
		var picture1 = $('#px-citizen-security-report-form-picture1-img-path').val();
		var picture2 = $('#px-citizen-security-report-form-picture2-img-path').val();
		var picture3 = $('#px-citizen-security-report-form-picture3-img-path').val();
		
		var checkPicture1 = picture1 === undefined || picture1 == "";
		var checkPicture2 = picture2 === undefined || picture2 == "";
		var checkPicture3 = picture3 === undefined || picture3 == "";
		
		return !(checkPicture1 && checkPicture2 && checkPicture3);
	}, "Silahkan menambahkan minimal 1 foto kejadian.");
	
	$('#px-citizen-security-report-form').validate({
		ignore: [],
		rules: {
			username: {
				required: true
			},
			email: {
				required: true
			},
			datetime: {
				required: true
			},
			place: {
				required: true
			},
			notes: {
				required: true
			},
			picture1: {
				min1pic: true
			},
			picture2: {
				min1pic: true
			},
			picture3: {
				min1pic: true
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-citizen-security-report-form .alert-warning').removeClass('hidden');
			$('#px-citizen-security-report-form .alert-success').addClass('hidden');
			$('#px-citizen-security-report-form .alert-danger').addClass('hidden');
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					$('#px-citizen-security-report-form .alert-warning').addClass('hidden');
					if(response.status == 'ok'){
						$('#px-citizen-security-report-form .alert-success').removeClass('hidden').children('span').text(response.msg);
						document.body.scrollTop = document.documentElement.scrollTop = 0;
						window.setTimeout(function () {
							window.location.href = response.redirect;
						}, 2000);
					}
					else
						$('#px-citizen-security-report-form .alert-danger').removeClass('hidden').children('span').text(response.msg);	
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
	
	$('.form_datetime').datetimepicker({format: 'yyyy-mm-dd hh:ii', autoclose: true, todayBtn: true, todayHighlight: true, startDate: '2018-01-01 00:00'});
	
	$('.btn-upload1').click(function(){
		var target = $(this).attr('data-target');
		$('#file-upload1 #target-file1').val(target);
	});
	
	$('.btn-upload2').click(function(){
		var target = $(this).attr('data-target');
		$('#file-upload2 #target-file2').val(target);
	});
	
	$('.btn-upload3').click(function(){
		var target = $(this).attr('data-target');
		$('#file-upload3 #target-file3').val(target);
	});
	
	$("#file-upload1").fileupload({
		dataType: 'text',
		autoUpload: false,
		acceptFileTypes: /(\.|\/)(jpe?g|png)$/i,
		maxFileSize: 200000 // 2 KB
		}).on('fileuploadadd', function(e, data) {
			data.process();
		}).on('fileuploadprocessalways', function (e, data) {
		if (data.files.error) {
			data.abort();
			alert('Image file must be jpeg/jpg or png with less than 200 KB');
		} else {
			data.submit();
		}
		}).on('fileuploadprogressall', function (e, data) {
			var target = $('#target-file1').val();
			$('#px-citizen-security-report-form-picture1-button').attr('disabled',true);
		}).on('fileuploaddone', function (e, data) {
			var target = $('#target-file1').val();
			$('#px-citizen-security-report-form-picture1-button').removeAttr('disabled');
			$('[name="'+target+'"]').val(data.result);
			$('#px-citizen-security-report-form-picture1-row').html('<img src="'+data.result+'" alt="picture1" onclick="popPic1();" onmouseover="changeCursor1();" id="px-citizen-security-report-form-picture1-img" height="200px" width="250px" style="border-style: solid; border-color: black; border-width: 1px;"><br><span style="padding: 5px;">Click picture to enlarge</span>');
			$('#picture1modalbody').html('<img src="'+data.result+'" alt="picture1" id="px-citizen-security-report-form-picture1-modal-img" style="border-style: solid; border-color: black; border-width: 1px;">');
		}).on('fileuploadfail', function (e, data) {
			alert('File upload failed. Error message: ' + data.files[0].error);
			$('#px-citizen-security-report-form-picture1-button').removeAttr('disabled');
		}).on('fileuploadalways', function() {
	});
	
	$("#file-upload2").fileupload({
		dataType: 'text',
		autoUpload: false,
		acceptFileTypes: /(\.|\/)(jpe?g|png)$/i,
		maxFileSize: 200000 // 2 KB
		}).on('fileuploadadd', function(e, data) {
			data.process();
		}).on('fileuploadprocessalways', function (e, data) {
		if (data.files.error) {
			data.abort();
			alert('Image file must be jpeg/jpg or png with less than 200 KB');
		} else {
			data.submit();
		}
		}).on('fileuploadprogressall', function (e, data) {
			var target = $('#target-file2').val();
			$('#px-citizen-security-report-form-picture2-button').attr('disabled',true);
		}).on('fileuploaddone', function (e, data) {
			var target = $('#target-file2').val();
			$('#px-citizen-security-report-form-picture2-button').removeAttr('disabled');
			$('[name="'+target+'"]').val(data.result);
			$('#px-citizen-security-report-form-picture2-row').html('<img src="'+data.result+'" alt="picture2" onclick="popPic2();" onmouseover="changeCursor2();" id="px-citizen-security-report-form-picture2-img" height="200px" width="250px" style="border-style: solid; border-color: black; border-width: 1px;"><br><span style="padding: 5px;">Click picture to enlarge</span>');
			$('#picture2modalbody').html('<img src="'+data.result+'" alt="picture2" id="px-citizen-security-report-form-picture2-modal-img" style="border-style: solid; border-color: black; border-width: 1px;">');
		}).on('fileuploadfail', function (e, data) {
			alert('File upload failed. Error message: ' + data.files[0].error);
			$('#px-citizen-security-report-form-picture2-button').removeAttr('disabled');
		}).on('fileuploadalways', function() {
	});
	
	$("#file-upload3").fileupload({
		dataType: 'text',
		autoUpload: false,
		acceptFileTypes: /(\.|\/)(jpe?g|png)$/i,
		maxFileSize: 200000 // 2 KB
		}).on('fileuploadadd', function(e, data) {
			data.process();
		}).on('fileuploadprocessalways', function (e, data) {
		if (data.files.error) {
			data.abort();
			alert('Image file must be jpeg/jpg or png with less than 200 KB');
		} else {
			data.submit();
		}
		}).on('fileuploadprogressall', function (e, data) {
			var target = $('#target-file3').val();
			$('#px-citizen-security-report-form-picture3-button').attr('disabled',true);
		}).on('fileuploaddone', function (e, data) {
			var target = $('#target-file3').val();
			$('#px-citizen-security-report-form-picture3-button').removeAttr('disabled');
			$('[name="'+target+'"]').val(data.result);
			$('#px-citizen-security-report-form-picture3-row').html('<img src="'+data.result+'" alt="picture3" onclick="popPic3();" onmouseover="changeCursor3();" id="px-citizen-security-report-form-picture3-img" height="200px" width="250px" style="border-style: solid; border-color: black; border-width: 1px;"><br><span style="padding: 5px;">Click picture to enlarge</span>');
			$('#picture3modalbody').html('<img src="'+data.result+'" alt="picture3" id="px-citizen-security-report-form-picture3-modal-img" style="border-style: solid; border-color: black; border-width: 1px;">');
		}).on('fileuploadfail', function (e, data) {
			alert('File upload failed. Error message: ' + data.files[0].error);
			$('#px-citizen-security-report-form-picture3-button').removeAttr('disabled');
		}).on('fileuploadalways', function() {
	});
});