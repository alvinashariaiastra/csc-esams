$(document).ready(function(){
	$('#px-standarkompetensi-listrules-form').validate({
		rules: {                                            
			rules: {
				required: true
			}
		},
		submitHandler: function(form) {
			var target = $(form).attr('action');
			$('#px-standarkompetensi-listrules-form .alert-warning').removeClass('hidden');
			$('#px-standarkompetensi-listrules-form .alert-success').addClass('hidden');
			$('#px-standarkompetensi-listrules-form .alert-danger').addClass('hidden');
			$('.px-summernote').each(function() {
				$(this).val($(this).code());
			});
			$.ajax({
				url : target,
				type : 'POST',
				dataType : 'json',
				data : $(form).serialize(),
				success : function(response){
					$('#px-standarkompetensi-listrules-form .alert-warning').addClass('hidden');
					if(response.status == 'ok'){
						$('#px-standarkompetensi-listrules-form .alert-success').removeClass('hidden').children('span').text(response.msg);
						window.location.href = response.redirect;
					}
					else
						$('#px-standarkompetensi-listrules-form .alert-danger').removeClass('hidden').children('span').text(response.msg);	
				},
				error : function(jqXHR, textStatus, errorThrown) {
					alert(textStatus, errorThrown);
				}
			});
		}
	});
})