$(document).ready(function(){
    var form = $("#px-standar-kompetensi-form");
    $('#px-standar-kompetensi-form').validate({
        rules: {                                            
            rules: {
                required: true
            }
        },
        submitHandler: function(form) {
            var target = $(form).attr('action');
            $('#px-standar-kompetensi-form .alert-warning').removeClass('hidden');
            $('#px-standar-kompetensi-form .alert-success').addClass('hidden');
            $('#px-standar-kompetensi-form .alert-danger').addClass('hidden');
            $('.px-summernote').each(function() {
                $(this).val($(this).code());
            });
            $.ajax({
                url : target,
                type : 'POST',
                dataType : 'json',
                data : $(form).serialize(),
                success : function(response){
                    $('#px-standar-kompetensi-form .alert-warning').addClass('hidden');
                    if(response.status == 'ok'){
                        $('#px-standar-kompetensi-form .alert-success').removeClass('hidden').children('span').text(response.msg);
                        window.location.href = response.redirect;
                    }
                    else
                        $('#px-standar-kompetensi-form .alert-danger').removeClass('hidden').children('span').text(response.msg);  
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    alert(textStatus, errorThrown);
                }
            });
        }
    });
    form.children("div").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        onStepChanging: function (event, currentIndex, newIndex)
        {
            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onFinishing: function (event, currentIndex)
        {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
            var target = $(form).attr('action');
            $('#px-standar-kompetensi-form .alert-warning').removeClass('hidden');
            $('#px-standar-kompetensi-form .alert-success').addClass('hidden');
            $('#px-standar-kompetensi-form .alert-danger').addClass('hidden');
            $('.px-summernote').each(function() {
                $(this).val($(this).code());
            });
            $.ajax({
                url : target,
                type : 'POST',
                dataType : 'json',
                data : $(form).serialize(),
                success : function(response){
                    $('#px-standar-kompetensi-form .alert-warning').addClass('hidden');
                    if(response.status == 'ok'){
                        $('#px-standar-kompetensi-form .alert-success').removeClass('hidden').children('span').text(response.msg);
                        window.location.href = response.redirect;
                    }
                    else
                        $('#px-standar-kompetensi-form .alert-danger').removeClass('hidden').children('span').text(response.msg);  
                },
                error : function(jqXHR, textStatus, errorThrown) {
                    alert(textStatus, errorThrown);
                }
            });
        }
    });
    $('#px-standarkompetensi-form-standarkompetensi-perusahaan').change(function() {
        var perusahaan_id = this.value;

        $("#px-standarkompetensi-form-standarkompetensi-instalasi option").remove();
        $('#px-standarkompetensi-form-standarkompetensi-instalasi').append($('<option>', {
            value: '0',
            text : '-- Pilih Instalasi --'
        }));
        $("#px-standarkompetensi-form-standarkompetensi-user option").remove();
        $('#px-standarkompetensi-form-standarkompetensi-user').append($('<option>', {
            value: '0',
            text : '-- Pilih User --'
        }));

        $.ajax({
            url: 'user_standar_kompetensi/ajax_get_instalasi',
            type: 'POST',
            dataType: 'json',
            data: {'id': perusahaan_id},
            success : function(result){
                var data = result.result;
                //console.log(data);
                if (result.status == 'ok') {
                    $.each(data, function (i, item) {
                        $('#px-standarkompetensi-form-standarkompetensi-instalasi').append($('<option>', {
                            value: item.id,
                            text : item.name
                        }));
                    });
                };  
            },
            error : function(jqXHR, textStatus, errorThrown) {
                alert(textStatus, errorThrown);
            }

        })
    });
    $('#px-standarkompetensi-form-standarkompetensi-instalasi').change(function() {
        var perusahaan_id = this.value;

        $("#px-standarkompetensi-form-standarkompetensi-user option").remove();
        $('#px-standarkompetensi-form-standarkompetensi-user').append($('<option>', {
            value: '0',
            text : '-- Pilih User --'
        }));

        $.ajax({
            url: 'user_standar_kompetensi/ajax_get_user',
            type: 'POST',
            dataType: 'json',
            data: {'id': perusahaan_id},
            success : function(result){
                var data = result.result;
                //console.log(data);
                if (result.status == 'ok') {
                    $.each(data, function (i, item) {
                        $('#px-standarkompetensi-form-standarkompetensi-user').append($('<option>', {
                            value: item.id,
                            text : item.realname
                        }));
                    });
                };  
            },
            error : function(jqXHR, textStatus, errorThrown) {
                alert(textStatus, errorThrown);
            }

        })
    });
    $('#px-standarkompetensi-form-standarkompetensi-user').change(function(){
        var level_id = $(this).children('option:selected').attr('data-level');
        $.ajax({
            url: 'user_standar_kompetensi/ajax_get_stc',
            type: 'POST',
            dataType: 'json',
            data: {'level_id': level_id},
            success : function(result){
                $('section[data-section="section-stc1"').html(result.stc.stc1);
                $('section[data-section="section-stc2"').html(result.stc.stc2);
                $('section[data-section="section-stc3"').html(result.stc.stc3);
                $('section[data-section="section-stc4"').html(result.stc.stc4);
                hitung_nilai_akhir();
            },
            error : function(jqXHR, textStatus, errorThrown) {
                alert(textStatus, errorThrown);
            }

        })
    });
    // perhitungan nilai akhir
    $('body').delegate('.stc-score-1','click',function(){
        hitung_nilai_akhir();
    });
    $('body').delegate('.stc-score-2','click',function(){
        hitung_nilai_akhir();
    });
    $('body').delegate('.stc-score-3','click',function(){
        hitung_nilai_akhir();
    });
    $('body').delegate('.stc-score-4','click',function(){
        hitung_nilai_akhir();
    });
    hitung_nilai_akhir();
    function hitung_nilai_akhir(){
        var stc_1 = 0,
            stc_2 = 0,
            stc_3 = 0,
            stc_4 = 0,
            stc_1_count = 0,
            stc_2_count = 0,
            stc_3_count = 0,
            stc_4_count = 0;
        if($('.stc-score-1:checked').length > 0){
            $('.stc-score-1:checked').each(function(){
                stc_1 = parseFloat(stc_1) + parseFloat($(this).val());
                stc_1_count++;
            });
        }
        if($('.stc-score-2:checked').length > 0){
            $('.stc-score-2:checked').each(function(){
                stc_2 = parseFloat(stc_2) + parseFloat($(this).val());
                stc_2_count++;
            });
        }
        if($('.stc-score-3:checked').length > 0){
            $('.stc-score-3:checked').each(function(){
                stc_3 = parseFloat(stc_3) + parseFloat($(this).val());
                stc_3_count++;
            });
        }
        if($('.stc-score-4:checked').length > 0){
            $('.stc-score-4:checked').each(function(){
                stc_4 = parseFloat(stc_4) + parseFloat($(this).val());
                stc_4_count++;
            });
        }
        var total = (stc_1/(stc_1_count * 4) + stc_2/(stc_2_count * 4) + stc_3/(stc_3_count * 4) + stc_4/(stc_4_count * 4)) / 4;
        console.log('Stc 1 '+stc_1+' / '+(stc_1_count * 4)+' = '+stc_1/(stc_1_count * 4));
        console.log('Stc 2 '+stc_2+' / '+(stc_2_count * 4)+' = '+stc_2/(stc_2_count * 4));
        console.log('Stc 3 '+stc_3+' / '+(stc_3_count * 4)+' = '+stc_3/(stc_3_count * 4));
        console.log('Stc 4 '+stc_4+' / '+(stc_4_count * 4)+' = '+stc_4/(stc_4_count * 4));
        console.log('Stc Total '+total.toFixed(2) * 100+'%');
        $('#px-standarkompetensi-form-standarkompetensi-finalscore').val(total.toFixed(2) * 100);
    }
})