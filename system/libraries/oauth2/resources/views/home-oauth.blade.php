@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body" id="panelBody">
                    You are logged in! Our secret resources are just one click away! :)

                    <hr/>
                    Username: <span class="value">{!! $resource_owner['email'] !!}</span> <br/>
                    Access Token: <span class="value">{!! $access_token !!}</span> <br/>
                    Refresh Token: <span class="value">{!! $refresh_token !!}</span> <br/>
                    Token Expire Time: <span class="value">{!! $token_expire_time !!}</span> <br/>
                    Is Expired: <span class="value">{!! $token_is_expired !!}</span> <br/>

                    <hr/>
                    @foreach($resource_owner as $key => $value)
                        {!! $key !!}: <span class="value">{!! $value !!}</span> <br/>
                    @endforeach
                                        
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>    
</script>
@endsection
