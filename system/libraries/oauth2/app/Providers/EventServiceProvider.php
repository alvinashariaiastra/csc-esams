<?php

namespace App\Providers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Event::listen('Aacotroneo\Saml2\Events\Saml2LoginEvent', function ($event) {

            $user = $event->getSaml2User();
            //dd($event);
            $userData = [
                'id' => $user->getUserId(),
                'name' => $user->getNameId(),
                'attributes' => $user->getAttributes(),
                'sessionIndex' => $user->getSessionIndex()
                //'assertion' => $user->getRawSamlAssertion()
            ];

            Log::info('SAML2 Assertion for this user:');
            Log::info($user->getRawSamlAssertion());

            $ssoUser = \App\User::where('username', $user->getNameId());

            if($ssoUser->count() == 0){
                $ssoUser = new \App\User;
                $ssoUser->name  = $user->getNameId();
                $ssoUser->username  = $user->getNameId();
                $ssoUser->email  = $user->getNameId();
                $ssoUser->sessionIndex = $user->getSessionIndex();
                $ssoUser->attributes = json_encode($user->getAttributes());
                $ssoUser->save();
            }else{
                $ssoUser = $ssoUser->firstOrFail();
                $ssoUser->name  = $user->getNameId();
                $ssoUser->username  = $user->getNameId();
                $ssoUser->email  = $user->getNameId();
                $ssoUser->sessionIndex = $user->getSessionIndex();
                $ssoUser->attributes = json_encode($user->getAttributes());
                $ssoUser->save();
            };   

            \Auth::login($ssoUser);                                 
        });


         Event::listen('Aacotroneo\Saml2\Events\Saml2LogoutEvent', function ($event) {
            Log::info("LOGOUT EVENT FIRED AT YENLO!!");
            \File::cleanDirectory(storage_path('framework/sessions')); //stupid way to kill the session, as no session-related info is available at SAML2 Request from WSO2 IS
            
            \App\User::getQuery()->delete();
        });

    }
}
