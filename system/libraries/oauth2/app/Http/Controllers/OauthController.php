<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;


class OauthController extends Controller
{


    private function laravelLogin($resourceOwner){

        if(!isset($resourceOwner['email'])){
            $error['message'] = "No email field available in Resource Owner! Can't proceed.";
            $error['resouceOwner'] = $resourceOwner;
            echo json_encode($error);
            die();
        }

        $ssoUser = \App\User::where('username', $resourceOwner['email']);

        if($ssoUser->count() == 0){
            $ssoUser = new \App\User;
            $ssoUser->name  = isset($resourceOwner['family_name']) ? $resourceOwner['family_name'] : "- - -";
            $ssoUser->username  = $resourceOwner['email'];
            $ssoUser->email  = $resourceOwner['email'];
            $ssoUser->sessionIndex = md5($resourceOwner['email']);
            $ssoUser->attributes = json_encode($resourceOwner);
            $ssoUser->save();
        }else{
            $ssoUser = $ssoUser->firstOrFail();
            $ssoUser->name  = isset($resourceOwner['family_name']) ? $resourceOwner['family_name'] : "- - -";
            $ssoUser->username  = $resourceOwner['email'];
            $ssoUser->email  = $resourceOwner['email'];
            $ssoUser->sessionIndex = md5($resourceOwner['email']);
            $ssoUser->attributes = json_encode($resourceOwner);
            $ssoUser->save();
        };   

        \Auth::login($ssoUser);
    }
   
    public function login(){        
        $oAuthServerHost    = env("OAUTH2_HOST");
        $clientID           = env("OAUTH2_CLIENT_ID");
        $clientSecret       = env("OAUTH2_CLIENT_SECRET");
        $callback           = env("OAUTH2_CALLBACK");

        $provider = new \League\OAuth2\Client\Provider\GenericProvider([
            'clientId'                => $clientID,
            'clientSecret'            => $clientSecret,
            'redirectUri'             => $callback,
            'urlAuthorize'            => $oAuthServerHost.'/oauth2/authorize',
            'urlAccessToken'          => $oAuthServerHost.'/oauth2/token',
            'urlResourceOwnerDetails' => $oAuthServerHost.'/oauth2/userinfo',
            'scopes'                  => ['openid'],
            'scopeSeparator'          => ',',         
            'verify'                  => false
        ]);

        // If we don't have an authorization code then get one
        if (!isset($_GET['code'])) {

            // Fetch the authorization URL from the provider; this returns the
            // urlAuthorize option and generates and applies any necessary parameters
            // (e.g. state).
            $authorizationUrl = $provider->getAuthorizationUrl();

            // Get the state generated for you and store it to the session.
            $_SESSION['oauth2state'] = $provider->getState();

            // Redirect the user to the authorization URL.
            //header('Location: ' . $authorizationUrl);
            return \Redirect::to($authorizationUrl);
            exit;

        // Check given state against previously stored one to mitigate CSRF attack
        } elseif (empty($_GET['state']) || (isset($_SESSION['oauth2state']) && $_GET['state'] !== $_SESSION['oauth2state'])) {

            if (isset($_SESSION['oauth2state'])) {
                unset($_SESSION['oauth2state']);
            }
            
            exit('Invalid state');

        } else {

            try {                
                // Try to get an access token using the authorization code grant.
                $accessToken = $provider->getAccessToken('authorization_code', [
                    'code' => $_GET['code']
                ]);                

                // We have an access token, which we may use in authenticated
                // requests against the service provider's API.
                $data['access_token'] =     $accessToken->getToken();
                $data['refresh_token'] = $accessToken->getRefreshToken();
                $data['token_expire_time'] = $accessToken->getExpires();
                $data['token_is_expired'] = $accessToken->hasExpired() ? true : false;                
                // Using the access token, we may look up details about the
                // resource owner.
                
                
                $resourceOwner = $provider->getResourceOwner($accessToken);     

                $this->laravelLogin($resourceOwner->toArray());           

                $data['resource_owner'] = $resourceOwner->toArray();      
                $data['sessionIndex'] = md5($data['resource_owner']['email']); //this is not relevant to OAuth2
                $data['oauth'] = true;

                return view('home-oauth', $data);
            
            } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {

                // Failed to get the access token or user details.
                exit($e->getMessage());

            }

        }
    }
}
