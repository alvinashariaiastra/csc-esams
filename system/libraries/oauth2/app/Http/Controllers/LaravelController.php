<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LaravelController extends Controller
{
    public function test(){
    	Log::info(\Carbon\Carbon::now(). ' - Laravel has been fired!');
    	//Log::info(\Request::url());
    	$response['foo'] = "bar";
    	$response['bar'] = "foo";
    	$response['status'] = 200;
    	return $response;
    }

    public function showDashboard(Request $request){    	        
    	$user = \Auth::user();

    	if($user == null){
    		return redirect('/');
    	}else{
    		$data['username'] = $user->username;
	    	$data['sessionIndex'] = $user->sessionIndex;
	    	$data['attributes'] = json_decode($user->attributes);
	    	return view('home', $data);
    	}    	
    }    
}
