<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|				 NOTE: For MySQL and MySQLi databases, this setting is only used
| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
|				 (and in table creation queries made with DB Forge).
| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
| 				 can make your site vulnerable to SQL injection if you are using a
| 				 multi-byte character set and are running versions lower than these.
| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group = 'default';
$active_record = TRUE;

// $db['default']['hostname'] = '10.248.141.132';
// $db['default']['username'] = 'csc';
// $db['default']['password'] = 'nasigoreng';
// $db['default']['database'] = 'csc-assist-dev';
// $db['default']['dbdriver'] = 'mysqli';
// $db['default']['dbprefix'] = '';
// $db['default']['pconnect'] = FALSE;
// $db['default']['db_debug'] = TRUE;
// $db['default']['cache_on'] = FALSE;
// $db['default']['cachedir'] = '';
// $db['default']['char_set'] = 'utf8';
// $db['default']['dbcollat'] = 'utf8_general_ci';
// $db['default']['swap_pre'] = '';
// $db['default']['autoinit'] = TRUE;
// $db['default']['stricton'] = FALSE;

$db['default'] = array(
	'dsn'	=> '',
	'hostname' => isset($_SERVER['CI_DB_HOSTNAME'])?$_SERVER['CI_DB_HOSTNAME']:'10.248.141.132',
	'username' => isset($_SERVER['CI_DB_USERNAME'])?$_SERVER['CI_DB_USERNAME']:'csc',
	'password' => isset($_SERVER['CI_DB_PASSWORD'])?$_SERVER['CI_DB_PASSWORD']:'nasigoreng',
	'database' => isset($_SERVER['CI_DB_DATABASE'])?$_SERVER['CI_DB_DATABASE']:'cscprod',
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);

$db['mysqlserver'] = array(
	'dsn' => '',
	'hostname' => '10.1.20.4',
	'port' => '3306',
	'username' => 'cist',
	'password' => 'CIST2019!',
	'database' => 'ms_log',
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);

$db['sqlsrvserver'] = array(
	'dsn' => '',
	'hostname' => '10.1.20.4',
	'port' => '1433',
	'username' => 'SA',
	'password' => 'P@ssw0rd',
	'database' => 'MS_LOG',
	'dbdriver' => 'sqlsrv',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);
// echo '<pre>';
// print_r($db['default']);
// echo '</pre>';

// echo 'connecting to database: ' .$db['default']['database']. 'using password: '.$db['default']['password'] ;
// $con=mysqli_connect('10.248.140.123', 'root', 'devxapps07@2017!', 'csc2')
// or die('cannot connect to the database because: ' . mysqli_connect_errno());
// if(mysqli_connect_errno()){echo 'Failed to connect to MySQL: ' . mysqli_connect_error();}
// if(!mysqli_query($con,"select * from px_user limit 10;")){echo('Error description: ' . mysqli_error($con));}
// mysqli_close($con);
// // $dbh=mysqli_connect($db['default']['hostname'],$db['default']['username'],$db['default']['password'],$db['default']['database'])
// // or die('cannot connect to the database because: ' . mysqli_error($dbh));
// // mysqli_select_db ($db['default']['database']);

// echo '<br />   connected ok:'  ;
// die( 'file: ' .__file__ . ' line: ' .__line__); 
/* End of file database.php */
/* Location: ./application/config/database.php */
