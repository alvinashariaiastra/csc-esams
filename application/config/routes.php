<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = 'user';
$route['404_override'] = '';

/* API */
// Sample
$route[':GET:api/sample/get_success'] = 'api/sample/get_success';
$route[':GET:api/sample/get_error'] = 'api/sample/get_error';
$route[':GET:api/sample/get_notfound'] = 'api/sample/get_notfound';
$route[':GET:api/sample/get_forbidden'] = 'api/sample/get_forbidden';
$route[':GET:api/sample/get_unauthorized'] = 'api/sample/get_unauthorized';
$route[':POST:api/sample/post_success'] = 'api/sample/post_success';
$route[':POST:api/sample/post_error'] = 'api/sample/post_error';
$route[':POST:api/sample/post_notfound'] = 'api/sample/post_notfound';
$route[':POST:api/sample/post_forbidden'] = 'api/sample/post_forbidden';
$route[':POST:api/sample/post_unauthorized'] = 'api/sample/post_unauthorized';
// Authentication
$route[':POST:api/esams/oauth/login'] = 'api/esams/oauth/login';
$route[':GET:api/esams/oauth/logout'] = 'api/esams/oauth/logout';
$route[':GET:api/esams/oauth/userinfo'] = 'api/esams/oauth/userinfo';
$route[':POST:api/esams/oauth/refresh'] = 'api/esams/oauth/refresh';
$route[':GET:api/esams/oauth/check'] = 'api/esams/oauth/check';
$route[':GET:api/esams/oauth/userinfo_testing'] = 'api/esams/oauth/userinfo_testing';
$route[':GET:api/esams/oauth/check_testing'] = 'api/esams/oauth/check_testing';
// Assignment
$route[':GET:api/esams/assignment/task_list'] = 'api/esams/assignment/task_list';
$route[':GET:api/esams/assignment/task_detail_list'] = 'api/esams/assignment/task_detail_list';
$route[':GET:api/esams/assignment/task_detail'] = 'api/esams/assignment/task_detail';
$route[':POST:api/esams/assignment/task_checkin'] = 'api/esams/assignment/task_checkin';
$route[':POST:api/esams/assignment/task_submit'] = 'api/esams/assignment/task_submit';
$route[':GET:api/esams/assignment/problem_list'] = 'api/esams/assignment/problem_list';
$route[':GET:api/esams/assignment/problem_detail'] = 'api/esams/assignment/problem_detail';
$route[':POST:api/esams/assignment/problem_submit'] = 'api/esams/assignment/problem_submit';
// DPS
$route[':GET:api/esams/dps/discover'] = 'api/esams/dps/discover';
$route[':GET:api/esams/dps/status_patrol'] = 'api/esams/dps/status_patrol';
$route[':POST:api/esams/dps/verify_cctv'] = 'api/esams/dps/verify_cctv';
$route[':POST:api/esams/dps/verify_camera'] = 'api/esams/dps/verify_camera';
$route[':GET:api/esams/dps/repost_checklist'] = 'api/esams/dps/repost_checklist';
$route[':POST:api/esams/dps/repost_checklist_checkpoint'] = 'api/esams/dps/repost_checklist_checkpoint';
$route[':POST:api/esams/dps/report_submit'] = 'api/esams/dps/report_submit';
// Issue
$route[':GET:api/esams/issue/report_type'] = 'api/esams/issue/report_type';
$route[':GET:api/esams/issue/location'] = 'api/esams/issue/location';
$route[':GET:api/esams/issue/building'] = 'api/esams/issue/building';
$route[':GET:api/esams/issue/building_detail'] = 'api/esams/issue/building_detail';
$route[':POST:api/esams/issue/submit'] = 'api/esams/issue/submit';
// Profile
$route[':GET:api/esams/profile/detail'] = 'api/esams/profile/detail';


/* End of file routes.php */
/* Location: ./application/config/routes.php */