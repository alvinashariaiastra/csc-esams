<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_system extends PX_Controller {

    public function __construct() {
        parent::__construct();
        $this->check_login();
        $this->controller_attr = array('controller' => 'admin_system', 'controller_name' => 'Admin System', 'controller_id' => 0);
        /* 
            Add by: Alvin (atsalvin0017), Date: 2019-08-13 11:07 AM 
            to handle error mycrypt deprecated for php version 7
        */
        if (PHP_VERSION >= 7) {
            ini_set('display_errors', 0);
            error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);
        }
    }

    public function index() {
        
    }

    function my_profile() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin My Profile', 'my_profile');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);
        $data['data'] = $this->session_admin;
        $data['content'] = $this->load->view('backend/admin_system/my_profile', $data, true);
        $this->load->view('backend/index', $data);
    }

    function my_profile_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin My Profile', 'my_profile');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);
        $table_field = $this->db->list_fields($this->tbl_admin);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }
        unset($update['id_usergroup']);
        $foto = $this->input->post('photo');
        $old_foto = $this->input->post('old_photo');
        $img_name_crop = uniqid().'-adminprofile.jpg';
        $update['date_modified'] = date('Y-m-d H:i:s', now());
        $password_check = $this->password_checker($this->input->post('password'));
        if($password_check->status == TRUE)
        {
            $update['password'] = $this->encrypt->encode($this->input->post('password'));

            if (!is_dir(FCPATH . "assets/uploads/admin/" . $update['id']))
                mkdir(FCPATH . "assets/uploads/admin/" . $update['id']);
            if (($foto && (basename($foto) != $old_foto)) || ($this->input->post('x') || $this->input->post('y') || $this->input->post('w') || $this->input->post('h')))
                $update['photo'] = $img_name_crop;
            else
                $update['photo'] = $this->input->post('old_photo');

            if ($update['realname'] && $update['email'] && $update['username'] && $this->input->post('password') && ($this->input->post('password') == $this->input->post('c-password'))) {
                $do_update = $this->model_basic->update($this->tbl_admin, $update, 'id', $update['id']);
                if ($do_update) {
                    if(($foto && (basename($foto) != $old_foto)) || ($this->input->post('x') || $this->input->post('y') || $this->input->post('w') || $this->input->post('h')))
                    {
                            $origw = $this->input->post('origwidth');
                            $origh = $this->input->post('origheight');
                            $fakew = $this->input->post('fakewidth');
                            $fakeh = $this->input->post('fakeheight');
                            $x = $this->input->post('x') * $origw / $fakew;
                            $y = $this->input->post('y') * $origh / $fakeh;
                            # ambil width crop
                            $targ_w = $this->input->post('w') * $origw / $fakew;
                            # abmil heigth crop
                            $targ_h = $this->input->post('h') * $origh / $fakeh;
                            # rasio gambar crop
                            $jpeg_quality = 100;
                            if(!is_dir(FCPATH . 'assets/uploads/admin/'.$update['id']))
                                    mkdir(FCPATH . 'assets/uploads/admin/'.$update['id']);

                            if(basename($foto) && $foto != null)
                                    $src = $this->input->post('photo');
                            else if($this->input->post('x')||$this->input->post('y')||$this->input->post('w')||$this->input->post('h'))
                                    $src = "assets/uploads/admin/".$update['id'].'/'.$old_foto;
                            # inisial handle copy gambar
                            $ext = pathinfo($src, PATHINFO_EXTENSION);

                            if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'JPG' || $ext == 'JPEG')
                                    $img_r = imagecreatefromjpeg($src);
                            if($ext == 'png' || $ext == 'PNG')
                                    $img_r = imagecreatefrompng($src);
                            if($ext == 'gif' || $ext == 'GIF')
                                    $img_r = imagecreatefromgif($src);

                            $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
                            # simpan hasil croping pada folder lain
                            $path_img_crop = realpath(FCPATH . "assets/uploads/admin/".$update['id']);
                            # nama gambar yg di crop
                            # proses copy
                            imagecopyresampled($dst_r,$img_r,0,0,$x,$y,$targ_w,$targ_h,$targ_w,$targ_h);
                            # buat gambar
                            if(imagejpeg($dst_r,$path_img_crop .'/'. $img_name_crop,$jpeg_quality))
                                    @unlink('assets/uploads/admin/'.$update['id'].'/'.$this->input->post('old_photo'));
                            $this->delete_temp('temp_folder');
                    }
                    $data_user = array(
                        'admin_id' => $update['id'],
                        'username' => $update['username'],
                        'password' => $this->input->post('password'),
                        'realname' => $update['realname'],
                        'email' => $update['email'],
                        'id_usergroup' => $this->session_admin['id_usergroup'],
                        'name_usergroup' => $this->session_admin['name_usergroup'],
                        'photo' => $update['photo'],
                        'date_modified' => $update['date_modified']
                    );
                    $this->session->set_userdata('admin', $data_user);
                    $this->save_log_admin(ACT_UPDATE, 'Update Profile');
                    $this->delete_temp('temp_folder');
                    $this->returnJson(array('status' => 'ok', 'msg' => 'Update success', 'redirect' => $data['controller'] . '/' . $data['function']));
                }
                else
                    $this->returnJson(array('status' => 'error', 'msg' => 'Failed when updating data'));
            }
            else
                $this->returnJson(array('status' => 'error', 'msg' => 'Please check the form'));
        }
        else
        {
            $this->returnJson(array('status' => 'error', 'msg' => $password_check->msg));
        }
    }

    function admin_user() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin User', 'admin_user');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);
        $data['data'] = $this->model_admin_user->get_all();
        $data['data_usergroup'] = $this->model_basic->select_all($this->tbl_usergroup);
        $data['content'] = $this->load->view('backend/admin_system/user', $data, true);
        $this->load->view('backend/index', $data);
    }

    function admin_user_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin User', 'admin_user');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);
        $table_field = $this->db->list_fields($this->tbl_admin);
        $insert = array();
        foreach ($table_field as $field) {
            // Add by: Alvin (atsalvin0017), Date: 2019-08-13 11:10 AM
            if ($field!='id') { 
                $insert[$field] = $this->input->post($field);
            }
            // $insert[$field] = $this->input->post($field);
        }
        $insert['date_modified'] = date('Y-m-d H:i:s', now());
        $insert['email_notification'] = $insert['email_notification']?1:0;
        $password_check = $this->password_checker($this->input->post('password'));
        if($password_check->status == TRUE)
        {
            $insert['password'] = $this->encrypt->encode($insert['password']);
            if ($insert['username'] && $insert['password'] && $insert['realname'] && $insert['email'] && $insert['id_usergroup']) {
                $do_insert = $this->model_basic->insert_all($this->tbl_admin, $insert);
                if ($do_insert)
                {
                    $this->save_log_admin(ACT_CREATE, 'Insert Admin User');
                    $this->returnJson(array('status' => 'ok', 'msg' => 'Input success', 'redirect' => $data['controller'] . '/' . $data['function']));
                }
                else
                    $this->returnJson(array('status' => 'error', 'msg' => 'Failed when saving data'));
            }
            else
                $this->returnJson(array('status' => 'error', 'msg' => 'Please check the form'));
        }
        else
            $this->returnJson(array('status' => 'error', 'msg' => $password_check->msg));
    }

    function admin_user_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin User', 'admin_user');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);
        $table_field = $this->db->list_fields($this->tbl_admin);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }
        $update['date_modified'] = date('Y-m-d H:i:s', now());
        $update['email_notification'] = $update['email_notification']?1:0;
        $password_check = $this->password_checker($this->input->post('password'));
        if($password_check->status == TRUE)
        {
            $update['password'] = $this->encrypt->encode($update['password']);
            if ($update['username'] && $update['password'] && $update['realname'] && $update['email'] && $update['id_usergroup']) {
                $do_update = $this->model_basic->update($this->tbl_admin, $update, 'id', $update['id']);
                if ($do_update)
                {
                    $this->save_log_admin(ACT_UPDATE, 'Update Admin User');
                    $this->returnJson(array('status' => 'ok', 'msg' => 'Update success', 'redirect' => $data['controller'] . '/' . $data['function']));
                }
                else
                    $this->returnJson(array('status' => 'error', 'msg' => 'Failed when updating data'));
            }
            else
                $this->returnJson(array('status' => 'error', 'msg' => 'Please check the form'));
        }
        else
            $this->returnJson(array('status' => 'error', 'msg' => $password_check->msg));
    }

    function admin_user_delete() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin User', 'admin_user');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $do_delete = $this->model_basic->delete_full($this->tbl_admin, 'id', $id);
        if ($do_delete)
        {
            $this->save_log_admin(ACT_DELETE, 'Delete Admin User');
            $this->returnJson(array('status' => 'ok', 'msg' => 'Delete success', 'redirect' => $data['controller'] . '/' . $data['function']));
        }
        else
            $this->returnJson(array('status' => 'error', 'msg' => 'Delete failed'));
    }

    function admin_user_get() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin User', 'admin_user');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);
        $id = $this->input->post('id');
        $data['row'] = $this->model_basic->select_where($this->tbl_admin, 'id', $id)->row();
        $data['row']->password = $this->encrypt->decode($data['row']->password);
        if ($data['row'])
            $this->returnJson(array('status' => 'ok', 'data' => $data));
        else
            $this->returnJson(array('status' => 'error', 'msg' => 'Data not found'));
    }

    function admin_user_check_email() {
        $id = $this->input->post('id');
        $email = $this->input->post('email');
        if ($id == null || $id == '') {
            $check = $this->model_basic->select_where($this->tbl_admin, 'email', $email)->num_rows();
            if ($check == 0)
                echo 'true';
            else
                echo 'false';
        }
        else {
            $now = $this->model_basic->select_where_array($this->tbl_admin, 'id != ' . $id . ' and email = "' . $email . '"')->num_rows();
            if ($now == 0)
                echo 'true';
            else {
                echo 'false';
            }
        }
    }

    function admin_user_check_username() {
        $id = $this->input->post('id');
        $username = $this->input->post('username');
        if ($id == null || $id == '') {
            $check = $this->model_basic->select_where($this->tbl_admin, 'username', $username)->num_rows();
            if ($check == 0)
                echo 'true';
            else
                echo 'false';
        }
        else {
            $now = $this->model_basic->select_where_array($this->tbl_admin, 'id != ' . $id . ' and username = "' . $username . '"')->num_rows();
            if ($now == 0)
                echo 'true';
            else {
                echo 'false';
            }
        }
    }

    function menu() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin Menu', 'menu');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);
        $data['data'] = $this->model_menu->get_all();
        $data['data_parent'] = $this->model_basic->select_where($this->tbl_menu, 'id_parent', 0)->result();
        $data['data_icon'] = $this->model_basic->select_where($this->tbl_master_data, 'id_parent', 1)->result();
        $data['content'] = $this->load->view('backend/admin_system/menu', $data, true);
        $this->load->view('backend/index', $data);
    }

    function menu_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin Menu', 'menu');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);
        $table_field = $this->db->list_fields($this->tbl_menu);
        $insert = array();
        foreach ($table_field as $field) {
            // Add by: Alvin (atsalvin0017), Date: 2019-08-12 13:53 PM
            if ($field!='id') { 
                $insert[$field] = $this->input->post($field);
            }
            // $insert[$field] = $this->input->post($field);
        }
        if ($insert['name'] && $insert['target'] && $insert['icon']) {
            $do_insert = $this->model_basic->insert_all($this->tbl_menu, $insert);
            if ($do_insert)
            {
                $this->save_log_admin(ACT_CREATE, 'Insert Menu');
                $this->returnJson(array('status' => 'ok', 'msg' => 'Input success', 'redirect' => $data['controller'] . '/' . $data['function']));
            }
            else
                $this->returnJson(array('status' => 'error', 'msg' => 'Failed when saving data'));
        }
        else
            $this->returnJson(array('status' => 'error', 'msg' => 'Please check the form'));
    }

    function menu_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin Menu', 'menu');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);
        $table_field = $this->db->list_fields($this->tbl_menu);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }
        if ($update['name'] && $update['target'] && $update['icon']) {
            $do_update = $this->model_basic->update($this->tbl_menu, $update, 'id', $update['id']);
            if ($do_update)
            {
                $this->save_log_admin(ACT_UPDATE, 'Update Menu');
                $this->returnJson(array('status' => 'ok', 'msg' => 'Update success', 'redirect' => $data['controller'] . '/' . $data['function']));
            }
            else
                $this->returnJson(array('status' => 'error', 'msg' => 'Failed when updating data'));
        }
        else
            $this->returnJson(array('status' => 'error', 'msg' => 'Please check the form'));
    }

    function menu_delete() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin Menu', 'menu');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $do_delete = $this->model_basic->delete_full($this->tbl_menu, 'id', $id);
        $do_delete_child = $this->model_basic->delete_full($this->tbl_menu, 'id_parent', $id);
        $do_delete_useraccess = $this->model_basic->delete_full($this->tbl_useraccess, 'id_menu', $id);
        if ($do_delete)
        {
            $this->save_log_admin(ACT_DELETE, 'Delete Menu');
            $this->returnJson(array('status' => 'ok', 'msg' => 'Delete success', 'redirect' => $data['controller'] . '/' . $data['function']));
        }
        else
            $this->returnJson(array('status' => 'error', 'msg' => 'Delete failed'));
    }

    function menu_get() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin Menu', 'menu');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);
        $id = $this->input->post('id');
        $data['row'] = $this->model_basic->select_where($this->tbl_menu, 'id', $id)->row();
        if ($data['row'])
            $this->returnJson(array('status' => 'ok', 'data' => $data));
        else
            $this->returnJson(array('status' => 'error', 'msg' => 'Data not found'));
    }

    function usergroup() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin Usergroup', 'usergroup');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);
        $data['data'] = $this->model_usergroup->get_all();
        $data['content'] = $this->load->view('backend/admin_system/usergroup', $data, true);
        $this->load->view('backend/index', $data);
    }

    function usergroup_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin Usergroup', 'usergroup');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);
        $table_field = $this->db->list_fields($this->tbl_usergroup);
        $insert = array();
        foreach ($table_field as $field) {
            // Add by: Alvin (atsalvin0017), Date: 2019-08-13 10:57 AM
            if ($field!='id') { 
                $insert[$field] = $this->input->post($field);
            }
            // $insert[$field] = $this->input->post($field);
        }
        if ($insert['usergroup_name']) {
            $do_insert = $this->model_basic->insert_all($this->tbl_usergroup, $insert);
            if ($do_insert)
            {
                $this->save_log_admin(ACT_CREATE, 'Insert Usergroup');
                $this->returnJson(array('status' => 'ok', 'msg' => 'Input success', 'redirect' => $data['controller'] . '/' . $data['function']));
            }
            else
                $this->returnJson(array('status' => 'error', 'msg' => 'Failed when saving data'));
        }
        else
            $this->returnJson(array('status' => 'error', 'msg' => 'Please check the form'));
    }

    function usergroup_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin Usergroup', 'usergroup');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);
        $table_field = $this->db->list_fields($this->tbl_usergroup);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }
        if ($update['usergroup_name']) {
            $do_update = $this->model_basic->update($this->tbl_usergroup, $update, 'id', $update['id']);
            if ($do_update)
            {
                $this->save_log_admin(ACT_UPDATE, 'Update Usergroup');
                $this->returnJson(array('status' => 'ok', 'msg' => 'Update success', 'redirect' => $data['controller'] . '/' . $data['function']));
            }
            else
                $this->returnJson(array('status' => 'error', 'msg' => 'Failed when updating data'));
        }
        else
            $this->returnJson(array('status' => 'error', 'msg' => 'Please check the form'));
    }

    function usergroup_delete() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin Usergroup', 'usergroup');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $do_delete = $this->model_basic->delete_full($this->tbl_usergroup, 'id', $id);
        $do_delete_useraccess = $this->model_basic->delete_full($this->tbl_useraccess, 'id_usergroup', $id);
        if ($do_delete)
        {
            $this->save_log_admin(ACT_DELETE, 'Delete Usergroup');
            $this->returnJson(array('status' => 'ok', 'msg' => 'Delete success', 'redirect' => $data['controller'] . '/' . $data['function']));
        }
        else
            $this->returnJson(array('status' => 'error', 'msg' => 'Delete failed'));
    }

    function usergroup_get() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin Usergroup', 'usergroup');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);
        $id = $this->input->post('id');
        $data['row'] = $this->model_basic->select_where($this->tbl_usergroup, 'id', $id)->row();
        if ($data['row'])
            $this->returnJson(array('status' => 'ok', 'data' => $data));
        else
            $this->returnJson(array('status' => 'error', 'msg' => 'Data not found'));
    }

    function useraccess() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin Useraccess', 'useraccess');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);
        $data['data'] = $this->model_useraccess->get_all();
        $data['data_menu'] = $this->get_all_menu();
        $data['data_available_user'] = $this->model_useraccess->get_available_user();
        $data['content'] = $this->load->view('backend/admin_system/useraccess', $data, true);
        $this->load->view('backend/index', $data);
    }

    function useraccess_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin Useraccess', 'useraccess');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        $id_menu = $this->input->post('id_menu');
        $act_read = $this->input->post('act_read');
        $act_create = $this->input->post('act_create');
        $act_update = $this->input->post('act_update');
        $act_delete = $this->input->post('act_delete');

        if ($this->input->post('id_usergroup')) {
            $error = 0;
            foreach ($id_menu as $mi) {
                $data_insert = array(
                    'id_usergroup' => $this->input->post('id_usergroup'),
                    'id_menu' => $mi,
                    'act_create' => $act_create[$mi],
                    'act_read' => $act_read[$mi],
                    'act_update' => $act_update[$mi],
                    'act_delete' => $act_delete[$mi],
                    'delete_flag' => 0 // Add by: Alvin (atsalvin0017), Date: 2019-08-12 13:53 PM
                );
                $insert = $this->model_basic->insert_all($this->tbl_useraccess, $data_insert);
                if (!$insert)
                    $error++;
            }
            if ($error == 0)
            {
                $this->save_log_admin(ACT_CREATE, 'Insert Useraccess');
                $this->returnJson(array('status' => 'ok', 'msg' => 'Input success', 'redirect' => $data['controller'] . '/' . $data['function']));
            }
            else
                $this->returnJson(array('status' => 'error', 'msg' => 'Failed when saving data'));
        }
        else
            $this->returnJson(array('status' => 'error', 'msg' => 'Please check the form'));
    }

    function useraccess_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin Useraccess', 'useraccess');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);

        $id = $this->input->post('id');
        $id_menu = $this->input->post('id_menu');
        $id_useraccess = $this->input->post('id_useraccess');
        $act_read = $this->input->post('act_read');
        $act_create = $this->input->post('act_create');
        $act_update = $this->input->post('act_update');
        $act_delete = $this->input->post('act_delete');

        if ($id) {
            $error = 0;
            foreach ($id_menu as $mi) {
                if (isset($id_useraccess[$mi])) {
                    $data_update = array(
                        'act_create' => $act_create[$mi],
                        'act_read' => $act_read[$mi],
                        'act_update' => $act_update[$mi],
                        'act_delete' => $act_delete[$mi],
                        'delete_flag' => 0, // Add by: Alvin (atsalvin0017), Date: 2019-08-12 10:39 AM
                    );
                    $update = $this->model_basic->update($this->tbl_useraccess, $data_update, 'id', $id_useraccess[$mi]);
                    if (!$update)
                        $error++;
                }
                else {
                    $data_insert = array(
                        'id_usergroup' => $id,
                        'id_menu' => $mi,
                        'act_read' => $act_read[$mi],
                        'act_create' => $act_create[$mi],
                        'act_update' => $act_update[$mi],
                        'act_delete' => $act_delete[$mi],
                        'delete_flag' => 0, // Add by: Alvin (atsalvin0017), Date: 2019-08-12 10:39 AM
                    );
                    $insert = $this->model_basic->insert_all($this->tbl_useraccess, $data_insert);
                    if (!$insert)
                        $error++;
                }
            }
            if ($error == 0)
            {
                $this->save_log_admin(ACT_UPDATE, 'Update Useraccess');
                $this->returnJson(array('status' => 'ok', 'msg' => 'Update success', 'redirect' => $data['controller'] . '/' . $data['function']));
            }
            else
                $this->returnJson(array('status' => 'error', 'msg' => 'Failed when updating data'));
        }
        else
            $this->returnJson(array('status' => 'error', 'msg' => 'Please check the form'));
    }

    function useraccess_delete() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin Useraccess', 'useraccess');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $do_delete = $this->model_basic->delete_full($this->tbl_useraccess, 'id_usergroup', $id);
        if ($do_delete)
        {
            $this->save_log_admin(ACT_DELETE, 'Delete Useraccess');
            $this->returnJson(array('status' => 'ok', 'msg' => 'Delete success', 'redirect' => $data['controller'] . '/' . $data['function']));
        }
        else
            $this->returnJson(array('status' => 'error', 'msg' => 'Delete failed'));
    }

    function master_data($id_parent = 0) {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        if ($id_parent == 0)
            $data += $this->get_function('Admin Master Data', 'master_data');
        else {
            $master_parent = $this->model_basic->select_where($this->tbl_master_data, 'id', $id_parent)->row();
            $data += $this->get_function('Admin Master Data ' . $master_parent->content, 'master_data');
        }
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);
        $data['id_parent'] = $id_parent;
        $data['data'] = $this->model_master->get_all($id_parent);
        $data['content'] = $this->load->view('backend/admin_system/master_data', $data, true);
        $this->load->view('backend/index', $data);
    }

    function master_data_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin Master Data', 'master_data');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);
        $table_field = $this->db->list_fields($this->tbl_master_data);
        $insert = array();
        foreach ($table_field as $field) {
            $insert[$field] = $this->input->post($field);
        }
        if ($insert['content']) {
            $do_insert = $this->model_basic->insert_all($this->tbl_master_data, $insert);
            if ($do_insert) {
                if ($insert['id_parent'])
                    $redirect = $data['controller'] . '/' . $data['function'] . '/' . $insert['id_parent'];
                else
                    $redirect = $data['controller'] . '/' . $data['function'];
                $this->returnJson(array('status' => 'ok', 'msg' => 'Input success', 'redirect' => $redirect));
            }
            else
                $this->returnJson(array('status' => 'error', 'msg' => 'Failed when saving data'));
        }
        else
            $this->returnJson(array('status' => 'error', 'msg' => 'Please check the form'));
    }

    function master_data_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin Master Data', 'master_data');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);
        $table_field = $this->db->list_fields($this->tbl_master_data);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }
        if ($update['content']) {
            $do_update = $this->model_basic->update($this->tbl_master_data, $update, 'id', $update['id']);
            if ($do_update) {
                if ($update['id_parent'])
                    $redirect = $data['controller'] . '/' . $data['function'] . '/' . $update['id_parent'];
                else
                    $redirect = $data['controller'] . '/' . $data['function'];
                $this->returnJson(array('status' => 'ok', 'msg' => 'Update success', 'redirect' => $redirect));
            }
            else
                $this->returnJson(array('status' => 'error', 'msg' => 'Failed when updating data'));
        }
        else
            $this->returnJson(array('status' => 'error', 'msg' => 'Please check the form'));
    }

    function master_data_delete() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin Master Data', 'master_data');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $deleted_data = $this->model_basic->select_where($this->tbl_master_data, 'id', $id)->row();
        $do_delete = $this->model_basic->delete_full($this->tbl_master_data, 'id', $id);
        $do_delete_child = $this->model_basic->delete_full($this->tbl_master_data, 'id_parent', $id);
        if ($do_delete) {
            if ($deleted_data->id_parent > 0)
                $redirect = $data['controller'] . '/' . $data['function'] . '/' . $deleted_data->id_parent;
            else
                $redirect = $data['controller'] . '/' . $data['function'];
            $this->returnJson(array('status' => 'ok', 'msg' => 'Delete success', 'redirect' => $redirect));
        }
        else
            $this->returnJson(array('status' => 'error', 'msg' => 'Delete failed'));
    }

    function master_data_get() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin Master Data', 'master_data');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);
        $id = $this->input->post('id');
        $data['row'] = $this->model_basic->select_where($this->tbl_master_data, 'id', $id)->row();
        if ($data['row'])
            $this->returnJson(array('status' => 'ok', 'data' => $data));
        else
            $this->returnJson(array('status' => 'error', 'msg' => 'Data not found'));
    }

    function useraccess_get() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin Useraccess', 'useraccess');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);
        $id = $this->input->post('id');
        $data['row'] = $this->model_basic->select_where($this->tbl_useraccess, 'id_usergroup', $id)->result();
        if ($data['row'])
            $this->returnJson(array('status' => 'ok', 'data' => $data));
        else
            $this->returnJson(array('status' => 'error', 'msg' => 'Data not found'));
    }

    function settings() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin Settings', 'settings');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);
        $data['data'] = $this->model_usergroup->get_all();
        $data['content'] = $this->load->view('backend/admin_system/settings', $data, true);
        $this->load->view('backend/index', $data);
    }

    function settings_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin Settings', 'settings');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);
        $table_field = $this->db->list_fields($this->tbl_adm_config);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }
        if ($update['login_logo']) {
            if (!@copy($update['login_logo'], 'assets/uploads/app_settings/' . basename($update['login_logo'])))
                $update['login_logo'] = $this->input->post('old_login_logo');
            else
                $update['login_logo'] = basename($update['login_logo']);
        }
        else
            $update['login_logo'] = $this->input->post('old_login_logo');

        if ($update['mini_logo']) {
            if (!@copy($update['mini_logo'], 'assets/uploads/app_settings/' . basename($update['mini_logo'])))
                $update['mini_logo'] = $this->input->post('old_mini_logo');
            else
                $update['mini_logo'] = basename($update['mini_logo']);
        }
        else
            $update['mini_logo'] = $this->input->post('old_mini_logo');

        if ($update['single_logo']) {
            if (!@copy($update['single_logo'], 'assets/uploads/app_settings/' . basename($update['single_logo'])))
                $update['single_logo'] = $this->input->post('old_single_logo');
            else
                $update['single_logo'] = basename($update['single_logo']);
        }
        else
            $update['single_logo'] = $this->input->post('old_single_logo');

        if ($update['favicon_logo']) {
            if (!@copy($update['favicon_logo'], 'assets/uploads/app_settings/' . basename($update['favicon_logo'])))
                $update['favicon_logo'] = $this->input->post('old_favicon_logo');
            else
                $update['favicon_logo'] = basename($update['favicon_logo']);
        }
        else
            $update['favicon_logo'] = $this->input->post('old_favicon_logo');

        if ($update['title'] && $update['desc']) {
            $do_update = $this->model_basic->update($this->tbl_adm_config, $update, 'id', $update['id']);
            if ($do_update) {
                $this->delete_temp('temp_folder');
                $this->save_log_admin(ACT_UPDATE, 'Update Settings');
                $this->returnJson(array('status' => 'ok', 'msg' => 'Update success', 'redirect' => $data['controller'] . '/' . $data['function']));
            }
            else
                $this->returnJson(array('status' => 'error', 'msg' => 'Failed when updating data'));
        }
        else
            $this->returnJson(array('status' => 'error', 'msg' => 'Please check the form'));
    }

    function menu_orders() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin Menu Orders', 'menu_orders');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);
        $data['data'] = $this->get_all_menu();
        $data['content'] = $this->load->view('backend/admin_system/menu_orders', $data, true);
        $this->load->view('backend/index', $data);
    }

    function menu_orders_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin Menu Orders', 'menu_orders');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);

        $item = $this->input->post('item');

        if (count($item) > 0) {
            $orders = 1;
            $error = 0;
            foreach ($item as $menu_id) {
                $update['orders'] = $orders;
                if (!$this->model_basic->update($this->tbl_menu, $update, 'id', $menu_id))
                    $error++;
                $orders++;
            }
            if ($error < 1)
            {
                $this->save_log_admin(ACT_UPDATE, 'Update Menu Orders');
                $this->returnJson(array('status' => 'ok', 'msg' => 'Update success'));
            }
            else
                $this->returnJson(array('status' => 'error', 'msg' => 'Failed when updating data'));
        }
        else
            $this->returnJson(array('status' => 'error', 'msg' => 'Please check the form'));
    }

    function maintain_notif_email(){
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Maintain Notif Email', 'maintain_notif_email');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);
        $data['data'] = $this->model_maintain_email->get_all();
        $data['data_user'] = $this->model_basic->select_all($this->tbl_admin);
        $data['content'] = $this->load->view('backend/admin_system/maintain_email', $data, true);
        $this->load->view('backend/index', $data);
    }

    function maintain_notif_email_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Maintain Notif Email', 'maintain_notif_email');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);
        $table_field = $this->db->list_fields($this->tbl_maintain_email);
        $insert = array();
        foreach ($table_field as $field) {
            $insert[$field] = $this->input->post($field);
        }
        //$insert['date_created'] = date('Y-m-d H:i:s', now());
        $insert['date_modified'] = date('Y-m-d H:i:s', now());

        //for get user data in maintain email
        $user_data = array();
        $user_data['email'] = $this->model_basic->select_where_multiple($this->tbl_maintain_email, 'admin_id', $insert['admin_id'], 'transaksi', $insert['transaksi']);
        
        if ($insert['transaksi'] && $insert['admin_id'] != "") 
        {
            if ($user_data['email'] == "tidak") 
            {
                $do_insert = $this->model_basic->insert_all($this->tbl_maintain_email, $insert);
                if ($do_insert)
                {
                    $this->save_log_admin(ACT_CREATE, 'Insert Maintain Email Data');
                    $this->returnJson(array('status' => 'ok', 'msg' => 'Input success', 'redirect' => $data['controller'] . '/' . $data['function']));
                }
                else
                    $this->returnJson(array('status' => 'error', 'msg' => 'Failed when saving data'));
            }
            else
            {
                $this->returnJson(array('status' => 'error', 'msg' => "User has been registered in this transaction"));
            }
        }
        else
            $this->returnJson(array('status' => 'error', 'msg' => 'Please check the form'));
    }

    function maintain_notif_email_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Maintain Notif Email', 'maintain_notif_email');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);
        $table_field = $this->db->list_fields($this->tbl_maintain_email);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }
        $update['date_modified'] = date('Y-m-d H:i:s', now());

        //for get user data in maintain email
        $user_data = array();
        $user_data['email'] = $this->model_basic->select_where_multiple($this->tbl_maintain_email, 'admin_id', $update['admin_id'], 'transaksi', $update['transaksi']);
        
        if ($update['transaksi'] && $update['admin_id'] != "") 
        {
            if ($user_data['email'] == "tidak") 
            {
                $do_update = $this->model_basic->update($this->tbl_maintain_email, $update, 'id', $update['id']);
                if ($do_update)
                {
                    $this->save_log_admin(ACT_UPDATE, 'Update Maintain Email Data');
                    $this->returnJson(array('status' => 'ok', 'msg' => 'Update success', 'redirect' => $data['controller'] . '/' . $data['function']));
                }
                else
                    $this->returnJson(array('status' => 'error', 'msg' => 'Failed when updating data'));
            }
            else
            {
                $this->returnJson(array('status' => 'error', 'msg' => "User has been registered in this transaction"));
            }
        }
        else
            $this->returnJson(array('status' => 'error', 'msg' => 'Please check the form'));
    }

    function maintain_notif_email_get() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Maintain Notif Email', 'maintain_notif_email');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);
        $id = $this->input->post('id');
        $data['row'] = $this->model_maintain_email->get_where($id)->row();
        //$data['row'] = $this->model_basic->select_where_all($this->tbl_maintain_email, 'id', $id)->row();
        //$data['row']->password = $this->encrypt->decode($data['row']->password);
        if ($data['row'])
            //$this->returnJson(array('status' => 'error', 'msg' => $data['row']));
            $this->returnJson(array('status' => 'ok', 'data' => $data));
        else
            $this->returnJson(array('status' => 'error', 'msg' => 'Data not found'));
    }

     function maintain_notif_email_delete() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Maintain Notif Email', 'maintain_notif_email');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $do_delete = $this->model_basic->delete_full($this->tbl_maintain_email, 'id', $id);
        if ($do_delete)
        {
            $this->save_log_admin(ACT_DELETE, 'Delete Maintain Email Data');
            $this->returnJson(array('status' => 'ok', 'msg' => 'Delete success', 'redirect' => $data['controller'] . '/' . $data['function']));
        }
        else
            $this->returnJson(array('status' => 'error', 'msg' => 'Delete failed'));
    }

}
