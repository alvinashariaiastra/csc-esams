<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_standar_kompetensi extends PX_Controller {

	public function __construct() {
		parent::__construct();
		$this->check_login();
		$this->controller_attr = array('controller' => 'admin_standar_kompetensi','controller_name' => 'Admin Standar Kompetensi','controller_id' => 0);
	}

	public function index()
	{
		$this->standar_kompetensi();
	}

	//kompetensi//
	function standar_kompetensi(){
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Standar Kompetensi','standar_kompetensi');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);

		$data['data'] = $this->model_basic->select_all($this->tbl_standar_kompetensi_user);
		$data['content'] = $this->load->view('backend/admin_standar_kompetensi/standar_kompetensi',$data,true);
		$this->load->view('backend/index',$data); 
	}

	function standar_kompetensi_form(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Standar Kompetensi','standar_kompetensi');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);
		$id = $this->input->post('id');
		$data['perusahaan'] = $this->model_basic->select_where($this->tbl_perusahaan,'delete_flag',0);

		//rules
		$data['stc1'] = $this->model_basic->select_where($this->tbl_standar_kompetensi,'stc_id',1);
		foreach ($data['stc1']->result() as $row) {
			$row->document = $this->model_basic->select_where_all($this->tbl_list_rules_file,'id_list_rules',$row->id);
		}
		$data['stc2'] = $this->model_basic->select_where($this->tbl_standar_kompetensi,'stc_id',2);
		foreach ($data['stc2']->result() as $row) {
			$row->document = $this->model_basic->select_where_all($this->tbl_list_rules_file,'id_list_rules',$row->id);
		}
		$data['stc3'] = $this->model_basic->select_where($this->tbl_standar_kompetensi,'stc_id',3);
		foreach ($data['stc3']->result() as $row) {
			$row->document = $this->model_basic->select_where_all($this->tbl_list_rules_file,'id_list_rules',$row->id);
		}
		$data['stc4'] = $this->model_basic->select_where($this->tbl_standar_kompetensi,'stc_id',4);
		foreach ($data['stc4']->result() as $row) {
			$row->document = $this->model_basic->select_where_all($this->tbl_list_rules_file,'id_list_rules',$row->id);
		}

		if($id){
			$data['data'] = $this->model_basic->select_where($this->tbl_standar_kompetensi_result,'id',$id)->row();
			$user = $this->model_basic->select_where_all($this->tbl_user,'id',$data['data']->user_id)->row();
			if($user){
				$data['data']->perusahaan_id = $user->perusahaan_id;
				$data['data']->instalasi_id = $user->instalasi_id;
			}

			$data['stc1'] = $this->model_basic->select_where_array($this->tbl_standar_kompetensi,'stc_id = 1 and level_user_id = '.$data['data']->level_user_id);
			foreach ($data['stc1']->result() as $row) {
				$row->document = $this->model_basic->select_where_all($this->tbl_list_rules_file,'id_list_rules',$row->id);
			}
			$data['stc2'] = $this->model_basic->select_where_array($this->tbl_standar_kompetensi,'stc_id = 2 and level_user_id = '.$data['data']->level_user_id);
			foreach ($data['stc2']->result() as $row) {
				$row->document = $this->model_basic->select_where_all($this->tbl_list_rules_file,'id_list_rules',$row->id);
			}
			$data['stc3'] = $this->model_basic->select_where_array($this->tbl_standar_kompetensi,'stc_id = 3 and level_user_id = '.$data['data']->level_user_id);
			foreach ($data['stc3']->result() as $row) {
				$row->document = $this->model_basic->select_where_all($this->tbl_list_rules_file,'id_list_rules',$row->id);
			}
			$data['stc4'] = $this->model_basic->select_where_array($this->tbl_standar_kompetensi,'stc_id = 4 and level_user_id = '.$data['data']->level_user_id);
			foreach ($data['stc4']->result() as $row) {
				$row->document = $this->model_basic->select_where_all($this->tbl_list_rules_file,'id_list_rules',$row->id);
			}
			// print_r($data['data']->user_id);
			$data['instalasi'] = $this->model_basic->select_where_array($this->tbl_instalasi,'delete_flag = 0 or delete_flag > 0');
			$data['user'] = $this->model_basic->select_where_array($this->tbl_user,'delete_flag = 0 or delete_flag > 0');
			//stc1
			$data['result_stc1'] = $this->model_standar_kompetensi->get_kompetensi_result(1,$id,$data['data']->level_user_id)->result_array();
			//stc2
			$data['result_stc2'] = $this->model_standar_kompetensi->get_kompetensi_result(2,$id,$data['data']->level_user_id)->result_array();
			//stc3
			$data['result_stc3'] = $this->model_standar_kompetensi->get_kompetensi_result(3,$id,$data['data']->level_user_id)->result_array();
			//stc4
			$data['result_stc4'] = $this->model_standar_kompetensi->get_kompetensi_result(4,$id,$data['data']->level_user_id)->result_array();
		}
		else{
			$data['data'] = null;
		}
		$data['content'] = $this->load->view('backend/admin_standar_kompetensi/standar_kompetensi_form',$data,true);
		$this->load->view('backend/index',$data); 
    }

    function standar_kompetensi_detail($id){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Standar Kompetensi','standar_kompetensi');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);
		//$id = $this->input->post('id');
		$data['instalasi'] = $this->model_basic->select_where($this->tbl_instalasi,'delete_flag',0);
		$data['user'] = $this->model_basic->select_where($this->tbl_user,'delete_flag',0);

		if($id){
			$data['data'] = $data_result = $this->model_basic->select_where($this->tbl_standar_kompetensi_result,'id',$id)->row();
			$user = $this->model_basic->select_where_all($this->tbl_user,'id',$data_result->user_id)->row();
			$data['nama'] = $user->realname;
			$user_jabatan = $this->model_basic->select_where_all($this->tbl_user_jabatan,'id',$user->jabatan_id)->row();
			$data['jabatan'] = $user_jabatan->jabatan;
			$data['instalasi'] = $this->model_basic->select_where_all($this->tbl_instalasi,'id',$user->instalasi_id)->row()->name;

			//stc1
			$data['result_stc1'] = $this->model_standar_kompetensi->get_kompetensi_result(1,$id,$data_result->level_user_id);
			//stc2
			$data['result_stc2'] = $this->model_standar_kompetensi->get_kompetensi_result(2,$id,$data_result->level_user_id);
			//stc3
			$data['result_stc3'] = $this->model_standar_kompetensi->get_kompetensi_result(3,$id,$data_result->level_user_id);
			//stc4
			$data['result_stc4'] = $this->model_standar_kompetensi->get_kompetensi_result(4,$id,$data_result->level_user_id);
		}
		else{
			$data['data'] = null;
		}
		$data['content'] = $this->load->view('backend/admin_standar_kompetensi/standar_kompetensi_detail',$data,true);
		$this->load->view('backend/index',$data); 
    }

    function standar_kompetensi_add(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Standar Kompetensi','standar_kompetensi');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);
		//standar kompetensi result
		$user_id = $this->input->post('user_id');
		$level_user_id = $this->model_basic->select_where($this->tbl_user,'id',$user_id)->row()->level_user_id;
                $this->db->trans_begin();
		$insert = array(
				'user_id' => $user_id,
				'level_user_id' => $level_user_id,
				'final_score' => $this->input->post('final_score'),
				'date_created' => date('Y-m-d H:i:s',now()),
				'id_created' => $this->session_admin['admin_id']
			);
		$standar_kompetensi_result_id = $this->model_basic->insert_all($this->tbl_standar_kompetensi_result,$insert)->id;
                $this->update_stc($user_id,$standar_kompetensi_result_id);
                $this->save_log_admin(ACT_CREATE, 'Insert New STC');
                if($this->db->trans_status() === TRUE)
                {
                    $this->db->trans_commit();
                    $this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => $data['controller'].'/'.$data['function']));
                }
                else
                {
                    $this->db->trans_rollback();
                    $this->returnJson(array('status' => 'error','msg' => 'Input data error'));
                }
		/*if($insert['name']){
			$do_insert = $this->model_basic->insert_all($this->tbl_perusahaan,$insert);
			if($do_insert){
				$this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => $data['controller'].'/'.$data['function']));
			}
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));*/
    }

    function standar_kompetensi_edit(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Standar Kompetensi','standar_kompetensi');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_UPDATE);
		$table_field = $this->db->list_fields($this->tbl_standar_kompetensi_result);
		$update = array();
		foreach ($table_field as $field) {
			$update[$field] = $this->input->post($field);
		}
		unset($update['id_created']);
		unset($update['date_created']);
		unset($update['level_user_id']);
		$update['date_modified'] = date('Y-m-d H:i:s',now());
		$update['id_modified'] = $this->session_admin['admin_id'];
		//die(print_r($update));
		if($update['user_id']){
                        $this->db->trans_begin();
			$do_update = $this->model_basic->update($this->tbl_standar_kompetensi_result,$update,'id',$update['id']);
			$this->update_stc($update['user_id'],$update['id']);

			if($do_update)
                        {
                                $this->save_log_admin(ACT_UPDATE, 'Update STC');
                                if($this->db->trans_status() === TRUE)
                                {
                                    $this->db->trans_commit();
                                    $this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => $data['controller'].'/'.$data['function']));
                                }
                                else
                                {
                                    $this->db->trans_rollback();
                                    $this->returnJson(array('status' => 'error','msg' => 'Input data error'));
                                }
                        }
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when updating data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function update_stc($user_id,$standar_kompetensi_result_id){
    	$this->model_standar_kompetensi->delete_standar_kompetensi_user($user_id,$standar_kompetensi_result_id);
    	$stc_edit = $this->model_basic->select_where($this->tbl_standar_kompetensi_result,'id',$standar_kompetensi_result_id)->row();
    	//die(print_r($this->db->last_query()));
		//stc1
		$insert = array();
		$stc1 = $this->model_basic->select_where_array($this->tbl_standar_kompetensi,'stc_id = 1 and level_user_id = '.$stc_edit->level_user_id);
		$i = 1;
		if ($stc1->num_rows() > 0) {
			foreach ($stc1->result() as $row) {
				$insert['user_id'] = $user_id;
				$insert['standar_kompetensi_id'] = $row->id;
				$insert['standar_kompetensi_result_id'] = $standar_kompetensi_result_id;
				$insert['score'] = $this->input->post('stc1_score'.$i);
				$insert['date_created'] = date('Y-m-d H:i:s',now());
				$insert['id_created'] = $this->session_admin['admin_id'];

				$this->model_basic->insert_all($this->tbl_standar_kompetensi_user,$insert);
				$i++;
			}	
		}

		//stc2
		$insert = array();
		$stc2 = $this->model_basic->select_where_array($this->tbl_standar_kompetensi,'stc_id = 2 and level_user_id = '.$stc_edit->level_user_id);
		$i = 1;
		if ($stc2->num_rows() > 0) {
			foreach ($stc2->result() as $row) {
				$insert['user_id'] = $user_id;
				$insert['standar_kompetensi_id'] = $row->id;
				$insert['standar_kompetensi_result_id'] = $standar_kompetensi_result_id;
				$insert['score'] = $this->input->post('stc2_score'.$i);
				$insert['date_created'] = date('Y-m-d H:i:s',now());
				$insert['id_created'] = $this->session_admin['admin_id'];

				$this->model_basic->insert_all($this->tbl_standar_kompetensi_user,$insert);
				$i++;
			}	
		}
		//stc3
		$insert = array();
		$stc3 = $this->model_basic->select_where_array($this->tbl_standar_kompetensi,'stc_id = 3 and level_user_id = '.$stc_edit->level_user_id);
		$i = 1;
		if ($stc3->num_rows() > 0) {
			foreach ($stc3->result() as $row) {
				$insert['user_id'] = $user_id;
				$insert['standar_kompetensi_id'] = $row->id;
				$insert['standar_kompetensi_result_id'] = $standar_kompetensi_result_id;
				$insert['score'] = $this->input->post('stc3_score'.$i);
				$insert['date_created'] = date('Y-m-d H:i:s',now());
				$insert['id_created'] = $this->session_admin['admin_id'];

				$this->model_basic->insert_all($this->tbl_standar_kompetensi_user,$insert);
				$i++;
			}	
		}
		//stc4
		$insert = array();
		$stc4 = $this->model_basic->select_where_array($this->tbl_standar_kompetensi,'stc_id = 4 and level_user_id = '.$stc_edit->level_user_id);
		$i = 1;
		if ($stc4->num_rows() > 0) {
			foreach ($stc4->result() as $row) {
				$insert['user_id'] = $user_id;
				$insert['standar_kompetensi_id'] = $row->id;
				$insert['standar_kompetensi_result_id'] = $standar_kompetensi_result_id;
				$insert['score'] = $this->input->post('stc4_score'.$i);
				$insert['date_created'] = date('Y-m-d H:i:s',now());
				$insert['id_created'] = $this->session_admin['admin_id'];

				$this->model_basic->insert_all($this->tbl_standar_kompetensi_user,$insert);
				$i++;
			}	
		}
    }

    function standar_kompetensi_delete(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Standar Kompetensi','standar_kompetensi');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_DELETE);
		$id = $this->input->post('id');
		$do_delete = $this->model_basic->delete($this->tbl_standar_kompetensi_result,'id',$id);
		if($do_delete){
                        $this->save_log_admin(ACT_DELETE, 'Delete STC');
			$this->delete_folder('standar_kompetensi/'.$id);
			$this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => $data['controller'].'/'.$data['function']));
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }

    public function ajax_standar_kompetensi_list(){
		$list = $this->model_standar_kompetensi->get_datatables();
		//die(print_r($this->db->last_query()));
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $data_row) {
			if($data_row->level_user_id == 1)
				$level = 'Management';
			elseif($data_row->level_user_id == 2)
				$level = 'Middle Management';
			else
				$level = 'Operator';
			//render data
			$no++;
			
			$row = array();
			$row[] = $no;
			$row[] = $data_row->realname;
			$row[] = $level;
			$row[] = $data_row->name;
			$row[] = $data_row->final_score;
			//add html for action
			$row[] = '<div class="text-center">
						<form action="admin_standar_kompetensi/standar_kompetensi_form" method="post">
						<a href="admin_standar_kompetensi/standar_kompetensi_detail/'.$data_row->id.'"><span class="btn btn-success btn-xs btn-edit" data-original-title="Detail" data-placement="top" data-toggle="tooltip"><i class="fa fa-eye"></i></span></a>
						<input type="hidden" name="id" value="'.$data_row->id.'">
						<button class="btn btn-info btn-xs btn-edit" type="submit" data-original-title="Edit" data-placement="top" data-toggle="tooltip"><i class="fa fa-edit"></i></button>
						<button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Delete" data-placement="top" data-toggle="tooltip" data-target-id="'.$data_row->id.'"><i class="fa fa-trash-o"></i></button>
						</form>
						</div>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_standar_kompetensi->count_all(),
						"recordsFiltered" => $this->model_standar_kompetensi->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}


	public function ajax_get_stc(){
		$level_id = $this->input->post('level_id');
		$all_stc = array();
		
		for ($i=1; $i < 5; $i++) {
			$data['stc'] = $this->model_basic->select_where_array($this->tbl_standar_kompetensi,'stc_id = '.$i.' and level_user_id = '.$level_id);
			foreach ($data['stc']->result() as $row) {
				$row->document = $this->model_basic->select_where_all($this->tbl_list_rules_file,'id_list_rules',$row->id);
			}
			$stc =  $this->load->view('backend/admin_standar_kompetensi/standar_kompetensi_rules_view',$data,true);
			$all_stc['stc'.$i] = $stc;
		}
		if ($data) {
			$this->returnJson(array('status' => 'ok', 'stc' => $all_stc));
		}else{
			$this->returnJson(array('status' => 'not_ok'));
		}
	}

	//list rules//
	function list_rules(){
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('List Rules','list_rules');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);

		$data['data'] = $this->model_basic->select_all($this->tbl_standar_kompetensi);
		$data['content'] = $this->load->view('backend/admin_standar_kompetensi/list_rules',$data,true);
		$this->load->view('backend/index',$data); 
	}

	function list_rules_form(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('List Rules','list_rules');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);
		$id = $this->input->post('id');
		$data['training'] = $this->model_basic->select_where($this->tbl_standar_kompetensi_training,'delete_flag',0);
		$data['user'] = $this->model_basic->select_where($this->tbl_user,'delete_flag',0);

		if($id){
			$data['data'] = $this->model_basic->select_where($this->tbl_standar_kompetensi,'id',$id)->row();
			$data['data']->document = $this->model_basic->select_where_all($this->tbl_list_rules_file,'id_list_rules',$id)->result();
		}
		else{
			$data['data'] = null;
		}
		$data['content'] = $this->load->view('backend/admin_standar_kompetensi/list_rules_form',$data,true);
		$this->load->view('backend/index',$data); 
    }

    function list_rules_add(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('List Rules','list_rules');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);

		$document = $this->input->post('document');
		$table_field = $this->db->list_fields($this->tbl_standar_kompetensi);
		$insert = array();
		foreach ($table_field as $field) {
			$insert[$field] = $this->input->post($field);
		}
		$insert['date_created'] = date('Y-m-d H:i:s',now());
		$insert['id_created'] = $this->session_admin['admin_id'];
		if($insert['rules']){
			$do_insert = $this->model_basic->insert_all($this->tbl_standar_kompetensi,$insert);
			if($do_insert){
				if(!is_dir(FCPATH . 'assets/uploads/list_rules/'.$do_insert->id))
					mkdir(FCPATH . 'assets/uploads/list_rules/'.$do_insert->id);

				if(count($document) > 0 && $document){
					foreach ($document as $document) {
						$insert_document['id_list_rules'] = $do_insert->id;
						$insert_document['file'] = basename($document);
						if(@copy($document,'assets/uploads/list_rules/'.$do_insert->id.'/'.$insert_document['file']))
							$do_insert_document = $this->model_basic->insert_all($this->tbl_list_rules_file,$insert_document);
					}
				}
                                $this->save_log_admin(ACT_CREATE, 'Insert New Rules STC '.$insert['rules']);
				$this->delete_temp('temp_folder');
				$this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => $data['controller'].'/'.$data['function']));
			}
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function list_rules_edit(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('List Rules','list_rules');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_UPDATE);
		
		$document = $this->input->post('document');
		$document_old_id = $this->input->post('document_old_id');
		$document_old_data = $this->input->post('document_old');
		$table_field = $this->db->list_fields($this->tbl_standar_kompetensi);
		$update = array();
		foreach ($table_field as $field) {
			$update[$field] = $this->input->post($field);
		}
		unset($update['date_created']);
		$update['date_modified'] = date('Y-m-d H:i:s',now());
		$update['id_modified'] = $this->session_admin['admin_id'];

		if(!is_dir(FCPATH . 'assets/uploads/list_rules/'.$update['id']))
			mkdir(FCPATH . 'assets/uploads/list_rules/'.$update['id']);

		// add new uploaded product file
		if(count($document) > 0 && $document){
			foreach ($document as $document) {
				$insert_document['id_list_rules'] = $update['id'];
				$insert_document['file'] = basename($document);
				if(@copy($document,'assets/uploads/list_rules/'.$update['id'].'/'.$insert_document['file']))
					$do_insert_document = $this->model_basic->insert_all($this->tbl_list_rules_file,$insert_document);
			}
		}

		// checking and deleting old file if its deleted
		if(count($document_old_id) > 0 && $document_old_id){
			foreach ($document_old_id as $bioi) {
				if(!isset($document_old_data[$bioi])){
					$deleted_data = $this->model_basic->select_where_all($this->tbl_list_rules_file,'id',$bioi)->row();
					if($deleted_data){
						@unlink('assets/uploads/list_rules/'.$update['id'].'/'.$deleted_data->file);
						$this->model_basic->delete_full($this->tbl_list_rules_file,'id',$bioi);
					}
				}
			}
		}

		if($update['rules']){
			$do_update = $this->model_basic->update($this->tbl_standar_kompetensi,$update,'id',$update['id']);
			if($do_update)
                        {
                                $this->save_log_admin(ACT_UPDATE, 'Update Rules STC '.$update['rules']);
				$this->returnJson(array('status' => 'ok','msg' => 'Update success','redirect' => $data['controller'].'/'.$data['function']));
                        }
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when updating data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function list_rules_delete(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('List Rules','list_rules');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_DELETE);
		$id = $this->input->post('id');
		$do_delete = $this->model_basic->delete($this->tbl_standar_kompetensi,'id',$id);
		if($do_delete){
                        $this->save_log_admin(ACT_DELETE, 'Delete Rules STC');
			$this->delete_folder('list_rules/'.$id);
			$this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => $data['controller'].'/'.$data['function']));
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }

    public function ajax_list_rules_list(){
		$column = array('id','date_created');
		$list = $this->model_table->get_datatables($this->tbl_standar_kompetensi,$column);
		//die(print_r($this->db->last_query()));
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $data_row) {
			//render data
			//level user id
			if ($data_row->level_user_id == 1) {
				$level_user_nm = 'Management';
			}elseif($data_row->level_user_id == 2) {
				$level_user_nm = 'Middle Management';
			}else{
				$level_user_nm = 'Operational';
			}
			//standar kompetensi
			if ($data_row->stc_id == 1) {
				$stc_nm = "STC-1";
			}elseif ($data_row->stc_id == 2) {
				$stc_nm = "STC-2";
			}elseif ($data_row->stc_id == 3) {
				$stc_nm = "STC-3";
			}else{
				$stc_nm = "STC-4";
			}
			$no++;
			
			$row = array();
			$row[] = $no;
			$row[] = $level_user_nm;
			$row[] = $stc_nm;
			$row[] = $data_row->rules;
			//add html for action
			$row[] = '<div class="text-center">
						<form action="admin_standar_kompetensi/list_rules_form" method="post">
						<input type="hidden" name="id" value="'.$data_row->id.'">
						<button class="btn btn-info btn-xs btn-edit" type="submit" data-original-title="Update" data-placement="top" data-toggle="tooltip"><i class="fa fa-edit"></i></button>
						<button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Delete" data-placement="top" data-toggle="tooltip" data-target-id="'.$data_row->id.'"><i class="fa fa-trash-o"></i></button>
						</form>
						</div>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_table->count_all($this->tbl_standar_kompetensi),
						"recordsFiltered" => $this->model_table->count_filtered($this->tbl_standar_kompetensi,$column),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	//Training//
	function training(){
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Training','training');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);

		$data['data'] = $this->model_basic->select_all($this->tbl_standar_kompetensi_training);
		$data['content'] = $this->load->view('backend/admin_standar_kompetensi/training',$data,true);
		$this->load->view('backend/index',$data); 
	}

	function training_form(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Training','training');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);
		$id = $this->input->post('id');
		$data['user'] = $this->model_basic->select_where($this->tbl_user,'delete_flag',0);

		if($id){
			$data['data'] = $this->model_basic->select_where($this->tbl_standar_kompetensi_training,'id',$id)->row();
		}
		else{
			$data['data'] = null;
		}
		$data['content'] = $this->load->view('backend/admin_standar_kompetensi/training_form',$data,true);
		$this->load->view('backend/index',$data); 
    }

    function training_add(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Training','training');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);

		$table_field = $this->db->list_fields($this->tbl_standar_kompetensi_training);
		$insert = array();
		foreach ($table_field as $field) {
			$insert[$field] = $this->input->post($field);
		}
		$insert['date_created'] = date('Y-m-d H:i:s',now());
		$insert['id_created'] = $this->session_admin['admin_id'];
		if($insert['name']){
			$do_insert = $this->model_basic->insert_all($this->tbl_standar_kompetensi_training,$insert);
			if($do_insert){
                                $this->save_log_admin(ACT_CREATE, 'Insert New Rules STC Training '.$insert['name']);
				$this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => $data['controller'].'/'.$data['function']));
			}
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function training_edit(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Training','training');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_UPDATE);
		
		$table_field = $this->db->list_fields($this->tbl_standar_kompetensi_training);
		$update = array();
		foreach ($table_field as $field) {
			$update[$field] = $this->input->post($field);
		}
		unset($update['date_created']);
		$update['date_modified'] = date('Y-m-d H:i:s',now());
		$update['id_modified'] = $this->session_admin['admin_id'];

		if($update['name']){
			$do_update = $this->model_basic->update($this->tbl_standar_kompetensi_training,$update,'id',$update['id']);
			if($do_update)
                        {
                                $this->save_log_admin(ACT_UPDATE, 'Update Rules STC Training '.$update['name']);
				$this->returnJson(array('status' => 'ok','msg' => 'Update success','redirect' => $data['controller'].'/'.$data['function']));
                        }
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when updating data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function training_delete(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Training','training');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_DELETE);
		$id = $this->input->post('id');
		$do_delete = $this->model_basic->delete($this->tbl_standar_kompetensi_training,'id',$id);
		if($do_delete){
                        $this->save_log_admin(ACT_DELETE, 'Delete Rules STC Training');
			$this->delete_folder('training/'.$id);
			$this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => $data['controller'].'/'.$data['function']));
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }

    public function ajax_training_list(){
		$list = $this->model_standar_kompetensi_training->get_datatables();
		//die(print_r($this->db->last_query()));
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $data_row) {
			//render data
			$no++;
			
			$row = array();
			$row[] = $no;
			$row[] = $data_row->name;
			//add html for action
			$row[] = '<div class="text-center">
						<form action="admin_standar_kompetensi/training_form" method="post">
						<input type="hidden" name="id" value="'.$data_row->id.'">
						<button class="btn btn-info btn-xs btn-edit" type="submit" data-original-title="Update" data-placement="top" data-toggle="tooltip"><i class="fa fa-edit"></i></button>
						<button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Delete" data-placement="top" data-toggle="tooltip" data-target-id="'.$data_row->id.'"><i class="fa fa-trash-o"></i></button>
						</form>
						</div>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_standar_kompetensi_training->count_all(),
						"recordsFiltered" => $this->model_standar_kompetensi_training->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_get_instalasi(){
		$perusahaan_id = $this->input->post('id');

		$results = $this->model_basic->select_where($this->tbl_instalasi, 'perusahaan_id', $perusahaan_id);
		if ($results->num_rows() > 0) {
			$this->returnJson(array('status' => 'ok', 'result' => $results->result()));
		}else{
			$this->returnJson(array('status' => 'not_ok'));
		}
	}

	public function ajax_get_user() {
		$instalasi_id = $this->input->post('id');

		$results = $this->model_basic->select_where($this->tbl_user, 'instalasi_id', $instalasi_id);
		if ($results->num_rows() > 0) {
			$this->returnJson(array('status' => 'ok', 'result' => $results->result()));
		}else{
			$this->returnJson(array('status' => 'not_ok'));
		}
	}

}

/* End of file admin_standar_kompetensi.php */
/* Location: ./application/controllers/admin_standar_kompetensi.php */