<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class user_report extends PX_Controller {

	public function __construct() {
		parent::__construct();
		$this->user_check_login();
		$this->controller_attr = array('controller' => 'user_report','controller_name' => 'Admin Report','controller_id' => 0);
	}

	public function index()
	{
		$this->report();
	}

	public function report() {
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function_user('Report','user_report');
		$data += $this->user_get_menu();
		$this->check_userakses_user($data['function_id'], ACT_READ);

		$data['content'] = $this->load->view('user_backend/admin_report/index',$data,true);
		$this->load->view('user_backend/index',$data); 
	}

	function search_report() {
        $date_start = date('Y-m-d',strtotime($this->input->post('date_start')));
        $date_end = date('Y-m-d',strtotime($this->input->post('date_end')));
        $documents = $this->input->post('documents');
        if ($date_start && $date_end && $documents) {
            if ($documents == 1)
                $this->get_document_asesmen2004($date_start, $date_end);
            else if ($documents == 2)
                $this->get_document_asesmen2015($date_start, $date_end);
            else if ($documents == 3)
                $this->get_document_stc($date_start, $date_end);
            else
                redirect('admin_report?status=error');
        }
        else
            redirect('admin_report?status=error');
    }

    function get_document_asesmen2004($date_start, $date_end) {
        $i = 1;
        $list = array();
    	$list_asesment2004 = $this->model_assessment2004->get_asesment_by_date($date_start, $date_end)->result();
       // die(print_r($this->db->last_query()));
    	foreach ($list_asesment2004 as $row) {
    		$asesmen_value = $this->model_basic->select_where($this->tbl_asesmen_2004_value,'id_asesmen',$row->id)->result();
            //die(print_r($this->db->last_query()));
            $tgl = date('d-m-Y',strtotime($row->date_created));
    		$list[] = array($i,$row->name, $asesmen_value[0]->value, $asesmen_value[1]->value, $asesmen_value[2]->value, $asesmen_value[3]->value, $row->nilai_akhir,$tgl);
            $i++;
    	}

        $data['list'] = $list;

    	$this->to_excel($this->load->view('user_backend/admin_report/asesment2004', $data),'report_asesment2004');
    }

    function get_document_asesmen2015($date_start, $date_end) {
        $i = 1;
        $list = array();
        $list_asesment2015 = $this->model_assessment2015->get_asesment_by_date($date_start, $date_end)->result();
        foreach ($list_asesment2015 as $row) {
            $pemenuhan_system = $this->model_assessment2015->sum_value($this->tbl_asesmen_2015_value,'value','id_asesmen',$row->id)->jumlah / 4;
            //die(print_r($this->db->last_query()));
            $tgl = date('d-m-Y',strtotime($row->date_created));
            $security_reliability = ($row->people + $row->device_and_infrastructure) / 2;
            $list[] = array($i,$row->name, $pemenuhan_system, $row->security_performance, $security_reliability, $row->csi, $row->nilai_akhir,$row->critical_point,$tgl);
            $i++;
        }

        $data['list'] = $list;

        $this->to_excel($this->load->view('user_backend/admin_report/asesment2015', $data),'report_asesment2015');
    }

    function get_document_stc($date_start, $date_end) {
        $i = 1;
        $list = array();
        $list_stc = $this->model_standar_kompetensi->get_stc_by_date($date_start, $date_end)->result();
        foreach ($list_stc as $row) {
            $asesmen_value = $this->model_basic->select_where($this->tbl_asesmen_2004_value,'id_asesmen',$row->id)->result();
            //die(print_r($this->db->last_query()));
            $tgl = date('d-m-Y',strtotime($row->date_created));
            $list[] = array($i,$row->realname, $row->name, $row->final_score, $tgl);
            $i++;
        }

        $data['list'] = $list;

        $this->to_excel($this->load->view('user_backend/admin_report/standar_kompetensi', $data),'report_stc');
    }

    function to_excel($query, $filename='xlsoutput') {
		$headers = '';
		header("Content-type: application/x-msdownload");
		header("Content-Disposition: attachment; filename=$filename.xls");
		echo "$headers\n$query";
	}

}

/* End of file admin_report.php */
/* Location: ./application/controllers/admin_report.php */