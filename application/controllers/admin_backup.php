<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends PX_Controller {

	public function __construct() {
		parent::__construct();
		// $data_row_ctr = $this->model_basic->select_where($this->table_menu,'target','admin')->row();
		$this->controller_attr = array('controller' => 'admin','controller_name' => 'Admin','controller_id' => 0);
	}

	public function index() {
		$data = $this->controller_attr;
		if($this->session->userdata('admin') != FALSE){
			redirect('admin/dashboard');
		}
		else
			redirect('admin/login');
	}

	function dashboard() {
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Admin','admin');
		$data += $this->get_menu();
		if($this->session->userdata('admin') != FALSE){
			$data['banner'] = $this->model_basic->select_all($this->tbl_banner);
			$data['status_hijau'] = $this->model_basic->select_where($this->tbl_instalasi,'status_kerawanan',1)->num_rows();
                        $kuning = $this->model_basic->select_where($this->tbl_instalasi,'status_kerawanan',2);
                        $merah = $this->model_basic->select_where($this->tbl_instalasi,'status_kerawanan',3);
			$data['status_kuning'] = $kuning->num_rows();
			$data['status_merah'] = $merah->num_rows();
                        $data['list_kuning'] = $kuning->result();
                        $data['list_merah'] = $merah->result();
			$data['province'] = $this->model_basic->select_all_noflag($this->tbl_province);
			$data['security_analysis_info'] = $this->model_basic->select_where_limit_order($this->tbl_security_analysis,'kategori',2, 3,'DESC','date_created')->result();
			$data['security_analysis_analyst'] = $this->model_basic->select_where_limit_order($this->tbl_security_analysis,'kategori',1, 3,'DESC','date_created')->result();
			$data['info_training'] = $this->model_basic->select_all_limit_order($this->tbl_training_info,2, 'date_created', 'desc')->result();
			$data['data_instalasi'] = $this->model_basic->select_all($this->tbl_instalasi);
                        $data['total_visitor'] = $this->model_basic->select_where($this->tbl_visitor, 'delete_flag', 0)->num_rows();
			$data['content'] = $this->load->view('backend/admin/dashboard',$data,true);
			$data['year_now'] = date('Y');
			$this->load->view('backend/index',$data);
		}
		else
			redirect('admin');
	}

	function login() {
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Admin Login','admin_login');
		$data += $this->get_menu();
		$this->load->view('backend/admin/login',$data);
	}

	function do_login() {
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$user_data = $this->model_basic->select_where($this->tbl_admin,'username',$username)->row();
		if($user_data){
			if($this->encrypt->decode($user_data->password) == $password){
				$user_group = $this->model_basic->select_where($this->tbl_usergroup,'id',$user_data->id_usergroup)->row()->usergroup_name;
				$data_user = array(
					'admin_id' => $user_data->id,
					'username' => $user_data->username,
					'password' => $password,
					'realname' => $user_data->realname,
					'email' => $user_data->email,
					'id_usergroup' => $user_data->id_usergroup,
					'name_usergroup' => $user_group,
					'photo' => $user_data->photo,
                                        'date_modified' => $user_data->date_modified
					);
				$this->session->set_userdata('admin',$data_user);
                                $this->insert_visitory_history($user_data->id, 1);
				$this->returnJson(array('status' => 'ok','msg' => 'Login Success, you\'ll be redirect soon.','redirect' => 'admin/dashboard'));
			}
			else
				$this->returnJson(array('status' => 'error','msg' => 'Login failed, Wrong password.'));
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Login failed, Username not registered.'));
	}
        
	function do_logout() {
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Admin Logout','admin_logout');
		$data += $this->get_menu();
		if($this->session->userdata('admin') != FALSE){
			$this->session->unset_userdata('admin');
			redirect('admin');
		}
		else
			redirect('admin');
	}

	function chart_jumlah_instalasi(){
		//$data = $this->model_chart->jumlah_instalasi();
                $data = $this->model_basic->select_all_order($this->tbl_jenis_bisnis, 'position', 'ASC');
		$category = array();
		$series_data = array();
		foreach ($data as $d) {
			array_push($category, $d->name);
                        $jumlah_instalasi = $this->model_basic->select_where($this->tbl_perusahaan, 'jenis_bisnis_id', $d->id)->num_rows();
			array_push($series_data,(int)$jumlah_instalasi);
		}
		echo json_encode(array('status' => 'ok','category' => $category,'series' => array('name' => 'Jumlah Instalasi','data' => $series_data)));
	}
	
	function chart_jumlah_satpam(){
		$data = $this->model_basic->select_all_order($this->tbl_jenis_bisnis, 'position', 'ASC');
		$category = array();
		$series = array();
		$series_data_organik = array();
		$series_data_outsourching = array();
		foreach ($data as $d) {
                        $result = $this->model_chart->get_satpam_sum_by_jenis_bisnis($d->id);
			array_push($category, $d->name);
			array_push($series_data_organik,(int)$result->jumlah_organik);
			array_push($series_data_outsourching,(int)$result->jumlah_outsourching);
		}
		array_push($series, array(
					'name' => 'Organik',
					'data' => $series_data_organik
					));
		array_push($series, array(
					'name' => 'Outsourching',
					'data' => $series_data_outsourching
					));
		echo json_encode(
			array(
				'status' => 'ok',
				'category' => $category,
				'series' => $series
				)
			);
	}

	function chart_jumlah_asesmen() {
		$data = $this->model_basic->select_all_order($this->tbl_jenis_bisnis, 'position', 'ASC');
		$category = array();
		$series = array();
		$series_data = array();
		foreach ($data as $d) {
                        $result = $this->model_chart->get_assessment_by_jenis_bisnis($d->id);
			array_push($category, $d->name);
			array_push($series_data, (int)$result->jumlah_asesmen);
		}
		array_push($series, array(
					'name' => 'Jumlah Asesmen',
					'data' => $series_data
					));
		echo json_encode(
			array(
				'status' => 'ok',
				'category' => $category,
				'series' => $series
				)
			);
	}

	function chart_hasil_asesmen_per_grup() {
		$category = array();
		$series = array();
		$series_data_hitam = array();
		$series_data_merah = array();
		$series_data_biru = array();
		$series_data_hijau = array();
		$series_data_emas = array();

		$jenis_bisnis = $this->model_basic->select_all_order($this->tbl_jenis_bisnis, 'position', 'ASC');
		
		foreach ($jenis_bisnis as $d) {
			$cnt_emas = 0;
			$cnt_hijau = 0;
			$cnt_biru = 0;
			$cnt_merah = 0;
			$cnt_hitam = 0;
			array_push($category, $d->name);
			$nilai_akhir = $this->model_chart->hasil_asesmen($d->id);
			if ($nilai_akhir->num_rows() > 0) {
				foreach ($nilai_akhir->result() as $e) {
					$warna = $this->check_colour_text_dashboard($e->nilai_akhir,'Nilai Akhir (Asesmen 2015)');
					if ($warna == 'emas') {
						$cnt_emas++;
					}elseif ($warna == 'hijau') {
						$cnt_hijau++;
					}elseif ($warna == 'biru') {
						$cnt_biru++;
					}elseif ($warna == 'merah') {
						$cnt_merah++;
					}elseif ($warna == 'hitam') {
						$cnt_hitam++;
					}
				}
			}
			array_push($series_data_emas, (int)$cnt_emas);
			array_push($series_data_hijau, (int)$cnt_hijau);
			array_push($series_data_biru, (int)$cnt_biru);
			array_push($series_data_merah, (int)$cnt_merah);
			array_push($series_data_hitam, (int)$cnt_hitam);
		}
		array_push($series, array(
					'name' => 'Emas',
					'data' => $series_data_emas
					));
		array_push($series, array(
					'name' => 'Hijau',
					'data' => $series_data_hijau
					));
		array_push($series, array(
					'name' => 'Biru',
					'data' => $series_data_biru
					));
		array_push($series, array(
					'name' => 'Merah',
					'data' => $series_data_merah
					));
		array_push($series, array(
					'name' => 'Hitam',
					'data' => $series_data_hitam
					));
		
		echo json_encode(
			array(
				'status' => 'ok',
				'category' => $category,
				'series' => $series
				)
			);
	}

	function chart_hasil_asesmen_per_instalasi() {
		$data = $this->model_chart->jumlah_asesmen_instalasi();
		//die(print_r($this->db->last_query()));
		$category = array();
		$series = array();
		$series_data = array();
		foreach ($data as $d) {
			array_push($category, $d->instalasi_name);
			array_push($series_data, (int)$d->jumlah_asesmen);
		}
		array_push($series, array(
					'name' => 'Jumlah Asesmen',
					'data' => $series_data
					));
		echo json_encode(
			array(
				'status' => 'ok',
				'category' => $category,
				'series' => $series
				)
			);
	}

	function chart_hasil_asesmen_per_grup_tahun() {
		if (!($this->input->post('year'))) {
			$periode = date('Y');
		}else{
			$periode = $this->input->post('year');
		}
		
		$category = array();
		$series = array();
		$series_data_hitam = array();
		$series_data_merah = array();
		$series_data_biru = array();
		$series_data_hijau = array();
		$series_data_emas = array();

		$jenis_bisnis = $this->model_basic->select_all_order($this->tbl_jenis_bisnis, 'position', 'ASC');
		
		foreach ($jenis_bisnis as $d) {
			$cnt_emas = 0;
			$cnt_hijau = 0;
			$cnt_biru = 0;
			$cnt_merah = 0;
			$cnt_hitam = 0;
			array_push($category, $d->name);
			$nilai_akhir = $this->model_chart->hasil_asesmen_periode($d->id,$periode);
			if ($nilai_akhir->num_rows() > 0) {
				foreach ($nilai_akhir->result() as $e) {
					$warna = $this->check_colour_text_dashboard($e->nilai_akhir,'Nilai Akhir (Asesmen 2015)');
					if ($warna == 'emas') {
						$cnt_emas++;
					}elseif ($warna == 'hijau') {
						$cnt_hijau++;
					}elseif ($warna == 'biru') {
						$cnt_biru++;
					}elseif ($warna == 'merah') {
						$cnt_merah++;
					}elseif ($warna == 'hitam') {
						$cnt_hitam++;
					}
				}
			}
			array_push($series_data_emas, (int)$cnt_emas);
			array_push($series_data_hijau, (int)$cnt_hijau);
			array_push($series_data_biru, (int)$cnt_biru);
			array_push($series_data_merah, (int)$cnt_merah);
			array_push($series_data_hitam, (int)$cnt_hitam);
		}
		array_push($series, array(
					'name' => 'Emas',
					'data' => $series_data_emas
					));
		array_push($series, array(
					'name' => 'Hijau',
					'data' => $series_data_hijau
					));
		array_push($series, array(
					'name' => 'Biru',
					'data' => $series_data_biru
					));
		array_push($series, array(
					'name' => 'Merah',
					'data' => $series_data_merah
					));
		array_push($series, array(
					'name' => 'Hitam',
					'data' => $series_data_hitam
					));
		
		echo json_encode(
			array(
				'status' => 'ok',
				'category' => $category,
				'series' => $series
				)
			);
	}

	function chart_hasil_asesmen2004_per_instalasi() {
		$instalasi_id = 1;
		$data = $this->model_chart->jumlah_asesmen2004_instalasi($instalasi_id);
		//die(print_r($this->db->last_query()));
		$category = array();
		$series = array();
		$series_data = array();
		foreach ($data as $d) {
			array_push($category, $d->rules_name);
			array_push($series_data, (int)$d->jumlah_asesmen);
		}
		array_push($series, array(
					'name' => 'Jumlah Asesmen',
					'data' => $series_data
					));
		echo json_encode(
			array(
				'status' => 'ok',
				'category' => $category,
				'series' => $series
				)
			);
	}

	function chart_hasil_asesmen2015_per_instalasi() {
		$instalasi_id = 1;
		$data = $this->model_chart->jumlah_asesmen2015_instalasi($instalasi_id);
		//die(print_r($this->db->last_query()));
		$series = array();
		$series_data = array();
		//pemenuhan system
		$pemenuhan_system = $this->model_assessment2015->sum_value($this->tbl_asesmen_2015_value,'value','id_asesmen',$data->id)->jumlah / 4;
		array_push($series_data, (int)$pemenuhan_system);
		//performance
		array_push($series_data, (int)$data->security_performance);
		//reliability
		$reliability = ($data->people + $data->device_and_infrastructure) / 2;
		array_push($series_data, (int) $reliability);
		//csi
		array_push($series_data, (int)$data->csi);

		array_push($series, array(
					'name' => 'Jumlah Asesmen',
					'data' => $series_data
					));
		echo json_encode(
			array(
				'status' => 'ok',
				'series' => $series
				)
			);
	}

	function chart_kompetensi_per_grup() {
		$category = array();
		$series = array();
		$series_data_m = array();
		$series_data_mm = array();
		$series_data_o = array();
		$jenis_bisnis = $this->model_basic->select_all_order($this->tbl_jenis_bisnis, 'position', 'ASC');
		
		foreach ($jenis_bisnis as $d) {
			$cnt_m = 0;
			$cnt_mm = 0;
			$cnt_o = 0;
			array_push($category, $d->name);
			$final_score = $this->model_chart->rataan_kompetensi($d->id);
			if ($final_score->num_rows() > 0) {
				foreach ($final_score->result() as $e) {
					if ($e->level_user == 1) {
						$cnt_m = $cnt_m + $e->final_score;
					}elseif ($e->level_user == 2) {
						$cnt_mm = $cnt_mm + $e->final_score;
					}elseif ($e->level_user == 3) {
						$cnt_o = $cnt_o + $e->final_score;
					}
				}
				$avg_m = $cnt_m / $final_score->num_rows();
				$avg_mm = $cnt_mm / $final_score->num_rows();
				$avg_o = $cnt_o / $final_score->num_rows();
			}
                        else
                        {
                            $avg_m = $cnt_m;
                            $avg_mm = $cnt_mm;
                            $avg_o = $cnt_o;
                        }
			array_push($series_data_m, (int)$avg_m);
			array_push($series_data_mm, (int)$avg_mm);
			array_push($series_data_o, (int)$avg_o);
		}
		array_push($series, array(
					'name' => 'Management',
					'data' => $series_data_m
					));
		array_push($series, array(
					'name' => 'Middle Management',
					'data' => $series_data_mm
					));
		array_push($series, array(
					'name' => 'Operasional',
					'data' => $series_data_o
					));
		echo json_encode(
			array(
				'status' => 'ok',
				'category' => $category,
				'series' => $series
				)
			);
	}

	function chart_training_per_grup() {
		$category = array();
		$series = array();
		$series_data_m = array();
		$series_data_mm = array();
		$series_data_o = array();
		$jenis_bisnis = $this->model_basic->select_all_order($this->tbl_jenis_bisnis, 'position', 'ASC');
		
		foreach ($jenis_bisnis as $d) {
			$cnt_m = 0;
			$cnt_mm = 0;
			$cnt_o = 0;
			array_push($category, $d->name);
			$training = $this->model_chart->rataan_training($d->id);
			if ($training->num_rows() > 0) {
				foreach ($training->result() as $e) {
					if ($e->level_user == 1 && $e->training != 0) {
						$cnt_m++;
					}elseif ($e->level_user == 2 && $e->training != 0) {
						$cnt_mm++;
					}elseif ($e->level_user == 3 && $e->training != 0) {
						$cnt_o++;
					}
				}
				$avg_m = $cnt_m;
				$avg_mm = $cnt_mm;
				$avg_o = $cnt_o;
			}
                        else
                        {
                            $avg_m = 0;
                            $avg_mm = 0;
                            $avg_o = 0;
                        }
			array_push($series_data_m, (int)$avg_m);
			array_push($series_data_mm, (int)$avg_mm);
			array_push($series_data_o, (int)$avg_o);
		}
		array_push($series, array(
					'name' => 'Management',
					'data' => $series_data_m
					));
		array_push($series, array(
					'name' => 'Middle Management',
					'data' => $series_data_mm
					));
		array_push($series, array(
					'name' => 'Operasional',
					'data' => $series_data_o
					));
		echo json_encode(
			array(
				'status' => 'ok',
				'category' => $category,
				'series' => $series
				)
			);
	}

	function chart_stc_per_grup() {
		$category = array();
		$series = array();
		$series_data_1 = array();
		$series_data_2 = array();
		$series_data_3 = array();
		$series_data_4 = array();
		$jenis_bisnis = $this->model_basic->select_all_order($this->tbl_jenis_bisnis, 'position', 'ASC');
		
		foreach ($jenis_bisnis as $d) {
			$cnt_1 = 0;
			$cnt_2 = 0;
			$cnt_3 = 0;
			$cnt_4 = 0;
			array_push($category, $d->name);
			$final_score = $this->model_chart->rataan_stc($d->id);
			//die(print_r($this->db->last_query()));
			if ($final_score->num_rows() > 0) {
				foreach ($final_score->result() as $e) {
					if ($e->stc_id == 1) {
						if ($e->final_score >= $e->score_standard) {
							$cnt_1++;
							//print_r('STC_ID = '.$e->stc_id.' - SCR_STD = '.$e->score_standard.' - SCR = '.$e->final_score.' <br>');
						}
					}elseif ($e->stc_id == 2) {
						if ($e->final_score >= $e->score_standard) {
							$cnt_2++;
							//print_r('STC_ID = '.$e->stc_id.' - SCR_STD = '.$e->score_standard.' - SCR = '.$e->final_score.' <br>');
						}
					}elseif ($e->stc_id == 3) {
						if ($e->final_score >= $e->score_standard) {
							$cnt_3++;
							//print_r('STC_ID = '.$e->stc_id.' - SCR_STD = '.$e->score_standard.' - SCR = '.$e->final_score.' <br>');
						}
					}elseif ($e->stc_id == 4) {
						if ($e->final_score >= $e->score_standard) {
							$cnt_4++;
							//print_r('STC_ID = '.$e->stc_id.' - SCR_STD = '.$e->score_standard.' - SCR = '.$e->final_score.' <br>');
						}
					}
				}
			}
                        $avg_1 = $cnt_1;
                        $avg_2 = $cnt_2;
                        $avg_3 = $cnt_3;
                        $avg_4 = $cnt_4;
			array_push($series_data_1, (int)$avg_1);
			array_push($series_data_2, (int)$avg_2);
			array_push($series_data_3, (int)$avg_3);
			array_push($series_data_4, (int)$avg_4);
		}
		array_push($series, array(
					'name' => 'STC1',
					'data' => $series_data_1
					));
		array_push($series, array(
					'name' => 'STC2',
					'data' => $series_data_2
					));
		array_push($series, array(
					'name' => 'STC3',
					'data' => $series_data_3
					));
		array_push($series, array(
					'name' => 'STC4',
					'data' => $series_data_4
					));
		echo json_encode(
			array(
				'status' => 'ok',
				'category' => $category,
				'series' => $series
				)
			);
	}

	function chart_stc_perinstalasi($id_instalasi = 0){
		$stc_1_count = 0;
		$stc_2_count = 0;
		$stc_3_count = 0;
		$stc_4_count = 0;

		$stc_1_total = 0;
		$stc_2_total = 0;
		$stc_3_total = 0;
		$stc_4_total = 0;

		(float)$stc_1_percentage = 0;
		(float)$stc_2_percentage = 0;
		(float)$stc_3_percentage = 0;
		(float)$stc_4_percentage = 0;

		$counter = 0;
		if($id_instalasi < 1)
			$id_instalasi = $this->model_basic->select_all_data($this->tbl_instalasi)->row()->id;

		$data_instalasi = $this->model_basic->select_where($this->tbl_instalasi,'id',$id_instalasi)->row();
		$data_user = $this->model_basic->select_where($this->tbl_user,'instalasi_id',$id_instalasi);
		if($data_user){
			foreach ($data_user->result() as $row) {
				$data_stc = $this->data_stc_perorangan($row->id);
				$stc_1_total = $stc_1_total + $data_stc['nilai_stc_1'];
				$stc_2_total = $stc_2_total + $data_stc['nilai_stc_2'];
				$stc_3_total = $stc_3_total + $data_stc['nilai_stc_3'];
				$stc_4_total = $stc_4_total + $data_stc['nilai_stc_4'];
				$counter++;
			}
			if($counter > 0){
				(float)$stc_1_percentage = ($stc_1_total / $counter);
				(float)$stc_2_percentage = ($stc_2_total / $counter);
				(float)$stc_3_percentage = ($stc_3_total / $counter);
				(float)$stc_4_percentage = ($stc_4_total / $counter);
			}
			// echo $stc_1_total.'<br>';
			// echo $stc_2_total.'<br>';
			// echo $stc_3_total.'<br>';
			// echo $stc_4_total.'<br>';

			// echo $stc_1_percentage.'<br>';
			// echo $stc_2_percentage.'<br>';
			// echo $stc_3_percentage.'<br>';
			// echo $stc_4_percentage.'<br>';
			// echo $counter.'<br>';

			$json_nilai = array(
				'name' => 'Perolehan Nilai',
                'data' => array($stc_1_percentage,$stc_2_percentage,$stc_3_percentage,$stc_4_percentage),
                'pointPlacement' => 'on'
                );
			$json_data = array(
				$json_nilai
				);
			echo json_encode(array('status' => 'ok','data' => $json_data,'nama_instalasi' => $data_instalasi->name));
		}
	}

	function data_stc_perorangan($id_user = 0, $id_result = 0){
		$stc_1 = 0;
		$stc_2 = 0;
		$stc_3 = 0;
		$stc_4 = 0;
		$stc_1_counter = 0;
		$stc_2_counter = 0;
		$stc_3_counter = 0;
		$stc_4_counter = 0;
		$stc_1_total = 0;
		$stc_2_total = 0;
		$stc_3_total = 0;
		$stc_4_total = 0;
		(float)$stc_1_percentage = 0;
		(float)$stc_2_percentage = 0;
		(float)$stc_3_percentage = 0;
		(float)$stc_4_percentage = 0;
		$stc_1_lowest = 0;
		$stc_2_lowest = 0;
		$stc_3_lowest = 0;
		$stc_4_lowest = 0;
		$stc_1_highest = 0;
		$stc_2_highest = 0;
		$stc_3_highest = 0;
		$stc_4_highest = 0;
		if($id_result > 0)
			$data_chart = $this->model_standar_kompetensi->get_detail_standar_kompetensi_by_user_id($id_user,$id_result);
		else
			$data_chart = $this->model_standar_kompetensi->get_detail_standar_kompetensi_by_user_id($id_user);

		if($data_chart){
			$data_stc = $data_chart->result();
			foreach ($data_stc as $row) {
				// echo 'STC-'.$row->stc_no.' Score '.$row->score.'<br>';
				if($row->stc_no == 1){
					$stc_1_counter++;
					$stc_1_total = $stc_1_total + $row->score;

					if($stc_1_counter == 1){
						$stc_1_lowest = $row->score;
						$stc_1_highest = $row->score;
					}
					if($row->score > $stc_1_highest){
						$stc_1_highest = $row->score;
					}

					if($row->score < $stc_1_lowest){
						$stc_1_lowest = $row->score;
					}
				}
				if($row->stc_no == 2){
					$stc_2_counter++;
					$stc_2_total = $stc_2_total + $row->score;

					if($stc_2_counter == 1){
						$stc_2_lowest = $row->score;
						$stc_2_highest = $row->score;
					}
					if($row->score > $stc_2_highest){
						$stc_2_highest = $row->score;
					}

					if($row->score < $stc_1_lowest){
						$stc_1_lowest = $row->score;
					}
				}
				if($row->stc_no == 3){
					$stc_3_counter++;
					$stc_3_total = $stc_3_total + $row->score;

					if($stc_3_counter == 1){
						$stc_3_lowest = $row->score;
						$stc_3_highest = $row->score;
					}
					if($row->score > $stc_3_highest){
						$stc_3_highest = $row->score;
					}

					if($row->score < $stc_3_lowest){
						$stc_3_lowest = $row->score;
					}
				}
				if($row->stc_no == 4){
					$stc_4_counter++;
					$stc_4_total = $stc_4_total + $row->score;

					if($stc_4_counter == 1){
						$stc_4_lowest = $row->score;
						$stc_4_highest = $row->score;
					}
					if($row->score > $stc_4_highest){
						$stc_4_highest = $row->score;
					}

					if($row->score < $stc_4_lowest){
						$stc_4_lowest = $row->score;
					}
				}
			}
			if($stc_1_counter > 0)
				(float)$stc_1_percentage = ($stc_1_total / ($stc_1_counter * 4)) * 100;
			if($stc_2_counter > 0)
				(float)$stc_2_percentage = ($stc_2_total / ($stc_2_counter * 4)) * 100;
			if($stc_3_counter > 0)
				(float)$stc_3_percentage = ($stc_3_total / ($stc_3_counter * 4)) * 100;
			if($stc_4_counter > 0)
				(float)$stc_4_percentage = ($stc_4_total / ($stc_4_counter * 4)) * 100;
			(int)$stc_1_nilai_max = ($stc_1_counter * 4);
			(int)$stc_2_nilai_max = ($stc_2_counter * 4);
			(int)$stc_3_nilai_max = ($stc_3_counter * 4);
			(int)$stc_4_nilai_max = ($stc_4_counter * 4);
			(int)$stc_1_nilai_min = ($stc_1_counter * 1);
			(int)$stc_2_nilai_min = ($stc_2_counter * 1);
			(int)$stc_3_nilai_min = ($stc_3_counter * 1);
			(int)$stc_4_nilai_min = ($stc_4_counter * 1);
			// echo 'STC 1 = Percentage = '.$stc_1_percentage.' - Total '.$stc_1_total.' - Nilai Max '.($stc_1_counter * 4).' - Nilai Min '.($stc_1_counter * 1).' - Jumlah Data '.$stc_1_counter.' - Nilai Tertinggi '.$stc_1_highest.' - Nilai Terendah '.$stc_1_lowest.'<br>';
			// echo 'STC 2 = Percentage = '.$stc_2_percentage.' - Total '.$stc_2_total.' - Nilai Max '.($stc_2_counter * 4).' - Nilai Min '.($stc_2_counter * 1).' - Jumlah Data '.$stc_2_counter.' - Nilai Tertinggi '.$stc_2_highest.' - Nilai Terendah '.$stc_2_lowest.'<br>';
			// echo 'STC 3 = Percentage = '.$stc_3_percentage.' - Total '.$stc_3_total.' - Nilai Max '.($stc_3_counter * 4).' - Nilai Min '.($stc_3_counter * 1).' - Jumlah Data '.$stc_3_counter.' - Nilai Tertinggi '.$stc_3_highest.' - Nilai Terendah '.$stc_3_lowest.'<br>';
			// echo 'STC 4 = Percentage = '.$stc_4_percentage.' - Total '.$stc_4_total.' - Nilai Max '.($stc_4_counter * 4).' - Nilai Min '.($stc_4_counter * 1).' - Jumlah Data '.$stc_4_counter.' - Nilai Tertinggi '.$stc_4_highest.' - Nilai Terendah '.$stc_4_lowest.'<br>';
			$json_nilai = array(
				'nilai_stc_1' => $stc_1_percentage,
				'nilai_stc_2' => $stc_2_percentage,
				'nilai_stc_3' => $stc_3_percentage,
				'nilai_stc_4' => $stc_4_percentage
                );
			return $json_nilai;
		}
	}

	function php_info(){
		echo phpinfo();
	}
        
        function get_session_id()
        {
            $session = $this->session->all_userdata();
            print_r(date('d M Y H:i:s', $session['last_activity']));
        }
		
		function get_password()
		{
			$password = 'HY19dUeMuNdc2/kMJNsjOiF5ZMblf5wx19DqPjIO5N1G5eZ3Yp37025E2EAo5f73HpLkptMWNTj1ZV26NRDw6g==';
			echo $this->encrypt->decode($password);
		}

}
