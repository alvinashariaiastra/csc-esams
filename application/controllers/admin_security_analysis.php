<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_security_analysis extends PX_Controller {

	public function __construct() {
		parent::__construct();
		$this->check_login();
		$this->controller_attr = array('controller' => 'admin_security_analysis','controller_name' => 'Admin Security Analysis','controller_id' => 0);
	}

	public function index()
	{
		$this->security_analysis();
	}

	function security_analysis(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Admin Security Analysis','security_analysis');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);
		$data['data'] = $this->model_basic->select_all($this->tbl_security_analysis);
		$data['content'] = $this->load->view('backend/admin_security_analysis/security_analysis',$data,true);
		$this->load->view('backend/index',$data); 
    }

    function security_analysis_form(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Admin Security Analysis','security_analysis');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);
		$id = $this->input->post('id');
		if($id){
			$data['data'] = $this->model_basic->select_where($this->tbl_security_analysis,'id',$id)->row();
			$content = new domDocument;
			libxml_use_internal_errors(true);
			$content->loadHTML($data['data']->content);
			libxml_use_internal_errors(false);
			$content->preserveWhiteSpace = false;
			$images = $content->getElementsByTagName('img');
			if($images){
				foreach ($images as $image) {
				  $data['data']->image[] = $image->getAttribute('src');
				}
			}
		}
		else
			$data['data'] = null;
		$data['content'] = $this->load->view('backend/admin_security_analysis/security_analysis_form',$data,true);
		$this->load->view('backend/index',$data); 
    }

    function security_analysis_add(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Admin Security Analysis','security_analysis');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);

		$images = $this->input->post('images');
		$table_field = $this->db->list_fields($this->tbl_security_analysis);
		$insert = array();
		foreach ($table_field as $field) {
			$insert[$field] = $this->input->post($field);
		}
		$insert['date_created'] = date('Y-m-d H:i:s',now());
		$insert['date_modified'] = date('Y-m-d H:i:s',now());
		
		if($insert['title'] && $insert['content']){
			$do_insert = $this->model_basic->insert_all($this->tbl_security_analysis,$insert);
			if($do_insert){
				if($images){
					if(!is_dir(FCPATH . "assets/uploads/security_analysis/".$do_insert->id))
						mkdir(FCPATH . "assets/uploads/security_analysis/".$do_insert->id);
					$content = $insert['content']; 
					foreach($images as $im) {
						if(strpos($content,$im) !== false)
						{
							$new_im = 'assets/uploads/security_analysis/'.$do_insert->id.'/'.basename($im);
							@copy($im,$new_im);
						    $content = str_replace($im, $new_im, $content);
						}
					}
					$update['content'] = $content;
					$do_update = $this->model_basic->update($this->tbl_security_analysis,$update,'id',$do_insert->id);
					$this->delete_temp('temp_folder');
				}
                                $this->save_log_admin(ACT_CREATE, 'Insert New Security Analysis '.$insert['title']);
				$this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => $data['controller'].'/'.$data['function']));
			}
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function security_analysis_edit(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Admin Security Analysis','security_analysis');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_UPDATE);
		
		$images = $this->input->post('images');
		$table_field = $this->db->list_fields($this->tbl_security_analysis);
		$update = array();
		foreach ($table_field as $field) {
			$update[$field] = $this->input->post($field);
		}
		unset($update['date_created']);
		$update['date_modified'] = date('Y-m-d H:i:s',now());
		$update['id_modifier'] = $this->session_admin['admin_id'];
		if($images){
			if(!is_dir(FCPATH . "assets/uploads/security_analysis/".$update['id']))
				mkdir(FCPATH . "assets/uploads/security_analysis/".$update['id']);
			$content = $update['content']; 
			foreach($images as $im) {
				if(strpos($content,$im) !== false)
				{
					$new_im = 'assets/uploads/security_analysis/'.$update['id'].'/'.basename($im);
					if($im != $new_im)
						@copy($im,$new_im);
				    $content = str_replace($im, $new_im, $content);
				}
				else
					@unlink($im);
			}
			$update['content'] = $content;
			$this->delete_temp('temp_folder');
		}
		if($update['title'] && $update['content']){
			$do_update = $this->model_basic->update($this->tbl_security_analysis,$update,'id',$update['id']);
			if($do_update)
                        {
				$this->returnJson(array('status' => 'ok','msg' => 'Update success','redirect' => $data['controller'].'/'.$data['function']));
                                $this->save_log_admin(ACT_UPDATE, 'Update Security Analysis '.$update['title']);
                        }
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when updating data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function security_analysis_delete(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Admin Security Analysis','security_analysis');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_DELETE);
		$id = $this->input->post('id');
		$do_delete = $this->model_basic->delete($this->tbl_security_analysis,'id',$id);
		if($do_delete){
                        $this->save_log_admin(ACT_DELETE, 'Delete Security Analysis');
			$this->delete_folder('security_analysis/'.$id);
			$this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => $data['controller'].'/'.$data['function']));
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }

    public function ajax_security_analysis_list()
	{
		$column = array('title','kategori','status','date_modified');
		$list = $this->model_table->get_datatables($this->tbl_security_analysis,$column);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $data_row) {
			//render data
			$no++;
			
			$row = array();
			$row[] = $no;
			$row[] = $data_row->title;
			$row[] = ($data_row->kategori == 1) ? 'Analysis' : 'Informasi';
			$row[] = ($data_row->status == 1) ? 'Visible' : 'Hidden';
                        $row[] = date('d M Y H:i:s', strtotime($data_row->date_modified));
			//add html for action
			$row[] = '<div class="text-center">
						<form action="admin_security_analysis/security_analysis_form" method="post">
						<input type="hidden" name="id" value="'.$data_row->id.'">
						<button class="btn btn-info btn-xs btn-edit" type="submit" data-original-title="Update" data-placement="top" data-toggle="tooltip"><i class="fa fa-edit"></i></button>
						<button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Delete" data-placement="top" data-toggle="tooltip" data-target-id="'.$data_row->id.'"><i class="fa fa-trash-o"></i></button>
						</form>
						</div>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_table->count_all($this->tbl_security_analysis),
						"recordsFiltered" => $this->model_table->count_filtered($this->tbl_security_analysis,$column),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
        public function detail($id) {
    	$data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin Security Analysis','security_analysis');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);
        $security_analysis = $this->model_basic->select_where($this->tbl_security_analysis, 'id', $id);

        foreach ($security_analysis->result() as $row) {
                $row->date_created = date('j F Y',strtotime($row->date_created));
                $creator = $this->model_basic->select_where($this->tbl_user, 'id', $row->id_creator)->row();
                if($creator)
                        $row->realname = $creator->realname;
                else
                        $row->realname = 'Administrator';
                //increase views
                $initial_views = $row->views;
                $new_views = $initial_views + 1;
                $this->model_basic->update($this->tbl_security_analysis, array('views' => $new_views), 'id', $id);
        }

        $data['security_analysis'] = $security_analysis;
        $data['content'] = $this->load->view('user_backend/admin_security_analysis/detail',$data,true);
        $this->load->view('backend/index',$data); 
    }

}

/* End of file admin_security_analysis.php */
/* Location: ./application/controllers/admin_security_analysis.php */