<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_activity extends PX_Controller {

	public function __construct() {
		parent::__construct();
		$this->check_login();
		$this->controller_attr = array('controller' => 'admin_activity','controller_name' => 'Admin Activity','controller_id' => 0);
	}

	public function index()
	{            
            $data = $this->get_app_settings();
            $data += $this->controller_attr;
            $data += $this->get_function('Admin Activity','index');
            $data += $this->get_menu();
            $this->check_userakses($data['function_id'], ACT_READ);
            $data['submenu'] = $this->get_submenu($data['controller']);

            $data['content'] = $this->load->view('backend/admin_activity/index',$data,true);
            $this->load->view('backend/index',$data);
	}
        
        public function admin_log()
	{            
            $data = $this->get_app_settings();
            $data += $this->controller_attr;
            $data += $this->get_function('Admin Log','admin_log');
            $data += $this->get_menu();
            $this->check_userakses($data['function_id'], ACT_READ);
            
            $update = array('read_flag' => 1);
            $where = array('read_flag' => 0, 'access_type' => 1);
            $this->model_basic->update_array($this->tbl_user_log, $update, $where);

            $data['content'] = $this->load->view('backend/admin_activity/admin_log',$data,true);
            $this->load->view('backend/index',$data);
	}
        
        public function user_log()
	{            
            $data = $this->get_app_settings();
            $data += $this->controller_attr;
            $data += $this->get_function('User Log','user_log');
            $data += $this->get_menu();
            $this->check_userakses($data['function_id'], ACT_READ);
            
            $update = array('read_flag' => 1);
            $where = array('read_flag' => 0, 'access_type' => 0);
            $this->model_basic->update_array($this->tbl_user_log, $update, $where);

            $data['content'] = $this->load->view('backend/admin_activity/user_log',$data,true);
            $this->load->view('backend/index',$data);
	}
        
        public function admin_log_ajax()
        {
            $date_month = date('Y-m-d H:i:s', strtotime("-6 month", now()));
            $column = array('id','user_id','type_id','log_description','date_created', 'access_type');
            $list = $this->model_activity->get_datatables_admin($this->tbl_user_log,$column,$date_month);
            $data = array();
            $no = $_POST['start'];
            foreach ($list as $data_row) {
                    //render data
                    if($data_row->access_type == 1)
                    {
                        $v = $this->model_basic->select_where($this->tbl_admin, 'id', $data_row->user_id);
                        if($v->num_rows() == 1)
                            $visitor = $v->row()->realname;
                        else
                            $visitor = 'Unknown';

                        switch($data_row->type_id)
                        {
                            case ACT_READ:
                                $activity = 'READ';
                                break;
                            case ACT_CREATE:
                                $activity = 'INSERT';
                                break;
                            case ACT_UPDATE:
                                $activity = 'UPDATE';
                                break;
                            case ACT_DELETE:
                                $activity = 'DELETE';
                                break;
                        }
                        $no++;

                        $row = array();
                        $row[] = $no;
                        $row[] = $visitor;
                        $row[] = $activity;
                        $row[] = $data_row->log_description;
                        $row[] = date('d M Y H:i:s', strtotime($data_row->date_created));
                        $data[] = $row;
                    }
            }

            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->model_activity->count_all_admin($this->tbl_user_log,$date_month),
                "recordsFiltered" => $this->model_activity->count_filtered_admin($this->tbl_user_log,$column,$date_month),
                "data" => $data,
            );
            //output to json format
            echo json_encode($output);
        }
        
        public function user_log_ajax()
        {
            $date_month = date('Y-m-d H:i:s', strtotime("-6 month", now()));
            $column = array('id','user_id','type_id','log_description','date_created', 'access_type');
            $list = $this->model_activity->get_datatables_user($this->tbl_user_log,$column,$date_month);
            $data = array();
            $no = $_POST['start'];
            foreach ($list as $data_row) {
                    //render data
                    if($data_row->access_type == 0)
                    {
                        $v = $this->model_basic->select_where($this->tbl_user, 'id', $data_row->user_id);
                        if($v->num_rows() == 1)
                            $visitor = $v->row()->realname;
                        else
                            $visitor = 'Unknown';

                        switch($data_row->type_id)
                        {
                            case ACT_READ:
                                $activity = 'READ';
                                break;
                            case ACT_CREATE:
                                $activity = 'INSERT';
                                break;
                            case ACT_UPDATE:
                                $activity = 'UPDATE';
                                break;
                            case ACT_DELETE:
                                $activity = 'DELETE';
                                break;
                        }
                        $no++;

                        $row = array();
                        $row[] = $no;
                        $row[] = $visitor;
                        $row[] = $activity;
                        $row[] = $data_row->log_description;
                        $row[] = date('d M Y H:i:s', strtotime($data_row->date_created));
                        $data[] = $row;
                    }
            }

            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->model_activity->count_all_user($this->tbl_user_log,$date_month),
                "recordsFiltered" => $this->model_activity->count_filtered_user($this->tbl_user_log,$column,$date_month),
                "data" => $data,
            );
            //output to json format
            echo json_encode($output);
        }

}

/* End of file admin_visitor_history.php */
/* Location: ./application/controllers/admin_visitor_history.php */