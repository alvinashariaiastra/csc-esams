<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class admin_lost_found extends PX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('text');
        $this->check_login();
        $this->controller_attr = array('controller' => 'admin_lost_found', 'controller_name' => 'Admin Lost & Found', 'controller_id' => 0);
    }

    public function index() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Lost & Found', 'admin_lost_found');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);
        /*$data['lost_found'] = $this->model_basic->select_all_data('px_lost_found')->result();
        //echo json_encode($data['lost_found']); die();
        $data['lost_found'] = $this->model_basic->select_row_by_query($sql);
        foreach ($data['lost_found'] as $row) {
            $row->user = $this->model_basic->select_wheres('px_user', 'id', $row->creator_id);
                //echo json_encode($data['user']); die();
           $row->date_created = date('j F Y', strtotime($row->date_created));
        }*/ 
        $sql = '
                SELECT a.*, b.realname 
                from 
                    px_lost_found a
                JOIN px_user b on b.id = a.creator_id
                where 
                    a.delete_flag = 0';
        $data['lost_found'] = $this->model_basic->select_all_by_query($sql);
        //echo json_encode($data['lost_found']); die();
        $data['content'] = $this->load->view('backend/admin_lost_found/index', $data, true);
        $this->load->view('backend/index', $data);
    }

    function admin_lost_found_delete() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Lost & Found', 'admin_lost_found');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $do_delete = $this->model_basic->delete('px_lost_found', 'id', $id);
        if ($do_delete) {
            $this->model_basic->delete('px_lost_found_comments', 'lost_found_id', $id);
            $this->model_basic->delete('px_lost_found_images', 'lost_found_id', $id);
            $this->save_log_admin(ACT_DELETE, 'Delete Lost & Found');
            $this->delete_folder('lost_found/' . $id);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Delete Success', 'redirect' => $data['controller']));
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Delete Failed'));
    }

    function detail($id) {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Lost & Found', 'admin_lost_found');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $lost_found = $this->model_basic->select_where('px_lost_found', 'id', $id);

        foreach ($lost_found->result() as $row) {
            $row->date_created = date('j F Y', strtotime($row->date_created));
            $creator = $this->model_basic->select_where($this->tbl_user, 'id', $row->creator_id)->row();
            if ($creator)
                $row->realname = $creator->realname;
            else
                $row->realname = 'Administrator';
            $photos = $this->model_basic->select_where('px_lost_found_images', 'lost_found_id', $row->id);
            if ($photos->num_rows() != 0) {
                $row->photos = $photos;
            } else {
                $row->photos = array();
            }
            $comments = $this->model_basic->select_where('px_lost_found_comments', 'lost_found_id', $row->id);
            if ($comments->num_rows() != 0) {
                $row->comments = $comments->result();
                foreach ($row->comments as $data_row) {
                    $commentator = $this->model_basic->select_where($this->tbl_user, 'id', $data_row->user_id)->row();
                    if ($commentator)
                        $data_row->realname = $commentator->realname;
                    else
                        $data_row->realname = 'Administrator';
                    $data_row->date_created = date('j F Y', strtotime($data_row->date_created));
                }
            }
            else {
                $row->comments = array();
            }
        }

        $data['lost_found'] = $lost_found;
        $data['content'] = $this->load->view('backend/admin_lost_found/detail', $data, true);
        $this->load->view('backend/index', $data);
    }

    function submit_comments() {
        $lost_found_id = $this->input->post('lost_found_id');
        $comments = $this->input->post('comments');

        $temp = $this->model_basic->select_where('px_lost_found', 'id', $lost_found_id);
        if ($temp->num_rows() != 0) {
            $insert = array(
                'lost_found_id' => $lost_found_id,
                'user_id' => 0,
                'comments' => $comments,
                'date_created' => date('Y-m-d H:i:s', now())
            );
            if (!$this->model_basic->insert_all('px_lost_found_comments', $insert))
                redirect('admin_lost_found/detail/' . $lost_found_id . '?submit_comments=false');
            redirect('admin_lost_found/detail/' . $lost_found_id . '?submit_comments=success');
        } else
            redirect('admin_lost_found/detail/' . $lost_found_id . '?submit_comments=false');
    }

    function change_flag($id, $new_flag) {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Lost & Found', 'admin_lost_found');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);

        //for get email user
        //$con = mysqli_connect("10.248.141.132","csc","nasigoreng","cscprod");
        /*$con = mysqli_connect("localhost","root","","cscprod");
        $query = "SELECT * FROM px_lost_found a JOIN px_user b on a.creator_id = b.id where a.id = ".$id;
        $email_user = mysqli_query($con,$query);*/

        //print_r($query);die();
        //print_r($result);die();

        $lost_found = $this->model_basic->select_where('px_lost_found', 'id', $id);
        if ($lost_found->num_rows() == 1) {
            $update = array('flag' => $new_flag);
            if (!$this->model_basic->update('px_lost_found', $update, 'id', $id))
                redirect('admin_lost_found/detail/' . $id . '?update_flag=error');
            else {
                $this->save_log_admin(ACT_UPDATE, 'Update Lost & Found ' . $lost_found->row()->title);
                //Email Approve
                if ($new_flag == 1) {
                    $recipient = array();
                    //$recipient['user'] = $email_user;
                    $recipient['user'] = $this->model_basic->select_join_all($id)->result();
                    $recipient['admin'] = $this->model_basic->select_where_all($this->tbl_admin, 'id_usergroup', 18)->result(); // sent to admin with specific usergroup
                    $recipient['data'] = $this->model_basic->select_where('px_lost_found', 'id', $id)->result();
                    $this->send_email_lostfound_acc($recipient);
                }
                //Email Completed
                else if ($new_flag == 2) {
                    $recipient = array();
                    $recipient['user'] = $this->model_basic->select_join_all($id)->result();//get email user
                    $recipient['admin'] = $this->model_basic->select_where_all($this->tbl_admin, 'id_usergroup', 18)->result(); // sent to admin with specific usergroup
                    $recipient['data'] = $this->model_basic->select_where('px_lost_found', 'id', $id)->result();
                    $this->send_email_lostfound_cmp($recipient);
                }
                //Email Reject
                else if ($new_flag == 3) {
                    $recipient = array();
                    $recipient['user'] = $this->model_basic->select_join_all($id)->result();
                    $recipient['admin'] = $this->model_basic->select_where_all($this->tbl_admin, 'id_usergroup', 18)->result(); // sent to admin with specific usergroup
                    $recipient['data'] = $this->model_basic->select_where('px_lost_found', 'id', $id)->result();
                    $this->send_email_lostfound_rjc($recipient);
                }
                redirect('admin_lost_found/detail/' . $id . '?update_flag=success');
            }
        }
    }

}

/* End of file admin_lost_found.php */
/* Location: ./application/controllers/admin_lost_found.php */