<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_user extends PX_Controller {

    public function __construct() {
        parent::__construct();
        $this->check_login();
        $this->controller_attr = array('controller' => 'admin_user', 'controller_name' => 'Admin User', 'controller_id' => 0);
        /* 
            Add by: Alvin (atsalvin0017), Date: 2019-08-12 10:59 AM 
            to handle error mycrypt deprecated for php version 7
        */
        if (PHP_VERSION >= 7) {
            ini_set('display_errors', 0);
            error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);
        }
    }

    public function index() {
        $this->user_list();
    }

    //user_list//
    function user_list() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('User List', 'user_list');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        $data['content'] = $this->load->view('backend/admin_user/user_list', $data, true);
        $this->load->view('backend/index', $data);
    }

    function user_list_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('User List', 'user_list');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        $data['user_group_num_row'] = $this->model_basic->get_count($this->tbl_usergroup_user);
        $data['user_group'] = $this->model_basic->select_all($this->tbl_usergroup_user);

        $data['perusahaan_num_row'] = $this->model_basic->get_count($this->tbl_perusahaan);
        $data['perusahaan'] = $this->model_basic->select_all_order($this->tbl_perusahaan, 'name', 'asc');

        $data['jabatan_num_row'] = $this->model_basic->get_count($this->tbl_user_jabatan);
        $data['jabatan'] = $this->model_basic->select_all($this->tbl_user_jabatan);

        $id = $this->input->post('id');
        if ($id) {
            $data['data'] = $user = $this->model_basic->select_where($this->tbl_user, 'id', $id)->row();
            $data['instalasi'] = $this->model_basic->select_where($this->tbl_instalasi, 'perusahaan_id', $user->perusahaan_id)->result();
            $data['password'] = $this->encrypt->decode($user->password);
            $user->password = $this->encrypt->decode($user->password);
        } else {
            $data['data'] = null;
        }
        $data['content'] = $this->load->view('backend/admin_user/user_list_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function user_list_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('User List', 'user_list');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        $table_field = $this->db->list_fields($this->tbl_user);
        $img_name_crop = uniqid() . '-userprofile.jpg';
        $insert = array();
        foreach ($table_field as $field) {
            // Add by: Alvin (atsalvin0017), Date: 2019-08-12 10:59 AM
            if ($field!='id') { 
                $insert[$field] = $this->input->post($field);
            }
            // $insert[$field] = $this->input->post($field);
        }
        $insert['date_modified'] = date('Y-m-d H:i:s', now());
        $insert['photo'] = $img_name_crop;
        $password_check = $this->password_checker($this->input->post('password'));
        if ($password_check->status == TRUE) {
            $insert['password'] = $this->encrypt->encode($this->input->post('password'));
            //$insert['date_created'] = date('Y-m-d H:i:s',now());
            //$insert['id_created'] = $this->session_admin['admin_id'];
            if ($insert['username'] && $this->input->post('photo')) {
                $do_insert = $this->model_basic->insert_all($this->tbl_user, $insert);
                if ($do_insert) {

                    $origw = $this->input->post('origwidth');
                    $origh = $this->input->post('origheight');
                    $fakew = $this->input->post('fakewidth');
                    $fakeh = $this->input->post('fakeheight');
                    $x = $this->input->post('x') * $origw / $fakew;
                    $y = $this->input->post('y') * $origh / $fakeh;
                    # ambil width crop
                    $targ_w = $this->input->post('w') * $origw / $fakew;
                    # ambil heigth crop
                    $targ_h = $this->input->post('h') * $origh / $fakeh;
                    # rasio gambar crop
                    $jpeg_quality = 100;
                    if (!is_dir(FCPATH . 'assets/uploads/user/' . $do_insert->id))
                        mkdir(FCPATH . 'assets/uploads/user/' . $do_insert->id);
                    if (basename($this->input->post('photo')) && $this->input->post('photo') != null) {
                        $src = $this->input->post('photo');
                    }
                    # inisial handle copy gambar
                    $ext = pathinfo($src, PATHINFO_EXTENSION);

                    if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'JPG' || $ext == 'JPEG')
                        $img_r = imagecreatefromjpeg($src);
                    if ($ext == 'png' || $ext == 'PNG')
                        $img_r = imagecreatefrompng($src);
                    if ($ext == 'gif' || $ext == 'GIF')
                        $img_r = imagecreatefromgif($src);

                    $dst_r = ImageCreateTrueColor($targ_w, $targ_h);
                    # simpan hasil croping pada folder lain
                    $path_img_crop = realpath(FCPATH . 'assets/uploads/user/' . $do_insert->id);
                    # nama gambar yg di crop
                    # proses copy
                    imagecopyresampled($dst_r, $img_r, 0, 0, $x, $y, $targ_w, $targ_h, $targ_w, $targ_h);
                    # buat gambar
                    if (!imagejpeg($dst_r,$path_img_crop .'/'. $img_name_crop,$jpeg_quality)) {
                        $this->model_basic->delete($this->tbl_user, 'id', $do_insert->id);
                        $this->delete_folder('user/' . $do_insert->id);
                        $this->returnJson(array('status' => 'error', 'msg' => 'Upload Falied'));
                    } else {
                        $this->delete_temp('temp_folder');
                        $this->save_log_admin(ACT_CREATE, 'Insert New User ' . $insert['realname']);
                        //$this->save_log_admin(ACT_Create, 'Create User');
                        $this->returnJson(array('status' => 'ok', 'msg' => 'Input data success', 'redirect' => $data['controller'] . '/' . $data['function']));
                    }
                } else
                    $this->returnJson(array('status' => 'error', 'msg' => 'Failed when saving data'));
            } else
                $this->returnJson(array('status' => 'error', 'msg' => 'Please complete the form'));
        }
        else {
            $this->returnJson(array('status' => 'error', 'msg' => $password_check->msg));
        }
    }

    function user_list_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('User List', 'user_list');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);

        $foto = $this->input->post('photo');
        $old_foto = $this->input->post('old_photo');
        $img_name_crop = uniqid() . '-userprofile.jpg';
        $table_field = $this->db->list_fields($this->tbl_user);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }
        $update['date_modified'] = date('Y-m-d H:i:s', now());
        $password_check = $this->password_checker($this->input->post('password'));
        if ($password_check->status == TRUE) {
            $update['password'] = $this->encrypt->encode($this->input->post('password'));
            //unset($update['id_created']);
            //unset($update['date_created']);
            if (($foto && (basename($foto) != $old_foto)) || ($this->input->post('x') || $this->input->post('y') || $this->input->post('w') || $this->input->post('h')))
                $update['photo'] = $img_name_crop;
            else
                $update['photo'] = $this->input->post('old_photo');
            //$update['date_modified'] = date('Y-m-d H:i:s',now());
            //$update['id_modified'] = $this->session_admin['admin_id'];
            //die($old_photo." ".$file_nm);
            if ($update['username']) {
                $do_update = $this->model_basic->update($this->tbl_user, $update, 'id', $update['id']);
                if ($do_update) {
                    if (($foto && (basename($foto) != $old_foto)) || ($this->input->post('x') || $this->input->post('y') || $this->input->post('w') || $this->input->post('h'))) {
                        $origw = $this->input->post('origwidth');
                        $origh = $this->input->post('origheight');
                        $fakew = $this->input->post('fakewidth');
                        $fakeh = $this->input->post('fakeheight');
                        $x = $this->input->post('x') * $origw / $fakew;
                        $y = $this->input->post('y') * $origh / $fakeh;
                        # ambil width crop
                        $targ_w = $this->input->post('w') * $origw / $fakew;
                        # abmil heigth crop
                        $targ_h = $this->input->post('h') * $origh / $fakeh;
                        # rasio gambar crop
                        $jpeg_quality = 100;
                        if (!is_dir(FCPATH . 'assets/uploads/user/' . $update['id']))
                            mkdir(FCPATH . 'assets/uploads/user/' . $update['id']);

                        if (basename($foto) && $foto != null)
                            $src = $this->input->post('photo');
                        else if ($this->input->post('x') || $this->input->post('y') || $this->input->post('w') || $this->input->post('h'))
                            $src = "assets/uploads/user/" . $update['id'] . '/' . $old_foto;
                        # inisial handle copy gambar
                        $ext = pathinfo($src, PATHINFO_EXTENSION);

                        if ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'JPG' || $ext == 'JPEG')
                            $img_r = imagecreatefromjpeg($src);
                        if ($ext == 'png' || $ext == 'PNG')
                            $img_r = imagecreatefrompng($src);
                        if ($ext == 'gif' || $ext == 'GIF')
                            $img_r = imagecreatefromgif($src);

                        $dst_r = ImageCreateTrueColor($targ_w, $targ_h);
                        # simpan hasil croping pada folder lain
                        $path_img_crop = realpath(FCPATH . "assets/uploads/user/" . $update['id']);
                        # nama gambar yg di crop
                        # proses copy
                        imagecopyresampled($dst_r, $img_r, 0, 0, $x, $y, $targ_w, $targ_h, $targ_w, $targ_h);
                        # buat gambar
                        if (imagejpeg($dst_r, $path_img_crop . '/' . $img_name_crop, $jpeg_quality))
                            @unlink('assets/uploads/user/' . $update['id'] . '/' . $this->input->post('old_photo'));
                        $this->delete_temp('temp_folder');
                    }
                    $this->save_log_admin(ACT_UPDATE, 'Update User ' . $update['realname']);
                    $this->returnJson(array('status' => 'ok', 'msg' => 'Update success', 'redirect' => $data['controller'] . '/' . $data['function']));
                }else {
                    $this->returnJson(array('status' => 'error', 'msg' => 'Failed when updating data'));
                }
            } else
                $this->returnJson(array('status' => 'error', 'msg' => 'Please complete the form'));
        } else
            $this->returnJson(array('status' => 'error', 'msg' => $password_check->msg));
    }

    function user_list_delete() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('User List', 'user_list');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $do_delete = $this->model_basic->delete($this->tbl_user, 'id', $id);
        if ($do_delete) {
            $this->save_log_admin(ACT_DELETE, 'Delete User');
            $this->delete_folder('user_list/' . $id);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Delete Success', 'redirect' => $data['controller'] . '/' . $data['function']));
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Delete Failed'));
    }

    /* 
        Update by: Alvin (atsalvin0017), Date: 2019-08-12 10:30 AM 
        Note: This function conflict with px_controller
    */
    function makeThumbnails_old($updir, $img) {
        $thumbnail_width = 134;
        $thumbnail_height = 189;
        $thumb_beforeword = "thumb";
        $arr_image_details = getimagesize("$updir" . "$img"); // pass id to thumb name
        $original_width = $arr_image_details[0];
        $original_height = $arr_image_details[1];
        if ($original_width > $original_height) {
            $new_width = $thumbnail_width;
            $new_height = intval($original_height * $new_width / $original_width);
        } else {
            $new_height = $thumbnail_height;
            $new_width = intval($original_width * $new_height / $original_height);
        }
        $dest_x = intval(($thumbnail_width - $new_width) / 2);
        $dest_y = intval(($thumbnail_height - $new_height) / 2);
        if ($arr_image_details[2] == 1) {
            $imgt = "ImageGIF";
            $imgcreatefrom = "ImageCreateFromGIF";
        }
        if ($arr_image_details[2] == 2) {
            $imgt = "ImageJPEG";
            $imgcreatefrom = "ImageCreateFromJPEG";
        }
        if ($arr_image_details[2] == 3) {
            $imgt = "ImagePNG";
            $imgcreatefrom = "ImageCreateFromPNG";
        }
        if ($imgt) {
            $old_image = $imgcreatefrom("$updir" . "$img");
            $new_image = imagecreatetruecolor($thumbnail_width, $thumbnail_height);
            imagecopyresized($new_image, $old_image, $dest_x, $dest_y, 0, 0, $new_width, $new_height, $original_width, $original_height);
            $imgt($new_image, "$updir" . "$thumb_beforeword" . "$img");
        }
    }

    function admin_user_check_username() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin User', 'user_list');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);
        $id = $this->input->post('id');
        $username = $this->input->post('username');
        if ($id == null || $id == '') {
            $check = $this->model_basic->select_where($this->tbl_user, 'username', $username)->num_rows();
            if ($check == 0)
                echo 'true';
            else
                echo 'false';
        }
        else {
            $now = $this->model_basic->select_where_array($this->tbl_user, 'id != ' . $id . ' and username = "' . $username . '"')->num_rows();
            if ($now == 0)
                echo 'true';
            else {
                echo 'false';
            }
        }
    }

    function admin_user_check_email() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Admin User', 'user_list');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);
        $id = $this->input->post('id');
        $email = $this->input->post('email');
        if ($id == null || $id == '') {
            $check = $this->model_basic->select_where($this->tbl_admin, 'email', $email)->num_rows();
            if ($check == 0)
                echo 'true';
            else
                echo 'false';
        }
        else {
            $now = $this->model_basic->select_where_array($this->tbl_admin, 'id != ' . $id . ' and email = "' . $email . '"')->num_rows();
            if ($now == 0)
                echo 'true';
            else {
                echo 'false';
            }
        }
    }

    public function ajax_user_list_list() {
        $list = $this->model_user_list->get_datatables();
        //die(print_r($this->db->last_query()));
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $data_row) {
            //render data
            $no++;
            if ($data_row->level_user_id == 1)
                $level_user = 'Management';
            else if ($data_row->level_user_id == 2)
                $level_user = 'Middle Management';
            else if ($data_row->level_user_id == 3)
                $level_user = 'Operational';
            else if ($data_row->level_user_id == 4)
                $level_user = 'Guard';
            $row = array();
            $row[] = $no;
            $row[] = $data_row->realname;
            $row[] = $data_row->user_group;
            $row[] = $level_user;
            $row[] = $data_row->perusahaan;
            $row[] = $data_row->instalasi;
            $row[] = $data_row->jabatan;
            //add html for action
            $row[] = '<div class="text-center">
						<form action="admin_user/user_list_form" method="post">
						<input type="hidden" name="id" value="' . $data_row->id . '">
						<button class="btn btn-info btn-xs btn-edit" type="submit" data-original-title="Update" data-placement="top" data-toggle="tooltip"><i class="fa fa-edit"></i></button>
						<button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Delete" data-placement="top" data-toggle="tooltip" data-target-id="' . $data_row->id . '"><i class="fa fa-trash-o"></i></button>
						</form>
						</div>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model_user_list->count_all(),
            "recordsFiltered" => $this->model_user_list->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    //user jabatan//
    function user_jabatan() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('User Jabatan', 'user_jabatan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_READ);

        //$data['data'] = $this->model_basic->select_all($this->tbl_user);
        $data['content'] = $this->load->view('backend/admin_user/user_jabatan', $data, true);
        $this->load->view('backend/index', $data);
    }

    function user_jabatan_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('User Jabatan', 'user_jabatan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        $id = $this->input->post('id');
        if ($id) {
            $data['data'] = $user = $this->model_basic->select_where($this->tbl_user_jabatan, 'id', $id)->row();
        } else {
            $data['data'] = null;
        }
        $data['content'] = $this->load->view('backend/admin_user/user_jabatan_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function user_jabatan_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('User Jabatan', 'user_jabatan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_CREATE);

        $table_field = $this->db->list_fields($this->tbl_user_jabatan);
        $insert = array();
        foreach ($table_field as $field) {
            $insert[$field] = $this->input->post($field);
        }
        $insert['date_created'] = date('Y-m-d H:i:s', now());
        $insert['id_created'] = $this->session_admin['admin_id'];
        if ($insert['jabatan']) {
            $do_insert = $this->model_basic->insert_all($this->tbl_user_jabatan, $insert);
            if ($do_insert) {
                $this->save_log_admin(ACT_CREATE, 'Insert New Jabatan User ' . $insert['jabatan']);
                $this->returnJson(array('status' => 'ok', 'msg' => 'Input data success', 'redirect' => $data['controller'] . '/' . $data['function']));
            } else
                $this->returnJson(array('status' => 'error', 'msg' => 'Failed when saving data'));
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Please complete the form'));
    }

    function user_jabatan_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('User Jabatan', 'user_jabatan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);

        $table_field = $this->db->list_fields($this->tbl_user_jabatan);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }
        unset($update['date_created']);
        $update['date_modified'] = date('Y-m-d H:i:s', now());
        $update['id_modified'] = $this->session_admin['admin_id'];

        if ($update['jabatan']) {
            $do_update = $this->model_basic->update($this->tbl_user_jabatan, $update, 'id', $update['id']);
            if ($do_update) {
                $this->save_log_admin(ACT_UPDATE, 'Update Jabatan User ' . $update['jabatan']);
                $this->returnJson(array('status' => 'ok', 'msg' => 'Update success', 'redirect' => $data['controller'] . '/' . $data['function']));
            } else
                $this->returnJson(array('status' => 'error', 'msg' => 'Failed when updating data'));
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Please complete the form'));
    }

    function user_jabatan_delete() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('User Jabatan', 'user_jabatan');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $do_delete = $this->model_basic->delete($this->tbl_user_jabatan, 'id', $id);
        if ($do_delete) {
            $this->save_log_admin(ACT_DELETE, 'Delete Jabatan User');
            $this->delete_folder('user_jabatan/' . $id);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Delete Success', 'redirect' => $data['controller'] . '/' . $data['function']));
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Delete Failed'));
    }

    public function ajax_user_jabatan_list() {
        $column = array('id');
        $list = $this->model_user_jabatan->get_datatables($this->tbl_user_jabatan, $column);
        //die(print_r($this->db->last_query()));
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $data_row) {
            //render data
            $no++;

            $row = array();
            $row[] = $no;
            $row[] = $data_row->jabatan;
            //add html for action
            $row[] = '<div class="text-center">
						<form action="admin_user/user_jabatan_form" method="post">
						<input type="hidden" name="id" value="' . $data_row->id . '">
						<button class="btn btn-info btn-xs btn-edit" type="submit" data-original-title="Update" data-placement="top" data-toggle="tooltip"><i class="fa fa-edit"></i></button>
						<button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Delete" data-placement="top" data-toggle="tooltip" data-target-id="' . $data_row->id . '"><i class="fa fa-trash-o"></i></button>
						</form>
						</div>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model_user_jabatan->count_all($this->tbl_user),
            "recordsFiltered" => $this->model_user_jabatan->count_filtered($this->tbl_user, $column),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_get_instalasi() {
        $perusahaan_id = $this->input->post('id');

        $results = $this->model_basic->select_where_order($this->tbl_instalasi, 'perusahaan_id', $perusahaan_id, 'name', 'asc');
        if ($results->num_rows() > 0) {
            $this->returnJson(array('status' => 'ok', 'result' => $results->result()));
        } else {
            $this->returnJson(array('status' => 'not_ok'));
        }
    }

}

/* End of file admin_user.php */
/* Location: ./application/controllers/admin_user.php */