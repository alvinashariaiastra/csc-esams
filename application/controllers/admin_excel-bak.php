<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_excel extends PX_Controller {

    public function __construct() {
        parent::__construct();
        $this->check_login();
        $this->controller_attr = array('controller' => 'admin_excel', 'controller_name' => 'Admin Excel', 'controller_id' => 0);
    }

    function export_company() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('List Perusahaan', 'company_list');
        $this->check_userakses($data['function_id'], ACT_READ);

        $company = $this->model_basic->select_all($this->tbl_perusahaan);

        $this->load->library('excel');
        $objPHPExcel = new PHPExcel();

        $sheet_name = 'data_perusahaan';
        $file_name = 'data_perusahaan';

        $objPHPExcel->getProperties()->setTitle($sheet_name)->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);
        $no = 1;
        $fields = array(
            'Row_ID',
            'Jenis Bisnis',
            'Nama Perusahaan',
            'Alamat Perusahaan',
            'Phone',
            'Mobile Phone',
            'Karyawan Tetap',
            'Karyawan Tidak Tetap',
            'Security Organik',
            'Security Outsourching',
            'Serikat Pekerja',
            'Jumlah Anggota SP',
            'Pimpinan',
            'Manager',
            'PIC',
            'Koordinator Satpam');
        $col = 0;
        foreach ($fields as $field) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, 1)->getFont()->setSize(11);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, 1)->getFont()->setBold(true);
            // $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            $col++;
        }
        $row = 2;
        foreach ($company as $data) {
            $jenis_bisnis = $this->model_basic->select_where($this->tbl_jenis_bisnis, 'id', $data->jenis_bisnis_id);
            if ($jenis_bisnis->num_rows() == 1)
                $jb = $jenis_bisnis->row()->name;
            else
                $jb = '';
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $data->id);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $jb);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $row, $data->name);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $row, $data->alamat);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $row, $data->telp);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, $data->mobile_phone);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $row, $data->jumlah_karyawan_tetap);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $data->jumlah_karyawan_tidak_tetap);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $row, $data->jumlah_personil_security_organik);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $row, $data->jumlah_personil_security_outsourching);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $row, $data->nama_serikat_pekerja);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $row, $data->jumlah_anggota_serikat_pekerja);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $row, $data->jabatan_1);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $row, $data->jabatan_2);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $row, $data->jabatan_3);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, $row, $data->jabatan_4);
            $row++;
            $no++;
        }
        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $filename = $file_name . '.xls';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }

    function export_instalasi() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('List Instalasi', 'instalasi');
        $this->check_userakses($data['function_id'], ACT_READ);

        $company = $this->model_basic->select_all($this->tbl_instalasi);

        $this->load->library('excel');
        $objPHPExcel = new PHPExcel();

        $sheet_name = 'data_instalasi';
        $file_name = 'data_instalasi';

        $objPHPExcel->getProperties()->setTitle($sheet_name)->setDescription("none");
        $objPHPExcel->setActiveSheetIndex(0);
        $no = 1;
        $fields = array(
            'Row_ID',
            'Jenis Bisnis',
            'Perusahaan',
            'Nama Instalasi',
            'Alamat Instalasi',
            'Provinsi',
            'Koordinat X (Lat)',
            'Koordinat Y (Long)',
            'Phone',
            'Mobile Phone',
            'Karyawan Tetap',
            'Karyawan Tidak Tetap',
            'Security Organik',
            'Security Outsourching',
            'Serikat Pekerja',
            'Jumlah Anggota SP',
            'Pimpinan',
            'Manager',
            'PIC',
            'Koordinator Satpam');
        $col = 0;
        foreach ($fields as $field) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, 1)->getFont()->setSize(11);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($col, 1)->getFont()->setBold(true);
            // $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            $col++;
        }
        $row = 2;
        foreach ($company as $data) {
            $jenis_bisnis = $this->model_basic->select_where($this->tbl_jenis_bisnis, 'id', $data->jenis_bisnis_id);
            if ($jenis_bisnis->num_rows() == 1)
                $jb = $jenis_bisnis->row()->name;
            else
                $jb = '';
            $perusahaan = $this->model_basic->select_where($this->tbl_perusahaan, 'id', $data->perusahaan_id);
            if ($perusahaan->num_rows() == 1)
                $ps = $perusahaan->row()->name;
            else
                $ps = '';
            $province = $this->model_basic->select_where($this->tbl_province, 'id', $data->province_id);
            if ($province->num_rows() == 1)
                $pv = $province->row()->name;
            else
                $pv = '';
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $data->id);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, $jb);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $row, $ps);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $row, $data->name);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $row, $data->alamat);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, $pv);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $row, $data->coordinate_x);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, $data->coordinate_y);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $row, $data->telp);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $row, $data->mobile_phone);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $row, $data->jumlah_karyawan_tetap);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $row, $data->jumlah_karyawan_tidak_tetap);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $row, $data->jumlah_personil_security_organik);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $row, $data->jumlah_personil_security_outsourching);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $row, $data->nama_serikat_pekerja);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(15, $row, $data->jumlah_anggota_serikat_pekerja);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(16, $row, $data->jabatan_1);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(17, $row, $data->jabatan_2);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(18, $row, $data->jabatan_3);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(19, $row, $data->jabatan_4);
            $row++;
            $no++;
        }
        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $filename = $file_name . '.xls';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }

    function import_company_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('List Perusahaan', 'company_list');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);
        $data['content'] = $this->load->view('backend/admin_excel/upload_company_form', $data, true);
        $this->load->view('backend/index', $data);
    }
    
    function import_instalasi_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('List Instalasi', 'instalasi');
        $data += $this->get_menu();
        $this->check_userakses($data['function_id'], ACT_UPDATE);
        $data['content'] = $this->load->view('backend/admin_excel/upload_instalasi_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function import_company() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('List Perusahaan', 'company_list');
        $this->check_userakses($data['function_id'], ACT_UPDATE);

        set_time_limit(0);
        $file = $this->input->post('files');
        $this->load->library('excel');
        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($file);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($file);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();

        //  Loop through each row of the worksheet in turn
        for ($row = 2; $row <= $highestRow; $row++) {
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':L' . $row, '', TRUE, FALSE);
            //  Insert row data array into your database of choice here
            $data_object = new stdClass();
            $data_object->id = $rowData[0][0];
            $data_object->jenis_bisnis = $rowData[0][1];
            $data_object->name = $rowData[0][2];
            $data_object->alamat = $rowData[0][3];
            $data_object->telp = $rowData[0][4];
            $data_object->mobile_phone = $rowData[0][5];
            $data_object->jumlah_karyawan_tetap = $rowData[0][6];
            $data_object->jumlah_karyawan_tidak_tetap = $rowData[0][7];
            $data_object->jumlah_personil_security_organik = $rowData[0][8];
            $data_object->jumlah_personil_security_outsourching = $rowData[0][9];
            $data_object->nama_serikat_pekerja = $rowData[0][10];
            $data_object->jumlah_anggota_serikat_pekerja = $rowData[0][11];
            $data_object->jabatan_1 = $rowData[0][12];
            $data_object->jabatan_2 = $rowData[0][13];
            $data_object->jabatan_3 = $rowData[0][14];
            $data_object->jabatan_4 = $rowData[0][15];

            $this->db->trans_start();
            $temp = $this->model_basic->select_where($this->tbl_jenis_bisnis, 'name', $data_object->jenis_bisnis);
            if ($temp->num_rows() == 1) {
                $data_object->jenis_bisnis_id = $temp->row()->id;
                if ($data_object->id != '') {
                    $site = $this->model_basic->select_where($this->tbl_perusahaan, 'id', $data_object->id);
                    if ($site->num_rows() == 1) {
                        $data_update = array(
                            'jenis_bisnis_id' => $data_object->jenis_bisnis_id,
                            'name' => $data_object->name,
                            'alamat' => $data_object->alamat,
                            'telp' => $data_object->telp,
                            'mobile_phone' => $data_object->mobile_phone,
                            'jumlah_karyawan_tetap' => $data_object->jumlah_karyawan_tetap,
                            'jumlah_karyawan_tidak_tetap' => $data_object->jumlah_karyawan_tidak_tetap,
                            'jumlah_personil_security_organik' => $data_object->jumlah_personil_security_organik,
                            'jumlah_personil_security_outsourching' => $data_object->jumlah_personil_security_outsourching,
                            'nama_serikat_pekerja' => $data_object->nama_serikat_pekerja,
                            'jumlah_anggota_serikat_pekerja' => $data_object->jumlah_anggota_serikat_pekerja,
                            'jabatan_1' => $data_object->jabatan_1,
                            'jabatan_2' => $data_object->jabatan_2,
                            'jabatan_3' => $data_object->jabatan_3,
                            'jabatan_4' => $data_object->jabatan_4);
                        $this->model_basic->update($this->tbl_perusahaan, $data_update, 'id', $data_object->id);
                    }
                } else {
                    $data_insert = array(
                            'jenis_bisnis_id' => $data_object->jenis_bisnis_id,
                            'name' => $data_object->name,
                            'alamat' => $data_object->alamat,
                            'telp' => $data_object->telp,
                            'mobile_phone' => $data_object->mobile_phone,
                            'jumlah_karyawan_tetap' => $data_object->jumlah_karyawan_tetap,
                            'jumlah_karyawan_tidak_tetap' => $data_object->jumlah_karyawan_tidak_tetap,
                            'jumlah_personil_security_organik' => $data_object->jumlah_personil_security_organik,
                            'jumlah_personil_security_outsourching' => $data_object->jumlah_personil_security_outsourching,
                            'nama_serikat_pekerja' => $data_object->nama_serikat_pekerja,
                            'jumlah_anggota_serikat_pekerja' => $data_object->jumlah_anggota_serikat_pekerja,
                            'jabatan_1' => $data_object->jabatan_1,
                            'jabatan_2' => $data_object->jabatan_2,
                            'jabatan_3' => $data_object->jabatan_3,
                            'jabatan_4' => $data_object->jabatan_4);
                    $this->model_basic->insert_all($this->tbl_perusahaan, $data_insert);
                }
            }
            $this->db->trans_complete();
        }
        if ($this->db->trans_status() != FALSE) {
            $this->delete_temp('temp_folder');
            $this->returnJson(array('status' => 'ok', 'redirect' => 'admin_company/company_list?import=success', 'msg' => 'Import Success'));
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Failed when Importing data, try Again'));
    }
    
    function import_instalasi() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('List Instalasi', 'instalasi');
        $this->check_userakses($data['function_id'], ACT_UPDATE);

        set_time_limit(0);
        $file = $this->input->post('files');
        $this->load->library('excel');
        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($file);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($file);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();

        //  Loop through each row of the worksheet in turn
        for ($row = 2; $row <= $highestRow; $row++) {
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':P' . $row, '', TRUE, FALSE);
            //  Insert row data array into your database of choice here
            $data_object = new stdClass();
            $data_object->id = $rowData[0][0];
            $data_object->jenis_bisnis = $rowData[0][1];
            $data_object->perusahaan = $rowData[0][2];
            $data_object->name = $rowData[0][3];
            $data_object->alamat = $rowData[0][4];
            $data_object->province = $rowData[0][5];
            $data_object->coordinate_x = $rowData[0][6];
            $data_object->coordinate_y = $rowData[0][7];
            $data_object->telp = $rowData[0][8];
            $data_object->mobile_phone = $rowData[0][9];
            $data_object->jumlah_karyawan_tetap = $rowData[0][10];
            $data_object->jumlah_karyawan_tidak_tetap = $rowData[0][11];
            $data_object->jumlah_personil_security_organik = $rowData[0][12];
            $data_object->jumlah_personil_security_outsourching = $rowData[0][13];
            $data_object->nama_serikat_pekerja = $rowData[0][14];
            $data_object->jumlah_anggota_serikat_pekerja = $rowData[0][15];
            $data_object->jabatan_1 = $rowData[0][16];
            $data_object->jabatan_2 = $rowData[0][17];
            $data_object->jabatan_3 = $rowData[0][18];
            $data_object->jabatan_4 = $rowData[0][19];

            $this->db->trans_start();
            $temp1 = $this->model_basic->select_where($this->tbl_jenis_bisnis, 'name', $data_object->jenis_bisnis);
            $temp2 = $this->model_basic->select_where($this->tbl_perusahaan, 'name', $data_object->perusahaan);
            $temp3 = $this->model_basic->select_where($this->tbl_province, 'name', $data_object->province);
            if ($temp1->num_rows() == 1 && $temp2->num_rows() == 1 && $temp3->num_rows() == 1) {
                $data_object->jenis_bisnis_id = $temp1->row()->id;
                $data_object->perusahaan_id = $temp2->row()->id;
                $data_object->province_id = $temp3->row()->id;
                if ($data_object->id != '') {
                    $site = $this->model_basic->select_where($this->tbl_instalasi, 'id', $data_object->id);
                    if ($site->num_rows() == 1) {
                        $data_update = array(
                            'jenis_bisnis_id' => $data_object->jenis_bisnis_id,
                            'name' => $data_object->name,
                            'alamat' => $data_object->alamat,
                            'telp' => $data_object->telp,
                            'mobile_phone' => $data_object->mobile_phone,
                            'jumlah_karyawan_tetap' => $data_object->jumlah_karyawan_tetap,
                            'jumlah_karyawan_tidak_tetap' => $data_object->jumlah_karyawan_tidak_tetap,
                            'jumlah_personil_security_organik' => $data_object->jumlah_personil_security_organik,
                            'jumlah_personil_security_outsourching' => $data_object->jumlah_personil_security_outsourching,
                            'nama_serikat_pekerja' => $data_object->nama_serikat_pekerja,
                            'jumlah_anggota_serikat_pekerja' => $data_object->jumlah_anggota_serikat_pekerja,
                            'perusahaan_id' => $data_object->perusahaan_id,
                            'province_id' => $data_object->province_id,
                            'coordinate_x' => $data_object->coordinate_x,
                            'coordinate_y' => $data_object->coordinate_y,
                            'jabatan_1' => $data_object->jabatan_1,
                            'jabatan_2' => $data_object->jabatan_2,
                            'jabatan_3' => $data_object->jabatan_3,
                            'jabatan_4' => $data_object->jabatan_4);
                        $this->model_basic->update($this->tbl_instalasi, $data_update, 'id', $data_object->id);
                    }
                } else {
                    $data_insert = array(
                            'jenis_bisnis_id' => $data_object->jenis_bisnis_id,
                            'name' => $data_object->name,
                            'alamat' => $data_object->alamat,
                            'telp' => $data_object->telp,
                            'mobile_phone' => $data_object->mobile_phone,
                            'jumlah_karyawan_tetap' => $data_object->jumlah_karyawan_tetap,
                            'jumlah_karyawan_tidak_tetap' => $data_object->jumlah_karyawan_tidak_tetap,
                            'jumlah_personil_security_organik' => $data_object->jumlah_personil_security_organik,
                            'jumlah_personil_security_outsourching' => $data_object->jumlah_personil_security_outsourching,
                            'nama_serikat_pekerja' => $data_object->nama_serikat_pekerja,
                            'jumlah_anggota_serikat_pekerja' => $data_object->jumlah_anggota_serikat_pekerja,
                            'perusahaan_id' => $data_object->perusahaan_id,
                            'province_id' => $data_object->province_id,
                            'coordinate_x' => $data_object->coordinate_x,
                            'coordinate_y' => $data_object->coordinate_y,
                            'jabatan_1' => $data_object->jabatan_1,
                            'jabatan_2' => $data_object->jabatan_2,
                            'jabatan_3' => $data_object->jabatan_3,
                            'jabatan_4' => $data_object->jabatan_4);
                    $this->model_basic->insert_all($this->tbl_instalasi, $data_insert);
                }
            }
            $this->db->trans_complete();
        }
        if ($this->db->trans_status() != FALSE) {
            $this->delete_temp('temp_folder');
            $this->returnJson(array('status' => 'ok', 'redirect' => 'admin_company/instalasi?import=success', 'msg' => 'Import Success'));
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Failed when Importing data, try Again'));
    }

}
