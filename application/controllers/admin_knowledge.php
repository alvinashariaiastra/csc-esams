<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_knowledge extends PX_Controller {

	public function __construct() {
		parent::__construct();
		$this->check_login();
		$this->controller_attr = array('controller' => 'admin_knowledge','controller_name' => 'Admin Knowledge','controller_id' => 0);
	}

	public function index()
	{
		$this->knowledge();
	}

	//knowledge//
	function knowledge()
	{
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Knowledge','knowledge');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);

		//$data['data'] = $this->model_basic->select_all($this->tbl_knowledge_base);
		$data['content'] = $this->load->view('backend/admin_knowledge/knowledge',$data,true);
		$this->load->view('backend/index',$data); 
	}

	function knowledge_form(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Knowledge','knowledge');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);
		$data['knowledge_category'] = $this->model_basic->select_where($this->tbl_knowledge_base_category,'delete_flag',0);
		$id = $this->input->post('id');
		if($id){
			$data['data'] = $this->model_basic->select_where($this->tbl_knowledge_base,'id',$id)->row();
		}
		else{
			$data['data'] = null;
		}
		$data['content'] = $this->load->view('backend/admin_knowledge/knowledge_form',$data,true);
		$this->load->view('backend/index',$data); 
    }

    function knowledge_add(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Knowledge','knowledge');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);

		$table_field = $this->db->list_fields($this->tbl_knowledge_base);
		$insert = array();
		foreach ($table_field as $field) {
			$insert[$field] = $this->input->post($field);
		}
		$insert['file'] = $file_nm = $this->input->post('knowledge_file');
		$insert['date_created'] = date('Y-m-d H:i:s',now());
		$insert['id_created'] = $this->session_admin['admin_id'];
		if($insert['name']){
			$do_insert = $this->model_basic->insert_all($this->tbl_knowledge_base,$insert);
			if($do_insert){
				if(!is_dir(FCPATH . 'assets/uploads/knowledge/'.$do_insert->id))
					mkdir(FCPATH . 'assets/uploads/knowledge/'.$do_insert->id);
				$src = $this->input->post('file_src');
				$dst_path = realpath(FCPATH . 'assets/uploads/knowledge/'.$do_insert->id);
				$dst = $dst_path.'/'.$file_nm;
				if (! copy($src, $dst)) {
					$this->model_basic->delete($this->tbl_knowledge_base,'id',$do_insert->id);
					$this->delete_folder('knowledge/'.$do_insert->id);
					$this->returnJson(array('status' => 'error','msg' => 'Upload Falied'));
				}else{
                                        $this->save_log_admin(ACT_CREATE, 'Insert New Knowledge '.$insert['name']);
					$this->delete_temp('temp_folder');
					$this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => $data['controller'].'/'.$data['function']));
				}
			}
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function knowledge_edit(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Knowledge','knowledge');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_UPDATE);
		
		$old_file = $this->input->post('old_file');
		$file_nm = $this->input->post('knowledge_file');
		$table_field = $this->db->list_fields($this->tbl_knowledge_base);
		$update = array();
		foreach ($table_field as $field) {
			$update[$field] = $this->input->post($field);
		}
		unset($update['id_created']);
		unset($update['date_created']);
		if ($file_nm != $old_file && $file_nm != "") {
			$update['file'] = $file_nm;
		}else{
			$update['file'] = $old_file;
		}
		$update['date_modified'] = date('Y-m-d H:i:s',now());
		$update['id_modified'] = $this->session_admin['admin_id'];
		//die($old_file." ".$file_nm);
		if($update['name']){
			$do_update = $this->model_basic->update($this->tbl_knowledge_base,$update,'id',$update['id']);
			if($do_update){
				if ($file_nm != $old_file && $file_nm != "") {
					if(!is_dir(FCPATH . 'assets/uploads/knowledge/'.$update['id']))
					mkdir(FCPATH . 'assets/uploads/knowledge/'.$update['id']);
					$src = $this->input->post('file_src');
					$dst_path = realpath(FCPATH . 'assets/uploads/knowledge/'.$update['id']);
					$dst = $dst_path.'/'.$file_nm;
					if (! copy($src, $dst)) {
						$this->model_basic->delete($this->tbl_knowledge_base,'id',$update['id']);
						$this->delete_folder('knowledge/'.$update['id']);
						$this->returnJson(array('status' => 'error','msg' => 'Upload Falied'));
					}else{
						$this->delete_temp('temp_folder');
					}
				}
                                $this->save_log_admin(ACT_UPDATE, 'Update Knowledge '.$update['name']);
				$this->returnJson(array('status' => 'ok','msg' => 'Update success','redirect' => $data['controller'].'/'.$data['function']));
			}else{
				$this->returnJson(array('status' => 'error','msg' => 'Failed when updating data'));	
			}
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function knowledge_delete(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Knowledge','knowledge');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_DELETE);
		$id = $this->input->post('id');
		$do_delete = $this->model_basic->delete($this->tbl_knowledge_base,'id',$id);
		if($do_delete){
                        $this->save_log_admin(ACT_DELETE, 'Delete Knowledge');
			$this->delete_folder('knowledge/'.$id);
			$this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => $data['controller'].'/'.$data['function']));
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }

	public function ajax_knowledge_list()
	{
		$column = array('name');
		$list = $this->model_table->get_datatables($this->tbl_knowledge_base,$column);
		//die(print_r($list));
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $data_row) {
			//render data
			//$file = file_get_contents('/assets/uploads/knowledge/'.$data_row->id.'/'.$data_row->file);
			$no++;
			
			$row = array();
			$row[] = $no;
			$row[] = $data_row->name;
			$row[] = ($data_row->status == 1) ? 'Visible' : 'Hidden';

			$row[] = '<a href="admin_knowledge/download_file/'.$data_row->id.'/'.$data_row->file.'"><label class="btn btn-primary btn-download">Download</label></a>';
			//add html for action
			$row[] = '<div class="text-center">
						<form action="admin_knowledge/knowledge_form" method="post">
						<input type="hidden" name="id" value="'.$data_row->id.'">
						<button class="btn btn-info btn-xs btn-edit" type="submit" data-original-title="Update" data-placement="top" data-toggle="tooltip"><i class="fa fa-edit"></i></button>
						<button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Delete" data-placement="top" data-toggle="tooltip" data-target-id="'.$data_row->id.'"><i class="fa fa-trash-o"></i></button>
						</form>
						</div>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_table->count_all($this->tbl_knowledge_base),
						"recordsFiltered" => $this->model_table->count_filtered($this->tbl_knowledge_base,$column),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	function download_file($id,$file_nm) {
		$this->load->helper('download');
		$file = file_get_contents('assets/uploads/knowledge/'.$id.'/'.$file_nm);
		force_download($file_nm,$file);
	}
	//knowledge category//
	function knowledge_category()
	{
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Admin Knowledge Category','knowledge_category');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);

		$data['data'] = $this->model_basic->select_all($this->tbl_knowledge_base_category);
		$data['content'] = $this->load->view('backend/admin_knowledge/knowledge_category',$data,true);
		$this->load->view('backend/index',$data); 
	}

	function knowledge_category_form(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Admin Knowledge Category','knowledge_category');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);
		$id = $this->input->post('id');
		if($id){
			$data['data'] = $this->model_basic->select_where($this->tbl_knowledge_base_category,'id',$id)->row();
		}
		else{
			$data['data'] = null;
		}
		$data['content'] = $this->load->view('backend/admin_knowledge/knowledge_category_form',$data,true);
		$this->load->view('backend/index',$data); 
    }

    function knowledge_category_add(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Admin Knowledge Category','knowledge_category');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);

		$table_field = $this->db->list_fields($this->tbl_knowledge_base_category);
		$insert = array();
		foreach ($table_field as $field) {
			$insert[$field] = $this->input->post($field);
		}
		$insert['date_created'] = date('Y-m-d H:i:s',now());
		$insert['id_created'] = $this->session_admin['admin_id'];
		if($insert['name']){
			$do_insert = $this->model_basic->insert_all($this->tbl_knowledge_base_category,$insert);
			if($do_insert){
                                $this->save_log_admin(ACT_CREATE, 'Insert New Knowledge Category '.$insert['name']);
				$this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => $data['controller'].'/'.$data['function']));
			}
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function knowledge_category_edit(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Admin Knowledge Category','knowledge_category');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_UPDATE);
		
		$table_field = $this->db->list_fields($this->tbl_knowledge_base_category);
		$update = array();
		foreach ($table_field as $field) {
			$update[$field] = $this->input->post($field);
		}
		unset($update['date_created']);
		$update['date_modified'] = date('Y-m-d H:i:s',now());
		$update['id_modified'] = $this->session_admin['admin_id'];

		if($update['name']){
			$do_update = $this->model_basic->update($this->tbl_knowledge_base_category,$update,'id',$update['id']);
			if($do_update)
                        {
				$this->returnJson(array('status' => 'ok','msg' => 'Update success','redirect' => $data['controller'].'/'.$data['function']));
                                $this->save_log_admin(ACT_UPDATE, 'Update Knowledge Category '.$update['name']);
                        }
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when updating data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function knowledge_category_delete(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Admin Knowledge Category','knowledge_category');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_DELETE);
		$id = $this->input->post('id');
		$do_delete = $this->model_basic->delete($this->tbl_knowledge_base_category,'id',$id);
		if($do_delete){
                        $this->save_log_admin(ACT_DELETE, 'Delete Knowledge Category');
			$this->delete_folder('knowledge_category/'.$id);
			$this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => $data['controller'].'/'.$data['function']));
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }

	public function ajax_knowledge_category_list()
	{
		$column = array('name');
		$list = $this->model_table->get_datatables($this->tbl_knowledge_base_category,$column);
		//die(print_r($list));
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $data_row) {
			//render data
			$no++;
			
			$row = array();
			$row[] = $no;
			$row[] = $data_row->name;
			//add html for action
			$row[] = '<div class="text-center">
						<form action="admin_knowledge/knowledge_category_form" method="post">
						<input type="hidden" name="id" value="'.$data_row->id.'">
						<button class="btn btn-info btn-xs btn-edit" type="submit" data-original-title="Update" data-placement="top" data-toggle="tooltip"><i class="fa fa-edit"></i></button>
						<button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Delete" data-placement="top" data-toggle="tooltip" data-target-id="'.$data_row->id.'"><i class="fa fa-trash-o"></i></button>
						</form>
						</div>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_table->count_all($this->tbl_knowledge_base_category),
						"recordsFiltered" => $this->model_table->count_filtered($this->tbl_knowledge_base_category,$column),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}


}

/* End of file admin_knowledge.php */
/* Location: ./application/controllers/admin_knowledge.php */