<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class user_security_analysis extends PX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper('text');
		$this->user_check_login();
		$this->controller_attr = array('controller' => 'user_security_analysis','controller_name' => 'User Security Analysis','controller_id' => 0);
	}

	public function index()
	{
		$this->security_analysis();
	}

	function security_analysis(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function_user('User Security Analysis','user_security_analysis');
		$data += $this->user_get_menu();
		$this->check_userakses_user($data['function_id'], ACT_READ);
                if($this->session_user['id_usergroup'] != 4)
                    $data['security_analysis'] = $security_analysis = $this->model_basic->select_where_array($this->tbl_security_analysis, 'delete_flag = 0 and status = 1');
                else
                    $data['security_analysis'] = $security_analysis = $this->model_basic->select_where_array($this->tbl_security_analysis, 'delete_flag = 0 and status = 1 and karyawan_flag = 1');
		foreach ($security_analysis->result() as $row) {
			$string = strip_tags($row->content);
			$row->content = word_limiter($string,50);
			$row->date_created = date('j F Y',strtotime($row->date_created));
			$creator = $this->model_basic->select_where($this->tbl_user, 'id', $row->id_creator)->row();
			if($creator)
				$row->realname = $creator->realname;
			else
				$row->realname = 'Administrator';
		}
		$data['content'] = $this->load->view('user_backend/admin_security_analysis/security_analysis',$data,true);
		$this->load->view('user_backend/index',$data); 
    }
    function information(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function_user('User Security Analysis Kategori Information','information');
		$data += $this->user_get_menu();
		$this->check_userakses_user($data['function_id'], ACT_READ);
		$data['security_analysis'] = $security_analysis = $this->model_basic->select_where_array($this->tbl_security_analysis, 'kategori = 2 and delete_flag = 0 and status = 1');
		foreach ($security_analysis->result() as $row) {
			$string = strip_tags($row->content);
			$row->content = word_limiter($string,50);
			$row->date_created = date('j F Y',strtotime($row->date_created));
			$creator = $this->model_basic->select_where($this->tbl_user, 'id', $row->id_creator)->row();
			if($creator)
				$row->realname = $creator->realname;
			else
				$row->realname = 'Administrator';
		}
		$data['content'] = $this->load->view('user_backend/admin_security_analysis/security_analysis',$data,true);
		$this->load->view('user_backend/index',$data); 
    }
	function analysist(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function_user('User Security Analysis Kategori Analysis','analysist');
		$data += $this->user_get_menu();
		$this->check_userakses_user($data['function_id'], ACT_READ);
		$data['security_analysis'] = $security_analysis = $this->model_basic->select_where_array($this->tbl_security_analysis, 'kategori = 1 and delete_flag = 0 and status = 1');
		foreach ($security_analysis->result() as $row) {
			$string = strip_tags($row->content);
			$row->content = word_limiter($string,50);
			$row->date_created = date('j F Y',strtotime($row->date_created));
			$creator = $this->model_basic->select_where($this->tbl_user, 'id', $row->id_creator)->row();
			if($creator)
				$row->realname = $creator->realname;
			else
				$row->realname = 'Administrator';
		}
		$data['content'] = $this->load->view('user_backend/admin_security_analysis/security_analysis',$data,true);
		$this->load->view('user_backend/index',$data); 
    }
    public function detail($id) {
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function_user('Detail','user_security_analysis');
		$data += $this->user_get_menu();
		$this->check_userakses_user($data['function_id'], ACT_READ);
		$security_analysis = $this->model_basic->select_where($this->tbl_security_analysis, 'id', $id);

		foreach ($security_analysis->result() as $row) {
			$row->date_created = date('j F Y',strtotime($row->date_created));
			$creator = $this->model_basic->select_where($this->tbl_user, 'id', $row->id_creator)->row();
			if($creator)
				$row->realname = $creator->realname;
			else
				$row->realname = 'Administrator';
			//increase views
			$initial_views = $row->views;
			$new_views = $initial_views + 1;
			$this->model_basic->update($this->tbl_security_analysis, array('views' => $new_views), 'id', $id);
		}

		$data['security_analysis'] = $security_analysis;
		$data['content'] = $this->load->view('user_backend/admin_security_analysis/detail',$data,true);
		$this->load->view('user_backend/index',$data); 
    }

}

/* End of file admin_security_analysis.php */
/* Location: ./application/controllers/admin_security_analysis.php */