<?php
/*
	Created by: Alvin (atsalvin0017)
	Date: 2019-08-12 13:53 PM
	Action: Add status for add assessment
*/
defined('BASEPATH') OR exit('No direct script access allowed');

class admin_request extends PX_Controller {

	public function __construct() {
		parent::__construct();
		$this->check_login();
		$this->controller_attr = array('controller' => 'admin_request','controller_name' => 'Request','controller_id' => 0);
	}

	function index(){
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Asesmen 2015','admin_request');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);
		$data['data'] = $this->model_basic->select_all($this->tbl_assessment2015);
		// Added by Alvin, 2019-08-26 13:50 PM
		// $data['is_admin'] = (in_array($this->session->userdata['admin']['id_usergroup'], [1, 19])) ? 1 : 0;
		// $data['is_superadmin'] = ($this->session->userdata['admin']['id_usergroup']==1) ? 1 : 0;
		// 
		$data['content'] = $this->load->view('backend/admin_request/assessment2015',$data,true);
		$this->load->view('backend/index',$data); 
	}

	function admin_request_view($id){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Asesmen 2015 Details','admin_request');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);

		if($id){
			$id = $id;
			$data['rules'] = $this->model_basic->select_where($this->tbl_asesmen_2015_value,'id_asesmen',$id)->result();
			$total_value = 0;
			if(count($data['rules']) > 0) {
				foreach ($data['rules'] as $r) {
					$rules_data = $this->model_basic->select_where($this->tbl_rules_asms_2015,'id',$r->id_rules)->row();
					$r->name = $rules_data->name;
					$total_value = $total_value + $r->value;
				}
			}
			else
				$data['rules'] = $this->model_basic->select_where($this->tbl_rules_asms_2015,'status',1)->result();

			$data['data'] = $this->model_basic->select_where($this->tbl_assessment2015,'id',$id)->row();
			
			// Add by: Alvin (atsalvin0017), Date: 2019-08-15 14:03 PM
			$data['history_revise'] = [];
			$history = $this->model_basic->select_where($this->tbl_assessment2015_history,'asesmen_2015_id',$data['data']->id)->result();
			foreach ($history as $k=>$row) {
				if ($row->revise)
					$data['history_revise'][] = $row;
			}
			//

			$data['data']->pemenuhan_system = $this->check_colour(number_format($total_value/4,2),'Pemenuhan System (Asesmen 2015)');
			$data['data']->security_performance = $this->check_colour(number_format($data['data']->security_performance,2),'Security Performance (Asesmen 2015)');
			$data['data']->security_reliability = $this->check_colour(number_format(($data['data']->people + $data['data']->device_and_infrastructure) / 2,2),'Security Reliability (Asesmen 2015)');
			$data['data']->csi = $this->check_colour(number_format($data['data']->csi,2),'CSI (Asesmen 2015)');
			$data['data']->nilai_akhir = $this->check_colour(number_format($data['data']->nilai_akhir,2),'Nilai Akhir (Asesmen 2015)');

			$instalasi = $this->model_basic->select_where($this->tbl_instalasi,'id',$data['data']->instalasi_id)->row();
			if($instalasi)
				$data['data']->nama_instalasi = $instalasi->name;
			else
				$data['data']->nama_instalasi = ' ';

			$data['content'] = $this->load->view('backend/admin_request/assessment2015_view',$data,true);
			$this->load->view('backend/index',$data);
		}
		else
			redirect($data['function']);
    }

	function admin_request_form(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Asesmen 2015','admin_request');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);
		$id = $this->input->post('id');

		$data['company'] = $this->model_basic->select_where($this->tbl_instalasi,'delete_flag',0);
		if($id){
			$data['rules'] = $this->model_basic->select_where($this->tbl_asesmen_2015_value,'id_asesmen',$id)->result();
			if(count($data['rules']) > 0) {
				foreach ($data['rules'] as $r) {
					$rules_data = $this->model_basic->select_where($this->tbl_rules_asms_2015,'id',$r->id_rules)->row();
					$r->name = $rules_data->name;
					$r->id = $rules_data->id;
				}
			}
			else
				$data['rules'] = $this->model_basic->select_where($this->tbl_rules_asms_2015,'status',1)->result();
			$data['data'] = $this->model_basic->select_where($this->tbl_assessment2015,'id',$id)->row();
			// Add by: Alvin (atsalvin0017), Date: 2019-08-15 14:03 PM
			$data['history_revise'] = [];
			$history = $this->model_basic->select_where($this->tbl_assessment2015_history,'asesmen_2015_id',$id)->result();
			foreach ($history as $k=>$row) {
				if ($row->revise)
					$data['history_revise'][] = $row;
			}
			//
		}
		else{
			$data['rules'] = $this->model_basic->select_where($this->tbl_rules_asms_2015,'status',1)->result();
			$data['data'] = null;
		}
		$data['content'] = $this->load->view('backend/admin_request/assessment2015_form',$data,true);
		$this->load->view('backend/index',$data); 
    }

    /*
    function admin_request_add(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Asesmen 2015','admin_request');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);

		$table_field = $this->db->list_fields($this->tbl_assessment2015);
		$insert = array();
		foreach ($table_field as $field) {
			// Add by: Alvin (atsalvin0017), Date: 2019-08-12 13:53 PM
			if ($field!='id') { 
				$insert[$field] = $this->input->post($field);
			}
			// $insert[$field] = $this->input->post($field);
		}
		$insert['date_created'] = date('Y-m-d H:i:s',now());
		$insert['id_created'] = $this->session_admin['admin_id'];
		$insert['status'] = 3; // Add by: Alvin (atsalvin0017), Date: 2019-08-12 13:53 PM
		if($insert['instalasi_id']){
			$do_insert = $this->model_basic->insert_all($this->tbl_assessment2015,$insert);
			if($do_insert){
				// Insert to Asesmen 2015 history
				$insert_asesment2015_history = [];
				$insert_asesment2015_history['asesmen_2015_id'] = $do_insert->id;
				$insert_asesment2015_history['user_id'] = $this->session_admin['admin_id'];
				$insert_asesment2015_history['user_status'] = 'Assessor';
				$insert_asesment2015_history['action'] = 'Submit';
				$insert_asesment2015_history['date_created'] = date('Y-m-d H:i:s',now());
				$insert_asesment2015_history['status'] = 1;
				$do_insert_asesment2015_history = $this->model_basic->insert_all($this->tbl_assessment2015_history,$insert_asesment2015_history);
				if ($do_insert_asesment2015_history) {
					$rules_id = $this->input->post('rules_id');
					$rules_value = $this->input->post('rules_value');
					$insert_rules_value = array();
					for ($i=0; $i < count($rules_id); $i++) { 
						$rules_value_data = array(
							'id_asesmen' => $do_insert->id,
							'id_rules' => $rules_id[$i],
							'value' => $rules_value[$rules_id[$i]],
							'delete_flag' => 0 // Add by: Alvin (atsalvin0017), Date: 2019-12 13:52 PM
							);
						array_push($insert_rules_value, $rules_value_data);
					}
					if(count($insert_rules_value) > 0)
						$do_insert_rules_value = $this->model_basic->insert_all_batch($this->tbl_asesmen_2015_value,$insert_rules_value);
					$this->save_log_admin(ACT_CREATE, 'Insert Assessment 2015');
	                                $this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => $data['controller'].'/'.$data['function']));
	            } 
				else
					$this->returnJson(array('status' => 'error','msg' => 'Failed when saving data history'));
			}
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }
	*/

    function admin_request_edit(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Asesmen 2015','admin_request');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_UPDATE);
		
		$table_field = $this->db->list_fields($this->tbl_assessment2015);
		$update = array();
		foreach ($table_field as $field) {
			$update[$field] = $this->input->post($field);
		}
		unset($update['date_created']);
		$update['date_modified'] = date('Y-m-d H:i:s',now());
		$update['id_modified'] = $this->session_admin['admin_id'];
		$update['status'] = 3; // Add by: Alvin (atsalvin0017), Date: 2019-08-13 09:37 PM
		
		$rules_id = $this->input->post('rules_id');
		$rules_value = $this->input->post('rules_value');
		$insert_rules_value = array();
		for ($i=0; $i < count($rules_id); $i++) { 
			$rules_value_data = array(
				'id_asesmen' => $update['id'],
				'id_rules' => $rules_id[$i],
				'value' => $rules_value[$rules_id[$i]],
				'delete_flag' => 0 // Add by: Alvin (atsalvin0017), Date: 2019-12 13:52 PM
				);
			array_push($insert_rules_value, $rules_value_data);
		}
		if(count($insert_rules_value) > 0){
			$this->model_basic->delete_full($this->tbl_asesmen_2015_value,'id_asesmen',$update['id']);
			$do_insert_rules_value = $this->model_basic->insert_all_batch($this->tbl_asesmen_2015_value,$insert_rules_value);
		}

		if($update['instalasi_id']){
			$do_update = $this->model_basic->update($this->tbl_assessment2015,$update,'id',$update['id']);
            if($do_update) {
            	// Update to Asesmen 2015 history
            	$update_asesment2015_history = [];
				$update_asesment2015_history['status'] = 0;
				$do_update_asesment2015_history = $this->model_basic->update($this->tbl_assessment2015_history,$update_asesment2015_history,'asesmen_2015_id',$update['id']);
            	// Insert to Asesmen 2015 history
				$insert_asesment2015_history = [];
				$insert_asesment2015_history['asesmen_2015_id'] = $update['id'];
				$insert_asesment2015_history['user_id'] = $this->session_admin['admin_id'];
				$insert_asesment2015_history['user_status'] = 'Admin';
				$insert_asesment2015_history['action'] = 'Submit';
				$insert_asesment2015_history['date_created'] = date('Y-m-d H:i:s',now());
				$insert_asesment2015_history['status'] = 1;
				$do_insert_asesment2015_history = $this->model_basic->insert_all($this->tbl_assessment2015_history,$insert_asesment2015_history);
				if ($do_insert_asesment2015_history) {
					$this->save_log_admin(ACT_UPDATE, 'Update Assessment 2015');
					$this->returnJson(array('status' => 'ok','msg' => 'Update success','redirect' => $data['controller'].'/'.$data['function']));
				} 
				else
					$this->returnJson(array('status' => 'error','msg' => 'Failed when saving data history'));
            }
            else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when updating data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    /*
    function admin_request_delete(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Asesmen 2015','admin_request');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_DELETE);
		$id = $this->input->post('id');
		$do_delete = $this->model_basic->delete($this->tbl_assessment2015,'id',$id);
		if($do_delete){
                        $this->save_log_admin(ACT_DELETE, 'Delete Assessment 2015');
			$this->delete_folder('assessment2015/'.$id);
			$this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => $data['controller'].'/'.$data['function']));
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }
    */

	public function ajax_assessment2015_list()
	{
		$status = 3;
		$id_created = false;
		if ($this->session_admin['id_usergroup']==1)
			$status = [3];
		if ($this->session_admin['id_usergroup']==19)
			$id_created = true;
		$list = $this->model_assessment2015->get_datatables($status, false, $id_created);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $data_row) {
			//render data
			$no++;
			$w_ps = $this->check_colour_text_dashboard(number_format($data_row->pemenuhan_system,2),'Pemenuhan System (Asesmen 2015)');
			$w_sp = $this->check_colour_text_dashboard(number_format($data_row->security_performance,2),'Security Performance (Asesmen 2015)');
			$w_sr = $this->check_colour_text_dashboard(number_format($data_row->security_reliability,2),'Security Reliability (Asesmen 2015)');
			$w_csi = $this->check_colour_text_dashboard(number_format($data_row->csi,2),'CSI (Asesmen 2015)');
			$semua_warna = array($w_ps,$w_sp,$w_sr,$w_csi);
			$w_na = '<div class="bg-Emas text-center">'.number_format($data_row->nilai_akhir,2).'</div>';
			if(in_array('hijau', $semua_warna))
				$w_na = '<div class="bg-Hijau text-center">'.number_format($data_row->nilai_akhir,2).'</div>';
			if(in_array('biru', $semua_warna))
				$w_na = '<div class="bg-Biru text-center">'.number_format($data_row->nilai_akhir,2).'</div>';
			if(in_array('merah', $semua_warna))
				$w_na = '<div class="bg-Merah text-center">'.number_format($data_row->nilai_akhir,2).'</div>';
			if(in_array('hitam', $semua_warna))
				$w_na = '<div class="bg-Hitam text-center">'.number_format($data_row->nilai_akhir,2).'</div>';
			$row = array();
			$row[] = $no;
			$row[] = $data_row->name;
			$row[] = $data_row->periode;
			$row[] = $this->check_colour(number_format($data_row->pemenuhan_system,2),'Pemenuhan System (Asesmen 2015)');
			$row[] = $this->check_colour(number_format($data_row->security_performance,2),'Security Performance (Asesmen 2015)');
			$row[] = $this->check_colour(number_format($data_row->security_reliability,2),'Security Reliability (Asesmen 2015)');
			$row[] = $this->check_colour(number_format($data_row->csi,2),'CSI (Asesmen 2015)');
			$row[] = $w_na;
			$row[] = '<a href="admin_request/admin_request_view/'.$data_row->id.'">Critical Point</a>';
			$row[] = date('d-m-Y',strtotime($data_row->date_created));
			// Add by: Alvin (atsalvin0017), Date: 2019-08-15 13:30 AM
			$status = $data_row->status;
			if ($status==1) {
				$status='Complete';
			} else
			if ($status==2) {
				$status=$data_row->revise?'Revise':'Request';
			} else 
			if ($status==3) {
				$status='Check';
				// if ($this->session_admin['id_usergroup']==1)
				// 	$status = 'Process';
			} else
				$status='';
			$row[] = $status;
			// 
			//add html for action
			// if ($this->session_admin['id_usergroup']==1) {
				$row[] = '<div class="text-center">
							<form action="admin_request/admin_request_form" method="post">
							<input type="hidden" name="id" value="'.$data_row->id.'">
							<button class="btn-link" style="font-weight: 400; color: #428bca; padding: 0;" type="submit" data-original-title="Detail" data-placement="top" data-toggle="tooltip">Detail Assessment</button>
							</form>
							</div>';
				// $row[] = ($status=='Check')?'Admin Asesor':'Asesor';
			// }
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_assessment2015->count_all(2),
						"recordsFiltered" => $this->model_assessment2015->count_filtered(2),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	function assessment_2015_check_colour(){
		$nilai = $this->input->post('nilai');
		if($nilai)
			$warna = $this->check_colour_text($nilai,'Nilai Akhir (Asesmen 2015)');
		else
			$warna = $this->check_colour_text(0,'Nilai Akhir (Asesmen 2015)');
		$result = array(
			'status' => 'ok',
			'warna' => $warna
			);
		$this->returnJson($result);
	}

}

/* End of file admin_request.php */
/* Location: ./application/controllers/admin_request.php */