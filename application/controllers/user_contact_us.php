<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class user_contact_us extends PX_Controller {

	public function __construct() {
		parent::__construct();
		$this->user_check_login();
		$this->controller_attr = array('controller' => 'user_contact_us','controller_name' => 'User Contact Us','controller_id' => 0);
	}

	public function index()
	{
		$this->message();
	}

	public function message() {
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function_user('User Contact Us','user_contact_us');
		$data += $this->user_get_menu();
		$this->check_userakses_user($data['function_id'], ACT_READ);
		$admin_id = 7;
		$data['admin'] = $this->model_basic->select_where($this->tbl_admin, 'id', $admin_id)->row();
		$data['user_id'] = $user_id =$this->session_user['admin_id'];
		
		$data['message'] = $message = $this->model_message->get_message($user_id);
                $this->model_message->update_readable_messages($user_id, 0);

		$data['content'] = $this->load->view('user_backend/admin_contact_us/message',$data,true);
		$this->load->view('user_backend/index',$data); 
	}

	function ajax_message_save() {
		$user_id = $this->session_user['admin_id'];
		$admin_id = $this->input->post('admin_id');
		$message = $this->input->post('message_txt');

		$insert = array(
			'user_id' => $user_id,
			'admin_id' => $admin_id,
			'message' => $message,
			'type' => 1,
			'status' => 0,
			'id_created' => $user_id,
			'date_created' => date('Y-m-d H:i:s', now())
		);

		$save_message = $this->model_basic->insert_all($this->tbl_message, $insert);
		if ($save_message) {
			$admin = $this->model_basic->select_where($this->tbl_admin, 'id', $admin_id)->row();
			$row = $this->model_message->get_message_by_id($save_message->id)->row();
			if ($row->type == 1) {
				$ret = '<div class="item in item-visible">
                        <div class="image">
                            <img src="assets/uploads/user/'.$row->user_id.'/'.$row->photo.'" alt="'.$row->realname.'">
                        </div>
                        <div class="text">
                            <div class="heading">
                                <a href="#">'.$row->realname.'</a>
                                <span class="date">'.date('H:i', strtotime($row->date_created)).'</span>
                            </div>
                            '.$row->message.'
                        </div>
                    </div>';
			}else{
				$ret = '<div class="item item-visible">
                        <div class="image">
                            <img src="assets/uploads/admin/'.$admin_id.'/'.$admin->photo.'" alt="'.$admin->realname.'">
                        </div>
                        <div class="text">
                            <div class="heading">
                                <a href="#">'.$admin->realname.'</a>
                                <span class="date">'.date('H:i', strtotime($row->date_created)).'</span>
                            </div>
                            '.$row->message.'
                        </div>
                    </div>';
			}
			
			echo $ret;
		}else{
			$ret = "";
			echo $ret;
		}		
	}

}

/* End of file user_contact_us.php */
/* Location: ./application/controllers/user_contact_us.php */