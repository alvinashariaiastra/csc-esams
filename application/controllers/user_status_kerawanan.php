<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class user_status_kerawanan extends PX_Controller {

	public function __construct() {
		parent::__construct();
		$this->user_check_login();
		$this->controller_attr = array('controller' => 'user_status_kerawanan','controller_name' => 'Admin Status Kerawanan','controller_id' => 0);
	}

	public function index()
	{
		
	}

	// peta status kerawanan //
	function peta(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function_user('Peta Status Kerawanan','peta');
		$data += $this->user_get_menu();
		$this->check_userakses_user($data['function_id'], ACT_READ);
		$data['status_merah'] = $this->model_basic->select_where_array($this->tbl_status_kerawanan,'instalasi_id = '.$this->session_user['id_instalasi'].' and status = 3')->num_rows();
		$data['status_kuning'] = $this->model_basic->select_where_array($this->tbl_status_kerawanan,'instalasi_id = '.$this->session_user['id_instalasi'].' and status = 2')->num_rows();
		$data['status_hijau'] = $this->model_basic->select_where_array($this->tbl_status_kerawanan,'instalasi_id = '.$this->session_user['id_instalasi'].' and status = 1')->num_rows();
		$data['province'] = $this->model_basic->select_all_noflag($this->tbl_province);
		$data['content'] = $this->load->view('user_backend/admin_status_kerawanan/peta',$data,true);
		$this->load->view('user_backend/index',$data); 
    }

    function peta_form(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function_user('Peta Status Kerawanan','peta');
		$data += $this->user_get_menu();
		$this->check_userakses_user($data['function_id'], ACT_CREATE);
		$id = $this->input->post('id');

		$data['instalasi'] = $this->model_basic->select_where($this->tbl_instalasi,'id',$this->session_user['id_instalasi'])->row();
		$data['perusahaan'] = $this->model_basic->select_where($this->tbl_perusahaan,'id',$data['instalasi']->perusahaan_id)->row();
		$data['jenis_bisnis'] = $this->model_basic->select_where($this->tbl_jenis_bisnis,'id',$data['instalasi']->jenis_bisnis_id)->row();
		if($id){
			$data['data'] = $this->model_basic->select_where($this->tbl_status_kerawanan,'id',$id)->row();
			$content = new domDocument;
			libxml_use_internal_errors(true);
			$content->loadHTML($data['data']->content);
			libxml_use_internal_errors(false);
			$content->preserveWhiteSpace = false;
			$images = $content->getElementsByTagName('img');
			if($images){
				foreach ($images as $image) {
				  $data['data']->image[] = $image->getAttribute('src');
				}
			}
		}
		else
			$data['data'] = null;
		$data['content'] = $this->load->view('user_backend/admin_status_kerawanan/peta_form',$data,true);
		$this->load->view('user_backend/index',$data); 
    }

    //ajax
    function get_instalasi_list() {
    	$perusahaan_id = $this->input->post('id');
    	$opt = "<option value=\"0\">Pilih Instalasi</option>";
    	if ($perusahaan_id == 0) {
    		$opt = $opt;
    	}else{
    		$instalasi = $this->model_basic->select_all($this->tbl_instalasi,'perusahaan_id',$perusahaan_id);
	    	foreach ($instalasi as $data_row) {
	    		$opt .= "<option value=\"".$data_row->id."\">".$data_row->name."</option>";  
	    	}
    	}

    	echo $opt;
    }

    function get_instalasi_detail() {
    	$instalasi_id = $this->input->post('id');

    	$instalasi = $this->model_basic->select_where($this->tbl_instalasi,'id',$instalasi_id)->row();
    	$jenis_bisnis_nm = $this->model_basic->select_where($this->tbl_jenis_bisnis,'id',$instalasi->jenis_bisnis_id)->row()->name;

    	echo json_encode(array('jenis_bisnis_id' => $instalasi->jenis_bisnis_id, 'jenis_bisnis_nm' => $jenis_bisnis_nm, 'instalasi_alamat' => $instalasi->alamat));
    }

    function get_all_location() {
    	$status_kerawanan = $this->model_basic->select_where($this->tbl_instalasi,'id',$this->session_user['id_instalasi'])->result();
    	$data = array();
    	foreach ($status_kerawanan as $data_row) {
    		$instalasi_nm = $data_row->name;
    		$instalasi_alamat = $data_row->alamat;
    		$instalasi_id = $data_row->id;
    		if ($data_row->status_kerawanan == 3) {
    			$icon_status = "marker-red.png";
    		}elseif ($data_row->status_kerawanan == 2) {
    			$icon_status = "marker-yellow.png";
    		}else{
    			$icon_status = "marker-green.png";
    		}
    		$data_new = array('instalasi_id' => $instalasi_id,'instalasi_nm' => $instalasi_nm, 'instalasi_alamat' => $instalasi_alamat, 'latitude' => $data_row->coordinate_x, 'longitude' => $data_row->coordinate_y, 'icon_status' => $icon_status);
    		array_push($data, $data_new);
    	}

    	echo json_encode($data);
    }

    function get_all_news() {
    	$id = $this->input->post('id');
    	$data = $this->model_basic->select_where($this->tbl_status_kerawanan,'instalasi_id',$id)->result();
    	if($data){
    		$this->returnJson(array('status' => 'ok','data' => $data));
    	}
    	else
    		$this->returnJson(array('status' => 'error'));
    }

    function get_detail_news() {
    	$id = $this->input->post('id');
    	$data = $this->model_basic->select_where($this->tbl_status_kerawanan,'id',$id)->row();
    	if($data){
    		$this->returnJson(array('status' => 'ok','data' => $data));
    	}
    	else
    		$this->returnJson(array('status' => 'error'));
    }

    //e.o. ajax
    function peta_add(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function_user('Admin Status Kerawanan','peta');
		$data += $this->user_get_menu();
		$this->check_userakses_user($data['function_id'], ACT_CREATE);

		$images = $this->input->post('images');
		$news_title = $this->input->post('title');
		$news_content = $this->input->post('content');
		$date_created = date('Y-m-d H:i:s',now());
		$table_field = $this->db->list_fields($this->tbl_status_kerawanan);
		$insert = array();
		foreach ($table_field as $field) {
			$insert[$field] = $this->input->post($field);
		}
		$insert['date_created'] = $date_created;
		$insert['date_modified'] = date('Y-m-d H:i:s',now());
		$insert['id_created'] = $this->session_admin['admin_id'];
		$insert['id_modified'] = $this->session_admin['admin_id'];
		
		if($insert['perusahaan_id'] && $insert['instalasi_id']){
			$do_insert = $this->model_basic->insert_all($this->tbl_status_kerawanan,$insert);
			if($do_insert){
				$update = array(
					'status_kerawanan' => $insert['status']
					);
				$do_update = $this->model_basic->update($this->tbl_instalasi,$update,'id',$insert['instalasi_id']);
				if($images){
					if(!is_dir(FCPATH . "assets/uploads/status_kerawanan/".$do_insert->id))
						mkdir(FCPATH . "assets/uploads/status_kerawanan/".$do_insert->id);
					$content = $insert['content']; 
					foreach($images as $im) {
						if(strpos($content,$im) !== false)
						{
							$new_im = 'assets/uploads/status_kerawanan/'.$do_insert->id.'/'.basename($im);
							@copy($im,$new_im);
						    $content = str_replace($im, $new_im, $content);
						}
					}
					//$update['content'] = $content;
					//$do_update = $this->model_basic->update($this->tbl_status_kerawanan_news,$update,'id',$do_insert->id);
					$this->delete_temp('temp_folder');
				}
                                $this->save_log_user(ACT_CREATE, 'Insert New Status Kerawanan');
				$this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => $data['controller'].'/'.$data['function']));
			}
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

}

/* End of file admin_status_kerawanan.php */
/* Location: ./application/controllers/admin_status_kerawanan.php */