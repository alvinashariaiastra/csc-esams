<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_visitor_history extends PX_Controller {

	public function __construct() {
		parent::__construct();
		$this->check_login();
		$this->controller_attr = array('controller' => 'admin_visitor_history','controller_name' => 'Admin Visitor History','controller_id' => 0);
	}

	public function index()
	{            
            $data = $this->get_app_settings();
            $data += $this->controller_attr;
            $data += $this->get_function('Visitor History','index');
            $data += $this->get_menu();
            $this->check_userakses($data['function_id'], ACT_READ);

            $data['content'] = $this->load->view('backend/admin_visitor/index',$data,true);
            $this->load->view('backend/index',$data);
	}
        
        public function visitor_history_ajax()
        {
            $date_month = date('Y-m-d H:i:s', strtotime("-6 month", now()));
            $column = array('id','type','visitor_id','browser','ip_address','date_login');
            $list = $this->model_history->get_datatables_visitor($this->tbl_visitor,$column,$date_month);
            $data = array();
            $no = $_POST['start'];
            foreach ($list as $data_row) {
                    //render data
                    if($data_row->type == 1)
                    {
                        $login_type = 'Admin';
                        $v = $this->model_basic->select_where($this->tbl_admin, 'id', $data_row->visitor_id);
                        if($v->num_rows() == 1)
                            $visitor = $v->row()->realname;
                        else
                            $visitor = 'Unknown';
                    }
                    else
                    {
                        $login_type = 'User';
                        $v = $this->model_basic->select_where($this->tbl_user, 'id', $data_row->visitor_id);
                        if($v->num_rows() == 1)
                            $visitor = $v->row()->realname;
                        else
                            $visitor = 'Unknown';
                    }
                    $no++;

                    $row = array();
                    $row[] = $no;
                    $row[] = $login_type;
                    $row[] = $visitor;
                    $row[] = $data_row->browser;
                    $row[] = $data_row->ip_address;
                    $row[] = date('d M Y H:i:s', strtotime($data_row->date_login));
                    $data[] = $row;
            }

            $output = array(
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->model_history->count_all_visitor($this->tbl_visitor,$date_month),
                "recordsFiltered" => $this->model_history->count_filtered_visitor($this->tbl_visitor,$column,$date_month),
                "data" => $data,
            );
            //output to json format
            echo json_encode($output);
        }

}

/* End of file admin_visitor_history.php */
/* Location: ./application/controllers/admin_visitor_history.php */