<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_citizen_security extends PX_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->check_login();
		$this->controller_attr = array('controller' => 'admin_citizen_security', 'controller_name' => 'Admin Citizen Security', 'controller_id' => 0);
	}
	
	public function index()
	{
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Citizen Security', 'admin_citizen_security');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);
		$data['data'] = $this->model_basic->select_all($this->tbl_citizen_security);
		$data['content'] = $this->load->view('backend/admin_citizen_security/index', $data, true);
		$this->load->view('backend/index', $data);
	}
	
	public function ajax_index()
	{
		$column = array($this->tbl_user.'.username', $this->tbl_user.'.email', $this->tbl_citizen_security.'.infraction_place', $this->tbl_citizen_security.'.infraction_notes');
		$list = $this->model_citizen_security->get_datatables($this->tbl_citizen_security, $column);
		// die(print_r($this->db->last_query()));
		$data = array();
		$no = $_POST['start'];
		
		foreach ($list as $data_row) {
			$no++;
			
			$row = array();
			$row[] = $no;
			$row[] = $data_row->username;
			$row[] = $data_row->email;
			$row[] = $data_row->infraction_place;
			$row[] = $data_row->infraction_notes;
			$picno = 1;
			$picture1 = '';
			if ($data_row->infraction_photo1 != '') {
				$picture1 = '<a target="_blank" rel="noopener noreferrer" href="'.$data_row->infraction_photo1.'">{'.$picno.'}</a>';
				$picno++;
			}
			$picture2 = '';
			if ($data_row->infraction_photo2 != '') {
				$picture2 = '<a target="_blank" rel="noopener noreferrer" href="'.$data_row->infraction_photo2.'">{'.$picno.'}</a>';
				$picno++;
			}
			$picture3 = '';
			if ($data_row->infraction_photo3 != '') {
				$picture3 = '<a target="_blank" rel="noopener noreferrer" href="'.$data_row->infraction_photo3.'">{'.$picno.'}</a>';
				$picno++;
			}
			$row[] = $picture1.' '.$picture2.' '.$picture3;
			$row[] = $data_row->infraction_date;
			
			$data[] = $row;
		}
		
		if ($this->input->post('startdatetime') != "" && $this->input->post('enddatetime') != "") {
			$GLOBALS['startdatetime'] = $this->input->post('startdatetime');
			$GLOBALS['enddatetime'] = $this->input->post('enddatetime');
		}
		
		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_citizen_security->count_all($this->tbl_citizen_security),
						"recordsFiltered" => $this->model_citizen_security->count_filtered($this->tbl_citizen_security, $column),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	
}