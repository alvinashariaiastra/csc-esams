<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_company extends PX_Controller {

	public function __construct() {
		parent::__construct();
		$this->check_login();
		$this->controller_attr = array('controller' => 'admin_company','controller_name' => 'Admin Perusahaan','controller_id' => 0);
	}

	public function index()
	{
		$this->company_list();
	}

	//company list//
	function company_list(){
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('List Perusahaan','company_list');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);

		$data['data'] = $this->model_basic->select_all($this->tbl_perusahaan);
		$data['content'] = $this->load->view('backend/admin_company/company_list',$data,true);
		$this->load->view('backend/index',$data); 
	}

	function company_list_form(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('List Perusahaan','company_list');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);
		$id = $this->input->post('id');
		$data['business_type'] = $this->model_basic->select_where_order($this->tbl_jenis_bisnis,'delete_flag',0, 'name', 'asc');
		$data['jabatan1'] = $this->model_basic->select_where($this->tbl_user,'delete_flag',0);
		$data['jabatan2'] = $this->model_basic->select_where($this->tbl_user,'delete_flag',0);
		$data['jabatan3'] = $this->model_basic->select_where($this->tbl_user,'delete_flag',0);
		$data['jabatan4'] = $this->model_basic->select_where($this->tbl_user,'delete_flag',0);
		if($id){
			$data['data'] = $this->model_basic->select_where($this->tbl_perusahaan,'id',$id)->row();
		}
		else{
			$data['data'] = null;
		}
		$data['content'] = $this->load->view('backend/admin_company/company_list_form',$data,true);
		$this->load->view('backend/index',$data); 
    }

    function company_list_add(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('List Perusahaan','company_list');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);

		$table_field = $this->db->list_fields($this->tbl_perusahaan);
		$insert = array();
		foreach ($table_field as $field) {
			$insert[$field] = $this->input->post($field);
		}
		$insert['date_created'] = date('Y-m-d H:i:s',now());
		$insert['id_created'] = $this->session_admin['admin_id'];
		if($insert['name']){
			$do_insert = $this->model_basic->insert_all($this->tbl_perusahaan,$insert);
			if($do_insert){
				$this->save_log_admin(ACT_CREATE, 'Insert New Perusahaan '.$insert['name']);
				$this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => $data['controller'].'/'.$data['function']));
			}
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function company_list_edit(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('List Perusahaan','company_list');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_UPDATE);
		
		$table_field = $this->db->list_fields($this->tbl_perusahaan);
		$update = array();
		foreach ($table_field as $field) {
			$update[$field] = $this->input->post($field);
		}
		unset($update['date_created']);
		$update['date_modified'] = date('Y-m-d H:i:s',now());
		$update['id_modified'] = $this->session_admin['admin_id'];

		if($update['name']){
			$do_update = $this->model_basic->update($this->tbl_perusahaan,$update,'id',$update['id']);
			if($do_update)
                        {
                                $this->save_log_admin(ACT_UPDATE, 'Update Perusahaan '.$update['name']);
				$this->returnJson(array('status' => 'ok','msg' => 'Update success','redirect' => $data['controller'].'/'.$data['function']));
                        }
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when updating data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function company_list_delete(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('List Perusahaan','company_list');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_DELETE);
		$id = $this->input->post('id');
		$do_delete = $this->model_basic->delete($this->tbl_perusahaan,'id',$id);
		if($do_delete){
                        $this->save_log_admin(ACT_DELETE, 'Delete Perusahaan');
			$this->delete_folder('company_list/'.$id);
			$this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => $data['controller'].'/'.$data['function']));
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }

    public function ajax_company_list_list()
	{
		$column = array('name','jenis_bisnis_id');
		$list = $this->model_table->get_datatables($this->tbl_perusahaan,$column);
		// die(print_r($this->db->last_query()));
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $data_row) {
			//render data
			$jenis_bisnis_nm = $this->model_basic->select_where($this->tbl_jenis_bisnis,'id',$data_row->jenis_bisnis_id)->row()->name;
			$no++;
			
			$row = array();
			$row[] = $no;
			$row[] = $data_row->name;
			$row[] = $jenis_bisnis_nm;
			$row[] = $data_row->alamat;
			//add html for action
			$row[] = '<div class="text-center">
						<form action="admin_company/company_list_form" method="post">
						<input type="hidden" name="id" value="'.$data_row->id.'">
						<button class="btn btn-info btn-xs btn-edit" type="submit" data-original-title="Update" data-placement="top" data-toggle="tooltip"><i class="fa fa-edit"></i></button>
						<button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Delete" data-placement="top" data-toggle="tooltip" data-target-id="'.$data_row->id.'"><i class="fa fa-trash-o"></i></button>
						</form>
						</div>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_table->count_all($this->tbl_perusahaan),
						"recordsFiltered" => $this->model_table->count_filtered($this->tbl_perusahaan,$column),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	//instalasi//
	function instalasi(){
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Instalasi','instalasi');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);

		$data['data'] = $this->model_basic->select_all($this->tbl_instalasi);
		$data['content'] = $this->load->view('backend/admin_company/cabang',$data,true);
		$this->load->view('backend/index',$data); 
	}

	function instalasi_form(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Instalasi','instalasi');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);
		$id = $this->input->post('id');
		$data['province'] = $this->model_basic->select_all_noflag($this->tbl_province);
		$data['perusahaan'] = $this->model_basic->select_where_order($this->tbl_perusahaan,'delete_flag',0,'name','asc');
		$data['business_type'] = $this->model_basic->select_where_order($this->tbl_jenis_bisnis,'delete_flag',0,'name','asc');
		$data['jabatan1'] = $this->model_basic->select_where($this->tbl_user,'delete_flag',0);
		$data['jabatan2'] = $this->model_basic->select_where($this->tbl_user,'delete_flag',0);
		$data['jabatan3'] = $this->model_basic->select_where($this->tbl_user,'delete_flag',0);
		$data['jabatan4'] = $this->model_basic->select_where($this->tbl_user,'delete_flag',0);
		if($id){
			$data['data'] = $this->model_basic->select_where($this->tbl_instalasi,'id',$id)->row();
		}
		else{
			$data['data'] = null;
		}
		$data['content'] = $this->load->view('backend/admin_company/cabang_form',$data,true);
		$this->load->view('backend/index',$data); 
    }

    function instalasi_add(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Instalasi','instalasi');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);

		$table_field = $this->db->list_fields($this->tbl_instalasi);
		$insert = array();
		foreach ($table_field as $field) {
			$insert[$field] = $this->input->post($field);
		}
		$insert['date_created'] = date('Y-m-d H:i:s',now());
		$insert['id_created'] = $this->session_admin['admin_id'];
		unset($insert['status_kerawanan']);
		if($insert['name']){
			$do_insert = $this->model_basic->insert_all($this->tbl_instalasi,$insert);
			if($do_insert){
                                $this->save_log_admin(ACT_CREATE, 'Insert New Instalasi '.$insert['name']);
				$this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => $data['controller'].'/'.$data['function']));
			}
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function instalasi_edit(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Instalasi','instalasi');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_UPDATE);
		
		$table_field = $this->db->list_fields($this->tbl_instalasi);
		$update = array();
		foreach ($table_field as $field) {
			$update[$field] = $this->input->post($field);
		}
		unset($update['date_created']);
		unset($update['status_kerawanan']);
		$update['date_modified'] = date('Y-m-d H:i:s',now());
		$update['id_modified'] = $this->session_admin['admin_id'];

		if($update['name']){
			$do_update = $this->model_basic->update($this->tbl_instalasi,$update,'id',$update['id']);
			if($do_update)
                        {
                            $this->save_log_admin(ACT_UPDATE, 'Update Instalasi '.$update['name']);
				$this->returnJson(array('status' => 'ok','msg' => 'Update success','redirect' => $data['controller'].'/'.$data['function']));
                        }
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when updating data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function instalasi_detail($id = 0) {
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Instalasi','instalasi');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);
		
		$data['data'] = $instalasi = $this->model_basic->select_where($this->tbl_instalasi,'id',$id)->row();

		$data['province'] = $this->model_basic->select_where($this->tbl_province, 'id', $instalasi->province_id)->row()->name;
		$data['perusahaan'] = $this->model_basic->select_where($this->tbl_perusahaan,'id',$instalasi->perusahaan_id)->row()->name;
		$data['business_type'] = $this->model_basic->select_where($this->tbl_jenis_bisnis,'id',$instalasi->jenis_bisnis_id)->row()->name;

		$jabatan1 = $this->model_basic->select_where($this->tbl_user,'id',$instalasi->jabatan1_id);
		if ($jabatan1->num_rows() > 0) {
			$data['jabatan1'] = $jabatan1->row()->realname;
		}else{
			$data['jabatan1'] = "";
		}
		$jabatan2 = $this->model_basic->select_where($this->tbl_user,'id',$instalasi->jabatan2_id);
		if ($jabatan2->num_rows() > 0) {
			$data['jabatan2'] = $jabatan2->row()->realname;
		}else{
			$data['jabatan2'] = "";
		}
		$jabatan3 = $this->model_basic->select_where($this->tbl_user,'id',$instalasi->jabatan3_id);
		if ($jabatan3->num_rows() > 0) {
			$data['jabatan3'] = $jabatan3->row()->realname;
		}else{
			$data['jabatan3'] = "";
		}
		$jabatan4 = $this->model_basic->select_where($this->tbl_user,'id',$instalasi->jabatan4_id);
		if ($jabatan4->num_rows() > 0) {
			$data['jabatan4'] = $jabatan4->row()->realname;
		}else{
			$data['jabatan4'] = "";
		}
	
		$data['content'] = $this->load->view('backend/admin_company/cabang_detail',$data,true);
		$this->load->view('backend/index',$data); 
    }

    function instalasi_delete(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Instalasi','instalasi');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_DELETE);
		$id = $this->input->post('id');
		$do_delete = $this->model_basic->delete($this->tbl_instalasi,'id',$id);
		if($do_delete){
                        $this->save_log_admin(ACT_DELETE, 'Delete Instalasi');
			$this->delete_folder('instalasi/'.$id);
			$this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => $data['controller'].'/'.$data['function']));
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }

    public function ajax_instalasi_list()
	{
		$list = $this->model_instalasi->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $data_row) {
			//render data
			$no++;
			
			$row = array();
			$row[] = $no;
			$row[] = '<a href="admin_company/instalasi_detail/'.$data_row->id.'">'.$data_row->nama_perusahaan.'</a>';
			$row[] = $data_row->name;
			$row[] = $data_row->nama_jenis_bisnis;
			$row[] = $data_row->alamat;
			//add html for action
			$row[] = '<div class="text-center">
						<form action="admin_company/instalasi_form" method="post">
						<input type="hidden" name="id" value="'.$data_row->id.'">
						<button class="btn btn-info btn-xs btn-edit" type="submit" data-original-title="Update" data-placement="top" data-toggle="tooltip"><i class="fa fa-edit"></i></button>
						<button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Delete" data-placement="top" data-toggle="tooltip" data-target-id="'.$data_row->id.'"><i class="fa fa-trash-o"></i></button>
						</form>
						</div>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_instalasi->count_all(),
						"recordsFiltered" => $this->model_instalasi->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_get_perusahaan() {
		$jenis_bisnis_id = $this->input->post('id');

		$results = $this->model_basic->select_where($this->tbl_perusahaan, 'jenis_bisnis_id', $jenis_bisnis_id);
		if ($results->num_rows() > 0) {
			$this->returnJson(array('status' => 'ok', 'result' => $results->result()));
		}else{
			$this->returnJson(array('status' => 'not_ok'));
		}
	}


	//business type//
	function business_type(){
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Jenis Bisnis','business_type');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);

		$data['data'] = $this->model_basic->select_all($this->tbl_jenis_bisnis);
		$data['content'] = $this->load->view('backend/admin_company/business_type',$data,true);
		$this->load->view('backend/index',$data); 
	}

	function business_type_form(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Jenis Bisnis','business_type');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);
		$id = $this->input->post('id');
		if($id){
			$data['data'] = $this->model_basic->select_where($this->tbl_jenis_bisnis,'id',$id)->row();
		}
		else{
			$data['data'] = null;
		}
		$data['content'] = $this->load->view('backend/admin_company/business_type_form',$data,true);
		$this->load->view('backend/index',$data); 
    }

    function business_type_add(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Jenis Bisnis','business_type');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);

		$table_field = $this->db->list_fields($this->tbl_jenis_bisnis);
		$insert = array();
		foreach ($table_field as $field) {
			$insert[$field] = $this->input->post($field);
		}
		$insert['date_created'] = date('Y-m-d H:i:s',now());
		$insert['id_created'] = $this->session_admin['admin_id'];
		if($insert['name']){
			$do_insert = $this->model_basic->insert_all($this->tbl_jenis_bisnis,$insert);
			if($do_insert){
                                $this->save_log_admin(ACT_CREATE, 'Insert New Jenis Bisnis '.$insert['name']);
				$this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => $data['controller'].'/'.$data['function']));
			}
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function business_type_edit(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Jenis Bisnis','business_type');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_UPDATE);
		
		$table_field = $this->db->list_fields($this->tbl_jenis_bisnis);
		$update = array();
		foreach ($table_field as $field) {
			$update[$field] = $this->input->post($field);
		}
		unset($update['date_created']);
		$update['date_modified'] = date('Y-m-d H:i:s',now());
		$update['id_modified'] = $this->session_admin['admin_id'];

		if($update['name']){
			$do_update = $this->model_basic->update($this->tbl_jenis_bisnis,$update,'id',$update['id']);
			if($do_update)
                        {
                                $this->save_log_admin(ACT_UPDATE, 'Update Jenis Bisnis '.$update['name']);
				$this->returnJson(array('status' => 'ok','msg' => 'Update success','redirect' => $data['controller'].'/'.$data['function']));
                        }
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when updating data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function business_type_delete(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Jenis Bisnis','business_type');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_DELETE);
		$id = $this->input->post('id');
		$do_delete = $this->model_basic->delete($this->tbl_jenis_bisnis,'id',$id);
		if($do_delete){
			$this->delete_folder('business_type/'.$id);
			$this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => $data['controller'].'/'.$data['function']));
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }

	public function ajax_business_type_list()
	{
		$column = array('name');
		$list = $this->model_table->get_datatables($this->tbl_jenis_bisnis,$column);
		//die(print_r($list));
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $data_row) {
			//render data
			$no++;
			
			$row = array();
			$row[] = $no;
			$row[] = $data_row->name;
			$row[] = $data_row->position;
			//add html for action
			$row[] = '<div class="text-center">
						<form action="admin_company/business_type_form" method="post">
						<input type="hidden" name="id" value="'.$data_row->id.'">
						<button class="btn btn-info btn-xs btn-edit" type="submit" data-original-title="Update" data-placement="top" data-toggle="tooltip"><i class="fa fa-edit"></i></button>
						<button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Delete" data-placement="top" data-toggle="tooltip" data-target-id="'.$data_row->id.'"><i class="fa fa-trash-o"></i></button>
						</form>
						</div>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_table->count_all($this->tbl_jenis_bisnis),
						"recordsFiltered" => $this->model_table->count_filtered($this->tbl_jenis_bisnis,$column),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	//scoring warna
	function scoring_warna(){
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Scoring Warna','scoring_warna');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);

		$data['data'] = $this->model_basic->select_all($this->tbl_asesmen_scoring_colour);
		$data['content'] = $this->load->view('backend/admin_company/scoring_warna',$data,true);
		$this->load->view('backend/index',$data); 
	}

	function scoring_warna_form(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Scoring Warna','scoring_warna');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);
		$id = $this->input->post('id');
		if($id){
			$data['data'] = $this->model_basic->select_where($this->tbl_asesmen_scoring_colour,'id',$id)->row();
			$data['data']->colour_config = $this->model_basic->select_where($this->tbl_asesmen_scoring_colour_config,'id_scoring_colour',$id)->result();
		}
		else{
			$data['data'] = null;
		}
		$data['content'] = $this->load->view('backend/admin_company/scoring_warna_form',$data,true);
		$this->load->view('backend/index',$data); 
    }

    function scoring_warna_add(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Scoring Warna','scoring_warna');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);

		$table_field = $this->db->list_fields($this->tbl_asesmen_scoring_colour);
		$insert = array();
		foreach ($table_field as $field) {
			$insert[$field] = $this->input->post($field);
		}
		$insert['date_created'] = date('Y-m-d H:i:s',now());
		$insert['id_created'] = $this->session_admin['admin_id'];

		$colour_name = $this->input->post('colour_name');
		$min = $this->input->post('min');
		$max = $this->input->post('max');
		$insert_colour_config = array();
		if($insert['name'] && count($colour_name) == 5 && count($min) == 5 && count($max) == 5){
			$do_insert = $this->model_basic->insert_all($this->tbl_asesmen_scoring_colour,$insert);
			if($do_insert){
				for ($i=0; $i < count($colour_name); $i++) { 
					$colour_config = array(
						'id_scoring_colour' => $do_insert->id,
						'name' => $colour_name[$i],
						'min' => $min[$i],
						'max' => $max[$i]
						);
					array_push($insert_colour_config, $colour_config);
				}
				$this->model_basic->insert_all_batch($this->tbl_asesmen_scoring_colour_config,$insert_colour_config);
				$this->save_log_admin(ACT_CREATE, 'Insert New Scoring Warna '.$insert['name']);
                                $this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => $data['controller'].'/'.$data['function']));
			}
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function scoring_warna_edit(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Scoring Warna','scoring_warna');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_UPDATE);
		
		$table_field = $this->db->list_fields($this->tbl_asesmen_scoring_colour);
		$update = array();
		foreach ($table_field as $field) {
			$update[$field] = $this->input->post($field);
		}
		unset($update['date_created']);
		unset($update['id_created']);
		$update['date_modified'] = date('Y-m-d H:i:s',now());
		$update['id_modified'] = $this->session_admin['admin_id'];

		$colour_name = $this->input->post('colour_name');
		$min = $this->input->post('min');
		$max = $this->input->post('max');
		$insert_colour_config = array();

		if($update['name']){
			$do_update = $this->model_basic->update($this->tbl_asesmen_scoring_colour,$update,'id',$update['id']);
			if($do_update){
				$delete_colour_config = $this->model_basic->delete_full($this->tbl_asesmen_scoring_colour_config,'id_scoring_colour',$update['id']);
				if($delete_colour_config){
					for ($i=0; $i < count($colour_name); $i++) { 
						$colour_config = array(
							'id_scoring_colour' => $update['id'],
							'name' => $colour_name[$i],
							'min' => $min[$i],
							'max' => $max[$i]
							);
						array_push($insert_colour_config, $colour_config);
					}
					$this->model_basic->insert_all_batch($this->tbl_asesmen_scoring_colour_config,$insert_colour_config);
				}
				$this->save_log_admin(ACT_UPDATE, 'Update Scoring Warna '.$update['name']);
                                $this->returnJson(array('status' => 'ok','msg' => 'Update success','redirect' => $data['controller'].'/'.$data['function']));
			}
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when updating data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function scoring_warna_delete(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Scoring Warna','scoring_warna');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_DELETE);
		$id = $this->input->post('id');
		$do_delete = $this->model_basic->delete($this->tbl_asesmen_scoring_colour,'id',$id);
		if($do_delete){
                        $this->save_log_admin(ACT_DELETE, 'Delete Scoring Warna');
			$this->delete_folder('business_type/'.$id);
			$this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => $data['controller'].'/'.$data['function']));
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }

	public function ajax_scoring_warna_list(){
		$column = array('name');
		$list = $this->model_table->get_datatables($this->tbl_asesmen_scoring_colour,$column);
		//die(print_r($list));
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $data_row) {
			//render data
			$no++;
			
			$row = array();
			$row[] = $no;
			$row[] = $data_row->name;
			//add html for action
			$row[] = '<div class="text-center">
						<form action="admin_company/scoring_warna_form" method="post">
						<input type="hidden" name="id" value="'.$data_row->id.'">
						<button class="btn btn-info btn-xs btn-edit" type="submit" data-original-title="Update" data-placement="top" data-toggle="tooltip"><i class="fa fa-edit"></i></button>
						<!--<button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Delete" data-placement="top" data-toggle="tooltip" data-target-id="'.$data_row->id.'"><i class="fa fa-trash-o"></i></button>-->
						</form>
						</div>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_table->count_all($this->tbl_asesmen_scoring_colour),
						"recordsFiltered" => $this->model_table->count_filtered($this->tbl_asesmen_scoring_colour,$column),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	//rules asms 2004//
	function rules_asms_2004(){
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Rules ASMS 2004','rules_asms_2004');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);

		$data['data'] = $this->model_basic->select_all($this->tbl_rules_asms_2004);
		$data['content'] = $this->load->view('backend/admin_company/rules_asms_2004',$data,true);
		$this->load->view('backend/index',$data); 
	}

	function rules_asms_2004_form(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Rules ASMS 2004','rules_asms_2004');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);
		$id = $this->input->post('id');
		if($id){
			$data['data'] = $this->model_basic->select_where($this->tbl_rules_asms_2004,'id',$id)->row();
		}
		else{
			$data['data'] = null;
		}
		$data['content'] = $this->load->view('backend/admin_company/rules_asms_2004_form',$data,true);
		$this->load->view('backend/index',$data); 
    }

    function rules_asms_2004_add(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Rules ASMS 2004','rules_asms_2004');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);

		$table_field = $this->db->list_fields($this->tbl_rules_asms_2004);
		$insert = array();
		foreach ($table_field as $field) {
			$insert[$field] = $this->input->post($field);
		}
		$insert['type'] = 2004;
		$insert['date_created'] = date('Y-m-d H:i:s',now());
		$insert['id_created'] = $this->session_admin['admin_id'];
		if($insert['name']){
			$do_insert = $this->model_basic->insert_all($this->tbl_rules_asms_2004,$insert);
			if($do_insert){
                            $this->save_log_admin(ACT_CREATE, 'Insert New Rules ASMS 2004 '.$insert['name']);
				$this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => $data['controller'].'/'.$data['function']));
			}
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function rules_asms_2004_edit(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Rules ASMS 2004','rules_asms_2004');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_UPDATE);
		
		$table_field = $this->db->list_fields($this->tbl_rules_asms_2004);
		$update = array();
		foreach ($table_field as $field) {
			$update[$field] = $this->input->post($field);
		}
		unset($update['date_created']);
		$update['type'] = 2004;
		$update['date_modified'] = date('Y-m-d H:i:s',now());
		$update['id_modified'] = $this->session_admin['admin_id'];

		if($update['name']){
			$do_update = $this->model_basic->update($this->tbl_rules_asms_2004,$update,'id',$update['id']);
			if($do_update)
                        {
                                $this->save_log_admin(ACT_UPDATE, 'Update Rules ASMS 2004 '.$update['name']);
				$this->returnJson(array('status' => 'ok','msg' => 'Update success','redirect' => $data['controller'].'/'.$data['function']));
                        }
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when updating data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function rules_asms_2004_delete(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Rules ASMS 2004','rules_asms_2004');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_DELETE);
		$id = $this->input->post('id');
		$do_delete = $this->model_basic->delete($this->tbl_rules_asms_2004,'id',$id);
		if($do_delete){
                        $this->save_log_admin(ACT_DELETE, 'Delete Rules ASMS 2004');
			$this->delete_folder('business_type/'.$id);
			$this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => $data['controller'].'/'.$data['function']));
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }

    public function ajax_rules_asms_2004_list(){
		$column = array('name');
		$list = $this->model_table->get_datatables($this->tbl_rules_asms_2004,$column);
		//die(print_r($list));
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $data_row) {
			//render data
			$no++;
			
			$row = array();
			$row[] = $no;
			$row[] = $data_row->name;
			$row[] = ($data_row->status == 1) ? 'Active' : 'Inactive';
			//add html for action
			$row[] = '<div class="text-center">
						<form action="admin_company/rules_asms_2004_form" method="post">
						<input type="hidden" name="id" value="'.$data_row->id.'">
						<button class="btn btn-info btn-xs btn-edit" type="submit" data-original-title="Update" data-placement="top" data-toggle="tooltip"><i class="fa fa-edit"></i></button>
						<!--<button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Delete" data-placement="top" data-toggle="tooltip" data-target-id="'.$data_row->id.'"><i class="fa fa-trash-o"></i></button>-->
						</form>
						</div>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_table->count_all($this->tbl_rules_asms_2004),
						"recordsFiltered" => $this->model_table->count_filtered($this->tbl_rules_asms_2004,$column),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	function assessment_2004_check_colour(){
		$nilai = $this->input->post('nilai');
		if($nilai)
			$warna = $this->check_colour_text($nilai,'Nilai Akhir (Asesmen 2004)');
		else
			$warna = $this->check_colour_text(0,'Nilai Akhir (Asesmen 2004)');
		$result = array(
			'status' => 'ok',
			'warna' => $warna
			);
		$this->returnJson($result);
	}

    //rules asms 2004//
	function rules_asms_2015(){
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Rules ASMS 2015','rules_asms_2015');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);

		$data['data'] = $this->model_basic->select_all($this->tbl_rules_asms_2015);
		$data['content'] = $this->load->view('backend/admin_company/rules_asms_2015',$data,true);
		$this->load->view('backend/index',$data); 
	}

	function rules_asms_2015_form(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Rules ASMS 2015','rules_asms_2015');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);
		$id = $this->input->post('id');
		if($id){
			$data['data'] = $this->model_basic->select_where($this->tbl_rules_asms_2015,'id',$id)->row();
		}
		else{
			$data['data'] = null;
		}
		$data['content'] = $this->load->view('backend/admin_company/rules_asms_2015_form',$data,true);
		$this->load->view('backend/index',$data); 
    }

    function rules_asms_2015_add(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Rules ASMS 2015','rules_asms_2015');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);

		$table_field = $this->db->list_fields($this->tbl_rules_asms_2015);
		$insert = array();
		foreach ($table_field as $field) {
			$insert[$field] = $this->input->post($field);
		}
		$insert['type'] = 2015;
		$insert['date_created'] = date('Y-m-d H:i:s',now());
		$insert['id_created'] = $this->session_admin['admin_id'];
		if($insert['name']){
			$do_insert = $this->model_basic->insert_all($this->tbl_rules_asms_2015,$insert);
			if($do_insert){
                                $this->save_log_admin(ACT_CREATE, 'Insert New Rules ASMS 2015 '.$insert['name']);
				$this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => $data['controller'].'/'.$data['function']));
			}
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function rules_asms_2015_edit(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Rules ASMS 2015','rules_asms_2015');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_UPDATE);
		
		$table_field = $this->db->list_fields($this->tbl_rules_asms_2015);
		$update = array();
		foreach ($table_field as $field) {
			$update[$field] = $this->input->post($field);
		}
		unset($update['date_created']);
		$update['type'] = 2015;
		$update['date_modified'] = date('Y-m-d H:i:s',now());
		$update['id_modified'] = $this->session_admin['admin_id'];

		if($update['name']){
			$do_update = $this->model_basic->update($this->tbl_rules_asms_2015,$update,'id',$update['id']);
			if($do_update)
                        {
                            $this->save_log_admin(ACT_UPDATE, 'Update Rules ASMS 2015 '.$update['name']);
				$this->returnJson(array('status' => 'ok','msg' => 'Update success','redirect' => $data['controller'].'/'.$data['function']));
                        }
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when updating data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function rules_asms_2015_delete(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Rules ASMS 2015','rules_asms_2015');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_DELETE);
		$id = $this->input->post('id');
		$do_delete = $this->model_basic->delete($this->tbl_rules_asms_2015,'id',$id);
		if($do_delete){
			$this->delete_folder('business_type/'.$id);
                        $this->save_log_admin(ACT_DELETE, 'Delete Rules ASMS 2015');
			$this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => $data['controller'].'/'.$data['function']));
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }

	public function ajax_rules_asms_2015_list(){
		$column = array('name');
		$list = $this->model_table->get_datatables($this->tbl_rules_asms_2015,$column);
		//die(print_r($list));
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $data_row) {
			//render data
			$no++;
			
			$row = array();
			$row[] = $no;
			$row[] = $data_row->name;
			$row[] = ($data_row->status == 1) ? 'Active' : 'Inactive';
			//add html for action
			$row[] = '<div class="text-center">
						<form action="admin_company/rules_asms_2015_form" method="post">
						<input type="hidden" name="id" value="'.$data_row->id.'">
						<button class="btn btn-info btn-xs btn-edit" type="submit" data-original-title="Update" data-placement="top" data-toggle="tooltip"><i class="fa fa-edit"></i></button>
						<!-- <button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Delete" data-placement="top" data-toggle="tooltip" data-target-id="'.$data_row->id.'"><i class="fa fa-trash-o"></i></button> -->
						</form>
						</div>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_table->count_all($this->tbl_rules_asms_2015),
						"recordsFiltered" => $this->model_table->count_filtered($this->tbl_rules_asms_2015,$column),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	function assessment_2015_check_colour(){
		$nilai = $this->input->post('nilai');
		if($nilai)
			$warna = $this->check_colour_text($nilai,'Nilai Akhir (Asesmen 2015)');
		else
			$warna = $this->check_colour_text(0,'Nilai Akhir (Asesmen 2015)');
		$result = array(
			'status' => 'ok',
			'warna' => $warna
			);
		$this->returnJson($result);
	}

	//sub business//
	function sub_business(){
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Sub Bisnis','sub_business');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);

		$data['data'] = $this->model_basic->select_all($this->tbl_sub_bisnis);

		$data['content'] = $this->load->view('backend/admin_company/sub_business',$data,true);
		$this->load->view('backend/index',$data); 
	}

	function sub_business_form(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Sub Bisnis','sub_business');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);
		$id = $this->input->post('id');

		$data['business_type'] = $this->model_basic->select_where_order($this->tbl_jenis_bisnis,'delete_flag',0,'name','asc');
		if($id){
			$data['data'] = $this->model_basic->select_where($this->tbl_sub_bisnis,'id',$id)->row();
		}
		else{
			$data['data'] = null;
		}

		$data['content'] = $this->load->view('backend/admin_company/sub_business_form',$data,true);
		$this->load->view('backend/index',$data); 
    }

    function sub_business_add(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Sub Bisnis','sub_business');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);

		$table_field = $this->db->list_fields($this->tbl_sub_bisnis);
		$insert = array();
		foreach ($table_field as $field) {
			$insert[$field] = $this->input->post($field);
		}
		$insert['date_created'] = date('Y-m-d H:i:s',now());
		$insert['id_created'] = $this->session_admin['admin_id'];
		if($insert['name']){
			$do_insert = $this->model_basic->insert_all($this->tbl_sub_bisnis,$insert);
			if($do_insert){
                                $this->save_log_admin(ACT_CREATE, 'Insert New Sub Bisnis '.$insert['name']);
				$this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => $data['controller'].'/'.$data['function']));
			}
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function sub_business_edit(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Sub Bisnis','sub_business');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_UPDATE);
		
		$table_field = $this->db->list_fields($this->tbl_sub_bisnis);
		$update = array();
		foreach ($table_field as $field) {
			$update[$field] = $this->input->post($field);
		}
		unset($update['date_created']);
		$update['date_modified'] = date('Y-m-d H:i:s',now());
		$update['id_modified'] = $this->session_admin['admin_id'];

		if($update['name']){
			$do_update = $this->model_basic->update($this->tbl_sub_bisnis,$update,'id',$update['id']);
			if($do_update)
                        {
                            $this->save_log_admin(ACT_UPDATE, 'Update Sub Bisnis '.$update['name']);
				$this->returnJson(array('status' => 'ok','msg' => 'Update success','redirect' => $data['controller'].'/'.$data['function']));
                        }
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when updating data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function sub_business_delete(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Sub Bisnis','sub_business');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_DELETE);
		$id = $this->input->post('id');
		$do_delete = $this->model_basic->delete($this->tbl_sub_bisnis,'id',$id);
		if($do_delete){
			$this->delete_folder('sub_business/'.$id);
                        $this->save_log_admin(ACT_DELETE, 'Delete Sub Bisnis');
			$this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => $data['controller'].'/'.$data['function']));
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }

	public function ajax_sub_business_list(){
		$column = array('jenis_bisnis_id','name');
		$list = $this->model_table->get_datatables($this->tbl_sub_bisnis,$column);
		//die(print_r($list));
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $data_row) {
			//render data
			$jenis_bisnis_nm = $this->model_basic->select_where($this->tbl_jenis_bisnis,'id',$data_row->jenis_bisnis_id)->row()->name;

			$no++;

			$row = array();
			$row[] = $no;
			$row[] = $jenis_bisnis_nm;
			$row[] = $data_row->name;
			//add html for action
			$row[] = '<div class="text-center">
						<form action="admin_company/sub_business_form" method="post">
						<input type="hidden" name="id" value="'.$data_row->id.'">
						<button class="btn btn-info btn-xs btn-edit" type="submit" data-original-title="Update" data-placement="top" data-toggle="tooltip"><i class="fa fa-edit"></i></button>
						<button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Delete" data-placement="top" data-toggle="tooltip" data-target-id="'.$data_row->id.'"><i class="fa fa-trash-o"></i></button>
						</form>
						</div>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_table->count_all($this->tbl_sub_bisnis),
						"recordsFiltered" => $this->model_table->count_filtered($this->tbl_sub_bisnis,$column),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	//assessment 2004//
	function assessment2004(){
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Asesmen 2004','assessment2004');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);

		$data['rules'] = $this->model_basic->select_where($this->tbl_rules_asms_2004,'status',1)->result();
		$data['data'] = $this->model_basic->select_all($this->tbl_assessment2004);
		$data['content'] = $this->load->view('backend/admin_company/assessment2004',$data,true);
		$this->load->view('backend/index',$data); 
	}

	function assessment2004_view($id){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Asesmen 2004','assessment2004');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);

		if($id){
			$id = $id;
			$data['rules'] = $this->model_basic->select_where($this->tbl_asesmen_2004_value,'id_asesmen',$id)->result();
			$total_value = 0;
			if(count($data['rules']) > 0) {
				foreach ($data['rules'] as $r) {
					$rules_data = $this->model_basic->select_where($this->tbl_rules_asms_2004,'id',$r->id_rules)->row();
					$r->name = $rules_data->name;
					$total_value = $total_value + $r->value;
				}
			}
			else
				$data['rules'] = $this->model_basic->select_where($this->tbl_rules_asms_2004,'status',1)->result();

			$data['data'] = $this->model_basic->select_where($this->tbl_assessment2004,'id',$id)->row();

			$data['data']->nilai_akhir = $this->check_colour(number_format($data['data']->nilai_akhir,2),'Nilai Akhir (Asesmen 2004)');

			$instalasi = $this->model_basic->select_where($this->tbl_instalasi,'id',$data['data']->instalasi_id)->row();
			if($instalasi)
				$data['data']->nama_instalasi = $instalasi->name;
			else
				$data['data']->nama_instalasi = ' ';

			$data['content'] = $this->load->view('backend/admin_company/assessment2004_view',$data,true);
			$this->load->view('backend/index',$data);
		}
		else
			redirect($data['function']);
    }

	function assessment2004_form(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Asesmen 2004','assessment2004');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);
		$id = $this->input->post('id');

		$data['company'] = $this->model_basic->select_where_order($this->tbl_instalasi,'delete_flag',0, 'name', 'ASC');
		if($id){
			$data['rules'] = $this->model_basic->select_where($this->tbl_asesmen_2004_value,'id_asesmen',$id)->result();
			if(count($data['rules']) > 0) {
				foreach ($data['rules'] as $r) {
					$rules_data = $this->model_basic->select_where($this->tbl_rules_asms_2004,'id',$r->id_rules)->row();
					$r->name = $rules_data->name;
					$r->id = $rules_data->id;
				}
			}
			else
				$data['rules'] = $this->model_basic->select_where($this->tbl_rules_asms_2004,'status',1)->result();
			$data['data'] = $this->model_basic->select_where($this->tbl_assessment2004,'id',$id)->row();
		}
		else{
			$data['rules'] = $this->model_basic->select_where($this->tbl_rules_asms_2004,'status',1)->result();
			$data['data'] = null;
		}
		$data['content'] = $this->load->view('backend/admin_company/assessment2004_form',$data,true);
		$this->load->view('backend/index',$data); 
    }

    function assessment2004_add(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Asesmen 2004','assessment2004');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);

		$table_field = $this->db->list_fields($this->tbl_assessment2004);
		$insert = array();
		foreach ($table_field as $field) {
			$insert[$field] = $this->input->post($field);
		}
		$insert['date_created'] = date('Y-m-d H:i:s',now());
		$insert['id_created'] = $this->session_admin['admin_id'];
		if($insert['instalasi_id']){
                    if($this->check_instalasi($insert['instalasi_id'], 2004, $insert['periode']))
                    {
			$do_insert = $this->model_basic->insert_all($this->tbl_assessment2004,$insert);
			if($do_insert){
				$rules_id = $this->input->post('rules_id');
				$rules_value = $this->input->post('rules_value');
				$insert_rules_value = array();
				for ($i=0; $i < count($rules_id); $i++) { 
					$rules_value_data = array(
						'id_asesmen' => $do_insert->id,
						'id_rules' => $rules_id[$i],
						'value' => $rules_value[$rules_id[$i]]
						);
					array_push($insert_rules_value, $rules_value_data);
				}
				if(count($insert_rules_value) > 0)
					$do_insert_rules_value = $this->model_basic->insert_all_batch($this->tbl_asesmen_2004_value,$insert_rules_value);
				$instalasi = $this->model_basic->select_where($this->tbl_instalasi, 'id', $insert['instalasi_id'])->row();
                                $this->save_log_admin(ACT_CREATE, 'Insert New ASMS 2004 '.$instalasi->name);
                                $this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => $data['controller'].'/'.$data['function']));
			}
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));
                    }
                    else
                        $this->returnJson(array('status' => 'error','msg' => 'Instalasi Data Already Existing, Please Choose Another'));
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function assessment2004_edit(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Asesmen 2004','assessment2004');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_UPDATE);
		
		$table_field = $this->db->list_fields($this->tbl_assessment2004);
		$update = array();
		foreach ($table_field as $field) {
			$update[$field] = $this->input->post($field);
		}
		unset($update['date_created']);
		$update['date_modified'] = date('Y-m-d H:i:s',now());
		$update['id_modified'] = $this->session_admin['admin_id'];
		$rules_id = $this->input->post('rules_id');
		$rules_value = $this->input->post('rules_value');
                if($this->check_instalasi($update['instalasi_id'], 2004, $update['periode'], $update['id']))
                {
                    $insert_rules_value = array();
                    for ($i=0; $i < count($rules_id); $i++) { 
                            $rules_value_data = array(
                                    'id_asesmen' => $update['id'],
                                    'id_rules' => $rules_id[$i],
                                    'value' => $rules_value[$rules_id[$i]]
                                    );
                            array_push($insert_rules_value, $rules_value_data);
                    }
                    if(count($insert_rules_value) > 0){
                            $this->model_basic->delete_full($this->tbl_asesmen_2004_value,'id_asesmen',$update['id']);
                            $do_insert_rules_value = $this->model_basic->insert_all_batch($this->tbl_asesmen_2004_value,$insert_rules_value);
                    }

                    if($update['instalasi_id']){
                            $do_update = $this->model_basic->update($this->tbl_assessment2004,$update,'id',$update['id']);
                            if($do_update)
                            {
                                $instalasi = $this->model_basic->select_where($this->tbl_instalasi, 'id', $update['instalasi_id'])->row();
                                $this->save_log_admin(ACT_UPDATE, 'Update ASMS 2004 '.$instalasi->name);
                                    $this->returnJson(array('status' => 'ok','msg' => 'Update success','redirect' => $data['controller'].'/'.$data['function']));
                            }
                            else
                                    $this->returnJson(array('status' => 'error','msg' => 'Failed when updating data'));	
                    }
                    else
                            $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
                }
                else
                    $this->returnJson(array('status' => 'error','msg' => 'Instalasi Data Already Existing, Please Choose Another'));
    }

    function assessment2004_delete(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Asesmen 2004','assessment2004');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_DELETE);
		$id = $this->input->post('id');
                $asesmen = $this->model_basic->select_where($this->tbl_assessment2004, 'id', $id)->row();
                $instalasi = $this->model_basic->select_where($this->tbl_instalasi, 'id', $asesmen->instalasi_id)->row();
		$do_delete = $this->model_basic->delete($this->tbl_assessment2004,'id',$id);
		if($do_delete){
                        $this->save_log_admin(ACT_DELETE, 'Delete ASMS 2004 '.$instalasi->name);
			$this->delete_folder('assessment2004/'.$id);
			$this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => $data['controller'].'/'.$data['function']));
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }

	public function ajax_assessment2004_list()
	{
		$list = $this->model_assessment2004->get_datatables();
		
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $data_row) {
			//render data
			$asesmen_value = $this->model_basic->select_where($this->tbl_asesmen_2004_value,'id_asesmen',$data_row->id)->result();
			$no++;

			$row = array();
			$row[] = $no;
			$row[] = $data_row->name;
			$row[] = $data_row->periode;
			$row[] = number_format($asesmen_value[0]->value,2);
			$row[] = number_format($asesmen_value[1]->value,2);
			$row[] = number_format($asesmen_value[2]->value,2);
			$row[] = number_format($asesmen_value[3]->value,2);
			$row[] = $this->check_colour(number_format($data_row->nilai_akhir,2),'Nilai Akhir (Asesmen 2004)');
			$row[] = '<a href="admin_company/assessment2004_view/'.$data_row->id.'">Critical Point</a>';
			$row[] = date('d-m-Y',strtotime($data_row->date_created));
			//add html for action
			$row[] = '<div class="text-center">
						<form action="admin_company/assessment2004_form" method="post">
						<input type="hidden" name="id" value="'.$data_row->id.'">
						<button class="btn btn-info btn-xs btn-edit" type="submit" data-original-title="Update" data-placement="top" data-toggle="tooltip"><i class="fa fa-edit"></i></button>
						<button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Delete" data-placement="top" data-toggle="tooltip" data-target-id="'.$data_row->id.'"><i class="fa fa-trash-o"></i></button>
						</form>
						</div>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_assessment2004->count_all(),
						"recordsFiltered" => $this->model_assessment2004->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

		//assessment 2015//
	function assessment2015()
	{
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Asesmen 2015','assessment2015');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);
		$data['data'] = $this->model_basic->select_all($this->tbl_assessment2015);
		// Added by Alvin, 2019-08-26 13:50 PM
		$data['create'] = ($this->session->userdata['admin']['id_usergroup']=='15' or $this->session->userdata['admin']['name_usergroup']=='Super Admin Khusus') ? 0 : 1;
		// 
		$data['content'] = $this->load->view('backend/admin_company/assessment2015',$data,true);
		$this->load->view('backend/index',$data); 
	}
	function assessment2015_1()
	{
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Asesmen 2015','assessment2015');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);
		$data['data'] = $this->model_basic->select_all($this->tbl_assessment2015);
		$data['content'] = $this->load->view('backend/admin_company/assessment2015_1',$data,true);
		$this->load->view('backend/index',$data); 
	}

	function assessment2015_view($id, $param=null){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Asesmen 2015','assessment2015');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);

		if($id){
			$id = $id;
			$data['rules'] = $this->model_basic->select_where($this->tbl_asesmen_2015_value,'id_asesmen',$id)->result();
			$total_value = 0;
			if(count($data['rules']) > 0) {
				foreach ($data['rules'] as $r) {
					$rules_data = $this->model_basic->select_where($this->tbl_rules_asms_2015,'id',$r->id_rules)->row();
					$r->name = $rules_data->name;
					$total_value = $total_value + $r->value;
				}
			}
			else
				$data['rules'] = $this->model_basic->select_where($this->tbl_rules_asms_2015,'status',1)->result();

			$data['data'] = $this->model_basic->select_where($this->tbl_assessment2015,'id',$id)->row();

			// Add by: Alvin (atsalvin0017), Date: 2019-08-19 11:46 AM
			$data['history_revise'] = [];
			if ($param and $param=='detail') {
				$history = $this->model_basic->select_where($this->tbl_assessment2015_history,'asesmen_2015_id',$data['data']->id)->result();
				foreach ($history as $k=>$row) {
					if ($row->revise)
						$data['history_revise'][] = $row;
				}
			}
			//

			$data['data']->pemenuhan_system = $this->check_colour(number_format($total_value/4,2),'Pemenuhan System (Asesmen 2015)');
			$data['data']->security_performance = $this->check_colour(number_format($data['data']->security_performance,2),'Security Performance (Asesmen 2015)');
			$data['data']->security_reliability = $this->check_colour(number_format(($data['data']->people + $data['data']->device_and_infrastructure) / 2,2),'Security Reliability (Asesmen 2015)');
			$data['data']->csi = $this->check_colour(number_format($data['data']->csi,2),'CSI (Asesmen 2015)');
			$data['data']->nilai_akhir = $this->check_colour(number_format($data['data']->nilai_akhir,2),'Nilai Akhir (Asesmen 2015)');

			$instalasi = $this->model_basic->select_where($this->tbl_instalasi,'id',$data['data']->instalasi_id)->row();
			if($instalasi)
				$data['data']->nama_instalasi = $instalasi->name;
			else
				$data['data']->nama_instalasi = ' ';

			$data['content'] = $this->load->view('backend/admin_company/assessment2015_view',$data,true);
			$this->load->view('backend/index',$data);
		}
		else
			redirect($data['function']);
    }

	function assessment2015_form(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Asesmen 2015','assessment2015');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);
		$id = $this->input->post('id');

		$data['company'] = $this->model_basic->select_where_order($this->tbl_instalasi,'delete_flag',0, 'name', 'ASC');
		if($id){
			$data['rules'] = $this->model_basic->select_where($this->tbl_asesmen_2015_value,'id_asesmen',$id)->result();
			if(count($data['rules']) > 0) {
				foreach ($data['rules'] as $r) {
					$rules_data = $this->model_basic->select_where($this->tbl_rules_asms_2015,'id',$r->id_rules)->row();
					$r->name = $rules_data->name;
					$r->id = $rules_data->id;
				}
			}
			else
				$data['rules'] = $this->model_basic->select_where($this->tbl_rules_asms_2015,'status',1)->result();
			$data['data'] = $this->model_basic->select_where($this->tbl_assessment2015,'id',$id)->row();

			// Add by: Alvin (atsalvin0017), Date: 2019-08-19 11:46 AM
			$data['history_revise'] = [];
			// if ($param and $param=='detail') {
				$history = $this->model_basic->select_where($this->tbl_assessment2015_history,'asesmen_2015_id',$data['data']->id)->result();
				foreach ($history as $k=>$row) {
					if ($row->revise)
						$data['history_revise'][] = $row;
				}
			// }
			//
		}
		else{
			$data['rules'] = $this->model_basic->select_where($this->tbl_rules_asms_2015,'status',1)->result();
			$data['data'] = null;
			$data['history_revise'] = [];
		}
		$data['content'] = $this->load->view('backend/admin_company/assessment2015_form',$data,true);
		$this->load->view('backend/index',$data); 
    }

    /*
		Updated by: Alvin (atsalvin0017)
		Date: 2019-08-09 10:52 AM
		Action: Add status for add assessment
    */
    function assessment2015_add(){ 
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Asesmen 2015','assessment2015');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);

		$table_field = $this->db->list_fields($this->tbl_assessment2015);
		$insert = array();
		foreach ($table_field as $field) {
			// Add by: Alvin (atsalvin0017), Date: 2019-08-09 10:52 AM
			if ($field!='id') { 
				$insert[$field] = $this->input->post($field);
			}
			// $insert[$field] = $this->input->post($field);
		}
		$insert['date_created'] = date('Y-m-d H:i:s',now());
		$insert['id_created'] = $this->session_admin['admin_id'];
		$insert['status'] = 3; // Add by: Alvin (atsalvin0017), Date: 2019-08-09 10:52 AM
		if($insert['instalasi_id']){
            if($this->check_instalasi($insert['instalasi_id'], 2015, $insert['periode']))
            {
				$do_insert = $this->model_basic->insert_all($this->tbl_assessment2015,$insert);
				if($do_insert)
				{
					// Insert to Asesmen 2015 history
					$insert_asesment2015_history = [];
					$insert_asesment2015_history['asesmen_2015_id'] = $do_insert->id;
					$insert_asesment2015_history['user_id'] = $this->session_admin['admin_id'];
					$insert_asesment2015_history['user_status'] = 'Admin';
					$insert_asesment2015_history['action'] = 'Submit';
					$insert_asesment2015_history['date_created'] = date('Y-m-d H:i:s',now());
					$insert_asesment2015_history['status'] = 1;
					$do_insert_asesment2015_history = $this->model_basic->insert_all($this->tbl_assessment2015_history,$insert_asesment2015_history);
					if ($do_insert_asesment2015_history) {
						$rules_id = $this->input->post('rules_id');
						$rules_value = $this->input->post('rules_value');
						$insert_rules_value = array();

						for ($i=0; $i < count($rules_id); $i++) 
						{ 
							$rules_value_data = array(
								'id_asesmen' => $do_insert->id,
								'id_rules' => $rules_id[$i],
								'value' => $rules_value[$rules_id[$i]],
								'delete_flag' => 0 // Add by: Alvin (atsalvin0017), Date: 2019-08-09 10:52 AM
							);
							array_push($insert_rules_value, $rules_value_data);
						}
				
						if(count($insert_rules_value) > 0)
						{
							$do_insert_rules_value = $this->model_basic->insert_all_batch($this->tbl_asesmen_2015_value,$insert_rules_value);
							$instalasi = $this->model_basic->select_where($this->tbl_instalasi, 'id', $insert['instalasi_id'])->row();

							// Send Email
					        // $recipient = [];
				            // $recipient['email'] = 'dedy.halim@ai.astra.co.id';
				            // $recipient['name'] = 'Dedy Halim';
				            // $recipient['email'] = 'nadia.natasatresiasitorus@ai.astra.co.id';
				            // $recipient['name'] = 'Nadia Natasatresiasitorus';
				            // $this->send_email_division_head($recipient);
							$where = [];
					        $where['delete_flag'] = 0;
					        $where['id_usergroup'] = 15;
					        $where['email_notification'] = 1;
					        $division_head = $this->model_basic->select_where_array($this->tbl_admin, $where)->result();
					        foreach ($division_head as $key => $value) {
					            $recipient = [];
					            $recipient['email'] = $value->email;
					            $recipient['name'] = $value->realname;
					            $this->send_email_division_head($recipient);
					        }
					        // 

	                        $this->save_log_admin(ACT_CREATE, 'Insert New ASMS 2015 '.$instalasi->name);
	                        $this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => $data['controller'].'/'.$data['function']));
	                    }
					} 
					else
					{
						$this->returnJson(array('status' => 'error','msg' => 'Failed when saving data history'));
					}
				}
				else
				{
					$this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));
				}
	        }
	        else
	        {
	            $this->returnJson(array('status' => 'error','msg' => 'Instalasi Data Already Existing, Please Choose Another'));
	        }
		}
		else
		{
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
		}
    }

    function assessment2015_edit(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Asesmen 2015','assessment2015');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_UPDATE);
		
		$table_field = $this->db->list_fields($this->tbl_assessment2015);
		$update = array();
		foreach ($table_field as $field) {
			$update[$field] = $this->input->post($field);
		}
		unset($update['date_created']);
		$update['date_modified'] = date('Y-m-d H:i:s',now());
		$update['id_modified'] = $this->session_admin['admin_id'];
		
		$rules_id = $this->input->post('rules_id');
		$rules_value = $this->input->post('rules_value');
        if($this->check_instalasi($update['instalasi_id'], 2015, $update['periode'], $update['id']))
        {
            $insert_rules_value = array();
            for ($i=0; $i < count($rules_id); $i++) { 
                    $rules_value_data = array(
                            'id_asesmen' => $update['id'],
                            'id_rules' => $rules_id[$i],
                            'value' => $rules_value[$rules_id[$i]],
							'delete_flag' => 0 // Add by: Alvin (atsalvin0017), Date: 2019-08-09 10:52 AM
                            );
                    array_push($insert_rules_value, $rules_value_data);
            }
            if(count($insert_rules_value) > 0){
                    $this->model_basic->delete_full($this->tbl_asesmen_2015_value,'id_asesmen',$update['id']);
                    $do_insert_rules_value = $this->model_basic->insert_all_batch($this->tbl_asesmen_2015_value,$insert_rules_value);
            }

            if($update['instalasi_id']){
                    $do_update = $this->model_basic->update($this->tbl_assessment2015,$update,'id',$update['id']);
                    if($do_update)
                    {
                            $instalasi = $this->model_basic->select_where($this->tbl_instalasi, 'id', $update['instalasi_id'])->row();
                            $this->save_log_admin(ACT_UPDATE, 'Update ASMS 2015 '.$instalasi->name);
                            $this->returnJson(array('status' => 'ok','msg' => 'Update success','redirect' => $data['controller'].'/'.$data['function']));
                    }
                    else
                            $this->returnJson(array('status' => 'error','msg' => 'Failed when updating data'));	
            }
            else
                    $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
        }
        else
            $this->returnJson(array('status' => 'error','msg' => 'Instalasi Data Already Existing, Please Choose Another'));
    }

    function assessment2015_delete(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Asesmen 2015','assessment2015');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_DELETE);
		$id = $this->input->post('id');
                $asesmen = $this->model_basic->select_where($this->tbl_assessment2015, 'id', $id)->row();
                $instalasi = $this->model_basic->select_where($this->tbl_instalasi, 'id', $asesmen->instalasi_id)->row();
		$do_delete = $this->model_basic->delete($this->tbl_assessment2015,'id',$id);
		if($do_delete){
			$this->delete_folder('assessment2015/'.$id);
                        $this->save_log_admin(ACT_DELETE, 'Delete ASMS 2015 '.$instalasi->name);
			$this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => $data['controller'].'/'.$data['function']));
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }

	public function ajax_assessment2015_list()
	{
		//$column = array('instalasi_id','periode','nilai_akhir','date_created');
		$list = $this->model_assessment2015->get_datatables();
		//die(print_r($this->db->last_query()));
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $data_row) {
			//render data
			$no++;
			$w_ps = $this->check_colour_text_dashboard(number_format($data_row->pemenuhan_system,2),'Pemenuhan System (Asesmen 2015)');
			$w_sp = $this->check_colour_text_dashboard(number_format($data_row->security_performance,2),'Security Performance (Asesmen 2015)');
			$w_sr = $this->check_colour_text_dashboard(number_format($data_row->security_reliability,2),'Security Reliability (Asesmen 2015)');
			$w_csi = $this->check_colour_text_dashboard(number_format($data_row->csi,2),'CSI (Asesmen 2015)');
			$semua_warna = array($w_ps,$w_sp,$w_sr,$w_csi);
			$w_na = '<div class="bg-Emas text-center">'.number_format($data_row->nilai_akhir,2).'</div>';
			if(in_array('hijau', $semua_warna))
				$w_na = '<div class="bg-Hijau text-center">'.number_format($data_row->nilai_akhir,2).'</div>';
			if(in_array('biru', $semua_warna))
				$w_na = '<div class="bg-Biru text-center">'.number_format($data_row->nilai_akhir,2).'</div>';
			if(in_array('merah', $semua_warna))
				$w_na = '<div class="bg-Merah text-center">'.number_format($data_row->nilai_akhir,2).'</div>';
			if(in_array('hitam', $semua_warna))
				$w_na = '<div class="bg-Hitam text-center">'.number_format($data_row->nilai_akhir,2).'</div>';
			$row = array();
			$row[] = $no;
			$row[] = $data_row->name;
			$row[] = $data_row->periode;
			$row[] = $this->check_colour(number_format($data_row->pemenuhan_system,2),'Pemenuhan System (Asesmen 2015)');
			$row[] = $this->check_colour(number_format($data_row->security_performance,2),'Security Performance (Asesmen 2015)');
			$row[] = $this->check_colour(number_format($data_row->security_reliability,2),'Security Reliability (Asesmen 2015)');
			$row[] = $this->check_colour(number_format($data_row->csi,2),'CSI (Asesmen 2015)');
			$row[] = $w_na;
			$row[] = '<a href="admin_company/assessment2015_view/'.$data_row->id.'">Critical Point</a>';
			$row[] = date('d-m-Y',strtotime($data_row->date_created));
			// Add by: Alvin (atsalvin0017), Date: 2019-08-19 11:30 AM
			// $row[] = '<center><a href="admin_company/assessment2015_view/'.$data_row->id.'/detail">Detail Assessment</a></center>';
			$row[] = '<div class="text-center">
						<form action="admin_company/assessment2015_form" method="post">
						<input type="hidden" name="id" value="'.$data_row->id.'">
						<button class="btn-link" style="font-weight: 400; color: #428bca; padding: 0;" type="submit" data-original-title="Detail" data-placement="top" data-toggle="tooltip">Detail Assessment</button>
						</form>
						</div>';
			// 
			//add html for action
			// $row[] = '<div class="text-center">
			// 			<form action="admin_company/assessment2015_form" method="post">
			// 			<input type="hidden" name="id" value="'.$data_row->id.'">
			// 			<button class="btn btn-info btn-xs btn-edit" type="submit" data-original-title="Update" data-placement="top" data-toggle="tooltip"><i class="fa fa-edit"></i></button>
			// 			<button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Delete" data-placement="top" data-toggle="tooltip" data-target-id="'.$data_row->id.'"><i class="fa fa-trash-o"></i></button>
			// 			</form>
			// 			</div>';
			
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_assessment2015->count_all(),
						"recordsFiltered" => $this->model_assessment2015->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
        
    function check_instalasi($instalasi_id, $asesmen, $periode, $row_id = 0)
    {
        $where_array = array('instalasi_id' => $instalasi_id, 'periode' => $periode);
        if($row_id == 0)
        {
            $result = $this->model_basic->select_where_array('px_asesmen_'.$asesmen, $where_array);
            if($result->num_rows() != 0)
                return FALSE;
            return TRUE;
        }
        else
        {
            $result = $this->model_basic->select_where('px_asesmen_'.$asesmen, 'id', $row_id);
            if($result->num_rows() == 1)
            {
                if($result->row()->instalasi_id == $instalasi_id)
                    return TRUE;
                else
                {
                    $result2 = $this->model_basic->select_where_array('px_asesmen_'.$asesmen, $where_array);
                    if($result2->num_rows() != 0)
                        return FALSE;
                    return TRUE;
                }
            }
            else
                return FALSE;
        }
        
    }


}

/* End of file admin_company.php */
/* Location: ./application/controllers/admin_company.php */