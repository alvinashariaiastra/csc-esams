<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_contact_us extends PX_Controller {

	public function __construct() {
		parent::__construct();
		$this->check_login();
		$this->controller_attr = array('controller' => 'admin_contact_us','controller_name' => 'Admin Contact Us','controller_id' => 0);
	}

	public function index()
	{
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Admin Contact Us','admin_contact_us');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);

		$data['content'] = $this->load->view('backend/admin_contact_us/index',$data,true);
		$this->load->view('backend/index',$data); 
	}

	public function message($user_id) {
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Message','admin_contact_us');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);

		$admin_id = $this->session_admin['admin_id'];
		$data['admin'] = $this->model_basic->select_where($this->tbl_admin, 'id', $admin_id)->row();
                $user = $this->model_basic->select_where($this->tbl_user, 'id', $user_id);
                if($user->num_rows() != 1)
                    redirect('admin_contact_us');
                else
                    $data['user'] = $user->row();
		
		$data['message'] = $message = $this->model_message->get_message($user_id);
                $this->model_message->update_readable_messages($user_id, 1);

		$data['content'] = $this->load->view('backend/admin_contact_us/message',$data,true);
		$this->load->view('backend/index',$data); 
	}

	function admin_contact_us_delete(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Admin Contact Us','admin_contact_us');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_DELETE);
		$id = $this->input->post('user_id');
		$do_delete = $this->model_basic->delete($this->tbl_message,'user_id',$id);
		if($do_delete){
			$this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => $data['controller'].'/'.$data['function']));
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }

	public function ajax_user_list()
	{
		$list = $this->model_message->get_datatables();
		//die(print_r($this->db->last_query()));
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $data_row) {
                        $new_messages = $this->model_message->get_count_unread_messages_admin_by_user_id($data_row->user_id);
			//render data
			$no++;
			
			$row = array();
			$row[] = $no;
			$row[] = $data_row->realname;
			$row[] = $data_row->instalasi_nm;
                        if($new_messages == 0)
                            $row[] = '<a class="btn btn-success" href="admin_contact_us/message/'.$data_row->user_id.'">'.$new_messages.' New Message</a>';
                        else
                            $row[] = '<a class="btn btn-danger" href="admin_contact_us/message/'.$data_row->user_id.'">'.$new_messages.' New Message</a>';
			//add html for action
			$row[] = '<div class="text-center">
						<a href="admin_contact_us/message/'.$data_row->user_id.'"><button class="btn btn-info btn-xs btn-edit" data-original-title="View Message" data-placement="top" data-toggle="tooltip"><i class="fa fa-eye"></i></button></a>
						<button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Delete" data-placement="top" data-toggle="tooltip" data-target-id="'.$data_row->user_id.'"><i class="fa fa-trash-o"></i></button>
						</div>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_message->count_all(),
						"recordsFiltered" => $this->model_message->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	function ajax_message_save() {
		$admin_id = $this->session_admin['admin_id'];
		$user_id = $this->input->post('user_id');
		$message = $this->input->post('message_txt');

		$insert = array(
			'user_id' => $user_id,
			'admin_id' => $admin_id,
			'message' => $message,
			'type' => 0,
			'status' => 0,
			'id_created' => $user_id,
			'date_created' => date('Y-m-d H:i:s', now())
		);

		$save_message = $this->model_basic->insert_all($this->tbl_message, $insert);
		if ($save_message) {
			$admin = $this->model_basic->select_where($this->tbl_admin, 'id', $admin_id)->row();
			$row = $this->model_message->get_message_by_id($save_message->id)->row();
			if ($row->type == 1) {
				$ret = '<div class="item in item-visible">
                        <div class="image">
                            <img src="assets/uploads/user/'.$row->user_id.'/'.$row->photo.'" alt="'.$row->realname.'">
                        </div>
                        <div class="text">
                            <div class="heading">
                                <a href="#">'.$row->realname.'</a>
                                <span class="date">'.date('H:i', strtotime($row->date_created)).'</span>
                            </div>
                            '.$row->message.'
                        </div>
                    </div>';
			}else{
				$ret = '<div class="item item-visible">
                        <div class="image">
                            <img src="assets/uploads/admin/'.$admin_id.'/'.$admin->photo.'" alt="'.$admin->realname.'">
                        </div>
                        <div class="text">
                            <div class="heading">
                                <a href="#">'.$admin->realname.'</a>
                                <span class="date">'.date('H:i', strtotime($row->date_created)).'</span>
                            </div>
                            '.$row->message.'
                        </div>
                    </div>';
			}
			
			echo $ret;
		}else{
			$ret = "";
			echo $ret;
		}		
	}

}

/* End of file admin_contact_us.php */
/* Location: ./application/controllers/admin_contact_us.php */