<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_info_training extends PX_Controller {

	public function __construct() {
		parent::__construct();
		$this->check_login();
		$this->controller_attr = array('controller' => 'admin_info_training','controller_name' => 'Admin Info Training','controller_id' => 0);
	}

	public function index()
	{
		$this->info();
	}

	function info(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Info','info');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);
		$data['data'] = $this->model_basic->select_all($this->tbl_training_info);
		$data['content'] = $this->load->view('backend/admin_info_training/info_training',$data,true);
		$this->load->view('backend/index',$data); 
    }

    function info_form(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Info Training','info');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);
		$id = $this->input->post('id');
		$data['pic'] = $this->model_basic->select_all_order($this->tbl_admin, 'realname', 'ASC');
		if($id){
			$data['data'] = $this->model_basic->select_where($this->tbl_training_info,'id',$id)->row();
			$content = new domDocument;
			libxml_use_internal_errors(true);
			$content->loadHTML($data['data']->content);
			libxml_use_internal_errors(false);
			$content->preserveWhiteSpace = false;
			$images = $content->getElementsByTagName('img');
			if($images){
				foreach ($images as $image) {
				  $data['data']->image[] = $image->getAttribute('src');
				}
			}
		}
		else
			$data['data'] = null;
		$data['content'] = $this->load->view('backend/admin_info_training/info_training_form',$data,true);
		$this->load->view('backend/index',$data); 
    }

    function info_add(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Info Training','info');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);

		$images = $this->input->post('images');
		$table_field = $this->db->list_fields($this->tbl_training_info);
		$insert = array();
		foreach ($table_field as $field) {
			$insert[$field] = $this->input->post($field);
		}
		$insert['date_start'] = date('Y-m-d H:i:s',strtotime($insert['date_start']));
		$insert['date_end'] = date('Y-m-d H:i:s',strtotime($insert['date_end']));
		$insert['pendaftaran_start'] = date('Y-m-d H:i:s',strtotime($insert['pendaftaran_start']));
		$insert['pendaftaran_end'] = date('Y-m-d H:i:s',strtotime($insert['pendaftaran_end']));
		$insert['date_created'] = date('Y-m-d H:i:s',now());
		$insert['date_modified'] = date('Y-m-d H:i:s',now());
		$insert['id_created'] = $this->session_admin['admin_id'];
		$insert['id_modified'] = $this->session_admin['admin_id'];
		
		if($insert['title'] && $insert['content']){
			$do_insert = $this->model_basic->insert_all($this->tbl_training_info,$insert);
			if($do_insert){
				if($images){
					if(!is_dir(FCPATH . "assets/uploads/info_training/".$do_insert->id))
						mkdir(FCPATH . "assets/uploads/info_training/".$do_insert->id);
					$content = $insert['content']; 
					foreach($images as $im) {
						if(strpos($content,$im) !== false)
						{
							$new_im = 'assets/uploads/info_training/'.$do_insert->id.'/'.basename($im);
							@copy($im,$new_im);
						    $content = str_replace($im, $new_im, $content);
						}
					}
					$update['content'] = $content;
					$do_update = $this->model_basic->update($this->tbl_training_info,$update,'id',$do_insert->id);
					$this->delete_temp('temp_folder');
				}
                                $this->save_log_admin(ACT_CREATE, 'Insert New Info Training '.$insert['title']);
				$this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => $data['controller'].'/'.$data['function']));
			}
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function info_edit(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Info Training','info');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_UPDATE);
		
		$images = $this->input->post('images');
		$table_field = $this->db->list_fields($this->tbl_training_info);
		$update = array();
		foreach ($table_field as $field) {
			$update[$field] = $this->input->post($field);
		}
		unset($update['date_created']);
		unset($update['id_created']);
		$update['date_start'] = date('Y-m-d H:i:s',strtotime($update['date_start']));
		$update['date_end'] = date('Y-m-d H:i:s',strtotime($update['date_end']));
		$update['pendaftaran_start'] = date('Y-m-d H:i:s',strtotime($update['pendaftaran_start']));
		$update['pendaftaran_end'] = date('Y-m-d H:i:s',strtotime($update['pendaftaran_end']));
		$update['date_modified'] = date('Y-m-d H:i:s',now());
		$update['id_modified'] = $this->session_admin['admin_id'];
		if($images){
			if(!is_dir(FCPATH . "assets/uploads/info_training/".$update['id']))
				mkdir(FCPATH . "assets/uploads/info_training/".$update['id']);
			$content = $update['content']; 
			foreach($images as $im) {
				if(strpos($content,$im) !== false)
				{
					$new_im = 'assets/uploads/info_training/'.$update['id'].'/'.basename($im);
					if($im != $new_im)
						@copy($im,$new_im);
				    $content = str_replace($im, $new_im, $content);
				}
				else
					@unlink($im);
			}
			$update['content'] = $content;
			$this->delete_temp('temp_folder');
		}
		if($update['title'] && $update['content']){
			$do_update = $this->model_basic->update($this->tbl_training_info,$update,'id',$update['id']);
			if($do_update)
                        {
                                $this->save_log_admin(ACT_UPDATE, 'Update Info Training '.$update['title']);
				$this->returnJson(array('status' => 'ok','msg' => 'Update success','redirect' => $data['controller'].'/'.$data['function']));
                        }
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when updating data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function info_delete(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Info Training','info');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_DELETE);
		$id = $this->input->post('id');
		$do_delete = $this->model_basic->delete($this->tbl_training_info,'id',$id);
		if($do_delete){
                        $this->save_log_admin(ACT_DELETE, 'Delete Info Training');
			$this->delete_folder('info_training/'.$id);
			$this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => $data['controller'].'/'.$data['function']));
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }

    public function ajax_info_list()
	{
                $admin_usergroup = $this->session_admin['id_usergroup'];
                $admin_id = $this->session_admin['admin_id'];
		$list = $this->model_info_training->get_datatables($admin_usergroup, $admin_id);

		$data = array();
		$no = $_POST['start'];
		foreach ($list as $data_row) {
			//render data
			$no++;

			//filter tanggal pendaftaran
			$today = new DateTime();
                        $start = new DateTime($data_row->pendaftaran_start);
                        $end = new DateTime($data_row->pendaftaran_end);
			if ($today >= $start && $today <= $end) {
				$btn_daftar = '<a href="admin_info_training/pendaftaran_training_form/'.$data_row->id.'"><button class="btn btn-success btn-xs btn-edit" type="button" data-original-title="Daftar" data-placement="top" data-toggle="tooltip"><i class="fa fa-edit"></i></button></a>';
			}else{
				$btn_daftar = '';
			}
			
			
			$row = array();
			$row[] = $no;
			$row[] = $data_row->title;
			$row[] = $data_row->realname;
			$row[] = date('d F Y',strtotime($data_row->date_start));
			$row[] = date('d F Y',strtotime($data_row->pendaftaran_start));
			//add html for action
			$row[] = '<div class="text-center">

						<form action="admin_info_training/info_form" method="post">
						<input type="hidden" name="id" value="'.$data_row->id.'">
						'.$btn_daftar.'
                                                <a href="admin_info_training/list_pendaftar/'.$data_row->id.'" class="btn btn-xs btn-primary" data-original-title="List Pendaftar Training" data-placement="top" data-toggle="tooltip"><i class="fa fa-list"></i></a>
						<button class="btn btn-info btn-xs btn-edit" type="submit" data-original-title="Update" data-placement="top" data-toggle="tooltip"><i class="fa fa-edit"></i></button>
						<button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Delete" data-placement="top" data-toggle="tooltip" data-target-id="'.$data_row->id.'"><i class="fa fa-trash-o"></i></button>
						</form>
						</div>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_info_training->count_all($admin_usergroup, $admin_id),
						"recordsFiltered" => $this->model_info_training->count_filtered($admin_usergroup, $admin_id),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	function get_info_training(){
		$data = $this->model_basic->select_all($this->tbl_training_info);
		$data_info_training = array();
		foreach ($data as $d) {
			$pendaftaran = array(
				'id' => $d->id,
				'title' => '(Pendaftaran) '.$d->title,
				'start' => date('Y-m-d',strtotime($d->pendaftaran_start)),
				'end' => date('Y-m-d',strtotime($d->pendaftaran_end)),
				'className' => 'blue'
				);
			$training = array(
				'id' => $d->id,
				'title' => '(Pelaksanaan Training) '.$d->title,
				'start' => date('Y-m-d',strtotime($d->date_start)),
				'end' => date('Y-m-d',strtotime($d->date_end)),
				'className' => 'green'
				);
			array_push($data_info_training,$pendaftaran);
			array_push($data_info_training,$training);
		}
		echo json_encode($data_info_training);
	}

	function check_calendar_event() {
		$id = $this->input->post('id');
		$training = $this->model_basic->select_where($this->tbl_training_info, 'id', $id)->row();

		$today = date('d-m-Y');
		if ($today >= date('d-m-Y',strtotime($training->pendaftaran_start)) && $today <= date('d-m-Y',strtotime($training->pendaftaran_end))) {
			//die(print_r($today.' > '.date('d-m-Y',strtotime($training->pendaftaran_start)).' & '.$today.' < '.date('d-m-Y',strtotime($training->pendaftaran_end)) ));
			$this->returnJson(array('status' => 'ok', 'redirect' => 'admin_info_training/pendaftaran_training_form/'.$id));
		}else{
			$this->returnJson(array('status' => 'false'));
		}
	}
    function detail_info_training($id){
    	if($id){
    		$data = $this->get_app_settings();
			$data += $this->controller_attr;
			$data += $this->get_function('Info Training','info');
			$data += $this->get_menu();
			$this->check_userakses($data['function_id'], ACT_READ);
			$data['info_training'] = $this->model_basic->select_where($this->tbl_training_info,'id',$id)->row();
			$data['content'] = $this->load->view('backend/admin_info_training/info_training_detail',$data,true);
			$this->load->view('backend/index',$data); 
    	}
    	else
    		redirect('user_info_training');
    }

	//form pendaftaran
	function pendaftaran_training(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Pendaftaran Training','info');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);
                if($this->session_admin['id_usergroup'] == 1)
                    $data['data'] = $this->model_basic->select_all($this->tbl_training_info);
                else
                    $data['data'] = $this->model_basic->select_where($this->tbl_training_info, 'id_created', $this->session_admin['admin_id'])->result();
		$data['content'] = $this->load->view('backend/admin_info_training/pendaftaran_training',$data,true);
		$this->load->view('backend/index',$data);
    }

    function pendaftaran_training_detail($id = 0){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Pendaftaran Training Detail','info');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);

		$pendaftaran = $this->model_basic->select_where($this->tbl_training_pendaftaran,'id',$id);
		if ($pendaftaran->num_rows() > 0) {
			$data['pendaftaran'] = $pendaftaran->row();
			$data['pendaftaran']->date_created = date('d-m-Y',strtotime($data['pendaftaran']->date_created));
			$info_training = $this->model_basic->select_where($this->tbl_training_info, 'id', $data['pendaftaran']->training_info_id);	
			if ($info_training->num_rows() > 0) {
				$data['info_training'] = $info_training->row();
				$data['info_training']->date_start = date('d-m-Y',strtotime($data['info_training']->date_start));
				$data['info_training']->date_end = date('d-m-Y',strtotime($data['info_training']->date_end));
			}
		}
		
		$data['content'] = $this->load->view('backend/admin_info_training/pendaftaran_training_detail',$data,true);
		$this->load->view('backend/index',$data); 
    }

    function pendaftaran_training_form($training_info_id){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Pendaftaran Training','info');
		$data += $this->get_menu();
		//$this->check_userakses($data['function_id'], ACT_CREATE);
		$id = $this->input->post('id');
		$data['training'] = $this->model_info_training->get_training_info_detail($training_info_id)->row();
		if($id){
			$data['data'] = $this->model_basic->select_where($this->tbl_training_pendaftaran,'id',$id)->row();
		}
		else
			$data['data'] = null;
		$data['content'] = $this->load->view('backend/admin_info_training/pendaftaran_training_form',$data,true);
		$this->load->view('backend/index',$data); 
    }

    function pendaftaran_training_add(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Pendaftaran Training','info');
		$data += $this->get_menu();
		//$this->check_userakses($data['function_id'], ACT_CREATE);

		//$images = $this->input->post('images');
		$table_field = $this->db->list_fields($this->tbl_training_pendaftaran);
		$insert = array();
		foreach ($table_field as $field) {
			$insert[$field] = $this->input->post($field);
		}

		$insert['id_created'] = $this->session_admin['admin_id'];
                $insert['created_type'] = 1;
		$insert['date_created'] = date('Y-m-d H:i:s',now());
		
		if($insert['name'] && $insert['email']){
			$do_insert = $this->model_basic->insert_all($this->tbl_training_pendaftaran,$insert);
			if($do_insert)
                        {
                            $training = $this->model_basic->select_where($this->tbl_training_info, 'id', $insert['training_info_id'])->row();
                                $this->save_log_admin(ACT_CREATE, 'Insert Pendaftaran Training '.$training->title);
                                $this->email_pendaftaran($do_insert->id);
				redirect('admin_info_training/info?status=ok');
				// $this->send_mail($do_insert->id);
				// $this->session->set_flashdata('success', '<div class="alert alert-success"><strong>Selamat, pendaftaran anda berhasil!</strong><span></span></div>');
			}
			else{
				redirect('admin_info_training/info?status=error');
				// $this->session->set_flashdata('failed', '<div class="alert alert-danger"><strong>Maaf, pendaftaran anda gagal!</strong><span></span></div>');
			}
		}
		else{
			redirect('admin_info_training/info?status=error');
			// $this->session->set_flashdata('failed', '<div class="alert alert-danger"><strong>Maaf, pendaftaran anda gagal!</strong><span></span></div>');
		}

		redirect('admin_info_training/info','refresh');
    }

    function pendaftaran_training_delete(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Pendaftaran Training','info');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_DELETE);
		$id = $this->input->post('id');
		$do_delete = $this->model_basic->delete($this->tbl_training_pendaftaran,'id',$id);
		if($do_delete){
                        $this->save_log_admin(ACT_DELETE, 'Delete Pendaftaran Training');
			$this->delete_folder('berita/'.$id);
			$this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => $data['controller'].'/'.$data['function']));
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }

    public function ajax_pendaftaran_training_list()
	{
		$list = $this->model_pendaftaran_training->get_datatables();
		//die(print_r($this->db->last_query()));
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $data_row) {
			//render data
			$no++;
			
			$row = array();
			$row[] = $no;
			$row[] = '<a href="admin_info_training/pendaftaran_training_detail/'.$data_row->id.'">'.$data_row->title.'</a>';
			$row[] = date('d F Y',strtotime($data_row->date_created));
			$row[] = date('d F Y',strtotime($data_row->date_start));
			//add html for action
			$row[] = '<div class="text-center">
						<form action="admin_info_training/pendaftaran_training_form" method="post">
						<input type="hidden" name="id" value="'.$data_row->id.'">
						<button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Delete" data-placement="top" data-toggle="tooltip" data-target-id="'.$data_row->id.'"><i class="fa fa-trash-o"></i></button>
						</form>
						</div>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_pendaftaran_training->count_all(),
						"recordsFiltered" => $this->model_pendaftaran_training->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function get_training_detail() {
		$results = $this->model_basic->select_where($this->tbl_training_info, 'id', $this->input->post('training_info_id'))->row();
		$results->pendaftaran_start = date('d F Y',strtotime($results->pendaftaran_start));
		$results->pendaftaran_end = date('d F Y',strtotime($results->pendaftaran_end));
		$results->date_start = date('d F Y',strtotime($results->date_start));
		$results->date_end = date('d F Y',strtotime($results->date_end));
		//die(print_r($this->db->last_query()));
		$this->returnJson(array('status' => 'ok', 'result' => $results));
	}

	public function get_training_detail_calendar() {
		$results = $this->model_basic->select_where($this->tbl_training_info, 'id', $this->input->post('training_info_id'))->row();
		$title = $results->title;
		$training_content = $results->content;
		$pendaftaran_start = date('d F Y',strtotime($results->pendaftaran_start));
		$pendaftaran_end = date('d F Y',strtotime($results->pendaftaran_end));
		$date_start = date('d F Y',strtotime($results->date_start));
		$date_end = date('d F Y',strtotime($results->date_end));
		$content = "<div class='row'>";
		$content .= "<div class='col-md-4'>Pendaftaran Start</div><div class='col-md-4'>:</div><div class='col-md-4'>".$pendaftaran_start."</div>";
		$content .= "</div>";
		$content .= "<div class='row'>";
		$content .= "<div class='col-md-4'>Pendaftaran End</div><div class='col-md-4'>:</div><div class='col-md-4'>".$pendaftaran_end."</div>";
		$content .= "</div>";
		$content .= "<div class='row'>";
		$content .= "<div class='col-md-4'>Training Start</div><div class='col-md-4'>:</div><div class='col-md-4'>".$date_start."</div>";
		$content .= "</div>";
		$content .= "<div class='row'>";
		$content .= "<div class='col-md-4'>Training End</div><div class='col-md-4'>:</div><div class='col-md-4'>".$date_end."</div>";
		$content .= "</div>";
		$content .= "<div class='row'>";
		$content .= "<div class='col-md-12'  style='padding:20px 0;'>".$training_content."</div>";
		$content .= "</div>";
		$this->returnJson(array('status' => 'ok', 'title' => $title, 'content' => $content));
	}

	public function send_mail($pendaftaran_training_id)
	{
		$this->load->library('email');

		$pendaftaran_training = $this->model_info_training->get_training_detail($pendaftaran_training_id);
		//die(print_r($this->db->last_query()));

		if($pendaftaran_training->num_rows() > 0)
		{
			$row = $pendaftaran_training->row();
			$id_pic = $row->id_pic;
			$data['training_tl'] = $training_tl = $row->title;
			$data['content'] = $row->content;
			$data['pendaftaran_start'] = $row->pendaftaran_start;
			$data['pendaftaran_end'] = $row->pendaftaran_end;
			$data['date_start'] = $row->date_start;
			$data['date_end'] = $row->date_end;
			$data['date_created'] = $row->date_created;
			$data['nama_from'] = $nama_from = $row->realname;
			$email_from = $row->email;
			$telp_from = $row->telp;
			$jabatan_from = $row->jabatan;
			$instalasi_nm_from = $row->instalasi_nm;

			// $pic_detail = $this->model_info_training->get_pic_detail($id_pic);
			// $row_pic = $pic_detail->row();
			// $data['nama_to'] = $nama_to = $row_pic->realname;
			// $data['jabatan'] = $row_pic->jabatan;
			// $data['instalasi'] = $row_pic->instalasi_nm;
			// $email_to = $row_pic->email;

			// $message = $this->load->view('backend/email/mail_pendaftaran_training.php',$data,TRUE);

			// $config = array (
			// 			'protocol' => 'smtp',
			// 			'smtp_host' => 'ssl://iix53.rumahweb.com',
			// 			'smtp_port' => 465,
			// 			'smtp_user' => 'training@asissec.com',
			// 			'smtp_pass' => '28161324', 
			// 			'mailtype' => 'html',
			// 			'charset' => 'iso-8859-1',
			// 			'wordwrap' => TRUE
	  //                  );
	  //       $this->email->initialize($config);
			// $this->email->from('training@asissec.com', 'ASIS');
			// $this->email->to($email_to);
			// $this->email->bcc('yudiripayansah@gmail.com');

			
			// $this->email->subject('Form Pendaftaran Training dari '.$nama_from);
			// $this->email->message($message);
			
			// $this->email->send();
			//echo $this->email->print_debugger();
		}else{
			die('debugger : '.$pendaftaran_training->num_rows());	
		}
		//echo $this->email->print_debugger();
	}
        
        function list_pendaftar($training_id)
        {
            $data = $this->get_app_settings();
            $data += $this->controller_attr;
            $data += $this->get_function('Peserta Training','info');
            $data += $this->get_menu();
            $this->check_userakses($data['function_id'], ACT_READ);
            $data_training = $this->model_basic->select_where($this->tbl_training_info, 'id', $training_id);
            if($data_training->num_rows() != 1)
                redirect('admin_info_training/info');
            else
                $data['data_training'] = $data_training->row();
            $data['list_pendaftar'] = $this->model_basic->select_where_order($this->tbl_training_pendaftaran, 'training_info_id', $training_id, 'date_created', 'DESC')->result();
            $data['content'] = $this->load->view('backend/admin_info_training/list_pendaftar',$data,true);
            $this->load->view('backend/index',$data); 
        }
        
        function proses_pendaftar($pendaftar_id)
        {
            $data = $this->get_app_settings();
            $data += $this->controller_attr;
            $data += $this->get_function('Peserta Training','info');
            $data += $this->get_menu();
            $this->check_userakses($data['function_id'], ACT_READ);
            $data_pendaftar = $this->model_basic->select_where($this->tbl_training_pendaftaran, 'id', $pendaftar_id);
            if($data_pendaftar->num_rows() != 1)
                redirect('admin_info_training/info');
            else
            {
                $data['data_pendaftar'] = $data_pendaftar->row();
                $training = $this->model_basic->select_where($this->tbl_training_info, 'id', $data_pendaftar->row()->training_info_id);
                if($training->num_rows() == 1)
                {
                    $data['data_pendaftar']->data_training = $training->row();
                    $data['data_pendaftar']->data_training->date_start = date('d M Y H:i:s', strtotime($data['data_pendaftar']->data_training->date_start));
                    $data['data_pendaftar']->data_training->date_end = date('d M Y H:i:s', strtotime($data['data_pendaftar']->data_training->date_end));
                    $data['data_pendaftar']->data_training->pendaftaran_start = date('d M Y H:i:s', strtotime($data['data_pendaftar']->data_training->pendaftaran_start));
                    $data['data_pendaftar']->data_training->pendaftaran_end = date('d M Y H:i:s', strtotime($data['data_pendaftar']->data_training->pendaftaran_end));
                    $pic = $this->model_basic->select_where($this->tbl_admin, 'id', $data['data_pendaftar']->data_training->id_pic);
                    if($pic->num_rows() == 1)
                        $data['data_pendaftar']->data_training->pic = $pic->row()->realname;
                    else
                        $data['data_pendaftar']->data_training->pic = 'Unknown';
                }
                else
                    redirect('admin_info_training/info');
            }
            $data['content'] = $this->load->view('backend/admin_info_training/list_pendaftar_proses',$data,true);
            $this->load->view('backend/index',$data);
        }
        
        function do_process_pendaftar()
        {
            $data = $this->get_app_settings();
            $data += $this->controller_attr;
            $data += $this->get_function('Info Training','info');
            $data += $this->get_menu();
            $this->check_userakses($data['function_id'], ACT_UPDATE);
            $id = $this->input->post('id');
            $status = $this->input->post('status');
            if($status == 1)
            {
                $data_update = array(
                    'name' => $this->input->post('name'),
                    'company' => $this->input->post('company'),
                    'position' => $this->input->post('position'),
                    'phone' => $this->input->post('phone'),
                    'email' => $this->input->post('email'),
                    'supervisor_name' => $this->input->post('supervisor_name'),
                    'status' => $status
                );
            }
            else
            {
                $data_update = array(
                    'status' => $status
                );
            }
            
            if(!$this->model_basic->update($this->tbl_training_pendaftaran, $data_update, 'id', $id))
            {
                $this->returnJson(array('status' => 'failed'));
            }
            else
            {
                $pendaftar = $this->model_basic->select_where($this->tbl_training_pendaftaran, 'id', $id)->row();
                $training = $this->model_basic->select_where($this->tbl_training_info, 'id', $pendaftar->training_info_id)->row();
                $this->save_log_admin(ACT_UPDATE, 'Update Pendaftar Training '.$training->title);
                $this->returnJson(array('status' => 'ok', 'redirect' => $data['controller'].'/list_pendaftar/'.$pendaftar->training_info_id));
            }
        }

}

/* End of file admin_info_training.php */
/* Location: ./application/controllers/admin_info_training.php */