<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends PX_Controller {

	protected $tokenkey = null;
    protected $user_id = null;

    public function __construct() {
        parent::__construct();
        $headers = $this->input->request_headers();
        if (isset($headers['Authorization'])) $this->tokenkey = $this->check_token($headers['Authorization']);
        if (!$this->tokenkey) show_403_api('Please provide correct token!');
        $token = $this->get_token_wso2_by_tokenkey($this->tokenkey);
        if (!$token) show_401_api('Invalid token!');
        if (OAUTH_TYPE!='mobile') if ($token->expires_in < time()) show_401_api('Token has expired!');
        $this->user_id = $token->user_id;
    }

    // Detail Profile User
    public function detail() {
        $data = null;
        $user_id = $this->user_id;
        // Get user by user id
        $user = $this->model_basic->select_where($this->tbl_user, 'id', $user_id)->row();
        if (!$user)
            return $this->res(['message' => 'User not found'])->error();
        $data = [];
        $data['email'] = $user->email;
        $data['username'] = $user->username;
        $data['realname'] = $user->realname;
        $photo_inet = 'http://www.fancyhands.com/images/default-avatar-250x250.png';
        $photo_profile = base_url().'assets/uploads/user/'.$user->id.'/'.$user->photo;
        $data['photo'] = file_exists($photo_profile)?$photo_profile:$photo_inet;
        return $this->res($data)->success();
    }

}
