<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Issue extends PX_Controller {

	protected $tokenkey = null;
    protected $user_id = null;

    public function __construct() {
        parent::__construct();
        $headers = $this->input->request_headers();
        if (isset($headers['Authorization'])) $this->tokenkey = $this->check_token($headers['Authorization']);
        if (!$this->tokenkey) show_403_api('Please provide correct token!');
        $token = $this->get_token_wso2_by_tokenkey($this->tokenkey);
        if (!$token) show_401_api('Invalid token!');
        if (OAUTH_TYPE!='mobile') if ($token->expires_in < time()) show_401_api('Token has expired!');
        $this->user_id = $token->user_id;
    }

    // GET all Report Type
    public function report_type() {
        $data = null;
        $user_id = $this->user_id;

        // Get Issue report type
        $issue_report_type = $this->model_basic->select_all($this->tbl_issue_report_type);
        if (count($issue_report_type))
            $data = [];

        foreach ($issue_report_type as $k => $v) {
            $data[$k]['id'] = $v->id;
            $data[$k]['name'] = $v->name;
        }

        return $this->res($data)->success();
    }

    // GET all location
    public function location() {
        $data = null;
        $user_id = $this->user_id;

        // Get instalasi
        $instalasi = $this->model_basic->select_all($this->tbl_instalasi);
        if (count($instalasi))
            $data = [];

        foreach ($instalasi as $k => $v) {
            $data[$k]['id'] = $v->id;
            $data[$k]['name'] = $v->name;
        }

        return $this->res($data)->success();
    }

    // GET building by location
    public function building() {
        $instalasi_id = $this->input->get('instalasi');

        $data = null;
        $user_id = $this->user_id;

        // Check instalasi id
        if (!$instalasi_id)
            return $this->res(['message' => 'Instalasi is required'])->error();
        
        // Get building
        $building = $this->model_basic->select_where($this->tbl_building, 'instalasi_id', $instalasi_id)->result();
        if (count($building))
            $data = [];

        foreach ($building as $k => $v) {
            $data[$k]['id'] = $v->id;
            $data[$k]['name'] = $v->building_name;
        }

        return $this->res($data)->success();
    }

    // GET building detail by building
    public function building_detail() {
        $building_id = $this->input->get('building');

        $data = null;
        $user_id = $this->user_id;

        // Check building id
        if (!$building_id)
            return $this->res(['message' => 'Building is required'])->error();
        
        // Get building detail
        $building_detail = $this->model_basic->select_where($this->tbl_detail_building, 'building_id', $building_id)->result();
        if (count($building_detail))
            $data = [];

        foreach ($building_detail as $k => $v) {
            $data[$k]['id'] = $v->id;
            $data[$k]['floor'] = $v->floor;
        }

        return $this->res($data)->success();
    }

    // SUBMIT issue
    public function submit() {
        $post = $this->populate_post();
        if (!$post) return $this->res(['message' => 'Please complete form'])->error();

        $this->form_validation->set_rules('report_type_id', 'report_type_id', 'required');
        $this->form_validation->set_rules('building_id', 'building_id', 'required');
        $this->form_validation->set_rules('building_detail_id', 'building_detail_id', 'required');
        $this->form_validation->set_rules('area', 'area', 'required');
        // $this->form_validation->set_rules('description', 'description', 'required');
        $this->form_validation->set_rules('fact', 'fact', 'required');
        $this->form_validation->set_rules('analysis', 'analysis', 'required');
        $this->form_validation->set_rules('summary', 'summary', 'required');

        if (!$this->form_validation->run())
            return $this->res(['message' => $this->validation_msg(validation_errors())])->error();

        $issue_report_type_id = $post['report_type_id'];
        $building_id = $post['building_id'];
        $building_detail_id = $post['building_detail_id'];
        $area = $post['area'];
        // $description = $post['description'];
        $fact = $post['fact'];
        $analysis = $post['analysis'];
        $summary = $post['summary'];
        $floor = null;

        $data = null;
        $user_id = $this->user_id;

        if (count($_FILES)==0)
            return $this->res(['message' => 'The image field is required'])->error();

        // Cek building
        $building = $this->model_basic->select_where($this->tbl_building, 'id', $building_id)->row();
        if (!$building)
            return $this->res(['message' => 'Building not found'])->error();

        // Cek building detail
        $building_detail = $this->model_basic->select_where($this->tbl_detail_building, 'id', $building_detail_id)->row();
        if (!$building_detail)
            return $this->res(['message' => 'Building detail not found'])->error();

        $floor = $building_detail->floor;

        try {
            // DB Transaction Start
            $this->db->trans_start();

            // Get count issue current
            $issue = $this->model_basic->get_count_no_flag($this->tbl_issue);
            if (!$issue) $issue = 0;
            // Generate number 6 digit sort by total current issue
            $g_num = sprintf('%06d', ((int)$issue+1));
            // Compile ticket number
            $ticket_number = date('Ymd').$g_num;
            
            // Insert to issue
            $insert = [];
            $insert['ticket_number'] = $ticket_number;
            $insert['issue_report_type_id'] = $issue_report_type_id;
            $insert['building_id'] = $building_id;
            $insert['floor'] = $floor;
            $insert['area'] = $area;
            $insert['description'] = null;
            $insert['note_admin'] = null;
            $insert['fact'] = $fact;
            $insert['analysis'] = $analysis;
            $insert['summary'] = $summary;
            // $insert['action'] = 'dispatch';
            $insert['created_by'] = $user_id;
            $insert['created_date'] = date('Y-m-d H:i:s');
            $insert['delete_flag'] = 0;
            $issue_insert = $this->model_basic->insert_all($this->tbl_issue,$insert);
            if (!$issue_insert)
                return $this->res(['message' => 'Failed insert issue'])->error();

            // Insert issue attachment
            $files = $_FILES;
            $cFiles = count($_FILES['images']['name']);
            for($i=0; $i<$cFiles; $i++) {
                $_FILES['image']['name'] = $files['images']['name'][$i];
                $_FILES['image']['type'] = $files['images']['type'][$i];
                $_FILES['image']['tmp_name'] = $files['images']['tmp_name'][$i];
                $_FILES['image']['error'] = $files['images']['error'][$i];
                $_FILES['image']['size'] = $files['images']['size'][$i];
                // Upload single photo
                $name = date('YmdHis').'_issue_attachment_'.$i.'_user_'.$user_id;
                $path = 'assets/uploads/esams/issue';
                $uploads = $this->do_upload_image($name, $path, 'image');
                if (empty($uploads['file_name']))
                    return $this->res(['message' => strip_tags($uploads)])->error();

                // Insert issue attachment
                $insert = [];
                $insert['issue_id'] = $issue_insert->id;
                $insert['image_name'] = $uploads['file_name'];
                $insert['created_by'] = $user_id;
                $insert['created_date'] = date('Y-m-d H:i:s');
                $insert['delete_flag'] = 0;
                $issue_attachment_insert = $this->model_basic->insert_all($this->tbl_issue_attachment,$insert);
                if (!$issue_attachment_insert)
                    return $this->res(['message' => 'Failed insert issue attachment'])->error();
            }
            // DB Transaction Commit
            $this->db->trans_complete();

            return $this->res('Issue has been submited')->success();
        } catch (Exception $e) {
            // DB Transaction Rollback
            $this->db->trans_rollback();
            return $this->res(['message' => 'Invalid Proccess'])->error();
        }
    }

}
