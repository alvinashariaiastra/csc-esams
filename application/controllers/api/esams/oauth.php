<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Oauth extends PX_Controller {

    protected $tokenkey = null;

    public function __construct() {
        parent::__construct();
        if (PHP_VERSION >= 7) {
            ini_set('display_errors', 0);
            error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);
        }
        $headers = $this->input->request_headers();
        if (isset($headers['Authorization']))
           $this->tokenkey = $this->check_token($headers['Authorization']);
    }

    // Login User
    public function login() {
        $post = $this->populate_post();

        if (OAUTH_TYPE=='wso2' or OAUTH_TYPE=='local') {
            $this->form_validation->set_rules('username', 'username', 'required');
            $this->form_validation->set_rules('password', 'password', 'required');
        } else 
        if (OAUTH_TYPE=='wso2' or OAUTH_TYPE=='mobile') {
            $this->form_validation->set_rules('imei', 'imei', 'required');
        }

        if (!$this->form_validation->run())
            return $this->res(['message' => $this->validation_msg(validation_errors())])->error();

        $username = @$post['username'];
        $password = @$post['password'];
        $imei = @$post['imei'];
        $tokenkey = null;
        $response_wso2 = array();

        // Get token from encrypt
        if (OAUTH_TYPE=='wso2') {
            $auth = [];
            if (self::wso2_env=='development') {
                $auth['header']['Authorization'] = 'Basic b1JBRDJJbkpoWW5yM2JPZTJpQ3dpYVh6VTk4YTpLUE9EU0l2VW92UXV1S3VTMkxxWXlYZk1uNk1h';
                $auth['username'] = 'oRAD2InJhYnr3bOe2iCwiaXzU98a';
                $auth['password'] = 'KPODSIvUovQuuKuS2LqYyXfMn6Ma';
            } else
            if (self::wso2_env=='production') {
                $auth['header']['Authorization'] = 'Basic TkNwWjVBejVZc1h5TWRkNlVXUGwxSldqclVVYTpPejdvTG0wN0lTX2FlYTl2YlR2N2h2ZDZOeW9h';
                $auth['username'] = 'NCpZ5Az5YsXyMdd6UWPl1JWjrUUa';
                $auth['password'] = 'Oz7oLm07IS_aea9vbTv7hvd6Nyoa';
            }

            $body = [];
            $body['grant_type'] = 'password';
            $body['username'] = $username;
            $body['password'] = $password;
            $body['scope'] = 'openid';

            $request = httprequest::post(self::wso2_url.'/token', $auth, $body, 1);
            $response = json_decode($request);
            if (!$response)
                return $this->res(['message' => $request])->error();

            if (!isset($response->access_token))
                return $this->res(['message' => $response->error_description])->error();

            $tokenkey = $response->access_token;
            $response->expires_in = time()+$response->expires_in;
            $response_wso2 = $response;
        } else
        if (OAUTH_TYPE=='local') {
            // Get user by email
            $where = [];
            $where['email'] = $username;
            $where['level_user_id'] = 4;
            $user = $this->model_basic->select_where_array($this->tbl_user, $where)->row();
            if (!$user)
                return $this->res(['message' => 'Personel not found'])->error();
            if ($this->encrypt->decode($user->password) != $password)
                return $this->res(['message' => 'Personel not found'])->error();

            $tokenkey = $this->encrypt->encode($username.date('YmdHis'));
            $response_wso2['access_token'] = $tokenkey;
            $response_wso2['expires_in'] = time()+self::wso2_expired_time;
        } else
        if (OAUTH_TYPE=='mobile') {
            // Get user info
            $auth = [];
            $auth['header']['Authorization'] = 'Bearer '.$this->tokenkey;
            $request = httprequest::get(self::wso2_url.'/userinfo?scope=openid', $auth, 1);
            $response = json_decode($request);
            if (!$response)
                return $this->res(['message' => $request])->error();

            if (!isset($response->sub))
                return $this->res(['message' => $response->error_description])->error();

            // $username = $response->email;
            $username = $response->email_address;

            // Get user by email
            $where = [];
            $where['email'] = $username;
            $where['level_user_id'] = 4;
            $user = $this->model_basic->select_where_array($this->tbl_user, $where)->row();
            if (!$user)
                return $this->res(['message' => 'Personel not found'])->error();

            $tokenkey = $this->tokenkey;
            // Not Used
            // $token = $this->get_token_wso2_by_tokenkey($this->tokenkey, true);
            // if ($token and $token->email==$username)
            //      return $this->res(['message' => 'Personel has login'])->error();
        }

        // Get token key current // not used
        /* $_token = $this->model_basic->select_where($this->tbl_esams_log_wso2_token, 'user_id', $user->id)->row();
        if ($_token) {
            // return $this->res(['message' => 'user has been login'])->error();
            $do_delete = $this->model_basic->delete($this->tbl_esams_log_wso2_token, 'id', $_token->id);
            if (!$do_delete)
                return $this->res(['message' => 'Something error database'])->error();
        } */

        try {
            // DB Transaction Start
            $this->db->trans_start();

            // Insert Data token
            $insert = [];
            if (OAUTH_TYPE=='wso2') $insert['email'] = $username;
            if (OAUTH_TYPE=='local') $insert['user_id'] = !empty($user)?$user->id:null;
            if (OAUTH_TYPE=='wso2' or OAUTH_TYPE=='local') {
                $insert['response_wso2_request'] = json_encode($response_wso2);
                $response_wso2 = (array) $response_wso2;
                $insert['access_token'] = $response_wso2['access_token'];
                $insert['expires_in'] = $response_wso2['expires_in'];
            } else
            if (OAUTH_TYPE=='mobile') {
                $insert['email'] = $username;
                $insert['imei'] = $imei;
            }
            $insert['delete_flag'] = false;
            $do_insert = $this->model_basic->insert_all($this->tbl_esams_log_wso2_token, $insert);
            if (!$do_insert)
                return $this->res(['message' => 'Failed when saving data'])->error();

            // DB Transaction Commit
            $this->db->trans_complete();

            $data = [];
            $data['tokenkey'] = $tokenkey;
            return $this->res($data)->success();  
        } catch (Exception $e) {
            // DB Transaction Rollback
            $this->db->trans_rollback();
            return $this->res(['message' => 'Invalid Proccess'])->error();
        }
    }

    // Logout User
    public function logout() {
        if (!$this->tokenkey) show_403_api('Please provide correct token!');
        $token = $this->get_token_wso2_by_tokenkey($this->tokenkey);
        if (!$token) show_403_api('Token not found!');
        
        // Delete token
        $do_delete = $this->model_basic->delete($this->tbl_esams_log_wso2_token, 'id', $token->id);
        if (!$do_delete)
            return $this->res(['message' => 'Delete Failed'])->error();

        $this->tokenkey = null;

        return $this->res(['tokenkey' => ''])->success();     
    }

    // Get User Info Detail
    public function userinfo() {
        if (OAUTH_TYPE=='mobile')
            return $this->res(['message' => 'Invalid Request'])->error();

        if (!$this->tokenkey) show_403_api('Please provide correct token!');
        $token = $this->get_token_wso2_by_tokenkey($this->tokenkey);
        if (!$token) show_403_api('Token not found!');
        if ($token->expires_in < time()) return $this->res(['message' => 'Token has expired!'])->error();

        $data = null;

        if (OAUTH_TYPE=='wso2') {
            $response_wso2_request = json_decode($token->response_wso2_request);

            $auth = [];
            $auth['header']['Authorization'] = 'Bearer '.$response_wso2_request->access_token;

            $request = httprequest::get(self::wso2_url.'/userinfo?scope=openid', $auth, 1);
            $response = json_decode($request);
            if (!$response)
                return $this->res(['message' => $request])->error();

            if (!isset($response->sub))
                return $this->res(['message' => $response->error_description])->error();
            
            $data = $response;
        } else
        if (OAUTH_TYPE=='local') {
            // Get user by user id
            $user = $this->model_basic->select_where($this->tbl_user, 'id', $token->user_id)->row();
            if (!$user)
                return $this->res(['message' => 'Personel not found'])->error();

            $data = [];
            $data['email'] = $user->email;
        }
        
        return $this->res($data)->success();
    }

    // Refresh Token User
    public function refresh() {
        if (OAUTH_TYPE=='mobile')
            return $this->res(['message' => 'Invalid Request'])->error();

        if (!$this->tokenkey) show_403_api('Please provide correct token!');
        $token = $this->get_token_wso2_by_tokenkey($this->tokenkey);
        if (!$token) show_403_api('Token not found!');
        // if ($token->expires_in >= time()) return $this->res(['message' => 'Token still active!'])->error();
        
        $data = null;
        $response = null;
        $tokenkey = $this->tokenkey;
        $expires_in = $token->expires_in;

        if (OAUTH_TYPE=='wso2') {
            $response_wso2_request = json_decode($token->response_wso2_request);

            $auth = [];
            if (self::wso2_env=='development') {
                $auth['header']['Authorization'] = 'Basic b1JBRDJJbkpoWW5yM2JPZTJpQ3dpYVh6VTk4YTpLUE9EU0l2VW92UXV1S3VTMkxxWXlYZk1uNk1h';
                $auth['username'] = 'oRAD2InJhYnr3bOe2iCwiaXzU98a';
                $auth['password'] = 'KPODSIvUovQuuKuS2LqYyXfMn6Ma';
            } else
            if (self::wso2_env=='production') {
                $auth['header']['Authorization'] = 'Basic TkNwWjVBejVZc1h5TWRkNlVXUGwxSldqclVVYTpPejdvTG0wN0lTX2FlYTl2YlR2N2h2ZDZOeW9h';
                $auth['username'] = 'NCpZ5Az5YsXyMdd6UWPl1JWjrUUa';
                $auth['password'] = 'Oz7oLm07IS_aea9vbTv7hvd6Nyoa';
            }

            $body = [];
            $body['grant_type'] = 'refresh_token';
            $body['refresh_token'] = $response_wso2_request->refresh_token;

            $request = httprequest::post(self::wso2_url.'/token', $auth, $body, 1);
            $response = json_decode($request);
            if (!$response)
                return $this->res(['message' => $request])->error();

            if (!isset($response->access_token))
                return $this->res(['message' => $response->error_description])->error();

            $tokenkey = $response->access_token;
            $response->expires_in = time()+$response->expires_in;
            $response_wso2 = $response;

            $expires_in = time()+self::wso2_expired_time; // debug 5 minutes
            // $expires_in = $response->expires_in; // original 3600 seconds
            // $this->tokenkey = $tokenkey; // not used
        } else
        if (OAUTH_TYPE=='local')
            $expires_in = time()+self::wso2_expired_time;

        try {
            // DB Transaction Start
            $this->db->trans_start();

            // Update token expire in
            $update = [];
            $update['access_token'] = $tokenkey;
            $update['expires_in'] = $expires_in;
            $update['response_wso2_request'] = json_encode($response);
            $do_update = $this->model_basic->update($this->tbl_esams_log_wso2_token,$update,'id',$token->id);
            if (!$do_update)
                return $this->res(['message' => 'Failed update token'])->error();

            // DB Transaction Commit
            $this->db->trans_complete();

            $data = [];
            $data['tokenkey'] = $tokenkey;
            return $this->res($data)->success(); 
        } catch (Exception $e) {
            // DB Transaction Rollback
            $this->db->trans_rollback();
            return $this->res(['message' => 'Invalid Proccess'])->error();
        }
    }

    // Check Token User
    public function check() {
        if (OAUTH_TYPE=='mobile')
            return $this->res(['message' => 'Invalid Request'])->error();

        if (!$this->tokenkey) show_403_api('Please provide correct token!');
        $token = $this->get_token_wso2_by_tokenkey($this->tokenkey);
        if (!$token) show_403_api('Token not found!');
        
        $data = null;
        $response = null;
        $active = true;
        $expires_in = $token->expires_in;

        if (OAUTH_TYPE=='wso2') {
            $response_wso2_request = json_decode($token->response_wso2_request);

            $auth = [];
            $auth['header']['Authorization'] = 'Basic U3lzRGlnaXRhbFBhdHJvbDpVc2VyU3lzdGVtQDEyMw==';
            $auth['username'] = 'SysDigitalPatrol';
            $auth['password'] = 'UserSystem@123';

            $body = [];
            $body['token'] = $response_wso2_request->access_token;

            $request = httprequest::post(self::wso2_url.'/introspect', $auth, $body, 1);
            $response = json_decode($request);
            if (!$response)
                return $this->res(['message' => $request])->error();

            if (!isset($response->username))
                return $this->res(['message' => 'Request failed'])->error();

            $active = $response->active;
        } else
        if (OAUTH_TYPE=='local') {
            if ($token->expires_in < time())
                $active = false;
        }

        try {
            // DB Transaction Start
            $this->db->trans_start();

            // Update token
            $update = [];
            $update['response_wso2_introspect'] = json_encode($response);
            $do_update = $this->model_basic->update($this->tbl_esams_log_wso2_token,$update,'id',$token->id);
            if (!$do_update)
                return $this->res(['message' => 'Failed update token'])->error();

            // DB Transaction Commit
            $this->db->trans_complete();

            $data = [];
            $data['active'] = $active;
            return $this->res($data)->success(); 
        } catch (Exception $e) {
            // DB Transaction Rollback
            $this->db->trans_rollback();
            return $this->res(['message' => 'Invalid Proccess'])->error();
        }
    }

}
