<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dps extends PX_Controller {

	protected $tokenkey = null;
    protected $user_id = null;

    public function __construct() {
        parent::__construct();
        $headers = $this->input->request_headers();
        if (isset($headers['Authorization'])) $this->tokenkey = $this->check_token($headers['Authorization']);
        if (!$this->tokenkey) show_403_api('Please provide correct token!');
        $token = $this->get_token_wso2_by_tokenkey($this->tokenkey);
        if (!$token) show_401_api('Invalid token!');
        if (OAUTH_TYPE!='mobile') if ($token->expires_in < time()) show_401_api('Token has expired!');
        $this->user_id = $token->user_id;
    }

    // Home DPS
    public function discover() {
        $data = null;
        $user_id = $this->user_id;
        // Get DPS by User
        $sql = '
            SELECT 
                a.id as id,
                d.name as instalasi_name,
                c.building_name as building_name,
                DATE_FORMAT(a.date, "%M %d, %Y") as date,
                e.shift_name as shift_name,
                DATE_FORMAT(e.start_time, "%H:%i") as shift_start_time,
                DATE_FORMAT(e.end_time, "%H:%i") as shift_end_time,
                b.user_id as user_id,
                count(f.id) as total_pattern,
                count(g.id) as total_report
            FROM 
                '.$this->tbl_patrol.' a
                JOIN '.$this->tbl_patrol_user.' b ON b.patrol_id = a.id
                JOIN (
                    '.$this->tbl_building.' c
                    JOIN '.$this->tbl_instalasi.' d ON d.id = c.instalasi_id
                ) ON c.id = a.building_id
                JOIN '.$this->tbl_shift.' e ON e.id = a.shift_id
                JOIN (
                    '.$this->tbl_patrol_pattern.' f
                    LEFT JOIN '.$this->tbl_patrol_report.' g ON g.patrol_pattern_id = f.id AND g.user_id = '.$user_id.' and g.delete_flag = 0
                ) ON f.patrol_id = a.id and f.delete_flag = 0
            WHERE 
                1=1
                AND a.delete_flag = 0
                AND b.delete_flag = 0
                AND a.date = "'.date('Y-m-d').'"
                AND b.user_id = '.$user_id.'
            GROUP BY a.id
            HAVING total_pattern > total_report
            ORDER BY a.id asc
            LIMIT 1
        ';
        $patrol = $this->model_basic->select_row_by_query($sql);
        if (!$patrol)
            return $this->res($data)->success();
        if ($patrol->total_pattern==$patrol->total_report)
            return $this->res(['message'=>'Patrol Complete'])->error();

        $data = [];
        $data['location'] = $patrol->instalasi_name;
        $data['building'] = $patrol->building_name;
        $data['date'] = $patrol->date;
        $data['shift']['name'] = $patrol->shift_name;
        $data['shift']['start_time'] = $patrol->shift_start_time;
        $data['shift']['end_time'] = $patrol->shift_end_time;
        // Get Patrol User by patrol id
        $patrol_user = $this->model_basic->select_where($this->tbl_patrol_user, 'patrol_id', $patrol->id)->result();
        foreach ($patrol_user as $k => $v) {
            // Get User by user id
            $user = $this->model_basic->select_where($this->tbl_user, 'id', $v->user_id)->row();
            if (!$user) continue;
            $data['personel'][$k]['name'] = $user->realname;
            $photo_inet = 'http://www.fancyhands.com/images/default-avatar-250x250.png';
            $photo_profile = base_url().'assets/uploads/user/'.$user->id.'/'.$user->photo;
            $data['personel'][$k]['image'] = file_exists($photo_profile)?$photo_profile:$photo_inet;
        }

        return $this->res($data)->success();
    }

    // Status complete and in progress
    public function status_patrol() {
        $data = null;
        $user_id = $this->user_id;
        // Get Patrol
        /*$sql = '
            SELECT 
                a.id as id,
                d.name as instalasi_name,
                c.building_name as building_name,
                DATE_FORMAT(a.date, "%M %d, %Y") as date,
                b.user_id as user_id
            FROM 
                '.$this->tbl_patrol.' a
                JOIN '.$this->tbl_patrol_user.' b ON b.patrol_id = a.id
                JOIN (
                    '.$this->tbl_building.' c
                    JOIN '.$this->tbl_instalasi.' d ON d.id = c.instalasi_id
                ) ON c.id = a.building_id
            WHERE 
                1=1
                AND a.delete_flag = 0
                AND b.delete_flag = 0
                AND a.date = "'.date('Y-m-d').'"
                AND b.user_id = '.$user_id.'
        ';*/
        $sql = '
            SELECT z.* FROM (
                SELECT
                    a.id as id,
                    d.name as instalasi_name,
                    c.building_name as building_name,
                    DATE_FORMAT(a.date, "%M %d, %Y") as date,
                    b.user_id as user_id,
                    count(f.id) as total_pattern,
                    count(g.id) as total_report
                FROM
                    '.$this->tbl_patrol.' a
                    JOIN '.$this->tbl_patrol_user.' b ON b.patrol_id = a.id
                    JOIN (
                        '.$this->tbl_building.' c
                        JOIN '.$this->tbl_instalasi.' d ON d.id = c.instalasi_id
                    ) ON c.id = a.building_id
                    JOIN (
                        '.$this->tbl_patrol_pattern.' f
                        LEFT JOIN '.$this->tbl_patrol_report.' g ON g.patrol_pattern_id = f.id AND g.user_id = '.$user_id.' and g.delete_flag = 0
                    ) ON f.patrol_id = a.id and f.delete_flag = 0
                WHERE
                    1=1
                    AND a.delete_flag = 0
                    AND b.delete_flag = 0
                    AND a.date = "'.date('Y-m-d').'"
                    AND b.user_id = '.$user_id.'
                group by
                    a.id
            ) z 
            WHERE 
                z.total_report < z.total_pattern
        ';
        $patrol = $this->model_basic->select_row_by_query($sql);
        if (!$patrol)
            return $this->res($data)->success();

        $data = [];
        // Get Patrol Pattern by patrol id
        $patrol_pattern_progress = 0;
        $patrol_pattern_total = 0;
        $patrol_pattern_id = null;
        $checkpoint_id = null;
        $patrol_pattern = $this->model_basic->select_where_order($this->tbl_patrol_pattern, 'patrol_id', $patrol->id, 'sequence', 'asc')->result();
        foreach ($patrol_pattern as $k => $v) {
            $patrol_pattern_total ++;
            if ($patrol_pattern_id) continue;
            // Get Patrol checkin and report by patrol pattern id
            $where = [];
            $where['patrol_pattern_id'] = $v->id;
            $where['user_id'] = $patrol->user_id;
            // $where_checkin = $where;
            // $where_checkin['status'] = 1;
            // $patrol_checkin = $this->model_basic->select_where_array($this->tbl_patrol_checkin, $where_checkin)->row();
            $patrol_report = $this->model_basic->select_where_array($this->tbl_patrol_report, $where)->row();
            // if (!$patrol_checkin and !$patrol_report) {
            if (!$patrol_report) {
                $patrol_pattern_id = $v->id;
                $checkpoint_id = $v->checkpoint_id;
            }
            $patrol_pattern_progress ++;
        }
        $data['status']['progress'] = $patrol_pattern_progress;
        $data['status']['total'] = $patrol_pattern_total;
        $data['patrol_pattern_id'] = $patrol_pattern_id;
        $location = null;
        // Get checkpoint name
        $checkpoint = $this->model_basic->select_where($this->tbl_checkpoint, 'id', $checkpoint_id)->row();
        if ($checkpoint) $location = $checkpoint->checkpoint_name;
        $cctv = $this->model_basic->select_where($this->tbl_cctv, 'id', $checkpoint->cctv_csc_id)->row();
        if ($cctv) $location = $location.' (CCTV: '.$cctv->location.')';
        $data['location'] = $location;
        
        return $this->res($data)->success();
    }

    // Verify CCTV
    public function verify_cctv() {
        $data = null;
        $user_id = $this->user_id;

        $post = $this->populate_post();
        if (!$post) return $this->res(['message' => 'Please complete form'])->error();

        $this->form_validation->set_rules('patrol_pattern_id', 'patrol_pattern_id', 'required');

        if (!$this->form_validation->run())
            return $this->res(['message' => $this->validation_msg(validation_errors())])->error();

        $patrol_pattern_id = $post['patrol_pattern_id'];

        // Get Patrol
        $sql = '
            SELECT 
                a.id as id,
                b.user_id as user_id
            FROM 
                '.$this->tbl_patrol.' a
                JOIN '.$this->tbl_patrol_user.' b ON b.patrol_id = a.id
                JOIN '.$this->tbl_patrol_pattern.' c ON c.patrol_id = a.id
            WHERE 
                1=1
                AND a.delete_flag = 0
                AND b.delete_flag = 0
                AND a.date = "'.date('Y-m-d').'"
                AND b.user_id = '.$user_id.'
                AND c.delete_flag = 0
                AND c.id = '.$patrol_pattern_id.'
        ';
        $patrol = $this->model_basic->select_row_by_query($sql);
        if (!$patrol)
            return $this->res(['message' => 'Patrol not found'])->error();

        // Check patrol checkin
        $where = [];
        $where['patrol_pattern_id'] = $patrol_pattern_id;
        $where['user_id'] = $user_id;
        // $where['status'] = 1;
        $patrol_checkin = $this->model_basic->select_where_array($this->tbl_patrol_checkin, $where)->row();
        if ($patrol_checkin and $patrol_checkin->status==1)
            return $this->res(['message' => 'Personel has request checkin','data_error' => ['chackin_type'=>$patrol_checkin->checkin_type]])->error();

        try {
            // DB Transaction Start
            $this->db->trans_start();

            $status = false;
            // Check API CCTV
            // Get User Realname
            $user = $this->model_basic->select_where($this->tbl_user, 'id', $user_id)->row();
            if (!$user)
                return $this->res(['message' => 'User not found'])->error();
            $name = $user->realname;
            // Get IP CCTV
            $patrol_pattern = $this->model_basic->select_where($this->tbl_patrol_pattern, 'id', $patrol_pattern_id)->row();
            if (!$patrol_pattern)
                return $this->res(['message' => 'Patrol pattern not found'])->error();
            $checkpoint_id = $patrol_pattern->checkpoint_id;
            $checkpoint = $this->model_basic->select_where($this->tbl_checkpoint, 'id', $checkpoint_id)->row();
            if (!$checkpoint)
                return $this->res(['message' => 'Checkpoint not found'])->error();
            $cctv_csc_id = $checkpoint->cctv_csc_id;
            $cctv_csc = $this->model_basic->select_where($this->tbl_cctv, 'id', $cctv_csc_id)->row();
            if (!$cctv_csc)
                return $this->res(['message' => 'CCTV not found'])->error();
            $ip = $cctv_csc->ip4_address;
            // $ip = '192.168.2.226'; // sample
            // Set date range
            $current_date = date('Y-m-d H:i');
            $current_date_plus = date('Y-m-d H:i',strtotime('+5 minute',strtotime($current_date)));
            $current_date_minus = date('Y-m-d H:i',strtotime('-5 minute',strtotime($current_date)));
            // Create Query SQL
            $sql = "
                SELECT
                    NPK
                FROM 
                    ms_logtable
                WHERE
                    1=1
                    AND NPK = '".$name."'
                    AND ip = '".$ip."'
                    AND date BETWEEN '".$current_date_minus."' AND '".$current_date_plus."'
                LIMIT 1
            ";
            // Define DB MySQL Server
            $this->mysqlserver = $this->load->database('mysqlserver', true);
            $res = $this->mysqlserver->query($sql)->row();
            if ($res) $status = true;

            if ($status==false) {
                // Insert to patrol checkin
                if (!$patrol_checkin) {
                    $insert = [];
                    $insert['patrol_pattern_id'] = $patrol_pattern_id;
                    $insert['user_id'] = $user_id;
                    // $insert['image_name'] = $uploads['file_name'];
                    $insert['image_name'] = '';
                    $insert['checkin_type'] = 'manual';
                    $insert['status'] = 0;
                    $insert['created_by'] = $user_id;
                    $insert['created_date'] = date('Y-m-d H:i:s');
                    $insert['delete_flag'] = 0;
                    $do_insert = $this->model_basic->insert_all($this->tbl_patrol_checkin,$insert);
                    if (!$do_insert)
                        return $this->res(['message' => 'Failed insert checkin'])->error();
                }
            } else 
            if ($status==true) {
                if ($patrol_checkin) {
                    // Delete checkin manual
                    $do_delete = $this->model_basic->delete($this->tbl_patrol_checkin,'id',$patrol_checkin->id);
                    if (!$do_delete)
                        return $this->res(['message' => 'Failed delete checkin'])->error();
                }
                $insert = [];
                $insert['patrol_pattern_id'] = $patrol_pattern_id;
                $insert['user_id'] = $user_id;
                // $insert['image_name'] = $uploads['file_name'];
                $insert['image_name'] = '';
                $insert['checkin_type'] = 'cctv';
                $insert['status'] = 1;
                $insert['created_by'] = $user_id;
                $insert['created_date'] = date('Y-m-d H:i:s');
                $insert['delete_flag'] = 0;
                $do_insert = $this->model_basic->insert_all($this->tbl_patrol_checkin,$insert);
                if (!$do_insert)
                    return $this->res(['message' => 'Failed insert checkin'])->error();
            }

            // DB Transaction Commit
            $this->db->trans_complete();

            $data = [];
            $data['status'] = $status;
            return $this->res($data)->success();
        } catch (Exception $e) {
            // DB Transaction Rollback
            $this->db->trans_rollback();
            return $this->res(['message' => 'Invalid Proccess'])->error();
        }
    }

    // Verify Camera // Not Used
    public function verify_camera() {
        $data = null;
        $user_id = $this->user_id;

        $post = $this->populate_post();
        if (!$post) return $this->res(['message' => 'Please complete form'])->error();

        $this->form_validation->set_rules('patrol_pattern_id', 'patrol_pattern_id', 'required');

        if (!$this->form_validation->run())
            return $this->res(['message' => $this->validation_msg(validation_errors())])->error();

        $patrol_pattern_id = $post['patrol_pattern_id'];

        /* Check Image upload
        if (count($_FILES)==0)
            return $this->res(['message' => 'The image field is required'])->error();
        */
        /*
        // Get Patrol
        $sql = '
            SELECT 
                a.id as id,
                b.user_id as user_id
            FROM 
                '.$this->tbl_patrol.' a
                JOIN '.$this->tbl_patrol_user.' b ON b.patrol_id = a.id
                JOIN '.$this->tbl_patrol_pattern.' c ON c.patrol_id = a.id
            WHERE 
                1=1
                AND a.delete_flag = 0
                AND b.delete_flag = 0
                AND a.date = "'.date('Y-m-d').'"
                AND b.user_id = '.$user_id.'
                AND c.id = '.$patrol_pattern_id.'
        ';
        $patrol = $this->model_basic->select_row_by_query($sql);
        if (!$patrol)
            return $this->res(['message' => 'Patrol not found'])->error();

        // Check patrol checkin
        $where = [];
        $where['patrol_pattern_id'] = $patrol_pattern_id;
        $where['user_id'] = $user_id;
        // $where['status'] = 1;
        $patrol_checkin = $this->model_basic->select_where_array($this->tbl_patrol_checkin, $where)->row();
        if ($patrol_checkin)
            return $this->res(['message' => 'Personel has request checkin'])->error();
        */
        try {
            // DB Transaction Start
            $this->db->trans_start();

            /* Upload Photo
            $name = date('YmdHis').'_patrol_checkin_pattern_'.$patrol_pattern_id.'_user_'.$user_id;
            $path = 'assets/uploads/esams/patrol';
            $uploads = $this->do_upload_image($name, $path, 'image');
            if (empty($uploads['file_name']))
                return $this->res(['message' => strip_tags($uploads)])->error();
            */
            /*
            // Insert to patrol checkin
            $insert = [];
            $insert['patrol_pattern_id'] = $patrol_pattern_id;
            $insert['user_id'] = $user_id;
            // $insert['image_name'] = $uploads['file_name'];
            $insert['image_name'] = '';
            $insert['checkin_type'] = 'manual';
            $insert['status'] = 1;
            $insert['created_by'] = $user_id;
            $insert['created_date'] = date('Y-m-d H:i:s');
            $insert['delete_flag'] = 0;
            $do_insert = $this->model_basic->insert_all($this->tbl_patrol_checkin,$insert);
            if (!$do_insert)
                return $this->res(['message' => 'Failed insert checkin'])->error();
            */
            // DB Transaction Commit
            $this->db->trans_complete();

            $data = [];
            $data['status'] = true;
            return $this->res($data)->success();
        } catch (Exception $e) {
            // DB Transaction Rollback
            $this->db->trans_rollback();
            return $this->res(['message' => 'Invalid Proccess'])->error();
        }
    }

    // Form Report Checklist by Patrol Pattern
    public function report_checklist() {
        $data = null;
        $user_id = $this->user_id;

        /* POST checklist per checkpoint
        $patrol_pattern_id = $post['patrol_pattern_id'];

        // Get checkpoint id
        $patrol_pattern = $this->model_basic->select_where($this->tbl_patrol_pattern, 'id', $patrol_pattern_id)->row();
        if (!$patrol_pattern)
            return $this->res(['message' => 'Patrol pattern not found'])->error();
        $checkpoint_id = $patrol_pattern->checkpoint_id;

        // Get Checklist 
        $checklist = $this->model_basic->select_where($this->tbl_checklist, 'checkpoint_id', $checkpoint_id)->result();
        */
        
        // Get Checklist 
        $checklist = $this->model_basic->select_all($this->tbl_checklist);
        if (count($checklist)==0)
            return $this->res($data)->success();

        $data = [];
        foreach ($checklist as $k => $v) {
            $data[$k]['id'] = $v->id;
            $data[$k]['name'] = $v->checklist_name;
        }

        return $this->res($data)->success();
    }
    public function report_checklist_checkpoint() {
        $post = $this->populate_post();
        if (!$post) return $this->res(['message' => 'Please complete form'])->error();

        $this->form_validation->set_rules('patrol_pattern_id', 'patrol_pattern_id', 'required');

        if (!$this->form_validation->run())
            return $this->res(['message' => $this->validation_msg(validation_errors())])->error();

        $data = null;
        $user_id = $this->user_id;

        // POST checklist per checkpoint
        $patrol_pattern_id = $post['patrol_pattern_id'];

        // Get checkpoint id
        $patrol_pattern = $this->model_basic->select_where($this->tbl_patrol_pattern, 'id', $patrol_pattern_id)->row();
        if (!$patrol_pattern)
            return $this->res(['message' => 'Patrol pattern not found'])->error();
        $checkpoint_id = $patrol_pattern->checkpoint_id;

        // Get Checklist 
        $checklist = $this->model_basic->select_where($this->tbl_checklist, 'checkpoint_id', $checkpoint_id)->result();
        if (count($checklist)==0)
            return $this->res($data)->success();

        $data = [];
        foreach ($checklist as $k => $v) {
            $data[$k]['id'] = $v->id;
            $data[$k]['name'] = $v->checklist_name;
            $value = null;
            // Get Checklist Value
            $checklist_detail = $this->model_basic->select_where($this->tbl_checklist_detail, 'checklist_id', $v->id)->result();
            if (count($checklist_detail)>0) $value = [];
            foreach ($checklist_detail as $l => $r) {
                $value[$l] = $checklist_detail->value;
            }
            $data[$k]['value'] = $value;
        }

        return $this->res($data)->success();
    }

    // Submit Report
    public function report_submit() {
        if (PHP_VERSION >= 7) {
            ini_set('display_errors', 0);
            error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);
        }
        $data = null;
        $user_id = $this->user_id;

        $post = $this->populate_post();
        if (!$post) return $this->res(['message' => 'Please complete form'])->error();

        $this->form_validation->set_rules('patrol_pattern_id', 'patrol_pattern_id', 'required');

        if (!$this->form_validation->run())
            return $this->res(['message' => $this->validation_msg(validation_errors())])->error();

        $patrol_pattern_id = $post['patrol_pattern_id'];
        $report_text = $post['report_text'];
        $checklist = $post['checklist'];

        if (count($_FILES)==0)
            return $this->res(['message' => 'The image field is required'])->error();

        // Get Patrol
        $sql = '
            SELECT 
                a.id as id,
                b.user_id as user_id
            FROM 
                '.$this->tbl_patrol.' a
                JOIN '.$this->tbl_patrol_user.' b ON b.patrol_id = a.id
                JOIN '.$this->tbl_patrol_pattern.' c ON c.patrol_id = a.id
            WHERE 
                1=1
                AND a.delete_flag = 0
                AND b.delete_flag = 0
                AND a.date = "'.date('Y-m-d').'"
                AND b.user_id = '.$user_id.'
                AND c.delete_flag = 0
                AND c.id = '.$patrol_pattern_id.'
        ';
        $patrol = $this->model_basic->select_row_by_query($sql);
        if (!$patrol)
            return $this->res(['message' => 'Patrol not found'])->error();

        // Get patrol report
        $where = [];
        $where['patrol_pattern_id'] = $patrol_pattern_id;
        $where['user_id'] = $user_id;
        $patrol_report = $this->model_basic->select_where_array($this->tbl_patrol_report, $where)->row();
        if ($patrol_report)
            return $this->res(['message' => 'Personel has report'])->error();

        // Get checkpoint id
        $patrol_pattern = $this->model_basic->select_where($this->tbl_patrol_pattern, 'id', $patrol_pattern_id)->row();
        if (!$patrol_pattern)
            return $this->res(['message' => 'Patrol pattern not found'])->error();

        // Check patrol checkin
        $checkin_manual = false;
        $where = [];
        $where['patrol_pattern_id'] = $patrol_pattern_id;
        $where['user_id'] = $user_id;
        // $where['status'] = 1;
        $patrol_checkin = $this->model_basic->select_where_array($this->tbl_patrol_checkin, $where)->row();
        if (!$patrol_checkin)
            return $this->res(['message' => 'Personel not checkin'])->error();
        if ($patrol_checkin->status==0 and $patrol_checkin->checkin_type=='cctv') 
            return $this->res(['message' => 'Personel not checkin'])->error();
        if ($patrol_checkin->status==0 and $patrol_checkin->checkin_type=='manual')
            return $this->res(['message' => 'Please contact admin for approve manual checkin'])->error();
        if ($patrol_checkin->status==1 and $patrol_checkin->checkin_type=='manual') {
            if (!isset($_FILES['checkin']))
                return $this->res(['message' => 'Please complete form (upload photo checkin)'])->error();
            $checkin_manual = true;
        }

        try {
            // DB Transaction Start
            $this->db->trans_start();

            // Upload Photo checkin manual
            if ($checkin_manual==true) {
                $name = date('YmdHis').'_patrol_checkin_pattern_'.$patrol_pattern_id.'_user_'.$user_id;
                $path = 'assets/uploads/esams/patrol';
                $uploads = $this->do_upload_image($name, $path, 'checkin');
                if (empty($uploads['file_name'])) 
                    return $this->res(['message' => strip_tags($uploads)])->error();

                // Update checkin manual image name
                $update = [];
                $update['image_name'] = $uploads['file_name'];
                $do_update = $this->model_basic->update($this->tbl_patrol_checkin,$update,'id',$patrol_checkin->id);
                if (!$do_update)
                    return $this->res(['message' => 'Failed update patrol checkin'])->error();
            }
            
            // Insert to patrol report
            $insert = [];
            $insert['patrol_pattern_id'] = $patrol_pattern_id;
            $insert['user_id'] = $user_id;
            $insert['report_text'] = $report_text;
            $insert['created_by'] = $user_id;
            $insert['created_date'] = date('Y-m-d H:i:s');
            $insert['delete_flag'] = 0;
            $patrol_report_insert = $this->model_basic->insert_all($this->tbl_patrol_report,$insert);
            if (!$patrol_report_insert)
                return $this->res(['message' => 'Failed insert patrol report'])->error();

            // Insert to patrol report checkpoint
            foreach ($checklist as $k => $v) {
                $insert = [];
                $insert['patrol_report_id'] = $patrol_report_insert->id;
                $insert['checkpoint_id'] = $patrol_pattern->checkpoint_id;
                $insert['checklist_id'] = $k;
                $insert['value'] = $v;
                $insert['created_by'] = $user_id;
                $insert['created_date'] = date('Y-m-d H:i:s');
                $insert['delete_flag'] = 0;
                $patrol_report_checkpoint_insert = $this->model_basic->insert_all($this->tbl_patrol_report_checkpoint,$insert);
                if (!$patrol_report_checkpoint_insert)
                    return $this->res(['message' => 'Failed insert patrol report checkpoint'])->error();
            }

            // Insert patrol report attachment
            $files = $_FILES;
            $cFiles = count($_FILES['images']['name']);
            for($i=0; $i<$cFiles; $i++) {
                $_FILES['image']['name'] = $files['images']['name'][$i];
                $_FILES['image']['type'] = $files['images']['type'][$i];
                $_FILES['image']['tmp_name'] = $files['images']['tmp_name'][$i];
                $_FILES['image']['error'] = $files['images']['error'][$i];
                $_FILES['image']['size'] = $files['images']['size'][$i];
                // Upload single photo
                $name = date('YmdHis').'_patrol_report_attachment_'.$i.'_pattern_'.$patrol_pattern_id.'_user_'.$user_id;
                $path = 'assets/uploads/esams/patrol';
                $uploads = $this->do_upload_image($name, $path, 'image');
                if (empty($uploads['file_name'])) {
                    return $this->res(['message' => strip_tags($uploads)])->error();
                    exit;
                }
                // Insert patrol report attachment
                $insert = [];
                $insert['patrol_report_id'] = $patrol_report_insert->id;
                $insert['image_name'] = $uploads['file_name'];
                $insert['created_by'] = $user_id;
                $insert['created_date'] = date('Y-m-d H:i:s');
                $insert['delete_flag'] = 0;
                $patrol_report_attachment_insert = $this->model_basic->insert_all($this->tbl_patrol_report_attachment,$insert);
                if (!$patrol_report_attachment_insert)
                    return $this->res(['message' => 'Failed insert patrol report attachment'])->error();
            }

            // Get Response
            $complete_patrol = true;
            $patrol_pattern_id = null;

            // Get patrol pattern by patrol id
            $patrol_pattern = $this->model_basic->select_where_order($this->tbl_patrol_pattern, 'patrol_id', $patrol->id, 'sequence', 'asc')->result();
            foreach ($patrol_pattern as $k => $v) {
                $where = [];
                $where['patrol_pattern_id'] = $v->id;
                $where['user_id'] = $user_id;
                // Get Patrol report by patrol pattern id
                $patrol_report = $this->model_basic->select_where_array($this->tbl_patrol_report,$where)->row();
                if (!$patrol_report)
                    $complete_patrol = false;

                if ($patrol_pattern_id) continue;
                // Get Patrol checkin by patrol pattern id
                $patrol_checkin = $this->model_basic->select_where_array($this->tbl_patrol_checkin,$where)->row();
                if (!$patrol_checkin and !$patrol_report)
                    $patrol_pattern_id = $v->id;
            }
            // DB Transaction Commit
            $this->db->trans_complete();

            $data = [];
            $data['complete_patrol'] = $complete_patrol;
            $data['next_patrol'] = $complete_patrol==true?false:true;
            $data['patrol_pattern_id'] = $patrol_pattern_id;
            return $this->res($data)->success();
        } catch (Exception $e) {
            // DB Transaction Rollback
            $this->db->trans_rollback();
            return $this->res(['message' => 'Invalid Proccess'])->error();
        }
    }

}
