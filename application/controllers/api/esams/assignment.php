<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Assignment extends PX_Controller {

	protected $tokenkey = null;
    protected $user_id = null;

    public function __construct() {
        parent::__construct();
        $headers = $this->input->request_headers();
        if (isset($headers['Authorization'])) $this->tokenkey = $this->check_token($headers['Authorization']);
        if (!$this->tokenkey) show_403_api('Please provide correct token!');
        $token = $this->get_token_wso2_by_tokenkey($this->tokenkey);
        if (!$token) show_401_api('Invalid token!');
        if (OAUTH_TYPE!='mobile') if ($token->expires_in < time()) show_401_api('Token has expired!');
        $this->user_id = $token->user_id;
    }

    // List Assignment
    public function task_list() {
        $data = null;
        $user_id = $this->user_id;

        // Get assignment by user id
        // $sql = '
        //     SELECT 
        //         a.id,
        //         a.assignment_title,
        //         a.assignment_type,
        //         b.status as status
        //     FROM 
        //         '.$this->tbl_assignment.' a
        //         JOIN '.$this->tbl_assignment_people.' b ON b.assignment_id = a.id
        //         LEFT join (
        //             '.$this->tbl_assignment_people.' c
        //             JOIN (
        //                 '.$this->tbl_group_security.' d
        //                 JOIN '.$this->tbl_group_detail.' e ON e.group_security_id = d.id AND e.user_id = '.$user_id.'
        //             ) ON d.id = c.group_security_id
        //         ) ON c.assignment_id = a.id
        //     WHERE 
        //         1=1
        //         AND a.delete_flag = 0
        //         AND b.delete_flag = 0
        //         AND b.user_id = '.$user_id.'
        // ';
        $sql = '
            SELECT 
                a.id,
                a.assignment_title,
                a.assignment_type,
                IF(c.status is not null, c.status, b.status) as status,
                count(f.id) as total_detail,
                count(g.id) as total_checkin,
                f.end_time
            FROM 
                '.$this->tbl_assignment.' a
                LEFT JOIN '.$this->tbl_assignment_people.' b ON b.assignment_id = a.id AND b.delete_flag = 0 AND b.user_id = '.$user_id.'
                LEFT join (
                    '.$this->tbl_assignment_people.' c
                    JOIN (
                        '.$this->tbl_group_security.' d
                        JOIN '.$this->tbl_group_detail.' e ON e.group_security_id = d.id AND e.user_id = '.$user_id.'
                    ) ON d.id = c.group_security_id
                ) ON c.assignment_id = a.id
                join (
                    '.$this->tbl_assignment_detail.' f
                    left join '.$this->tbl_assignment_people_checkin.' g on g.assignment_detail_id = f.id and g.user_id = '.$user_id.' and g.delete_flag = 0
                ) on f.assignment_id = a.id and f.delete_flag = 0
            WHERE 
                1=1
                AND a.delete_flag = 0
                AND f.end_time > "'.date('Y-m-d H:i:s').'"
            group by 
                a.id
            having 
                status is not null
        ';
        $assignment = $this->model_basic->select_all_by_query($sql);
        if (count($assignment)==0)
            return $this->res($data)->success();

        $data = [];
        foreach ($assignment as $k => $v) {
            $data[$k]['assignment_id'] = $v->id;
            $data[$k]['assignment_title'] = $v->assignment_title;
            $data[$k]['assignment_type'] = $v->assignment_type;
            $start_date = null;
            $start_time = null;
            $end_date = null;
            $end_time = null;
            // Get assignment detail
            $assignment_detail = $this->model_basic->select_where($this->tbl_assignment_detail, 'assignment_id', $v->id)->result();
            if (count($assignment_detail)>0) {
                $start_date = date('d M Y', strtotime($assignment_detail[0]->start_time));
                $start_time = date('H:i', strtotime($assignment_detail[0]->start_time));
                if ($v->assignment_type=='single') {
                    $end_date = date('d M Y', strtotime($assignment_detail[0]->end_time));
                    $end_time = date('H:i', strtotime($assignment_detail[0]->end_time));
                } else
                if ($v->assignment_type=='series') {
                    $end_date = date('d M Y', strtotime($assignment_detail[(count($assignment_detail)-1)]->end_time));
                    $end_time = date('H:i', strtotime($assignment_detail[(count($assignment_detail)-1)]->end_time));
                }
            }
            $data[$k]['start_date'] = $start_date;
            $data[$k]['start_time'] = $start_time;
            $data[$k]['end_date'] = $end_date;
            $data[$k]['end_time'] = $end_time;
            $data[$k]['sub_task'] = count($assignment_detail);
            if ($v->total_checkin < $v->total_detail)
                $v->status = 'active';
            $data[$k]['status'] = $v->status;
        }
        
        return $this->res($data)->success();
    }

    // List Sub Task Assignment
    public function task_detail_list() {
        $assignment_id = $this->input->get('assignment_id');
        if (!$assignment_id)
            return $this->res(['message' => 'Please complete form'])->error();

        $data = null;
        $user_id = $this->user_id;

        // Get assignment by user id
        $sql = '
            SELECT 
                a.id,
                a.assignment_title,
                a.assignment_type,
                b.status as status
            FROM 
                '.$this->tbl_assignment.' a
                LEFT JOIN '.$this->tbl_assignment_people.' b ON b.assignment_id = a.id AND b.delete_flag = 0 AND b.user_id = '.$user_id.'
                LEFT join (
                    '.$this->tbl_assignment_people.' c
                    JOIN (
                        '.$this->tbl_group_security.' d
                        JOIN '.$this->tbl_group_detail.' e ON e.group_security_id = d.id AND e.user_id = '.$user_id.'
                    ) ON d.id = c.group_security_id
                ) ON c.assignment_id = a.id
            WHERE 
                1=1
                AND a.delete_flag = 0
                AND a.id = '.$assignment_id.'
        ';
        $assignment = $this->model_basic->select_row_by_query($sql);
        if (!$assignment)
            return $this->res(['message' => 'Task not found'])->error();

        $data = [];
        $data['assignment_id'] = $assignment->id;
        $data['assignment_title'] = $assignment->assignment_title;
        $data['assignment_type'] = $assignment->assignment_type;
        $start_date = null;
        $start_time = null;
        $end_date = null;
        $end_time = null;
        // Get assignment detail
        $assignment_detail = $this->model_basic->select_where($this->tbl_assignment_detail, 'assignment_id', $assignment_id)->result();
        if (count($assignment_detail)>0) {
            $start_date = date('d M Y', strtotime($assignment_detail[0]->start_time));
            $start_time = date('H:i', strtotime($assignment_detail[0]->start_time));
            $end_date = date('d M Y', strtotime($assignment_detail[(count($assignment_detail)-1)]->end_time));
            $end_time = date('H:i', strtotime($assignment_detail[(count($assignment_detail)-1)]->end_time));
        }
        $data['start_date'] = $start_date;
        $data['start_time'] = $start_time;
        $data['end_date'] = $end_date;
        $data['end_time'] = $end_time;
        $detail = [];
        foreach ($assignment_detail as $k => $v) {
            $detail[$k]['task_id'] = $v->id;
            $detail[$k]['task_title'] = $v->task_title;
            $detail[$k]['start_date'] = date('d M Y', strtotime($v->start_time));
            $detail[$k]['start_time'] = date('H:i', strtotime($v->start_time));
            $detail[$k]['end_date'] = date('d M Y', strtotime($v->end_time));
            $detail[$k]['end_time'] = date('H:i', strtotime($v->end_time));
            // get report assignmend detail by user id
            $where = [];
            $where['assignment_detail_id'] = $v->id;
            $where['user_id'] = $user_id;
            $assignment_report = $this->model_basic->select_where_array($this->tbl_assignment_report, $where)->row();
            $detail[$k]['status'] = $assignment_report?'done':'active';
        }
        $data['detail'] = $detail;
        return $this->res($data)->success();
    }

    // Detail Assignment
    public function task_detail() {
        $task_id = $this->input->get('task_id');
        $assignment_id = $this->input->get('assignment_id');
        if (!$assignment_id)
            return $this->res(['message' => 'Please complete form'])->error();

        $data = null;
        $user_id = $this->user_id;

        // Get assignment by user id
        $sql = '
            SELECT 
                a.id,
                a.assignment_title,
                a.assignment_type,
                b.status as status,
                f.id as task_id,
                f.location,
                f.radius,
                f.task_title,
                f.description,
                f.start_time,
                f.end_time
            FROM 
                '.$this->tbl_assignment.' a
                LEFT JOIN '.$this->tbl_assignment_people.' b ON b.assignment_id = a.id AND b.delete_flag = 0 AND b.user_id = '.$user_id.' AND b.status IN ("active", "failed")
                LEFT join (
                    '.$this->tbl_assignment_people.' c
                    JOIN (
                        '.$this->tbl_group_security.' d
                        JOIN '.$this->tbl_group_detail.' e ON e.group_security_id = d.id AND e.user_id = '.$user_id.'
                    ) ON d.id = c.group_security_id
                ) ON c.assignment_id = a.id
                JOIN '.$this->tbl_assignment_detail.' f ON f.assignment_id = a.id
            WHERE 
                1=1
                AND a.delete_flag = 0
                AND a.id = '.$assignment_id.'
                '.(($task_id)?'AND f.id = '.$task_id:'').'
        ';
        $assignment = $this->model_basic->select_row_by_query($sql);
        if (!$assignment)
            return $this->res(['message' => 'Task not found'])->error();

        $assignment->status_checkin = false;

        // Check assignment people checkin
        $where = [];
        $where['assignment_detail_id'] = $assignment->task_id;
        $where['user_id'] = $user_id;
        $where['status'] = 1;
        $assignment_people_checkin = $this->model_basic->select_where_array($this->tbl_assignment_people_checkin, $where)->row();
        if ($assignment_people_checkin)
            $assignment->status_checkin = true;
            // return $this->res(['message' => 'Task has been checkin'])->error();

        $data = [];
        $data['task_id'] = $assignment->task_id;
        $location = json_decode($assignment->location);
        $location->longitude = (float)$location->longitude;
        $location->latitude = (float)$location->latitude;
        $data['status_checkin'] = $assignment->status_checkin;
        $data['location'] = $location;
        $data['radius'] = $assignment->radius;
        $data['title'] = $assignment->task_title==''?$assignment->assignment_title:$assignment->task_title;
        $data['description'] = strip_tags($assignment->description);
        // $data['start_date'] = date('d M Y', strtotime($assignment->start_time));
        // $data['start_time'] = date('H:i', strtotime($assignment->start_time));
        $data['end_date'] = date('d M Y', strtotime($assignment->end_time));
        $data['end_time'] = date('H:i', strtotime($assignment->end_time));
        // Get assignment attachment
        $assignment_attachment = $this->model_basic->select_where($this->tbl_assignment_attachment, 'assignment_detail_id', $assignment->task_id)->result();
        $image = [];
        foreach ($assignment_attachment as $k => $v)
            $image[$k] = base_url().'assets/uploads/esams/assignment/'.$v->image_name;
        $data['image'] = $image;
        return $this->res($data)->success();
    }

    // Check In
    public function task_checkin() {
        $post = $this->input->post(NULL, TRUE);

        $this->form_validation->set_rules('task_id', 'task_id', 'required');

        if (!$this->form_validation->run())
            return $this->res(['message' => $this->validation_msg(validation_errors())])->error();

        $data = null;
        $user_id = $this->user_id;
        $task_id = $post['task_id'];
        $latitude = $post['latitude'];
        $longitude = $post['longitude'];

        // Get assignment by user id
        $sql = '
            SELECT 
                a.id,
                f.id as assignment_detail_id,
                f.start_time,
                f.end_time
            FROM 
                '.$this->tbl_assignment.' a
                JOIN '.$this->tbl_assignment_people.' b ON b.assignment_id = a.id AND b.delete_flag = 0 AND b.status IN ("active", "failed")
                LEFT join (
                    '.$this->tbl_assignment_people.' c
                    JOIN (
                        '.$this->tbl_group_security.' d
                        JOIN '.$this->tbl_group_detail.' e ON e.group_security_id = d.id AND e.user_id = '.$user_id.'
                    ) ON d.id = c.group_security_id
                ) ON c.assignment_id = a.id
                LEFT join '.$this->tbl_assignment_people.' g on g.assignment_id = a.id AND g.delete_flag = 0 AND g.user_id = '.$user_id.'
                JOIN '.$this->tbl_assignment_detail.' f ON f.assignment_id = a.id
            WHERE 
                1=1
                AND a.delete_flag = 0
                AND f.id = '.$task_id.'
        ';
        
        $assignment = $this->model_basic->select_row_by_query($sql);
        if (!$assignment)
            return $this->res(['message' => 'Task not found!'])->error();

        if (date('Y-m-d H:i:s') < $assignment->start_time)
            return $this->res(['message' => 'Task not available!'])->error();

        if (date('Y-m-d H:i:s') > $assignment->end_time)
            return $this->res(['message' => 'Task has been expired!'])->error();

        if (count($_FILES)==0)
            return $this->res(['message' => 'The image field is required'])->error();

        try {
            // DB Transaction Start
            $this->db->trans_start();

            // Upload image
            $name = date('YmdHis').'_assignment_checkin_task_'.$task_id.'_user_'.$user_id;
            $path = 'assets/uploads/esams/assignment';
            $uploads = $this->do_upload_image($name, $path, 'image');
            if (empty($uploads['file_name']))
                return $this->res(['message' => strip_tags($uploads)])->error();

            // Insert to assignment attachment
            $insert = [];
            $insert['assignment_detail_id'] = $assignment->assignment_detail_id;
            $insert['user_id'] = $user_id;
            $location = [];
            $location['latitude'] = $latitude?$latitude:'0.0';
            $location['longitude'] = $longitude?$longitude:'0.0';
            $insert['location'] = json_encode($location);
            $insert['image_name'] = $uploads['file_name'];
            $insert['status'] = 1;
            $insert['created_by'] = $user_id;
            $insert['created_date'] = date('Y-m-d H:i:s');
            $insert['delete_flag'] = 0;
            $do_insert = $this->model_basic->insert_all($this->tbl_assignment_people_checkin,$insert);
            if (!$do_insert)
                return $this->res(['message' => 'Failed saving image'])->error();

            // DB Transaction Commit
            $this->db->trans_complete();

            return $this->res(['message' => 'Image has been submited'])->success();
        } catch (Exception $e) {
            // DB Transaction Rollback
            $this->db->trans_rollback();
            return $this->res(['message' => 'Invalid Proccess'])->error();
        }
    }

    // Submit Report
    public function task_submit() {
        $post = $this->input->post(NULL, TRUE);

        $this->form_validation->set_rules('task_id', 'task_id', 'required');
        $this->form_validation->set_rules('description', 'description', 'required');

        if (!$this->form_validation->run())
            return $this->res(['message' => $this->validation_msg(validation_errors())])->error();

        $data = null;
        $user_id = $this->user_id;
        $task_id = $post['task_id'];
        $description = $post['description'];

        // Get assignment by user id
        $sql = '
            SELECT 
                a.id,
                a.assignment_type,
                b.id as assignment_people_id,
                f.id as assignment_detail_id
            FROM 
                '.$this->tbl_assignment.' a
                JOIN '.$this->tbl_assignment_people.' b ON b.assignment_id = a.id AND b.delete_flag = 0 AND b.status IN ("active", "failed")
                LEFT join (
                    '.$this->tbl_assignment_people.' c
                    JOIN (
                        '.$this->tbl_group_security.' d
                        JOIN '.$this->tbl_group_detail.' e ON e.group_security_id = d.id AND e.user_id = '.$user_id.'
                    ) ON d.id = c.group_security_id
                ) ON c.assignment_id = a.id
                LEFT join '.$this->tbl_assignment_people.' g on g.assignment_id = a.id AND g.delete_flag = 0 AND g.user_id = '.$user_id.'
                JOIN '.$this->tbl_assignment_detail.' f ON f.assignment_id = a.id
            WHERE 
                1=1
                AND a.delete_flag = 0
                AND f.id = '.$task_id.'
        ';
        $assignment = $this->model_basic->select_row_by_query($sql);
        if (!$assignment)
            return $this->res(['message' => 'Task not found!'])->error();
        
        if (count($_FILES)==0)
            return $this->res(['message' => 'The images field is required'])->error();

        try {
            // DB Transaction Start
            $this->db->trans_start();

            // Insert to assignment report
            $insert = [];
            $insert['assignment_detail_id'] = $assignment->assignment_detail_id;
            $insert['user_id'] = $user_id;
            $insert['report_text'] = $description;
            $insert['created_by'] = $user_id;
            $insert['created_date'] = date('Y-m-d H:i:s');
            $insert['delete_flag'] = 0;
            $do_insert_report = $this->model_basic->insert_all($this->tbl_assignment_report,$insert);
            if (!$do_insert_report)
                return $this->res(['message' => 'Failed saving report'])->error();

            // Upload images
            for ($i=0; $i < count($_FILES); $i++) {
                // Upload single image
                $name = date('YmdHis').'_assignment_report_'.($i+1).'_task_'.$task_id.'_user_'.$user_id;
                $path = 'assets/uploads/esams/assignment';
                $uploads = $this->do_upload_image($name, $path, 'image_'.($i+1));
                if (empty($uploads['file_name'])) {
                    return $this->res(['message' => strip_tags($uploads)])->error();
                    exit;
                }

                // Insert to assignment report attachment
                $insert = [];
                $insert['assignment_report_id'] = $do_insert_report->id;
                $insert['image_name'] = $uploads['file_name'];
                $insert['created_by'] = $user_id;
                $insert['created_date'] = date('Y-m-d H:i:s');
                $insert['delete_flag'] = 0;
                $do_insert = $this->model_basic->insert_all($this->tbl_assignment_report_attachment,$insert);
                if (!$do_insert) {
                    return $this->res(['message' => 'Failed saving report image'])->error();
                    exit;
                }
            }

            // Update status people
            $status = 'done';
            if ($assignment->assignment_type=='series') {
                // get all assignment detail
                $assignment_detail = $this->model_basic->select_where($this->tbl_assignment_detail, 'assignment_id', $assignment->id)->result();
                foreach ($assignment_detail as $k => $v) {
                    // get people checkin
                    $where = [];
                    $where['assignment_detail_id'] = $v->id;
                    $where['user_id'] = $user_id;
                    $where['status'] = 1;
                    $assignment_people_checkin = $this->model_basic->select_where_array($this->tbl_assignment_people_checkin, $where)->row();
                    if (!$assignment_people_checkin)
                        $status = 'active';
                }
            }
            // update status assignment people
            $update = [];
            $update['status'] = $status;
            $do_update = $this->model_basic->update($this->tbl_assignment_people,$update,'id',$assignment->assignment_people_id);
            if (!$do_update)
                return $this->res(['message' => 'Failed update assignment people'])->error();

            // DB Transaction Commit
            $this->db->trans_complete();

            return $this->res('Report has been submited')->success();
        } catch (Exception $e) {
            // DB Transaction Rollback
            $this->db->trans_rollback();
            return $this->res(['message' => 'Invalid Proccess'])->error();
        }
    }

    // List Problem
    public function problem_list() {
        $data = null;
        $user_id = $this->user_id;

        // Get problem by user id
        $sql = '
            SELECT 
                b.id as issue_id,
                c.building_name,
                d.name as location,
                b.floor,
                b.area,
                a.created_date as created_date
            FROM 
                '.$this->tbl_issue_assign.' a
                JOIN ('.$this->tbl_issue.' b
                    JOIN (
                        '.$this->tbl_building.' c
                        JOIN '.$this->tbl_instalasi.' d ON d.id = c.instalasi_id
                    ) ON c.id = b.building_id
                ) ON b.id = a.issue_id
            WHERE 
                1=1
                AND a.delete_flag = 0
                AND b.delete_flag = 0
                AND a.user_id = '.$user_id.'
                AND a.status = "active"
        ';
        $issue = $this->model_basic->select_all_by_query($sql);
        if (count($issue)==0)
            return $this->res($data)->success();

        $data = [];
        foreach ($issue as $k => $v) {
            $data[$k]['issue_id'] = $v->issue_id;
            $data[$k]['location'] = $v->location;
            $data[$k]['building'] = $v->building_name;
            $data[$k]['floor'] = $v->floor;
            $data[$k]['area'] = $v->area;
            $data[$k]['created_date'] = date('d M Y (H:i)', strtotime($v->created_date));
        }
        
        return $this->res($data)->success();
    }

    // Detail Problem
    public function problem_detail() {
        $issue_id = $this->input->get('issue_id');
        if (!$issue_id)
            return $this->res(['message' => 'Please complete form'])->error();

        $data = null;
        $user_id = $this->user_id;

        // Get problem by user id
        $sql = '
            SELECT 
                a.id,
                d.id as issue_assign_id,
                b.building_name,
                c.name as location,
                a.floor,
                a.area,
                a.description,
                a.fact,
                a.analysis,
                a.summary
            FROM 
                '.$this->tbl_issue.' a
                JOIN (
                    '.$this->tbl_building.' b
                    JOIN '.$this->tbl_instalasi.' c ON c.id = b.instalasi_id
                ) ON b.id = a.building_id
                JOIN '.$this->tbl_issue_assign.' d ON d.issue_id = a.id AND d.delete_flag = 0 AND d.status = "active"
            WHERE 
                1=1
                AND a.delete_flag = 0
                AND a.id = '.$issue_id.'
                AND d.user_id = '.$user_id.'
        ';
        $issue = $this->model_basic->select_row_by_query($sql);
        if (!$issue)
            return $this->res(['message' => 'Issue not found'])->error();

        $data = [];
        $data['issue_assign_id'] = $issue->issue_assign_id;
        $data['location'] = $issue->location;
        $data['building'] = $issue->building_name;
        $data['floor'] = $issue->floor;
        $data['area'] = $issue->area;
        // $data['description'] = $issue->description;
        $data['fact'] = strip_tags($issue->fact);
        $data['summary'] = strip_tags($issue->summary);
        $data['analysis'] = strip_tags($issue->analysis);
        // Get issue attachment
        $issue_attachment = $this->model_basic->select_where($this->tbl_issue_attachment, 'issue_id', $issue->id)->result();
        $image = [];
        foreach ($issue_attachment as $k => $v)
            $image[$k] = base_url().'assets/uploads/esams/issue/'.$v->image_name;
        $data['image'] = $image;
        
        return $this->res($data)->success();
    }

    // Submit Problem
    public function problem_submit() {
        $post = $this->populate_post();
        if (!$post) return $this->res(['message' => 'Please complete form'])->error();

        $this->form_validation->set_rules('issue_assign_id', 'issue_assign_id', 'required');
        $this->form_validation->set_rules('report_text', 'report_text', 'required');

        if (!$this->form_validation->run())
            return $this->res(['message' => $this->validation_msg(validation_errors())])->error();

        $issue_assign_id = $post['issue_assign_id'];
        $report_text = $post['report_text'];
        $floor = null;

        $data = null;
        $user_id = $this->user_id;

        if (count($_FILES)==0)
            return $this->res(['message' => 'The image field is required'])->error();

        // Cek Issue assign
        $where = [];
        $where['id'] = $issue_assign_id;
        $where['status'] = 'active';
        $issue_assign = $this->model_basic->select_where_array($this->tbl_issue_assign,$where)->row();
        if (!$issue_assign)
            return $this->res(['message' => 'Issue assign not found'])->error();

        try {
            // DB Transaction Start
            $this->db->trans_start();
            
            // Insert to issue
            $insert = [];
            $insert['issue_assign_id'] = $issue_assign_id;
            $insert['report_text'] = $report_text;
            $insert['created_by'] = $user_id;
            $insert['created_date'] = date('Y-m-d H:i:s');
            $insert['delete_flag'] = 0;
            $issue_report_insert = $this->model_basic->insert_all($this->tbl_issue_report,$insert);
            if (!$issue_report_insert)
                return $this->res(['message' => 'Failed insert issue report'])->error();

            // Insert issue attachment
            $files = $_FILES;
            $cFiles = count($_FILES['images']['name']);
            for($i=0; $i<$cFiles; $i++) {
                $_FILES['image']['name'] = $files['images']['name'][$i];
                $_FILES['image']['type'] = $files['images']['type'][$i];
                $_FILES['image']['tmp_name'] = $files['images']['tmp_name'][$i];
                $_FILES['image']['error'] = $files['images']['error'][$i];
                $_FILES['image']['size'] = $files['images']['size'][$i];
                // Upload single photo
                $name = date('YmdHis').'_issue_report_attachment_'.$i.'_user_'.$user_id;
                $path = 'assets/uploads/esams/issue';
                $uploads = $this->do_upload_image($name, $path, 'image');
                if (empty($uploads['file_name']))
                    return $this->res(['message' => strip_tags($uploads)])->error();

                // Insert issue attachment
                $insert = [];
                $insert['issue_report_id'] = $issue_report_insert->id;
                $insert['image_name'] = $uploads['file_name'];
                $insert['created_by'] = $user_id;
                $insert['created_date'] = date('Y-m-d H:i:s');
                $insert['delete_flag'] = 0;
                $issue_report_attachment_insert = $this->model_basic->insert_all($this->tbl_issue_report_attachment,$insert);
                if (!$issue_report_attachment_insert)
                    return $this->res(['message' => 'Failed insert issue report attachment'])->error();

                // Set done issue assign
                $update = [];
                $update['status'] = 'done';
                $update['modified_by'] = $user_id;
                $update['modified_date'] = date('Y-m-d H:i:s');
                $do_update = $this->model_basic->update($this->tbl_issue_assign,$update,'id',$issue_assign_id);
                if (!$do_update)
                    return $this->res(['message' => 'Failed update issue assign'])->error();

            }
            // DB Transaction Commit
            $this->db->trans_complete();

            return $this->res('Issue has been submited')->success();
        } catch (Exception $e) {
            // DB Transaction Rollback
            $this->db->trans_rollback();
            return $this->res(['message' => 'Invalid Proccess'])->error();
        }
    }

}
