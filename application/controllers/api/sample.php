<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sample extends PX_Controller {

	protected $tokenkey = null;

    public function __construct() {
        parent::__construct();
        $headers = $this->input->request_headers();
    	if (isset($headers['Authorization']))
           $this->tokenkey = $this->check_token($headers['Authorization']);
        if (!$this->tokenkey)
            show_403_api('Please provide correct token!');
    }

    // GET
    /*
    public function get_success() {
    	$get = $this->input->get();
        $this->res(($get?$get:'This is place for data/value if exist'))->success();
    }

    public function get_error() {
        $this->res(['message' => 'This is place for message error'])->error();
    }

    public function get_notfound() {
        $this->res(['message' => 'This is place for message notfound'])->notfound();
    }

    public function get_forbidden() {
        $this->res(['message' => 'This is place for message forbidden'])->forbidden();
    }

    public function get_unauthorized() {
        $this->res(['message' => 'This is place for message unauthorized'])->unauthorized();
    }

    // POST
    public function post_success() {
    	$post = $this->input->post();
        $this->res(($post?$post:'This is place for data/value if exist'))->success();
    }

    public function post_error() {
        $this->res(['message' => 'This is place for message error'])->error();
    }

    public function post_notfound() {
        $this->res(['message' => 'This is place for message notfound'])->notfound();
    }

    public function post_forbidden() {
        $this->res(['message' => 'This is place for message forbidden'])->forbidden();
    }

    public function post_unauthorized() {
        $this->res(['message' => 'This is place for message unauthorized'])->unauthorized();
    }
    */

}
