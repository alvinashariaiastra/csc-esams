<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends PX_Controller {

	public function __construct() {
		parent::__construct();
		// $data_row_ctr = $this->model_basic->select_where($this->table_menu,'target','admin')->row();
		$this->controller_attr = array('controller' => 'admin','controller_name' => 'Admin','controller_id' => 0);
	}

	public function index() {
		$data = $this->controller_attr;
		if($this->session->userdata('user') != FALSE){
			redirect('user/dashboard');
		}
		else
			redirect('user/login');
	}

	function dashboard() {
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('User','user');
		$data += $this->user_get_menu();
		if($this->session->userdata('user') != FALSE){
                    if($this->session_user['id_usergroup'] != 4)
                    {
			$data['banner'] = $this->model_basic->select_all($this->tbl_banner);
			$data['province'] = $this->model_basic->select_all_noflag($this->tbl_province);
			$data['status_merah'] = $this->model_basic->select_where_array($this->tbl_status_kerawanan,'instalasi_id = '.$this->session_user['id_instalasi'].' and status = 3')->num_rows();
			$data['status_kuning'] = $this->model_basic->select_where_array($this->tbl_status_kerawanan,'instalasi_id = '.$this->session_user['id_instalasi'].' and status = 2')->num_rows();
			$data['status_hijau'] = $this->model_basic->select_where_array($this->tbl_status_kerawanan,'instalasi_id = '.$this->session_user['id_instalasi'].' and status = 1')->num_rows();
			$data['security_analysis_info'] = $this->model_basic->select_where_limit_order($this->tbl_security_analysis,'kategori',2, 3,'DESC','date_created')->result();
			$data['security_analysis_analyst'] = $this->model_basic->select_where_limit_order($this->tbl_security_analysis,'kategori',1, 3,'DESC','date_created')->result();
			$data['info_training'] = $this->model_basic->select_all_limit_order($this->tbl_training_info,2,'date_created', 'DESC')->result();
			$data['asesmen_2004'] = $this->model_basic->select_where_order($this->tbl_assessment2004,'instalasi_id',$this->session_user['id_instalasi'],'date_created','DESC')->result();
			$data['asesmen_2015'] = $this->model_basic->select_where_order($this->tbl_assessment2015,'instalasi_id',$this->session_user['id_instalasi'],'date_created','DESC')->result();
			$data['stc_per_orang'] = $this->model_basic->select_where_order($this->tbl_standar_kompetensi_result,'user_id',$this->session_user['admin_id'],'date_created','DESC')->result();
			$data['content'] = $this->load->view('user_backend/admin/dashboard',$data,true);
			$this->load->view('user_backend/index',$data);
                    }
                    else
                    {
                        $data['banner'] = $this->model_basic->select_all($this->tbl_banner);
                        $where_array_info = array('kategori' => 2, 'karyawan_flag' => 1);
                        $where_array_analyst = array('kategori' => 1, 'karyawan_flag' => 1);
                        $data['security_analysis_info'] = $this->model_basic->select_where_array_order_limit($this->tbl_security_analysis,$where_array_info, 'date_created', 'DESC', 3)->result();
			$data['security_analysis_analyst'] = $this->model_basic->select_where_array_order_limit($this->tbl_security_analysis,$where_array_analyst, 'date_created', 'DESC', 3)->result();
                        $data['knowledge_base'] = $this->model_basic->select_all_limit_order($this->tbl_knowledge_base, 3,'id', 'desc')->result();
                        $data['content'] = $this->load->view('user_backend/admin/dashboard_karyawan',$data,true);
			$this->load->view('user_backend/index',$data);
                        
                    }
		}
		else
			redirect('user');
	}

	function login() {
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Admin Login','admin_login');
		$data += $this->user_get_menu();
		$this->load->view('user_backend/admin/login',$data);
	}

	function do_login() {
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$user_data = $this->model_basic->select_where($this->tbl_user,'username',$username)->row();
		if($user_data){
			if($this->encrypt->decode($user_data->password) == $password){
				$user_group = $this->model_basic->select_where($this->tbl_usergroup_user,'id',$user_data->user_group_id)->row()->usergroup_name;
				$instalasi_name = $this->model_basic->select_where($this->tbl_instalasi,'id',$user_data->instalasi_id)->row()->name;
				$data_user = array(
					'admin_id' => $user_data->id,
					'username' => $user_data->username,
					'password' => $password,
					'realname' => $user_data->realname,
					'email' => $user_data->email,
					'id_usergroup' => $user_data->user_group_id,
					'id_level' => $user_data->level_user_id,
					'name_usergroup' => $user_group,
					'photo' => $user_data->photo,
					'id_instalasi' => $user_data->instalasi_id,
					'name_instalasi' => $instalasi_name,
                                        'date_modified' => $user_data->date_modified
					);
				$this->session->set_userdata('user',$data_user);
                                $this->insert_visitory_history($user_data->id, 2);
				$this->returnJson(array('status' => 'ok','msg' => 'Login Success, you\'ll be redirect soon.','redirect' => 'user/dashboard'));
			}
			else
				$this->returnJson(array('status' => 'error','msg' => 'Login failed, Wrong password.'));
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Login failed, Username not registered.'));
	}

	function do_logout() {
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Admin Logout','admin_logout');
		$data += $this->user_get_menu();
		if($this->session->userdata('user') != FALSE){
			$this->session->unset_userdata('user');
			redirect('user');
		}
		else
			redirect('user');
	}

	function chart_jumlah_instalasi(){
		$data = $this->model_chart->jumlah_instalasi();
		$category = array();
		$series_data = array();
		foreach ($data as $d) {
			array_push($category, $d->jenis_bisnis_name);
			array_push($series_data,(float)$d->jumlah_instalasi);
		}
		echo json_encode(array('status' => 'ok','category' => $category,'series' => array('name' => 'Jumlah Instalasi','data' => $series_data)));
	}
        
        function chart_jumlah_perusahaan(){
		$data = $this->model_chart->jumlah_perusahaan();
		$category = array();
		$series_data = array();
		foreach ($data as $d) {
			array_push($category, $d->jenis_bisnis_name);
			array_push($series_data,(float)$d->jumlah_perusahaan);
		}
		echo json_encode(array('status' => 'ok','category' => $category,'series' => array('name' => 'Jumlah Perusahaan','data' => $series_data)));
	}
	
	function chart_jumlah_satpam(){
		$data = $this->model_chart->jumlah_satpam();
		$category = array();
		$series = array();
		$series_data_organik = array();
		$series_data_outsourching = array();
		foreach ($data as $d) {
			array_push($category, $d->jenis_bisnis_name);
			array_push($series_data_organik,(float)$d->jumlah_organik);
			array_push($series_data_outsourching,(float)$d->jumlah_outsourching);
		}
		array_push($series, array(
					'name' => 'Organik',
					'data' => $series_data_organik
					));
		array_push($series, array(
					'name' => 'Outsourching',
					'data' => $series_data_outsourching
					));
		echo json_encode(
			array(
				'status' => 'ok',
				'category' => $category,
				'series' => $series
				)
			);
	}

	function chart_asesment_2004(){
		$data = $this->model_basic->select_where($this->tbl_assessment2004,'instalasi_id',$this->session_user['id_instalasi'])->result();
		$data_range = $this->model_basic->select_where($this->tbl_asesmen_scoring_colour_config,'id_scoring_colour',1)->result(); 
		$emas = 0;
		$hijau = 0;
		$biru = 0;
		$merah = 0;
		$hitam = 0;
		foreach ($data as $d) {
			foreach ($data_range as $dr) {
				if($d->nilai_akhir > $dr->min && $d->nilai_akhir <= $dr->max){
					if($dr->name == 'Emas')
						$emas = $emas + 1;
					elseif($dr->name == 'Hijau')
						$hijau = $hijau + 1;
					elseif($dr->name == 'Biru')
						$biru = $biru + 1;
					elseif($dr->name == 'Merah')
						$merah = $merah + 1;
					elseif($dr->name == 'Hitam')
						$hitam = $hitam + 1;
				}
			}
		}
		$response = array(
			'nama_instalasi' => $this->session_user['name_instalasi'],
			'nilai' => array(
				'emas' => $emas,
				'hijau' => $hijau,
				'biru' => $biru,
				'merah' => $merah,
				'hitam' => $hitam,
				)
			);
		echo json_encode($response);
	}

	function chart_asesment_2015(){
		$data = $this->model_basic->select_where($this->tbl_assessment2015,'instalasi_id',$this->session_user['id_instalasi'])->result();
		$data_range = $this->model_basic->select_where($this->tbl_asesmen_scoring_colour_config,'id_scoring_colour',2)->result(); 
		$emas = 0;
		$hijau = 0;
		$biru = 0;
		$merah = 0;
		$hitam = 0;
		foreach ($data as $d) {
			foreach ($data_range as $dr) {
				if($d->nilai_akhir > $dr->min && $d->nilai_akhir <= $dr->max){
					if($dr->name == 'Emas')
						$emas = $emas + 1;
					elseif($dr->name == 'Hijau')
						$hijau = $hijau + 1;
					elseif($dr->name == 'Biru')
						$biru = $biru + 1;
					elseif($dr->name == 'Merah')
						$merah = $merah + 1;
					elseif($dr->name == 'Hitam')
						$hitam = $hitam + 1;
				}
			}
		}
		$response = array(
			'nama_instalasi' => $this->session_user['name_instalasi'],
			'nilai' => array(
				'emas' => $emas,
				'hijau' => $hijau,
				'biru' => $biru,
				'merah' => $merah,
				'hitam' => $hitam,
				)
			);
		echo json_encode($response);
	}

	function chart_hasil_asesment_2004(){
		$selected = $this->input->post('id');
		if($selected)
			$data = $this->model_basic->select_where($this->tbl_assessment2004,'id',$selected)->row();
		else
			$data = $this->model_basic->select_where_limit_order($this->tbl_assessment2004, 'instalasi_id', $this->session_user['id_instalasi'], 1,'DESC','date_created')->row();
		$data_value = $this->model_basic->select_where($this->tbl_asesmen_2004_value,'id_asesmen',$data->id)->result();
		$category = array();
		$series_data = array();
		$total_value = 0;
		foreach ($data_value as $dv) {
			array_push($series_data, (float)$dv->value);
			array_push($category, $this->model_basic->select_where($this->tbl_rules_asms_2004,'id',$dv->id_rules)->row()->name);
			$total_value = $total_value + $dv->value;
		}
		$total_value = $total_value / 4;
		array_push($series_data, (float)$total_value);
		array_push($category, 'Nilai Akhir');
		$w_na = $this->check_colour_text_dashboard(number_format($total_value,2),'Nilai Akhir (Asesmen 2004)');

		echo json_encode(
			array('status' => 'ok',
				'name' => $this->session_user['name_instalasi'],
				'category' => $category,
				'series' => array(
					'name' => 'Hasil Asesmen',
					'data' => $series_data
					),
				'warna' => array('#ADADAD','#ADADAD','#ADADAD','#ADADAD',$this->check_colour_hexa($w_na))
				)
			);
	}
	
	function chart_hasil_asesment_2015(){
		$selected = $this->input->post('id');
		if($selected)
			$data = $this->model_basic->select_where($this->tbl_assessment2015,'id',$selected)->row();
		else
			$data = $this->model_basic->select_where_limit_order($this->tbl_assessment2015, 'instalasi_id', $this->session_user['id_instalasi'], 1,'DESC','date_created')->row();
		
		$pemenuhan_system = (float)$this->model_assessment2015->sum_value($this->tbl_asesmen_2015_value,'value','id_asesmen',$data->id)->jumlah / 4;
		$security_performance = (float)$data->security_performance;
		$security_reliability = (float)($data->people + $data->device_and_infrastructure) / 2;
		$csi = (float)$data->csi;
		$nilai_akhir = (float)$data->nilai_akhir;

		$data_range = $this->model_basic->select_where($this->tbl_asesmen_scoring_colour_config,'id_scoring_colour',2)->result(); 
		foreach ($data_range as $dr) {
			if($dr->name == 'Emas')
				$emas = $dr->min;
			elseif($dr->name == 'Hijau')
				$hijau = $dr->min;
			elseif($dr->name == 'Biru')
				$biru = $dr->min;
			elseif($dr->name == 'Merah')
				$merah = $dr->min;
			elseif($dr->name == 'Hitam')
				$hitam = $dr->min;
		}
		$w_ps = $this->check_colour_text_dashboard(number_format($pemenuhan_system,2),'Pemenuhan System (Asesmen 2015)');
		$w_sp = $this->check_colour_text_dashboard(number_format($security_performance,2),'Security Performance (Asesmen 2015)');
		$w_sr = $this->check_colour_text_dashboard(number_format($security_reliability,2),'Security Reliability (Asesmen 2015)');
		$w_csi = $this->check_colour_text_dashboard(number_format($csi,2),'CSI (Asesmen 2015)');
		$semua_warna = array($w_ps,$w_sp,$w_sr,$w_csi);
		$w_na = 'emas';
		if(in_array('hijau', $semua_warna))
			$w_na = 'hijau';
		if(in_array('biru', $semua_warna))
			$w_na = 'biru';
		if(in_array('merah', $semua_warna))
			$w_na = 'merah';
		if(in_array('hitam', $semua_warna))
			$w_na = 'hitam';
		// $w_na = $this->check_colour_text_2015_dashboard(number_format($nilai_akhir,2),'Nilai Akhir (Asesmen 2015)');

		$category = array('Pemenuhan System','Security Performance','Security Reliability','CSI','Nilai Akhir');
		$series_data = array($pemenuhan_system,$security_performance,$security_reliability,$csi,$nilai_akhir);
		$plotline = array(
			'Emas' => (float)$emas,
			'Hijau' => (float)$hijau,
			'Merah' => (float)$merah,
			'Biru' => (float)$biru,
			'Hitam' => (float)$hitam
			);
		$warna = array(
			$this->check_colour_hexa($w_ps),
			$this->check_colour_hexa($w_sp),
			$this->check_colour_hexa($w_sr),
			$this->check_colour_hexa($w_csi),
			$this->check_colour_hexa($w_na)
			);

		echo json_encode(array('status' => 'ok','plotline' => $plotline,'name' => $this->session_user['name_instalasi'],'category' => $category,'series' => array('name' => 'Hasil Asesmen','data' => $series_data),'warna'=>$warna));
	}

	function chart_stc_perorangan(){
		$id_result = $this->input->post('id');
		$stc_1 = 0;
		$stc_2 = 0;
		$stc_3 = 0;
		$stc_4 = 0;
		$stc_1_counter = 0;
		$stc_2_counter = 0;
		$stc_3_counter = 0;
		$stc_4_counter = 0;
		$stc_1_total = 0;
		$stc_2_total = 0;
		$stc_3_total = 0;
		$stc_4_total = 0;
		$stc_1_percentage = 0;
		$stc_2_percentage = 0;
		$stc_3_percentage = 0;
		$stc_4_percentage = 0;
		$stc_1_lowest = 0;
		$stc_2_lowest = 0;
		$stc_3_lowest = 0;
		$stc_4_lowest = 0;
		$stc_1_highest = 0;
		$stc_2_highest = 0;
		$stc_3_highest = 0;
		$stc_4_highest = 0;
		if($id_result)
			$data_chart = $this->model_standar_kompetensi->get_detail_standar_kompetensi_by_user_id($this->session_user['admin_id'],$id_result);
		else
			$data_chart = $this->model_standar_kompetensi->get_detail_standar_kompetensi_by_user_id($this->session_user['admin_id']);
		if($data_chart->num_rows() != 0){
			$data_stc = $data_chart->result();
			foreach ($data_stc as $row) {
				// echo 'STC-'.$row->stc_no.' Score '.$row->score.'<br>';
				if($row->stc_no == 1){
					$stc_1_counter++;
					$stc_1_total = $stc_1_total + $row->score;

					if($stc_1_counter == 1){
						$stc_1_lowest = $row->score;
						$stc_1_highest = $row->score;
					}
					if($row->score > $stc_1_highest){
						$stc_1_highest = $row->score;
					}

					if($row->score < $stc_1_lowest){
						$stc_1_lowest = $row->score;
					}
				}
				if($row->stc_no == 2){
					$stc_2_counter++;
					$stc_2_total = $stc_2_total + $row->score;

					if($stc_2_counter == 1){
						$stc_2_lowest = $row->score;
						$stc_2_highest = $row->score;
					}
					if($row->score > $stc_2_highest){
						$stc_2_highest = $row->score;
					}

					if($row->score < $stc_1_lowest){
						$stc_1_lowest = $row->score;
					}
				}
				if($row->stc_no == 3){
					$stc_3_counter++;
					$stc_3_total = $stc_3_total + $row->score;

					if($stc_3_counter == 1){
						$stc_3_lowest = $row->score;
						$stc_3_highest = $row->score;
					}
					if($row->score > $stc_3_highest){
						$stc_3_highest = $row->score;
					}

					if($row->score < $stc_3_lowest){
						$stc_3_lowest = $row->score;
					}
				}
				if($row->stc_no == 4){
					$stc_4_counter++;
					$stc_4_total = $stc_4_total + $row->score;

					if($stc_4_counter == 1){
						$stc_4_lowest = $row->score;
						$stc_4_highest = $row->score;
					}
					if($row->score > $stc_4_highest){
						$stc_4_highest = $row->score;
					}

					if($row->score < $stc_4_lowest){
						$stc_4_lowest = $row->score;
					}
				}
			}
                        if($stc_1_counter != 0)
                            (float)$stc_1_percentage = ($stc_1_total / ($stc_1_counter * 4)) * 100;
                        else
                            $stc_1_percentage = 0;
                        if($stc_2_counter != 0)
                            (float)$stc_2_percentage = ($stc_2_total / ($stc_2_counter * 4)) * 100;
                        else
                            $stc_2_percentage = 0;
                        if($stc_3_counter != 0)
                            (float)$stc_3_percentage = ($stc_3_total / ($stc_3_counter * 4)) * 100;
                        else
                            $stc_3_percentage = 0;
                        if($stc_4_counter != 0)
                            (float)$stc_4_percentage = ($stc_4_total / ($stc_4_counter * 4)) * 100;
                        else
                            $stc_4_percentage = 0;
			(int)$stc_1_nilai_max = ($stc_1_counter * 4);
			(int)$stc_2_nilai_max = ($stc_2_counter * 4);
			(int)$stc_3_nilai_max = ($stc_3_counter * 4);
			(int)$stc_4_nilai_max = ($stc_4_counter * 4);
			(int)$stc_1_nilai_min = ($stc_1_counter * 1);
			(int)$stc_2_nilai_min = ($stc_2_counter * 1);
			(int)$stc_3_nilai_min = ($stc_3_counter * 1);
			(int)$stc_4_nilai_min = ($stc_4_counter * 1);
			// echo 'STC 1 = Percentage = '.$stc_1_percentage.' - Total '.$stc_1_total.' - Nilai Max '.($stc_1_counter * 4).' - Nilai Min '.($stc_1_counter * 1).' - Jumlah Data '.$stc_1_counter.' - Nilai Tertinggi '.$stc_1_highest.' - Nilai Terendah '.$stc_1_lowest.'<br>';
			// echo 'STC 2 = Percentage = '.$stc_2_percentage.' - Total '.$stc_2_total.' - Nilai Max '.($stc_2_counter * 4).' - Nilai Min '.($stc_2_counter * 1).' - Jumlah Data '.$stc_2_counter.' - Nilai Tertinggi '.$stc_2_highest.' - Nilai Terendah '.$stc_2_lowest.'<br>';
			// echo 'STC 3 = Percentage = '.$stc_3_percentage.' - Total '.$stc_3_total.' - Nilai Max '.($stc_3_counter * 4).' - Nilai Min '.($stc_3_counter * 1).' - Jumlah Data '.$stc_3_counter.' - Nilai Tertinggi '.$stc_3_highest.' - Nilai Terendah '.$stc_3_lowest.'<br>';
			// echo 'STC 4 = Percentage = '.$stc_4_percentage.' - Total '.$stc_4_total.' - Nilai Max '.($stc_4_counter * 4).' - Nilai Min '.($stc_4_counter * 1).' - Jumlah Data '.$stc_4_counter.' - Nilai Tertinggi '.$stc_4_highest.' - Nilai Terendah '.$stc_4_lowest.'<br>';
			$json_nilai = array(
				'name' => 'Perolehan Nilai',
                'data' => array($stc_1_percentage,$stc_2_percentage,$stc_3_percentage,$stc_4_percentage),
                'pointPlacement' => 'on'
                );
			$json_nilai_max = array(
				'name' => 'Nilai Maximum',
                'data' => array($stc_1_nilai_max,$stc_2_nilai_max,$stc_3_nilai_max,$stc_4_nilai_max),
                'pointPlacement' => 'on'
                );
			$json_nilai_min = array(
				'name' => 'Nilai Minimum',
                'data' => array($stc_1_nilai_min,$stc_2_nilai_min,$stc_3_nilai_min,$stc_4_nilai_min),
                'pointPlacement' => 'on'
                );
			$json_data = array(
				$json_nilai
				);
			echo json_encode(array('status' => 'ok','data' => $json_data));
		}
                else
                {
                    $json_nilai = array(
				'name' => 'Perolehan Nilai',
                'data' => array(0,0,0,0),
                'pointPlacement' => 'on'
                );
                    $json_data = array(
				$json_nilai
				);
			echo json_encode(array('status' => 'ok','data' => $json_data));
                }
	}

	function chart_stc_perinstalasi(){
		$stc_1_count = 0;
		$stc_2_count = 0;
		$stc_3_count = 0;
		$stc_4_count = 0;

		$stc_1_total = 0;
		$stc_2_total = 0;
		$stc_3_total = 0;
		$stc_4_total = 0;

		$stc_1_percentage = 0;
		$stc_2_percentage = 0;
		$stc_3_percentage = 0;
		$stc_4_percentage = 0;

		$counter = 0;
		$id_instalasi = $this->session_user['id_instalasi'];
		$data_user = $this->model_basic->select_where($this->tbl_user,'instalasi_id',$id_instalasi);
		if($data_user){
			foreach ($data_user->result() as $row) {
				$data_stc = $this->data_stc_perorangan($row->id);
				$stc_1_total = $stc_1_total + $data_stc['nilai_stc_1'];
				$stc_2_total = $stc_2_total + $data_stc['nilai_stc_2'];
				$stc_3_total = $stc_3_total + $data_stc['nilai_stc_3'];
				$stc_4_total = $stc_4_total + $data_stc['nilai_stc_4'];
				$counter++;
			}
			(float)$stc_1_percentage = ($stc_1_total / $counter);
			(float)$stc_2_percentage = ($stc_2_total / $counter);
			(float)$stc_3_percentage = ($stc_3_total / $counter);
			(float)$stc_4_percentage = ($stc_4_total / $counter);
			// echo $stc_1_total.'<br>';
			// echo $stc_2_total.'<br>';
			// echo $stc_3_total.'<br>';
			// echo $stc_4_total.'<br>';

			// echo $stc_1_percentage.'<br>';
			// echo $stc_2_percentage.'<br>';
			// echo $stc_3_percentage.'<br>';
			// echo $stc_4_percentage.'<br>';
			// echo $counter.'<br>';

			$json_nilai = array(
				'name' => 'Perolehan Nilai',
                'data' => array($stc_1_percentage,$stc_2_percentage,$stc_3_percentage,$stc_4_percentage),
                'pointPlacement' => 'on'
                );
			$json_data = array(
				$json_nilai
				);
			echo json_encode(array('status' => 'ok','data' => $json_data));
		}
	}

	function data_stc_perorangan($id_user = 0, $id_result = 0){
		$stc_1 = 0;
		$stc_2 = 0;
		$stc_3 = 0;
		$stc_4 = 0;
		$stc_1_counter = 0;
		$stc_2_counter = 0;
		$stc_3_counter = 0;
		$stc_4_counter = 0;
		$stc_1_total = 0;
		$stc_2_total = 0;
		$stc_3_total = 0;
		$stc_4_total = 0;
		(float)$stc_1_percentage = 0;
		(float)$stc_2_percentage = 0;
		(float)$stc_3_percentage = 0;
		(float)$stc_4_percentage = 0;
		$stc_1_lowest = 0;
		$stc_2_lowest = 0;
		$stc_3_lowest = 0;
		$stc_4_lowest = 0;
		$stc_1_highest = 0;
		$stc_2_highest = 0;
		$stc_3_highest = 0;
		$stc_4_highest = 0;
		if($id_result > 0)
			$data_chart = $this->model_standar_kompetensi->get_detail_standar_kompetensi_by_user_id($id_user,$id_result);
		else
			$data_chart = $this->model_standar_kompetensi->get_detail_standar_kompetensi_by_user_id($id_user);

		if($data_chart){
			$data_stc = $data_chart->result();
			foreach ($data_stc as $row) {
				// echo 'STC-'.$row->stc_no.' Score '.$row->score.'<br>';
				if($row->stc_no == 1){
					$stc_1_counter++;
					$stc_1_total = $stc_1_total + $row->score;

					if($stc_1_counter == 1){
						$stc_1_lowest = $row->score;
						$stc_1_highest = $row->score;
					}
					if($row->score > $stc_1_highest){
						$stc_1_highest = $row->score;
					}

					if($row->score < $stc_1_lowest){
						$stc_1_lowest = $row->score;
					}
				}
				if($row->stc_no == 2){
					$stc_2_counter++;
					$stc_2_total = $stc_2_total + $row->score;

					if($stc_2_counter == 1){
						$stc_2_lowest = $row->score;
						$stc_2_highest = $row->score;
					}
					if($row->score > $stc_2_highest){
						$stc_2_highest = $row->score;
					}

					if($row->score < $stc_1_lowest){
						$stc_1_lowest = $row->score;
					}
				}
				if($row->stc_no == 3){
					$stc_3_counter++;
					$stc_3_total = $stc_3_total + $row->score;

					if($stc_3_counter == 1){
						$stc_3_lowest = $row->score;
						$stc_3_highest = $row->score;
					}
					if($row->score > $stc_3_highest){
						$stc_3_highest = $row->score;
					}

					if($row->score < $stc_3_lowest){
						$stc_3_lowest = $row->score;
					}
				}
				if($row->stc_no == 4){
					$stc_4_counter++;
					$stc_4_total = $stc_4_total + $row->score;

					if($stc_4_counter == 1){
						$stc_4_lowest = $row->score;
						$stc_4_highest = $row->score;
					}
					if($row->score > $stc_4_highest){
						$stc_4_highest = $row->score;
					}

					if($row->score < $stc_4_lowest){
						$stc_4_lowest = $row->score;
					}
				}
			}
			if($stc_1_counter > 0)
				(float)$stc_1_percentage = ($stc_1_total / ($stc_1_counter * 4)) * 100;
			if($stc_2_counter > 0)
				(float)$stc_2_percentage = ($stc_2_total / ($stc_2_counter * 4)) * 100;
			if($stc_3_counter > 0)
				(float)$stc_3_percentage = ($stc_3_total / ($stc_3_counter * 4)) * 100;
			if($stc_4_counter > 0)
				(float)$stc_4_percentage = ($stc_4_total / ($stc_4_counter * 4)) * 100;
			(int)$stc_1_nilai_max = ($stc_1_counter * 4);
			(int)$stc_2_nilai_max = ($stc_2_counter * 4);
			(int)$stc_3_nilai_max = ($stc_3_counter * 4);
			(int)$stc_4_nilai_max = ($stc_4_counter * 4);
			(int)$stc_1_nilai_min = ($stc_1_counter * 1);
			(int)$stc_2_nilai_min = ($stc_2_counter * 1);
			(int)$stc_3_nilai_min = ($stc_3_counter * 1);
			(int)$stc_4_nilai_min = ($stc_4_counter * 1);
			// echo 'STC 1 = Percentage = '.$stc_1_percentage.' - Total '.$stc_1_total.' - Nilai Max '.($stc_1_counter * 4).' - Nilai Min '.($stc_1_counter * 1).' - Jumlah Data '.$stc_1_counter.' - Nilai Tertinggi '.$stc_1_highest.' - Nilai Terendah '.$stc_1_lowest.'<br>';
			// echo 'STC 2 = Percentage = '.$stc_2_percentage.' - Total '.$stc_2_total.' - Nilai Max '.($stc_2_counter * 4).' - Nilai Min '.($stc_2_counter * 1).' - Jumlah Data '.$stc_2_counter.' - Nilai Tertinggi '.$stc_2_highest.' - Nilai Terendah '.$stc_2_lowest.'<br>';
			// echo 'STC 3 = Percentage = '.$stc_3_percentage.' - Total '.$stc_3_total.' - Nilai Max '.($stc_3_counter * 4).' - Nilai Min '.($stc_3_counter * 1).' - Jumlah Data '.$stc_3_counter.' - Nilai Tertinggi '.$stc_3_highest.' - Nilai Terendah '.$stc_3_lowest.'<br>';
			// echo 'STC 4 = Percentage = '.$stc_4_percentage.' - Total '.$stc_4_total.' - Nilai Max '.($stc_4_counter * 4).' - Nilai Min '.($stc_4_counter * 1).' - Jumlah Data '.$stc_4_counter.' - Nilai Tertinggi '.$stc_4_highest.' - Nilai Terendah '.$stc_4_lowest.'<br>';
			$json_nilai = array(
				'nilai_stc_1' => $stc_1_percentage,
				'nilai_stc_2' => $stc_2_percentage,
				'nilai_stc_3' => $stc_3_percentage,
				'nilai_stc_4' => $stc_4_percentage
                );
			return $json_nilai;
		}
	}
}
