<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class user_citizen_security extends PX_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->user_check_login();
		$this->controller_attr = array('controller' => 'user_citizen_security', 'controller_name' => 'User Citizen Security', 'controller_id' => 0);
	}
	
	public function index()
	{
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function_user('Citizen Security', 'user_citizen_security');
		$data += $this->user_get_menu();
		$this->check_userakses_user($data['function_id'], ACT_READ);
		$data['content'] = $this->load->view('user_backend/admin_citizen_security/index', $data, true);
		$this->load->view('user_backend/index', $data);
	}
	
	function citizen_security_add()
	{
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function_user('Citizen Security', 'user_citizen_security');
		$data += $this->user_get_menu();
		$this->check_userakses_user($data['function_id'], ACT_READ);

		//for create ticket number
		$ticket1 = $this->model_basic->select_where_all('px_ticket_number','prefix_number','CSP')->result();
        foreach ($ticket1 as $tickets)
        {
            $ticket = array();
            $ticket['id'] = $tickets->id;
            $ticket['prefix_number'] = $tickets->prefix_number;
            $ticket['running_number'] = $tickets->running_number + 1;
            $ticket['length_number'] = $tickets->length_number;
            $ticket['row_status'] = $tickets->row_status;
            $ticket['modified_by'] = $this->session_user['email'];
            $ticket['modified_date'] = date('Y-m-d H:i:s', now());

            $ticketNumber = str_pad($ticket['running_number'], $ticket['length_number'], '0', STR_PAD_LEFT);
            $ticketNumber1 = $ticket['prefix_number'].$ticketNumber;
        }
        $newTicket =  $ticketNumber1;
        
		$insert = array();
        $insert['ticket_number'] = $newTicket;
		$insert['reporter_id'] = $this->input->post('reporter_id');
		$insert['infraction_place'] = $this->input->post('place');
		$insert['infraction_notes'] = $this->input->post('notes');
		$insert['infraction_date'] = $this->input->post('datetime');
		$insert['infraction_photo1'] = $this->input->post('picture1');
		$insert['infraction_photo2'] = $this->input->post('picture2');
		$insert['infraction_photo3'] = $this->input->post('picture3');
		$insert['date_created'] = date('Y-m-d H:i:s',now());
		$insert['delete_flag'] = 0;

		$do_insert = $this->model_basic->insert_all($this->tbl_citizen_security, $insert);
		
		if ($do_insert) {
			
			$do_update = $this->model_basic->update('px_ticket_number', $ticket, 'id', $ticket['id']);

			$recipient = array();
			$recipient['user'] = $this->session_user['email'];
			/*$recipient['admin'] = $this->model_basic->select_where_all($this->tbl_admin, 'id_usergroup', 17)->result(); // sent to admin with specific usergroup*/
			$recipient['admin'] = $this->model_maintain_email->get_email('transaksi','Citizen Security')->result();
			$recipient['id'] = $do_insert->id;
			$recipient['ticket_number'] = $do_insert->ticket_number;
			$recipient['user_username'] = $this->session_user['username'];
			$recipient['user_email'] = $this->session_user['email'];
			$recipient['user_realname'] = $this->session_user['realname'];
			$recipient['place'] = $insert['infraction_place'];
			$recipient['notes'] = $insert['infraction_notes'];
			$recipient['datetime'] = $insert['infraction_date'];

			$this->send_email_telnet($recipient);
			$this->returnJson(array('status' => 'ok', 'msg' => 'Input data success', 'redirect' => $data['controller'].'/'.$data['function']));
		} else {
			$this->returnJson(array('status' => 'error', 'msg' => 'Failed when saving data'));
		}
	}
	
}