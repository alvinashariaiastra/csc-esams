<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_my_profile extends PX_Controller {

    public function __construct() {
        parent::__construct();
        $this->user_check_login();
        $this->controller_attr = array('controller' => 'user_my_profile', 'controller_name' => 'My Profile', 'controller_id' => 0);
    }
    
    public function index()
    {
            $this->my_profile();
    }

    public function my_profile() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function_user('My Profile', 'user_my_profile');
        $data += $this->user_get_menu();
        $this->check_userakses_user($data['function_id'], ACT_READ);
        $data['data'] = $this->session_user;
        $data['content'] = $this->load->view('user_backend/my_profile/index', $data, true);
        $this->load->view('user_backend/index', $data);
    }

    function user_my_profile_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function_user('My Profile', 'user_my_profile');
        $data += $this->user_get_menu();
        $this->check_userakses_user($data['function_id'], ACT_UPDATE);
        $table_field = $this->db->list_fields($this->tbl_user);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }
        unset($update['id_usergroup']);
        unset($update['level_user_id']);
        unset($update['perusahaan_id']);
        unset($update['instalasi_id']);
        unset($update['jabatan_id']);
        unset($update['user_group_id']);
        unset($update['id_created']);
        unset($update['id_modified']);
        unset($update['date_created']);
        unset($update['delete_flag']);
        unset($update['telp']);
        unset($update['bio']);
        unset($update['address']);
        $update['date_modified'] = date('Y-m-d H:i:s', now());
        $foto = $this->input->post('photo');
        $old_foto = $this->input->post('old_photo');
        $img_name_crop = uniqid().'-userprofile.jpg';
        $password_check = $this->password_checker($this->input->post('password'));
        if($password_check->status == TRUE)
        {
            $update['password'] = $this->encrypt->encode($this->input->post('password'));

            if (!is_dir(FCPATH . "assets/uploads/user/" . $update['id']))
                mkdir(FCPATH . "assets/uploads/user/" . $update['id']);

            if (($foto && (basename($foto) != $old_foto)) || ($this->input->post('x') || $this->input->post('y') || $this->input->post('w') || $this->input->post('h')))
                $update['photo'] = $img_name_crop;
            else
                $update['photo'] = $this->input->post('old_photo');

            if ($update['realname'] && $update['email'] && $update['username'] && $this->input->post('password') && ($this->input->post('password') == $this->input->post('c-password'))) {
                $do_update = $this->model_basic->update($this->tbl_user, $update, 'id', $update['id']);
                if ($do_update) {
                    if(($foto && (basename($foto) != $old_foto)) || ($this->input->post('x') || $this->input->post('y') || $this->input->post('w') || $this->input->post('h')))
                    {
                            $origw = $this->input->post('origwidth');
                            $origh = $this->input->post('origheight');
                            $fakew = $this->input->post('fakewidth');
                            $fakeh = $this->input->post('fakeheight');
                            $x = $this->input->post('x') * $origw / $fakew;
                            $y = $this->input->post('y') * $origh / $fakeh;
                            # ambil width crop
                            $targ_w = $this->input->post('w') * $origw / $fakew;
                            # abmil heigth crop
                            $targ_h = $this->input->post('h') * $origh / $fakeh;
                            # rasio gambar crop
                            $jpeg_quality = 100;
                            if(!is_dir(FCPATH . 'assets/uploads/user/'.$update['id']))
                                    mkdir(FCPATH . 'assets/uploads/user/'.$update['id']);

                            if(basename($foto) && $foto != null)
                                    $src = $this->input->post('photo');
                            else if($this->input->post('x')||$this->input->post('y')||$this->input->post('w')||$this->input->post('h'))
                                    $src = "assets/uploads/user/".$update['id'].'/'.$old_foto;
                            # inisial handle copy gambar
                            $ext = pathinfo($src, PATHINFO_EXTENSION);

                            if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'JPG' || $ext == 'JPEG')
                                    $img_r = imagecreatefromjpeg($src);
                            if($ext == 'png' || $ext == 'PNG')
                                    $img_r = imagecreatefrompng($src);
                            if($ext == 'gif' || $ext == 'GIF')
                                    $img_r = imagecreatefromgif($src);

                            $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );
                            # simpan hasil croping pada folder lain
                            $path_img_crop = realpath(FCPATH . "assets/uploads/user/".$update['id']);
                            # nama gambar yg di crop
                            # proses copy
                            imagecopyresampled($dst_r,$img_r,0,0,$x,$y,$targ_w,$targ_h,$targ_w,$targ_h);
                            # buat gambar
                            if(imagejpeg($dst_r,$path_img_crop .'/'. $img_name_crop,$jpeg_quality))
                                    @unlink('assets/uploads/user/'.$update['id'].'/'.$this->input->post('old_photo'));
                            $this->delete_temp('temp_folder');
                    }
                    $data_user = array(
                        'admin_id' => $update['id'],
                        'username' => $update['username'],
                        'password' => $this->input->post('password'),
                        'realname' => $update['realname'],
                        'email' => $update['email'],
                        'id_usergroup' => $this->session_user['id_usergroup'],
                        'name_usergroup' => $this->session_user['name_usergroup'],
                        'photo' => $update['photo'],
                        'id_instalasi' => $this->session_user['id_instalasi'],
                        'name_instalasi' => $this->session_user['name_instalasi'],
                        'date_modified' => $update['date_modified']
                        
                    );
                    $this->session->set_userdata('user', $data_user);
                    $this->save_log_user(ACT_UPDATE, 'Update Profile');
                    $this->delete_temp('temp_folder');
                    $this->returnJson(array('status' => 'ok', 'msg' => 'Update success', 'redirect' => $data['controller'] . '/' . $data['function']));
                }
                else
                    $this->returnJson(array('status' => 'error', 'msg' => 'Failed when updating data'));
            }
            else
                $this->returnJson(array('status' => 'error', 'msg' => 'Please check the form'));
        }
        else
        {
            $this->returnJson(array('status' => 'error', 'msg' => $password_check->msg));
        }
    }
    
    function user_check_email() {
        $id = $this->input->post('id');
        $email = $this->input->post('email');
        if ($id == null || $id == '') {
            $check = $this->model_basic->select_where($this->tbl_user, 'email', $email)->num_rows();
            if ($check == 0)
                echo 'true';
            else
                echo 'false';
        }
        else {
            $now = $this->model_basic->select_where_array($this->tbl_user, 'id != ' . $id . ' and email = "' . $email . '"')->num_rows();
            if ($now == 0)
                echo 'true';
            else {
                echo 'false';
            }
        }
    }

    function user_check_username() {
        $id = $this->input->post('id');
        $username = $this->input->post('username');
        if ($id == null || $id == '') {
            $check = $this->model_basic->select_where($this->tbl_user, 'username', $username)->num_rows();
            if ($check == 0)
                echo 'true';
            else
                echo 'false';
        }
        else {
            $now = $this->model_basic->select_where_array($this->tbl_user, 'id != ' . $id . ' and username = "' . $username . '"')->num_rows();
            if ($now == 0)
                echo 'true';
            else {
                echo 'false';
            }
        }
    }
}
