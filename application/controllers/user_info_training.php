<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class user_info_training extends PX_Controller {

	public function __construct() {
		parent::__construct();
		$this->user_check_login();
		$this->controller_attr = array('controller' => 'user_info_training','controller_name' => 'Admin Info Training','controller_id' => 0);
	}

	public function index()
	{
		$this->info();
	}

	function info(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function_user('Info Training','user_info_training');
		$data += $this->user_get_menu();
		$this->check_userakses_user($data['function_id'], ACT_READ);
		$data['data'] = $this->model_basic->select_all($this->tbl_training_info);
		$data['content'] = $this->load->view('user_backend/admin_info_training/info_training',$data,true);
		$this->load->view('user_backend/index',$data); 
    }

    public function ajax_info_list()
	{
		$list = $this->model_info_training->get_datatables();

		$data = array();
		$no = $_POST['start'];
		foreach ($list as $data_row) {
			//render data
			$no++;

			//filter tanggal pendaftaran
			$today = date('d F Y');
			if ($today > date('d F Y',strtotime($data_row->pendaftaran_start)) && $today < date('d F Y',strtotime($data_row->pendaftaran_end))) {
				$btn_daftar = '<a href="admin_info_training/pendaftaran_training_form/'.$data_row->id.'"><button class="btn btn-success btn-xs btn-edit" type="button" data-original-title="Daftar" data-placement="top" data-toggle="tooltip"><i class="fa fa-edit"></i></button></a>';
			}else{
				$btn_daftar = '';
			}
			
			
			$row = array();
			$row[] = $no;
			$row[] = $data_row->title;
			$row[] = $data_row->realname;
			$row[] = date('d F Y',strtotime($data_row->date_start));
			$row[] = date('d F Y',strtotime($data_row->pendaftaran_start));
			//add html for action
			$row[] = '<div class="text-center">

						<form action="admin_info_training/info_form" method="post">
						<input type="hidden" name="id" value="'.$data_row->id.'">
						'.$btn_daftar.'
						<button class="btn btn-info btn-xs btn-edit" type="submit" data-original-title="Update" data-placement="top" data-toggle="tooltip"><i class="fa fa-edit"></i></button>
						<button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Delete" data-placement="top" data-toggle="tooltip" data-target-id="'.$data_row->id.'"><i class="fa fa-trash-o"></i></button>
						</form>
						</div>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_info_training->count_all(),
						"recordsFiltered" => $this->model_info_training->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	function get_info_training(){
		$data = $this->model_basic->select_all($this->tbl_training_info);
		$data_info_training = array();
		foreach ($data as $d) {
			$pendaftaran = array(
				'id' => $d->id,
				'title' => '(Pendaftaran) '.$d->title,
				'start' => date('Y-m-d',strtotime($d->pendaftaran_start)),
				'end' => date('Y-m-d',strtotime($d->pendaftaran_end)),
				'className' => 'blue'
				);
			$training = array(
				'id' => $d->id,
				'title' => '(Pelaksanaan Training) '.$d->title,
				'start' => date('Y-m-d',strtotime($d->date_start)),
				'end' => date('Y-m-d',strtotime($d->date_end)),
				'className' => 'green'
				);
			array_push($data_info_training,$pendaftaran);
			array_push($data_info_training,$training);
		}
		echo json_encode($data_info_training);
	}
	function check_calendar_event(){
		$id = $this->input->post('id');
		$training = $this->model_basic->select_where($this->tbl_training_info, 'id', $id)->row();

		$today = date('d F Y');
		if ($today >= date('d F Y',strtotime($training->pendaftaran_start)) && $today <= date('d F Y',strtotime($training->pendaftaran_end))) {
			$this->returnJson(array('status' => 'ok', 'redirect' => 'user_info_training/pendaftaran_training_form/'.$id));
		}else{
			$this->returnJson(array('status' => 'notok'));
		}
	}
    function pendaftaran_training_form($training_info_id){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function_user('Pendaftaran Training','info');
		$data += $this->user_get_menu();
		//$this->check_userakses_user($data['function_id'], ACT_CREATE);
		$id = $this->input->post('id');
		$data['training'] = $this->model_info_training->get_training_info_detail($training_info_id)->row();
		if($id){
			$data['data'] = $this->model_basic->select_where($this->tbl_training_pendaftaran,'id',$id)->row();
		}
		else
			$data['data'] = null;
		$data['content'] = $this->load->view('user_backend/admin_info_training/pendaftaran_training_form',$data,true);
		$this->load->view('user_backend/index',$data); 
    }
    function pendaftaran_training_add(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function_user('Pendaftaran Training','info');
		$data += $this->user_get_menu();
		$table_field = $this->db->list_fields($this->tbl_training_pendaftaran);
		$insert = array();
		foreach ($table_field as $field) {
			$insert[$field] = $this->input->post($field);
		}

		$insert['id_created'] = $this->session_user['admin_id'];
                $insert['created_type'] = 2;
		$insert['date_created'] = date('Y-m-d H:i:s',now());
		
		if($insert['name'] && $insert['email']){
			$do_insert = $this->model_basic->insert_all($this->tbl_training_pendaftaran,$insert);
			if($do_insert){
                                $training = $this->model_basic->select_where($this->tbl_training_info, 'id', $insert['training_info_id'])->row()->title;
                                $this->save_log_user(ACT_CREATE, 'Insert Pendaftaran Training '.$training);
                                $this->email_pendaftaran($do_insert->id);
				redirect('user_info_training/info?status=ok');
				// $this->send_mail($do_insert->id);
				// $this->session->set_flashdata('success', '<div class="alert alert-success"><strong>Pendaftaran Training Berhasil!!!!</strong><span></span></div>');
			}
			else{
				redirect('user_info_training/info?status=error');
				// $this->session->set_flashdata('failed', '<div class="alert alert-danger"><strong>Pendaftaran Training Gagal!!!!</strong><span></span></div>');
			}
		}
		else{
			redirect('user_info_training/info?status=error');
			// $this->session->set_flashdata('failed', '<div class="alert alert-danger"><strong>Pendaftaran Training Gagal!!!!</strong><span></span></div>');
		}
    }
    function detail_info_training($id){
    	if($id){
    		$data = $this->get_app_settings();
			$data += $this->controller_attr;
			$data += $this->get_function_user('Info Training','user_info_training');
			$data += $this->user_get_menu();
			$this->check_userakses_user($data['function_id'], ACT_READ);
			$data['info_training'] = $this->model_basic->select_where($this->tbl_training_info,'id',$id)->row();
			$data['content'] = $this->load->view('user_backend/admin_info_training/info_training_detail',$data,true);
			$this->load->view('user_backend/index',$data); 
    	}
    	else
    		redirect('user_info_training');
    }
	public function get_training_detail(){
		$results = $this->model_basic->select_where($this->tbl_training_info, 'id', $this->input->post('training_info_id'))->row();
		$results->pendaftaran_start = date('d F Y',strtotime($results->pendaftaran_start));
		$results->pendaftaran_end = date('d F Y',strtotime($results->pendaftaran_end));
		$results->date_start = date('d F Y',strtotime($results->date_start));
		$results->date_end = date('d F Y',strtotime($results->date_end));
		$this->returnJson(array('status' => 'ok', 'result' => $results));
	}
}