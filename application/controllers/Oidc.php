<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Oidc extends CI_Controller {

public function login()
	{					
		// error_log("creating new provider...");
        $provider = new \League\OAuth2\Client\Provider\GenericProvider([
            'clientId'                => 'W4jh80N7koq2b_lffO2bODWxHKEa',    // The client ID assigned to you by the provider
            'clientSecret'            => 'w5ydz1krpkqRUaZuJddGrXAbSnYa',   // The client password assigned to you by the provider
            'redirectUri'             => 'https://astranet.astra.co.id/csc/user/do_login_sso',
            'urlAuthorize'            => 'https://10.248.141.203/oauth2/authorize',
            'urlAccessToken'          => 'https://10.248.141.203/oauth2/token',
            'urlResourceOwnerDetails' => 'https://10.248.141.203/oauth2/userinfo',
            'scopes'                  => 'openid',
            'verify'                  => false
        ]);
        // print_r($provider);
        // die();
        error_log("provider created");
        // If we don't have an authorization code then get one
        if (!isset($_GET['code'])) {
            
        // Fetch the authorization URL from the provider; this returns the
        // urlAuthorize option and generates and applies any necessary parameters
        // (e.g. state).
        $authorizationUrl = $provider->getAuthorizationUrl();

        // Get the state generated for you and store it to the session.
        $_SESSION['oauth2state'] = $provider->getState();

        // Redirect the user to the authorization URL.
        header('Location: ' . $authorizationUrl);                                
        exit;

    // Check given state against previously stored one to mitigate CSRF attack
    } elseif (empty($_GET['state']) || (isset($_SESSION['oauth2state']) && $_GET['state'] !== $_SESSION['oauth2state'])) {

        if (isset($_SESSION['oauth2state'])) {
            unset($_SESSION['oauth2state']);
        }
        
        exit('Invalid state');

    } else {

        try {                
            // Try to get an access token using the authorization code grant.
            $accessToken = $provider->getAccessToken('authorization_code', [
                'code' => $_GET['code']
            ]);                

            // We have an access token, which we may use in authenticated
            // requests against the service provider's API.
            $data['access_token'] = $accessToken->getToken();
            $data['refresh_token'] = $accessToken->getRefreshToken();
            $data['token_expire_time'] = $accessToken->getExpires();
            $data['token_is_expired'] = $accessToken->hasExpired() ? true : false;                
            // Using the access token, we may look up details about the
            // resource owner.
            
            
            $resourceOwner = $provider->getResourceOwner($accessToken);                        

            $data['resource_owner'] = $resourceOwner->toArray();
            //print_r($data['resource_owner']);
            //$email = $data['resource_owner']['sub'];                                         
           // redirect('admin/do_login_sso?email='.$email);
            $this->load->view('oidc', $data);
        
        } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {

            // Failed to get the access token or user details.
            exit($e->getMessage());

        }

    }
                    
	}

}