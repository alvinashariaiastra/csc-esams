<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class user_knowledge extends PX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper('text');
		$this->user_check_login();
		$this->controller_attr = array('controller' => 'user_knowledge','controller_name' => 'User Knowledge','controller_id' => 0);
	}

	public function index()
	{
		$this->knowledge();
	}
	//knowledge//
	function knowledge()
	{
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function_user('User Knowledge','user_knowledge');
		$data += $this->user_get_menu();
		$this->check_userakses_user($data['function_id'], ACT_READ);

		$data['knowledge'] = $knowledge = $this->model_basic->select_where_array($this->tbl_knowledge_base, 'delete_flag = 0 and status = 1');
		foreach ($knowledge->result() as $row) {
			$string = strip_tags($row->description);
			$row->content = word_limiter($string,50);
			$row->date_created = date('j F Y',strtotime($row->date_created));
		}
		$data['content'] = $this->load->view('user_backend/admin_knowledge/knowledge',$data,true);
		$this->load->view('user_backend/index',$data); 
	}

	public function detail($id) {
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function_user('Detail','user_knowledge');
		$data += $this->user_get_menu();
		$this->check_userakses_user($data['function_id'], ACT_READ);
		$data['knowledge'] = $knowledge = $this->model_basic->select_where($this->tbl_knowledge_base, 'id', $id);
		foreach ($knowledge->result() as $row) {
			$row->date_created = date('j F Y',strtotime($row->date_created));
		}
		$data['content'] = $this->load->view('user_backend/admin_knowledge/detail',$data,true);
		$this->load->view('user_backend/index',$data); 
    }

	public function ajax_knowledge_list()
	{
		$column = array('name');
		$list = $this->model_table->get_datatables($this->tbl_knowledge_base,$column);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $data_row) {
			//render data
			$no++;
			
			$row = array();
			$row[] = $no;
			$row[] = $data_row->name;

			$row[] = '<a href="user_knowledge/download_file/'.$data_row->id.'/'.$data_row->file.'"><label class="btn btn-primary btn-download">Download</label></a>';
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_table->count_all($this->tbl_knowledge_base),
						"recordsFiltered" => $this->model_table->count_filtered($this->tbl_knowledge_base,$column),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
	function download_file($id,$file_nm) {
		$this->load->helper('download');
		$file = file_get_contents('assets/uploads/knowledge/'.$id.'/'.$file_nm);
		force_download($file_nm,$file);
	}
}

/* End of file admin_knowledge.php */
/* Location: ./application/controllers/admin_knowledge.php */