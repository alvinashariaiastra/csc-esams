<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Patrol extends PX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('pdf');
        $this->check_login();
        $this->controller_attr = array('controller' => 'patrol', 'controller_name' => 'Patrol', 'controller_id' => 0);
    }

    const modul = 'esams';

    /* Created: Alvin (atsalvin0017), Date: 2019-09-26 14:49 PM  
    Patrol Management
    start:: */
    function index() {
        return redirect('esams/patrol/assignment');
    }

    function assignment() {
        return $this->patrol_view('assignment');
    }

    function inprogress() {
        return $this->patrol_view('inprogress');
    }

    function report() {
        return $this->patrol_view('report');
    }

    function patrol_view($tab) {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Patrol', 'patrol');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $data['modul'] = self::modul;

        if ($tab=='assignment') {
            $where_clouse_z = 'zz.g_id = 0 AND zz.date_a >= "'.date('Y-m-d').'"';
        } elseif ($tab=='inprogress') {
            $where_clouse_z = '(zz.g_id > 0 AND zz.g_id != zz.h_id) AND zz.date_a = "'.date('Y-m-d').'"';
        } elseif ($tab=='report') {
            $where_clouse_z = '(zz.f_id = zz.h_id OR (zz.g_id = 0 AND zz.date_a != "'.date('Y-m-d').'") OR (zz.g_id > 0 AND zz.date_a != "'.date('Y-m-d').'"))';
        }

        $data['tab'] = $tab;
        // start:: Pagination
        $sql = '
            SELECT zz.* FROM (
                SELECT 
                    a.id as id, 
                    a.date as date_a, 
                    DATE_FORMAT(a.date, "%M %d, %Y") as date, 
                    d.building_name as building_name, 
                    e.name as instalasi_name, 
                    b.realname as created_by, 
                    a.created_date as created_date, 
                    c.realname as modified_by, 
                    a.modified_date as modified_date,
                    count(f.id) * count(usr.id) as f_id,
                    count(g.id) * count(usr.id) as g_id,
                    count(h.id) as h_id,
                    count(usr.id) as usr_id
                FROM 
                    px_patrol a 
                    LEFT JOIN px_admin b ON b.id = a.created_by 
                    LEFT JOIN px_admin c ON c.id = a.modified_by
                    JOIN px_patrol_user usr ON a.id = usr.patrol_id AND usr.delete_flag = 0
                    JOIN ( 
                        px_building d 
                        JOIN px_asesmen_instalasi e ON e.id = d.instalasi_id 
                    ) ON d.id = a.building_id
                    JOIN (
                        px_patrol_pattern f
                         LEFT JOIN px_patrol_checkin g ON g.patrol_pattern_id = f.id AND g.delete_flag = 0 
                         LEFT JOIN px_patrol_report h ON h.patrol_pattern_id = f.id 
                    ) ON f.patrol_id = a.id AND f.delete_flag = 0
                WHERE 
                    a.delete_flag = 0
                 GROUP BY
                    a.id, usr.user_id, g.user_id
            ) zz
            WHERE '.$where_clouse_z.'
            GROUP BY zz.id
        ';
        $columns = ['date','building_name','instalasi_name','date','created_date','modified_date'];
        $data['datas'] = $this->pagination_render($sql, $columns);
        $data['filter'] = $this->load->view('backend/esams/filter', $data, true);
        $data['paging'] = $this->load->view('backend/esams/paging', $data, true);
        // ::end
        $data['content'] = $this->load->view('backend/esams/patrol', $data, true);
        $this->load->view('backend/index', $data);
    }

    function patrol_form($act=null) {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Patrol Form', 'patrol_form');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $current_patrol_user = [];
        $id = $this->input->post('id');
        if ($_SERVER['REQUEST_METHOD'] == 'GET' and !$id) $id = $act;
        $data['data'] = ($id)?$this->model_basic->select_where($this->tbl_patrol, 'id', $id)->row():null;
        if ($data['data']) {
            $data['data']->instalasi_id = null;
            // get building
            $building = $this->model_basic->select_where($this->tbl_building, 'id', $data['data']->building_id)->row();
            if ($building) {
                // get instalasi
                $instalasi = $this->model_basic->select_where($this->tbl_instalasi, 'id', $building->instalasi_id)->row();
                if ($instalasi)
                    $data['data']->instalasi_id = $instalasi->id;
            }
            // get patrol user 
            $patrol_user = $this->model_basic->select_wheres($this->tbl_patrol_user,'patrol_id',$data['data']->id);
            foreach ($patrol_user as $k => $v) $current_patrol_user[$k] = $v->user_id;
        }
        $data['modul'] = self::modul;
        $data['instalasi'] = $this->model_basic->select_all($this->tbl_instalasi);
        $data['building'] = $data['data']?$data['data']->instalasi_id?$this->model_basic->select_where($this->tbl_building, 'instalasi_id', $data['data']->instalasi_id)->result():[]:[];
        $shift = $this->model_basic->select_all($this->tbl_shift);
        foreach ($shift as $r) {
            $r->start_time = date('H:i', strtotime($r->start_time));
            $r->end_time = date('H:i', strtotime($r->end_time));
        }
        $data['shift'] = $shift;
        $personel = $this->model_basic->select_wheres($this->tbl_user, 'level_user_id', 4);
        foreach ($personel as $r) $r->selected = in_array($r->id, $current_patrol_user)?1:0;
        $data['personel'] = $personel;
        $data['pattern'] = $this->model_basic->select_all($this->tbl_pattern);
        // Get post tab
        $tab = $this->input->post('tab');
        $data['tab'] = !empty($tab)?$tab:'assignment';
        $data['content'] = $this->load->view('backend/esams/patrol_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function patrol_form_add() { 
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Patrol','patrol');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_CREATE);
        $this->patrol_form_post($data, 'add');
    }

    function patrol_form_edit() { 
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Patrol','patrol');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_UPDATE);
        $this->patrol_form_post($data, 'edit');
    }

    function patrol_form_post($data, $action) {
        $this->form_validation->set_rules('date', 'date', 'required');
        $this->form_validation->set_rules('instalasi_id', 'instalasi_id', 'required');
        $this->form_validation->set_rules('building_id', 'building_id', 'required');
        $this->form_validation->set_rules('shift_id', 'shift_id', 'required');
        $this->form_validation->set_rules('personel[]', 'personel[]', 'required');
        $this->form_validation->set_rules('sequence[]', 'sequence[]', 'required');
        $this->form_validation->set_rules('patrol_pattern[]', 'patrol_pattern[]', 'required');
        if (!$this->form_validation->run())
            return $this->returnJson(array('status' => 'error','msg' => 'Please complete the mandatory form'));

        // Define Input POST
        $patrol_id = $this->input->post('id');
        $date = $this->input->post('date');
        $instalasi_id = $this->input->post('instalasi_id');
        $building_id = $this->input->post('building_id');
        $shift_id = $this->input->post('shift_id');
        $personel = $this->input->post('personel');
        $patrol_pattern = $this->input->post('patrol_pattern');
        $patrol_pattern_id = $this->input->post('patrol_pattern_id');
        $sequence = $this->input->post('sequence');
        $checkpoint = $this->input->post('checkpoint');

        $count_checkpoint = array_count_values($checkpoint);

        foreach ($count_checkpoint as $key => $value) {
            if ($value>1) {
                return $this->returnJson(array('status' => 'error','msg' => 'Checkpoint cannot be the same'));
            }
        }

        try {
            // DB Transaction Start
            $this->db->trans_start();

            // Insert/Update to Patrol DB
            $post_patrol = [];
            $post_patrol['building_id'] = $building_id;
            $post_patrol['shift_id'] = $shift_id;
            $post_patrol['date'] = date('Y-m-d', strtotime($date));
            if ($patrol_id) {
                $post_patrol['modified_date'] = date('Y-m-d H:i:s',now());
                $post_patrol['modified_by'] = $this->session_admin['admin_id'];
                $do_update_patrol = $this->model_basic->update($this->tbl_patrol,$post_patrol,'id',$patrol_id);
                if (!$do_update_patrol) return $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));
            } else {
                $post_patrol['created_date'] = date('Y-m-d H:i:s',now());
                $post_patrol['created_by'] = $this->session_admin['admin_id'];
                $post_patrol['delete_flag'] = 0;
                $do_insert_patrol = $this->model_basic->insert_all($this->tbl_patrol,$post_patrol);
                if (!$do_insert_patrol) return $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));
                $patrol_id = $do_insert_patrol->id;
            }

            // Get Patrol User Current
            $current_patrol_user = []; 
            $patrol_user = $this->model_basic->select_wheres($this->tbl_patrol_user,'patrol_id',$patrol_id);
            foreach ($patrol_user as $k => $v) $current_patrol_user[$v->user_id] = $v->id;
            // Delete Patrol User Current
            $do_delete_patrol_user = $this->model_basic->delete($this->tbl_patrol_user,'patrol_id',$patrol_id);
            if (!$do_delete_patrol_user) return $this->returnJson(array('status' => 'error','msg' => 'Failed delete patrol user'));
            // Insert/Update to Patrol User DB
            if ($personel) {
                for ($i=0; $i < count($personel); $i++) { 
                    $post_patrol_user = [];
                    $post_patrol_user['patrol_id'] = $patrol_id;
                    $post_patrol_user['user_id'] = $personel[$i];
                    if (!in_array($personel[$i], array_keys($current_patrol_user))) {
                        $post_patrol_user['status'] = 'active';
                        $post_patrol_user['created_date'] = date('Y-m-d H:i:s',now());
                        $post_patrol_user['created_by'] = $this->session_admin['admin_id'];
                        $post_patrol_user['delete_flag'] = 0;
                        $do_insert_patrol_user = $this->model_basic->insert_all($this->tbl_patrol_user,$post_patrol_user);
                        if (!$do_insert_patrol_user) return $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));
                    } else {
                        $post_patrol_user['modified_date'] = date('Y-m-d H:i:s',now());
                        $post_patrol_user['modified_by'] = $this->session_admin['admin_id'];
                        $post_patrol_user['delete_flag'] = 0;
                        $do_update_patrol_user = $this->model_basic->update($this->tbl_patrol_user,$post_patrol_user, 'id', $current_patrol_user[$personel[$i]]);
                        if (!$do_update_patrol_user) return $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));
                    }
                }
            } else $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));

            // Get Patrol Pattern Current
            $current_patrol_pattern = []; 
            $patrol_patterns = $this->model_basic->select_wheres($this->tbl_patrol_pattern,'patrol_id',$patrol_id);
            foreach ($patrol_patterns as $k => $v) $current_patrol_pattern[$k] = $v->id;
            // Delete Patrol Pattern Current
            $do_delete_patrol_pattern = $this->model_basic->delete($this->tbl_patrol_pattern,'patrol_id',$patrol_id);
            if (!$do_delete_patrol_pattern) return $this->returnJson(array('status' => 'error','msg' => 'Failed delete patrol pattern'));
            // Insert/Update to Patrol Pattern DB
            for ($i=0; $i < count($patrol_pattern); $i++) { 
                $post_patrol_pattern = [];
                $post_patrol_pattern['patrol_id'] = $patrol_id;
                $post_patrol_pattern['checkpoint_id'] = $checkpoint[$i];
                $post_patrol_pattern['sequence'] = $sequence[$i];
                if (!in_array($patrol_pattern_id[$i], $current_patrol_pattern)) {
                    $post_patrol_pattern['created_date'] = date('Y-m-d H:i:s',now());
                    $post_patrol_pattern['created_by'] = $this->session_admin['admin_id'];
                    $post_patrol_pattern['delete_flag'] = 0;
                    $do_insert_patrol_pattern = $this->model_basic->insert_all($this->tbl_patrol_pattern,$post_patrol_pattern);
                    if (!$do_insert_patrol_pattern) return $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));
                } else {
                    $post_patrol_pattern['modified_date'] = date('Y-m-d H:i:s',now());
                    $post_patrol_pattern['modified_by'] = $this->session_admin['admin_id'];
                    $post_patrol_pattern['delete_flag'] = 0;
                    $do_update_patrol_pattern = $this->model_basic->update($this->tbl_patrol_pattern,$post_patrol_pattern, 'id', $patrol_pattern_id[$i]);
                    if (!$do_update_patrol_pattern) return $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));
                }
            }
            
            // DB Transaction Commit
            $this->db->trans_complete();
            
            if ($action=='add') $this->save_log_admin(2, 'Insert Esams Master Data patrol');
            elseif ($action=='edit') $this->save_log_admin(3, 'Update Esams Master Data patrol');
            return $this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
        } catch (Exception $e) {
            // DB Transaction Rollback
            $this->db->trans_rollback();
            return $this->returnJson(array('status' => 'error','msg' => 'Something error'));
        }
    }

    function patrol_delete(){
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Patrol','patrol');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        try {
            // Delete patrol
            $delete_patrol = $this->model_basic->delete($this->tbl_patrol,'id',$id);
            if (!$delete_patrol) 
                return $this->returnJson(array('status' => 'error','msg' => 'Delete Patrol Failed'));
            // Delete patrol user
            $delete_patrol_user = $this->model_basic->delete($this->tbl_patrol_user,'patrol_id',$id);
            if (!$delete_patrol_user) 
                return $this->returnJson(array('status' => 'error','msg' => 'Delete Patrol User Failed'));
            // Delete patrol pattern
            $delete_patrol_pattern = $this->model_basic->delete($this->tbl_patrol_pattern,'patrol_id',$id);
            if (!$delete_patrol_pattern) 
                return $this->returnJson(array('status' => 'error','msg' => 'Delete Patrol Pattern Failed'));

            $this->save_log_admin(4, 'Delete Esams Master Data patrol');
            $this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
        } catch (Exception $e) {
            $this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
        }
    }

    function patrol_form_verify() { 
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Patrol','patrol');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_CREATE);
        $post = $this->input->post();
        $patrol_pattern_id = $post['patrol_pattern_id'];
        $reason_manual_checkin = $post['reason'];

        try {
            // DB Transaction Start
            $this->db->trans_start();

            // Update Patrol Pattern Reason
            $update = [];
            $update['reason_manual_checkin'] = $reason_manual_checkin;
            $update['modified_date'] = date('Y-m-d H:i:s',now());
            $update['modified_by'] = $this->session_admin['admin_id'];
            $do_update = $this->model_basic->update($this->tbl_patrol_pattern,$update,'id',$patrol_pattern_id);
            if (!$do_update) return $this->returnJson(array('status' => 'error','msg' => 'Failed update patrol pattern reason'));

            // Get Patrol Pattern
            $patrol_pattern = $this->model_basic->select_where($this->tbl_patrol_pattern,'id',$patrol_pattern_id)->row();
            if (!$patrol_pattern)
                return $this->returnJson(array('status' => 'error','msg' => 'Patrol Pattern not Found'));
            $patrol_id = $patrol_pattern->patrol_id;
            // Get Patrol User
            $patrol_user = $this->model_basic->select_where($this->tbl_patrol_user,'patrol_id',$patrol_id)->result();
            foreach ($patrol_user as $k => $v) {
                // get patrol checkin current
                $where = [];
                $where['patrol_pattern_id'] = $patrol_pattern_id;
                $where['user_id'] = $v->user_id;
                $patrol_checkin = $this->model_basic->select_where_array($this->tbl_patrol_checkin, $where)->row();
                if (!$patrol_checkin) {
                    // Insert to patrol checkin
                    $insert = [];
                    $insert['patrol_pattern_id'] = $patrol_pattern_id;
                    $insert['user_id'] = $v->user_id;
                    $insert['image_name'] = '';
                    $insert['checkin_type'] = 'manual';
                    $insert['status'] = 1;
                    $insert['created_by'] = $this->session_admin['admin_id'];
                    $insert['created_date'] = date('Y-m-d H:i:s');
                    $insert['delete_flag'] = 0;
                    $do_insert = $this->model_basic->insert_all($this->tbl_patrol_checkin,$insert);
                    if (!$do_insert)
                        return $this->returnJson(array('status' => 'error','msg' => 'Failed insert checkin'));
                } else {
                    // Update to patrol checkin
                    $update = [];
                    $update['status'] = 1;
                    $do_update = $this->model_basic->update($this->tbl_patrol_checkin,$update,'id',$patrol_checkin->id);
                    if (!$do_update)
                        return $this->returnJson(array('status' => 'error','msg' => 'Failed update checkin'));
                }
            }

            // DB Transaction Commit
            $this->db->trans_complete();
            
            return $this->returnJson(array('status' => 'ok','msg' => 'Process data success'));
        } catch (Exception $e) {
            // DB Transaction Rollback
            $this->db->trans_rollback();
            return $this->returnJson(array('status' => 'error','msg' => 'Something error'));
        }
    }

    function patrol_report($act=null) {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Patrol Report', 'patrol_report');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        
        $current_patrol_user = [];
        $id = $this->input->post('id');
        if ($_SERVER['REQUEST_METHOD'] == 'GET' and !$id) $id = $act;
        $data_report = $this->get_patrol_report($id);
        // echo json_encode($data_report); die();
        $data['data'] = $data_report;
        $data['patrol_id'] = $id;
        $data['modul'] = self::modul;
        $data['content'] = $this->load->view('backend/esams/patrol_report', $data, true);
        $this->load->view('backend/index', $data);
    }

    public function patrol_report_pdf($userid,$patrolid){
        $data_patrol = $this->get_patrol_report($patrolid,$userid);
        $data['data'] = $data_patrol;

        // return $this->load->view('backend/esams/patrol_report_pdf',$data); die();
        $this->load->view('backend/esams/patrol_report_pdf',$data);
        $html = $this->output->get_output();

        $this->load->library('Dom_pdf');
        $this->dompdf->set_option('isRemoteEnabled', true);

        $date = date('Y-m-d');
        $username = $data_patrol[0]["user_name"];

        // Convert to PDF
        $this->dompdf->load_html($html);      
        $this->dompdf->setPaper('A4', 'potrait');  
        $this->dompdf->render();
        $this->dompdf->stream("ReportPatrol_".$date."_".$patrolid."_".$username.".pdf");
    }

    private function get_patrol_report($id,$userid=null){
        $data_report = [];
        $patrol = ($id)?$this->model_basic->select_where($this->tbl_patrol, 'id', $id)->row():null;
        if ($patrol) {
            // get patrol user 
            if($userid){
                $where = [];
                $where['patrol_id'] = $id;
                $where['user_id'] = $userid;
                $patrol_user = $this->model_basic->select_where_array($this->tbl_patrol_user,$where)->result();
            }
            else{
                $patrol_user = $this->model_basic->select_wheres($this->tbl_patrol_user,'patrol_id',$patrol->id);
            }
            foreach ($patrol_user as $k => $v) {
                $user = $this->model_basic->select_where($this->tbl_user,'id',$v->user_id)->row();
                // $current_patrol_user[$k] = $user->realname;

                $data_report[$k]['user_id'] = $user->id;
                $data_report[$k]['user_name'] = $user->realname;
                $data_report[$k]['header']['personel'] = $user->realname;
                // convert date
                $data_report[$k]['header']['date'] = date('d/m/Y', strtotime($patrol->date));
                // get shift
                $shift = $this->model_basic->select_where($this->tbl_shift,'id',$patrol->shift_id)->row();
                $data_report[$k]['header']['shift']['name'] = $shift->shift_name;
                $data_report[$k]['header']['shift']['start_time'] = date('H:i', strtotime($shift->start_time));
                $data_report[$k]['header']['shift']['end_time'] = date('H:i', strtotime($shift->end_time));
                // get building & instalasi
                $instalasi = null;
                $building = $this->model_basic->select_where($this->tbl_building, 'id', $patrol->building_id)->row();
                if ($building)
                    $instalasi = $this->model_basic->select_where($this->tbl_instalasi, 'id', $building->instalasi_id)->row();
                $data_report[$k]['header']['location'] = $instalasi?$instalasi->name:null;
                $data_report[$k]['header']['building'] = $building?$building->building_name:null;
                $checklist_patrol = [];
                $detail_patrol = [];
                // Get patrol pattern
                $patrol_pattern = $this->model_basic->select_where($this->tbl_patrol_pattern,'patrol_id',$patrol->id)->result();
                foreach ($patrol_pattern as $i => $r) {
                    $checklist_patrol[$i]['checklist'] = [];
                    $detail_patrol[$i]['checkin'] = null;
                    $detail_patrol[$i]['checklist'] = [];
                    $detail_patrol[$i]['description'] = null;
                    $detail_patrol[$i]['images_patrol'] = null;
                    $detail_patrol[$i]['admin_notes'] = null;
                    // get checkpoint
                    $checkpoint = $this->model_basic->select_where($this->tbl_checkpoint,'id',$r->checkpoint_id)->row();
                    $checklist_patrol[$i]['checkpoint_name'] = $checkpoint->checkpoint_name;
                    // Get patrol report
                    $where = [];
                    $where['patrol_pattern_id'] = $r->id;
                    $where['user_id'] = $v->user_id;
                    $patrol_report = $this->model_basic->select_where_array($this->tbl_patrol_report,$where)->row();
                    $detail_patrol[$i]['checkpoint_name'] = $checkpoint->checkpoint_name;
                    if (!$patrol_report) continue;
                    // get checkpoint
                    $where = [];
                    $where['patrol_report_id'] = $patrol_report->id;
                    // $where['checkpoint_id'] = $r->checkpoint_id;
                    $patrol_report_checkpoint = $this->model_basic->select_where_array($this->tbl_patrol_report_checkpoint,$where)->result();
                    foreach ($patrol_report_checkpoint as $j => $l) {
                        // get checklist
                        $checklist = $this->model_basic->select_where($this->tbl_checklist,'id',$l->checklist_id)->row();
                        $checklist_patrol[$i]['checklist'][$j]['name'] = $checklist->checklist_name;
                        $checklist_patrol[$i]['checklist'][$j]['value'] = $l->value;
                    }
                    // get patrol checkin
                    $where = [];
                    $where['patrol_pattern_id'] = $r->id;
                    $where['user_id'] = $v->user_id;
                    $patrol_checkin = $this->model_basic->select_where_array($this->tbl_patrol_checkin,$where)->row();
                    if (!$patrol_checkin) continue;
                    $detail_patrol[$i]['checkin'] = [];
                    $detail_patrol[$i]['checkin']['checkpoint_name'] = $checkpoint->checkpoint_name;
                    $detail_patrol[$i]['checkin']['verification_time'] = date('d M Y (H:i:s)', strtotime($patrol_checkin->created_date));
                    $detail_patrol[$i]['checkin']['verification_method'] = ucwords($patrol_checkin->checkin_type);
                    $detail_patrol[$i]['checklist'] = $checklist_patrol[$i]['checklist'];
                    $detail_patrol[$i]['description'] = $patrol_report->report_text;
                    $images = [];
                    // Get patrol report image
                    $patrol_report_attachment = $this->model_basic->select_where($this->tbl_patrol_report_attachment,'patrol_report_id',$patrol_report->id)->result();
                    for ($ii=0; $ii < 3; $ii++) { 
                        $images[$ii] = isset($patrol_report_attachment[$ii])?base_url().'assets/uploads/esams/patrol/'.$patrol_report_attachment[$ii]->image_name:null;
                    }
                    $images[3] = $patrol_checkin->image_name?base_url().'assets/uploads/esams/patrol/'.$patrol_checkin->image_name:null;
                    $detail_patrol[$i]['images_patrol'] = $images;
                    $detail_patrol[$i]['admin_notes'] = $r->reason_manual_checkin;
                }
                $data_report[$k]['checklist_patrol'] = $checklist_patrol;
                $data_report[$k]['detail_patrol'] = $detail_patrol;
            }
        }
        return $data_report;
    }

    function get_patrol_pattern($patrol_id=null) {
        $patrol_pattern = $patrol_id?$this->model_basic->select_where($this->tbl_patrol_pattern, 'patrol_id', $patrol_id)->result():[];
        return $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($patrol_pattern));
    }
    /* end:: */

}
