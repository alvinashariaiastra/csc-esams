<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Issue extends PX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('pdf');
        $this->check_login();
        $this->controller_attr = array('controller' => 'issue', 'controller_name' => 'Issue', 'controller_id' => 0);
    }

    const modul = 'esams';

    /* Created: Alvin (atsalvin0017), Date: 2019-09-26 14:49 PM  
    Issue Management
    start:: */
    function index() {
        return redirect('esams/issue/assignment');
    }

    function assignment() {
        return $this->issue_view('assignment');
    }

    function inprogress() {
        return $this->issue_view('inprogress');
    }

    function completed() {
        return $this->issue_view('completed');
    }

    function issue_view($tab) {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Issue', 'issue');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $data['modul'] = self::modul;

        if ($tab=='assignment') {
            $where_clouse_z = 'having count(b.id) = 0';
            $where = 'a.action IS NULL AND a.status = "active" AND a.delete_flag = 0';
        } elseif ($tab=='inprogress') {
            $where_clouse_z = 'having count(b.id) >= 0';
            $where = 'a.action IS NOT NULL AND a.status = "active" AND a.delete_flag = 0';
        } elseif ($tab=='completed') {
            $where_clouse_z = 'having count(b.id) >= 0';
            $where = 'a.action IS NOT NULL AND a.status = "done" AND a.delete_flag = 0';
        }

        $data['tab'] = $tab;
        // start:: Pagination
        $sql = '
            SELECT
                a.id as id, 
                a.ticket_number as ticket_number,
                a.building_id as building_id,
                d.name as instalasi_name,
                c.building_name as building_name,
                a.floor as floor, 
                a.area as area, 
                a.created_date as created_date,
                e.realname as created_by
            FROM 
                px_issue as a
            JOIN
                px_user as e on e.id = a.created_by
            LEFT JOIN 
                px_issue_assign as b on a.id = b.issue_id
            JOIN 
                (
                    px_building as c 
                    JOIN 
                    px_asesmen_instalasi as d on d.id = c.instalasi_id
                )on c.id = a.building_id
            where '.$where.'
            group by a.id
            '.$where_clouse_z.'
        ';
        $columns = ['instalasi_name','building_name','floor','area','created_date'];
        $data['datas'] = $this->pagination_render($sql, $columns);
        //echo json_encode($data['datas']); die();
        $data['filter'] = $this->load->view('backend/esams/filter', $data, true);
        $data['paging'] = $this->load->view('backend/esams/paging', $data, true);
        // ::end
        $data['content'] = $this->load->view('backend/esams/issue', $data, true);
        $this->load->view('backend/index', $data);
    }

    function issue_form($act=null) {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Issue Form', 'issue_form');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $id = $this->input->post('id');
        if ($_SERVER['REQUEST_METHOD'] == 'GET' and !$id) $id = $act;
        //echo($id); die();

        //get data issue
        $data['data'] = ($id)?$this->model_basic->select_where($this->tbl_issue, 'id', $id)->row():null;
        //get data building
        $data['building'] = ($id)?$this->model_basic->select_where($this->tbl_building, 'id', $data['data']->building_id)->row():null;
        //get data instalasi
        $data['instalasi'] = ($id)?$this->model_basic->select_where($this->tbl_instalasi, 'id', $data['building']->instalasi_id)->row():null;

        //get data attachment
        $data['attachment'] = $this->model_basic->select_wheres($this->tbl_issue_attachment, 'issue_id', $data['data']->id);

        //get data user
        $data['user'] = ($id)?$this->model_basic->select_where($this->tbl_user, 'id', $data['data']->created_by)->row():null;

        // Get Member current issue_assign
        $current_members = []; 
        $issue_assign_id = [];
        if ($id) {
            $issue_assign = $this->model_basic->select_wheres($this->tbl_issue_assign, 'issue_id', $id);
            if ($issue_assign) {
                //echo json_encode($issue_assign); die();
                $get_data_report = [];
                //$get_attachment = [];
                foreach ($issue_assign as $v) {
                    $current_members[] = $v->user_id;
                    $issue_assign_id = $v->id;

                    $sql = '
                    SELECT a.*, b.realname 
                    FROM px_issue_report a
                    JOIN px_user b on b.id = a.created_by
                    WHERE a.issue_assign_id ='.$v->id;

                    $data_report = $this->model_basic->select_row_by_query($sql);
                    if (!$data_report) continue;
                    $data_report->attachment = $this->model_basic->select_wheres($this->tbl_issue_report_attachment, 'issue_report_id', $data_report->id);

                    $get_data_report[] = $data_report;                    
                    
                }
                //echo json_encode($get_data_report); die();
                if ($get_data_report) {
                    $data['issue_report'] = $get_data_report;
                }
                else
                {
                    $data['issue_report'] = null;
                }
                //echo json_encode($data['issue_report']); die();
            }
            else{
                $data['issue_report'] = null;
            }
        }
        $member = $this->model_basic->select_wheres($this->tbl_user, "level_user_id", 4);
        // Set selected for current member group detail
        foreach ($member as $k => $v) $v->selected = in_array($v->id, $current_members)?1:0;
        $data['personel_security'] = $member;
        
        $data['modul'] = self::modul;
        
        // Get post tab
        $tab = $this->input->post('tab');
        $data['tab'] = !empty($tab)?$tab:'assignment';
        $data['content'] = $this->load->view('backend/esams/issue_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function issue_form_edit(){
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Issue', 'issue');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_UPDATE);
        $issue_id = $this->input->post('id');
        $tab = $this->input->post('tab');
        $members = $this->input->post('member');

        //update action issue
        $update = array();
        $update['action'] = $this->input->post('select_action');
        $update['modified_by'] = $this->session_admin['admin_id'];
        $update['modified_date'] = date('Y-m-d H:i:s',now());
        $update['delete_flag'] = 0;
        // do update member (change delete flag 0)
        $this->model_basic->update($this->tbl_issue,$update,'id',$issue_id);

        // Get current data personel by issue_id
        $current_personel = []; 
        $issue_assign = $this->model_basic->select_wheres($this->tbl_issue_assign, 'issue_id', $issue_id);
        foreach ($issue_assign as $v) $current_personel[$v->user_id] = $v->id;
        try {
            // DB Transaction Start
            $this->db->trans_start();
            // Delete data current (change delete flag 1)
            $do_delete = $this->model_basic->delete($this->tbl_issue_assign,'issue_id',$issue_id);
            $table_field = $this->db->list_fields($this->tbl_issue_assign);
            if ($members) {
                foreach ($members as $k => $v) {
                    if (!in_array($v, array_keys($current_personel))) { // check member existing
                        $insert = array();
                        $insert['issue_id'] = $issue_id;
                        $insert['user_id'] = $v;
                        $insert['status'] = 'active';
                        $insert['created_date'] = date('Y-m-d H:i:s',now());
                        $insert['created_by'] = $this->session_admin['admin_id'];
                        $insert['modified_date'] = null;
                        $insert['modified_by'] = null;
                        $insert['delete_flag'] = 0;
                        // do insert member
                        $do_insert = $this->model_basic->insert_all($this->tbl_issue_assign,$insert);
                    } else {
                        $update = array();
                        $update['id'] = $current_personel[$v];
                        $update['modified_by'] = $this->session_admin['admin_id'];
                        $update['modified_date'] = date('Y-m-d H:i:s',now());
                        $update['delete_flag'] = 0;
                        // do update member (change delete flag 0)
                        $this->model_basic->update($this->tbl_issue_assign,$update,'id',$update['id']);
                    }
                }
            }
            // DB Transaction Commit
            $this->db->trans_complete();
            $this->save_log_admin(3, 'Update Esams Issue');
            /*$this->returnJson(array('status' => 'ok','msg' => 'Update data success','redirect' => self::modul.'/'.$data['controller'].'/completed'));*/
            $this->returnJson(array('status' => 'ok','msg' => 'Update data success','redirect' => self::modul.'/'.$data['controller'].'/'.$tab));

        } catch (Exception $e) {
            // DB Transaction Rollback
            $this->db->trans_rollback();
            $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));
        }
        
    }

    function issue_form_close_ticket(){
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Issue Completed', 'completed');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_UPDATE);
        $post = $this->input->post();
        $issue_id = $post['issue_id'];
        $note_admin = $post['reason'];

        /*echo($issue_id);
        echo($note_admin); die();*/

        //update action issue
        $update = array();
        $update['status'] = "done";
        $update['note_admin'] = $note_admin;
        $update['modified_by'] = $this->session_admin['admin_id'];
        $update['modified_date'] = date('Y-m-d H:i:s',now());
        $update['delete_flag'] = 0;
        // do update status (change staus done)
        $update_status = $this->model_basic->update($this->tbl_issue,$update,'id',$issue_id);

        if ($update_status) {
            $this->save_log_admin(3, 'Update Esams status issue done');
            $this->returnJson(array('status' => 'ok','msg' => 'Update data success','redirect' => self::modul.'/'.$data['controller'].'/completed'));
        } else{
            $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));
        }
        
    }

    function issue_report($act=null) {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Issue Report', 'issue_report');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        
        $current_issue_user = [];
        $id = $this->input->post('id');
        if ($_SERVER['REQUEST_METHOD'] == 'GET' and !$id) $id = $act;
        $data_report = $this->get_issue_report($id);
        //echo json_encode($data_report); die();
        $data['data'] = $data_report;
        $data['issue_id'] = $id;
        $data['modul'] = self::modul;
        $data['content'] = $this->load->view('backend/esams/issue_report', $data, true);
        $this->load->view('backend/index', $data);
    }

    function get_issue_report_pdf($issue_id) {
        $data['data'] = $this->get_issue_report($issue_id);
        //echo json_encode($data['data']['detail'][0]['ticket_number']); die();

        //return $this->load->view('backend/esams/issue_report_pdf',$data); die();
        $this->load->view('backend/esams/issue_report_pdf',$data);
        $html = $this->output->get_output();

        $this->load->library('Dom_pdf');
        $this->dompdf->set_option('isRemoteEnabled', true);

        $date = date('Y-m-d');
        $ticket_number = $data['data']['detail'][0]['ticket_number'];
        // Convert to PDF
        $this->dompdf->load_html($html);     
        $this->dompdf->setPaper('A4', 'potrait');    
        $this->dompdf->render();
        $this->dompdf->stream("ReportIssue_".$ticket_number."_".$date.".pdf");
    }

    private function get_issue_report($issue_id){
        $data_report = [];
        //get issue
        $issue = $this->model_basic->select_where($this->tbl_issue, 'id', $issue_id)->row();
        //get user by issue created_by
        $user = $this->model_basic->select_where($this->tbl_user,'id',$issue->created_by)->row();
        // get report type
        $issue_report_type = $this->model_basic->select_where($this->tbl_issue_report_type,'id',$issue->issue_report_type_id)->row();
        // get building
        $building = $this->model_basic->select_where($this->tbl_building,'id',$issue->building_id)->row();
        // get instalasi
        $instalasi = $building?$this->model_basic->select_where($this->tbl_instalasi,'id',$building->instalasi_id)->row():null;

        //echo json_encode($issue_report_type); die();
        if (!$issue) return $data_report;
        // get issue user
        $issue_assign = $this->model_basic->select_wheres($this->tbl_issue_assign,'issue_id',$issue->id);
        if ($issue_assign) {
            foreach ($issue_assign as $k => $v) {
                // get user detail
                $user_report = $this->model_basic->select_where($this->tbl_user,'id',$v->user_id)->row();
                $data_report['detail'][$k]['ticket_number'] = $issue->ticket_number;
                $data_report['detail'][$k]['user_id'] = $v->user_id;
                $data_report['detail'][$k]['personel'] = $user_report->realname;
                // get report per user
                $issue_report = $this->model_basic->select_where($this->tbl_issue_report,'issue_assign_id',$v->id)->row();
                if (!$issue_report) continue;
                $data_report['detail'][$k]['report_date'] = date('d/m/Y H:i', strtotime($issue_report->created_date));
                
                $data_report['detail'][$k]['investigation_results'] = $issue_report->report_text;
                // get report attachment
                $issue_report_images = [];
                $issue_report_attachment = $this->model_basic->select_where($this->tbl_issue_report_attachment,'issue_report_id',$issue_report->id)->result();
                for ($i=0; $i < 3; $i++) { 
                    $issue_report_images[$i] = isset($issue_report_attachment[$i])?base_url().'assets/uploads/esams/issue/'.$issue_report_attachment[$i]->image_name:null;
                }
                $data_report['detail'][$k]['issue_report_images'] = $issue_report_images;
            }
        }
        else{
            $data_report['detail'] = null;
        }
        
        $data_report['ticket_number'] = $issue->ticket_number;
        $data_report['nama_pelapor'] = $user->realname;
        $data_report['created_date'] = date('d/m/Y H:i', strtotime($issue->created_date));
        $data_report['report_type'] = $issue_report_type->name;
        $data_report['lokasi'] = $instalasi?$instalasi->name:null;
        $data_report['building'] = $building?$building->building_name:null;
        $data_report['floor'] = $issue->floor;
        $data_report['area'] = $issue->area;
        $data_report['fakta'] = $issue->fact;
        $data_report['analysis'] = $issue->analysis;
        $data_report['summary'] = $issue->summary;
        $data_report['note_admin'] = $issue->note_admin;
        // get issue attachment
        $issue_attachment = [];
        $issue_attachment = $this->model_basic->select_where($this->tbl_issue_attachment,'issue_id',$issue->id)->result();
        for ($i=0; $i < 3; $i++) { 
            $issue_attachment[$i] = isset($issue_attachment[$i])?base_url().'assets/uploads/esams/issue/'.$issue_attachment[$i]->image_name:null;
        }
        $data_report['issue_attachment'] = $issue_attachment;
        return $data_report;
    }

}
