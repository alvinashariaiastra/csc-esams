<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Assignment extends PX_Controller {

    public function __construct() {
        parent::__construct();
        $this->check_login();
        $this->controller_attr = array('controller' => 'assignment', 'controller_name' => 'Assignment', 'controller_id' => 0);
    }

    const modul = 'esams';

    function index() {
        return redirect('esams/assignment/assignment_task');
    }

    function assignment_task() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Assignment', 'assignment');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $data['modul'] = self::modul;
        $data['tab'] = 'assignment';
        // start:: Pagination
        $sql = '
            SELECT 
                a.id as id, 
                a.assignment_title as assignment_title,
                a.assignment_type as assignment_type,
                b.realname as created_by, 
                a.created_date as created_date,
                c.realname as modified_by, 
                a.modified_date as modified_date,
                if (e.group_security_id is null, count(e.id), 0) as total_people,
                if (e.group_security_id is not null,
                (
                    select 
                        count(id) 
                    from 
                        px_group_security_detail sa 
                    where 
                        sa.delete_flag = 0 
                        AND sa.group_security_id = e.group_security_id
                ), 0) as total_people_group,
                (
                    select 
                        count(sc.id)
                    from
                        px_assignment_detail sb
                        left JOIN px_assignment_people_checkin sc on sc.assignment_detail_id = sb.id AND sc.delete_flag = 0
                    where
                        sb.delete_flag = 0 
                        AND sb.assignment_id = a.id
                ) as total_people_checkin,
                (
                    select 
                        count(se.id)
                    from
                        px_assignment_detail sd
                        left JOIN px_assignment_report se on se.assignment_detail_id = sd.id AND se.delete_flag = 0
                    where
                        sd.delete_flag = 0 
                        AND sd.assignment_id = a.id
                ) as total_people_report,
                (
                    select 
                        count(sf.id)
                    from
                        px_assignment_detail sf
                    where
                        sf.delete_flag = 0 
                        AND sf.assignment_id = a.id
                ) as total_assignment,
                (
                    select 
                        aa.end_time
                    FROM 
                        px_assignment_detail aa
                        LEFT JOIN px_assignment ab on ab.id = aa.assignment_id
                    where 
                        aa.assignment_id = a.id
                    order by 
                        aa.end_time desc limit 1
                ) as end_time,
                (
                    select 
                        aa.start_time
                    FROM 
                        px_assignment_detail aa
                        LEFT JOIN px_assignment ab on ab.id = aa.assignment_id
                    where 
                        aa.assignment_id = a.id
                    order by 
                        aa.start_time asc limit 1
                ) as start_time
            FROM 
                px_assignment a 
                LEFT JOIN px_admin b ON b.id = a.created_by 
                LEFT JOIN px_admin c ON c.id = a.modified_by  
                JOIN px_assignment_people e on e.assignment_id = a.id AND e.delete_flag = 0
            WHERE 
                a.delete_flag = 0
            GROUP BY 
                a.id
            HAVING
                total_people_checkin = 0 
                AND end_time > CURRENT_TIMESTAMP()
        ';
        $columns = ['assignment_title','assignment_type','created_date','modified_date','start_time','end_time'];
        $data['datas'] = $this->pagination_render($sql, $columns);
        //get personel
        foreach ($data['datas']['data'] as $key => $value) {
            $personel = [];
            $assignment_people = $this->model_basic->select_wheres($this->tbl_assignment_people, 'assignment_id', $value->id);
            foreach ($assignment_people as $k => $v) {
                if ($v->user_id) {
                    $user = $this->model_basic->select_where($this->tbl_user, 'id', $v->user_id)->row();
                    $personel[$k] = $user->realname;
                }
                else{
                    $group = $this->model_basic->select_where($this->tbl_group_security, 'id', $v->group_security_id)->row();
                    $personel[$k] = $group->groupsecurity_name;
                }

            }
            $value->personel = $personel;
        }
        //echo json_encode($data['datas']); die();
        $data['filter'] = $this->load->view('backend/esams/filter', $data, true);
        $data['paging'] = $this->load->view('backend/esams/paging', $data, true);
        // ::end
        $data['content'] = $this->load->view('backend/esams/assignment', $data, true);
        $this->load->view('backend/index', $data);
    }

    function inprogress() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Assignment', 'assignment');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $data['modul'] = self::modul;
        $data['tab'] = 'inprogress';
        // start:: Pagination
        $sql = '
            SELECT zzz.* FROM (
                SELECT 
                    zz.id as id,
                    zz.assignment_title as assignment_title,
                    zz.assignment_type as assignment_type,
                    zz.start_time as start_time,
                    zz.end_time as end_time,
                    zz.created_by as created_by,
                    zz.created_date as created_date,
                    zz.modified_by as modified_by,
                    zz.modified_date as modified_date,
                    if (zz.total_people>0, zz.total_people, zz.total_people_group) as total_people,
                    zz.total_people_checkin as total_people_checkin,
                    zz.total_people_report as total_people_report,
                    if (zz.total_people>0, zz.total_people*zz.total_assignment, zz.total_people_group*zz.total_assignment) as total_report_assignment
                FROM (
                    SELECT 
                        a.id as id, 
                        a.assignment_title as assignment_title,
                        a.assignment_type as assignment_type,
                        b.realname as created_by, 
                        a.created_date as created_date, 
                        c.realname as modified_by, 
                        a.modified_date as modified_date,
                        if (e.group_security_id is null, count(e.id), 0) as total_people,
                        if (e.group_security_id is not null,
                        (
                            select 
                                count(id) 
                            from 
                                px_group_security_detail sa 
                            where 
                                sa.delete_flag = 0 
                                AND sa.group_security_id = e.group_security_id
                        ), 0) as total_people_group,
                        (
                            select 
                                count(sc.id)
                            from
                                px_assignment_detail sb
                                left JOIN px_assignment_people_checkin sc on sc.assignment_detail_id = sb.id AND sc.delete_flag = 0
                            where
                                sb.delete_flag = 0 
                                AND sb.assignment_id = a.id
                        ) as total_people_checkin,
                        (
                            select 
                                count(se.id)
                            from
                                px_assignment_detail sd
                                left JOIN px_assignment_report se on se.assignment_detail_id = sd.id AND se.delete_flag = 0
                            where
                                sd.delete_flag = 0 
                                AND sd.assignment_id = a.id
                        ) as total_people_report,
                        (
                            select 
                                count(sf.id)
                            from
                                px_assignment_detail sf
                            where
                                sf.delete_flag = 0 
                                AND sf.assignment_id = a.id
                        ) as total_assignment,
                        (
                            select 
                                aa.end_time
                            FROM 
                                px_assignment_detail aa
                                LEFT JOIN px_assignment ab on ab.id = aa.assignment_id
                            where 
                                aa.assignment_id = a.id
                            order by 
                                aa.end_time desc limit 1
                        ) as end_time,
                        (
                            select 
                                aa.start_time
                            FROM 
                                px_assignment_detail aa
                                LEFT JOIN px_assignment ab on ab.id = aa.assignment_id
                            where 
                                aa.assignment_id = a.id
                            order by 
                                aa.start_time asc limit 1
                        ) as start_time
                    FROM 
                        px_assignment a 
                        LEFT JOIN px_admin b ON b.id = a.created_by 
                        LEFT JOIN px_admin c ON c.id = a.modified_by  
                        JOIN px_assignment_people e on e.assignment_id = a.id AND e.delete_flag = 0
                    WHERE 
                        a.delete_flag = 0
                    GROUP BY 
                        a.id
                ) zz
            ) zzz
            WHERE
                 1=1
                -- AND total_people_checkin = 0 AND end_time > CURRENT_TIMESTAMP() -- view
                 AND total_people_checkin > 0 AND (total_people_report < total_report_assignment) AND end_time > CURRENT_TIMESTAMP() -- inprogress
                -- AND total_people_checkin > 0 AND total_people = total_people_checkin AND total_people_checkin = total_people_report OR end_time < CURRENT_TIMESTAMP() -- completed new
            GROUP BY zzz.id DESC
        ';
        $columns = ['assignment_title','assignment_type','created_date','modified_date','start_time','end_time'];
        $data['datas'] = $this->pagination_render($sql, $columns);
        //get personel
        foreach ($data['datas']['data'] as $key => $value) {
            $personel = [];
            $assignment_people = $this->model_basic->select_wheres($this->tbl_assignment_people, 'assignment_id', $value->id);
            foreach ($assignment_people as $k => $v) {
                if ($v->user_id) {
                    $user = $this->model_basic->select_where($this->tbl_user, 'id', $v->user_id)->row();
                    $personel[$k] = $user->realname;
                }
                else{
                    $group = $this->model_basic->select_where($this->tbl_group_security, 'id', $v->group_security_id)->row();
                    $personel[$k] = $group->groupsecurity_name;
                }

            }
            $value->personel = $personel;
        }
        $data['filter'] = $this->load->view('backend/esams/filter', $data, true);
        $data['paging'] = $this->load->view('backend/esams/paging', $data, true);
        // ::end
        $data['content'] = $this->load->view('backend/esams/assignment_inprogress', $data, true);
        $this->load->view('backend/index', $data);
    }

    function completed() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Assignment', 'assignment');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $data['modul'] = self::modul;
        $data['tab'] = 'completed';
        // start:: Pagination
        $sql = '
            SELECT zzz.* FROM (
                SELECT 
                    zz.id as id,
                    zz.assignment_title as assignment_title,
                    zz.assignment_type as assignment_type,
                    zz.start_time as start_time,
                    zz.end_time as end_time,
                    zz.created_by as created_by,
                    zz.created_date as created_date,
                    zz.modified_by as modified_by,
                    zz.modified_date as modified_date,
                    if (zz.total_people>0, zz.total_people, zz.total_people_group) as total_people,
                    zz.total_people_checkin as total_people_checkin,
                    zz.total_people_report as total_people_report,
                    if (zz.total_people>0, zz.total_people*zz.total_assignment, zz.total_people_group*zz.total_assignment) as total_report_assignment
                FROM (
                    SELECT 
                        a.id as id, 
                        a.assignment_title as assignment_title,
                        a.assignment_type as assignment_type,
                        b.realname as created_by, 
                        a.created_date as created_date, 
                        c.realname as modified_by, 
                        a.modified_date as modified_date,
                        if (e.group_security_id is null, count(e.id), 0) as total_people,
                        if (e.group_security_id is not null,
                        (
                            select 
                                count(id) 
                            from 
                                px_group_security_detail sa 
                            where 
                                sa.delete_flag = 0 
                                AND sa.group_security_id = e.group_security_id
                        ), 0) as total_people_group,
                        (
                            select 
                                count(sc.id)
                            from
                                px_assignment_detail sb
                                left JOIN px_assignment_people_checkin sc on sc.assignment_detail_id = sb.id AND sc.delete_flag = 0
                            where
                                sb.delete_flag = 0 
                                AND sb.assignment_id = a.id
                        ) as total_people_checkin,
                        (
                            select 
                                count(se.id)
                            from
                                px_assignment_detail sd
                                left JOIN px_assignment_report se on se.assignment_detail_id = sd.id AND se.delete_flag = 0
                            where
                                sd.delete_flag = 0 
                                AND sd.assignment_id = a.id
                        ) as total_people_report,
                        (
                            select 
                                count(sf.id)
                            from
                                px_assignment_detail sf
                            where
                                sf.delete_flag = 0 
                                AND sf.assignment_id = a.id
                        ) as total_assignment,
                        (
                            select 
                                aa.end_time
                            FROM 
                                px_assignment_detail aa
                                LEFT JOIN px_assignment ab on ab.id = aa.assignment_id
                            where 
                                aa.assignment_id = a.id
                            order by 
                                aa.end_time desc limit 1
                        ) as end_time,
                        (
                            select 
                                aa.start_time
                            FROM 
                                px_assignment_detail aa
                                LEFT JOIN px_assignment ab on ab.id = aa.assignment_id
                            where 
                                aa.assignment_id = a.id
                            order by 
                                aa.start_time asc limit 1
                        ) as start_time
                    FROM 
                        px_assignment a 
                        LEFT JOIN px_admin b ON b.id = a.created_by 
                        LEFT JOIN px_admin c ON c.id = a.modified_by  
                        JOIN px_assignment_people e on e.assignment_id = a.id AND e.delete_flag = 0
                    WHERE 
                        a.delete_flag = 0
                    GROUP BY 
                        a.id
                ) zz
            ) zzz
            WHERE
                 1=1
                -- AND total_people_checkin = 0 AND end_time > CURRENT_TIMESTAMP() -- view
                -- AND total_people_checkin > 0 AND (total_people_report < total_report_assignment) AND end_time > CURRENT_TIMESTAMP() -- inprogress
                -- AND total_people_checkin > 0 AND total_people = total_people_checkin AND total_people_checkin = total_people_report OR end_time < CURRENT_TIMESTAMP() -- completed new
                AND total_people_checkin > 0 AND total_people_checkin = total_people_report OR end_time < CURRENT_TIMESTAMP() -- completed new
            GROUP BY zzz.id DESC
        ';
        $columns = ['assignment_title','assignment_type','created_date','modified_date','start_time','end_time'];
        $data['datas'] = $this->pagination_render($sql, $columns);
        //get personel
        foreach ($data['datas']['data'] as $key => $value) {
            $personel = [];
            $assignment_people = $this->model_basic->select_wheres($this->tbl_assignment_people, 'assignment_id', $value->id);
            foreach ($assignment_people as $k => $v) {
                if ($v->user_id) {
                    $user = $this->model_basic->select_where($this->tbl_user, 'id', $v->user_id)->row();
                    $personel[$k] = $user->realname;
                }
                else{
                    $group = $this->model_basic->select_where($this->tbl_group_security, 'id', $v->group_security_id)->row();
                    $personel[$k] = $group->groupsecurity_name;
                }

            }
            $value->personel = $personel;
        }
        $data['filter'] = $this->load->view('backend/esams/filter', $data, true);
        $data['paging'] = $this->load->view('backend/esams/paging', $data, true);
        // ::end
        $data['content'] = $this->load->view('backend/esams/assignment_completed', $data, true);
        $this->load->view('backend/index', $data);
    }

    function assignment_form($act=null) {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Assignment Form', 'assignment_form');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $id = $this->input->post('id');
        if ($_SERVER['REQUEST_METHOD'] == 'GET' and !$id) $id = $act;
        $data['data'] = $id?$this->model_basic->select_where($this->tbl_assignment, 'id', $id)->row():null;

        $assign_personel = array();
        $assign_group = array();
        if ($data['data']) {
            $detail = $this->model_basic->select_where($this->tbl_assignment_detail, 'assignment_id', $data['data']->id)->result();
            $data['data']->assignment_detail = $detail;
            /*$json = json_encode($data['data']->assignment_detail);
            echo $json;
            die();*/ 
            foreach ($data['data']->assignment_detail as $key => $value)
            {
                $data['loc'] = json_decode($value->location);
            }
            $people = $this->model_basic->select_where($this->tbl_assignment_people, 'assignment_id', $data['data']->id)->result();
            $assignment_people = "Personel";
            foreach ($people as $key => $value) {
                if ($value->group_security_id) {
                    $assignment_people = "Group";
                    $assign_group[$key] = $value->group_security_id;     
                }
                else{
                    $assign_personel[$key] = $value->user_id;
                }
            }

            $type = $this->model_basic->select_where($this->tbl_assignment, 'id', $data['data']->id)->result();
            foreach ($type as $key => $value) {
                $assignment_type = $value->assignment_type;
            }

            $data['data']->assignment_people = $assignment_people;
            $data['data']->assignment_type = $assignment_type;
            $data['data']->assign_personel = $assign_personel;
            $data['data']->assign_group = $assign_group;

            //get attachment
            foreach ($detail as $key => $value) {
                $attachment = $this->model_basic->select_where($this->tbl_assignment_attachment, 'assignment_detail_id', $value->id)->result();
                $data['data']->assignment_detail[$key]->attachment = $attachment;
                
            }
/*
            echo json_encode($data['data']);
                die();*/

        }
        $data['modul'] = self::modul;
        //
        $group_security = $this->model_basic->select_all($this->tbl_group_security);
        foreach ($group_security as $k => $v) {
            $v->selected = in_array($v->id, $assign_group)?1:0;
        }
        $data['group_security'] = $group_security;
        /*echo json_encode($data['group_security']);
        die();*/
        //
        $personel_security = $this->model_basic->select_wheres($this->tbl_user, "level_user_id", 4);
        foreach ($personel_security as $k => $v) {
            $v->selected = in_array($v->id, $assign_personel)?1:0;
        }
        $data['personel_security'] = $personel_security;
        $data['content'] = $this->load->view('backend/esams/assignment_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function assignment_form_detail() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Assignment Form Detail', 'assignment_form_detail');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $id = $this->input->post('id');

        $data['data'] = $id?$this->model_basic->select_where($this->tbl_assignment, 'id', $id)->row():null;

        $assign_personel = array();
        $assign_group = array();
        if ($data['data']) {
            $detail = $this->model_basic->select_where($this->tbl_assignment_detail, 'assignment_id', $data['data']->id)->result();
            $data['data']->assignment_detail = $detail;
            /*$json = json_encode($data['data']->assignment_detail);
            echo $json;
            die();*/ 
            foreach ($data['data']->assignment_detail as $key => $value)
            {
                $data['loc'] = json_decode($value->location);
            }
            $people = $this->model_basic->select_where($this->tbl_assignment_people, 'assignment_id', $data['data']->id)->result();
            $assignment_people = "Personel";
            foreach ($people as $key => $value) {
                if ($value->group_security_id) {
                    $assignment_people = "Group";
                    $assign_group[$key] = $value->group_security_id;     
                }
                else{
                    $assign_personel[$key] = $value->user_id;
                }
            }

            $type = $this->model_basic->select_where($this->tbl_assignment, 'id', $data['data']->id)->result();
            foreach ($type as $key => $value) {
                $assignment_type = $value->assignment_type;
            }

            $data['data']->assignment_people = $assignment_people;
            $data['data']->assignment_type = $assignment_type;
            $data['data']->assign_personel = $assign_personel;
            $data['data']->assign_group = $assign_group;

            //get attachment
            foreach ($detail as $key => $value) {
                $attachment = $this->model_basic->select_where($this->tbl_assignment_attachment, 'assignment_detail_id', $value->id)->result();
                $data['data']->assignment_detail[$key]->attachment = $attachment;
                
            }
/*
            echo json_encode($data['data']);
                die();*/

        }
        $data['modul'] = self::modul;
        //
        $group_security = $this->model_basic->select_all($this->tbl_group_security);
        foreach ($group_security as $k => $v) {
            $v->selected = in_array($v->id, $assign_group)?1:0;
        }
        $data['group_security'] = $group_security;
        /*echo json_encode($data['group_security']);
        die();*/
        //
        $personel_security = $this->model_basic->select_wheres($this->tbl_user, "level_user_id", 4);
        foreach ($personel_security as $k => $v) {
            $v->selected = in_array($v->id, $assign_personel)?1:0;
        }
        $data['personel_security'] = $personel_security;
        $data['content'] = $this->load->view('backend/esams/assignment_form_detail', $data, true);
        $this->load->view('backend/index', $data);
    }

    function assignment_form_add() { 
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Assignment Form', 'assignment');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_CREATE);

        // echo json_encode($this->input->post());
        // echo json_encode($_FILES);
        // die();
        /*Get field assignment*/
        $assignment = $this->db->list_fields($this->tbl_assignment);
        $insert = array();
        foreach ($assignment as $field) {
            if ($field=='id') continue;
            $insert[$field] = $this->input->post($field);
        }
        $insert['created_date'] = date('Y-m-d H:i:s',now());
        $insert['created_by'] = $this->session_admin['admin_id'];
        $insert['modified_date'] = null;
        $insert['modified_by'] = null;

        /*Get field assignment detail*/
        $assignment_detail = $this->db->list_fields($this->tbl_assignment_detail);
        $insert_detail = array();
        foreach ($assignment_detail as $field) {
            if ($field=='id') continue;
            $insert_detail[$field] = $this->input->post($field);
        }

        /*Get field assignment people*/
        $assignment_people = $this->db->list_fields($this->tbl_assignment_people);
        $insert_people = array();
        foreach ($assignment_people as $field) {
            if ($field=='id') continue;
            $insert_people[$field] = $this->input->post($field);
        }

        if($insert['assignment_title']){
            /*insert to table assignment*/
            $do_insert = $this->model_basic->insert_all($this->tbl_assignment,$insert);
            if($do_insert)
            {
                //jika tipe assignment = single
                //insert type single
                if($insert['assignment_type']=="single")
                {
                    //echo("ini single"); die();
                    //insert detail assignment
                    $location = $this->input->post('location_single');
                    $longitude = $this->input->post('longitude_single');   
                    $latitude = $this->input->post('latitude_single');
                    $start_time = $this->input->post('start_time_single');
                    $end_time = $this->input->post('end_time_single');
                    $description = $this->input->post('description_single');
                    $radius = $this->input->post('radius_single');
                    /*echo($start_time);
                    echo($end_time);
                    echo($task_title); die();*/
                    $myObj = array();
                    $myObj['location'] = $location;               
                    $myObj['longitude'] = $longitude;
                    $myObj['latitude'] = $latitude;              
                    $insert_detail['assignment_id'] = $do_insert->id;
                    $insert_detail['location'] = json_encode($myObj);
                    $insert_detail['start_time'] = $start_time;
                    $insert_detail['end_time'] = $end_time;
                    $insert_detail['task_title'] = null;
                    $insert_detail['description'] = $description;
                    $insert_detail['radius'] = $radius;
                    $insert_detail['created_date'] = date('Y-m-d H:i:s',now());
                    $insert_detail['created_by'] = $this->session_admin['admin_id'];
                    $insert_detail['modified_date'] = null;
                    $insert_detail['modified_by'] = null;
                    $do_insert_detail = $this->model_basic->insert_all($this->tbl_assignment_detail,$insert_detail);
                    //end insert detail assignment

                    //insert assignment people
                    $group = $this->input->post('group_security');
                    $personel = $this->input->post('personel_security');
                    if($group){
                        foreach ($group as $g => $groups){
                            $insert_people['group_security_id'] = $groups;
                            $insert_people['user_id'] = null;
                            $insert_people['assignment_id'] = $do_insert->id;
                            $insert_people['status'] = "active";
                            $insert_people['created_date'] = date('Y-m-d H:i:s',now());
                            $insert_people['created_by'] = $this->session_admin['admin_id'];
                            $insert_people['modified_date'] = null;
                            $insert_people['modified_by'] = null;
                            $do_insert_people = $this->model_basic->insert_all($this->tbl_assignment_people,$insert_people);
                        } 
                    }
                    else if($personel){
                        foreach ($personel as $p => $person){
                            $insert_people['user_id'] = $person;
                            $insert_people['group_security_id'] = null;
                            $insert_people['assignment_id'] = $do_insert->id;
                            $insert_people['status'] = "active";
                            $insert_people['created_date'] = date('Y-m-d H:i:s',now());
                            $insert_people['created_by'] = $this->session_admin['admin_id'];
                            $insert_people['modified_date'] = null;
                            $insert_people['modified_by'] = null;
                            $do_insert_people = $this->model_basic->insert_all($this->tbl_assignment_people,$insert_people);

                        } 
                    }
                                    
                    //end insert assignment people

                    //-------->insert assignment attachment
                    $assignment_attachment = $this->db->list_fields($this->tbl_assignment_attachment);
                    $insert_attachment = array();
                    foreach ($assignment_attachment as $field) {
                        if ($field=='id') continue;
                        $insert_attachment[$field] = $this->input->post($field);
                    }

                    $files = $_FILES;
                    $count = count($_FILES['image_name']['name']);
                    if($do_insert_detail){
                        for($i=0;$i<$count;$i++){
                            if(!empty($_FILES['image_name']['name'][$i])){
                                $_FILES['image_name']['name'] = $files['image_name']['name'][$i];
                                $_FILES['image_name']['type'] = $files['image_name']['type'][$i];
                                $_FILES['image_name']['tmp_name'] = $files['image_name']['tmp_name'][$i];
                                $_FILES['image_name']['error'] = $files['image_name']['error'][$i];
                                $_FILES['image_name']['size'] = $files['image_name']['size'][$i];

                                $config = array();
                                $config['upload_path'] = 'assets/uploads/esams/assignment/'; 
                                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                                $config['max_size'] = '5000';
                                $config['file_name'] = date('YmdHis')."_"."Assignment_".$i;
                           
                                $this->load->library('upload',$config); 
                            
                                if($this->upload->do_upload('image_name')){
                                    $uploadData = $this->upload->data();
                                    
                                    $insert_attachment['assignment_detail_id'] = $do_insert_detail->id;
                                    $insert_attachment['image_name'] = $uploadData['file_name'];
                                    $insert_attachment['created_date'] = date('Y-m-d H:i:s',now());
                                    $insert_attachment['created_by'] = $this->session_admin['admin_id'];
                                    $insert_attachment['modified_date'] = null;
                                    $insert_attachment['modified_by'] = null;
                                    $do_insert_attachment = $this->model_basic->insert_all($this->tbl_assignment_attachment,$insert_attachment);
                                } else {
                                    return $this->returnJson(array('status' => 'error','msg' => $this->upload->display_errors()));
                                    exit;

                                }
                                
                            }
                            
                        }                 

                    }
                    
                }
                //jika tipe assignment = series
                else if($insert['assignment_type']=="series")
                {
                    //insert assignment people
                    $group = $this->input->post('group_security');
                    $personel = $this->input->post('personel_security');
                    if($group){
                        foreach ($group as $g => $groups){
                            $insert_people['group_security_id'] = $groups;
                            $insert_people['user_id'] = null;
                            $insert_people['assignment_id'] = $do_insert->id;
                            $insert_people['status'] = "active";
                            $insert_people['created_date'] = date('Y-m-d H:i:s',now());
                            $insert_people['created_by'] = $this->session_admin['admin_id'];
                            $insert_people['modified_date'] = null;
                            $insert_people['modified_by'] = null;
                            $do_insert_people = $this->model_basic->insert_all($this->tbl_assignment_people,$insert_people);
                        } 
                    }
                    else if($personel){
                        foreach ($personel as $p => $person){
                            $insert_people['user_id'] = $person;
                            $insert_people['group_security_id'] = null;
                            $insert_people['assignment_id'] = $do_insert->id;
                            $insert_people['status'] = "active";
                            $insert_people['created_date'] = date('Y-m-d H:i:s',now());
                            $insert_people['created_by'] = $this->session_admin['admin_id'];
                            $insert_people['modified_date'] = null;
                            $insert_people['modified_by'] = null;
                            $do_insert_people = $this->model_basic->insert_all($this->tbl_assignment_people,$insert_people);

                        } 
                    }

                    // Series
                    $task = $this->input->post('task_series');
                    $task_title = $this->input->post('task_title_series');
                    $start_time = $this->input->post('start_time_series');
                    $end_time = $this->input->post('end_time_series');
                    $description = $this->input->post('description_series');
                    //echo json_encode($description);die();
                    $location = $this->input->post('location_series');
                    $latitude = $this->input->post('latitude_series');
                    $longitude = $this->input->post('longitude_series');
                    $radius = $this->input->post('radius_series');
                    //echo json_encode($task); die();
                    foreach ($task as $k => $v) {
                        // Insert Assignment Detail
                        $insert_detail = array();
                        $insert_detail['assignment_id'] = $do_insert->id;
                        $insert_detail['task_title'] = $task_title[$k];
                        $insert_detail['start_time'] = $start_time[$k];
                        $insert_detail['end_time'] = $end_time[$k];
                        $insert_detail['description'] = $description[$k];
                        //echo json_encode($description[$k]);die();
                        $insert_detail['location'] = json_encode(['location'=>$location[$k],'latitude'=>$latitude[$k],'longitude'=>$longitude[$k]]);
                        $insert_detail['radius'] = $radius[$k];
                        $insert_detail['created_date'] = date('Y-m-d H:i:s',now());
                        $insert_detail['created_by'] = $this->session_admin['admin_id'];
                        $insert_detail['modified_date'] = null;
                        $insert_detail['modified_by'] = null;
                        $insert_detail['delete_flag'] = 0;
                        $do_insert_detail = $this->model_basic->insert_all($this->tbl_assignment_detail,$insert_detail);

                        // Insert Attachment Detail
                        $files = $_FILES;
                        $count = count($_FILES['image_name_'.$k]['name']);
                        if($do_insert_detail){
                            for($i=0;$i<$count;$i++){
                                if(!empty($_FILES['image_name_'.$k]['name'][$i])){
                                    $_FILES['image_name']['name'] = $files['image_name_'.$k]['name'][$i];
                                    $_FILES['image_name']['type'] = $files['image_name_'.$k]['type'][$i];
                                    $_FILES['image_name']['tmp_name'] = $files['image_name_'.$k]['tmp_name'][$i];
                                    $_FILES['image_name']['error'] = $files['image_name_'.$k]['error'][$i];
                                    $_FILES['image_name']['size'] = $files['image_name_'.$k]['size'][$i];

                                    $config = array();
                                    $config['upload_path'] = 'assets/uploads/esams/assignment/'; 
                                    $config['allowed_types'] = 'jpg|jpeg|png|gif';
                                    $config['max_size'] = '5000';
                                    $config['file_name'] = date('YmdHis')."_"."Assignment_".$i."_".$k;
                               
                                    $this->load->library('upload',$config); 
                                
                                    if($this->upload->do_upload('image_name')){
                                        $uploadData = $this->upload->data();
                                        
                                        $insert_attachment['assignment_detail_id'] = $do_insert_detail->id;
                                        $insert_attachment['image_name'] = $uploadData['file_name'];
                                        $insert_attachment['created_date'] = date('Y-m-d H:i:s',now());
                                        $insert_attachment['created_by'] = $this->session_admin['admin_id'];
                                        $insert_attachment['modified_date'] = null;
                                        $insert_attachment['modified_by'] = null;
                                        $insert_attachment['delete_flag'] = 0;
                                        $do_insert_attachment = $this->model_basic->insert_all($this->tbl_assignment_attachment,$insert_attachment);
                                    } else {
                                        return $this->returnJson(array('status' => 'error','msg' => $this->upload->display_errors()));
                                        exit;

                                    }
                                    
                                }
                                
                            }                 

                        }
                    }

                }
                //------->end insert assignment attachment

                $this->save_log_admin(ACT_CREATE, 'Insert New Assignment'.$insert['assignment_title']);
                $this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => 'esams/'.$data['controller'].'/'.$data['function']));
                
            }
            else
                $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));   
        }
        else
            $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function assignment_form_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Assignment', 'assignment');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_CREATE);

        $assignment = $this->db->list_fields($this->tbl_assignment);
        $update = array();
        foreach ($assignment as $field) {
            $update[$field] = $this->input->post($field);
        }
        unset($update['created_date']);
        unset($update['created_by']);
        unset($update['delete_flag']);
        $update['modified_date'] = date('Y-m-d H:i:s',now());
        $update['modified_by'] = $this->session_admin['admin_id'];

        if($update['assignment_title']){
            $do_update = $this->model_basic->update($this->tbl_assignment,$update,'id',$update['id']);

            /*$assignment_detail = $this->db->list_fields($this->tbl_assignment_detail);
            $update_detail = array();
            foreach ($assignment_detail as $field) {
                if ($field=='id') continue;
                $update_detail[$field] = $this->input->post($field);
            }*/

            $assignment_people = $this->db->list_fields($this->tbl_assignment_people);
            $update_people = array();
            foreach ($assignment_people as $field) {
                if ($field=='id') continue;
                $update_people[$field] = $this->input->post($field);
            }

            $assignment_attachment = $this->db->list_fields($this->tbl_assignment_attachment);
            $update_attachment = array();
            foreach ($assignment_attachment as $field) {
                if ($field=='id') continue;
                $update_attachment[$field] = $this->input->post($field);
            }
            if($do_update){
                //update assignment people
                // Get current data group/people assignment  by assignment id
                $group = $this->input->post('group_security');

                $personel = $this->input->post('personel_security');
                /*echo json_encode($personel);
                echo json_encode($group);
                die();*/
                $current_member_group = [];
                $current_member_personel = []; 
                $assignment_member = $this->model_basic->select_wheres($this->tbl_assignment_people, 'assignment_id', $update['id']);
                /*echo json_encode($assignment_member);
                die();*/
                foreach ($assignment_member as $v)
                {
                    if($group){
                        $current_member_group[$v->group_security_id] = $v->id;
                        /*echo json_encode($current_members_personel[$v->user_id]);
                        die();*/
                    }
                    else if($personel){
                        $current_members_personel[$v->user_id] = $v->id;
                        /*echo('personel');
                        echo json_encode($current_members_personel[$v->user_id]);
                        die();*/
                    }
                }
                /*try {*/
                /*// DB Transaction Start
                $this->db->trans_start();*/
                // Delete data current (change delete flag 1)
                $do_delete = $this->model_basic->delete($this->tbl_assignment_people,'assignment_id',$update['id']);

                $assignment_people = $this->db->list_fields($this->tbl_assignment_people);

                if($group){
                    foreach ($group as $k => $v) {
                        if (!in_array($v, array_keys($current_member_group))) { // check member existing
                            $insert_people = array();
                            foreach ($assignment_people as $field) {
                                if ($field=='id') continue;
                                $insert_people[$field] = $this->input->post($field);
                            }
                            $insert_people['assignment_id'] = $update['id'];
                            $insert_people['created_date'] = date('Y-m-d H:i:s',now());
                            $insert_people['created_by'] = $this->session_admin['admin_id'];
                            $insert_people['modified_date'] = null;
                            $insert_people['modified_by'] = null;
                            $insert_people['user_id'] = null;
                            $insert_people['status'] = 'active';
                            $insert_people['group_security_id'] = $v;
                            // do insert member
                            $do_insert_member = $this->model_basic->insert_all($this->tbl_assignment_people,$insert_people);
                        } else {
                            $update_people = array();
                            $update_people['id'] = $current_member_group[$v];
                            $update_people['assignment_id'] = $update['id'];
                            $update_people['modified_by'] = $this->session_admin['admin_id'];
                            $update_people['modified_date'] = date('Y-m-d H:i:s',now());
                            $update_people['delete_flag'] = 0;
                            // do update member (change delete flag 0)
                            $do_update_member = $this->model_basic->update($this->tbl_assignment_people,$update_people,'id',$update_people['id']);
                        }
                    }
                    /*echo json_encode($do_insert_member);
                    die();*/
                    // DB Transaction Commit
                    /*$Commit = $this->db->trans_complete();
                    echo json_encode($Commit);
                    die();*/

                }
                else if($personel){
                    
                    foreach ($personel as $k => $v) {
                        /*echo json_encode($k)."k";
                        echo json_encode($v)."v";
                        echo json_encode($current_members_personel)."member current";
                        die();*/
                        if (!in_array($v, array_keys($current_members_personel))) { // check member existing
                            
                            foreach ($assignment_people as $field) {
                                if ($field=='id') 
                                {
                                    continue;
                                }
                                $insert_people[$field] = $this->input->post($field);
                            }
                            $insert_people['assignment_id'] = $update['id'];
                            $insert_people['created_date'] = date('Y-m-d H:i:s',now());
                            $insert_people['created_by'] = $this->session_admin['admin_id'];
                            $insert_people['modified_date'] = null;
                            $insert_people['modified_by'] = null;
                            $insert_people['group_security_id'] = null;
                            $insert_people['status'] = 'active';
                            $insert_people['user_id'] = $v;
                            // do insert member
                            $this->model_basic->insert_all($this->tbl_assignment_people,$insert_people);
                        } else {
                            $update_people = array();
                            $update_people['id'] = $current_members_personel[$v];
                            $update_people['assignment_id'] = $update['id'];
                            $update_people['modified_by'] = $this->session_admin['admin_id'];
                            $update_people['modified_date'] = date('Y-m-d H:i:s',now());
                            $update_people['delete_flag'] = 0;
                            // do update member (change delete flag 0)
                            $this->model_basic->update($this->tbl_assignment_people,$update_people,'id',$update_people['id']);
                        }
                    }
                }                                  
                //end insert assignment people

                //jika tipe assignment = single
                if($update['assignment_type']=="single")
                {
                    //update detail assignment
                    $assignment_detail = $this->db->list_fields($this->tbl_assignment_detail);
                    $update_detail = array();
                    foreach ($assignment_detail as $field) {
                        if ($field=='id') continue;
                        $update_detail[$field] = $this->input->post($field."_single");
                    }
                    $location = $this->input->post('location_single');
                    $longitude = $this->input->post('longitude_single');   
                    $latitude = $this->input->post('latitude_single');
                    $myObj = array();
                    $myObj['location'] = $location;               
                    $myObj['longitude'] = $longitude;
                    $myObj['latitude'] = $latitude;
                    unset($update_detail['task_title']);              
                    $update_detail['assignment_id'] = $update['id'];
                    $update_detail['location'] = json_encode($myObj);
                    unset($update_detail['created_date']);
                    unset($update_detail['created_by']);
                    unset($update_detail['delete_flag']);
                    $update_detail['modified_date'] = date('Y-m-d H:i:s',now());
                    $update_detail['modified_by'] = $this->session_admin['admin_id'];

                    //get Id assignment detail
                    $assignment_detail_id = $this->model_assignment->get_id_detail($update['id']);

                    foreach ($assignment_detail_id as $key => $value) {
                        $do_update_detail = $this->model_basic->update($this->tbl_assignment_detail,$update_detail,'id',$value->id);

                        //end update detail assignment

                        //Update assignment attachment
                        if ($do_update_detail) {

                            /*echo json_encode($_FILES);
                            die();*/
                            if ($_FILES) {
                                $do_delete = $this->model_basic->delete($this->tbl_assignment_attachment,'assignment_detail_id',$value->id);

                                $files = $_FILES;
                                if ($files) {
                                    $count = count($_FILES['image_name']['name']);
                                    //upload file
                                    for($i=0;$i<$count;$i++){
                                        if(!empty($_FILES['image_name']['name'][$i])){
                                            $_FILES['image_name']['name'] = $files['image_name']['name'][$i];
                                            $_FILES['image_name']['type'] = $files['image_name']['type'][$i];
                                            $_FILES['image_name']['tmp_name'] = $files['image_name']['tmp_name'][$i];
                                            $_FILES['image_name']['error'] = $files['image_name']['error'][$i];
                                            $_FILES['image_name']['size'] = $files['image_name']['size'][$i];

                                            $config = array();
                                            $config['upload_path'] = 'assets/uploads/esams/assignment/'; 
                                            $config['allowed_types'] = 'jpg|jpeg|png|gif';
                                            $config['max_size'] = '5000';
                                            $config['file_name'] = date('YmdHis')."_"."Assignment_".$i;
                                       
                                            $this->load->library('upload',$config); 
                                        
                                            if($this->upload->do_upload('image_name')){
                                                $uploadData = $this->upload->data();

                                                //end upload file
                                                //insert DB
                                                $insert_attachment = array();
                                                $table_field = $this->db->list_fields($this->tbl_assignment_attachment);
                                                foreach ($table_field as $field) {
                                                    if ($field=='id') continue;
                                                    $insert_attachment[$field] = $this->input->post($field);
                                                }
                                                $insert_attachment['assignment_detail_id'] = $value->id;
                                                $insert_attachment['created_date'] = date('Y-m-d H:i:s',now());
                                                $insert_attachment['created_by'] = $this->session_admin['admin_id'];
                                                $insert_attachment['modified_date'] = null;
                                                $insert_attachment['modified_by'] = null;
                                                $insert_attachment['image_name'] = $uploadData['file_name'];
                                                // do insert attachment
                                                $do_insert_attachment = $this->model_basic->insert_all($this->tbl_assignment_attachment,$insert_attachment);

                                            } else {
                                                return $this->returnJson(array('status' => 'error','msg' => $this->upload->display_errors()));
                                                exit;

                                            }
                                            
                                        }
                                        
                                    }
                                }
                            }       
                        }
                        //------->end update assignment attachment
                    }
                }
                //jika tipe assignment = series
                else if($update['assignment_type']=="series")
                {
                    // Series
                    $task = $this->input->post('task_series');
                    $detailid = $this->input->post('detailid_series');
                    $task_title = $this->input->post('task_title_series');
                    $start_time = $this->input->post('start_time_series');
                    $end_time = $this->input->post('end_time_series');
                    $description = $this->input->post('description_series');
                    $location = $this->input->post('location_series');
                    $latitude = $this->input->post('latitude_series');
                    $longitude = $this->input->post('longitude_series');
                    $radius = $this->input->post('radius_series');
                    $delete_id = $this->input->post('detail_id');
                    //print_r (explode(",",$delete_id));die();
                    //echo json_encode($delete_id);die();
                    $data_delete = explode(",",$delete_id);
                    if ($delete_id) {
                        foreach ($data_delete as $key => $value) {
                            $this->model_basic->delete($this->tbl_assignment_detail, 'id', $value);
                            $this->model_basic->delete($this->tbl_assignment_attachment, 'assignment_detail_id', $value);
                        }
                    }

                    $current_detail = [];
                    $assignment_detail = $this->model_basic->select_wheres($this->tbl_assignment_detail, 'assignment_id', $update['id']);

                    //$do_delete_detail = $this->model_basic->delete($this->tbl_assignment_detail,'assignment_id',$update['id']);

                    $field_detail = $this->db->list_fields($this->tbl_assignment_detail);

                    foreach ($assignment_detail as $key => $value) {
                        $current_detail[$key] = $value->id;
                    }

                    foreach ($task as $key => $value) {
                        
                        if (!in_array($detailid[$key], $current_detail)) { // check member existing
                            /*echo('tambah');
                            die();*/
                            $insert_detail = array();
                            foreach ($field_detail as $field) {
                                if ($field=='id') continue;
                                $insert_detail[$field] = $this->input->post($field);
                            }
                            $insert_detail['task_title'] = $task_title[$key];
                            $insert_detail['assignment_id'] = $update['id'];
                            $insert_detail['start_time'] = $start_time[$key];
                            $insert_detail['end_time'] = $end_time[$key];
                            $insert_detail['description'] = $description[$key];
                            $insert_detail['location'] = json_encode(['location'=>$location[$key],'latitude'=>$latitude[$key],'longitude'=>$longitude[$key]]);
                            $insert_detail['radius'] = $radius[$key];
                            $insert_detail['created_date'] = date('Y-m-d H:i:s',now());
                            $insert_detail['created_by'] = $this->session_admin['admin_id'];
                            $insert_detail['modified_date'] = null;
                            $insert_detail['modified_by'] = null;
                            $insert_detail['delete_flag'] = 0;
                            $do_insert_detail = $this->model_basic->insert_all($this->tbl_assignment_detail,$insert_detail);

                            if($do_insert_detail){
                                $files = $_FILES;
                                $count = count($_FILES['image_name_'.$key]['name']);
                                for($i=0;$i<$count;$i++){
                                    if(!empty($_FILES['image_name_'.$key]['name'][$i])){
                                        $_FILES['image_name']['name'] = $files['image_name_'.$key]['name'][$i];
                                        $_FILES['image_name']['type'] = $files['image_name_'.$key]['type'][$i];
                                        $_FILES['image_name']['tmp_name'] = $files['image_name_'.$key]['tmp_name'][$i];
                                        $_FILES['image_name']['error'] = $files['image_name_'.$key]['error'][$i];
                                        $_FILES['image_name']['size'] = $files['image_name_'.$key]['size'][$i];

                                        $config = array();
                                        $config['upload_path'] = 'assets/uploads/esams/assignment/'; 
                                        $config['allowed_types'] = 'jpg|jpeg|png|gif';
                                        $config['max_size'] = '5000';
                                        $config['file_name'] = date('YmdHis')."_"."Assignment_".$i."_".$key;
                                   
                                        $this->load->library('upload',$config); 
                                    
                                        if($this->upload->do_upload('image_name')){
                                            $uploadData = $this->upload->data();
                                            
                                            $insert_attachment['assignment_detail_id'] = $do_insert_detail->id;
                                            $insert_attachment['image_name'] = $uploadData['file_name'];
                                            $insert_attachment['created_date'] = date('Y-m-d H:i:s',now());
                                            $insert_attachment['created_by'] = $this->session_admin['admin_id'];
                                            $insert_attachment['modified_date'] = null;
                                            $insert_attachment['modified_by'] = null;
                                            $insert_attachment['delete_flag'] = 0;
                                            $do_insert_attachment = $this->model_basic->insert_all($this->tbl_assignment_attachment,$insert_attachment);
                                        } else {
                                            return $this->returnJson(array('status' => 'error','msg' => $this->upload->display_errors()));
                                            exit;

                                        }
                                        
                                    }
                                    
                                }                 

                            }
                        } else {
                            /*echo('Update');
                            die();*/
                            $update_detail['task_title'] = $task_title[$key];
                            $update_detail['assignment_id'] = $update['id'];
                            $update_detail['start_time'] = $start_time[$key];
                            $update_detail['end_time'] = $end_time[$key];
                            $update_detail['description'] = $description[$key];
                            $update_detail['location'] = json_encode(['location'=>$location[$key],'latitude'=>$latitude[$key],'longitude'=>$longitude[$key]]);
                            $update_detail['radius'] = $radius[$key];
                            unset($update_detail['created_date']);
                            unset($update_detail['created_by']);
                            $update_detail['modified_date'] = date('Y-m-d H:i:s',now());
                            $update_detail['modified_by'] = $this->session_admin['admin_id'];
                            $update_detail['delete_flag'] = 0;
                            // do update member (change delete flag 0)
                            $do_update_detail = $this->model_basic->update($this->tbl_assignment_detail,$update_detail,'id',$detailid[$key]);

                            //get Id assignment detail
                            $assignment_detail_id = $this->model_assignment->get_id_detail($update['id']);
                            foreach ($assignment_detail_id as $key => $value) {
                                //Update assignment attachment
                                if ($do_update_detail) {
                                
                                    if ($_FILES) {
                                        if (!isset($_FILES['image_name_'.$key]['name'])) continue;
                                        $do_delete = $this->model_basic->delete($this->tbl_assignment_attachment,'assignment_detail_id',$value->id);

                                        $files = $_FILES;
                                        $count = count($_FILES['image_name_'.$key]['name']);
                                        //upload file
                                        for($i=0;$i<$count;$i++){
                                            $_FILES['image_name']['name'] = $files['image_name_'.$key]['name'][$i];
                                            $_FILES['image_name']['type'] = $files['image_name_'.$key]['type'][$i];
                                            $_FILES['image_name']['tmp_name'] = $files['image_name_'.$key]['tmp_name'][$i];
                                            $_FILES['image_name']['error'] = $files['image_name_'.$key]['error'][$i];
                                            $_FILES['image_name']['size'] = $files['image_name_'.$key]['size'][$i];

                                            $config = array();
                                            $config['upload_path'] = 'assets/uploads/esams/assignment/'; 
                                            $config['allowed_types'] = 'jpg|jpeg|png|gif';
                                            $config['max_size'] = '5000';
                                            $config['file_name'] = date('YmdHis')."_"."Assignment_".$i;
                                       
                                            $this->load->library('upload',$config); 
                                        
                                            if($this->upload->do_upload('image_name')){
                                                $uploadData = $this->upload->data();

                                                //end upload file
                                                //insert DB
                                                $insert_attachment = array();
                                                $table_field = $this->db->list_fields($this->tbl_assignment_attachment);
                                                foreach ($table_field as $field) {
                                                    if ($field=='id') continue;
                                                    $insert_attachment[$field] = $this->input->post($field);
                                                }
                                                $insert_attachment['assignment_detail_id'] = $value->id;
                                                $insert_attachment['created_date'] = date('Y-m-d H:i:s',now());
                                                $insert_attachment['created_by'] = $this->session_admin['admin_id'];
                                                $insert_attachment['modified_date'] = null;
                                                $insert_attachment['modified_by'] = null;
                                                $insert_attachment['image_name'] = $uploadData['file_name'];
                                                // do insert attachment
                                                $do_insert_attachment = $this->model_basic->insert_all($this->tbl_assignment_attachment,$insert_attachment);

                                            } else {
                                                return $this->returnJson(array('status' => 'error','msg' => $this->upload->display_errors()));
                                                exit;

                                            }
                                            
                                        }
                                    }    
                                }
                                //------->end update assignment attachment
                            }
                        }
                    }
                }
                $this->save_log_admin(ACT_CREATE, 'Update Assignment'.$update['assignment_title']);
                $this->returnJson(array('status' => 'ok','msg' => 'Update data success','redirect' => 'esams/'.$data['controller'].'/'.$data['function']));               
            }
            else
                $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));   
        }
        else
            $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function assignment_delete() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Assignment', 'assignment');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        //delete assignment
        $data['view_assigned'] = $this->model_assignment->view_assignment($id);
        if ($data['view_assigned'] == null)
        {
            $do_delete = $this->model_basic->delete($this->tbl_assignment,'id',$id);
            if($do_delete){
                //delete assignment detail berdasarkan assignment_id
                $do_delete_detail = $this->model_basic->delete($this->tbl_assignment_detail,'assignment_id',$id);
                //delete assignment people berdasarkan assignment_id
                $do_delete_people = $this->model_basic->delete($this->tbl_assignment_people,'assignment_id',$id);
                if ($do_delete_detail) {
                    //get id assignment detail
                    $assign_id = $this->model_assignment->get_id_detail($id);
                    foreach ($assign_id as $key => $value) {
                        //delete assignment berdasarkan assigment_detail_id
                        $do_delete_attachment = $this->model_basic->delete($this->tbl_assignment_people,'assignment_detail_id',$value->id);
                    }
                    
                }
                $this->save_log_admin(4, 'Delete Esams Assignmen id: '.$id);
                $this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
            }
            else
                $this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
        }
        
    }

    function assignment_report(){
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Assignment Report', 'assignment_report');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $data['modul'] = self::modul;
        $id = $this->input->post('id');
        $data['data'] = $this->assignment_report_pdf($id);
        $data['content'] = $this->load->view('backend/esams/assignment_report', $data, true);
        $this->load->view('backend/index', $data);
    }

    function get_assignment_report_pdf($assignment_id,$user_id,$assignment_detail_id){
        $data['data'] = $this->assignment_report_pdf($assignment_id,$user_id,$assignment_detail_id);
        //echo json_encode($data['data']); die();
        //echo json_encode($data['data']['assignment_detail'][0]['detail'][0]['task_title']); die();

        //return $this->load->view('backend/esams/assignment_report_pdf',$data); die();
        $this->load->view('backend/esams/assignment_report_pdf',$data);
        $html = $this->output->get_output();

        $this->load->library('Dom_pdf');
        $this->dompdf->set_option('isRemoteEnabled', true);

        $date = date('Y-m-d');
        $username = $data['data']['assignment_detail'][0]['user_name'];
        $assignment_title = $data['data']['assignment_title'];
        $subtask = $data['data']['assignment_detail'][0]['detail'][0]['task_title'];

        // Convert to PDF
        $this->dompdf->load_html($html);  
        $this->dompdf->setPaper('A4', 'potrait');       
        $this->dompdf->render();
        $this->dompdf->stream("ReportAssignment_".$assignment_title."_".$username."_".$subtask."_".$date.".pdf");
    }

    private function assignment_report_pdf($assignment_id, $user_id=null, $assignment_detail_id=null) {
        $data_report = [];
        $assignment = $this->model_basic->select_where($this->tbl_assignment, 'id', $assignment_id)->row();
        if ($assignment) {
            if ($user_id) {
                $where = [];
                $where['assignment_id'] = $assignment_id;
                $where['user_id'] = $user_id;
                $assignment_people = $this->model_basic->select_where_array($this->tbl_assignment_people,$where)->result();
            }
            else{
                $assignment_people = $this->model_basic->select_where($this->tbl_assignment_people, 'assignment_id', $assignment->id)->result();
            }
            
            $data_report['assignment_id'] = $assignment->id;
            $data_report['assignment_title'] = $assignment->assignment_title;
            foreach ($assignment_people as $k1 => $v1) {
                $report_task = [];
                // Get User
                $user = $this->model_basic->select_where($this->tbl_user, 'id', $v1->user_id)->row();
                if (!$user) continue;
                $report_task['user_id'] = $v1->user_id;
                $report_task['user_name'] = $user->realname;
                // Get Assignment Detail
                if ($assignment_detail_id) {
                    $assignment_detail = $this->model_basic->select_where($this->tbl_assignment_detail, 'id', $assignment_detail_id)->result();
                }
                else{
                    $assignment_detail = $this->model_basic->select_where($this->tbl_assignment_detail, 'assignment_id', $assignment->id)->result();
                }
                
                foreach ($assignment_detail as $k2 => $v2) {
                    $report_task['detail'][$k2]['assignment_detail_id'] = $v2->id;
                    $report_task['detail'][$k2]['task_title'] = $v2->task_title!=''?$v2->task_title:$assignment->assignment_title;
                    $report_task['detail'][$k2]['user_name'] = $user->realname;
                    $report_task['detail'][$k2]['start_time'] = date('d/m/Y H:i', strtotime($v2->start_time));
                    $report_task['detail'][$k2]['end_time'] = date('d/m/Y H:i', strtotime($v2->end_time));
                    $report_task['detail'][$k2]['location'] = json_decode($v2->location)->location;
                    // Get People Checkin
                    $where = [];
                    $where['assignment_detail_id'] = $v2->id;
                    $where['user_id'] = $v1->user_id;
                    $assignment_people_checkin = $this->model_basic->select_where_array($this->tbl_assignment_people_checkin, $where)->row();
                    $report_task['detail'][$k2]['checkin_date'] = $assignment_people_checkin?date('d/m/Y H:i', strtotime($assignment_people_checkin->created_date)):null;
                    $images_report = [];
                    $images_report[] = $assignment_people_checkin?base_url().'assets/uploads/esams/assignment/'.$assignment_people_checkin->image_name:null;
                    // Get People Report
                    $assignment_report = $this->model_basic->select_where_array($this->tbl_assignment_report, $where)->row();
                    $report_task['detail'][$k2]['report_date'] = $assignment_report?date('d/m/Y H:i', strtotime($assignment_report->created_date)):null;
                    $report_task['detail'][$k2]['description_task'] = $v2->description;
                    $report_task['detail'][$k2]['report_text'] = $assignment_report?$assignment_report->report_text:null;
                    // Get People Attactmane Report
                    $assignment_report_attachment = $assignment_report?$this->model_basic->select_where($this->tbl_assignment_report_attachment, 'assignment_report_id', $assignment_report->id)->result():[];
                    for ($i=1; $i <= 3; $i++) {
                        $images_report[$i] = isset($assignment_report_attachment[($i-1)])?base_url().'assets/uploads/esams/assignment/'.$assignment_report_attachment[($i-1)]->image_name:null;
                    }
                    // foreach ($assignment_report_attachment as $k3 => $v3) {
                    //     $images_report[] = base_url().'assets/uploads/esams/assignment/'.$v3->image_name;
                    // }
                    $report_task['detail'][$k2]['images_report'] = $images_report;
                    $report_task['detail'][$k2]['assignment_agreement'] = 'Saya menyatakan bahwa laporan yang saya buat bedasarkan pada kondisi yang sebenarnya. Dan saya siap mempertanggungjawabkan laporan yang telah saya buat ini';
                }
                $data_report['assignment_detail'][$k1] = $report_task;
            }
        }
        //echo json_encode($data_report); die();
        return $data_report;
    }

}
