<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Masterdata extends PX_Controller {

    public function __construct() {
        parent::__construct();
        $this->check_login();
        $this->controller_attr = array('controller' => 'masterdata', 'controller_name' => 'Master data', 'controller_id' => 0);
    }

    const modul = 'esams';

    public function index() {
        $this->check_userakses($data['function_id'], ACT_READ);
    }

    function shift() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Shift', 'shift');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $data['modul'] = self::modul;
        // start:: Pagination
        $sql = '
            SELECT 
                a.id as id,
                a.shift_name as shift_name,
                a.start_time as start_time,
                a.end_time as end_time,
                b.realname as created_by,
                a.created_date as created_date,
                c.realname as modified_by,
                a.modified_date as modified_date
            FROM
                '.$this->tbl_shift.' a
                LEFT JOIN '.$this->tbl_admin.' b ON b.id = a.created_by
                LEFT JOIN '.$this->tbl_admin.' c ON c.id = a.modified_by
            WHERE 
                a.delete_flag = 0
        ';
        $columns = ['shift_name','start_time','end_time','created_date','modified_date'];
        $data['datas'] = $this->pagination_render($sql, $columns);
        $data['filter'] = $this->load->view('backend/esams/filter', $data, true);
        $data['paging'] = $this->load->view('backend/esams/paging', $data, true);
        // ::end
        $data['content'] = $this->load->view('backend/esams/masterdata/shift', $data, true);
        $this->load->view('backend/index', $data);
    }

    function shift_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Shift', 'shift');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $id = $this->input->post('id');
        $data['data'] = $id?$this->model_basic->select_where($this->tbl_shift, 'id', $id)->row():null;
        $data['modul'] = self::modul;
        //$data['instalasi'] = $this->model_basic->select_all($this->tbl_instalasi);
        $data['content'] = $this->load->view('backend/esams/masterdata/shift_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function shift_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Shift','Shift');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_CREATE);

        $this->form_validation->set_rules('shift_name', 'shift_name', 'required');
        $this->form_validation->set_rules('start_time', 'start_time', 'required');
        $this->form_validation->set_rules('end_time', 'end_time', 'required');
        if (!$this->form_validation->run())
            return $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));

        $table_field = $this->db->list_fields($this->tbl_shift);
        $insert = array();
        foreach ($table_field as $field) {
            if ($field=='id') continue;
            $insert[$field] = $this->input->post($field);
        }
        $insert['created_date'] = date('Y-m-d H:i:s',now());
        $insert['created_by'] = $this->session_admin['admin_id'];
        $insert['modified_date'] = null;
        $insert['modified_by'] = null;
        if($insert['shift_name'] && $insert['start_time'] && $insert['end_time']){
            $do_insert = $this->model_basic->insert_all($this->tbl_shift,$insert);
            if($do_insert){
                $this->save_log_admin(ACT_CREATE, 'Insert New Masterdata Shift '.$insert['shift_name']);
                $this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => 'esams/'.$data['controller'].'/'.$data['function']));
            }
            else
                $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));   
        }
        else
            $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function shift_edit() { 
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Shift','Shift');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_UPDATE);

        $this->form_validation->set_rules('shift_name', 'shift_name', 'required');
        $this->form_validation->set_rules('start_time', 'start_time', 'required');
        $this->form_validation->set_rules('end_time', 'end_time', 'required');
        if (!$this->form_validation->run())
            return $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));

        $table_field = $this->db->list_fields($this->tbl_shift);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }
        unset($update['created_date']);
        unset($update['created_by']);
        unset($update['delete_flag']);
        $update['modified_by'] = $this->session_admin['admin_id'];
        $update['modified_date'] = date('Y-m-d H:i:s',now());
        if(count($update)>0) {
            $do_update = $this->model_basic->update($this->tbl_shift,$update,'id',$update['id']);
            if($do_update) {
                $this->save_log_admin(3, 'Update Esams Master Data shift');
                $this->returnJson(array('status' => 'ok','msg' => 'Update data success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
            } else
                $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));
        } else
            $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function shift_delete(){
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Shift','shift');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $do_delete = $this->model_basic->delete($this->tbl_shift,'id',$id);
        if($do_delete){
            $this->save_log_admin(4, 'Delete Esams Master Data Shift');
            $this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
        }
        else
            $this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }

    //Master data group security
    function group_security() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Group Security', 'group_security');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $data['modul'] = self::modul;
        // start:: Pagination
        $sql = '
            SELECT 
                a.id as id,
                a.groupsecurity_name as groupsecurity_name,
                b.realname as created_by,
                a.created_date as created_date,
                c.realname as modified_by,
                a.modified_date as modified_date
            FROM
                '.$this->tbl_group_security.' a
                LEFT JOIN '.$this->tbl_admin.' b ON b.id = a.created_by
                LEFT JOIN '.$this->tbl_admin.' c ON c.id = a.modified_by
            WHERE 
                a.delete_flag = 0
        ';
        $columns = ['groupsecurity_name','created_date','modified_date'];
        $data['datas'] = $this->pagination_render($sql, $columns);
        $data['filter'] = $this->load->view('backend/esams/filter', $data, true);
        $data['paging'] = $this->load->view('backend/esams/paging', $data, true);
        // ::end
        $data['content'] = $this->load->view('backend/esams/masterdata/group_security', $data, true);
        $this->load->view('backend/index', $data);
    }

    function group_security_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Group Security', 'group_security');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $id = $this->input->post('id');
        $data['data'] = $id?$this->model_basic->select_where($this->tbl_group_security, 'id', $id)->row():null;
        $data['modul'] = self::modul;
        //$data['instalasi'] = $this->model_basic->select_all($this->tbl_instalasi);
        $data['content'] = $this->load->view('backend/esams/masterdata/group_security_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function group_security_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Group Security', 'group_security');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_CREATE);

        $this->form_validation->set_rules('groupsecurity_name', 'groupsecurity_name', 'required');
        if (!$this->form_validation->run())
            return $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));

        $table_field = $this->db->list_fields($this->tbl_group_security);
        $insert = array();
        foreach ($table_field as $field) {
            if ($field=='id') continue;
            $insert[$field] = $this->input->post($field);
        }
        $insert['created_date'] = date('Y-m-d H:i:s',now());
        $insert['created_by'] = $this->session_admin['admin_id'];
        $insert['modified_date'] = null;
        $insert['modified_by'] = null;
        if($insert['groupsecurity_name']){
            $do_insert = $this->model_basic->insert_all($this->tbl_group_security,$insert);
            if($do_insert){
                $this->save_log_admin(ACT_CREATE, 'Insert New Masterdata Group Security '.$insert['groupsecurity_name']);
                $this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => 'esams/'.$data['controller'].'/'.$data['function']));
            }
            else
                $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));   
        }
        else
            $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function group_security_edit() { 
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Group Security', 'group_security');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_UPDATE);

        $this->form_validation->set_rules('groupsecurity_name', 'groupsecurity_name', 'required');
        if (!$this->form_validation->run())
            return $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
        
        $table_field = $this->db->list_fields($this->tbl_group_security);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }
        unset($update['created_date']);
        unset($update['created_by']);
        unset($update['delete_flag']);
        $update['modified_by'] = $this->session_admin['admin_id'];
        $update['modified_date'] = date('Y-m-d H:i:s',now());
        if(count($update)>0) {
            $do_update = $this->model_basic->update($this->tbl_group_security,$update,'id',$update['id']);
            if($do_update) {
                $this->save_log_admin(3, 'Update Esams Master Data Group Security');
                $this->returnJson(array('status' => 'ok','msg' => 'Update data success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
            } else
                $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));
        } else
            $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function group_security_delete(){
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Group Security','group_security');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $do_delete = $this->model_basic->delete($this->tbl_group_security,'id',$id);
        // Delete data member group security
        $do_delete_member = $this->model_basic->delete($this->tbl_group_detail,'group_security_id',$id);
        if($do_delete and $do_delete_member){
            $this->save_log_admin(4, 'Delete Esams Master Data Group Security');
            $this->save_log_admin(4, 'Delete Esams Master Data Group Detail');
            $this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
        }
        else
            $this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }

    //Master data group security detail
    function group_detail() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Group Security Detail', 'group_detail');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $data['modul'] = self::modul;
        // start:: Pagination
        $sql = '
            SELECT 
                a.id as id, 
                a.groupsecurity_name as groupsecurity_name, 
                count(b.user_id) as jumlah,
                c.realname as created_by, 
                a.created_date as created_date, 
                d.realname as modified_by, 
                a.modified_date as modified_date
            FROM 
                px_group_security a
                LEFT JOIN px_group_security_detail b on a.id = b.group_security_id and b.delete_flag = 0
                LEFT JOIN px_admin c ON c.id = a.created_by
                LEFT JOIN px_admin d ON d.id = a.modified_by
            WHERE
                a.delete_flag = 0 
            GROUP BY a.id
        ';
        $columns = ['groupsecurity_name','jumlah','created_date','modified_date'];
        $data['datas'] = $this->pagination_render($sql, $columns);
        $data['filter'] = $this->load->view('backend/esams/filter', $data, true);
        $data['paging'] = $this->load->view('backend/esams/paging', $data, true);
        // ::end
        $data['content'] = $this->load->view('backend/esams/masterdata/group_detail', $data, true);
        $this->load->view('backend/index', $data);
    }

    function group_detail_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Group Detail', 'group_detail');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $id = $this->input->post('id');
        $data['data'] = $id?$this->model_basic->select_where($this->tbl_group_security, 'id', $id)->row():null;
        $data['modul'] = self::modul;
        $data['group'] = $this->model_basic->select_all($this->tbl_group_security);
        // Add by Alvin (atsalvin0017), 2019-09-11 14:43 PM
        // Get Member current group detail
        $current_members = []; 
        if ($id) {
            $group_detail = $this->model_basic->select_wheres($this->tbl_group_detail, 'group_security_id', $id);
            foreach ($group_detail as $v) $current_members[] = $v->user_id;
        }
        $member = $this->model_basic->select_wheres($this->tbl_user, "level_user_id", 4);
        // Set selected for current member group detail
        foreach ($member as $k => $v) $v->selected = in_array($v->id, $current_members)?1:0;
        $data['member'] = $member;
        // 
        $data['content'] = $this->load->view('backend/esams/masterdata/group_detail_form', $data, true);
        $this->load->view('backend/index', $data);
    }
    function group_detail_edit() { 
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Group Detail', 'group_detail');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_UPDATE);
        // Add by Alvin (atsalvin0017), 2019-09-11 16:23 PM
        $group_security_id = $this->input->post('group_security_id');
        $members = $this->input->post('member');
        if (empty($members))
            return $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));

        // Get current data group security by group id
        $current_members = []; 
        $group_detail = $this->model_basic->select_wheres($this->tbl_group_detail, 'group_security_id', $group_security_id);
        foreach ($group_detail as $v) $current_members[$v->user_id] = $v->id;
        try {
            // DB Transaction Start
            $this->db->trans_start();
            // Delete data current (change delete flag 1)
            $do_delete = $this->model_basic->delete($this->tbl_group_detail,'group_security_id',$group_security_id);
            $table_field = $this->db->list_fields($this->tbl_group_detail);
            foreach ($members as $k => $v) {
                if (!in_array($v, array_keys($current_members))) { // check member existing
                    $insert = array();
                    foreach ($table_field as $field) {
                        if ($field=='id') continue;
                        $insert[$field] = $this->input->post($field);
                    }
                    $insert['created_date'] = date('Y-m-d H:i:s',now());
                    $insert['created_by'] = $this->session_admin['admin_id'];
                    $insert['modified_date'] = null;
                    $insert['modified_by'] = null;
                    $insert['user_id'] = $v;
                    // do insert member
                    $this->model_basic->insert_all($this->tbl_group_detail,$insert);
                } else {
                    $update = array();
                    $update['id'] = $current_members[$v];
                    $update['modified_by'] = $this->session_admin['admin_id'];
                    $update['modified_date'] = date('Y-m-d H:i:s',now());
                    $update['delete_flag'] = 0;
                    // do update member (change delete flag 0)
                    $this->model_basic->update($this->tbl_group_detail,$update,'id',$update['id']);
                }
            }
            // DB Transaction Commit
            $this->db->trans_complete();
            $this->save_log_admin(3, 'Update Esams Master Data Group Detail');
            $this->returnJson(array('status' => 'ok','msg' => 'Update data success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
        } catch (Exception $e) {
            // DB Transaction Rollback
            $this->db->trans_rollback();
            $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));
        }
    }

    /* Created: Alvin (atsalvin0017), Date: 2019-09-03 11:38 AM  
    Management MasterData Building
    start:: */
    function building() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Building', 'building');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $data['modul'] = self::modul;
        // start:: Pagination
        $sql = '
            SELECT 
                a.id as id,
                a.building_name as building_name,
                d.name as instalasi_name,
                b.realname as created_by,
                a.created_date as created_date,
                c.realname as modified_by,
                a.modified_date as modified_date
            FROM
                '.$this->tbl_building.' a
                LEFT JOIN '.$this->tbl_admin.' b ON b.id = a.created_by
                LEFT JOIN '.$this->tbl_admin.' c ON c.id = a.modified_by
                JOIN '.$this->tbl_instalasi.' d ON d.id = a.instalasi_id
            WHERE 
                a.delete_flag = 0
        ';
        $columns = ['building_name','instalasi_name','created_date','modified_date'];
        $data['datas'] = $this->pagination_render($sql, $columns);
        $data['filter'] = $this->load->view('backend/esams/filter', $data, true);
        $data['paging'] = $this->load->view('backend/esams/paging', $data, true);
        // ::end
        $data['content'] = $this->load->view('backend/esams/masterdata/building', $data, true);
        $this->load->view('backend/index', $data);
    }

    function building_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Building Form', 'building_form');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $id = $this->input->post('id');
        $data['data'] = ($id)?$this->model_basic->select_where($this->tbl_building, 'id', $id)->row():null;
        $data['modul'] = self::modul;
        $data['instalasi'] = $this->model_basic->select_all($this->tbl_instalasi);
        $data['content'] = $this->load->view('backend/esams/masterdata/building_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function building_form_add() { 
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Building','building');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_CREATE);

        $this->form_validation->set_rules('instalasi_id', 'instalasi_id', 'required');
        $this->form_validation->set_rules('building_name', 'building_name', 'required');
        if (!$this->form_validation->run())
            return $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));

        $table_field = $this->db->list_fields($this->tbl_building);
        $insert = array();
        foreach ($table_field as $field) {
            if ($field=='id') continue;
            $insert[$field] = $this->input->post($field);
        }
        $insert['created_date'] = date('Y-m-d H:i:s',now());
        $insert['created_by'] = $this->session_admin['admin_id'];
        $insert['delete_flag'] = 0;
        $insert['modified_by'] = null;
        $insert['modified_date'] = null;
        if(count($insert)>0) {
            $do_insert = $this->model_basic->insert_all($this->tbl_building,$insert);
            if($do_insert) {
                $this->save_log_admin(2, 'Insert Esams Master Data Building');
                $this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
            } else
                $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));
        } else
            $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function building_form_edit() { 
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Building','building');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_UPDATE);

        $this->form_validation->set_rules('instalasi_id', 'instalasi_id', 'required');
        $this->form_validation->set_rules('building_name', 'building_name', 'required');
        if (!$this->form_validation->run())
            return $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));

        $table_field = $this->db->list_fields($this->tbl_building);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }
        unset($update['created_date']);
        unset($update['created_by']);
        unset($update['delete_flag']);
        $update['modified_by'] = $this->session_admin['admin_id'];
        $update['modified_date'] = date('Y-m-d H:i:s',now());
        if(count($update)>0) {
            $do_update = $this->model_basic->update($this->tbl_building,$update,'id',$update['id']);
            if($do_update) {
                $this->save_log_admin(3, 'Update Esams Master Data Building');
                $this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
            } else
                $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));
        } else
            $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function building_delete(){
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Building','building');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $do_delete = $this->model_basic->delete($this->tbl_building,'id',$id);
        if($do_delete){
            $this->save_log_admin(4, 'Delete Esams Master Data Building');
            $this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
        }
        else
            $this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }
    /* end:: */

    /* Created: Alvin (atsalvin0017), Date: 2019-09-23 16:13 PM  
    Management MasterData Detail Building
    start:: */
    function detail_building() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Detail Building', 'detail_building');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $data['modul'] = self::modul;
        // start:: Pagination
        $sql = '
            SELECT 
                a.id as id,
                a.floor as floor,
                b.building_name as building_name,
                e.name as instalasi_name,
                c.realname as created_by,
                a.created_date as created_date,
                d.realname as modified_by,
                a.modified_date as modified_date
            FROM
                '.$this->tbl_detail_building.' a
                JOIN (
                    '.$this->tbl_building.' b
                    JOIN '.$this->tbl_instalasi.' e ON e.id = b.instalasi_id
                ) ON b.id = a.building_id
                LEFT JOIN '.$this->tbl_admin.' c ON c.id = a.created_by
                LEFT JOIN '.$this->tbl_admin.' d ON d.id = a.modified_by
            WHERE 
                a.delete_flag = 0
        ';
        $columns = ['floor', 'building_name','instalasi_name','created_date','modified_date'];
        $data['datas'] = $this->pagination_render($sql, $columns);
        $data['filter'] = $this->load->view('backend/esams/filter', $data, true);
        $data['paging'] = $this->load->view('backend/esams/paging', $data, true);
        // ::end
        $data['content'] = $this->load->view('backend/esams/masterdata/detail_building', $data, true);
        $this->load->view('backend/index', $data);
    }

    function detail_building_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Detail Building Form', 'detail_building_form');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $id = $this->input->post('id');
        $data['data'] = ($id)?$this->model_basic->select_where($this->tbl_detail_building, 'id', $id)->row():null;
        if ($data['data']) {
            $building = $this->model_basic->select_where($this->tbl_building, 'id', $data['data']->building_id)->row();
            $data['data']->instalasi_id = $building?$building->instalasi_id:null;
        }
        $data['modul'] = self::modul;
        $data['instalasi'] = $this->model_basic->select_all($this->tbl_instalasi);
        $data['building'] = $data['data']?$this->model_basic->select_where($this->tbl_building, 'instalasi_id', $data['data']->instalasi_id)->result():[];
        $data['content'] = $this->load->view('backend/esams/masterdata/detail_building_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function detail_building_form_add() { 
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Detail Building','detail_building');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_CREATE);

        $this->form_validation->set_rules('building_id', 'building_id', 'required');
        $this->form_validation->set_rules('floor', 'floor', 'required');
        if (!$this->form_validation->run())
            return $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));

        $table_field = $this->db->list_fields($this->tbl_detail_building);
        $insert = array();
        foreach ($table_field as $field) {
            if ($field=='id') continue;
            $insert[$field] = $this->input->post($field);
        }
        $insert['created_date'] = date('Y-m-d H:i:s',now());
        $insert['created_by'] = $this->session_admin['admin_id'];
        $insert['delete_flag'] = 0;
        $insert['modified_by'] = null;
        $insert['modified_date'] = null;
        if(count($insert)>0) {
            $do_insert = $this->model_basic->insert_all($this->tbl_detail_building,$insert);
            if($do_insert) {
                $this->save_log_admin(2, 'Insert Esams Master Data Detail Building');
                $this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
            } else
                $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));
        } else
            $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function detail_building_form_edit() { 
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Detail Building','detail_building');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_UPDATE);

        $this->form_validation->set_rules('building_id', 'building_id', 'required');
        $this->form_validation->set_rules('floor', 'floor', 'required');
        if (!$this->form_validation->run())
            return $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));

        $table_field = $this->db->list_fields($this->tbl_detail_building);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }
        unset($update['created_date']);
        unset($update['created_by']);
        unset($update['delete_flag']);
        $update['modified_by'] = $this->session_admin['admin_id'];
        $update['modified_date'] = date('Y-m-d H:i:s',now());
        if(count($update)>0) {
            $do_update = $this->model_basic->update($this->tbl_detail_building,$update,'id',$update['id']);
            if($do_update) {
                $this->save_log_admin(3, 'Update Esams Master Data Detail Building');
                $this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
            } else
                $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));
        } else
            $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function detail_building_delete(){
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Detail Building','detail_building');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $do_delete = $this->model_basic->delete($this->tbl_detail_building,'id',$id);
        if($do_delete){
            $this->save_log_admin(4, 'Delete Esams Master Data Detail Building');
            $this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
        }
        else
            $this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }
    /* end:: */

    /* Created: Alvin (atsalvin0017), Date: 2019-09-23 16:47 PM  
    Management MasterData Checkpoint
    start:: */
    function checkpoint() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Checkpoint', 'checkpoint');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $data['modul'] = self::modul;
        // start:: Pagination
        $sql = '
            SELECT 
                a.id as id,
                a.checkpoint_name as checkpoint_name,
                g.channel as cctv_channel,
                d.floor as floor,
                e.building_name as building_name,
                f.name as instalasi_name,
                b.realname as created_by,
                a.created_date as created_date,
                c.realname as modified_by,
                a.modified_date as modified_date
            FROM
                '.$this->tbl_checkpoint.' a
                LEFT JOIN '.$this->tbl_admin.' b ON b.id = a.created_by
                LEFT JOIN '.$this->tbl_admin.' c ON c.id = a.modified_by
                JOIN (
                    '.$this->tbl_detail_building.' d
                    JOIN (
                        '.$this->tbl_building.' e
                        JOIN '.$this->tbl_instalasi.' f ON f.id = e.instalasi_id
                    ) ON e.id = d.building_id
                ) ON d.id = a.building_detail_id
                LEFT JOIN '.$this->tbl_cctv.' g ON g.id = a.cctv_csc_id
            WHERE 
                a.delete_flag = 0
        ';
        $columns = ['checkpoint_name','cctv_channel','floor','building_name','instalasi_name','created_date','modified_date'];
        $data['datas'] = $this->pagination_render($sql, $columns);
        $data['filter'] = $this->load->view('backend/esams/filter', $data, true);
        $data['paging'] = $this->load->view('backend/esams/paging', $data, true);
        // ::end
        $data['content'] = $this->load->view('backend/esams/masterdata/checkpoint', $data, true);
        $this->load->view('backend/index', $data);
    }

    function checkpoint_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Checkpoint Form', 'checkpoint_form');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $id = $this->input->post('id');
        $data['data'] = ($id)?$this->model_basic->select_where($this->tbl_checkpoint, 'id', $id)->row():null;
        if ($data['data']) {
            $detail_building = $this->model_basic->select_where($this->tbl_detail_building, 'id', $data['data']->building_detail_id)->row();
            $data['data']->building_id = $detail_building?$detail_building->building_id:null;
            $building = null;
            if ($detail_building)
                $building = $this->model_basic->select_where($this->tbl_building, 'id', $detail_building->building_id)->row();
            $data['data']->instalasi_id = $building?$building->instalasi_id:null;
            //get data cctv
            $cctv_channel = $this->model_basic->select_where($this->tbl_cctv, 'id', $data['data']->cctv_csc_id)->row();
            $data['data']->cctv_csc_id = $cctv_channel?$cctv_channel->id:null;
        }
        $data['modul'] = self::modul;
        $data['instalasi'] = $this->model_basic->select_all($this->tbl_instalasi);
        $data['building'] = $data['data']?$this->model_basic->select_where($this->tbl_building, 'instalasi_id', $data['data']->instalasi_id)->result():[];
        $data['detail_building'] = $data['data']?$this->model_basic->select_where($this->tbl_detail_building, 'building_id', $data['data']->building_id)->result():[];
        $data['cctv_channel'] = $this->model_basic->select_all($this->tbl_cctv);
        //echo json_encode($data['cctv_channel']); die();
        $data['content'] = $this->load->view('backend/esams/masterdata/checkpoint_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function checkpoint_form_add() { 
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Checkpoint','checkpoint');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_CREATE);

        $this->form_validation->set_rules('building_detail_id', 'building_detail_id', 'required');
        $this->form_validation->set_rules('checkpoint_name', 'checkpoint_name', 'required');
        $this->form_validation->set_rules('cctv_csc_id', 'cctv_csc_id', 'required');
        if (!$this->form_validation->run())
            return $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));

        $table_field = $this->db->list_fields($this->tbl_checkpoint);
        $insert = array();
        foreach ($table_field as $field) {
            if ($field=='id') continue;
            $insert[$field] = $this->input->post($field);
        }
        $insert['created_date'] = date('Y-m-d H:i:s',now());
        $insert['created_by'] = $this->session_admin['admin_id'];
        $insert['delete_flag'] = 0;
        $insert['modified_by'] = null;
        $insert['modified_date'] = null;
        if(count($insert)>0) {
            $do_insert = $this->model_basic->insert_all($this->tbl_checkpoint,$insert);
            if($do_insert) {
                $this->save_log_admin(2, 'Insert Esams Master Data Checkpoint');
                $this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
            } else
                $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));
        } else
            $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function checkpoint_form_edit() { 
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Checkpoint','checkpoint');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_UPDATE);

        $this->form_validation->set_rules('building_detail_id', 'building_detail_id', 'required');
        $this->form_validation->set_rules('checkpoint_name', 'checkpoint_name', 'required');
        if (!$this->form_validation->run())
            return $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));

        $table_field = $this->db->list_fields($this->tbl_checkpoint);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }
        unset($update['created_date']);
        unset($update['created_by']);
        unset($update['delete_flag']);
        $update['modified_by'] = $this->session_admin['admin_id'];
        $update['modified_date'] = date('Y-m-d H:i:s',now());
        if(count($update)>0) {
            $do_update = $this->model_basic->update($this->tbl_checkpoint,$update,'id',$update['id']);
            if($do_update) {
                $this->save_log_admin(3, 'Update Esams Master Data Checkpoint');
                $this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
            } else
                $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));
        } else
            $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function checkpoint_delete(){
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Checkpoint','checkpoint');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $do_delete = $this->model_basic->delete($this->tbl_checkpoint,'id',$id);
        if($do_delete){
            $this->save_log_admin(4, 'Delete Esams Master Data Checkpoint');
            $this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
        }
        else
            $this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }
    /* end:: */

    /* Created: Alvin (atsalvin0017), Date: 2019-09-23 17:07 PM  
    Management MasterData CCTV
    start:: */
    function cctv() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('CCTV', 'cctv');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $data['modul'] = self::modul;
        // start:: Pagination
        /*$sql = '
            SELECT 
                a.id as id,
                a.channel as channel,
                a.ip4_address as ip4_address, 
                a.port as port, 
                a.location as location,
                d.checkpoint_name as checkpoint_name,
                e.floor as floor,
                f.building_name as building_name,
                g.name as instalasi_name,
                b.realname as created_by,
                a.created_date as created_date,
                c.realname as modified_by,
                a.modified_date as modified_date
            FROM
                '.$this->tbl_cctv.' a
                LEFT JOIN '.$this->tbl_admin.' b ON b.id = a.created_by
                LEFT JOIN '.$this->tbl_admin.' c ON c.id = a.modified_by
                LEFT JOIN (
                    '.$this->tbl_checkpoint.' d
                    JOIN (
                        '.$this->tbl_detail_building.' e
                        JOIN (
                            '.$this->tbl_building.' f
                            JOIN '.$this->tbl_instalasi.' g ON g.id = f.instalasi_id
                        ) ON f.id = e.building_id
                    ) ON e.id = d.building_detail_id
                ) ON d.id = a.checkpoint_id
            WHERE 
                a.delete_flag = 0
        ';*/
        $sql ='
            SELECT 
                a.id as id,
                a.channel as channel,
                a.ip4_address as ip4_address, 
                a.port as port, 
                a.location as location,
                b.realname as created_by,
                a.created_date as created_date,
                c.realname as modified_by,
                a.modified_date as modified_date
            FROM
                '.$this->tbl_cctv.' a
                LEFT JOIN '.$this->tbl_admin.' b ON b.id = a.created_by
                LEFT JOIN '.$this->tbl_admin.' c ON c.id = a.modified_by
            WHERE 
                a.delete_flag = 0
        ';
        $columns = ['channel','ip4_address','location','created_date','modified_date'];
        $data['datas'] = $this->pagination_render($sql, $columns);
        $data['filter'] = $this->load->view('backend/esams/filter', $data, true);
        $data['paging'] = $this->load->view('backend/esams/paging', $data, true);
        // ::end
        $data['content'] = $this->load->view('backend/esams/masterdata/cctv', $data, true);
        $this->load->view('backend/index', $data);
    }

    function cctv_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('CCTV Form', 'cctv_form');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $id = $this->input->post('id');
        $data['data'] = ($id)?$this->model_basic->select_where($this->tbl_cctv, 'id', $id)->row():null;
        //echo json_encode($data['data']);die();
        /*if ($data['data']) {
            $checkpoint = $this->model_basic->select_where($this->tbl_checkpoint, 'id', $data['data']->checkpoint_id)->row();
            $data['data']->building_detail_id = $checkpoint?$checkpoint->building_detail_id:null;
            $detail_building = null;
            if ($checkpoint)
                $detail_building = $this->model_basic->select_where($this->tbl_detail_building, 'id', $data['data']->building_detail_id)->row();
            $data['data']->building_id = $detail_building?$detail_building->building_id:null;
            $building = null;
            if ($detail_building)
                $building = $this->model_basic->select_where($this->tbl_building, 'id', $detail_building->building_id)->row();
            $data['data']->instalasi_id = $building?$building->instalasi_id:null;
        }*/
        $data['modul'] = self::modul;
        /*$data['instalasi'] = $this->model_basic->select_all($this->tbl_instalasi);
        $data['building'] = $data['data']?$this->model_basic->select_where($this->tbl_building, 'instalasi_id', $data['data']->instalasi_id)->result():[];
        $data['detail_building'] = $data['data']?$this->model_basic->select_where($this->tbl_detail_building, 'building_id', $data['data']->building_id)->result():[];
        $data['checkpoint'] = $data['data']?$this->model_basic->select_where($this->tbl_checkpoint, 'building_detail_id', $data['data']->building_detail_id)->result():[];*/
        $data['content'] = $this->load->view('backend/esams/masterdata/cctv_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function cctv_form_add() { 
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('CCTV','cctv');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_CREATE);

        // $this->form_validation->set_rules('checkpoint_id', 'checkpoint_id', 'required');
        $this->form_validation->set_rules('ip4_address', 'ip4_address', 'required');
        $this->form_validation->set_rules('port', 'port', 'required');
        $this->form_validation->set_rules('channel', 'channel', 'required');
        $this->form_validation->set_rules('location', 'location', 'required');
        if (!$this->form_validation->run())
            return $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));

        $table_field = $this->db->list_fields($this->tbl_cctv);
        $insert = array();
        foreach ($table_field as $field) {
            if ($field=='id') continue;
            $insert[$field] = $this->input->post($field);
        }
        $insert['status'] = 'Active';
        $insert['created_date'] = date('Y-m-d H:i:s',now());
        $insert['created_by'] = $this->session_admin['admin_id'];
        $insert['delete_flag'] = 0;
        $insert['modified_by'] = null;
        $insert['modified_date'] = null;
        if(count($insert)>0) {
            $do_insert = $this->model_basic->insert_all($this->tbl_cctv,$insert);
            if($do_insert) {
                $this->save_log_admin(2, 'Insert Esams Master Data CCTV');
                $this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
            } else
                $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));
        } else
            $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function cctv_form_edit() { 
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('CCTV','cctv');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_UPDATE);

        // $this->form_validation->set_rules('checkpoint_id', 'checkpoint_id', 'required');
        $this->form_validation->set_rules('ip4_address', 'ip4_address', 'required');
        $this->form_validation->set_rules('port', 'port', 'required');
        $this->form_validation->set_rules('channel', 'channel', 'required');
        $this->form_validation->set_rules('location', 'location', 'required');
        if (!$this->form_validation->run())
            return $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));

        $table_field = $this->db->list_fields($this->tbl_cctv);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }
        $update['status'] = 'Active';
        unset($update['created_date']);
        unset($update['created_by']);
        unset($update['delete_flag']);
        $update['modified_by'] = $this->session_admin['admin_id'];
        $update['modified_date'] = date('Y-m-d H:i:s',now());
        if(count($update)>0) {
            $do_update = $this->model_basic->update($this->tbl_cctv,$update,'id',$update['id']);
            if($do_update) {
                $this->save_log_admin(3, 'Update Esams Master Data CCTV');
                $this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
            } else
                $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));
        } else
            $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function cctv_delete(){
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('CCTV','cctv');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $do_delete = $this->model_basic->delete($this->tbl_cctv,'id',$id);
        if($do_delete){
            $this->save_log_admin(4, 'Delete Esams Master Data CCTV');
            $this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
        }
        else
            $this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }
    /* end:: */

    /* Created: Alvin (atsalvin0017), Date: 2019-09-23 17:34 AM  
    Management MasterData Checklist
    start:: */
    function checklist() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Checklist', 'checklist');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $data['modul'] = self::modul;
        // start:: Pagination
        $sql = '
            SELECT 
                a.id as id,
                a.checklist_name as checklist_name,
                b.realname as created_by,
                a.created_date as created_date,
                c.realname as modified_by,
                a.modified_date as modified_date
            FROM
                '.$this->tbl_checklist.' a
                LEFT JOIN '.$this->tbl_admin.' b ON b.id = a.created_by
                LEFT JOIN '.$this->tbl_admin.' c ON c.id = a.modified_by
            WHERE 
                a.delete_flag = 0
        ';
        $columns = ['checklist_name','created_date','modified_date'];
        $data['datas'] = $this->pagination_render($sql, $columns);
        $data['filter'] = $this->load->view('backend/esams/filter', $data, true);
        $data['paging'] = $this->load->view('backend/esams/paging', $data, true);
        // ::end
        $data['content'] = $this->load->view('backend/esams/masterdata/checklist', $data, true);
        $this->load->view('backend/index', $data);
    }

    function checklist_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Checklist Form', 'checklist_form');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $id = $this->input->post('id');
        $checklist = ($id)?$this->model_basic->select_where($this->tbl_checklist, 'id', $id)->row():null;
        if ($checklist) {
            $checkpoint = $this->model_basic->select_where($this->tbl_checkpoint, 'id', $checklist->checkpoint_id)->row();
            $checklist->building_detail_id = $checkpoint?$checkpoint->building_detail_id:null;
            $building_detail = $this->model_basic->select_where($this->tbl_detail_building, 'id', $checklist->building_detail_id)->row();
            $checklist->building_id = $building_detail?$building_detail->building_id:null;
            $building = $this->model_basic->select_where($this->tbl_building, 'id', $checklist->building_id)->row();
            $checklist->instalasi_id = $building?$building->instalasi_id:null;

            $checklist->building = $this->get_building_by_instalasi($checklist->instalasi_id, false);
            $checklist->building_detail = $this->get_detail_building_by_building($checklist->building_id, false);
            $checklist->checkpoint = $this->get_checkpoint_by_detail_building($checklist->building_detail_id, false);
            $checklist->value = $this->model_basic->select_where($this->tbl_checklist_detail, 'checklist_id', $checklist->id)->result();
        }
        $data['data'] = $checklist;
        $data['modul'] = self::modul;
        $data['instalasi'] = $this->model_basic->select_all($this->tbl_instalasi);
        $data['content'] = $this->load->view('backend/esams/masterdata/checklist_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function checklist_form_add() { 
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Checklist','checklist');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_CREATE);

        $this->form_validation->set_rules('checklist_name', 'checklist_name', 'required');
        if (!$this->form_validation->run())
            return $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));

        $table_field = $this->db->list_fields($this->tbl_checklist);
        $insert = array();
        foreach ($table_field as $field) {
            if ($field=='id') continue;
            $insert[$field] = $this->input->post($field);
        }
        $insert['created_date'] = date('Y-m-d H:i:s',now());
        $insert['created_by'] = $this->session_admin['admin_id'];
        $insert['delete_flag'] = 0;
        $insert['modified_by'] = null;
        $insert['modified_date'] = null;
        if(count($insert)>0) {
            $do_insert = $this->model_basic->insert_all($this->tbl_checklist,$insert);
            if($do_insert) {
                $table_field = $this->db->list_fields($this->tbl_checklist_detail);
                $checklist_value = $this->input->post('checklist_value');
                $value = $this->input->post('value');
                $insert_detail = array();
                foreach ($value as $k => $v) {
                    $insert_detail['checklist_id'] = $do_insert->id;
                    $insert_detail['value'] = $value[$k];
                    $insert_detail['created_by'] = $this->session_admin['admin_id'];
                    $insert_detail['created_date'] = date('Y-m-d H:i:s',now());
                    $insert_detail['modified_by'] = null;
                    $insert_detail['modified_date'] = null;
                    $insert_detail['delete_flag'] = 0;
                    $do_insert_detail = $this->model_basic->insert_all($this->tbl_checklist_detail,$insert_detail);

                }

                $this->save_log_admin(2, 'Insert Esams Master Data Checklist');
                $this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
            } else
                $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));
        } else
            $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function checklist_form_edit() { 
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Checklist','checklist');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_UPDATE);

        $this->form_validation->set_rules('checklist_name', 'checklist_name', 'required');
        if (!$this->form_validation->run())
            return $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));

        try {
            // DB Transaction Start
            $this->db->trans_start();

            $table_field = $this->db->list_fields($this->tbl_checklist);
            $update = array();
            foreach ($table_field as $field) {
                $update[$field] = $this->input->post($field);
            }
            unset($update['created_date']);
            unset($update['created_by']);
            unset($update['delete_flag']);
            $update['modified_by'] = $this->session_admin['admin_id'];
            $update['modified_date'] = date('Y-m-d H:i:s',now());
            if(count($update)>0) {
                $do_update = $this->model_basic->update($this->tbl_checklist,$update,'id',$update['id']);
                if($do_update) {
                    $checklist_value = $this->input->post('checklist_value');
                    $checklist_detail_id = $this->input->post('checklist_detail_id');
                    $value = $this->input->post('value');
                    // Get current value
                    $current_value = []; 
                    $checklist_detail = $this->model_basic->select_wheres($this->tbl_checklist_detail, 'checklist_id', $update['id']);
                    foreach ($checklist_detail as $i=>$v) $current_value[$i] = $v->id;
                    // Delete data current (change delete flag 1)
                    $do_delete = $this->model_basic->delete($this->tbl_checklist_detail,'checklist_id',$update['id']);
                    // echo json_encode($checklist_detail_id); die();
                    foreach ($checklist_value as $k => $v) {
                        if (!$checklist_detail_id[$k]) {
                            $insert_value = [];
                            $insert_value['checklist_id'] = $update['id'];
                            $insert_value['value'] = $value[$k];
                            $insert_value['created_by'] = $this->session_admin['admin_id'];
                            $insert_value['created_date'] = date('Y-m-d H:i:s',now());
                            $insert_value['modified_by'] = null;
                            $insert_value['modified_date'] = null;
                            $insert_value['delete_flag'] = 0;
                            $this->model_basic->insert_all($this->tbl_checklist_detail,$insert_value);
                        } else if (in_array($checklist_detail_id[$k], $current_value)) {
                            $update_value = array();
                            $update_value['id'] = $checklist_detail_id[$k];
                            $update_value['modified_by'] = $this->session_admin['admin_id'];
                            $update_value['modified_date'] = date('Y-m-d H:i:s',now());
                            $update_value['delete_flag'] = 0;
                            $this->model_basic->update($this->tbl_checklist_detail,$update_value,'id',$checklist_detail_id[$k]);
                        }
                    }
                    // DB Transaction Commit
                    $this->db->trans_complete();
                    $this->save_log_admin(3, 'Update Esams Master Data Checklist');
                    $this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
                } else
                    $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));
            } else
                $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
        } catch (Exception $e) {
            // DB Transaction Rollback
            $this->db->trans_rollback();
            $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));
        }
    }

    function checklist_delete(){
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Checklist','checklist');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $do_delete = $this->model_basic->delete($this->tbl_checklist,'id',$id);
        if($do_delete){
            $this->save_log_admin(4, 'Delete Esams Master Data Checklist');
            $this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
        }
        else
            $this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }
    /* end:: */

    /* Created: Alvin (atsalvin0017), Date: 2019-09-24 10:55 AM  
    Management MasterData Pattern
    start:: */
    function pattern() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Pattern', 'pattern');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $data['modul'] = self::modul;
        // start:: Pagination
        $sql = '
            SELECT 
                a.id as id,
                a.pattern_name as pattern_name,
                b.realname as created_by,
                a.created_date as created_date,
                c.realname as modified_by,
                a.modified_date as modified_date
            FROM
                '.$this->tbl_pattern.' a
                LEFT JOIN '.$this->tbl_admin.' b ON b.id = a.created_by
                LEFT JOIN '.$this->tbl_admin.' c ON c.id = a.modified_by
            WHERE 
                a.delete_flag = 0
        ';
        $columns = ['pattern_name','created_date','modified_date'];
        $data['datas'] = $this->pagination_render($sql, $columns);
        $data['filter'] = $this->load->view('backend/esams/filter', $data, true);
        $data['paging'] = $this->load->view('backend/esams/paging', $data, true);
        // ::end
        $data['content'] = $this->load->view('backend/esams/masterdata/pattern', $data, true);
        $this->load->view('backend/index', $data);
    }

    function pattern_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Pattern Form', 'pattern_form');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $id = $this->input->post('id');
        $data['data'] = ($id)?$this->model_basic->select_where($this->tbl_pattern, 'id', $id)->row():null;
        $data['modul'] = self::modul;
        $data['content'] = $this->load->view('backend/esams/masterdata/pattern_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function pattern_form_add() { 
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Pattern','pattern');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_CREATE);

        $this->form_validation->set_rules('pattern_name', 'pattern_name', 'required');
        if (!$this->form_validation->run())
            return $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));

        $table_field = $this->db->list_fields($this->tbl_pattern);
        $insert = array();
        foreach ($table_field as $field) {
            if ($field=='id') continue;
            $insert[$field] = $this->input->post($field);
        }
        $insert['created_date'] = date('Y-m-d H:i:s',now());
        $insert['created_by'] = $this->session_admin['admin_id'];
        $insert['delete_flag'] = 0;
        $insert['modified_by'] = null;
        $insert['modified_date'] = null;
        if(count($insert)>0) {
            $do_insert = $this->model_basic->insert_all($this->tbl_pattern,$insert);
            if($do_insert) {
                $this->save_log_admin(2, 'Insert Esams Master Data Pattern');
                $this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
            } else
                $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));
        } else
            $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function pattern_form_edit() { 
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Pattern','pattern');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_UPDATE);

        $this->form_validation->set_rules('pattern_name', 'pattern_name', 'required');
        if (!$this->form_validation->run())
            return $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));

        $table_field = $this->db->list_fields($this->tbl_pattern);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }
        unset($update['created_date']);
        unset($update['created_by']);
        unset($update['delete_flag']);
        $update['modified_by'] = $this->session_admin['admin_id'];
        $update['modified_date'] = date('Y-m-d H:i:s',now());
        if(count($update)>0) {
            $do_update = $this->model_basic->update($this->tbl_pattern,$update,'id',$update['id']);
            if($do_update) {
                $this->save_log_admin(3, 'Update Esams Master Data Pattern');
                $this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
            } else
                $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));
        } else
            $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function pattern_delete(){
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Pattern','pattern');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $do_delete = $this->model_basic->delete($this->tbl_pattern,'id',$id);
        if($do_delete){
            $this->save_log_admin(4, 'Delete Esams Master Data Pattern');
            $this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
        }
        else
            $this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }
    /* end:: */

    /* Created: Alvin (atsalvin0017), Date: 2019-09-24 11:55 AM  
    Management MasterData Pattern
    start:: */
    function detail_pattern() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Detail Pattern', 'detail_pattern');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $data['modul'] = self::modul;
        // start:: Pagination
        $sql = '
            SELECT 
                a.id as id,
                a.pattern_name as pattern_name,
                count(d.id) as pattern_detail,
                b.realname as created_by,
                a.created_date as created_date,
                c.realname as modified_by,
                a.modified_date as modified_date
            FROM
                '.$this->tbl_pattern.' a
                LEFT JOIN '.$this->tbl_admin.' b ON b.id = a.created_by
                LEFT JOIN '.$this->tbl_admin.' c ON c.id = a.modified_by
                RIGHT JOIN '.$this->tbl_pattern_detail.' d ON d.pattern_id = a.id
            WHERE 
                a.delete_flag = 0
                AND d.delete_flag = 0
            GROUP BY
                a.id
        ';
        $columns = ['pattern_name','created_date','modified_date'];
        $data['datas'] = $this->pagination_render($sql, $columns);
        $data['filter'] = $this->load->view('backend/esams/filter', $data, true);
        $data['paging'] = $this->load->view('backend/esams/paging', $data, true);
        // ::end
        $data['content'] = $this->load->view('backend/esams/masterdata/detail_pattern', $data, true);
        $this->load->view('backend/index', $data);
    }

    function detail_pattern_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Detail Pattern Form', 'detail_pattern_form');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $id = $this->input->post('id');
        $pattern = ($id)?$this->model_basic->select_where($this->tbl_pattern, 'id', $id)->row():null;
        if ($pattern) {
            $detail_pattern = $this->get_detail_pattern($pattern->id, false);
            foreach ($detail_pattern as $k => $v) {
                $v->building = $this->get_building_by_instalasi($v->instalasi_id, false);
                $v->building_detail = $this->get_detail_building_by_building($v->building_id, false);
                $v->checkpoint = $this->get_checkpoint_by_detail_building($v->building_detail_id, false);
            }
        } else $detail_pattern = null;
        $data['data'] = $pattern;
        $data['instalasi'] = $this->get_instalasi(false);
        $data['detail_pattern'] = $detail_pattern;
        $data['modul'] = self::modul;
        $data['pattern'] = $this->model_basic->select_all($this->tbl_pattern);
        $data['content'] = $this->load->view('backend/esams/masterdata/detail_pattern_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function detail_pattern_form_add() { 
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Detail Pattern','detail_pattern');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_CREATE);
        $this->detail_pattern_post($data, 'add');
    }

    function detail_pattern_form_edit() { 
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Detail Pattern','detail_pattern');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_UPDATE);
        $this->detail_pattern_post($data, 'edit');
    }

    function detail_pattern_post($data, $action) {
        if ($action=='add') {
            $pattern_id = $this->input->post('pattern_id');
        } else
        if ($action=='edit') {
            $pattern_id = $this->input->post('id');
        }
        $detail_patterns = $this->input->post('detail_pattern');
        $detail_pattern_ids = $this->input->post('detail_pattern_id');
        $checkpoints = $this->input->post('checkpoint');
        $sequences = $this->input->post('sequence');
        if (empty($checkpoints) or empty($sequences))
            return $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));

        // Get current data group security by group id
        $current_detail_pattern = []; 
        $detail_pattern = $this->model_basic->select_wheres($this->tbl_pattern_detail,'pattern_id',$pattern_id);
        foreach ($detail_pattern as $k => $v) $current_detail_pattern[$k] = $v->id;
        try {
            // DB Transaction Start
            $this->db->trans_start();
            // Delete data current (change delete flag 1)
            $do_delete = $this->model_basic->delete($this->tbl_pattern_detail,'pattern_id',$pattern_id);
            $table_field = $this->db->list_fields($this->tbl_pattern_detail);
            for ($i=0; $i<count($detail_patterns); $i++) {
                $post = [];
                $post['pattern_id'] = $pattern_id;
                $post['sequence'] = $sequences[$i];
                $post['checkpoint_id'] = $checkpoints[$i];
                $post['delete_flag'] = 0;
                if (in_array($detail_pattern_ids[$i], $current_detail_pattern)) {
                    $post['id'] = $detail_pattern_ids[$i];
                    $post['modified_date'] = date('Y-m-d H:i:s',now());
                    $post['modified_by'] = $this->session_admin['admin_id'];
                    // do update member (change delete flag 0)
                    $do_update = $this->model_basic->update($this->tbl_pattern_detail,$post,'id',$post['id']);
                } else {
                    $post['created_date'] = date('Y-m-d H:i:s',now());
                    $post['created_by'] = $this->session_admin['admin_id'];
                    // do insert member
                    $do_insert = $this->model_basic->insert_all($this->tbl_pattern_detail,$post);
                }
            }
            // DB Transaction Commit
            $this->db->trans_complete();
            if ($action=='add') {
                $this->save_log_admin(2, 'Insert Esams Master Data Group Detail');
                $this->returnJson(array('status' => 'ok','msg' => 'Insert data success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
            } else
            if ($action=='edit') {
                $update = [];
                $update['modified_date'] = date('Y-m-d H:i:s',now());
                $update['modified_by'] = $this->session_admin['admin_id'];
                $this->model_basic->update($this->tbl_pattern,$update,'id',$pattern_id);
                $this->save_log_admin(3, 'Update Esams Master Data Group Detail');
                $this->returnJson(array('status' => 'ok','msg' => 'Update data success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
            }
        } catch (Exception $e) {
            // DB Transaction Rollback
            $this->db->trans_rollback();
            $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));
        }
    }

    function detail_pattern_delete(){
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Detail Pattern','detail_pattern');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $do_delete = $this->model_basic->delete($this->tbl_pattern_detail,'pattern_id',$id);
        if($do_delete){
            $this->save_log_admin(4, 'Delete Esams Master Data Detail Pattern');
            $this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
        }
        else
            $this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }
    /* end:: */

    /* Created: Alvin (atsalvin0017), Date: 2019-10-25 11:38 AM  
    Management MasterData Building
    start:: */
    function issue_report_type() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Issue Report Type', 'issue_report_type');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $data['modul'] = self::modul;
        // start:: Pagination
        $sql = '
            SELECT 
                a.id as id,
                a.name as name,
                b.realname as created_by,
                a.created_date as created_date,
                c.realname as modified_by,
                a.modified_date as modified_date
            FROM
                '.$this->tbl_issue_report_type.' a
                LEFT JOIN '.$this->tbl_admin.' b ON b.id = a.created_by
                LEFT JOIN '.$this->tbl_admin.' c ON c.id = a.modified_by
            WHERE 
                a.delete_flag = 0
        ';
        $columns = ['name','created_date','modified_date'];
        $data['datas'] = $this->pagination_render($sql, $columns);
        $data['filter'] = $this->load->view('backend/esams/filter', $data, true);
        $data['paging'] = $this->load->view('backend/esams/paging', $data, true);
        // ::end
        $data['content'] = $this->load->view('backend/esams/masterdata/issue_report_type', $data, true);
        $this->load->view('backend/index', $data);
    }

    function issue_report_type_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Issue Report Type Form', 'issue_report_type_form');
        $data += $this->get_menu();
        //$this->check_userakses($data['function_id'], ACT_READ);
        $id = $this->input->post('id');
        $data['data'] = ($id)?$this->model_basic->select_where($this->tbl_issue_report_type, 'id', $id)->row():null;
        $data['modul'] = self::modul;
        $data['instalasi'] = $this->model_basic->select_all($this->tbl_instalasi);
        $data['content'] = $this->load->view('backend/esams/masterdata/issue_report_type_form', $data, true);
        $this->load->view('backend/index', $data);
    }

    function issue_report_type_form_add() { 
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Issue Report Type','issue_report_type');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_CREATE);

        $this->form_validation->set_rules('name', 'name', 'required');
        if (!$this->form_validation->run())
            return $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));

        $table_field = $this->db->list_fields($this->tbl_issue_report_type);
        $insert = array();
        foreach ($table_field as $field) {
            if ($field=='id') continue;
            $insert[$field] = $this->input->post($field);
        }
        $insert['created_date'] = date('Y-m-d H:i:s',now());
        $insert['created_by'] = $this->session_admin['admin_id'];
        $insert['delete_flag'] = 0;
        $insert['modified_by'] = null;
        $insert['modified_date'] = null;
        if(count($insert)>0) {
            $do_insert = $this->model_basic->insert_all($this->tbl_issue_report_type,$insert);
            if($do_insert) {
                $this->save_log_admin(2, 'Insert Esams Master Data Issue Report Type');
                $this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
            } else
                $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));
        } else
            $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function issue_report_type_form_edit() { 
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Issue Report Type','issue_report_type');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_UPDATE);

        $this->form_validation->set_rules('name', 'name', 'required');
        if (!$this->form_validation->run())
            return $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));

        $table_field = $this->db->list_fields($this->tbl_issue_report_type);
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }
        unset($update['created_date']);
        unset($update['created_by']);
        unset($update['delete_flag']);
        $update['modified_by'] = $this->session_admin['admin_id'];
        $update['modified_date'] = date('Y-m-d H:i:s',now());
        if(count($update)>0) {
            $do_update = $this->model_basic->update($this->tbl_issue_report_type,$update,'id',$update['id']);
            if($do_update) {
                $this->save_log_admin(3, 'Update Esams Master Data Issue Report Type');
                $this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
            } else
                $this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));
        } else
            $this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function issue_report_type_delete(){
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function('Issue Report Type','issue_report_type');
        $data += $this->get_menu();
        // $this->check_userakses($data['function_id'], ACT_DELETE);
        $id = $this->input->post('id');
        $do_delete = $this->model_basic->delete($this->tbl_issue_report_type,'id',$id);
        if($do_delete){
            $this->save_log_admin(4, 'Delete Esams Master Data Issue Report Type');
            $this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => self::modul.'/'.$data['controller'].'/'.$data['function']));
        }
        else
            $this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }
    /* end:: */

    /* Created: Alvin (atsalvin0017), Date: 2019-09-24 14:00 PM  
    Management GET for Ajax
    start:: */
    function get_detail_pattern($pattern_id=1, $json=true) {
        $detail_pattern = $this->model_basic->select_where($this->tbl_pattern_detail, 'pattern_id', $pattern_id)->result();
        foreach ($detail_pattern as $k => $v) {
            $checkpoint = $this->model_basic->select_where($this->tbl_checkpoint, 'id', $v->checkpoint_id)->row();
            $v->building_detail_id = $checkpoint?$checkpoint->building_detail_id:null;
            $building_detail = $this->model_basic->select_where($this->tbl_detail_building, 'id', $v->building_detail_id)->row();
            $v->building_id = $building_detail?$building_detail->building_id:null;
            $building = $this->model_basic->select_where($this->tbl_building, 'id', $v->building_id)->row();
            $v->instalasi_id = $building?$building->instalasi_id:null;
        }
        if ($json==false) return $detail_pattern;
        return $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($detail_pattern));
    }
    function get_checkpoint() {
        $checkpoint = $this->model_basic->select_all($this->tbl_checkpoint);
        foreach ($checkpoint as $k => $v) {
            $cctv = $this->model_basic->select_where($this->tbl_cctv, 'id', $v->cctv_csc_id)->row();
            $v->checkpoint_name = $v->checkpoint_name." (CCTV: ".$cctv->location.")";
        }
        return $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($checkpoint));
    }
    function get_instalasi($json=true) {
        $instalasi = $this->model_basic->select_all($this->tbl_instalasi);
        if ($json==false) return $instalasi;
        return $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($instalasi));
    }
    function get_building_by_instalasi($instalasi_id=null, $json=true) {
        $building = ($instalasi_id)?$this->model_basic->select_where($this->tbl_building, 'instalasi_id', $instalasi_id)->result():[];
        if ($json==false) return $building;
        return $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($building));
    }
    function get_detail_building_by_building($building_id=null, $json=true) {
        $building_detail = ($building_id)?$this->model_basic->select_where($this->tbl_detail_building, 'building_id', $building_id)->result():[];
        if ($json==false) return $building_detail;
        return $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($building_detail));
    }
    function get_checkpoint_by_detail_building($building_detail_id=null, $json=true) {
        $checkpoint = ($building_detail_id)?$this->model_basic->select_where($this->tbl_checkpoint, 'building_detail_id', $building_detail_id)->result():[];
        if ($json==false) return $checkpoint;
        return $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($checkpoint));
    }
    function get_checkpoint_by_building($building_id=null, $json=true) {
        $building_detail_id = [];
        if ($building_id) {
            $building_detail = $this->model_basic->select_where($this->tbl_detail_building, 'building_id', $building_id)->result();
            foreach ($building_detail as $k => $v)
                $building_detail_id[$k] = $v->id;
            $building_detail_id = implode(',', $building_detail_id);
        }
        $sql = "
            select 
                a.id as id, 
                concat(a.checkpoint_name, ' (CCTV: ', b.location, ')') as checkpoint_name
            from 
                ".$this->tbl_checkpoint." a
                join ".$this->tbl_cctv." b on b.id = a.cctv_csc_id and b.delete_flag = 0
            where 
                a.delete_flag = 0
                and a.building_detail_id in (".$building_detail_id.") 
            ";
        $checkpoint = $this->model_basic->select_all_by_query($sql);
        if ($json==false) return $checkpoint;
        return $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($checkpoint));
    }
    function get_pattern_detail_by_pattern($pattern_id=null) {
        $pattern_detail = ($pattern_id)?$this->model_basic->select_where_order($this->tbl_pattern_detail, 'pattern_id', $pattern_id, 'sequence', 'asc')->result():[];
        return $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode($pattern_detail));
    }
    /* end:: */

}
