<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_status_kerawanan extends PX_Controller {

	public function __construct() {
		parent::__construct();
		$this->check_login();
		$this->controller_attr = array('controller' => 'admin_status_kerawanan','controller_name' => 'Admin Status Kerawanan','controller_id' => 0);
	}

	public function index()
	{
		$this->peta();
	}

	// peta status kerawanan //
	function peta(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Peta Status Kerawanan','peta');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);
		$data['province'] = $this->model_basic->select_all_noflag($this->tbl_province);
		$data['status_hijau'] = $this->model_basic->select_where($this->tbl_instalasi,'status_kerawanan',1)->num_rows();
		$data['status_kuning'] = $this->model_basic->select_where($this->tbl_instalasi,'status_kerawanan',2)->num_rows();
		$data['status_merah'] = $this->model_basic->select_where($this->tbl_instalasi,'status_kerawanan',3)->num_rows();
		$data['content'] = $this->load->view('backend/admin_status_kerawanan/peta',$data,true);
		$this->load->view('backend/index',$data); 
    }

    function peta_form(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Admin Status Kerawanan','peta');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);
		$id = $this->input->post('id');

		$data['company'] = $this->model_basic->select_all_order($this->tbl_perusahaan, 'name', 'asc');
		if($id){
			$data['data'] = $this->model_basic->select_where($this->tbl_status_kerawanan,'id',$id)->row();
			$content = new domDocument;
			libxml_use_internal_errors(true);
			$content->loadHTML($data['data']->content);
			libxml_use_internal_errors(false);
			$content->preserveWhiteSpace = false;
			$images = $content->getElementsByTagName('img');
			if($images){
				foreach ($images as $image) {
				  $data['data']->image[] = $image->getAttribute('src');
				}
			}
		}
		else
			$data['data'] = null;
		$data['content'] = $this->load->view('backend/admin_status_kerawanan/peta_form',$data,true);
		$this->load->view('backend/index',$data); 
    }

    function news_detail($id)
    {
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Peta Status Kerawanan','peta');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);

		$data['company'] = $this->model_basic->select_all($this->tbl_perusahaan);
		if($id){
			$data['data'] = $this->model_basic->select_where($this->tbl_status_kerawanan,'id',$id)->row();
			$content = new domDocument;
			libxml_use_internal_errors(true);
			$content->loadHTML($data['data']->content);
			libxml_use_internal_errors(false);
			$content->preserveWhiteSpace = false;
			$images = $content->getElementsByTagName('img');
			if($images){
				foreach ($images as $image) {
				  $data['data']->image[] = $image->getAttribute('src');
				}
			}
		}
		else
			$data['data'] = null;

		$data['content'] = $this->load->view('backend/admin_status_kerawanan/berita_detail',$data,true);
		$this->load->view('backend/index',$data); 
    }

    function news_detail_map($id)
    {
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Peta Status Kerawanan','peta');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);

		$data['company'] = $this->model_basic->select_all($this->tbl_perusahaan);
		if($id){
			$data['data'] = $this->model_basic->select_where($this->tbl_status_kerawanan,'id',$id)->row();
			$content = new domDocument;
			libxml_use_internal_errors(true);
			$content->loadHTML($data['data']->content);
			libxml_use_internal_errors(false);
			$content->preserveWhiteSpace = false;
			$images = $content->getElementsByTagName('img');
			if($images){
				foreach ($images as $image) {
				  $data['data']->image[] = $image->getAttribute('src');
				}
			}
		}
		else
			$data['data'] = null;

		$data['content'] = $this->load->view('backend/admin_status_kerawanan/berita_detail_map',$data,true);
		$this->load->view('backend/index',$data); 
    }

    //ajax
    function get_instalasi_list() {
    	$perusahaan_id = $this->input->post('id');
    	$opt = "<option value=\"0\">Pilih Instalasi</option>";
    	if ($perusahaan_id == 0) {
    		$opt = $opt;
    	}else{
    		$instalasi = $this->model_basic->select_all($this->tbl_instalasi,'perusahaan_id',$perusahaan_id);
	    	foreach ($instalasi as $data_row) {
	    		$opt .= "<option value=\"".$data_row->id."\">".$data_row->name."</option>";  
	    	}
    	}

    	echo $opt;
    }

    public function ajax_get_instalasi() {
		$perusahaan_id = $this->input->post('id');

		$results = $this->model_basic->select_where($this->tbl_instalasi, 'perusahaan_id', $perusahaan_id);
		if ($results->num_rows() > 0) {
			$this->returnJson(array('status' => 'ok', 'result' => $results->result()));
		}else{
			$this->returnJson(array('status' => 'not_ok'));
		}
	}

    function get_instalasi_detail() {
    	$instalasi_id = $this->input->post('id');

    	$instalasi = $this->model_basic->select_where($this->tbl_instalasi,'id',$instalasi_id)->row();
    	$jenis_bisnis_nm = $this->model_basic->select_where($this->tbl_jenis_bisnis,'id',$instalasi->jenis_bisnis_id)->row()->name;

    	echo json_encode(array('jenis_bisnis_id' => $instalasi->jenis_bisnis_id, 'jenis_bisnis_nm' => $jenis_bisnis_nm, 'instalasi_alamat' => $instalasi->alamat));
    }

    function get_all_location() {
    	$status_kerawanan = $this->model_basic->select_all($this->tbl_instalasi);
    	$data = array();
    	foreach ($status_kerawanan as $data_row) {
    		$instalasi_nm = $data_row->name;
    		$instalasi_alamat = $data_row->alamat;
    		$instalasi_id = $data_row->id;
    		if ($data_row->status_kerawanan == 3) {
    			$icon_status = "marker-red.png";
    		}elseif ($data_row->status_kerawanan == 2) {
    			$icon_status = "marker-yellow.png";
    		}else{
    			$icon_status = "marker-green.png";
    		}
    		$data_new = array('instalasi_id' => $instalasi_id,'instalasi_nm' => $instalasi_nm, 'instalasi_alamat' => $instalasi_alamat, 'latitude' => $data_row->coordinate_x, 'longitude' => $data_row->coordinate_y, 'icon_status' => $icon_status);
    		array_push($data, $data_new);
    	}

    	echo json_encode($data);
    }
    
    function get_location_by_status() {
        $status = $this->input->post('status');
        if($status != 0)
        {
            $status_kerawanan = $this->model_basic->select_where($this->tbl_instalasi, 'status_kerawanan', $status)->result();
        }
        else
        {
            $status_kerawanan = $this->model_basic->select_all($this->tbl_instalasi);
        }
    	$data = array();
    	foreach ($status_kerawanan as $data_row) {
    		$instalasi_nm = $data_row->name;
    		$instalasi_alamat = $data_row->alamat;
    		$instalasi_id = $data_row->id;
    		if ($data_row->status_kerawanan == 3) {
    			$icon_status = "marker-red.png";
    		}elseif ($data_row->status_kerawanan == 2) {
    			$icon_status = "marker-yellow.png";
    		}else{
    			$icon_status = "marker-green.png";
    		}
    		$data_new = array('instalasi_id' => $instalasi_id,'instalasi_nm' => $instalasi_nm, 'instalasi_alamat' => $instalasi_alamat, 'latitude' => $data_row->coordinate_x, 'longitude' => $data_row->coordinate_y, 'icon_status' => $icon_status);
    		array_push($data, $data_new);
    	}

    	echo json_encode($data);
    }

    function get_all_news() {
    	$id = $this->input->post('id');
    	$data = $this->model_basic->select_where_limit_order($this->tbl_status_kerawanan,'instalasi_id',$id,5,'DESC','date_created')->result();
    	if($data){
    		foreach ($data as $d) {
    			$d->date_created = date('d-m-Y H:i:s',strtotime($d->date_created));
    			$d->content = strip_tags($d->content);
            	$d->content = substr($d->content, 0, 350) . '....';
            	
    		}
    		$this->returnJson(array('status' => 'ok','data' => $data));
    	}
    	else
    		$this->returnJson(array('status' => 'error'));
    }

    function get_detail_news() {
    	$id = $this->input->post('id');
    	$data = $this->model_basic->select_where($this->tbl_status_kerawanan,'id',$id)->row();
    	if($data){
    		$this->returnJson(array('status' => 'ok','data' => $data));
    	}
    	else
    		$this->returnJson(array('status' => 'error'));
    }

    //e.o. ajax


    function peta_add(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Admin Status Kerawanan','peta');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_CREATE);

		$images = $this->input->post('images');
		$news_title = $this->input->post('title');
		$news_content = $this->input->post('content');
		$date_created = date('Y-m-d H:i:s',now());
		$table_field = $this->db->list_fields($this->tbl_status_kerawanan);
		$insert = array();
		foreach ($table_field as $field) {
			$insert[$field] = $this->input->post($field);
		}
		$insert['date_created'] = $date_created;
		$insert['date_modified'] = date('Y-m-d H:i:s',now());
		$insert['id_created'] = $this->session_admin['admin_id'];
		$insert['id_modified'] = $this->session_admin['admin_id'];
		
		if($insert['perusahaan_id'] && $insert['instalasi_id']){
			$do_insert = $this->model_basic->insert_all($this->tbl_status_kerawanan,$insert);
			if($do_insert){
				$update = array(
					'status_kerawanan' => $insert['status']
					);
				$do_update = $this->model_basic->update($this->tbl_instalasi,$update,'id',$insert['instalasi_id']);
				if($images){
					if(!is_dir(FCPATH . "assets/uploads/status_kerawanan/".$do_insert->id))
						mkdir(FCPATH . "assets/uploads/status_kerawanan/".$do_insert->id);
					$content = $insert['content']; 
					foreach($images as $im) {
						if(strpos($content,$im) !== false)
						{
							$new_im = 'assets/uploads/status_kerawanan/'.$do_insert->id.'/'.basename($im);
							@copy($im,$new_im);
						    $content = str_replace($im, $new_im, $content);
						}
					}
					$update_content['content'] = $content;
					$do_update = $this->model_basic->update($this->tbl_status_kerawanan,$update_content,'id',$do_insert->id);
					$this->delete_temp('temp_folder');
				}
                                $instalasi = $this->model_basic->select_where($this->tbl_instalasi, 'id', $insert['instalasi_id'])->row();
                                $this->save_log_admin(ACT_CREATE, 'Insert New Status Kerawanan '.$instalasi->name);
				$this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => $data['controller'].'/'.$data['function']));
			}
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    //Approval//
	function approval()
	{
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Approval Status Kerawanan','approval');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);

		$data['content'] = $this->load->view('backend/admin_status_kerawanan/approval',$data,true);
		$this->load->view('backend/index',$data); 
	}

	function approval_detail($id){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function('Approval Status Kerawanan','approval');
		$data += $this->get_menu();
		$this->check_userakses($data['function_id'], ACT_READ);

		if($id){
			$data['data'] = $data_result = $this->model_basic->select_where($this->tbl_status_kerawanan,'id',$id)->row();
			$data['data']->status = $this->model_basic->select_where($this->tbl_status_kerawanan_status,'id',$data_result->status)->row()->name;
			$data['perusahaan'] = $this->model_basic->select_where($this->tbl_perusahaan,'id',$data_result->perusahaan_id)->row()->name;
			$data['instalasi'] = $instalasi = $this->model_basic->select_where($this->tbl_instalasi,'id',$data_result->instalasi_id)->row();
		}
		else{
			$data['data'] = null;
		}
		$data['content'] = $this->load->view('backend/admin_status_kerawanan/approval_detail',$data,true);
		$this->load->view('backend/index',$data); 
    }

    function do_approve()
    {
    	$id = $this->input->post('id');

    	$result = $this->model_basic->update($this->tbl_status_kerawanan, array('is_approved' => 1, 'id_approved' => $this->session_admin['admin_id'], 'date_approved' => date('Y-m-d H:i:s',now())), 'id', $id);
    	$status_kerawanan = $this->model_basic->select_where($this->tbl_status_kerawanan, 'id', $id);
    	$instalasi_id = $status_kerawanan->row()->instalasi_id;
    	$status_kerawanan_id = $status_kerawanan->row()->id;
    	if ($result) {
    		//update status instalasi
                $instalasi = $this->model_basic->select_where($this->tbl_instalasi, 'id', $instalasi_id)->row();
                $this->save_log_admin(ACT_UPDATE, 'Approve Status Kerawanan '.$instalasi->name);
    		$this->model_basic->update($this->tbl_instalasi, array('status_kerawanan' => $status_kerawanan_id), 'id', $instalasi_id);
    		$this->returnJson(array('status' => 'ok', 'redirect' => 'admin_status_kerawanan/approval'));
    	}else{
    		$this->returnJson(array('status' => 'not'));
    	}
    	
    }

	public function ajax_approval_list()
	{
		$list = $this->model_status_kerawanan->get_datatables();
		//die(print_r($this->db->last_query()));
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $data_row) {
			//render data
			if ($data_row->is_approved == 1) {
				$status_approval = "Approved";
			}else{
				$status_approval = "-";
			}
			$no++;
			
			$row = array();
			$row[] = $no;
			$row[] = "<a href='admin_status_kerawanan/approval_detail/".$data_row->id."'>".$data_row->instalasi_nm."</a>";
			$row[] = $data_row->perusahaan_nm;
			$row[] = $data_row->status_nm;
			$row[] = $status_approval;
			$row[] = date('d F Y',strtotime($data_row->date_created));
			//add html for action
			$row[] = '<div class="text-center">
						
						<input type="hidden" name="id" value="'.$data_row->id.'">
						
						<button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Delete" data-placement="top" data-toggle="tooltip" data-target-id="'.$data_row->id.'"><i class="fa fa-trash-o"></i></button>
						
						</div>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_status_kerawanan->count_all(),
						"recordsFiltered" => $this->model_status_kerawanan->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}
        function approval_delete(){
            $data = $this->get_app_settings();
            $data += $this->controller_attr;
            $data += $this->get_function('Approval Status Kerawanan','approval');
            $data += $this->get_menu();
            $this->check_userakses($data['function_id'], ACT_DELETE);
            $id = $this->input->post('id');
            $do_delete = $this->model_basic->delete($this->tbl_status_kerawanan,'id',$id);
            if($do_delete){
                    $this->save_log_admin(ACT_DELETE, 'Delete Approval Status Kerawanan');
                    $this->delete_folder('status_kerawanan/'.$id);
                    $this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => $data['controller'].'/'.$data['function']));
            }
            else
                    $this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
        }

}

/* End of file admin_status_kerawanan.php */
/* Location: ./application/controllers/admin_status_kerawanan.php */