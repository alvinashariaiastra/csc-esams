<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class user_company extends PX_Controller {

	public function __construct() {
		parent::__construct();
		$this->user_check_login();
		$this->controller_attr = array('controller' => 'user_company','controller_name' => 'Admin Perusahaan','controller_id' => 0);
	}

	public function index()
	{
		
	}

	//assessment 2004//
	function assessment2004()
	{
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function_user('Asesmen 2004','assessment2004');
		$data += $this->user_get_menu();
		$this->check_userakses_user($data['function_id'], ACT_READ);

		$data['rules'] = $this->model_basic->select_where($this->tbl_rules_asms_2004,'status',1)->result();
		$data['data'] = $this->model_basic->select_where($this->tbl_assessment2004,'instalasi_id',$this->session_user['id_instalasi'])->result();
		$data['content'] = $this->load->view('user_backend/admin_company/assessment2004',$data,true);
		$this->load->view('user_backend/index',$data); 
	}

	function assessment2004_view($id){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function_user('Asesmen 2004 Details','assessment2004');
		$data += $this->user_get_menu();
		$this->check_userakses_user($data['function_id'], ACT_READ);

		if($id){
			$id = $id;
			$data['rules'] = $this->model_basic->select_where($this->tbl_asesmen_2004_value,'id_asesmen',$id)->result();
			$total_value = 0;
			if(count($data['rules']) > 0) {
				foreach ($data['rules'] as $r) {
					$rules_data = $this->model_basic->select_where($this->tbl_rules_asms_2004,'id',$r->id_rules)->row();
					$r->name = $rules_data->name;
					$total_value = $total_value + $r->value;
				}
			}
			else
				$data['rules'] = $this->model_basic->select_where($this->tbl_rules_asms_2004,'status',1)->result();

			$data['data'] = $this->model_basic->select_where($this->tbl_assessment2004,'id',$id)->row();

			$data['data']->nilai_akhir = $this->check_colour(number_format($data['data']->nilai_akhir,2),'Nilai Akhir (Asesmen 2004)');

			$instalasi = $this->model_basic->select_where($this->tbl_instalasi,'id',$data['data']->instalasi_id)->row();
			if($instalasi)
				$data['data']->nama_instalasi = $instalasi->name;
			else
				$data['data']->nama_instalasi = ' ';

			$data['content'] = $this->load->view('user_backend/admin_company/assessment2004_view',$data,true);
			$this->load->view('user_backend/index',$data);
		}
		else
			redirect($data['function']);
    }

	function assessment2004_form(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function_user('Asesmen 2004','assessment2004');
		$data += $this->user_get_menu();
		$this->check_userakses_user($data['function_id'], ACT_READ);
		$id = $this->input->post('id');

		$data['company'] = $this->model_basic->select_where($this->tbl_instalasi,'delete_flag',0);
		if($id){
			$data['rules'] = $this->model_basic->select_where($this->tbl_asesmen_2004_value,'id_asesmen',$id)->result();
			if(count($data['rules']) > 0) {
				foreach ($data['rules'] as $r) {
					$rules_data = $this->model_basic->select_where($this->tbl_rules_asms_2004,'id',$r->id_rules)->row();
					$r->name = $rules_data->name;
					$r->id = $rules_data->id;
				}
			}
			else
				$data['rules'] = $this->model_basic->select_where($this->tbl_rules_asms_2004,'status',1)->result();
			$data['data'] = $this->model_basic->select_where($this->tbl_assessment2004,'id',$id)->row();
		}
		else{
			$data['rules'] = $this->model_basic->select_where($this->tbl_rules_asms_2004,'status',1)->result();
			$data['data'] = null;
		}
		$data['content'] = $this->load->view('user_backend/admin_company/assessment2004_form',$data,true);
		$this->load->view('backend/index',$data); 
    }

    function assessment2004_add(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function_user('Asesmen 2004','assessment2004');
		$data += $this->user_get_menu();
		$this->check_userakses_user($data['function_id'], ACT_READ);

		$table_field = $this->db->list_fields($this->tbl_assessment2004);
		$insert = array();
		foreach ($table_field as $field) {
			$insert[$field] = $this->input->post($field);
		}
		$insert['date_created'] = date('Y-m-d H:i:s',now());
		$insert['id_created'] = $this->session_admin['admin_id'];
		if($insert['instalasi_id']){
			$do_insert = $this->model_basic->insert_all($this->tbl_assessment2004,$insert);
			if($do_insert){
				$rules_id = $this->input->post('rules_id');
				$rules_value = $this->input->post('rules_value');
				$insert_rules_value = array();
				for ($i=0; $i < count($rules_id); $i++) { 
					$rules_value_data = array(
						'id_asesmen' => $do_insert->id,
						'id_rules' => $rules_id[$i],
						'value' => $rules_value[$rules_id[$i]]
						);
					array_push($insert_rules_value, $rules_value_data);
				}
				if(count($insert_rules_value) > 0)
					$do_insert_rules_value = $this->model_basic->insert_all_batch($this->tbl_asesmen_2004_value,$insert_rules_value);
                                $this->save_log_user(ACT_CREATE, 'Insert Assessment 2004');
				$this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => $data['controller'].'/'.$data['function']));
			}
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function assessment2004_edit(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function_user('Asesmen 2004','assessment2004');
		$data += $this->user_get_menu();
		$this->check_userakses_user($data['function_id'], ACT_UPDATE);
		
		$table_field = $this->db->list_fields($this->tbl_assessment2004);
		$update = array();
		foreach ($table_field as $field) {
			$update[$field] = $this->input->post($field);
		}
		unset($update['date_created']);
		$update['date_modified'] = date('Y-m-d H:i:s',now());
		$update['id_modified'] = $this->session_admin['admin_id'];
		$rules_id = $this->input->post('rules_id');
		$rules_value = $this->input->post('rules_value');
		$insert_rules_value = array();
		for ($i=0; $i < count($rules_id); $i++) { 
			$rules_value_data = array(
				'id_asesmen' => $update['id'],
				'id_rules' => $rules_id[$i],
				'value' => $rules_value[$rules_id[$i]]
				);
			array_push($insert_rules_value, $rules_value_data);
		}
		if(count($insert_rules_value) > 0){
			$this->model_basic->delete_full($this->tbl_asesmen_2004_value,'id_asesmen',$update['id']);
			$do_insert_rules_value = $this->model_basic->insert_all_batch($this->tbl_asesmen_2004_value,$insert_rules_value);
		}
				
		if($update['instalasi_id']){
			$do_update = $this->model_basic->update($this->tbl_assessment2004,$update,'id',$update['id']);
                        $this->save_log_user(ACT_UPDATE, 'Update Assessment 2004');
			if($do_update)
				$this->returnJson(array('status' => 'ok','msg' => 'Update success','redirect' => $data['controller'].'/'.$data['function']));
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when updating data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function assessment2004_delete(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function_user('Asesmen 2004','assessment2004');
		$data += $this->user_get_menu();
		$this->check_userakses_user($data['function_id'], ACT_DELETE);
		$id = $this->input->post('id');
		$do_delete = $this->model_basic->delete($this->tbl_assessment2004,'id',$id);
		if($do_delete){
                        $this->save_log_user(ACT_DELETE, 'Delete Assessment 2004');
			$this->delete_folder('assessment2004/'.$id);
			$this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => $data['controller'].'/'.$data['function']));
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }

	public function ajax_assessment2004_list()
	{
		$list = $this->model_assessment2004->get_datatables_user();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $data_row) {
				//render data
				$no++;

				$row = array();
				$row[] = $no;
				$row[] = $data_row->name;
				$row[] = $data_row->periode;
				$row[] = number_format($data_row->element_1,2);
				$row[] = number_format($data_row->element_2,2);
				$row[] = number_format($data_row->element_3,2);
				$row[] = number_format($data_row->element_4,2);
				$row[] = $this->check_colour(number_format($data_row->nilai_akhir,2),'Nilai Akhir (Asesmen 2004)');
				$row[] = '<a href="user_company/assessment2004_view/'.$data_row->id.'">Critical Point</a>';
				$row[] = date('d-m-Y',strtotime($data_row->date_created));
				//add html for action
			
				$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_assessment2004->count_all_user(),
						"recordsFiltered" => $this->model_assessment2004->count_filtered_user(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	function assessment_2004_check_colour(){
		$nilai = $this->input->post('nilai');
		if($nilai)
			$warna = $this->check_colour_text($nilai,'Nilai Akhir (Asesmen 2004)');
		else
			$warna = $this->check_colour_text(0,'Nilai Akhir (Asesmen 2004)');
		$result = array(
			'status' => 'ok',
			'warna' => $warna
			);
		$this->returnJson($result);
	}

	//assessment 2015//
	function assessment2015(){
		$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function_user('Asesmen 2015','assessment2015');
		$data += $this->user_get_menu();
		$this->check_userakses_user($data['function_id'], ACT_READ);
		$data['data'] = $this->model_basic->select_all($this->tbl_assessment2015);
		$data['content'] = $this->load->view('user_backend/admin_company/assessment2015',$data,true);
		$this->load->view('user_backend/index',$data); 
	}

	function assessment2015_view($id, $param=null){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function_user('Asesmen 2015 Details','assessment2015');
		$data += $this->user_get_menu();
		$this->check_userakses_user($data['function_id'], ACT_READ);

		if($id){
			$id = $id;
			$data['rules'] = $this->model_basic->select_where($this->tbl_asesmen_2015_value,'id_asesmen',$id)->result();
			$total_value = 0;
			if(count($data['rules']) > 0) {
				foreach ($data['rules'] as $r) {
					$rules_data = $this->model_basic->select_where($this->tbl_rules_asms_2015,'id',$r->id_rules)->row();
					$r->name = $rules_data->name;
					$total_value = $total_value + $r->value;
				}
			}
			else
				$data['rules'] = $this->model_basic->select_where($this->tbl_rules_asms_2015,'status',1)->result();

			$data['data'] = $this->model_basic->select_where($this->tbl_assessment2015,'id',$id)->row();

			// Add by: Alvin (atsalvin0017), Date: 2019-08-19 11:46 AM
			$data['history_revise'] = [];
			if ($param and $param=='detail') {
				$history = $this->model_basic->select_where($this->tbl_assessment2015_history,'asesmen_2015_id',$data['data']->id)->result();
				foreach ($history as $k=>$row) {
					if ($row->revise)
						$data['history_revise'][] = $row;
				}
			}
			//

			$data['data']->pemenuhan_system = $this->check_colour(number_format($total_value/4,2),'Pemenuhan System (Asesmen 2015)');
			$data['data']->security_performance = $this->check_colour(number_format($data['data']->security_performance,2),'Security Performance (Asesmen 2015)');
			$data['data']->security_reliability = $this->check_colour(number_format(($data['data']->people + $data['data']->device_and_infrastructure) / 2,2),'Security Reliability (Asesmen 2015)');
			$data['data']->csi = $this->check_colour(number_format($data['data']->csi,2),'CSI (Asesmen 2015)');
			$data['data']->nilai_akhir = $this->check_colour(number_format($data['data']->nilai_akhir,2),'Nilai Akhir (Asesmen 2015)');

			$instalasi = $this->model_basic->select_where($this->tbl_instalasi,'id',$data['data']->instalasi_id)->row();
			if($instalasi)
				$data['data']->nama_instalasi = $instalasi->name;
			else
				$data['data']->nama_instalasi = ' ';

			$data['content'] = $this->load->view('user_backend/admin_company/assessment2015_view',$data,true);
			$this->load->view('user_backend/index',$data);
		}
		else
			redirect($data['function']);
    }

	function assessment2015_form(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function_user('Asesmen 2015','assessment2015');
		$data += $this->user_get_menu();
		$this->check_userakses_user($data['function_id'], ACT_CREATE);
		$id = $this->input->post('id');

		$data['company'] = $this->model_basic->select_where($this->tbl_instalasi,'delete_flag',0);
		if($id){
			$data['rules'] = $this->model_basic->select_where($this->tbl_asesmen_2015_value,'id_asesmen',$id)->result();
			if(count($data['rules']) > 0) {
				foreach ($data['rules'] as $r) {
					$rules_data = $this->model_basic->select_where($this->tbl_rules_asms_2015,'id',$r->id_rules)->row();
					$r->name = $rules_data->name;
					$r->id = $rules_data->id;
				}
			}
			else
				$data['rules'] = $this->model_basic->select_where($this->tbl_rules_asms_2015,'status',1)->result();
			$data['data'] = $this->model_basic->select_where($this->tbl_assessment2015,'id',$id)->row();

			// Add by: Alvin (atsalvin0017), Date: 2019-08-19 11:46 AM
			$data['history_revise'] = [];
			// if ($param and $param=='detail') {
				$history = $this->model_basic->select_where($this->tbl_assessment2015_history,'asesmen_2015_id',$data['data']->id)->result();
				foreach ($history as $k=>$row) {
					if ($row->revise)
						$data['history_revise'][] = $row;
				}
			// }
			//
		}
		else{
			$data['rules'] = $this->model_basic->select_where($this->tbl_rules_asms_2015,'status',1)->result();
			$data['data'] = null;
			$data['history_revise'] = [];
		}
		$data['content'] = $this->load->view('user_backend/admin_company/assessment2015_form',$data,true);
		$this->load->view('user_backend/index',$data); 
    }

    /*
		Updated by: Alvin (atsalvin0017)
		Date: 2019-08-12 13:53 PM
		Action: Add status for add assessment
    */
    function assessment2015_add(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function_user('Asesmen 2015','assessment2015');
		$data += $this->user_get_menu();
		$this->check_userakses_user($data['function_id'], ACT_CREATE);

		$table_field = $this->db->list_fields($this->tbl_assessment2015);
		$insert = array();
		foreach ($table_field as $field) {
			// Add by: Alvin (atsalvin0017), Date: 2019-08-12 13:53 PM
			if ($field!='id') { 
				$insert[$field] = $this->input->post($field);
			}
			// $insert[$field] = $this->input->post($field);
		}
		$insert['date_created'] = date('Y-m-d H:i:s',now());
		$insert['id_created'] = $this->session_user['admin_id'];
		$insert['status'] = 2; // Add by: Alvin (atsalvin0017), Date: 2019-08-12 13:53 PM
		if($insert['instalasi_id']){
			$do_insert = $this->model_basic->insert_all($this->tbl_assessment2015,$insert);
			if($do_insert){
				// Insert to Asesmen 2015 history
				$insert_asesment2015_history = [];
				$insert_asesment2015_history['asesmen_2015_id'] = $do_insert->id;
				$insert_asesment2015_history['user_id'] = $this->session_user['admin_id'];
				$insert_asesment2015_history['user_status'] = 'Assessor';
				$insert_asesment2015_history['action'] = 'Submit';
				$insert_asesment2015_history['date_created'] = date('Y-m-d H:i:s',now());
				$insert_asesment2015_history['status'] = 1;
				$do_insert_asesment2015_history = $this->model_basic->insert_all($this->tbl_assessment2015_history,$insert_asesment2015_history);
				if ($do_insert_asesment2015_history) {
					$rules_id = $this->input->post('rules_id');
					$rules_value = $this->input->post('rules_value');
					$insert_rules_value = array();
					for ($i=0; $i < count($rules_id); $i++) { 
						$rules_value_data = array(
							'id_asesmen' => $do_insert->id,
							'id_rules' => $rules_id[$i],
							'value' => $rules_value[$rules_id[$i]],
							'delete_flag' => 0 // Add by: Alvin (atsalvin0017), Date: 2019-12 13:52 PM
							);
						array_push($insert_rules_value, $rules_value_data);
					}
					if(count($insert_rules_value) > 0)
						$do_insert_rules_value = $this->model_basic->insert_all_batch($this->tbl_asesmen_2015_value,$insert_rules_value);
					$this->save_log_user(ACT_CREATE, 'Insert Assessment 2015');
					$redirect = $data['controller'].'/'.$data['function'];
					if ($this->session_user['admin_id']>0) $redirect = 'user_request';
	                $this->returnJson(array('status' => 'ok','msg' => 'Input data success','redirect' => $redirect));
	            } 
				else
					$this->returnJson(array('status' => 'error','msg' => 'Failed when saving data history'));
			}
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when saving data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function assessment2015_edit(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function_user('Asesmen 2015','assessment2015');
		$data += $this->user_get_menu();
		$this->check_userakses_user($data['function_id'], ACT_UPDATE);
		
		$table_field = $this->db->list_fields($this->tbl_assessment2015);
		$update = array();
		foreach ($table_field as $field) {
			$update[$field] = $this->input->post($field);
		}
		unset($update['date_created']);
		$update['date_modified'] = date('Y-m-d H:i:s',now());
		$update['id_modified'] = $this->session_user['admin_id'];
		
		$rules_id = $this->input->post('rules_id');
		$rules_value = $this->input->post('rules_value');
		$insert_rules_value = array();
		for ($i=0; $i < count($rules_id); $i++) { 
			$rules_value_data = array(
				'id_asesmen' => $update['id'],
				'id_rules' => $rules_id[$i],
				'value' => $rules_value[$rules_id[$i]],
				'delete_flag' => 0 // Add by: Alvin (atsalvin0017), Date: 2019-12 13:52 PM
				);
			array_push($insert_rules_value, $rules_value_data);
		}
		if(count($insert_rules_value) > 0){
			$this->model_basic->delete_full($this->tbl_asesmen_2015_value,'id_asesmen',$update['id']);
			$do_insert_rules_value = $this->model_basic->insert_all_batch($this->tbl_asesmen_2015_value,$insert_rules_value);
		}

		if($update['instalasi_id']){
			$do_update = $this->model_basic->update($this->tbl_assessment2015,$update,'id',$update['id']);
			$this->save_log_user(ACT_UPDATE, 'Update Assessment 2015');
                        if($do_update)
				$this->returnJson(array('status' => 'ok','msg' => 'Update success','redirect' => $data['controller'].'/'.$data['function']));
			else
				$this->returnJson(array('status' => 'error','msg' => 'Failed when updating data'));	
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Please complete the form'));
    }

    function assessment2015_delete(){
    	$data = $this->get_app_settings();
		$data += $this->controller_attr;
		$data += $this->get_function_user('Asesmen 2015','assessment2015');
		$data += $this->user_get_menu();
		$this->check_userakses_user($data['function_id'], ACT_DELETE);
		$id = $this->input->post('id');
		$do_delete = $this->model_basic->delete($this->tbl_assessment2015,'id',$id);
		if($do_delete){
                        $this->save_log_user(ACT_DELETE, 'Delete Assessment 2015');
			$this->delete_folder('assessment2015/'.$id);
			$this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => $data['controller'].'/'.$data['function']));
		}
		else
			$this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }

	public function ajax_assessment2015_list()
	{
		$list = $this->model_assessment2015->get_datatables_user();
		//die(print_r($this->db->last_query()));
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $data_row) {
				//render data
				$no++;
				$w_ps = $this->check_colour_text_dashboard(number_format($data_row->pemenuhan_system,2),'Pemenuhan System (Asesmen 2015)');
				$w_sp = $this->check_colour_text_dashboard(number_format($data_row->security_performance,2),'Security Performance (Asesmen 2015)');
				$w_sr = $this->check_colour_text_dashboard(number_format($data_row->security_reliability,2),'Security Reliability (Asesmen 2015)');
				$w_csi = $this->check_colour_text_dashboard(number_format($data_row->csi,2),'CSI (Asesmen 2015)');
				$semua_warna = array($w_ps,$w_sp,$w_sr,$w_csi);
				$w_na = '<div class="bg-Emas text-center">'.number_format($data_row->nilai_akhir,2).'</div>';
				if(in_array('hijau', $semua_warna))
					$w_na = '<div class="bg-Hijau text-center">'.number_format($data_row->nilai_akhir,2).'</div>';
				if(in_array('biru', $semua_warna))
					$w_na = '<div class="bg-Biru text-center">'.number_format($data_row->nilai_akhir,2).'</div>';
				if(in_array('merah', $semua_warna))
					$w_na = '<div class="bg-Merah text-center">'.number_format($data_row->nilai_akhir,2).'</div>';
				if(in_array('hitam', $semua_warna))
					$w_na = '<div class="bg-Hitam text-center">'.number_format($data_row->nilai_akhir,2).'</div>';

				$row = array();
				$row[] = $no;
				$row[] = $data_row->name;
				$row[] = $data_row->periode;
				$row[] = $this->check_colour(number_format($data_row->pemenuhan_system,2),'Pemenuhan System (Asesmen 2015)');
				$row[] = $this->check_colour(number_format($data_row->security_performance,2),'Security Performance (Asesmen 2015)');
				$row[] = $this->check_colour(number_format($data_row->security_reliability,2),'Security Reliability (Asesmen 2015)');
				$row[] = $this->check_colour(number_format($data_row->csi,2),'CSI (Asesmen 2015)');
				$row[] = $w_na;
				$row[] = '<a href="user_company/assessment2015_view/'.$data_row->id.'">Critical Point</a>';
				$row[] = date('d-m-Y',strtotime($data_row->date_created));
				// $row[] = '<a href="user_company/assessment2015_view/'.$data_row->id.'/detail">Detail Assessment</a>';
				//add html for action
				$row[] = '<div class="text-center">
						<form action="user_company/assessment2015_form" method="post">
						<input type="hidden" name="id" value="'.$data_row->id.'">
						<button class="btn-link" style="font-weight: 400; color: #428bca; padding: 0;" type="submit" data-original-title="Detail" data-placement="top" data-toggle="tooltip">Detail Assessment</button>
						</form>
						</div>';
			
				$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->model_assessment2015->count_all_user(),
						"recordsFiltered" => $this->model_assessment2015->count_filtered_user(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	function assessment_2015_check_colour(){
		$nilai = $this->input->post('nilai');
		if($nilai)
			$warna = $this->check_colour_text($nilai,'Nilai Akhir (Asesmen 2015)');
		else
			$warna = $this->check_colour_text(0,'Nilai Akhir (Asesmen 2015)');
		$result = array(
			'status' => 'ok',
			'warna' => $warna
			);
		$this->returnJson($result);
	}

}

/* End of file admin_company.php */
/* Location: ./application/controllers/admin_company.php */