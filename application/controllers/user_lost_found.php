<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class user_lost_found extends PX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('text');
        $this->user_check_login();
        $this->controller_attr = array('controller' => 'user_lost_found', 'controller_name' => 'User Lost & Found', 'controller_id' => 0);
    }

    public function index() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function_user('Lost & Found', 'user_lost_found');
        $data += $this->user_get_menu();
        $this->check_userakses_user($data['function_id'], ACT_READ);
        
        $data['lost_found'] = $lost_found = $this->model_basic->select_where_array_order_limit('px_lost_found', 'delete_flag = 0 and flag = 1', 'id', 'desc', 12);
        foreach ($lost_found->result() as $row) {
            $string = strip_tags($row->detail);
            $row->detail = word_limiter($string, 20);
            $row->date_created = date('j F Y', strtotime($row->date_created));
            $creator = $this->model_basic->select_where($this->tbl_user, 'id', $row->creator_id)->row();
            if ($creator)
                $row->realname = $creator->realname;
            else
                $row->realname = 'Administrator';
            $photos = $this->model_basic->select_where('px_lost_found_images', 'lost_found_id', $row->id);
            if($photos->num_rows() != 0)
            {
                $row->photo = 'assets/uploads/lost_found/'.$row->id.'/thumb'.$photos->row()->photo;
            }
            else
            {
                $row->photo = '';
            }
        }
        $data['content'] = $this->load->view('user_backend/admin_lost_found/index', $data, true);
        $this->load->view('user_backend/index', $data);
    }

    function lost_found() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function_user('Lost & Found', 'user_lost_found');
        $data += $this->user_get_menu();
        $this->check_userakses_user($data['function_id'], ACT_READ);
        $search = $this->input->post('search');
        $data['lost_found'] = $lost_found = $this->model_basic->select_where_array('px_lost_found', 'delete_flag = 0 and flag = 1 and (title like "%'.$search.'%" or detail like "%'.$search.'%")');
        foreach ($lost_found->result() as $row) {
            $string = strip_tags($row->detail);
            $row->detail = word_limiter($string, 50);
            $row->date_created = date('j F Y', strtotime($row->date_created));
            $creator = $this->model_basic->select_where($this->tbl_user, 'id', $row->creator_id)->row();
            if ($creator)
                $row->realname = $creator->realname;
            else
                $row->realname = 'Administrator';
            $photos = $this->model_basic->select_where('px_lost_found_images', 'lost_found_id', $row->id);
            if($photos->num_rows() != 0)
            {
                $row->photo = 'assets/uploads/lost_found/'.$row->id.'/thumb'.$photos->row()->photo;
            }
            else
            {
                $row->photo = '';
            }
        }
        $data['content'] = $this->load->view('user_backend/admin_lost_found/lost_found', $data, true);
        $this->load->view('user_backend/index', $data);
    }

    function manage_lost_found() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function_user('Lost & Found', 'user_lost_found');
        $data += $this->user_get_menu();
        $this->check_userakses_user($data['function_id'], ACT_READ);
        $user_id = $this->session->userdata['user']['admin_id'];
        $data['lost_found'] = $lost_found = $this->model_basic->select_where_array('px_lost_found', 'delete_flag = 0 and creator_id = ' . $user_id)->result();
        foreach ($lost_found as $row) {
            $string = strip_tags($row->detail);
            $row->content = word_limiter($string, 50);
            $row->date_created = date('j F Y', strtotime($row->date_created));
        }
        $data['content'] = $this->load->view('user_backend/admin_lost_found/manage_lost_found', $data, true);
        $this->load->view('user_backend/index', $data);
    }

    function user_lost_found_form() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function_user('Lost & Found', 'user_lost_found');
        $data += $this->user_get_menu();
        $this->check_userakses_user($data['function_id'], ACT_READ);
        $id = $this->input->post('id');
        if ($id) {
            $data['data'] = $this->model_basic->select_where('px_lost_found', 'id', $id)->row();
        } else
            $data['data'] = null;

        $data['content'] = $this->load->view('user_backend/admin_lost_found/lost_found_form', $data, true);
        $this->load->view('user_backend/index', $data);
    }

    function lost_found_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function_user('Lost & Found', 'user_lost_found');
        $data += $this->user_get_menu();
        $this->check_userakses_user($data['function_id'], ACT_CREATE);

        $table_field = $this->db->list_fields('px_lost_found');
        $insert = array();
        foreach ($table_field as $field) {
            $insert[$field] = $this->input->post($field);
        }

        //for generate ticket number
        /*$con = mysqli_connect("10.248.141.132","csc","nasigoreng","cscprod");
        //$con = mysqli_connect("localhost","root","","cscprod");
        $awalan = "LAF";
        $query = "SELECT ticket_number from px_lost_found order by id desc LIMIT 1";
        $result = mysqli_query($con,$query);

        if(mysqli_num_rows($result)==0)
        {
            $ticketID = 1;
            $ticketNumber = str_pad($ticketID, 7, '0', STR_PAD_LEFT);
        }
        else
        {
            $row = mysqli_fetch_assoc($result);
            //$ticketID = $row['ticket_number'] + 2;
            $ticketID = intval(substr($row['ticket_number'], strlen($awalan)))+1;
            $ticketNumber = str_pad($ticketID, 7, '0', STR_PAD_LEFT);
        }*/

        $ticket1 = $this->model_basic->select_where_all('px_ticket_number','prefix_number','LAF')->result();
        foreach ($ticket1 as $tickets)
        {
            $ticket = array();
            $ticket['id'] = $tickets->id;
            $ticket['prefix_number'] = $tickets->prefix_number;
            $ticket['running_number'] = $tickets->running_number + 1;
            $ticket['length_number'] = $tickets->length_number;
            $ticket['row_status'] = $tickets->row_status;
            $ticket['modified_by'] = $this->session_user['email'];
            $ticket['modified_date'] = date('Y-m-d H:i:s', now());

            $ticketNumber = str_pad($ticket['running_number'], $ticket['length_number'], '0', STR_PAD_LEFT);
            $ticketNumber1 = $ticket['prefix_number'].$ticketNumber;
        }
        $newTicket =  $ticketNumber1;
        
        $insert['ticket_number'] = $newTicket;
        $insert['date_created'] = date('Y-m-d H:i:s', now());
        $insert['creator_id'] = $this->session->userdata['user']['admin_id'];
        $insert['flag'] = 0;
        $insert['delete_flag'] = 0;

        if ($insert['title'] && $insert['detail']) {
            $do_insert = $this->model_basic->insert_all('px_lost_found', $insert);
            if ($do_insert) {

                $do_update = $this->model_basic->update('px_ticket_number', $ticket, 'id', $ticket['id']);

                $recipient = array();
                $recipient['user'] = $this->session_user['email'];
                /*$recipient['admin'] = $this->model_basic->select_where_all($this->tbl_admin, 'id_usergroup', 18)->result(); // sent to admin with specific usergroup*/
                $recipient['admin'] = $this->model_maintain_email->get_email('transaksi','Lost and Found')->result(); // sent to admin with specific usergroup
                $recipient['id'] = $do_insert->id;
                $recipient['type'] = $do_insert->type;
                $recipient['item_type'] = $do_insert->item_type;
                $recipient['color'] = $do_insert->color;
                $recipient['title'] = $do_insert->title;
                $recipient['detail'] = $do_insert->detail;
                $recipient['ticket_number'] = $do_insert->ticket_number;
                $recipient['user_username'] = $this->session_user['username'];
                $recipient['user_email'] = $this->session_user['email'];
                $recipient['user_realname'] = $this->session_user['realname'];
                //$recipient['place'] = $insert['infraction_place'];
                //$recipient['notes'] = $insert['infraction_notes'];
                //$recipient['datetime'] = $insert['infraction_date'];
                //print_r($recipient); die();
                $this->send_email_lostfound($recipient);
                $this->returnJson(array('status' => 'ok', 'msg' => 'Input data success', 'redirect' => $data['controller'] . '/manage_lost_found'));
            } else
                $this->returnJson(array('status' => 'error', 'msg' => 'Failed when saving data'));
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Please complete the form'));
    }
    
    function lost_found_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function_user('Lost & Found', 'user_lost_found');
        $data += $this->user_get_menu();
        $this->check_userakses_user($data['function_id'], ACT_UPDATE);

        $table_field = $this->db->list_fields('px_lost_found');
        $update = array();
        foreach ($table_field as $field) {
            $update[$field] = $this->input->post($field);
        }
        unset($update['date_created']);
        unset($update['creator_id']);
        unset($update['flag']);
        unset($update['delete_flag']);

        if ($update['title'] && $update['detail']) {
            $do_update = $this->model_basic->update('px_lost_found', $update, 'id', $update['id']);
            if ($do_update) {
                $this->returnJson(array('status' => 'ok', 'msg' => 'Input data success', 'redirect' => $data['controller'] . '/manage_lost_found'));
            } else
                $this->returnJson(array('status' => 'error', 'msg' => 'Failed when saving data'));
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Please complete the form'));
    }
    
    function lost_found_delete()
    {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function_user('Lost & Found', 'user_lost_found');
        $data += $this->user_get_menu();
        $this->check_userakses_user($data['function_id'], ACT_DELETE);
        
        $id = $this->input->post('id');
        $do_delete = $this->model_basic->delete('px_lost_found','id',$id);
        if($do_delete){
            $this->model_basic->delete('px_lost_found_images', 'lost_found_id', $id);
            $this->model_basic->delete('px_lost_found_comments', 'lost_found_id', $id);
            $this->delete_folder('lost_found/'.$id);
            $this->returnJson(array('status' => 'ok','msg' => 'Delete Success','redirect' => $data['controller'].'/manage_lost_found'));
        }
        else
            $this->returnJson(array('status' => 'error','msg' => 'Delete Failed'));
    }
    
    function lost_found_images($id) {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function_user('Lost & Found', 'user_lost_found');
        $data += $this->user_get_menu();
        $this->check_userakses_user($data['function_id'], ACT_READ);

        $lost_found = $this->model_basic->select_where('px_lost_found', 'id', $id);
        if ($lost_found->num_rows() != 1)
            redirect('user_lost_found/manage_lost_found');
        $data['lost_found'] = $lost_found->row();
        $data['lost_found_images'] = $this->model_basic->select_where('px_lost_found_images', 'lost_found_id', $id)->result();

        $data['content'] = $this->load->view('user_backend/admin_lost_found/lost_found_images', $data, true);
        $this->load->view('user_backend/index', $data);
    }

    function lost_found_images_form($lost_found_id) {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function_user('Lost & Found', 'user_lost_found');
        $data += $this->user_get_menu();
        $this->check_userakses_user($data['function_id'], ACT_READ);

        $lost_found = $this->model_basic->select_where('px_lost_found', 'id', $lost_found_id);
        if ($lost_found->num_rows() != 1)
            redirect('admin_activity/manual_input_activity');
        $data['lost_found'] = $lost_found->row();
        $id = $this->input->post('id');
        if ($id) {
            $data['data'] = $this->model_basic->select_where('px_lost_found_images', 'id', $id)->row();
        } else {
            $data['data'] = null;
        }

        $data['content'] = $this->load->view('user_backend/admin_lost_found/lost_found_images_form', $data, true);
        $this->load->view('user_backend/index', $data);
    }

    function lost_found_images_add() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function_user('Lost & Found', 'user_lost_found');
        $data += $this->user_get_menu();
        $this->check_userakses_user($data['function_id'], ACT_CREATE);

        $insert['lost_found_id'] = $this->input->post('lost_found_id');
        $insert['date_created'] = date('Y-m-d H:i:s', now());
        $insert['photo'] = basename($this->input->post('photo'));
        if ($insert['lost_found_id'] && $this->input->post('photo')) {
            $do_insert = $this->model_basic->insert_all('px_lost_found_images', $insert);
            if ($do_insert) {
                if (!is_dir(FCPATH . 'assets/uploads/lost_found/' . $insert['lost_found_id']))
                    mkdir(FCPATH . 'assets/uploads/lost_found/' . $insert['lost_found_id']);
                $src = $this->input->post('photo');
                $dst_path = realpath(FCPATH . 'assets/uploads/lost_found/' . $insert['lost_found_id']);
                $dst = $dst_path . '/' . basename($src);
                if (!copy($src, $dst)) {
                    $this->model_basic->delete('px_lost_found_images', 'id', $do_insert->id);
                    $this->returnJson(array('status' => 'error', 'msg' => 'Upload Falied'));
                } else {
                    $this->makeThumbnails($dst_path.'/', basename($src), 400, 400);
                    $this->delete_temp('temp_folder');
                    $this->returnJson(array('status' => 'ok', 'msg' => 'Input data success', 'redirect' => $data['controller'] . '/lost_found_images/'.$insert['lost_found_id']));
                }
            } else
                $this->returnJson(array('status' => 'error', 'msg' => 'Failed when saving data'));
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Please complete the form'));
    }

    function lost_found_images_edit() {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function_user('Lost & Found', 'user_lost_found');
        $data += $this->user_get_menu();
        $this->check_userakses_user($data['function_id'], ACT_UPDATE);

        $old_photo = $this->input->post('old_photo');
        $file_nm = basename($this->input->post('photo'));
        $lost_found_id = $this->input->post('lost_found_id');
        $update['id'] = $this->input->post('id');
        $update['date_created'] = date('Y-m-d H:i:s', now());
        if ($file_nm != $old_photo && $file_nm != "") {
            $update['photo'] = $file_nm;
        } else {
            $update['photo'] = $old_photo;
        }
            if ($lost_found_id) {
                $do_update = $this->model_basic->update('px_lost_found_images', $update, 'id', $update['id']);
                if ($do_update) {
                    if ($file_nm != $old_photo && $file_nm != "") {
                        @unlink('assets/uploads/lost_found/'.$lost_found_id.'/'.$old_photo);
                        @unlink('assets/uploads/lost_found/'.$lost_found_id.'/thumb'.$old_photo);
                        if (!is_dir(FCPATH . 'assets/uploads/lost_found/' . $lost_found_id))
                            mkdir(FCPATH . 'assets/uploads/lost_found/' . $lost_found_id);
                        $src = $this->input->post('photo');
                        $dst_path = realpath(FCPATH . 'assets/uploads/lost_found/' . $lost_found_id);
                        $dst = $dst_path . '/' . $file_nm;
                        if (!copy($src, $dst)) {
                            $this->model_basic->delete('px_lost_found_images', 'id', $update['id']);
                            $this->returnJson(array('status' => 'error', 'msg' => 'Upload Falied'));
                        } else {
                            $this->makeThumbnails($dst_path.'/', basename($src), 400, 400);
                            $this->delete_temp('temp_folder');
                        }
                    }
                    $this->returnJson(array('status' => 'ok', 'msg' => 'Update success', 'redirect' => $data['controller'] . '/lost_found_images/'.$lost_found_id));
                } else {
                    $this->returnJson(array('status' => 'error', 'msg' => 'Failed when updating data'));
                }
            } else
                $this->returnJson(array('status' => 'error', 'msg' => 'Please complete the form'));
    }

    function lost_found_images_delete($lost_found_id) {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function_user('Lost & Found', 'user_lost_found');
        $data += $this->user_get_menu();
        $this->check_userakses_user($data['function_id'], ACT_DELETE);
        
        $lost_found = $this->model_basic->select_where('px_lost_found', 'id', $lost_found_id);
        if ($lost_found->num_rows() != 1)
            redirect('user_lost_found/manage_lost_found');
        $id = $this->input->post('id');
        $deleted_file = $this->model_basic->select_where('px_lost_found_images', 'id', $id)->row();
        $do_delete = $this->model_basic->delete('px_lost_found_images', 'id', $id);
        if ($do_delete) {
            @unlink('assets/uploads/lost_found/'.$lost_found_id.'/'.$deleted_file->photo);
            @unlink('assets/uploads/lost_found/'.$lost_found_id.'/thumb'.$deleted_file->photo);
            $this->returnJson(array('status' => 'ok', 'msg' => 'Delete Success', 'redirect' => $data['controller'] . '/lost_found_images/'.$lost_found_id));
        } else
            $this->returnJson(array('status' => 'error', 'msg' => 'Delete Failed'));
    }

    public function detail($id) {
        $data = $this->get_app_settings();
        $data += $this->controller_attr;
        $data += $this->get_function_user('Lost & Found', 'user_lost_found');
        $data += $this->user_get_menu();
        $this->check_userakses_user($data['function_id'], ACT_READ);
        
        $lost_found = $this->model_basic->select_where('px_lost_found', 'id', $id);

        foreach ($lost_found->result() as $row) {
            $row->date_created = date('j F Y', strtotime($row->date_created));
            $creator = $this->model_basic->select_where($this->tbl_user, 'id', $row->creator_id)->row();
            if ($creator)
                $row->realname = $creator->realname;
            else
                $row->realname = 'Administrator';
            $photos = $this->model_basic->select_where('px_lost_found_images', 'lost_found_id', $row->id);
            if($photos->num_rows() != 0)
            {
                $row->photos = $photos;
            }
            else
            {
                $row->photos = array();
            }
            $comments = $this->model_basic->select_where('px_lost_found_comments', 'lost_found_id', $row->id);
            if($comments->num_rows() != 0)
            {
                $row->comments = $comments->result();
                foreach($row->comments as $data_row)
                {
                    $commentator = $this->model_basic->select_where($this->tbl_user, 'id', $data_row->user_id)->row();
                    if ($commentator)
                        $data_row->realname = $commentator->realname;
                    else
                        $data_row->realname = 'Administrator';
                    $data_row->date_created = date('j F Y', strtotime($data_row->date_created));
                }
            }
            else
            {
                $row->comments = array();
            }
        }

        $data['lost_found'] = $lost_found;
        $data['content'] = $this->load->view('user_backend/admin_lost_found/detail', $data, true);
        $this->load->view('user_backend/index', $data);
    }
    
    function submit_comments()
    {
        $lost_found_id = $this->input->post('lost_found_id');
        $comments = $this->input->post('comments');
        
        $temp = $this->model_basic->select_where('px_lost_found', 'id', $lost_found_id);
        if($temp->num_rows() != 0)
        {
            $insert = array(
                'lost_found_id' => $lost_found_id,
                'user_id' => $this->session->userdata['user']['admin_id'],
                'comments' => $comments,
                'date_created' => date('Y-m-d H:i:s', now())
            );
            if(!$this->model_basic->insert_all('px_lost_found_comments', $insert))
                    redirect('user_lost_found/detail/'.$lost_found_id.'?submit_comments=false');
            redirect('user_lost_found/detail/'.$lost_found_id.'?submit_comments=success');
        }
        else
            redirect('user_lost_found/detail/'.$lost_found_id.'?submit_comments=false');
    }

}

/* End of file user_lost_found.php */
/* Location: ./application/controllers/user_lost_found.php */