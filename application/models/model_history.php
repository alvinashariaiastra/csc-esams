<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_history extends PX_Model {

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	private function _get_datatables_query($table,$column,$date_month)
	{
		
		$this->db->from($table);
		$this->db->where('delete_flag', 0);
		$i = 1;

		foreach ($column as $item) 
		{
			if($_POST['search']['value'])
			{
				if ($i==1) {
					$where = "(".$item." LIKE '%".$_POST['search']['value']."%' ";
				}else{
					$where .= "OR ".$item." LIKE '%".$_POST['search']['value']."%' ";
				}

				if ($i == (count($column) - 1)) {
					$where .= ")";
					$this->db->where($where);
				}
			}
				
			$column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
                        if(isset($_POST['order']['1']))
                            $this->db->order_by($column[$_POST['order']['1']['column']], $_POST['order']['1']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
		$this->db->where('delete_flag', 0);
                $this->db->where('date_created >=', $date_month);
	}

	function get_datatables($table,$column,$date_month)
	{
		$this->_get_datatables_query($table,$column,$date_month);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($table,$column,$date_month)
	{
		$this->_get_datatables_query($table,$column,$date_month);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($table,$date_month)
	{
		$this->db->from($table);
		$this->db->where('delete_flag', 0);
                $this->db->where('date_created >=', $date_month);

		return $this->db->count_all_results();
	}
        
        private function _get_datatables_query_visitor($table,$column,$date_month)
	{
		
		$this->db->from($table);
		$this->db->where('delete_flag', 0);
		$i = 1;

		foreach ($column as $item) 
		{
			if($_POST['search']['value'])
			{
				if ($i==1) {
					$where = "(".$item." LIKE '%".$_POST['search']['value']."%' ";
				}else{
					$where .= "OR ".$item." LIKE '%".$_POST['search']['value']."%' ";
				}

				if ($i == (count($column) - 1)) {
					$where .= ")";
					$this->db->where($where);
				}
			}
				
			$column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
                        if(isset($_POST['order']['1']))
                            $this->db->order_by($column[$_POST['order']['1']['column']], $_POST['order']['1']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
		$this->db->where('delete_flag', 0);
                $this->db->where('date_login >=', $date_month);
	}

	function get_datatables_visitor($table,$column,$date_month)
	{
		$this->_get_datatables_query_visitor($table,$column,$date_month);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered_visitor($table,$column,$date_month)
	{
		$this->_get_datatables_query_visitor($table,$column,$date_month);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all_visitor($table,$date_month)
	{
		$this->db->from($table);
		$this->db->where('delete_flag', 0);
                $this->db->where('date_login >=', $date_month);

		return $this->db->count_all_results();
	}

	public function get_by_id($table,$id)
	{
		$this->db->from($table);
		$this->db->where('id',$id);
		$this->db->where('delete_flag', 0);

		$query = $this->db->get();

		return $query->row();
	}

	public function save($table,$data)
	{
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}

	public function update($table,$where, $data)
	{
		$this->db->update($table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($table,$id)
	{
		$this->db->where('id', $id);
		$this->db->delete($table);
	}
	public function sum_value($table,$sum,$column,$row_value)
	{
		$this->db->select('SUM('.$sum.') as jumlah');
		$this->db->where($column, $row_value);
		$this->db->from($table);
		return $this->db->get()->row();
	}

}

/* End of file model_table.php */
/* Location: ./application/models/model_history.php */