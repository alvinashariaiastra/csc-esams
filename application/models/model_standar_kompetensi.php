<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_standar_kompetensi extends PX_Model {

	var $column = array('a.id','b.realname','d.name','a.final_score','a.date_created'); //set column field database for order and search
	var $order = array('a.date_created' => 'desc'); // default order 

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	private function _get_datatables_query()
	{
		$this->db->select('a.id, a.level_user_id, b.realname, d.name,d.id as instalasi_id,a.final_score , a.date_created');
		$this->db->from($this->tbl_standar_kompetensi_result.' a');
		$this->db->join($this->tbl_user.' b', 'b.id = a.user_id', 'left');
		$this->db->join($this->tbl_user_jabatan.' c', 'c.id = b.jabatan_id', 'left');
		$this->db->join($this->tbl_instalasi.' d', 'd.id = b.instalasi_id', 'left');
		$this->db->where('a.delete_flag', 0);
		$i = 1;

		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
			{
				if ($i==1) {
					$where = "(".$item." LIKE '%".$_POST['search']['value']."%' ";
				}else{
					$where .= "OR ".$item." LIKE '%".$_POST['search']['value']."%' ";
				}

				if ($i == (count($this->column) - 1)) {
					$where .= ")";
					$this->db->where($where);
				}
			}
				
			$this->column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->tbl_standar_kompetensi_result);
		$this->db->where('delete_flag', 0);

		return $this->db->count_all_results();
	}

	public function get_by_id($table,$id)
	{
		$this->db->from($this->tbl_standar_kompetensi_result);
		$this->db->where('id',$id);
		$this->db->where('delete_flag', 0);

		$query = $this->db->get();

		return $query->row();
	}

	public function save($table,$data)
	{
		$this->db->insert($this->tbl_standar_kompetensi_result, $data);
		return $this->db->insert_id();
	}

	public function update($table,$where, $data)
	{
		$this->db->update($this->tbl_standar_kompetensi_result, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($table,$id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->tbl_standar_kompetensi_result);
	}
	public function sum_value($table,$sum,$column,$row_value)
	{
		$this->db->select('SUM('.$sum.') as jumlah');
		$this->db->where($column, $row_value);
		$this->db->from($table);
		return $this->db->get()->row();
	}
	public function get_stc_by_date($date_start, $date_end) {
		$this->db->select('a.id, b.realname, d.name,a.final_score , a.date_created');
		$this->db->from($this->tbl_standar_kompetensi_result.' a');
		$this->db->join($this->tbl_user.' b', 'b.id = a.user_id', 'left');
		$this->db->join($this->tbl_user_jabatan.' c', 'c.id = b.jabatan_id', 'left');
		$this->db->join($this->tbl_instalasi.' d', 'd.id = b.instalasi_id', 'left');
		$this->db->where('a.date_created >=', $date_start);
        $this->db->where('a.date_created <=', $date_end);
		$this->db->where('a.delete_flag', 0);
		$this->db->order_by('a.date_created', 'desc');

		return $this->db->get();
	}
	public function get_kompetensi_result($stc_id,$standar_kompetensi_result_id,$level = 1) {
        $this->db->select('a.id as standar_kompetensi_id,a.rules,a.score_standard, a.level_user_id,b.score,c.name as training_name');
        $this->db->from('px_standar_kompetensi a');
        $this->db->join('px_standar_kompetensi_user b', 'b.standar_kompetensi_id = a.id', 'left');
        $this->db->join('px_standar_kompetensi_training c', 'c.id = a.standar_kompetensi_training_id','left');
        $this->db->where('a.delete_flag', 0);
        $this->db->where('a.stc_id', $stc_id);
        $this->db->where('a.level_user_id', $level);
        $this->db->where('b.standar_kompetensi_result_id', $standar_kompetensi_result_id);
        $data = $this->db->get();
        return $data;
    }

    public function delete_standar_kompetensi_user($user_id,$standar_kompetensi_result_id) {
    	$this->db->delete($this->tbl_standar_kompetensi_user,array('user_id' => $user_id, 'standar_kompetensi_result_id' => $standar_kompetensi_result_id));
    }

    function get_detail_standar_kompetensi_by_user_id($user_id,$id_result = 0) {
    	$this->db->select('psku.score, psk.stc_id as stc_no');
    	$this->db->from('px_standar_kompetensi_result as pskr');
    	$this->db->join('px_standar_kompetensi_user as psku','psku.user_id = pskr.user_id','left');
    	$this->db->join('px_standar_kompetensi as psk','psk.id = psku.standar_kompetensi_id','left');
    	$this->db->where('pskr.user_id',$user_id);
        $this->db->where('pskr.delete_flag', 0);
    	if($id_result > 0)
    		$this->db->where('pskr.id',$id_result);
    	$this->db->order_by('pskr.date_created','DESC');
    	$data = $this->db->get();
    	return $data;
    }

}

/* End of file model_standar_kompetensi.php */
/* Location: ./application/models/model_standar_kompetensi.php */