<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_info_training extends PX_Model {

	var $column = array('a.title','b.realname','a.date_start','a.pendaftaran_start'); //set column field database for order and search
	var $order = array('a.date_created' => 'desc'); // default order 

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	private function _get_datatables_query($admin_usergroup, $admin_id)
	{
		$this->db->select('a.id,a.title,a.date_start,a.date_end,a.pendaftaran_start,a.pendaftaran_end,b.realname');
		$this->db->from($this->tbl_training_info.' a');
		$this->db->join($this->tbl_admin.' b', 'b.id = a.id_pic', 'left');
		$this->db->where('a.delete_flag', 0);
                if($admin_usergroup != 1)
                    $this->db->where('a.id_pic', $admin_id);
		$i = 1;

		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
			{
				if ($i==1) {
					$where = "(".$item." LIKE '%".$_POST['search']['value']."%' ";
				}else{
					$where .= "OR ".$item." LIKE '%".$_POST['search']['value']."%' ";
				}

				if ($i == (count($this->column) - 1)) {
					$where .= ")";
					$this->db->where($where);
				}
			}
				
			$this->column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($admin_usergroup, $admin_id)
	{
		$this->_get_datatables_query($admin_usergroup, $admin_id);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($admin_usergroup, $admin_id)
	{
		$this->_get_datatables_query($admin_usergroup, $admin_id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($admin_usergroup, $admin_id)
	{
		$this->db->from($this->tbl_training_info);
                if($admin_usergroup != 1)
                    $this->db->where('id_pic', $admin_id);
		$this->db->where('delete_flag', 0);

		return $this->db->count_all_results();
	}

	public function get_by_id($table,$id)
	{
		$this->db->from($this->tbl_training_info);
		$this->db->where('id',$id);
		$this->db->where('delete_flag', 0);

		$query = $this->db->get();

		return $query->row();
	}

	public function save($table,$data)
	{
		$this->db->insert($this->tbl_user, $data);
		return $this->db->insert_id();
	}

	public function update($table,$where, $data)
	{
		$this->db->update($this->tbl_user, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($table,$id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->tbl_user);
	}

	public function get_training_info_detail($pendaftaran_training_id) {
		$this->db->select('a.id,a.id_pic,a.title,a.date_start,a.date_end,a.pendaftaran_start,a.pendaftaran_end,b.realname,b.email');
		$this->db->from($this->tbl_training_info.' a');
		$this->db->join($this->tbl_admin.' b', 'b.id = a.id_pic', 'left');
		$this->db->where('a.id', $pendaftaran_training_id);

		return $this->db->get();
	}

	public function get_training_detail($pendaftaran_training_id) {
		$this->db->select('b.id_pic,b.title,a.content,b.date_start,b.date_end,b.pendaftaran_start,b.pendaftaran_end,b.date_created,c.realname,c.email,c.telp,d.jabatan,e.name AS instalasi_nm');
		$this->db->from($this->tbl_training_pendaftaran.' a');
		$this->db->join($this->tbl_training_info.' b', 'b.id = a.training_info_id', 'left');
		$this->db->join($this->tbl_admin.' c', 'c.id = a.id_created', 'left');
		$this->db->where('a.id', $pendaftaran_training_id);

		return $this->db->get();
	}

	public function get_pic_detail($id_pic) {
		$this->db->select('a.realname,a.email,a.telp,b.jabatan,c.name AS instalasi_nm');
		$this->db->from($this->tbl_user.' a');
		$this->db->join($this->tbl_user_jabatan.' b', 'b.id = a.jabatan_id', 'left');
		$this->db->join($this->tbl_instalasi.' c', 'c.id = a.instalasi_id', 'left');
		$this->db->where('c.id', $id_pic);

		return $this->db->get();
	}

}

/* End of file model_info.php */
/* Location: ./application/models/model_info.php */