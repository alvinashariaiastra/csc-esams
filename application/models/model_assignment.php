<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_assignment extends PX_Model {
	
	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	function view_assignment($data)
	{
		$this->db->select('*');
		$this->db->where('a.delete_flag', 0);
		$this->db->from($this->tbl_assignment.' a');
		$this->db->join($this->tbl_assignment_detail.' b', 'b.assignment_id = a.id');
		$this->db->join($this->tbl_assignment_people_checkin.' c', 'c.assignment_detail_id = b.id');
		$this->db->where('a.id', $data);
		$result = $this->db->get();
		$result = $result->result();
		return $result;

	}

	function view_complete($data)
	{
		$this->db->select('*');
		$this->db->where('a.delete_flag', 0);
		$this->db->from($this->tbl_assignment.' a');
		$this->db->join($this->tbl_assignment_detail.' b', 'b.assignment_id = a.id');
		$this->db->join($this->tbl_assignment_report.' c', 'c.assignment_detail_id = b.id');
		$this->db->where('a.id', $data);
		$result = $this->db->get();
		$result = $result->result();
		return $result;

	}

	function get_id_detail($data)
	{
		$this->db->select('id');
		$this->db->where('delete_flag', 0);
		$this->db->from($this->tbl_assignment_detail);
		$this->db->where('assignment_id', $data);
		$result = $this->db->get();
		$result = $result->result();
		return $result;

	}	

	function get_id_group($data)
	{
		$this->db->select('group_security_id');
		$this->db->where('delete_flag', 0);
		$this->db->from($this->tbl_assignment_people);
		$this->db->where('assignment_id', $data);
		$result = $this->db->get();
		$result = $result->result();
		return $result;

	}

	function get_id_user($data)
	{
		$this->db->select('user_id');
		$this->db->where('delete_flag', 0);
		$this->db->from($this->tbl_assignment_people);
		$this->db->where('assignment_id', $data);
		$result = $this->db->get();
		$result = $result->result();
		return $result;

	}
}
