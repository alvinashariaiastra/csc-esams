<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_user_company extends PX_Model {

	public function __construct() {
		parent::__construct();
	}

	function select_assessment2004($jenis_bisnis_id) {
		$this->db->select('');
		$this->db->from($this->tbl_assessment2004.' a');
		$this->db->join($this->tbl_instalasi.' b', 'b.id = a.instalasi_id', 'left');
		$this->db->where('a.delete_flag', 0);
		$this->db->where('b.jenis_bisnis_id', $jenis_bisnis_id);

		$query = $this->db->get();
		return $query;
	}

		private function _get_datatables_query($table,$table_join,$column)
	{
		
		$this->db->from($table.' a');
		$this->db->join($table_join.' b', 'b.id = a.instalasi_id', 'left');

		$i = 1;

		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
			{
				if ($i==0) {
					$where .= "(".$item." LIKE '%".$_POST['search']['value']."%' ";
				}else{
					$where .= "OR ".$item." LIKE '%".$_POST['search']['value']."%' ";
				}

				if ($i == (count($this->column) - 1)) {
					$where .= ")";
					$this->db->where($where);
				}
			}
				
			$column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
		$this->db->where('a.delete_flag', 0);
		$this->db->where('b.jenis_bisnis_id', $jenis_bisnis_id);
	}

	function get_datatables($table,$table_join,$column)
	{
		$this->_get_datatables_query($table,$table_join,$column);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($table,$column)
	{
		$this->_get_datatables_query($table,$column);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($table)
	{
		$this->db->from($table);
		$this->db->where('a.delete_flag', 0);

		return $this->db->count_all_results();
	}

	public function get_by_id($table,$id)
	{
		$this->db->from($table);
		$this->db->where('a.id',$id);
		$this->db->where('a.delete_flag', 0);

		$query = $this->db->get();

		return $query->row();
	}

}

/* End of file model_user_company.php */
/* Location: ./application/models/model_user_company.php */