<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_assessment2004 extends PX_Model {
	
	var $column = array('b.name','a.periode','ra.value','rb.value','rc.value','rd.value','a.nilai_akhir','a.critical_point','a.date_created'); //set column field database for order and search
	//var $column = array('b.name','a.periode','element_1','element_2','element_3','element_4','a.nilai_akhir','a.critical_point','a.date_created'); //set column field database for order and search
	var $order = array('a.date_created' => 'desc'); // default order 

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	function _get_datatables_query()
	{
		$this->db->select('a.id, a.instalasi_id, ra.value as element_1, rb.value as element_2, rc.value as element_3, rd.value as element_4, b.name, a.periode,a.nilai_akhir , a.date_created');
		$this->db->from($this->tbl_assessment2004.' a');
		$this->db->join($this->tbl_instalasi.' b', 'b.id = a.instalasi_id', 'left');
		$this->db->join($this->tbl_asesmen_2004_value.' ra', 'ra.id_asesmen = a.id and ra.id_rules = 5', 'right');
		$this->db->join($this->tbl_asesmen_2004_value.' rb', 'rb.id_asesmen = a.id and rb.id_rules = 6', 'right');
		$this->db->join($this->tbl_asesmen_2004_value.' rc', 'rc.id_asesmen = a.id and rc.id_rules = 7', 'right');
		$this->db->join($this->tbl_asesmen_2004_value.' rd', 'rd.id_asesmen = a.id and rd.id_rules = 8', 'right');
		$this->db->where('a.delete_flag', 0);
		$i = 1;

		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
			{
				if ($i==1) {
					$where = "(".$item." LIKE '%".$_POST['search']['value']."%' ";
				}else{
					$where .= "OR ".$item." LIKE '%".$_POST['search']['value']."%' ";
				}

				if ($i == (count($this->column) - 1)) {
					$where .= ")";
					$this->db->where($where);
				}
			}
				
			$this->column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}
	function _get_datatables_query_user()
	{
		$this->db->select('a.id, a.instalasi_id, ra.value as element_1, rb.value as element_2, rc.value as element_3, rd.value as element_4, b.name, a.periode,a.nilai_akhir , a.date_created');
		$this->db->from($this->tbl_assessment2004.' a');
		$this->db->join($this->tbl_instalasi.' b', 'b.id = a.instalasi_id', 'left');
		$this->db->join($this->tbl_asesmen_2004_value.' ra', 'ra.id_asesmen = a.id and ra.id_rules = 5', 'right');
		$this->db->join($this->tbl_asesmen_2004_value.' rb', 'rb.id_asesmen = a.id and rb.id_rules = 6', 'right');
		$this->db->join($this->tbl_asesmen_2004_value.' rc', 'rc.id_asesmen = a.id and rc.id_rules = 7', 'right');
		$this->db->join($this->tbl_asesmen_2004_value.' rd', 'rd.id_asesmen = a.id and rd.id_rules = 8', 'right');
		$this->db->where('a.delete_flag', 0);
		$this->db->where('a.instalasi_id', $this->session_user['id_instalasi']);
		$i = 1;

		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
			{
				if ($i==1) {
					$where = "(".$item." LIKE '%".$_POST['search']['value']."%' ";
				}else{
					$where .= "OR ".$item." LIKE '%".$_POST['search']['value']."%' ";
				}

				if ($i == (count($this->column) - 1)) {
					$where .= ")";
					$this->db->where($where);
				}
			}
				
			$this->column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}
	function get_datatables_user()
	{
		$this->_get_datatables_query_user();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered_user()
	{
		$this->_get_datatables_query_user();
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_all()
	{
		$this->db->from($this->tbl_assessment2004);
		$this->db->where('delete_flag', 0);
		return $this->db->count_all_results();
	}

	function count_all_user()
	{
		$this->db->from($this->tbl_assessment2004);
		$this->db->where('delete_flag', 0);
		$this->db->where('instalasi_id', $this->session_user['id_instalasi']);
		return $this->db->count_all_results();
	}

	function get_by_id($table,$id)
	{
		$this->db->from($this->tbl_assessment2004);
		$this->db->where('id',$id);
		$this->db->where('delete_flag', 0);

		$query = $this->db->get();

		return $query->row();
	}

	function save($table,$data)
	{
		$this->db->insert($this->tbl_assessment2004, $data);
		return $this->db->insert_id();
	}

	public function update($table,$where, $data)
	{
		$this->db->update($this->tbl_assessment2004, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($table,$id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->tbl_assessment2004);
	}
	function sum_value($table,$sum,$column,$row_value)
	{
		$this->db->select('SUM('.$sum.') as jumlah');
		$this->db->where($column, $row_value);
		$this->db->from($table);
		return $this->db->get()->row();
	}
	public function get_asesment_by_date($date_start, $date_end) {
		$this->db->select('a.id, b.name, a.periode,a.nilai_akhir , a.date_created');
		$this->db->from($this->tbl_assessment2004.' a');
		$this->db->join($this->tbl_instalasi.' b', 'b.id = a.instalasi_id', 'left');
		$this->db->where('a.date_created >=', $date_start);
        $this->db->where('a.date_created <=', $date_end);
		$this->db->where('a.delete_flag', 0);
		$this->db->order_by('a.date_created', 'desc');

		return $this->db->get();
	}
}

/* End of file model_assessment2004.php */
/* Location: ./application/models/model_assessment2004.php */