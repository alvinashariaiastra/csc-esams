<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_maintain_email extends PX_Model {
	
	public function __construct() {
		parent::__construct();
	}

	//for tabel
	function get_all(){
		$this->db->select($this->tbl_maintain_email.'.*,'.$this->tbl_admin.'.realname,'.$this->tbl_admin.'.email');
		$this->db->from($this->tbl_maintain_email);
		$this->db->join($this->tbl_admin,$this->tbl_admin.'.id = '.$this->tbl_maintain_email.'.admin_id','left');
		return $this->db->get()->result();

		/*$this->db->select('*');
		$this->db->from($this->tbl_maintain_email);
		return $this->db->get()->result();*/
	}

	//get data saat edit
	function get_where($id){
		$this->db->select($this->tbl_maintain_email.'.transaksi,'.$this->tbl_maintain_email.'.admin_id,'.$this->tbl_admin.'.realname,'.$this->tbl_admin.'.email');
		$this->db->from($this->tbl_maintain_email);
		$this->db->join($this->tbl_admin,$this->tbl_admin.'.id = '.$this->tbl_maintain_email.'.admin_id');
		$this->db->where($this->tbl_maintain_email.'.id',$id);
		
		$data = $this->db->get();
        return $data;

		/*$this->db->select('*');
		$this->db->from($this->tbl_maintain_email);
		return $this->db->get()->result();*/
	}

	//send email
	function get_email($column,$where){
		$this->db->select($this->tbl_maintain_email.'.*,'.$this->tbl_admin.'.realname,'.$this->tbl_admin.'.email');
		$this->db->from($this->tbl_maintain_email);
		$this->db->join($this->tbl_admin,$this->tbl_admin.'.id = '.$this->tbl_maintain_email.'.admin_id','left');
		$this->db->where($column, $where);
		$data = $this->db->get();
        return $data;

		//return $this->db->get()->result();

		/*$this->db->select('*');
		$this->db->from($this->tbl_maintain_email);
		return $this->db->get()->result();*/
	}
}