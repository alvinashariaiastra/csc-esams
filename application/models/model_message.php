<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_message extends PX_Model {

	var $column = array('mess_id','b.realname','c.name'); //set column field database for order and search
	var $order = array('a.date_created' => 'desc'); // default order 

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	private function _get_datatables_query()
	{
		$this->db->distinct();
		$this->db->select('max(a.id) as mess_id, a.user_id,b.realname,b.photo,c.name AS instalasi_nm');
		$this->db->from($this->tbl_message.' a');
		$this->db->join($this->tbl_user.' b', 'b.id = a.user_id', 'left');
		$this->db->join($this->tbl_instalasi.' c', 'c.id = b.instalasi_id', 'left');
		$this->db->where('a.delete_flag', 0);
                $this->db->where('a.type', 1);
                $this->db->group_by('a.user_id');

		$i = 1;

		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
			{
				if ($i==1) {
					$where = "(".$item." LIKE '%".$_POST['search']['value']."%' ";
				}else{
					$where .= "OR ".$item." LIKE '%".$_POST['search']['value']."%' ";
				}

				if ($i == (count($this->column))) {
					$where .= ")";
					$this->db->where($where);
				}
			}
				
			$this->column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->tbl_message);
		$this->db->where('delete_flag', 0);
                $this->db->where('type', 1);
		return $this->db->count_all_results();
	}

	public function get_message($user_id) {
		$this->db->select('a.id, a.user_id, b.realname, b.photo, a.type, a.message, a.date_created, a.status, a.date_read');
		$this->db->from($this->tbl_message.' a');
		$this->db->join($this->tbl_user.' b', 'b.id = a.user_id', 'left');
		$this->db->where('a.delete_flag', 0);
		$this->db->where('a.user_id', $user_id);
		$this->db->order_by('a.date_created', 'asc');

		return $this->db->get();
	}

	public function get_message_by_id($id) {
		$this->db->select('a.id, a.user_id, b.realname, b.photo, a.type, a.message, a.date_created, a.status, a.date_read');
		$this->db->from($this->tbl_message.' a');
		$this->db->join($this->tbl_user.' b', 'b.id = a.user_id', 'left');
		$this->db->where('a.id', $id);
		$this->db->order_by('a.date_created', 'asc');

		return $this->db->get();
	}

	public function get_user_list() {
		$this->db->distinct();
		$this->db->select('a.user_id,b.realname,b.photo,c.name AS instalasi_nm');
		$this->db->from($this->tbl_message.' a');
		$this->db->join($this->tbl_user.' b', 'b.id = a.user_id', 'left');
		$this->db->join($this->tbl_instalasi.' c', 'c.id = b.instalasi_id', 'left');
		$this->db->where('a.delete_flag', 0);
		$this->db->where('a.status', 0);
		$this->db->order_by('a.date_created', 'desc');

		return $this->db->get();
	}
        
        function update_readable_messages($user_id, $type)
        {
            $update = array('status' => 1, 'date_read' => date('Y-m-d H:i:s', now()));
            $this->db->where('user_id', $user_id);
            $this->db->where('type', $type);
            $this->db->where('status', 0);
            if(!$this->db->update($this->tbl_message, $update))
                    return FALSE;
            return TRUE;
        }
        
        function get_count_unread_messages_admin_by_user_id($user_id)
        {
            $this->db->where('delete_flag', 0);
            $this->db->where('user_id', $user_id);
            $this->db->where('type', 1);
            $this->db->where('status', 0);
            $query = $this->db->get($this->tbl_message);
            return $query->num_rows();
        }

}

/* End of file model_message.php */
/* Location: ./application/models/model_message.php */