<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_news extends PX_Model {
	
	public function __construct() {
		parent::__construct();
	}
	function get_paging_news($limit,$from,$order,$type) {
		$this->db->select('*');
		$this->db->from($this->tbl_news);
		$this->db->limit($limit,$from);
		$this->db->order_by($order,$type);
		return $this->db->get()->result();
	}
}