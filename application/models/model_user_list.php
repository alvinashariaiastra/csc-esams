<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_user_list extends PX_Model {

	var $column = array('a.id','a.realname','b.usergroup_name','a.level_user_id','c.name','d.name','e.jabatan'); //set column field database for order and search
	var $order = array('a.realname' => 'asc'); // default order 

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	private function _get_datatables_query()
	{
		$this->db->select('a.id, a.realname,a.level_user_id, b.usergroup_name AS user_group ,c.name AS perusahaan, d.name AS instalasi, e.jabatan, a.photo, a.date_created');
		$this->db->from($this->tbl_user.' a');
		$this->db->join($this->tbl_usergroup_user.' b', 'b.id = a.user_group_id', 'left');
		$this->db->join($this->tbl_perusahaan.' c', 'c.id = a.perusahaan_id', 'left');
		$this->db->join($this->tbl_instalasi.' d', 'd.id = a.instalasi_id', 'left');
		$this->db->join($this->tbl_user_jabatan.' e', 'e.id = a.jabatan_id', 'left');
		$this->db->where('a.delete_flag', 0);
		$i = 1;

		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
			{
				if ($i==1) {
					$where = "(".$item." LIKE '%".$_POST['search']['value']."%' ";
				}else{
					$where .= "OR ".$item." LIKE '%".$_POST['search']['value']."%' ";
				}

				if ($i == (count($this->column) - 1)) {
					$where .= ")";
					$this->db->where($where);
				}
			}
				
			$this->column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
                        if(isset($_POST['order']['1']))
                            $this->db->order_by($this->column[$_POST['order']['1']['column']], $_POST['order']['1']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
                //$this->db->order_by('a.level_user_id', 'asc');
                //$this->db->order_by('a.realname', 'asc');
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->tbl_user);
		$this->db->where('delete_flag', 0);

		return $this->db->count_all_results();
	}

	public function get_by_id($table,$id)
	{
		$this->db->from($this->tbl_user);
		$this->db->where('id',$id);
		$this->db->where('delete_flag', 0);

		$query = $this->db->get();

		return $query->row();
	}

	public function save($table,$data)
	{
		$this->db->insert($this->tbl_user, $data);
		return $this->db->insert_id();
	}

	public function update($table,$where, $data)
	{
		$this->db->update($this->tbl_user, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($table,$id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->tbl_user);
	}
	public function sum_value($table,$sum,$column,$row_value)
	{
		$this->db->select('SUM('.$sum.') as jumlah');
		$this->db->where($column, $row_value);
		$this->db->from($table);
		return $this->db->get()->row();
	}

}

/* End of file model_user_list.php */
/* Location: ./application/models/model_user_list.php */