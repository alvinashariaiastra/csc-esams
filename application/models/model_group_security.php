<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Model_group_security extends PX_Model {

    public function __construct() {
        parent::__construct();
    }

    //select count(user_id) from px_group_security_detail where group_security_id = 1 and delete_flag = 0
    function select_jumlahanggota($table,$groupid) {
        $this->load->database('default', TRUE);
        $this->db->select('count(user_id) as jumlah');
        $this->db->from($table);
        $multipleWhere = ['group_security_id' => $groupid, 'delete_flag' => 0];
        $this->db->where($multipleWhere);
        $data = $this->db->get();
        return $data->result();
    }

    function select_user($table,$groupid) {
        $this->load->database('default', TRUE);
        $this->db->select('user_id');
        $this->db->from($table);
        $multipleWhere = ['group_security_id' => $groupid, 'delete_flag' => 0];
        $this->db->where($multipleWhere);
        $data = $this->db->get();
        return $data->result();
    }

}