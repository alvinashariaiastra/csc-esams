<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_menu_user extends PX_Model {
	public function __construct() {
		parent::__construct();
	}
	function get_all(){
		$this->db->select($this->tbl_menu_user.'.*,parent_menu.name as parent');
		$this->db->from($this->tbl_menu_user);
		$this->db->join($this->tbl_menu_user.' as parent_menu','parent_menu.id = '.$this->tbl_menu_user.'.id_parent','left');
		$this->db->where($this->tbl_menu_user.'.delete_flag', 0);
		return $this->db->get()->result();
	}
	function get_menu_bar($user_level){
        $this->db->select('menu.*,menu.id as id_menu');
        $this->db->from($this->tbl_useraccess_user);
        $this->db->join($this->tbl_menu_user.' as menu','menu.id = '.$this->tbl_useraccess_user.'.id_menu');
        $this->db->where('menu.id_parent',0);
        $this->db->where($this->tbl_useraccess_user.'.act_read',1);
        $this->db->where($this->tbl_useraccess_user.'.id_usergroup',$user_level);
        $this->db->where('menu.delete_flag', 0);
        $this->db->order_by('orders','ASC');
        return $this->db->get()->result();
    }
    function get_sub_menu($user_level){
        $this->db->select('menu.*');
        $this->db->from($this->tbl_useraccess_user);
        $this->db->join($this->tbl_menu_user.' as menu','menu.id = '.$this->tbl_useraccess_user.'.id_menu');
        $this->db->where('menu.id_parent >','0');
        $this->db->where($this->tbl_useraccess_user.'.act_read',1);
        $this->db->where($this->tbl_useraccess_user.'.id_usergroup',$user_level);
        $this->db->where('menu.delete_flag', 0);
        $this->db->order_by('orders','ASC');
        return $this->db->get()->result();
    }
}

/* End of file model_menu_user.php */
/* Location: ./application/models/model_menu_user.php */