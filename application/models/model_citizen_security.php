<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_citizen_security extends PX_Model {
	
	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}
	
	private function _get_datatables_query($table, $column)
	{
		$this->db->from($table);
		$this->db->join($this->tbl_user, $this->tbl_user.'.id = '.$table.'.reporter_id');
		
		if ($this->input->post('startdatetime') != "" && $this->input->post('enddatetime') != "") {
			$this->db->where($table.'.infraction_date >= ', $this->input->post('startdatetime'));
			$this->db->where($table.'.infraction_date <= ', $this->input->post('enddatetime'));
		}
		
		$this->db->where($table.'.delete_flag', 0);
		
		$i = 0;
		foreach ($column as $item) 
		{
			if($_POST['search']['value'])
			{
				if ($i==0) {
					$where = "(".$item." LIKE '%".$_POST['search']['value']."%' ";
				}else{
					$where .= "OR ".$item." LIKE '%".$_POST['search']['value']."%' ";
				}

				if ($i == (count($column) - 1)) {
					$where .= ")";
					$this->db->where($where);
				}
			}
				
			$column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($this->tbl_citizen_security.'.infraction_date', $_POST['order']['0']['dir']);
			if(isset($_POST['order']['1']))
				$this->db->order_by($this->tbl_citizen_security.'.infraction_date', $_POST['order']['1']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
			die(print_r($this->db->last_query()));
		}
		$this->db->where($table.'.delete_flag', 0);
	}
	
	function get_datatables($table,$column)
	{
		$this->_get_datatables_query($table, $column);
		
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}
	
	function count_filtered($table,$column)
	{
		$this->_get_datatables_query($table, $column);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($table)
	{
		$this->db->from($table);
		$this->db->where('delete_flag', 0);

		return $this->db->count_all_results();
	}
	
	public function get_cs_by_date($date_start, $date_end) {
		$this->db->select('cs.id, cs.reporter_id, cs.infraction_place, cs.infraction_notes, cs.infraction_photo1, cs.infraction_photo2, cs.infraction_photo3, cs.infraction_date');
		$this->db->from($this->tbl_citizen_security.' cs');
		$this->db->where('cs.infraction_date >= ', $date_start);
        $this->db->where('cs.infraction_date <= ', $date_end);
		$this->db->where('cs.delete_flag', 0);
		$this->db->order_by('cs.infraction_date', 'desc');

		return $this->db->get();
	}
	
}

/* End of file model_citizen_security.php */
/* Location: ./application/models/model_citizen_security.php */