<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_usergroup_user extends PX_Model {

	public function __construct() {
		parent::__construct();
	}
	function get_all(){
		$this->db->select($this->tbl_usergroup_user.'.*');
		$this->db->from($this->tbl_usergroup_user);
		$this->db->where('delete_flag', 0);
		return $this->db->get()->result();
	}

}

/* End of file model_usergroup_user.php */
/* Location: ./application/models/model_usergroup_user.php */