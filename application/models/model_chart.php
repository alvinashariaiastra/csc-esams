<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Model_chart extends PX_Model {

    public function __construct() {
        parent::__construct();
    }

    function jumlah_instalasi() {
        $this->load->database('default', TRUE);
        $this->db->select('jenis_bisnis.name as jenis_bisnis_name, count(*) as jumlah_instalasi');
        $this->db->from($this->tbl_instalasi.' as instalasi');
        $this->db->group_by('instalasi.jenis_bisnis_id');
        $this->db->order_by('jenis_bisnis.position', 'ASC');
        $this->db->join($this->tbl_jenis_bisnis.' as jenis_bisnis','instalasi.jenis_bisnis_id = jenis_bisnis.id');
        return $this->db->get()->result();
    }
    function jumlah_perusahaan() {
        $this->load->database('default', TRUE);
        $this->db->select('jenis_bisnis.name as jenis_bisnis_name, count(*) as jumlah_perusahaan');
        $this->db->from($this->tbl_perusahaan.' as perusahaan');
        $this->db->group_by('perusahaan.jenis_bisnis_id');
        $this->db->order_by('jenis_bisnis.position', 'ASC');
        $this->db->join($this->tbl_jenis_bisnis.' as jenis_bisnis','perusahaan.jenis_bisnis_id = jenis_bisnis.id');
        return $this->db->get()->result();
    }
    function jumlah_satpam() {
        $this->load->database('default', TRUE);
        $this->db->select('jenis_bisnis.name as jenis_bisnis_name, sum(instalasi.jumlah_personil_security_organik) as jumlah_organik,sum(instalasi.jumlah_personil_security_outsourching) as jumlah_outsourching');
        $this->db->from($this->tbl_instalasi.' as instalasi');
        $this->db->group_by('instalasi.jenis_bisnis_id');
        $this->db->order_by('jenis_bisnis.position', 'ASC');
        $this->db->join($this->tbl_jenis_bisnis.' as jenis_bisnis','instalasi.jenis_bisnis_id = jenis_bisnis.id');
        return $this->db->get()->result();
    }
    function get_satpam_sum_by_jenis_bisnis($jenis_bisnis_id)
    {
        $this->load->database('default', TRUE);
        $this->db->select('sum(jumlah_personil_security_organik) as jumlah_organik,sum(jumlah_personil_security_outsourching) as jumlah_outsourching');
        $this->db->from($this->tbl_instalasi.' as instalasi');
        $this->db->where('jenis_bisnis_id', $jenis_bisnis_id);
        return $this->db->get()->row();
    }
    function jumlah_asesmen() {
        $this->load->database('default', TRUE);
        $this->db->select('jenis_bisnis.name as jenis_bisnis_name, COUNT(asesmen.instalasi_id) as jumlah_asesmen');
        $this->db->from($this->tbl_assessment2015.' as asesmen');
        $this->db->group_by('instalasi.jenis_bisnis_id');
        $this->db->join($this->tbl_instalasi.' as instalasi','asesmen.instalasi_id = instalasi.id');
        $this->db->join($this->tbl_jenis_bisnis.' as jenis_bisnis','instalasi.jenis_bisnis_id = jenis_bisnis.id');
        $this->db->where('asesmen.delete_flag',0);
        $this->db->order_by('jenis_bisnis.position', 'ASC');
        return $this->db->get()->result();
    }
    function get_assessment_by_jenis_bisnis($jenis_bisnis_id)
    {
        $this->load->database('default', TRUE);
        $this->db->select('COUNT(asesmen.instalasi_id) as jumlah_asesmen');
        $this->db->from($this->tbl_assessment2015.' as asesmen');
        $this->db->join($this->tbl_instalasi.' as instalasi','asesmen.instalasi_id = instalasi.id');
        $this->db->where('asesmen.delete_flag',0);
        $this->db->where('instalasi.jenis_bisnis_id', $jenis_bisnis_id);
        return $this->db->get()->row();
    }
    function hasil_asesmen($jenis_bisnis_id) {
        $this->load->database('default', TRUE);
        $this->db->select('asesmen.nilai_akhir as nilai_akhir');
        $this->db->from($this->tbl_assessment2015.' as asesmen');
        //$this->db->group_by('instalasi.jenis_bisnis_id');
        $this->db->join($this->tbl_instalasi.' as instalasi','asesmen.instalasi_id = instalasi.id');
        $this->db->where('instalasi.jenis_bisnis_id', $jenis_bisnis_id);
        $this->db->where('asesmen.delete_flag',0);
        return $this->db->get();
    }
    function hasil_asesmen_periode($jenis_bisnis_id,$periode) {
        $this->load->database('default', TRUE);
        $this->db->select('asesmen.nilai_akhir as nilai_akhir');
        $this->db->from($this->tbl_assessment2015.' as asesmen');
        //$this->db->group_by('instalasi.jenis_bisnis_id');
        $this->db->join($this->tbl_instalasi.' as instalasi','asesmen.instalasi_id = instalasi.id');
        $this->db->where('instalasi.jenis_bisnis_id', $jenis_bisnis_id);
        if ($periode != 0) {
            $this->db->where('asesmen.periode', $periode);
        }
        $this->db->where('asesmen.delete_flag',0);
        return $this->db->get();
    }
    function jumlah_asesmen_instalasi() {
        $this->load->database('default', TRUE);
        $this->db->select('instalasi.name as instalasi_name, COUNT(asesmen.instalasi_id) as jumlah_asesmen');
        $this->db->from($this->tbl_assessment2015.' as asesmen');
        $this->db->group_by('asesmen.instalasi_id');
        $this->db->join($this->tbl_instalasi.' as instalasi','asesmen.instalasi_id = instalasi.id');
        $this->db->where('asesmen.delete_flag',0);
        return $this->db->get()->result();
    }
    function jumlah_asesmen2004_instalasi($instalasi_id = 0) {
        $this->load->database('default', TRUE);
        $this->db->select('rules_asms.name as rules_name, SUM(asesmen_value.value) as jumlah_asesmen');
        $this->db->from($this->tbl_assessment2004.' as asesmen');
        $this->db->group_by('asesmen_value.id_rules');
        $this->db->join($this->tbl_asesmen_2004_value.' as asesmen_value','asesmen_value.id_asesmen = asesmen.id');
        $this->db->join($this->tbl_rules_asms_2004.' as rules_asms','asesmen_value.id_rules = rules_asms.id');
        $this->db->where('asesmen.instalasi_id',$instalasi_id);
        $this->db->where('asesmen.delete_flag',0);
        return $this->db->get()->result();
    }
    function jumlah_asesmen2015_instalasi($instalasi_id = 0) {
        $this->load->database('default', TRUE);
        $this->db->select('*');
        $this->db->from($this->tbl_assessment2015);
        $this->db->where('instalasi_id',$instalasi_id);
        $this->db->where('delete_flag',0);
        $this->db->order_by('date_created', 'desc');
        return $this->db->get()->row();
    }
    function rataan_kompetensi($jenis_bisnis_id) {
        $this->load->database('default', TRUE);
        $this->db->select('user.level_user_id as level_user, kompetensi.final_score as final_score');
        $this->db->from($this->tbl_standar_kompetensi_result.' as kompetensi');
        $this->db->join($this->tbl_user.' as user','user.id = kompetensi.user_id');
        $this->db->join($this->tbl_instalasi.' as instalasi','user.instalasi_id = instalasi.id');
        $this->db->where('instalasi.jenis_bisnis_id', $jenis_bisnis_id);
        $this->db->where('kompetensi.delete_flag',0);
        return $this->db->get();
    }
    function rataan_training($jenis_bisnis_id) {
        $this->load->database('default', TRUE);
        $this->db->select('user.level_user_id as level_user, standar_kompetensi.standar_kompetensi_training_id as training');
        $this->db->from($this->tbl_standar_kompetensi_result.' as kompetensi');
        $this->db->join($this->tbl_standar_kompetensi_user.' as kompetensi_user','kompetensi.id = kompetensi_user.standar_kompetensi_result_id');
        $this->db->join($this->tbl_standar_kompetensi.' as standar_kompetensi','kompetensi_user.standar_kompetensi_id = standar_kompetensi.id');
        $this->db->join($this->tbl_user.' as user','user.id = kompetensi.user_id');
        $this->db->join($this->tbl_instalasi.' as instalasi','user.instalasi_id = instalasi.id');
        $this->db->where('instalasi.jenis_bisnis_id', $jenis_bisnis_id);
        $this->db->where('kompetensi.delete_flag',0);
        return $this->db->get();
    }
    function rataan_stc($jenis_bisnis_id) {
        $this->load->database('default', TRUE);
        $this->db->select('standar_kompetensi.stc_id as stc_id, standar_kompetensi.score_standard, kompetensi_user.score as final_score');
        $this->db->from($this->tbl_standar_kompetensi_result.' as kompetensi');
        $this->db->join($this->tbl_standar_kompetensi_user.' as kompetensi_user','kompetensi_user.standar_kompetensi_result_id = kompetensi.id');
        $this->db->join($this->tbl_standar_kompetensi.' as standar_kompetensi','kompetensi_user.standar_kompetensi_id = standar_kompetensi.id');
        $this->db->join($this->tbl_user.' as user','user.id = kompetensi.user_id');
        $this->db->join($this->tbl_instalasi.' as instalasi','user.instalasi_id = instalasi.id');
        $this->db->where('instalasi.jenis_bisnis_id', $jenis_bisnis_id);
        $this->db->where('kompetensi.delete_flag',0);
        return $this->db->get();
    }
}