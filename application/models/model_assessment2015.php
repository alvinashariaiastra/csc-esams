<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_assessment2015 extends CI_Model {

	//var $column = array('b.name','a.periode','pemenuhan_system','a.security_performance','security_reliability','a.csi','a.nilai_akhir','a.critical_point','a.date_created'); //set column field database for order and search
	var $column = array(
		'a.id', 
		'b.name',
		'a.periode',
		'',
		'a.security_performance',
		'',
		'a.csi',
		'a.nilai_akhir',
		'a.critical_point',
		'a.date_created',
		'a.status'
	); //set column field database for order and search
	var $order = array('a.date_created' => 'desc'); // default order 

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	private function _get_datatables_query($status=1, $history=false, $id_created=false)
	{
		$select_revise = '';
		if ($history) 
			$select_revise = ',d.revise';
		$this->db->select('a.id, 
							a.instalasi_id, 
							b.name,
							a.date_created,
							sum(c.value)/4 as pemenuhan_system,
							a.periode,
							a.security_performance,
							(a.people + a.device_and_infrastructure)/2 as security_reliability, 
							a.csi,
							a.nilai_akhir,
							a.critical_point,
							a.people, 
							a.device_and_infrastructure,
							a.status'.$select_revise);
		$this->db->from($this->tbl_assessment2015.' a');
		$this->db->join($this->tbl_instalasi.' b', 'b.id = a.instalasi_id', 'left');
		$this->db->join($this->tbl_asesmen_2015_value.' c', 'c.id_asesmen = a.id', 'right');
		if ($this->session->userdata['admin']['id_usergroup']==1)
			$this->db->join($this->tbl_admin.' e', 'e.id = a.id_created', 'right');
		if ($history)
			$this->db->join($this->tbl_assessment2015_history.' d', 'd.asesmen_2015_id = a.id', 'left');
		$this->db->group_by('a.id');
		$this->db->where('a.delete_flag', 0);
		// Add by: Alvin (atsalvin0017), Date: 2019-08-09 10:52 AM
		if ($status!='all') {
			if (is_array($status)) {
				$this->db->where_in('a.status', $status);
			} else {
				$this->db->where('a.status', $status);
			}
		}
		if ($id_created==true)
			$this->db->where('a.id_created', $this->session_admin['admin_id']);
		if ($history)
			$this->db->where('d.status', 1);
		$i = 1;

		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
			{
				if ($i==1) {
					if ($item!='')
						$where = "(".$item." LIKE '%".$_POST['search']['value']."%' ";
				}else{
					if ($item!='')
						$where .= "OR ".$item." LIKE '%".$_POST['search']['value']."%' ";
				}

				if ($i == (count($this->column) - 1)) {
					$where .= ")";
					$this->db->where($where);
				}
			}
				
			$this->column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$_column = $this->column[($_POST['order']['0']['column']+1)];
			if ($_column=='') $_column = 'a.date_created';
			$this->db->order_by($_column, $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}
	private function _get_datatables_query_user($status=1, $history=false)
	{
		$select_revise = '';
		if ($history) 
			$select_revise = ',d.revise';
		$this->db->select('a.id, 
							a.instalasi_id, 
							b.name,
							a.date_created,
							sum(c.value)/4 as pemenuhan_system,
							a.periode,
							a.security_performance,
							(a.people + a.device_and_infrastructure)/2 as security_reliability, 
							a.csi,
							a.nilai_akhir,
							a.critical_point,
							a.people, 
							a.device_and_infrastructure,
							a.status'.$select_revise);
		$this->db->from($this->tbl_assessment2015.' a');
		$this->db->join($this->tbl_instalasi.' b', 'b.id = a.instalasi_id', 'left');
		$this->db->join($this->tbl_asesmen_2015_value.' c', 'c.id_asesmen = a.id', 'right');
		// Add by: Alvin (atsalvin0017), Date: 2019-08-15 13:30 AM
		if ($history)
			$this->db->join($this->tbl_assessment2015_history.' d', 'd.asesmen_2015_id = a.id', 'left');
		// 
		$this->db->group_by('a.id');
		$this->db->where('a.delete_flag', 0);
		// $this->db->where('a.instalasi_id', $this->session_user['id_instalasi']);
		$this->db->where('a.id_created', $this->session_user['admin_id']);
		// Add by: Alvin (atsalvin0017), Date: 2019-08-09 10:52 AM
		if ($status!='all') {
			if (is_array($status)) {
				$this->db->where_in('a.status', $status);
			} else
				$this->db->where('a.status', $status);
		}
		// Add by: Alvin (atsalvin0017), Date: 2019-08-15 13:30 AM
		if ($history) 
			$this->db->where('d.status', 1);
		$i = 1;

		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
			{
				if ($i==1) {
					if ($item!='')
						$where = "(".$item." LIKE '%".$_POST['search']['value']."%' ";
				}else{
					if ($item!='')
						$where .= "OR ".$item." LIKE '%".$_POST['search']['value']."%' ";
				}

				if ($i == (count($this->column) - 1)) {
					$where .= ")";
					$this->db->where($where);
				}
			}
				
			$this->column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$_column = $this->column[($_POST['order']['0']['column']+1)];
			if ($_column=='') $_column = 'a.date_created';
			$this->db->order_by($_column, $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($status=1, $history=false, $id_created=false)
	{
		$this->_get_datatables_query($status, $history, $id_created);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function get_datatables_user($status=1, $history=false)
	{
		$this->_get_datatables_query_user($status, $history);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($status=1)
	{
		$this->_get_datatables_query();
		$this->db->where('status', $status);
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_filtered_user()
	{
		$this->_get_datatables_query_user();
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_all($status=1)
	{
		$this->db->from($this->tbl_assessment2015);
		$this->db->where('status', $status);
		$this->db->where('delete_flag', 0);

		return $this->db->count_all_results();
	}

	function count_all_user()
	{
		$this->db->from($this->tbl_assessment2015);
		$this->db->where('delete_flag', 0);
		$this->db->where('instalasi_id', $this->session_user['id_instalasi']);

		return $this->db->count_all_results();
	}

	public function get_by_id($table,$id)
	{
		$this->db->from($this->tbl_assessment2015);
		$this->db->where('id',$id);
		$this->db->where('delete_flag', 0);

		$query = $this->db->get();

		return $query->row();
	}

	/*
		Created by: Alvin (atsalvin0017)
		Date: 2019-08-13 10:35 AM
		Action: Get data assessment 2015 which not resubmit by admin
    */
	public function get_count_by_status($table,$status,$id_created=null)
	{
		$this->db->from($this->tbl_assessment2015.' a');
		if ($id_created==1)
			$this->db->join($this->tbl_admin.' b', 'b.id = a.id_created', 'right');
		if (is_array($status)) {
			$this->db->where_in('a.status',$status);
		} else
			$this->db->where('a.status',$status);
		if ($id_created and $id_created>1)
			$this->db->where('a.id_created',$id_created);
		$this->db->where('a.delete_flag', 0);

		return $this->db->count_all_results();
	}

	public function save($table,$data)
	{
		$this->db->insert($this->tbl_assessment2015, $data);
		return $this->db->insert_id();
	}

	public function update($table,$where, $data)
	{
		$this->db->update($this->tbl_assessment2015, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($table,$id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->tbl_assessment2015);
	}
	public function sum_value($table,$sum,$column,$row_value)
	{
		$this->db->select('SUM('.$sum.') as jumlah');
		$this->db->where($column, $row_value);
		$this->db->from($table);
		return $this->db->get()->row();
	}

	public function get_asesment_by_date($date_start, $date_end) {
		$periode_start = date('Y',strtotime($date_start));
		$periode_end = date('Y',strtotime($date_end));
		$this->db->select('a.id, b.name, a.periode,a.security_performance, a.people, a.device_and_infrastructure,a.csi,a.nilai_akhir, a.critical_point, a.date_created');
		$this->db->from($this->tbl_assessment2015.' a');
		$this->db->join($this->tbl_instalasi.' b', 'b.id = a.instalasi_id', 'left');
		$this->db->where('a.periode >=', $periode_start);
        $this->db->where('a.periode <=', $periode_end);
		$this->db->where('a.delete_flag', 0);
		$this->db->order_by('a.date_created', 'desc');

		return $this->db->get();
	}

}

/* End of file model_assessment2015.php */
/* Location: ./application/models/model_assessment2015.php */