<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_status_kerawanan extends PX_Model {

	var $column = array('a.id', 'b.name AS instalasi_nm', 'c.name AS perusahaan_nm', 'd.name AS jenis_bisnis_nm', 'e.name AS status_nm', 'a.is_approved', 'a.date_created'); //set column field database for order and search
	var $order = array('a.date_created' => 'desc'); // default order 

	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	private function _get_datatables_query()
	{
		$this->db->select('a.id, b.name AS instalasi_nm, c.name AS perusahaan_nm, d.name AS jenis_bisnis_nm, e.name AS status_nm, a.is_approved, a.date_created');
		$this->db->from($this->tbl_status_kerawanan.' a');
		$this->db->join($this->tbl_instalasi.' b', 'b.id = a.instalasi_id', 'left');
		$this->db->join($this->tbl_perusahaan.' c', 'c.id = b.perusahaan_id', 'left');
		$this->db->join($this->tbl_jenis_bisnis.' d', 'd.id = b.jenis_bisnis_id', 'left');
		$this->db->join($this->tbl_status_kerawanan_status.' e', 'e.id = a.status', 'left');
		$this->db->where('a.delete_flag', 0);
		$i = 1;

		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
			{
				if ($i==1) {
					$where = "(".$item." LIKE '%".$_POST['search']['value']."%' ";
				}else{
					$where .= "OR ".$item." LIKE '%".$_POST['search']['value']."%' ";
				}

				if ($i == (count($this->column) - 1)) {
					$where .= ")";
					$this->db->where($where);
				}
			}
				
			$this->column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($this->column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->tbl_status_kerawanan);
		$this->db->where('delete_flag', 0);

		return $this->db->count_all_results();
	}

	public function get_by_id($table,$id)
	{
		$this->db->from($this->tbl_status_kerawanan);
		$this->db->where('id',$id);
		$this->db->where('delete_flag', 0);

		$query = $this->db->get();

		return $query->row();
	}

	public function save($table,$data)
	{
		$this->db->insert($this->tbl_status_kerawanan, $data);
		return $this->db->insert_id();
	}

	public function update($table,$where, $data)
	{
		$this->db->update($this->tbl_status_kerawanan, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($table,$id)
	{
		$this->db->where('id', $id);
		$this->db->delete($this->tbl_status_kerawanan);
	}
	public function sum_value($table,$sum,$column,$row_value)
	{
		$this->db->select('SUM('.$sum.') as jumlah');
		$this->db->where($column, $row_value);
		$this->db->from($table);
		return $this->db->get()->row();
	}


}

/* End of file model_status_kerawanan.php */
/* Location: ./application/models/model_status_kerawanan.php */