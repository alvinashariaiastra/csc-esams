<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_useraccess_user extends PX_Model {
	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}
	function get_all(){
		$this->db->select('access.*,grup.usergroup_name as grup_name');
		$this->db->from($this->tbl_useraccess_user.' as access');
		$this->db->join($this->tbl_usergroup_user.' as grup','grup.id = access.id_usergroup','LEFT');
		$this->db->where('access.delete_flag', 0);
		$this->db->group_by('access.id_usergroup');
		return $this->db->get()->result();
	}
	function get_available_user(){
		$this->db->select($this->tbl_usergroup_user.'.*');
		$this->db->from($this->tbl_usergroup_user);
		$this->db->join($this->tbl_useraccess_user.' as akses','akses.id_usergroup = '.$this->tbl_usergroup_user.'.id','LEFT');
		$this->db->where('akses.id_usergroup',NULL);
		$this->db->where($this->tbl_usergroup_user.'.delete_flag', 0);
		return $this->db->get()->result();
	}
	
    function get_useraccess($group_id, $menu_id)
    {
        $this->db->select('*');
        $this->db->where('id_usergroup', $group_id);
        $this->db->where('id_menu', $menu_id);
        $query = $this->db->get($this->tbl_useraccess_user);
        return $query->num_rows() == 1 ? $query->row() : FALSE;
    }	
}

/* End of file model_useraccess_user.php */
/* Location: ./application/models/model_useraccess_user.php */