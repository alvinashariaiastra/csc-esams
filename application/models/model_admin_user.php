<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_admin_user extends PX_Model {
	
	public function __construct() {
		parent::__construct();
	}
	function get_all(){
		$this->db->select($this->tbl_admin.'.*,'.$this->tbl_usergroup.'.usergroup_name');
		$this->db->from($this->tbl_admin);
		$this->db->join($this->tbl_usergroup,$this->tbl_usergroup.'.id = '.$this->tbl_admin.'.id_usergroup','left');
		return $this->db->get()->result();
	}
}