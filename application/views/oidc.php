<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter Saml Client</title>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}

	img#logo {
		text-align: center;
		width: 300px;
		display: block;
		margin-left: auto;
		margin-right: auto;
		padding-bottom: 3em;
		padding-top: 3em;
	}

	table {
		width: 100%;
	}

	th {
		height: 50px;
	}

	th {
   	 	background-color: #4CAF50;
    	color: white;
	}

	.atts-list {
		list-style: none;
	}

	tbody {
		background: antiquewhite;
	}

    ul, li {
        list-style: none;
    }
	</style>
</head>
<body>

<div id="container">	
	<h1>Welcome to CodeIgniter SAML2 And OIDC Client</h1>

	<div id="body">		
	
		<div style="text-align: center">
			<ul>
                <li>Access Token: <?php echo $access_token ?></li>
                <li>Refresh Token: <?php echo $refresh_token ?></li>
                <li>Expire Time: <?php echo $token_expire_time ?></li>                
            </ul>            

            <h3>User Data</h3>
            <ul>
            <?php foreach($resource_owner as $key => $val){ ?>
                <li><?php echo $key .": ".$val ?></li>
            <?php } ?>
            </ul>
		</div>
		
	</div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</div>

</body>
</html>