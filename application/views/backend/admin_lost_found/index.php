<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">

            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Data</h3>
                    <!-- <a class="btn btn-success pull-right btn-add" href="<?php echo $controller . '/user_lost_found_form' ?>"><i class="fa fa-plus"></i> Add New</a> -->                            
                </div>
                <div class="panel-body">
                    <table class="table datatable table-bordered">
                        <thead>
                            <tr>
                                <th width="6%" class="text-center">No</th>
                                <th class="text-center">User</th>
                                <th class="text-center">Judul</th>
                                <th class="text-center">Tipe</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Date Created</th>
                                <th width="15%" class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1;
                            //echo json_encode($lost_found);die();
                            foreach ($lost_found as $d_row) { ?>
                                <tr>
                                    <td class="text-center"><?php echo $no; ?></td>
                                    <td class="text-center"><?php echo $d_row->realname; ?></td>
                                    <td class="text-center"><?php if($d_row->type == 1) echo 'LOST'; else echo 'FOUND'; ?></td>
                                    <td class="text-center"><?php echo $d_row->title; ?></td>
                                    <td class="text-center">
                                        <?php if($d_row->flag == 0)
                                                echo '<button class="btn btn-danger">Unapproved</button>';
                                            else if($d_row->flag == 1)
                                                echo '<button class="btn btn-warning">On Process</button>';
                                            else if($d_row->flag == 2)
                                                echo '<button class="btn btn-success">Completed</button>';
                                            else
                                                echo '<button class="btn btn-danger">Rejected</button>';
                                        ?>
                                    </td>
                                    <td class="text-center"><?php echo $d_row->date_created; ?></td>
                                    <td class="text-center">
                                        <a href="admin_lost_found/detail/<?php echo $d_row->id ?>" class="btn btn-primary">Show Detail</a>
                                        <button class="btn btn-danger btn-xs btn-delete" type="button" data-original-title="Delete" data-placement="top" data-toggle="tooltip" data-target-id="<?php echo $d_row->id; ?>"><i class="fa fa-trash-o"></i></button>
                                    </td>
                                </tr>
                                <?php $no++;
                            } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END DEFAULT DATATABLE -->

        </div>
    </div>                                

</div>
<!-- PAGE CONTENT WRAPPER -->
<!-- MESSAGE BOX -->
<div id="px-lost_found-lost_found-message-box" class="message-box message-box-warning animated fadeIn fade">
	<div class="mb-container">
		<div class="mb-middle">
			<form action="<?php echo $controller.'/'.$function_delete; ?>" method="post" id="px-lost_found-lost_found-message-form">
			<input type="hidden" name="id">
			<div class="mb-title"><span class="fa fa-warning"></span> Warning</div>
			<div class="mb-content">
				<p>Are you sure you want to delete this data?</p>
				<p class="msg-status"></p>                  
			</div>
			<div class="mb-footer">
				<button class="btn btn-danger btn-lg pull-right" type="submit">Delete</button>
				<button class="btn btn-default btn-lg pull-right mb-control-close" type="button">Cancel</button>
			</div>
			</form>
		</div>
	</div>
</div>
<!-- EOF MESSAGE BOX -->

<!-- START SCRIPTS -->               
<!-- THIS PAGE PLUGINS -->
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>   
<!-- END PAGE PLUGINS -->
<!-- START TEMPLATE -->
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>

<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
<!-- END TEMPLATE -->  
<!-- THIS PAGE JS SETTINGS -->
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/lost_found/lost_found.js"></script>
<!--  -->
<!-- END SCRIPTS -->   