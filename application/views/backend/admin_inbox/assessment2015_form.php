<style type="text/css">
	.noneevent {
		pointer-events: none;
	}
	.mandatory {
		color: red;
	}
</style>

<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
	<li><a href="<?php echo $controller.'/'.$function; ?>"><?php echo $function_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?> Form</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">                                
					<h3 class="panel-title">Form</h3>                              
				</div>
				<form class="form-horizontal" id="px-company-assessment2015-form" method="POST" action="<?php if($data) echo $controller.'/'.$function_edit; else echo $controller.'/'.$function_add; ?>">
				<input type="hidden" value="<?php if($data!=null) echo $data->id; ?>" name="id">
				<div class="panel-body">
					<div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
					<div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
					<div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-assessment2015-form-assessment2015-periode">Periode</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control noneevent" id="px-company-assessment2015-form-assessment2015-periode" value="<?php if($data!=null) echo $data->periode; else echo date('Y'); ?>">
							<input type="hidden" name="periode" value="<?php if($data!=null) echo $data->periode; else echo date('Y'); ?>">
						</div>
					</div>
					<div class="form-group hidden">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-assessment2015-form-assessment2015-name">Name</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control <?php echo $is_admin==0?'noneevent':''; ?>" name="name" id="px-company-assessment2015-form-assessment2015-name" value="<?php if($data!=null) echo $data->name; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-assessment2015-form-assessment2015-businesstype">Instalasi</label>
						<div class="col-md-6 col-xs-12">
							<select readonly class="form-control noneevent" name="instalasi_id" id="px-company-assessment2015-form-assessment2015-businesstype">
								<?php 
								if ($company) {
									foreach ($company->result() as $data_row) {
										if ($data!=null && $data->instalasi_id == $data_row->id) {
												$selected = "selected";
											}else if($this->session_user['id_instalasi'] == $data_row->id){
												$selected = "selected";
											}else if($data->instalasi_id == $data_row->id){
												$selected = "selected";
											}else{
												$selected = "";
											}
										?>
										<option value="<?php echo $data_row->id ?>"<?php echo $selected ?>><?php echo $data_row->name ?></option>
								<?php
									}
								}
								 ?>
							</select>
						</div>
					</div>
					<?php $el = 1; foreach ($rules as $r) { ?>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-assessment2015-form-assessment2015-rules<?php echo $el; ?>"><?php echo $r->name; ?></label>
						<div class="col-md-6 col-xs-12">
							<input type="hidden" name="rules_id[]" value="<?php echo $r->id; ?>">
							<input type="text" class="form-control <?php echo $is_admin==0?'noneevent':''; ?> rules-element" data-element="<?php echo $el; ?>" name="rules_value[<?php echo $r->id; ?>]" id="px-company-assessment2015-form-assessment2015-rules<?php echo $el; ?>" value="<?php if($data!=null && isset($r->value)) echo $r->value; else echo 0; ?>">
						</div>
					</div>
					<?php $el++;} ?>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-assessment2015-form-assessment2015-pemenuhan_system">Pemenuhan System</label>
						<div class="col-md-6 col-xs-12">
							<input readonly="" type="text" class="form-control rules-last-value" name="pemenuhan_system" id="px-company-assessment2015-form-assessment2015-pemenuhan_system" value="<?php if($data!=null) echo $data->security_performance; else echo 0; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-assessment2015-form-assessment2015-security_performance">Security Performance</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control <?php echo $is_admin==0?'noneevent':''; ?> rules-last-value" name="security_performance" id="px-company-assessment2015-form-assessment2015-security_performance" value="<?php if($data!=null) echo $data->security_performance; else echo 0; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-assessment2015-form-assessment2015-people">Security Realibility People</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control <?php echo $is_admin==0?'noneevent':''; ?> rules-security-reliability" name="people" id="px-company-assessment2015-form-assessment2015-people" value="<?php if($data!=null) echo $data->people; else echo 0; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-assessment2015-form-assessment2015-device_and_infrastructure">Security Realibility Device and Infrastructure</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control <?php echo $is_admin==0?'noneevent':''; ?> rules-security-reliability" name="device_and_infrastructure" id="px-company-assessment2015-form-assessment2015-device_and_infrastructure" value="<?php if($data!=null) echo $data->device_and_infrastructure; else echo 0; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-assessment2015-form-assessment2015-security_reliability">Security Reliability</label>
						<div class="col-md-6 col-xs-12">
							<input readonly="" type="text" class="form-control rules-last-value" name="security_reliability" id="px-company-assessment2015-form-assessment2015-security_reliability" value="<?php if($data!=null) echo $data->security_performance; else echo 0; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-assessment2015-form-assessment2015-csi">CSI</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control <?php echo $is_admin==0?'noneevent':''; ?> rules-last-value" name="csi" id="px-company-assessment2015-form-assessment2015-csi" value="<?php if($data!=null) echo $data->csi; else echo 0; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-assessment2015-form-assessment2015-nilai_akhir">Nilai Akhir</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control <?php echo $is_admin==0?'noneevent':''; ?>" name="nilai_akhir" id="px-company-assessment2015-form-assessment2015-nilai_akhir" value="<?php if($data!=null) echo $data->nilai_akhir; else echo 0; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-assessment2015-form-assessment2015-nilai_akhir_warna">Nilai Akhir Warna</label>
						<div class="col-md-6 col-xs-12">
							<div id="nilai-akhir-colour"></div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-assessment2015-form-assessment2015-critical_point">Critical Point</label>
						<div class="col-md-10 col-xs-12">
							<textarea class="form-control <?php echo $is_admin==0?'noneevent':''; ?> px-summernote" name="critical_point" id="<?php echo $is_admin==1?'px-company-assessment2015-form-assessment2015-critical_point':''; ?>" <?php if ($is_admin==0) { ?>style="display: none;"<?php } ?>><?php if($data!=null) echo htmlentities($data->critical_point, ENT_QUOTES); ?></textarea>
							<?php if ($is_admin==0) { ?>
								<div style="background-color: #f9f9f9; padding: 10px;"><?php echo $data->critical_point; ?></div>
							<?php } ?>
						</div>
					</div>
					<?php if (count($history_revise)>0) { ?>
						<div class="form-group">
							<label class="col-md-2 col-xs-12 control-label" for="#px-company-assessment2015-form-assessment2015-history-revise">History Revise</label>
							<div class="col-md-10 col-xs-12">
								<?php foreach ($history_revise as $row) { ?>   
									<small><?php echo date('M, d Y, (H:i)', strtotime($row->date_created)); ?></small> 
	                        		<div style="background-color: #f9f9f9; padding: 10px;"><?php echo $row->revise; ?></div>
								<?php } ?>
							</div>
						</div>
					<?php } ?>
					<div class="form-group" id="div-revise">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-assessment2015-form-assessment2015-revise">Revise Note <sup class="mandatory">*</sup></label>
						<div class="col-md-10 col-xs-12">
							<textarea class="form-control px-summernote" name="revise" id="px-company-assessment2015-form-assessment2015-revise"></textarea>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<div class="pull-right">
						<input type="hidden" name="id_created" value="<?php echo $data->id_created; ?>">
						<?php if ($is_admin==0) { ?>
							<input type="hidden" name="_submit" value="Approve">
							<button class="btn btn-primary" type="button" value="Revise" id="ok" onclick="formDialog('revise')" style="display: none;">OK</button>
							<button class="btn btn-default" type="button" id="cancel" style="display: none;">Cancel</button>
							<button class="btn btn-warning" type="button" id="revise" data-toggle="modal" data-target="modal-revise">Revise</button>
							<button class="btn btn-primary" type="button" value="Submit" id="approve" onclick="formDialog('approve')">Approve</button>
						<?php } else { ?>
							<button class="btn btn-primary pull-right" type="button" onclick="formDialog('approve')">Approve</button>
						<?php } ?>
					</div>
				</div>
				</form>
			</div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                                
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- MODAL -->
<div class="modal fade animated" id="modal-revise" tabindex="-1" role="dialog" aria-labelledby="px-system-menu-modal-label" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="px-system-menu-modal-label">Are you sure want to <span id="span-dialog"></span> this data?</h4>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info" onclick="submitForm()" value="Submit">Yes</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
<!-- EOF MODAL -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>  
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>    
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/inbox/assessment2015_form.js"></script>
	<!--  -->
<!-- END SCRIPTS -->