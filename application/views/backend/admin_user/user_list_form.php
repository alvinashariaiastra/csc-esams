<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
	<li><a href="<?php echo $controller.'/'.$function; ?>"><?php echo $function_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?> Form</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">                                
					<h3 class="panel-title">Form</h3>                              
				</div>
				<form class="form-horizontal" id="px-admin-user-user-list-form" method="POST" action="<?php if($data) echo $controller.'/'.$function_edit; else echo $controller.'/'.$function_add; ?>">
				<input type="hidden" value="<?php if($data!=null) echo $data->id; ?>" name="id" id="px-admin-user-user-list-form-user-list-id">
				<div class="panel-body">
					<div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
					<div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
					<div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-admin-user-user-list-form-user-user-group">User Group</label>
						<div class="col-md-6 col-xs-12">
							<select class="form-control" name="user_group_id" id="px-admin-user-user-list-form-user-user-group">
								<option value="0">-- Pilih Group --</option>
								<?php 
									if ($user_group_num_row > 0) {
										foreach ($user_group as $row) { 
											if ($data!=null) {
												if ($data->user_group_id == $row->id) {
													$sel = "selected";
												}else{
													$sel = "";
												}
											}else{
												$sel = "";
											}
											?>
											<option value="<?php echo $row->id ?>" <?php echo $sel ?>><?php echo $row->usergroup_name ?></option>
										<?php }
									}
								 ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-admin-user-user-list-form-user-level-user">Level User</label>
						<div class="col-md-6 col-xs-12">
							<select class="form-control" name="level_user_id" id="px-admin-user-user-list-form-user-level-user">
								<option value="1" <?php if($data!=NULL) echo ($data->level_user_id == 1 ? "selected" : "") ?>>Management</option>
								<option value="2" <?php if($data!=NULL) echo ($data->level_user_id == 2 ? "selected" : "") ?>>Middle Management</option>
								<option value="3" <?php if($data!=NULL) echo ($data->level_user_id == 3 ? "selected" : "") ?>>Operational</option>
                                                                <option value="4" <?php if($data!=NULL) echo ($data->level_user_id == 4 ? "selected" : "") ?>>Guard</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-admin-user-user-list-form-user-list-perusahaan">Perusahaan</label>
						<div class="col-md-6 col-xs-12">
							<select class="form-control" name="perusahaan_id" id="px-admin-user-user-list-form-user-list-perusahaan">
								<option value="0">-- Pilih Perusahaan --</option>
								<?php foreach ($perusahaan as $row) { ?>
									<option value="<?php echo $row->id ?>" <?php if($data!=NULL) echo ($data->perusahaan_id == $row->id ? "selected" : "") ?>><?php echo $row->name ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-admin-user-user-list-form-user-list-instalasi">Instalasi</label>
						<div class="col-md-6 col-xs-12">
							<select class="form-control" name="instalasi_id" id="px-admin-user-user-list-form-user-list-instalasi">
								<option value="0">-- Pilih Instalasi --</option>
								<?php if ($data!=NULL) {
									foreach ($instalasi as $row) { ?>
										<option value="<?php echo $row->id ?>" <?php if($data!=NULL) echo ($data->instalasi_id == $row->id ? "selected" : "") ?>><?php echo $row->name ?></option>
								<?php }
								} ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-admin-user-user-list-form-user-list-jabatan">Jabatan</label>
						<div class="col-md-6 col-xs-12">
							<select class="form-control" name="jabatan_id" id="px-admin-user-user-list-form-user-list-jabatan">
								<option value="0">-- Pilih Jabatan --</option>
								<?php foreach ($jabatan as $row) { ?>
									<option value="<?php echo $row->id ?>" <?php if($data!=NULL) echo ($data->jabatan_id == $row->id ? "selected" : "") ?>><?php echo $row->jabatan ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-admin-user-user-list-form-user-list-username">Username</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="username" id="px-admin-user-user-list-form-user-list-username" value="<?php if($data!=null) echo $data->username; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-admin-user-user-list-form-user-list-password">Password</label>
						<div class="col-md-6 col-xs-12">
							<input type="password" class="form-control" name="password" id="px-admin-user-user-list-form-user-list-password" value="<?php if($data!=null) echo $data->password; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-admin-user-user-list-form-user-list-confirm-password">Confirm Password</label>
						<div class="col-md-6 col-xs-12">
							<input type="password" class="form-control" name="confirm_password" id="px-admin-user-user-list-form-user-list-confirm-password" value="<?php if($data!=null) echo $data->password; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-admin-user-user-list-form-user-list-realname">Real Name</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="realname" id="px-admin-user-user-list-form-user-list-realname" value="<?php if($data!=null) echo $data->realname; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-admin-user-user-list-form-user-list-email">Email</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="email" id="px-admin-user-user-list-form-user-list-email" value="<?php if($data!=null) echo $data->email; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-admin-user-user-list-form-user-list-phone">Phone</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="telp" id="px-admin-user-user-list-form-user-list-phone" value="<?php if($data!=null) echo $data->telp; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-admin-user-user-list-form-user-list-bio">Bio</label>
						<div class="col-md-6 col-xs-12">
							<textarea class="form-control" name="bio" id="px-admin-user-user-list-form-user-list-bio"><?php if($data!=null) echo $data->bio; ?></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-admin-user-user-list-form-user-list-address">Address</label>
						<div class="col-md-6 col-xs-12">
							<textarea class="form-control" name="address" id="px-admin-user-user-list-form-user-list-address"><?php if($data!=null) echo $data->address; ?></textarea>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label">Profile Pic</label>
						<div class="col-md-6 col-xs-12">  
							<input type="hidden" name="old_photo" value="<?php if($data!=null) echo $data->photo; ?>">
							<input type="hidden" name="photo"> 
                                                        <input type="hidden" name="x" id="x">
							<input type="hidden" name="y" id="y">
							<input type="hidden" name="w" id="w">
							<input type="hidden" name="h" id="h">
							<input type="hidden" name="origwidth" id="origwidth">
							<input type="hidden" name="origheight" id="origheight">
							<input type="hidden" name="fakewidth" id="fakewidth">
							<input type="hidden" name="fakeheight" id="fakeheight">
							<label for="file-upload-file" class="btn btn-primary btn-upload" data-target="photo">Browse</label>
						</div>
						<div class="gallery col-md-2 col-xs-12 col-md-offset-2 no-padding" id="preview_photo">
							<div class="image-original-preview-profile" id="image-original-previews">
								<img src="<?=base_url()?>assets/uploads/user/<?php if($data!=null) echo $data->id.'/'.$data->photo; ?>" alt="photo" id="original-image"/>                                                                                                           
							</div>
							<div class="image-crop-previews-profile" id="image-crop-previews">
								<img src="<?=base_url()?>assets/uploads/user/<?php if($data!=null) echo $data->id.'/'.$data->photo; ?>" alt="photo" id="crop-image"/>  
							</div>
                                                    <!--
								<div class="image">
									<img src="<?=base_url()?>assets/uploads/user/<?php if($data!=null) echo $data->id.'/'.$data->photo; ?>" alt="Gambar Profil"/>                                                                                                           
								</div>                               
                                                    -->
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<button class="btn btn-primary pull-right" type="submit">Save</button>
				</div>
				</form>
			</div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                                
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- FORM UPLOAD -->
<form id="file-upload" action="upload/image" method="POST" enctype="multipart/form-data" class="hidden">
	<input type="hidden" name="target" id="target-file">
	<input type="hidden" name="old" id="old-file">
	<input type="file" name="image" id="file-upload-file">
</form>
<!-- EOF FORM UPLOAD -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/bootstrap/bootstrap-file-input.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>  
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/fileupload/fileupload.min.js"></script>  
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/user/user_list_form.js"></script>
	<!--  -->
<!-- END SCRIPTS -->   