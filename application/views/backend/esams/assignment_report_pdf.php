<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style>
		table {
		  font-family: arial;
		  font-size: 12px;
		  border-collapse: collapse;
		  width: 100%;
		}

		td, th {
		  border: 1px solid #dddddd;
		  text-align: left;
		  padding: 5px;
		  width: 25%;
		}
	</style>
</head>
<body>
	<div class="col-md-8">
		<?php foreach ($data['assignment_detail'] as $k => $v) { ?>
			<div id="user-<?php echo $v['user_id']; ?>" class="collapse" style="padding-top: 15px; padding-bottom: 15px;">
				<?php foreach ($v['detail'] as $k1 => $v2) { ?>
				<div class="col-md-11 col-md-offset-1">
					<table class="table table-bordered">
						<tr>
							<td style="width: 25%;"><center><img src="<?php echo base_url("assets/uploads/esams/logo/astra.png"); ?>" style="max-width: 280px;"></center></td>
							<th colspan="2" style="width: 50%;"><center><h1>Laporan Hasil Penugasan</h1></center></th>
							<td style="width: 25%;"><center><img src="<?php echo base_url("assets/uploads/esams/logo/SECD.jpg"); ?>" style="max-width: 150px;"></center></td>
						</tr>
						<tr>
							<th>Nama Personil</th>
							<td colspan="3"><?php echo $v2['user_name']; ?></td>
						</tr>
						<tr>
							<th>Tanggal Penugasan</th>
							<td colspan="3"><?php echo $v2['start_time']; ?></td>
						</tr>
						<tr>
							<th>Lokasi Penugasan</th>
							<td colspan="3"><?php echo $v2['location']; ?></td>
						</tr>
						<tr>
							<th>Tanggal Check In</th>
							<td colspan="3"><?php echo $v2['checkin_date']; ?></td>
						</tr>
						<tr>
							<th>Tanggal Submit Laporan</th>
							<td colspan="3"><?php echo $v2['report_date']; ?></td>
						</tr>
						<tr>
							<th colspan="4"><center>Deskripsi Penugasan</center></th>
						</tr>
						<tr>
							<td colspan="4"><center><?php echo $v2['description_task']?$v2['description_task']:'Tidak ada deskripsi'; ?></center></td>
						</tr>
						<tr>
							<th colspan="4"><center>Hasil Penugasan</center></th>
						</tr>
						<tr>
							<td colspan="4"><center><?php echo $v2['report_text']?$v2['report_text']:'Tidak ada laporan'; ?></center></td>
						</tr>
						<tr>
							<th colspan="4"><center>Gambar Penugasan</center></th>
						</tr>
						<?php if (!empty($v2['images_report'][0])) { ?>
							<tr>
								<td colspan="2"><center><?php echo $v2['images_report'][0]?'<img src="'.$v2['images_report'][0].'" style="max-width: 230px; max-height: 230px;">':''; ?></center></td>
								<td colspan="2"><center><?php echo $v2['images_report'][1]?'<img src="'.$v2['images_report'][1].'" style="max-width: 230px; max-height: 230px;">':''; ?></center></td>
							</tr>
							<tr>
								<td colspan="2"><center><?php echo $v2['images_report'][2]?'<img src="'.$v2['images_report'][2].'" style="max-width: 230px; max-height: 230px;">':''; ?></center></td>
								<td colspan="2"><center><?php echo $v2['images_report'][3]?'<img src="'.$v2['images_report'][3].'" style="max-width: 230px; max-height: 230px;">':''; ?></center></td>
							</tr>
						<?php } else { ?>
							<tr>
								<td colspan="4">Tidak ada gambar laporan</td>
							</tr>
						<?php } ?>
						<tr>
							<td colspan="4"><?php echo $v2['assignment_agreement']; ?></td>
						</tr>
					</table>
				</div>				
				<br/>
				<?php } ?>
			</div>
			<br/>
		<?php } ?>
	</div>
</body>
</html>