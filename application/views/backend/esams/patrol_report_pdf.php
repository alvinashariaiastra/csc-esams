<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<style>
			table {
			  font-family: arial;
			  font-size: 12px;
			  border-collapse: collapse;
			  width: 100%;
			}

			td, th {
			  border: 1px solid #dddddd;
			  text-align: left;
			  padding: 5px;
			  width: 50%;
			}

		</style>
	</head>
	<body>
		<table>
			<tr>
				<td style="border: none; text-align: left;"><img src="<?php echo base_url("assets/uploads/esams/logo/astra.png"); ?>" style="max-width: 280px;"></td>
				<td style="border: none; text-align: right;"><img src="<?php echo base_url("assets/uploads/esams/logo/SECD.jpg"); ?>" style="max-width: 150px;"></td>
			</tr>
		</table>
		<center><h1>Laporan Hasil Patroli Petugas</h1></center>
		<div class="col-md-8">
			<?php foreach ($data as $k => $v) { ?>
				<div id="user-<?php echo $v['user_id']; ?>" class="collapse">
					<table>
						<tr>
							<th width="10%">Nama Personil</th>
							<td width="50%"><?php echo $v['header']['personel']; ?></td>
						</tr>
						<tr>
							<th width="10%">Tanggal Patroli</th>
							<td width="50%"><?php echo $v['header']['date']; ?></td>
						</tr>
						<tr>
							<th width="10%">Shift Patroli</th>
							<td width="50%"><?php echo $v['header']['shift']['name'].' ('.$v['header']['shift']['start_time'].' - '.$v['header']['shift']['end_time'].' )'; ?></td>
						</tr>
						<tr>
							<th width="10%">Lokasi Patroli</th>
							<td width="50%"><?php echo $v['header']['location']; ?></td>
						</tr>
						<tr>
							<th width="10%">Gedung Patroli</th>
							<td width="50%"><?php echo $v['header']['building']; ?></td>
						</tr>
					</table>
					<br/><br/>
					<table style="background-color: #f1f5f9; color: #56688A;">
						<tr>
							<th><center><h3>CHECKLIST PATROLI</h3></center></th>
						</tr>
					</table>
					<table>
						<?php foreach ($v['checklist_patrol'] as $k1 => $v1) { ?>
							<thead>
								<tr>
									<th colspan="2"><?php echo $v1['checkpoint_name']; ?></th>
								</tr>
							</thead>
							<?php if (count($v1['checklist'])) { ?>
								<?php foreach ($v1['checklist'] as $k2 => $v2) { ?>
									<tr>
										<td><?php echo $v2['name']; ?></td>
										<td><?php echo $v2['value']; ?></td>
									</tr>
								<?php } ?>
							<?php } else { ?>
								<tr>
									<td>Tidak ada laporan</td>
								</tr>
							<?php } ?>
						<?php } ?>
					</table>
					<br/><br/>
					<?php foreach ($v['detail_patrol'] as $k3 => $v3) { ?>
						<table style="background-color: #f1f5f9; color: #56688A;">
							<tr>
								<th><center><h3>DETAIL PATROLI</h3></center></th>
							</tr>
						</table>
						<table>
							<tr>
								<th width="10%">Lokasi Checkpoint</th>
								<td width="50%"><?php echo $v3['checkpoint_name']; ?></td>
							</tr>
							<tr>
								<th width="10%">Waktu Verifikasi</th>
								<td width="50%"><?php echo $v3['checkin']?$v3['checkin']['verification_time']:'-'; ?></td>
							</tr>
							<tr>
								<th width="10%">Metode Verifikasi</th>
								<td width="50%"><?php echo $v3['checkin']?$v3['checkin']['verification_method']:'-'; ?></td>
							</tr>
						</table>
						<?php if ($v3['checkin']) { ?>
							<table style="background-color: #f7f7f7;">
								<tr>
									<td><center><h4>CHECKLIST</h4></center></td>
								</tr>
							</table>
							<table>
								</thead>
									<tr>
										<th style="font-size: 13px"><center>Titik Pengecekan</center></th>
										<th style="font-size: 13px"><center>Hasil Pengamatan</center></th>
									</tr>
								</thead>
								<?php if (count($v3['checklist'])) { ?>
									<?php foreach ($v3['checklist'] as $k4 => $v4) { ?>
										<tr>
											<td><?php echo $v4['name']; ?></td>
											<td><?php echo $v4['value']; ?></td>
										</tr>
									<?php } ?>
								<?php } else { ?>
									<tr>
										<td colspan="2">Tidak ada laporan</td>
									</tr>
								<?php } ?>
							</table>
							<table style="background-color: #f7f7f7;">
								<tr>
									<td><center><h4>DETAIL LAPORAN</h4></center></td>
								</tr>
							</table>
							<table>
								<tr>
									<td><?php echo $v3['description']?$v3['description']:'Tidak ada laporan'; ?></td>
								</tr>
							</table>
							<table style="background-color: #f7f7f7;">
								<tr>
									<td><center><h4>GAMBAR HASIL PATROLI</h4></center></td>
								</tr>
							</table>
							<table>
								<?php if ($v3['images_patrol'][0]) { ?>
									<tr>
										<td><center><?php echo $v3['images_patrol'][0]?'<img src="'.$v3['images_patrol'][0].'" style="max-width: 280px;">':''; ?></center></td>
										<td><center><?php echo $v3['images_patrol'][1]?'<img src="'.$v3['images_patrol'][1].'" style="max-width: 280px;">':''; ?></center></td>
									</tr>
									<tr>
										<td><center><?php echo $v3['images_patrol'][2]?'<img src="'.$v3['images_patrol'][2].'" style="max-width: 280px;">':''; ?></center></td>
										<td><center><?php echo $v3['images_patrol'][3]?'<img src="'.$v3['images_patrol'][3].'" style="max-width: 280px;">':''; ?></center></td>
									</tr>
								<?php } else { ?>
									<tr>
										<td colspan="2">Tidak ada laporan</td>
									</tr>
								<?php } ?>
							</table>
							<table style="background-color: #f7f7f7;">
								<tr>
									<td><center><h4>CATATAN ADMIN</h4></center></td>
								</tr>
							</table>
							<table>
								<tr>
									<td><?php echo $v3['admin_notes']?$v3['admin_notes']:'Tidak ada catatan'; ?></td>
								</tr>
							</table>
						<?php } ?>
						<br/>
					<?php } ?>
				</div>
			<?php } ?>
		</div>
	</body>
</html>
