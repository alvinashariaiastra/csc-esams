<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li>E-Sams</li>                    
	<li><a href="<?php echo $modul.'/'.$controller; ?>"><?php echo $controller_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">                                
					<h3 class="panel-title">Form</h3>                              
				</div>
				<form class="form-horizontal" id="px-esams-patrol-form" method="POST" action="<?php echo $modul.'/'.$controller.'/'.($data?$function_edit:$function_add); ?>">
					<input type="hidden" value="<?php echo $data?$data->id:''; ?>" name="id">
					<input type="hidden" value="<?php echo $tab?$tab:''; ?>" name="tab">
					<div class="panel-body">
						<div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
						<div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
						<div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
						<div class="form-group">
							<label class="col-md-2 col-xs-12 control-label" for="#px-esams-assignment-form-start-time">Date <sup class="mandatory">*</sup></label>
							<div class="col-md-2 col-xs-12">
								<input type="text" class="form-control datepicker" name="date" id="px-esams-patrol-form-date" style="<?php echo $tab?$tab=='inprogress'?'pointer-events: none;':'':''; ?>" value="<?php echo $data?$data->date:date('d-m-Y'); ?>">
							</div>
							<div class="col-md-8"><i class="fa fa-lg fa-calendar"></i></div>
						</div>
						<div class="form-group">
							<label class="col-md-2 col-xs-12 control-label" for="#px-esams-patrol-form-instalasi_id">Instalasi <sup class="mandatory">*</sup></label>
							<div class="col-md-6 col-xs-12">
								<select class="form-control select2ins" name="instalasi_id" id="px-esams-patrol-form-instalasi_id" style="width: 100%;" <?php echo $tab?$tab=='inprogress'?'disabled="readonly"':'':''; ?>>
									<option value="" selected="" disabled=""></option>
									<?php foreach ($instalasi as $key => $value) { ?>
										<option value="<?php echo $value->id; ?>" <?php echo $data?($data->instalasi_id==$value->id?'selected':''):''; ?>><?php echo $value->name; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 col-xs-12 control-label" for="#px-esams-patrol-form-building_id">Building <sup class="mandatory">*</sup></label>
							<div class="col-md-6 col-xs-12">
								<select class="form-control select2ins" name="building_id" id="px-esams-patrol-form-building_id" style="width: 100%;" <?php echo $tab?$tab=='inprogress'?'disabled="readonly"':'':''; ?>>
									<option value="" selected="" disabled=""></option>
									<?php foreach ($building as $key => $value) { ?>
										<option value="<?php echo $value->id; ?>" <?php echo $data?($data->building_id==$value->id?'selected':''):''; ?>><?php echo $value->building_name; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 col-xs-12 control-label" for="#px-esams-patrol-form-shift_id">Shift <sup class="mandatory">*</sup></label>
							<div class="col-md-6 col-xs-12">
								<select class="form-control select2ins" name="shift_id" id="px-esams-patrol-form-shift_id" style="width: 100%;" <?php echo $tab?$tab=='inprogress'?'disabled="readonly"':'':''; ?>>
									<option value="" selected="" disabled=""></option>
									<?php foreach ($shift as $key => $value) { ?>
										<option value="<?php echo $value->id; ?>" <?php echo $data?($data->shift_id==$value->id?'selected':''):''; ?>><?php echo $value->shift_name.' ('.$value->start_time.' - '.$value->end_time.')'; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 col-xs-12 control-label" for="#px-esams-patrol-form-personel">Assigned Personel <sup class="mandatory">*</sup></label>
							<div class="col-md-6 col-xs-12">
								<select class="form-control select2ins" name="personel[]" multiple="multiple" id="px-esams-patrol-form-personel" style="width: 100%;" <?php echo $tab?$tab=='inprogress'?'disabled="readonly"':'':''; ?>>
									<option value="" disabled=""></option>
									<?php foreach ($personel as $key => $value) { ?>
										<option value="<?php echo $value->id; ?>" <?php echo $value->selected==1?'selected':''; ?>><?php echo $value->realname; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-2 col-xs-12"></div>
							<div class="col-md-10 col-xs-12">
								<div class="row">
									<?php if ($tab) { if ($tab=='assignment') { ?>
										<div class="col-md-10">
											<small>* Klik add detail in bottom if you want to add or more</small><br/>
			                                <button type="button" class="btn btn-default btn-sm" id="adddetailpattern">Add Detail</button>
										</div>
										<div class="col-md-2">
											<select class="form-control select2ins" name="pattern_id" id="px-esams-patrol-form-pattern_id" style="width: 100%;">
												<option value="" selected="" disabled="">Select Pattern</option>
												<?php foreach ($pattern as $key => $value) { ?>
													<option value="<?php echo $value->id; ?>"><?php echo $value->pattern_name; ?></option>
												<?php } ?>
											</select>
										</div>
									<?php } } ?>
									<div class="col-md-12 ">
		                                <div id="divadddetailpattern">
		                                	<table class="table" id="tableadddetailpattern" style="margin-top: 10px;">
		                                		<thead>
		                                			<tr>
		                                				<th style="width: 15%;">Sequence<sup class="mandatory">*</sup></th>
		                                				<th style="width: 40%;">Checkpoint<sup class="mandatory">*</sup></th>
		                                				<th style="width: 15%;">Action</th>
		                                				<th style="width: 30%;"></th>
		                                			</tr>
		                                		</thead>
		                                        <tbody id="tbodyadddetailpattern"></tbody>
		                                    </table>
		                                </div>
		                                <div id="divdeletedetailpattern"></div>
		                            </div>
		                            <!-- <div class="col-md-12 ">
		                            	<input type="text" name="checkpoint_id" id="checkpoint_id"></input>
		                            	<input type="text" name="get_id" id="get_id"></input>
		                            </div> -->
		                        </div>
                            </div>
						</div>
					</div>
					<div class="panel-footer">
						<?php if ($tab) { ?>
							<?php if ($tab=='assignment') { ?>
								<a href="<?php echo $modul.'/'.$controller.'/assignment'; ?>" class="btn btn-default pull-right">Back</a>
								<a href="" class="btn btn-primary pull-right" style="color: transparent; background-color: transparent; border-color: transparent;">&nbsp;</a>
								<button type="submit" class="btn btn-primary pull-right">Submit</button>
							<?php } else if ($tab=='inprogress') { ?>
								<a href="<?php echo $modul.'/'.$controller.'/'.$tab; ?>" class="btn btn-default pull-right">Back</a>
							<?php } ?>
						<?php } ?>
					</div>
				</form>
			</div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                                
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- MODAL -->
<div class="modal fade animated" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="px-system-menu-modal-label" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title" id="px-system-menu-modal-label">Are you sure want to <span id="span-dialog"></span> this data?</h4>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-info" onclick="submitFormMain()" value="Submit">Yes</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade animated" id="modal-verify" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title text-center">Are you sure you want to verify personnel attendance on this check point?</h4>
			</div>
			<div class="modal-body">
				<div class="panel-body">
					<div class="form-group">
						<label>Please input the reason <sup class="mandatory">*</sup></label>
						<textarea class="form-control" name="reason" id="reason"></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="patrol_pattern_id">
				<button type="button" class="btn btn-info" onclick="submitForm('<?php echo $modul.'/'.$controller.'/patrol_form'; ?>')" value="Submit">Yes</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
<!-- EOF MODAL -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>  
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>    
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/esams/patrol_form.js"></script>
	<!--  -->
<!-- END SCRIPTS -->