<!--START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li>E-Sams</li>                    
	<li><a href="<?php echo $modul.'/'.$controller; ?>"><?php echo $controller_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">                                
					<h3 class="panel-title">Form</h3>                              
				</div>
				<form class="form-horizontal" id="px-esams-patrol-form" method="POST" action="<?php echo $modul.'/'.$controller.'/'.($data?$function_edit:$function_add); ?>">
					<div class="panel-body">
						<h1>Laporan Hasil Penanganan Petugas</h1>
						<div class="col-md-8">
							<table class="table table-bordered">
								<tr>
									<th>Nomor Laporan</th>
									<td><?php echo $data['ticket_number']?$data['ticket_number']:'Tidak ada keterangan'; ?></td>
								</tr>
								<tr>
									<th>Nama Personil</th>
									<td><?php echo $data['nama_pelapor']?$data['nama_pelapor']:'Tidak ada keterangan'; ?></td>
								</tr>
								<tr>
									<th>Tanggal Laporan Dibuat</th>
									<td><?php echo $data['created_date']?$data['created_date']:'Tidak ada keterangan'; ?></td>
								</tr>
								<tr>
									<th>Jenis Laporan</th>
									<td><?php echo $data['report_type']?$data['report_type']:'Tidak ada keterangan'; ?></td>
								</tr>
								<tr>
									<th>Lokasi</th>
									<td><?php echo $data['lokasi']?$data['lokasi']:'Tidak ada keterangan'; ?></td>
								</tr>
								<tr>
									<th>Gedung</th>
									<td><?php echo $data['building']?$data['building']:'Tidak ada keterangan'; ?></td>
								</tr>
								<tr>
									<th>Lantai</th>
									<td><?php echo $data['floor']?$data['floor']:'Tidak ada keterangan'; ?></td>
								</tr>
								<tr>
									<th>Area</th>
									<td><?php echo $data['area']?$data['area']:'Tidak ada keterangan'; ?></td>
								</tr>
							</table>
							<h4>Fakta-Fakta</h4>
							<table class="table table-bordered">
								<tr>
									<td><?php echo $data['fakta']?$data['fakta']:'Tidak ada keterangan'; ?></td>
								</tr>
							</table>
							<h4>Analisa Awal</h4>
							<table class="table table-bordered">
								<tr>
									<td><?php echo $data['analysis']?$data['analysis']:'Tidak ada keterangan'; ?></td>
								</tr>
							</table>
							<h4>Kesimpulan dan Langkah Penanganan</h4>
							<table class="table table-bordered">
								<tr>
									<td><?php echo $data['summary']?$data['summary']:'Tidak ada keterangan'; ?></td>
								</tr>
							</table>
							<table class="table table-bordered">
								<?php if (!empty($data['issue_attachment'][0])) { ?>
									<tr>
										<td align="center"><?php echo $data['issue_attachment'][0]?'<a href="'.$data['issue_attachment'][0].'" target="_blank"><img src="'.$data['issue_attachment'][0].'" style="max-width: 280px;"></a>':''; ?></td>
										<td align="center"><?php echo $data['issue_attachment'][1]?'<a href="'.$data['issue_attachment'][1].'" target="_blank"><img src="'.$data['issue_attachment'][1].'" style="max-width: 280px;"></a>':''; ?></td>
									</tr>
									<tr>
										<td  colspan="2" align="center"><?php echo $data['issue_attachment'][2]?'<a href="'.$data['issue_attachment'][2].'" target="_blank"><img src="'.$data['issue_attachment'][2].'" style="max-width: 280px;"></a>':''; ?></td>
									</tr>
								<?php } else { ?>
									<tr>
										<td colspan="2">Tidak ada gambar laporan</td>
									</tr>
								<?php } ?>
							</table>
							<?php if ($data['detail']){
								/*echo json_encode($data['detail']); die();*/
								foreach ($data['detail'] as $k1 => $v2) { ?>
									<button type="button" class="form-control" data-toggle="collapse" data-target="#user-<?php echo $v2['user_id']; ?>">
										<span class="pull-left"><?php echo $v2['personel']; ?></span>
										<i class="fa fa-chevron-circle-down pull-right"></i>
									</button>
									<div id="user-<?php echo $v2['user_id']; ?>" class="collapse" style="padding-top: 15px; padding-bottom: 15px;">
										<fieldset>
											<div class="col-md-11 col-md-offset-1">
												<h4>Hasil Investigasi</h4>
												<table class="table table-bordered">
													<tr>
														<th>Nama Petugas</th>
														<td><?php echo $v2['personel']?$v2['personel']:'Tidak ada keterangan'; ?></td>
													</tr>
													<tr>
														<th>Tanggal Laporan Dibuat</th>
														<td><?php echo isset($v2['report_date'])?$v2['report_date']:'Tidak ada keterangan'; ?></td>
													</tr>
													<tr>
														<td colspan="2"><?php echo isset($v2['investigation_results'])?$v2['investigation_results']:'Tidak ada keterangan'; ?></td>
													</tr>
													<?php if (!empty($v2['issue_report_images'][0])) { ?>
														<tr>
															<td align="center"><?php echo $v2['issue_report_images'][0]?'<img src="'.$v2['issue_report_images'][0].'" style="max-width: 280px;">':''; ?></td>
															<td align="center"><?php echo $v2['issue_report_images'][1]?'<img src="'.$v2['issue_report_images'][1].'" style="max-width: 280px;">':''; ?></td>
														</tr>
														<tr>
															<td colspan="2" align="center"><?php echo $v2['issue_report_images'][2]?'<img src="'.$v2['issue_report_images'][2].'" style="max-width: 280px;">':''; ?></td>
														</tr>
													<?php } else { ?>
														<tr>
															<td colspan="2">Tidak ada gambar laporan</td>
														</tr>
													<?php } ?>
												</table>
											</div>
										</fieldset>
									</div>
									<br/>
								<?php } 
							}?>
							<h4>Catatan Admin</h4>
							<table class="table table-bordered">
								<tr>
									<td><?php echo $data['note_admin']?$data['note_admin']:'Tidak ada keterangan'; ?></td>
								</tr>
							</table>
							<a href="<?php echo $modul.'/'.$controller.'/get_issue_report_pdf/'.$issue_id; ?>" class="btn btn-default pull-left">Download as PDF</a>
						</div>
					</div>
					<div class="panel-footer">
						<a href="<?php echo $modul.'/'.$controller.'/assignment_report'; ?>" class="btn btn-default pull-right">Back</a>
					</div>
				</form>
			</div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                                
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>  
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>    
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->
	<!--  -->
<!-- END SCRIPTS