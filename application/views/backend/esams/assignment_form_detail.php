<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li>E-Sams</li>                    
	<li><a href="<?php echo $modul.'/'.$controller; ?>"><?php echo $controller_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">                                
					<h3 class="panel-title">Form</h3>                              
				</div>
				<form class="form-horizontal" id="px-esams-assignment-form" method="POST" action="<?php echo $modul.'/'.$controller.'/assignment'; ?>" enctype="multipart/form-data">
					<input type="hidden" value="<?php echo $data?$data->id:''; ?>" name="id">
					<div class="panel-body">
						<div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
						<div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
						<div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
						<div class="form-group">
							<label class="col-md-2 col-xs-12 control-label" for="#px-esams-assignment-form-assignment_title">Assignment Title <sup class="mandatory">*</sup></label>
							<div class="col-md-6 col-xs-12">
								<input type="text" class="form-control" name="assignment_title" id="px-esams-assignment-form-assignment_title" value="<?php echo $data?$data->assignment_title:''; ?>" readonly>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 col-xs-12 control-label" for="#px-esams-assignment-form-assigned">Assigned Personel / Group</label>
							<div class="col-md-6 col-xs-12">
								<input type="radio" name="assigned" value="Presonel" <?php echo isset($data->assignment_people) && $data->assignment_people == "Personel"?"checked":"";?> style="<?php echo $data?'pointer-events: none;':''; ?>"> Personel
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="radio" name="assigned" value="Group" <?php echo isset($data->assignment_people) && $data->assignment_people == "Group"?"checked":"";?> style="<?php echo $data?'pointer-events: none;':''; ?>"> Group
								<?php echo $data?'<br/><small>You cannot set this type</small>':''; ?>
							</div>
						</div>
						<div class="form-group" id="div-assign-group" <?php if(empty($data->assignment_people)){ ?> hidden <?php } echo isset($data->assignment_people) && $data->assignment_people == "Personel"?"hidden":"";?>>
							<label class="col-md-2 col-xs-12 control-label" for="#px-esams-assignment-form-businesstype">Group <sup class="mandatory">*</sup></label>
							<div class="col-md-6 col-xs-12">
								<select class="form-control js-example-basic-multiple" name="group_security[]" multiple="multiple" id="px-system-usergroup-form-group_security" style="width: 100%;" disabled="true">
									<?php foreach ($group_security as $key => $value) { ?>
										<option value="<?php echo $value->id; ?>" <?php echo $value->selected==1?'selected':''; ?>><?php echo $value->groupsecurity_name; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group" id="div-assign-personel" <?php if(empty($data->assignment_people)){ ?> hidden <?php } echo isset($data->assignment_people) && $data->assignment_people == "Group"?"hidden":"";?>>
							<label class="col-md-2 col-xs-12 control-label" for="#px-esams-assignment-form-businesstype">Personel <sup class="mandatory">*</sup></label>
							<div class="col-md-6 col-xs-12">
								<select class="form-control js-example-basic-multiple" name="personel_security[]" multiple="multiple" id="px-system-usergroup-form-personel_security" style="width: 100%;" disabled="true">
									<?php foreach ($personel_security as $key => $value) { ?>
										<option value="<?php echo $value->id; ?>" <?php echo $value->selected==1?'selected':''; ?>><?php echo $value->realname; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 col-xs-12 control-label" for="#px-esams-assignment-form-assignment_type">Assignment Type <sup class="mandatory">*</sup></label>
							<div class="col-md-6 col-xs-12">
								<input type="radio" name="assignment_type" value="single" <?php echo isset($data->assignment_type) && $data->assignment_type == "single"?"checked":"";?> style="<?php echo $data?'pointer-events: none;':''; ?>"> Single
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="radio" name="assignment_type" value="series" <?php echo isset($data->assignment_type) && $data->assignment_type == "series"?"checked":"";?> style="<?php echo $data?'pointer-events: none;':''; ?>"> Series
								<?php echo $data?'<br/><small>You cannot set this type</small>':''; ?>
							</div>
						</div>
						<div class="form-group" id="div-assign-subtask" <?php echo ($data && $data->assignment_type=="series")?'':'hidden'; ?>>
							<label class="col-md-2 col-xs-12 control-label"></label>	
							<!-- <div class="col-md-8">
								<button type="button" id="save-task" class="btn btn-success">Add Form Sub Task</button>
							</div> -->
						</div>
						<div id="div_task" <?php echo empty($data)?'hidden':''; ?>>
							<?php if (empty($data)) {?>
								<div class="form-group div-task-series" id="div-assign-title" hidden>
									<label class="col-md-2 col-xs-12 control-label" for="#px-esams-assignment-form-task_title" >Task Title <sup class="mandatory">*</sup></label>
									<div class="col-md-6 col-xs-12">
										<input type="text" class="form-control" name="task_title" id="px-esams-assignment-form-task_title" value="">
									</div>
								</div>
							<?php } ?>
							<?php 
								$value = null;
								$display = true;
								if ($data) {
									if ($data->assignment_type=="single") $value = $data->assignment_detail[0]; 
									if ($data->assignment_type=="series") $display = false; 
								}
								if ($display==true) {
							?>
								<div class="form-group div-task-series">
									<label class="col-md-2 col-xs-12 control-label" for="#px-esams-assignment-form-start-time">Start Time <sup class="mandatory">*</sup></label>
									<div class="col-md-6 col-xs-12">
										<input type="text" class="form-control datetimepicker" style="padding: 5px 12px;" name="start_time" id="px-esams-assignment-form-start-time" value="<?php echo isset($value->start_time)?$value->start_time:''; ?>" readonly>
									</div>
								</div>
								<div class="form-group div-task-series">
									<label class="col-md-2 col-xs-12 control-label" for="#px-esams-assignment-form-end-time">End Time <sup class="mandatory">*</sup></label>
									<div class="col-md-6 col-xs-12">
										<input type="text" class="form-control datetimepicker" style="padding: 5px 10px;" name="end_time" id="px-esams-assignment-form-end-time" value="<?php echo isset($value->end_time)?$value->end_time:''; ?>" readonly>
									</div>
								</div>
								<div class="form-group div-task-series">
									<label class="col-md-2 col-xs-12 control-label" for="#px-system-usergroup-form-description">Description <sup class="mandatory">*</sup></label>
									<div class="col-md-10 col-xs-12">
										<textarea class="form-control px-summernote" name="description" id="px-system-usergroup-form-description" disabled="true">
											<?php echo isset($value->description)?$value->description:''; ?>
										</textarea>
									</div>
								</div>
		                        <!-- <div class="form-group div-task-series">
		                        	<label class="col-md-2 col-xs-12 control-label" for="#px-system-usergroup-form-map">Map</label>
		                            <div class="col-md-6 col-xs-12">
		                            	<button class="btn btn-success" type="button" onclick="formMap('modal', 's')">Set Location Map</button>
		                            </div>
		                        </div> -->
		                        <div class="form-group div-task-series">
		                        	<label class="col-md-2 col-xs-12 control-label" for="#px-system-usergroup-form-location">Location <sup class="mandatory">*</sup></label>
		                            <div class="col-md-6 col-xs-12">
		                                <input type="text" name="location" id="location_s" class="form-control input-sm" value="<?php echo isset($loc->location)?$loc->location:''; ?>" readonly="">
		                            </div>
		                        </div>
		                        <div class="form-group div-task-series">
		                        	<label class="col-md-2 col-xs-12 control-label" for="#px-system-usergroup-form-location">Latitude <sup class="mandatory">*</sup></label>
		                            <div class="col-md-6 col-xs-12">
		                                <input type="text" name="latitude" id="latitude_s" class="form-control input-sm" value="<?php echo isset($loc->latitude)?$loc->latitude:''; ?>" readonly="">
		                            </div>
		                        </div>
		                        <div class="form-group div-task-series">
		                        	<label class="col-md-2 col-xs-12 control-label" for="#px-system-usergroup-form-location">Longitude <sup class="mandatory">*</sup></label>
		                            <div class="col-md-6 col-xs-12">
		                                <input type="text" name="longitude" id="longitude_s" class="form-control input-sm" value="<?php echo isset($loc->longitude)?$loc->longitude:''; ?>" readonly="">
		                            </div>
		                        </div>
		                        <div class="form-group div-task-series">
									<label class="col-md-2 col-xs-12 control-label" for="#px-esams-assignment-form-assignment_radius">Radius <sup class="mandatory">*</sup></label>
									<div class="col-md-6 col-xs-12">
										<input type="text" class="form-control" name="radius" id="px-esams-assignment-form-assignment_radius" value="<?php echo isset($value->radius)?$value->radius:''; ?>" readonly>
									</div>
								</div>
								<div class="form-group" id="div-assign-image-single">
									<label class="col-md-2 col-xs-12 control-label">Image <sup class="mandatory">*</sup></label>
									<div class="col-md-6 col-xs-12">
										<!-- <input type="file" name="image_name" multiple="multiple"> -->
										<?php if ($value) { ?>
											<br/><label>Current Images</label>
											<div class="row">
												<?php foreach ($value->attachment as $keys => $values) { ?>
													<div class="col-md-4">
														<img src="<?php echo base_url('/assets/uploads/esams/assignment/'.$values->image_name) ?>" style="width: 100%; height: 120px;">
													</div>
												<?php } ?>
											</div>
										<?php } ?>
									</div>
									<div class="col-lg-12 text-center" id="preview_file_div"><ul></ul></div>
								</div>
							<?php } ?>
							<div class="form-group" id="div-assign-subtask-data" <?php echo ($data && $data->assignment_type=="series")?'':'hidden'; ?>>
								<label class="col-md-2 col-xs-12 control-label">List Sub Task</label>
							  	<div class="col-md-10" style="border-color: #adadad">
							  		<div id="div-data-series">
							  		<?php if ($data && $data->assignment_type=="series") { ?>
							  			<?php foreach($data->assignment_detail as $key => $value) { ?>
									  	  	<div class="records" style="padding: 15px; border-left: 2px solid #ccc; border-top: 1px solid #ccc;">
								  				<h4>Task <?php echo $key+1; ?></h4>
										  	    <div class="row">
										  	      	<div class="col-md-12">
										  	      		<div class="form-group">
										        			<label class="col-md-2 col-xs-12 control-label">Task Title <sup class="mandatory">*</sup></label>
										        			<div class="col-md-8 col-xs-12">
										        				<input type="text" class="form-control" name="task_title[]" value="<?php echo isset($value->task_title)?$value->task_title:''; ?>" readonly>
										        			</div>
									        			</div>
									        			<div class="form-group">
										        			<label class="col-md-2 col-xs-12 control-label">Start Time <sup class="mandatory">*</sup></label>
										        			<div class="col-md-8 col-xs-12">
										        				<input type="text" class="form-control" name="start_time[]" value="<?php echo isset($value->start_time)?$value->start_time:''; ?>" readonly>
										        			</div>
										        		</div>
									        			<div class="form-group">
									        				<label class="col-md-2 col-xs-12 control-label">End Time <sup class="mandatory">*</sup></label>
									        				<div class="col-md-8 col-xs-12">
									        					<input type="text" class="form-control" name="end_time[]" value="<?php echo isset($value->end_time)?$value->end_time:''; ?>" readonly>
									        				</div>
									        			</div>
									        			<div class="form-group">
									        				<label class="col-md-2 col-xs-12 control-label">Description <sup class="mandatory">*</sup></label>
									        				<div class="col-md-8 col-xs-12">
									        					<textarea class="form-control px-summernote" name="description[]" disabled="true"><?php echo isset($value->description)?$value->description:''; ?></textarea>
									        				</div>
									        			</div>
											            <div class="form-group">
									                        <label class="col-md-2 col-xs-12 control-label">Map</label>
									                        <!-- <div class="col-md-6 col-xs-12">
									                            <button class="btn btn-success" type="button" onclick="formMap('modal', '<?=$key?>')">Set Location Map</button>
									                        </div> -->
									                    </div>
									        			<div class="form-group">
									        				<label class="col-md-2 col-xs-12 control-label">Location <sup class="mandatory">*</sup></label>
									        				<div class="col-md-8 col-xs-12">
									        					<input type="text" class="form-control" name="location[]" id="location_<?=$key?>" value="<?php echo isset($loc->location)?$loc->location:''; ?>" readonly>
										        			</div>
										        		</div>
									        			<div class="form-group">
									        				<label class="col-md-2 col-xs-12 control-label">Latitude <sup class="mandatory">*</sup></label>
									        				<div class="col-md-8 col-xs-12">
									        					<input type="text" class="form-control" name="latitude[]" id="latitude_<?=$key?>" value="<?php echo isset($loc->latitude)?$loc->latitude:''; ?>" readonly>
										        			</div>
										        		</div>
									        			<div class="form-group">
									        				<label class="col-md-2 col-xs-12 control-label">Longitude <sup class="mandatory">*</sup></label>
									        				<div class="col-md-8 col-xs-12">
									        					<input type="text" class="form-control" name="longitude[]" id="longitude_<?=$key?>" value="<?php echo isset($loc->longitude)?$loc->longitude:''; ?>" readonly>
										        			</div>
										        		</div>
										        		<div class="form-group">
											            	<label class="col-md-2 col-xs-12 control-label">Radius <sup class="mandatory">*</sup></label>
											            	<div class="col-md-8 col-xs-12">
											            		<input type="text" class="form-control" name="radius[]" value="<?php echo isset($value->radius)?$value->radius:''; ?>"readonly>
											            	</div>
											            </div>
											            <div class="form-group">
											              	<label class="col-md-2 col-xs-12 control-label">Image</label>
											              	<div class="col-md-10 col-xs-12">
											              		<!-- <input type="file" name="image_name_<?php echo $key; ?>" multiple="multiple"> -->
																<br/><label>Current Images</label>
																<div class="row">
																	<?php foreach ($value->attachment as $keys => $values) { ?>
																		<div class="col-md-4">
																			<img src="<?php echo base_url('/assets/uploads/esams/assignment/'.$values->image_name) ?>" style="width: 100%; height: 120px;">
																		</div>
																	<?php } ?>
																</div>
															</div>
											            </div>
											            <hr/>
									                    <!-- <div class="form-group">
									                        <label class="col-md-2 col-xs-12 control-label"></label>
									                        <div class="col-md-10 col-xs-12">
									                            <input type="hidden" name="task[]" value="<?=$key?>">
									                            <input type="hidden" name="detailid[]" value="<?=$value->id?>">
									                            <a class="btn btn-danger btn-sm delsubtask" href="javascript:;">Delete</a>
									                        </div>
									                    </div> -->
										  	     	</div>
										  	  	</div>
									      	</div>
							      		<?php } ?>
							      	<?php } ?>
							      	</div>
							    </div>	
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<!-- <button type="submit" class="btn btn-primary pull-right">Back</button> -->
						<a href="<?php echo $modul.'/'.$controller.'/inprogress'; ?>" class="btn btn-default pull-right">Back</a>
						<!-- <a href="" class="btn btn-primary pull-right" style="color: transparent; background-color: transparent; border-color: transparent;">&nbsp;</a>
						<a href="javascript:window.history.go(-1);" class="btn btn-primary pull-right">Cancel</a> -->
					</div>
				</form>
			</div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                              
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>  
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>    
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
    <link rel="stylesheet" href="<?=base_url()?>assets/backend_assets/css/bootstrap/bootstrap-datetimepicker.min.css">
    <script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/bootstrap/bootstrap-datetimepicker.min.js"></script>
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/esams/assignment_form.js"></script>
	<!--  -->
<!-- END SCRIPTS -->


<!-- JS GMaps -->
<style type="text/css">
    .input-controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
    }
    .searchInput {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 50%;
    }
    .searchInput:focus {
        border-color: #4d90fe;
    }
</style>