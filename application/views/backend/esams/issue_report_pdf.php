<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style>
		table {
		  font-family: arial;
		  font-size: 12px;
		  border-collapse: collapse;
		  width: 100%;
		}

		td, th {
		  border: 1px solid #dddddd;
		  text-align: left;
		  padding: 5px;
		  width: 25%;
		}
	</style>
</head>
<body>
	<div class="col-md-8">
		<div class="col-md-11 col-md-offset-1">
			<table class="table table-bordered">
				<tr>
					<td style="width: 25%;"><center><img src="<?php echo base_url("assets/uploads/esams/logo/astra.png"); ?>" style="max-width: 280px;"></center></td>
					<th style="width: 50%;"><center><h1>LAPORAN PENANGANAN MASALAH</h1></center></th>
					<td style="width: 25%;"><center><img src="<?php echo base_url("assets/uploads/esams/logo/SECD.jpg"); ?>" style="max-width: 150px;"></center></td>
				</tr>
				<tr>
					<th>Nomor Laporan</th>
					<td colspan="2"><?php echo $data['ticket_number']?$data['ticket_number']:'Tidak ada keterangan'; ?></td>
				</tr>
				<tr>
					<th>Nama Personil</th>
					<td colspan="2"><?php echo $data['nama_pelapor']?$data['nama_pelapor']:'Tidak ada keterangan'; ?></td>
				</tr>
				<tr>
					<th>Tanggal Laporan Dibuat</th>
					<td colspan="2"><?php echo $data['created_date']?$data['created_date']:'Tidak ada keterangan';?></td>
				</tr>
				<tr>
					<th>Jenis Laporan</th>
					<td colspan="2"><?php echo $data['report_type']?$data['report_type']:'Tidak ada keterangan'; ?></td>
				</tr>
				<tr>
					<th>Lokasi</th>
					<td colspan="2"><?php echo $data['lokasi']?$data['lokasi']:'Tidak ada keterangan'; ?></td>
				</tr>
				<tr>
					<th>Gedung</th>
					<td colspan="2"><?php echo $data['building']?$data['building']:'Tidak ada keterangan'; ?></td>
				</tr>
				<tr>
					<th>Lantai</th>
					<td colspan="2"><?php echo $data['floor']?$data['floor']:'Tidak ada keterangan'; ?></td>
				</tr>
				<tr>
					<th>Area</th>
					<td colspan="2"><?php echo $data['area']?$data['area']:'Tidak ada keterangan'; ?></td>
				</tr>
				<tr>
					<th colspan="3"><center>Fakta-Fakta</center></th>
				</tr>
				<tr>
					<td colspan="3"><center><?php echo $data['fakta']?$data['fakta']:'Tidak ada keterangan'; ?></center></td>
				</tr>
				<tr>
					<th colspan="3"><center>Analisa Awal</center></th>
				</tr>
				<tr>
					<td colspan="3"><center><?php echo $data['analysis']?$data['analysis']:'Tidak ada keterangan'; ?></center></td>
				</tr>
				<tr>
					<th colspan="3"><center>Kesimpulan dan Langkah Penanganan</center></th>
				</tr>
				<tr>
					<td colspan="3"><center><?php echo $data['summary']?$data['summary']:'Tidak ada keterangan'; ?></center></td>
				</tr>
			</table>
			<table class="table table-bordered">
				<tr>
					<th colspan="2"><center>Gambar Laporan</center></th>
				</tr>
				<?php if (!empty($data['issue_attachment'][0])) { ?>
					<tr>
						<td><center><?php echo $data['issue_attachment'][0]?'<img src="'.$data['issue_attachment'][0].'" style="max-width: 280px;">':''; ?></center></td>
						<td><center><?php echo $data['issue_attachment'][1]?'<img src="'.$data['issue_attachment'][1].'" style="max-width: 280px;">':''; ?></center></td>
					</tr>
					<tr>
						<td colspan="2"><center><?php echo $data['issue_attachment'][2]?'<img src="'.$data['issue_attachment'][2].'" style="max-width: 280px;">':''; ?></center></td>
					</tr>
				<?php } else { ?>
					<tr>
						<td colspan="2">Tidak ada gambar laporan</td>
					</tr>
				<?php } ?>
			</table>
			<?php if ($data['detail']){
				foreach ($data['detail'] as $k1 => $v2) { ?>
					<div id="user-<?php echo $v2['ticket_number']; ?>" class="collapse" style="padding-top: 15px; padding-bottom: 15px;">
						<div class="col-md-11 col-md-offset-1">
							<table class="table table-bordered">
								<tr>
									<th colspan="2"><center><h2>Hasil Investigasi</h2></center></th>
								</tr>
								<tr>
									<th>Nama Petugas</th>
									<td><?php echo $v2['personel']?$v2['personel']:'Tidak ada keterangan'; ?></td>
								</tr>
								<tr>
									<th>Tanggal Laporan Dibuat</th>
									<td><?php echo isset($v2['report_date'])?$v2['report_date']:'Tidak ada keterangan'; ?></td>
								</tr>
								<tr>
									<th>Keterangan</th>
									<td><?php echo isset($v2['investigation_results'])?$v2['investigation_results']:'Tidak ada keterangan'; ?></td>
								</tr>
								<?php if (!empty($v2['issue_report_images'][0])) { ?>
									<tr>
										<td><center><?php echo $v2['issue_report_images'][0]?'<img src="'.$v2['issue_report_images'][0].'" style="max-width: 280px;">':''; ?></center></td>
										<td><center><?php echo $v2['issue_report_images'][1]?'<img src="'.$v2['issue_report_images'][1].'" style="max-width: 280px;">':''; ?></center></td>
									</tr>
									<tr>
										<td colspan="2"><center><?php echo $v2['issue_report_images'][2]?'<img src="'.$v2['issue_report_images'][2].'" style="max-width: 280px;">':''; ?></center></td>
									</tr>
								<?php } else { ?>
									<tr>
										<td colspan="2">Tidak ada gambar laporan</td>
									</tr>
								<?php } ?>
							</table>
						</div>
					</div>
					<br/>
				<?php } 
			}?>
			<table class="table table-bordered">
				<tr>
					<th><center><h2>Catatan Admin</h2></center></th>
				</tr>
				<tr>
					<td><?php echo $data['note_admin']?$data['note_admin']:'Tidak ada keterangan'; ?></td>
				</tr>
			</table>
		</div>
		<br/>
	</div>
</body>
</html>