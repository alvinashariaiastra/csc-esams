<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li>E-Sams</li>                    
	<li><a href="<?php echo $modul.'/'.$controller; ?>"><?php echo $controller_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">                                
					<h3 class="panel-title">Form</h3>                              
				</div>
				<form class="form-horizontal" id="px-esams-patrol-form" method="POST" action="<?php echo $modul.'/'.$controller.'/'.($data?$function_edit:$function_add); ?>">
					<div class="panel-body">
						<h1>Laporan Hasil Assignment Petugas</h1>
						<div class="col-md-8">
							<h4>Assignment Title: <?php echo $data['assignment_title']; ?></h4>
							<?php if (isset($data['assignment_detail'])) { ?>
								<?php foreach ($data['assignment_detail'] as $k => $v) { ?>
									<button type="button" class="form-control" data-toggle="collapse" data-target="#user-<?php echo $v['user_id']; ?>">
										<span class="pull-left"><?php echo $v['user_name']; ?></span>
										<i class="fa fa-chevron-circle-down pull-right"></i>
									</button>
	  								<div id="user-<?php echo $v['user_id']; ?>" class="collapse" style="padding-top: 15px; padding-bottom: 15px;">
	  									<?php foreach ($v['detail'] as $k1 => $v2) { ?>
											<fieldset>
												<legend>Detail Laporan (Task: <?php echo $v2['task_title']; ?>)</legend>
												<div class="col-md-11 col-md-offset-1">
													<table class="table table-bordered">
														<tr>
															<th>Nama Personil</th>
															<td><?php echo $v2['user_name']; ?></td>
														</tr>
														<tr>
															<th>Tanggal Penugasan</th>
															<td><?php echo $v2['start_time']; ?></td>
														</tr>
														<tr>
															<th>Lokasi Penugasan</th>
															<td><?php echo $v2['location']; ?></td>
														</tr>
														<tr>
															<th>Tanggal Check In</th>
															<td><?php echo $v2['checkin_date']; ?></td>
														</tr>
														<tr>
															<th>Tanggal Submit Laporan</th>
															<td><?php echo $v2['report_date']; ?></td>
														</tr>
													</table>
													<h4>Deskripsi Penugasan</h4>
													<table class="table table-bordered">
														<tr>
															<td><?php echo $v2['description_task']?$v2['description_task']:'Tidak ada deskripsi'; ?></td>
														</tr>
													</table>
													<h4>Hasil Penugasan</h4>
													<table class="table table-bordered">
														<tr>
															<td><?php echo $v2['report_text']?$v2['report_text']:'Tidak ada laporan'; ?></td>
														</tr>
													</table>
													<h4>Gambar Penugasan</h4>
													<table class="table table-bordered">
														<?php if (!empty($v2['images_report'][0])) { ?>
															<tr>
																<td align="center"><?php echo $v2['images_report'][0]?'<a href="'.$v2['images_report'][0].'" target="_blank"><img src="'.$v2['images_report'][0].'" style="max-width: 280px;"></a>':''; ?></td>
																<td align="center"><?php echo $v2['images_report'][1]?'<a href="'.$v2['images_report'][1].'" target="_blank"><img src="'.$v2['images_report'][1].'" style="max-width: 280px;"></a>':''; ?></td>
															</tr>
															<tr>
																<td align="center"><?php echo $v2['images_report'][2]?'<a href="'.$v2['images_report'][2].'" target="_blank"><img src="'.$v2['images_report'][2].'" style="max-width: 280px;"></a>':''; ?></td>
																<td align="center"><?php echo $v2['images_report'][3]?'<a href="'.$v2['images_report'][3].'" target="_blank"><img src="'.$v2['images_report'][3].'" style="max-width: 280px;"></a>':''; ?></td>
															</tr>
														<?php } else { ?>
															<tr>
																<td>Tidak ada gambar laporan</td>
															</tr>
														<?php } ?>
														<tr>
															<td colspan="2"><?php echo $v2['assignment_agreement']; ?></td>
														</tr>
													</table>
													<a href="<?php echo $modul.'/'.$controller.'/get_assignment_report_pdf/'.$data['assignment_id'].'/'.$v['user_id'].'/'.$v2['assignment_detail_id']; ?>" class="btn btn-default pull-left">Download as PDF</a>
												</div>
											</fieldset>
											<br/>
										<?php } ?>
									</div>
									<br/>
								<?php } ?>
							<?php } else { ?>
								<br/><h5 style="font-style: italic;">Tidak ada laporan.</h5>
							<?php } ?>
						</div>
					</div>
					<div class="panel-footer">
						<a href="<?php echo $modul.'/'.$controller.'/assignment_report'; ?>" class="btn btn-default pull-right">Back</a>
					</div>
				</form>
			</div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                                
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>  
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>    
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/esams/patrol_form.js"></script>
	<!--  -->
<!-- END SCRIPTS -->