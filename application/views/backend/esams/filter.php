<div class="filter">
	<form action="" method="post">
		<div class="row">
			<div class="col-md-10">
				<label>Sort</label>
				<select name="sort_c">
					<option value="" <?php echo $datas['sort_c']===false?'selected':''; ?>>None</option>
					<?php foreach ($datas['sort'] as $k => $v) { ?>
						<option value="<?php echo $k; ?>" <?php echo $datas['sort_c']!==false?($datas['sort_c']==(string)$k?'selected':''):''; ?>><?php echo $v; ?></option>
					<?php } ?>
				</select>
				<select name="sort_t">
					<option value="" <?php echo !$datas['sort_t']?'selected':''; ?>>None</option>
					<option value="asc" <?php echo $datas['sort_t']?($datas['sort_t']=='asc'?'selected':''):''; ?>>A-Z</option>
					<option value="desc" <?php echo $datas['sort_t']?($datas['sort_t']=='desc'?'selected':''):''; ?>>Z-A</option>
				</select>
			</div>
			<div class="col-md-2">
				<input type="text" name="search_t" class="form-control pull-right" placeholder="Search by text" value="<?php echo $datas['search_t']?$datas['search_t']:''; ?>">
			</div>
			<div class="col-md-10">
				<br/>
				<label>Show</label>
				<select name="show_r">
					<?php $show_list = array('All', 2, 5, 10, 25, 50, 100, 200); 
					for ($i=0; $i < count($show_list); $i++) { ?>
						<option value="<?php echo $show_list[$i]; ?>" <?php echo $datas['show_r']?($datas['show_r']==$show_list[$i]?'selected':''):''; ?>><?php echo $show_list[$i]; ?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-md-2">
				<br/>
				<button type="submit" class="btn btn-sm btn-primary pull-right" name="filter_a" value="1">Apply</button>
			</div>
		</div>
	</form>
</div>
<hr/>