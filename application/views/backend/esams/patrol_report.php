<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li>E-Sams</li>                    
	<li><a href="<?php echo $modul.'/'.$controller; ?>"><?php echo $controller_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">                                
					<h3 class="panel-title">Form</h3>                              
				</div>
				<form class="form-horizontal" id="px-esams-patrol-form" method="POST" action="<?php echo $modul.'/'.$controller.'/'.($data?$function_edit:$function_add); ?>">
					<div class="panel-body">
						<h1>Laporan Hasil Patroli Petugas</h1>
						<div class="col-md-8">
							<?php foreach ($data as $k => $v) { ?>
								<button type="button" class="form-control" data-toggle="collapse" data-target="#user-<?php echo $v['user_id']; ?>">
									<span class="pull-left"><?php echo $v['user_name']; ?></span>
									<i class="fa fa-chevron-circle-down pull-right"></i>
								</button>
  								<div id="user-<?php echo $v['user_id']; ?>" class="collapse" style="padding-top: 15px; padding-bottom: 15px;">
									<fieldset>
										<legend>Detail Laporan</legend>
										<div class="col-md-11 col-md-offset-1">
											<table class="table table-bordered">
												<tr>
													<th>Nama Personil</th>
													<td><?php echo $v['header']['personel']; ?></td>
												</tr>
												<tr>
													<th>Tanggal Patroli</th>
													<td><?php echo $v['header']['date']; ?></td>
												</tr>
												<tr>
													<th>Shift Patroli</th>
													<td><?php echo $v['header']['shift']['name'].' ('.$v['header']['shift']['start_time'].' - '.$v['header']['shift']['end_time'].' )'; ?></td>
												</tr>
												<tr>
													<th>Lokasi Patroli</th>
													<td><?php echo $v['header']['location']; ?></td>
												</tr>
												<tr>
													<th>Gedung Patroli</th>
													<td><?php echo $v['header']['building']; ?></td>
												</tr>
											</table>
											<h4>Checklist Patrol</h4>
											<table class="table table-bordered">
												<?php foreach ($v['checklist_patrol'] as $k1 => $v1) { ?>
													<thead>
														<tr>
															<th colspan="2"><?php echo $v1['checkpoint_name']; ?></th>
														</tr>
													</thead>
													<?php if (count($v1['checklist'])) { ?>
														<?php foreach ($v1['checklist'] as $k2 => $v2) { ?>
															<tr>
																<td><?php echo $v2['name']; ?></td>
																<td><?php echo $v2['value']; ?></td>
															</tr>
														<?php } ?>
													<?php } else { ?>
														<tr>
															<td>Tidak ada laporan</td>
														</tr>
													<?php } ?>
												<?php } ?>
											</table>
											<?php foreach ($v['detail_patrol'] as $k3 => $v3) { ?>
												<h4>Detail Patrol</h4>
												<table class="table table-bordered">
													<tr>
														<th width="40%">Lokasi Checkpoint</th>
														<td width="60%"><?php echo $v3['checkpoint_name']; ?></td>
													</tr>
													<tr>
														<th width="40%">Waktu Verifikasi</th>
														<td width="60%"><?php echo $v3['checkin']?$v3['checkin']['verification_time']:'-'; ?></td>
													</tr>
													<tr>
														<th width="40%">Metode Verifikasi</th>
														<td width="60%"><?php echo $v3['checkin']?$v3['checkin']['verification_method']:'-'; ?></td>
													</tr>
												</table>
												<?php if ($v3['checkin']) { ?>
													<h5>Checklist</h5>
													<table class="table table-bordered">
														</thead>
															<tr>
																<th>Titik Pengecekan</th>
																<th>Hasil Pengamatan</th>
															</tr>
														</thead>
														<?php if (count($v3['checklist'])) { ?>
															<?php foreach ($v3['checklist'] as $k4 => $v4) { ?>
																<tr>
																	<td><?php echo $v4['name']; ?></td>
																	<td><?php echo $v4['value']; ?></td>
																</tr>
															<?php } ?>
														<?php } else { ?>
															<tr>
																<td colspan="2">Tidak ada laporan</td>
															</tr>
														<?php } ?>
													</table>
													<h5>Detail Laporan</h5>
													<table class="table table-bordered">
														<tr>
															<td><?php echo $v3['description']?$v3['description']:'Tidak ada laporan'; ?></td>
														</tr>
													</table>
													<h5>Gambar Hasil Patroli</h5>
													<table class="table table-bordered">
														<?php if ($v3['images_patrol'][0]) { ?>
															<tr>
																<td><?php echo $v3['images_patrol'][3]?'<a href="'.$v3['images_patrol'][3].'" target="_blank"><img src="'.$v3['images_patrol'][3].'" style="max-width: 280px;"></a>':''; ?></td>
																<td><?php echo $v3['images_patrol'][0]?'<a href="'.$v3['images_patrol'][0].'" target="_blank"><img src="'.$v3['images_patrol'][0].'" style="max-width: 280px;"></a>':''; ?></td>
															</tr>
															<tr>
																<td><?php echo $v3['images_patrol'][1]?'<a href="'.$v3['images_patrol'][1].'" target="_blank"><img src="'.$v3['images_patrol'][1].'" style="max-width: 280px;"></a>':''; ?></td>
																<td><?php echo $v3['images_patrol'][2]?'<a href="'.$v3['images_patrol'][2].'" target="_blank"><img src="'.$v3['images_patrol'][2].'" style="max-width: 280px;"></a>':''; ?></td>
															</tr>
														<?php } else { ?>
															<tr>
																<td>Tidak ada laporan</td>
															</tr>
														<?php } ?>
													</table>
													<h5>Catatan Admin</h5>
													<table class="table table-bordered">
														<tr>
															<td><?php echo $v3['admin_notes']?$v3['admin_notes']:'Tidak ada catatan'; ?></td>
														</tr>
													</table>
												<?php } ?>
											<?php } ?>
											<a href="<?php echo $modul.'/'.$controller.'/patrol_report_pdf/'.$v['user_id'].'/'.$patrol_id; ?>" class="btn btn-default pull-left">Download as PDF</a>
										</div>
									</fieldset>
								</div>
								<br/>
							<?php } ?>
						</div>
					</div>
					<div class="panel-footer">
						<a href="<?php echo $modul.'/'.$controller.'/report'; ?>" class="btn btn-default pull-right">Back</a>
					</div>
				</form>
			</div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                                
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>  
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>    
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/esams/patrol_form.js"></script>
	<!--  -->
<!-- END SCRIPTS -->