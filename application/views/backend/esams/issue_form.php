<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li>E-Sams</li>                    
	<li><a href="<?php echo $modul.'/'.$controller; ?>"><?php echo $controller_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">                                
					<h3 class="panel-title">Form</h3>                              
				</div>
				<form class="form-horizontal" id="px-esams-issue-form" method="POST" action="<?php echo $modul.'/'.$controller.'/'.($data?$function_edit:$function_add); ?>">
					<input type="hidden" value="<?php echo $data?$data->id:''; ?>" name="id">
					<input type="hidden" value="<?php echo $tab?$tab:''; ?>" name="tab">
					<div class="panel-body">
						<div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
						<div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
						<div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
						<div class="form-group">
							<!-- Ticket Number -->
							<label class="col-md-2 col-xs-12 control-label" for="#px-esams-issue-form-ticket-number">Ticket Number</label>
							<div class="col-md-2 col-xs-12">
								<input type="text" class="form-control" name="ticket_number" id="px-esams-issue-form-ticket-number" style="pointer-events: none" value="<?php echo $data?$data->ticket_number:''; ?>">
							</div>
							<!-- Building -->
							<label class="col-md-2 col-xs-12 control-label" for="#px-esams-issue-form-building-name">Building</label>
							<div class="col-md-2 col-xs-12">
								<input type="text" class="form-control" name="building_name" id="px-esams-issue-form-building-name" style="pointer-events: none" value="<?php echo $data?$building->building_name:''; ?>">
							</div>
						</div>
						<div class="form-group">
							<!-- reporter -->
							<label class="col-md-2 col-xs-12 control-label" for="#px-esams-issue-form-created-by">Reporter</label>
							<div class="col-md-2 col-xs-12">
								<input type="text" class="form-control" name="created_by" id="px-esams-issue-form-created-by" style="pointer-events: none" value="<?php echo $data?$user->realname:''; ?>">
							</div>
							<!-- Floor -->
							<label class="col-md-2 col-xs-12 control-label" for="#px-esams-issue-form-floor">Floor</label>
							<div class="col-md-2 col-xs-12">
								<input type="text" class="form-control" name="floor" id="px-esams-issue-form-floor" style="pointer-events: none" value="<?php echo $data?$data->floor:''; ?>">
							</div>
						</div>
						<div class="form-group">
							<!-- location -->
							<label class="col-md-2 col-xs-12 control-label" for="#px-esams-issue-form-instalasi-name">Location</label>
							<div class="col-md-2 col-xs-12">
								<input type="text" class="form-control" name="instalasi_name" id="px-esams-issue-form-instalasi-name" style="pointer-events: none" value="<?php echo $data?$instalasi->name:''; ?>">
							</div>
							<!-- Area -->
							<label class="col-md-2 col-xs-12 control-label" for="#px-esams-issue-form-area">Area</label>
							<div class="col-md-2 col-xs-12">
								<input type="text" class="form-control" name="area" id="px-esams-issue-form-area" style="pointer-events: none" value="<?php echo $data?$data->area:''; ?>">
							</div>
						</div>
						<!-- description -->
						<!-- <div class="form-group">
							<label class="col-md-2 col-xs-12 control-label" for="#px-esams-issue-form-description">Description</label>
							<div class="col-md-2 col-xs-12">
								<textarea class="form-control px-summernote" name="description" id="px-system-usergroup-form-description" disabled="true">
									<?php echo $data?$data->description:''; ?>
								</textarea>
							</div>
						</div> -->
						<div class="form-group">
							<!-- Fakta -->
							<label class="col-md-2 col-xs-12 control-label" for="#px-esams-issue-form-fakta">Fakta-fakta</label>
							<div class="col-md-2 col-xs-12">
								<textarea class="form-control px-summernote" name="fakta" id="px-system-usergroup-form-fakta" disabled="true">
									<?php echo $data?$data->fact:''; ?>
								</textarea>
							</div>
						</div>
						<div class="form-group">
							<!-- Summary -->
							<label class="col-md-2 col-xs-12 control-label" for="#px-esams-issue-form-summary">Summary</label>
							<div class="col-md-2 col-xs-12">
								<textarea class="form-control px-summernote" name="summary" id="px-system-usergroup-form-summary" disabled="true">
									<?php echo $data?$data->summary:''; ?>
								</textarea>
							</div>
						</div>
						<div class="form-group">
							<!-- Analysis -->
							<label class="col-md-2 col-xs-12 control-label" for="#px-esams-issue-form-analysis">Analysis</label>
							<div class="col-md-2 col-xs-12">
								<textarea class="form-control px-summernote" name="analysis" id="px-system-usergroup-form-analysis" disabled="true">
									<?php echo $data?$data->analysis:''; ?>
								</textarea>
							</div>
						</div>
						<div class="form-group" id="div-assign-image-single">
							<label class="col-md-2 col-xs-12 control-label">
								Attachment<br/>
								<small style="font-style: italic;">*click picture to view large</small>
							</label>
							<div class="col-md-6 col-xs-12">
								<?php if ($attachment) { ?>
									<div class="row">
										<?php foreach ($attachment as $keys => $values) { ?>
											<div class="col-md-4">
												<a href="<?php echo base_url('/assets/uploads/esams/issue/'.$values->image_name) ?>" target="_blank">
													<img src="<?php echo base_url('/assets/uploads/esams/issue/'.$values->image_name) ?>" style="width: 100%; height: 120px;">
												</a>
											</div>
										<?php } ?>
									</div>
								<?php } ?>
							</div>
							<div class="col-lg-12 text-center" id="preview_file_div"><ul></ul></div>
						</div>

						<!-- form add action -->
						<div class="form-group">
							<label class="col-md-2 col-xs-12 control-label" for="#px-esams-issue-form-action">Action <sup class="mandatory">*</sup></label>
							<div class="col-md-6 col-xs-12">
								<select class="form-control" name="select_action" id="px-esams-issue-form-action" style="width: 50%;">
									<!-- <option value=""></option> -->
									<option name="action" value="escalate" <?php echo $data?($data->action=='escalate'?'selected':''):''; ?>>Eskalasi ke Div Lain</option>
									<option name="action" value="dispatch" <?php echo $data?($data->action=='dispatch'?'selected':''):''; ?>>Menugaskan Personel</option>
								</select>
							</div>
						</div>

						<!-- form add personel -->
						<div class="form-group" id="div-assign-personel" <?php echo $data?($data->action=='dispatch'?'hidden="false"':'hidden="true"'):'hidden="true"'; ?>>
							<label class="col-md-2 col-xs-12 control-label" for="#px-esams-issue-form-personel">Personel</label>
							<div class="col-md-6 col-xs-12">
								<select class="js-example-basic-multiple" name="member[]" multiple="multiple" id="px-system-issue-form-user-id" style="width: 100%;">
									<?php foreach ($personel_security as $key => $value) { ?>
										<option value="<?php echo $value->id; ?>" <?php echo $value->selected==1?'selected':''; ?>><?php echo $value->realname; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>

						<?php if ($tab == "inprogress") { ?>
							<?php if($issue_report){ ?>
								<!-- history -->
								<div class="form-group" style="margin-left:100px;">
									<h2>Report History</h2>
									<!-- <label>History Report</label> -->
									<?php foreach ($issue_report as $k => $v) { ?>
										<button style="width: 85%" type="button" class="form-control" data-toggle="collapse" data-target="#user-<?php echo $v->created_by; ?>">
											<span class="pull-left"><?php echo $v->realname; ?></span>
											<i class="fa fa-chevron-circle-down pull-right"></i>
										</button>
										<div id="user-<?php echo $v->created_by; ?>" class="collapse" style="padding-top: 15px; padding-bottom: 15px;">
											<fieldset>
												<legend>Report Detail</legend>
									            <div class="form-group">
													<!-- Ticket Number -->
													<label class="col-md-2 col-xs-12 control-label" for="#px-esams-issue-form-detail-ticket-number">Ticket Number</label>
													<div class="col-md-2 col-xs-12">
														<input type="text" class="form-control" name="ticket_number" id="px-esams-issue-form-detail-ticket-number" style="pointer-events: none" value="<?php echo $data->ticket_number; ?>">
													</div>
												</div>
												<div class="form-group">
													<!-- Personel -->
													<label class="col-md-2 col-xs-12 control-label" for="#px-esams-issue-form-detail-personel">Personel</label>
													<div class="col-md-2 col-xs-12">
														<input type="text" class="form-control" name="building_name" id="px-esams-issue-form-detail-personel" style="pointer-events: none" value="<?php echo $v->realname; ?>">
													</div>
												</div>
												<div class="form-group">
													<!-- description -->
													<label class="col-md-2 col-xs-12 control-label" for="#px-esams-issue-form-detail-report">report</label>
													<div class="col-md-2 col-xs-12">
														<textarea class="form-control px-summernote" name="report" id="px-system-usergroup-form-detail-report" disabled="true">
															<?php echo $v->report_text; ?>
														</textarea>
													</div>
												</div>
												<div class="form-group" id="div-assign-image-single">
													<label class="col-md-2 col-xs-12 control-label">
														Attachment<br/>
														<small style="font-style: italic;">*click picture to view large</small>
													</label>
													<div class="col-md-6 col-xs-12">
														<?php if ($v->attachment) { ?>
															<div class="row">
																<?php foreach ($v->attachment as $keys => $values) { ?>
																	<div class="col-md-4">
																		<a href="<?php echo base_url('/assets/uploads/esams/issue/'.$values->image_name) ?>" target="_blank">
																			<img src="<?php echo base_url('/assets/uploads/esams/issue/'.$values->image_name) ?>" style="width: 100%; height: 120px;">
																		</a>
																	</div>
																<?php } ?>
															</div>
														<?php } 
														else {echo("Gambar laporan tidak tersedia");}?>
													</div>
													<div class="col-lg-12 text-center" id="preview_file_div"><ul></ul></div>
												</div>
									        </fieldset>
							            </div>
							        <?php } ?>
								</div>
								<!-- end history -->
							<?php } ?>
						<?php } ?>


					</div>
					<div class="panel-footer">
						<?php if ($tab) { ?>
							<?php if ($tab=='assignment') { ?>
								<div class="pull-right">
									<!-- submit -->
									<button type="submit" class="btn btn-primary">Submit</button>

									<!-- Back -->
									<a href="<?php echo $modul.'/'.$controller.'/issue'; ?>" class="btn btn-primary" style="background-color: white; color: black">Back</a>
								</div>
							<?php } else if ($tab=='inprogress') { ?>
								<div class="pull-right">
									<!-- submit -->
									<button type="submit" class="btn btn-primary">Submit</button>

									<!-- close ticket -->
									<!-- <a href="<?php echo $modul.'/'.$controller.'/close_ticket/'.$data->id; ?>" class="btn btn-primary" id="btn-close" style="background-color: #4CAF50; border: none;">Close Ticket</a> -->

									<button class="btn btn-primary" type="button" onclick="formDialog()" style="background-color: #4CAF50; border: none;">Verify</button>

									<!-- Back -->
									<a href="<?php echo $modul.'/'.$controller.'/issue'; ?>" class="btn btn-primary" style="background-color: white; color: black">Back</a>
								</div>
							<?php } ?>
						<?php } ?>
					</div>
				</form>
			</div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                                
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- MODAL -->
<div class="modal fade animated" id="modal-verify" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title text-center">Are you sure you want to close this issue?</h4>
			</div>
			<div class="modal-body">
				<div class="panel-body">
					<div class="form-group">
						<label>Please input the reason <sup class="mandatory">*</sup></label>
						<textarea class="form-control" name="reason" id="reason"></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="issue_id">
				<button type="button" class="btn btn-info" onclick="submitForm('<?php echo $modul.'/'.$controller.'/issue_form'; ?>')" value="Submit">Yes</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
<!-- EOF MODAL -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>  
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>    
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/esams/issue_form.js"></script>
	<!--  -->
<!-- END SCRIPTS