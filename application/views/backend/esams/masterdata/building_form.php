<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li><a href="<?php echo 'esams/'.$controller.'/building'; ?>">Building</a></li>
	<li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">                                
					<h3 class="panel-title">Form</h3>                              
				</div>
				<form class="form-horizontal" id="px-esams-masterdata-building-form" method="POST" action="<?php echo $modul.'/'.$controller.'/'.($data?$function_edit:$function_add); ?>">
					<input type="hidden" value="<?php echo $data?$data->id:''; ?>" name="id">
					<div class="panel-body">
						<div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
						<div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
						<div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
						<div class="form-group">
							<label class="col-md-2 col-xs-12 control-label" for="#px-esams-masterdata-building-form-businesstype">Instalasi <sup class="mandatory">*</sup></label>
							<div class="col-md-6 col-xs-12">
								<select class="form-control select2ins" name="instalasi_id" id="px-system-usergroup-form-instalasi_id" style="width: 100%;">
									<?php foreach ($instalasi as $key => $value) { ?>
										<option value="<?php echo $value->id; ?>" <?php echo $data?($data->instalasi_id==$value->id?'selected':''):''; ?>><?php echo $value->name; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 col-xs-12 control-label" for="#px-esams-masterdata-building-form-building_name">Building Name <sup class="mandatory">*</sup></label>
							<div class="col-md-6 col-xs-12">
								<input type="text" class="form-control" name="building_name" id="px-esams-masterdata-building-form-building_name" value="<?php echo $data?$data->building_name:''; ?>">
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<button type="submit" class="btn btn-primary pull-right">Submit</button>
					</div>
				</form>
			</div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                                
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>  
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>    
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/esams/masterdata/building_form.js"></script>
	<!--  -->
<!-- END SCRIPTS -->