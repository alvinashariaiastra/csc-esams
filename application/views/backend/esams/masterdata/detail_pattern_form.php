<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li><a href="<?php echo 'esams/'.$controller.'/detail_pattern'; ?>">Detail Pattern</a></li>
	<li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">                                
					<h3 class="panel-title">Form</h3>                              
				</div>
				<form class="form-horizontal" id="px-esams-masterdata-detail_pattern-form" method="POST" action="<?php echo $modul.'/'.$controller.'/'.($data?$function_edit:$function_add); ?>">
					<input type="hidden" value="<?php echo $data?$data->id:''; ?>" name="id">
					<div class="panel-body">
						<div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
						<div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
						<div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
						<div class="form-group">
							<label class="col-md-2 col-xs-12 control-label" for="#px-esams-masterdata-detail_pattern-form-pattern_id">Pattern <sup class="mandatory">*</sup></label>
							<div class="col-md-6 col-xs-12">
								<select <?php echo $data?'disabled':''; ?> class="form-control select2ins" name="pattern_id" id="px-system-usergroup-form-pattern_id" style="width: 100%;">
									<option value="" selected="" disabled=""></option>
									<?php foreach ($pattern as $key => $value) { ?>
										<option value="<?php echo $value->id; ?>" <?php echo $data?($data->id==$value->id?'selected':''):''; ?>><?php echo $value->pattern_name; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Detail Pattern <sup class="mandatory">*</sup></label>
                            <div class="col-md-8 col-xs-12">
        						<small>* Select detail pattern in bottom if you want to add or more</small><br/>
                                <button type="button" class="btn btn-default btn-sm" id="adddetailpattern">Add Detail</button>
                                <div id="divadddetailpattern">
                                	<table class="table" style="margin-top: 10px;">
                                        <tbody id="tbodyadddetailpattern">
                                        	<?php if ($detail_pattern) { ?>
                                        		<?php foreach ($detail_pattern as $i => $r) { ?>
                                        			<tr class="records">
											            <td>
											                <div class="form-group row">
											                    <div class="col-md-8">
											                        <select class="form-control m-input select2ins select-instalasi" name="instalasi[]">
											                            <?php foreach ($instalasi as $v) { ?>
											                            	<option value="<?=$v->id?>" <?=($v->id==$r->instalasi_id?'selected=""':'')?>><?=$v->name?></option>
											                            <?php } ?>
											                        </select>
											                        <sup>Instalasi</sup>
											                    </div>
											                    <div class="col-md-8">
											                        <select class="form-control m-input select2ins select-building" name="building[]">
											                            <?php foreach ($r->building as $v) { ?>
											                            	<option value="<?=$v->id?>" <?=($v->id==$r->building_id?'selected=""':'')?>><?=$v->building_name?></option>
											                            <?php } ?>
											                        </select>
											                        <sup>Building</sup>
											                    </div>
											                    <div class="col-md-8">
											                        <select class="form-control m-input select2ins select-detail-building" name="detail_building[]">
											                            <?php foreach ($r->building_detail as $v) { ?>
											                            	<option value="<?=$v->id?>" <?=($v->id==$r->building_detail_id?'selected=""':'')?>>Floor <?=$v->floor?></option>
											                            <?php } ?>
											                        </select>
											                        <sup>Detail Building</sup>
											                    </div>
											                    <div class="col-md-6">
											                        <select class="form-control m-input select2ins select-checkpoint" name="checkpoint[]">
											                            <?php foreach ($r->checkpoint as $v) { ?>
											                            	<option value="<?=$v->id?>" <?=($v->id==$r->checkpoint_id?'selected=""':'')?>><?=$v->checkpoint_name?></option>
											                            <?php } ?>
											                        </select>
											                        <sup>Checkpoint</sup>
											                    </div>
											                    <div class="col-md-3">
											                        <input type="text" name="sequence[]" class="form-control m-input" autocomplete="off" value="<?=$r->sequence?>">
											                        <sup>Sequence</sup>
											                    </div>
											                    <div class="col-md-3">
											                    	<input type="hidden" name="detail_pattern_id[]" value="<?=$r->id?>">
												                    <input type="hidden" name="detail_pattern[]" value="<?=$i?>">
												                    <a class="btn btn-danger btn-sm deldetailpattern" href="javascript:;"><i class="icon mdi mdi-delete"></i> Delete</a>
											                    </div>
											                </div>
											            </td>
											        </tr>
                                        		<?php } ?>
                                        	<?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div id="divdeletedetailpattern"></div>
                            </div>
                        </div>
					</div>
					<div class="panel-footer">
						<button type="submit" class="btn btn-primary pull-right">Submit</button>
					</div>
				</form>
			</div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                                
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>  
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>    
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/esams/masterdata/detail_pattern_form.js"></script>
	<!--  -->
<!-- END SCRIPTS -->