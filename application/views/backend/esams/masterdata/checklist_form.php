<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li><a href="<?php echo 'esams/'.$controller.'/checklist'; ?>">Checklist</a></li>
	<li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">                                
					<h3 class="panel-title">Form</h3>                              
				</div>
				<form class="form-horizontal" id="px-esams-masterdata-checklist-form" method="POST" action="<?php echo $modul.'/'.$controller.'/'.($data?$function_edit:$function_add); ?>">
					<input type="hidden" value="<?php echo $data?$data->id:''; ?>" name="id">
					<div class="panel-body">
						<div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
						<div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
						<div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
						<div class="form-group">
							<label class="col-md-2 col-xs-12 control-label" for="#px-esams-masterdata-checklist-form-instalasi_id">Instalasi <sup class="mandatory">*</sup></label>
							<div class="col-md-6 col-xs-12">
								<select class="form-control select2ins" name="instalasi_id" id="px-system-usergroup-form-instalasi_id" style="width: 100%;">
									<option value="" selected="" disabled=""></option>
									<?php foreach ($instalasi as $key => $value) { ?>
										<option value="<?php echo $value->id; ?>" <?php echo $data?($data->instalasi_id==$value->id?'selected':''):''; ?>><?php echo $value->name; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 col-xs-12 control-label" for="#px-esams-masterdata-checklist-form-building_id">Building <sup class="mandatory">*</sup></label>
							<div class="col-md-6 col-xs-12">
								<select class="form-control select2ins" name="building_id" id="px-system-usergroup-form-building_id" style="width: 100%;">
									<option value="" selected="" disabled=""></option>
									<?php if ($data) { foreach ($data->building as $key => $value) { ?>
										<option value="<?php echo $value->id; ?>" <?php echo $data?($data->building_id==$value->id?'selected':''):''; ?>><?php echo $value->building_name; ?></option>
									<?php } } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 col-xs-12 control-label" for="#px-esams-masterdata-checklist-form-building_detail_id">Detail Building <sup class="mandatory">*</sup></label>
							<div class="col-md-6 col-xs-12">
								<select class="form-control select2ins" name="building_detail_id" id="px-system-usergroup-form-building_detail_id" style="width: 100%;">
									<option value="" selected="" disabled=""></option>
									<?php if ($data) { foreach ($data->building_detail as $key => $value) { ?>
										<option value="<?php echo $value->id; ?>" <?php echo $data?($data->building_detail_id==$value->id?'selected':''):''; ?>>Floor <?php echo $value->floor; ?></option>
									<?php } } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 col-xs-12 control-label" for="#px-esams-masterdata-checklist-form-checkpoint_id">Checkpoint <sup class="mandatory">*</sup></label>
							<div class="col-md-6 col-xs-12">
								<select class="form-control select2ins" name="checkpoint_id" id="px-system-usergroup-form-checkpoint_id" style="width: 100%;">
									<option value="" selected="" disabled=""></option>
									<?php if ($data) { foreach ($data->checkpoint as $key => $value) { ?>
										<option value="<?php echo $value->id; ?>" <?php echo $data?($data->checkpoint_id==$value->id?'selected':''):''; ?>><?php echo $value->checkpoint_name; ?></option>
									<?php } } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 col-xs-12 control-label" for="#px-esams-masterdata-checklist-form-checklist_name">Checklist Name <sup class="mandatory">*</sup></label>
							<div class="col-md-6 col-xs-12">
								<input type="text" class="form-control" name="checklist_name" id="px-esams-masterdata-checklist-form-checklist_name" value="<?php echo $data?$data->checklist_name:''; ?>">
							</div>
						</div>
						<div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Value Checklist <sup class="mandatory">*</sup></label>
                            <div class="col-md-6 col-xs-12">
        						<small>* Click in bottom if you want to add or more</small><br/>
                                <button type="button" class="btn btn-default btn-sm" id="addchecklistvalue">Add Value</button>
                                <div id="divaddchecklistvalue">
                                	<table class="table" style="margin-top: 10px;">
                                        <tbody id="tbodyaddchecklistvalue">
                                        	<?php if ($data) { foreach ($data->value as $i => $r) { ?>
                                        		<tr class="records">
										            <td>
										                <div class="form-group row">
										                    <div class="col-md-8">
										                        <input type="text" name="value[]" class="form-control m-input" autocomplete="off" value="<?=$r->value?>">
										                        <sup>Value</sup>
										                    </div>
										                    <div class="col-md-4">
										                    	<input type="hidden" name="checklist_detail_id[]" value="<?=$r->id?>">
											                    <input type="hidden" name="checklist_value[]" value="<?=$i?>">
											                    <a class="btn btn-danger btn-sm delchecklistvalue" href="javascript:;"><i class="icon mdi mdi-delete"></i> Delete</a>
										                    </div>
										                </div>
										            </td>
										        </tr>
                                        	<?php } } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div id="divdeletechecklistvalue"></div>
                            </div>
                        </div>
					</div>
					<div class="panel-footer">
						<button type="submit" class="btn btn-primary pull-right">Submit</button>
					</div>
				</form>
			</div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                                
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>  
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>    
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/esams/masterdata/checklist_form.js"></script>
	<!--  -->
<!-- END SCRIPTS -->