<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                  
	<li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">                                
					<h3 class="panel-title">Data</h3>
					<a class="btn btn-success pull-right btn-add" href="<?php echo $modul.'/'.$controller.'/'.$function_form; ?>"><i class="fa fa-plus"></i> Add New</a>                     
				</div>
				<div class="panel-body">
					<?php echo $filter; ?>
					<?php foreach ($datas['data'] as $k => $v) { ?>
			            <div class="card-esams-masterdata">
			            	<div class="row">
				            	<div class="col-md-9">
				            		<div>
				            			<strong>Group Name: <?php echo $v->groupsecurity_name; ?></strong>
				            		</div>
				              		<div>
				              			Members: <?php echo $v->jumlah; ?>
				              		</div>
				            		<div>
				            			<small>
				            				Created: 
				            				<i class="fa fa-calendar"></i> <?php echo $v->created_date; ?>,
				            				<i class="fa fa-user"></i> <?php echo $v->created_by; ?>
				            			</small>
				            		</div>
				            		<div>
				            			<small>
				            				Modified: 
				            				<?php if ($v->modified_date) { ?>
				            					<i class="fa fa-calendar"></i> <?php echo $v->modified_date; ?>, 
				            					<i class="fa fa-user"></i> <?php echo $v->modified_by; ?>
				            				<?php } else echo '-'; ?>
				            			</small>
				            		</div>
				            	</div>
				            	<div class="col-md-3">
				            		<div class="pull-right">
				            			<form action="<?php echo $modul.'/'.$controller.'/'.$function_form; ?>" method="post">
				            				<input type="hidden" name="id" value="<?php echo $v->id; ?>">
					            			<button type="submit" class="btn btn-sm btn-link" data-original-title="Edit"><i class="fa fa-edit"></i></button>
				            			</form>
				            		</div>
				            	</div>
				            </div>
			            </div>
				    <?php } ?>
					<?php echo $paging; ?>
				</div>
			</div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                                
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- MESSAGE BOX -->
<div id="px-esams-masterdata-group-detail-box" class="message-box message-box-warning animated fadeIn fade">
	<div class="mb-container">
		<div class="mb-middle">
			<form action="<?php echo $modul.'/'.$controller.'/'.$function_delete; ?>" method="post" id="px-esams-masterdata-group-detail-form">
			<input type="hidden" name="id">
			<div class="mb-title"><span class="fa fa-warning"></span> Warning</div>
			<div class="mb-content">
				<p>Are you sure you want to delete this data?</p>
				<p class="msg-status"></p>                  
			</div>
			<div class="mb-footer">
				<div class="pull-right">
					<button class="btn btn-danger btn-lg" type="submit">Delete</button>
					<button class="btn btn-default btn-lg mb-control-close" type="button">Cancel</button>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>
<!-- EOF MESSAGE BOX -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>    
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/esams/masterdata/group_detail.js"></script>
	<!--  -->
<!-- END SCRIPTS -->   