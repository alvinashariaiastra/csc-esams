<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li><a href="<?php echo 'esams/'.$controller.'/'.$function; ?>"><?php echo $function_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?> Form</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">                                
					<h3 class="panel-title">Form</h3>                              
				</div>
				<form class="form-horizontal" id="px-esams-masterdata-group-detail-form" method="POST" action="<?php echo $modul.'/'.$controller.'/'.($data?$function_edit:$function_add); ?>">
					<input type="hidden" value="<?php echo $data?$data->id:''; ?>" name="group_security_id">
				<div class="panel-body">
					<div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
					<div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
					<div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-esams-masterdata-group-detail-form-groupname">Group Name<sup class="mandatory">*</sup></label>
						<div class="col-md-6 col-xs-12">
							<select disabled class="form-control select2ins" name="group_id" id="px-system-group-detail-form-group_id" style="width: 100%;">
								<?php foreach ($group as $key => $value) { ?>
									<option value="<?php echo $value->id; ?>" <?php echo $data?($data->id==$value->id?'selected':''):''; ?>><?php echo $value->groupsecurity_name; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-esams-masterdata-group-detail-form-member">Members<sup class="mandatory">*</sup></label>
						<div class="col-md-6 col-xs-12">
							<select class="js-example-basic-multiple" name="member[]" multiple="multiple" id="px-system-group-detail-form-user-id" style="width: 100%;">
								<?php foreach ($member as $key => $value) { ?>
									<option value="<?php echo $value->id; ?>" <?php echo $value->selected==1?'selected':''; ?>><?php echo $value->realname; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<button class="btn btn-primary pull-right" type="submit">Save</button>
				</div>
				</form>
			</div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                                
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>  
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>    
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/esams/masterdata/group_detail_form.js"></script>
	<!--  -->
<!-- END SCRIPTS -->   
