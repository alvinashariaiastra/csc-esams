<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li>E-Sams</li>                    
	<li><a href="<?php echo $modul.'/'.$controller; ?>"><?php echo $controller_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">
			<!-- Tab links -->
			<div class="tab">
				<button class="tablinks <?=($tab=='assignment')?'active':''?>" onclick="openTab('<?php echo base_url().$modul."/".$controller."/assignment"; ?>')">Issue</button>
				<button class="tablinks <?=($tab=='inprogress')?'active':''?>" onclick="openTab('<?php echo base_url().$modul."/".$controller."/inprogress"; ?>')">Issue In Progress</button>
				<button class="tablinks <?=($tab=='completed')?'active':''?>" onclick="openTab('<?php echo base_url().$modul."/".$controller."/completed"; ?>')">Issue completed</button>
			</div>
			<br/>

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">                                
					<h3 class="panel-title">Data</h3>                              
				</div>
				<div class="panel-body">
					<?php echo $filter; ?>
					<?php if (!$datas['data']) {?>
						<h4><center>No Data</center></h4>
					<?php } else{?>
						<?php foreach ($datas['data'] as $k => $v) { ?>
				            <div class="card-esams-masterdata">
				            	<div class="row">
					            	<div class="col-md-10">
					            		<div>
					            			<STRONG>Ticket Number: <?php echo $v->ticket_number; ?></STRONG><br/>
					            			<label>Location: <?php echo $v->instalasi_name; ?></label><br/>
					            			<label>Building: <?php echo $v->building_name; ?></label><br/>
					            			<label>Floor: <?php echo $v->floor; ?></label><br/>
					            			<label>Area: <?php echo $v->area; ?></label><br/>
					            		</div>
					            		<div>
					            			<small>
					            				Created: 
					            				<i class="fa fa-calendar"></i> <?php echo $v->created_date; ?>,
					            				<i class="fa fa-user"></i> <?php echo $v->created_by; ?>
					            			</small>
					            		</div>
					            	</div>
					            	<div class="col-md-2">
					            		<div class="pull-right">
					            			<form action="<?php echo $modul.'/'.$controller.'/'.($tab=='completed'?'issue_report':$function_form).'/'.$v->id; ?>" method="post">
					            				<input type="hidden" name="id" value="<?php echo $v->id; ?>">
					            				<input type="hidden" name="tab" value="<?php echo $tab; ?>">
						            			<button type="submit" class="btn btn-sm btn-link"><i class="fa fa-eye"></i></button>
					            			</form>
					            		</div>
					            	</div>
					            </div>
				            </div>
					    <?php } ?>
					<?php } ?>
					<?php echo $paging; ?>
				</div>
			</div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                                
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- MESSAGE BOX -->
<div id="px-esams-issue-box" class="message-box message-box-warning animated fadeIn fade">
	<div class="mb-container">
		<div class="mb-middle">
			<form action="<?php echo $modul.'/'.$controller.'/'.$function_delete; ?>" method="post" id="px-esams-issue-form">
			<input type="hidden" name="id">
			<div class="mb-title"><span class="fa fa-warning"></span> Warning</div>
			<div class="mb-content">
				<p>Are you sure you want to delete this data?</p>
				<p class="msg-status"></p>                  
			</div>
			<div class="mb-footer">
				<div class="pull-right">
					<button class="btn btn-danger btn-lg" type="submit">Delete</button>
					<button class="btn btn-default btn-lg mb-control-close" type="button">Cancel</button>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>
<!-- EOF MESSAGE BOX -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>    
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/esams/issue.js"></script>
	<!--  -->
<!-- END SCRIPTS -->   