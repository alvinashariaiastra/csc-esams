<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">

            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Data Pendaftar Training <?php echo $data_training->title; ?></h3>
                    <ul class="panel-controls">
                    </ul>
                </div>
                <div class="panel-body">
                    <table class="table datatable table-bordered">
                        <thead>
                            <tr>
                                <th width="6%" class="text-center">No</th>
                                <th class="text-center">Nama Peserta</th>
                                <th class="text-center">Perusahaan</th>
                                <th class="text-center">Tanggal Daftar</th>
                                <th width="15%" class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; foreach($list_pendaftar as $data_row) { ?>
                            <tr>
                                <td width="6%" class="text-center">No</th>
                                <td class="text-center"><?php echo $data_row->name ?></td>
                                <td class="text-center"><?php echo $data_row->company ?></td>
                                <td class="text-center"><?php echo $data_row->date_created ?></td>
                                <td width="15%" class="text-center">
                                    <a href="<?php echo $controller ?>/proses_pendaftar/<?php echo $data_row->id ?>" class="btn <?php if($data_row->status == 0) echo 'btn-primary'; else if($data_row->status == 1) echo 'btn-success'; else echo 'btn-danger'; ?>">
                                        <?php if($data_row->status == 0) echo 'Proses Pendaftar'; else if($data_row->status == 1) echo 'Approved'; else echo 'Rejected'; ?>
                                    </a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="panel-heading">                                  
                    <span class="text-danger">*Data Pendaftar Training yang sudah di Approve/Reject tidak dapat berubah</span>
                </div>
            </div>
            <!-- END DEFAULT DATATABLE -->
        </div>
    </div>                                

</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->               
<!-- THIS PAGE PLUGINS -->
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/moment.min.js"></script> 
<!-- END PAGE PLUGINS -->
<!-- START TEMPLATE -->
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>

<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
<!-- END TEMPLATE -->
<!-- END SCRIPTS -->   