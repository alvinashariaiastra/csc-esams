<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<?php
					if(isset($_GET['status'])){
						if($_GET['status'] == 'error')
							echo '<div class="alert alert-danger"><strong>Pendaftaran Training Gagal!!!!</strong><span></span></div>';
						else
							echo '<div class="alert alert-success"><strong>Pendaftaran Training Berhasil!!!!</strong><span></span></div>';
					}
				?>
				<div class="panel-heading">                                
					<h3 class="panel-title">Data</h3>
					<ul class="panel-controls">
					    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    	<li><a href="<?php echo $controller.'/'.$function_form; ?>" class="bg-success"><i class="fa fa-plus"></i></a>                           </li>
                    </ul>
				</div>
				<div class="panel-body">
					<table class="table datatable table-bordered">
						<thead>
							<tr>
								<th width="6%" class="text-center">No</th>
								<th class="text-center">Judul</th>
								<th class="text-center">PIC</th>
								<th class="text-center">Tanggal</th>
								<th class="text-center">Tanggal Pendaftaran</th>
								<th width="15%" class="text-center">Action</th>
							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
			</div>
			<!-- END DEFAULT DATATABLE -->
			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">                                
					<h3 class="panel-title">Calendar View</h3>

					<ul class="panel-controls pull-right">
                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    </ul>
				</div>
				<div class="panel-body">
					<div class="row">
                        <div class="col-md-12">
                            <div id="alert_holder"></div>
                            <div class="calendar">                                
                                <div id="calendar-info-training"></div>                            
                            </div>
                        </div>
                    </div>
				</div>
			</div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                                
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- DETAIL TRAINING BOX -->
<div id="detail-training-modal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <a class="btn-link"><button type="button" class="btn btn-primary">Daftar</button></a>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- EOF DETAIL TRAINING BOX -->

<!-- MESSAGE BOX -->
<div id="px-info-training-message-box" class="message-box message-box-warning animated fadeIn fade">
	<div class="mb-container">
		<div class="mb-middle">
			<form action="<?php echo $controller.'/'.$function_delete; ?>" method="post" id="px-info-training-message-form">
			<input type="hidden" name="id">
			<div class="mb-title"><span class="fa fa-warning"></span> Warning</div>
			<div class="mb-content">
				<p>Are you sure you want to delete this data?</p>
				<p class="msg-status"></p>                  
			</div>
			<div class="mb-footer">
				<button class="btn btn-danger btn-lg pull-right" type="submit">Delete</button>
				<button class="btn btn-default btn-lg pull-right mb-control-close" type="button">Cancel</button>
			</div>
			</form>
		</div>
	</div>
</div>
<!-- EOF MESSAGE BOX -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/moment.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/fullcalendar/fullcalendar.min.js"></script>   
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/info_training/info.js"></script>
	<!--  -->
<!-- END SCRIPTS -->   