<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
	<li><a href="<?php echo $controller.'/'.$function; ?>"><?php echo $function_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">                                
					<h3 class="panel-title">Detail Pendaftaran Training</h3>                              
				</div>
				<input type="hidden" value="<?php if($pendaftaran!=null) echo $pendaftaran->id; ?>" name="id">
				<div class="panel-body">
					<div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
					<div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
					<div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
					<!-- NEW LAYOUT -->
					<div class="row">
						<div class="col-md-12"><h3>Data Pendaftaran</h3></div>
						<div class="col-md-8">
							<table class="table">
								<tr>
									<td>Judul training</td>
									<td><?php echo $info_training->title ?></td>
								</tr>
								<tr>
									<td>Tanggal Start Training</td>
									<td><?php echo $info_training->date_start ?></td>
								</tr>
								<tr>
									<td>Tanggal Stop Training</td>
									<td><?php echo $info_training->date_end ?></td>
								</tr>
								<tr>
									<td>Tanggal Pendaftaran</td>
									<td><?php echo $pendaftaran->date_created ?></td>
								</tr>
								<tr>
									<td>Peserta Training</td>
									<td><?php echo $pendaftaran->content ?></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<a href="admin_info_training/pendaftaran_training"><button class="btn btn-primary pull-right">Back</button></a>
				</div>
			</div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                                
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>  
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>    
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<!--  -->
<!-- END SCRIPTS -->   