<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li><a href="<?php echo $controller . '/' . $function; ?>"><?php echo $function_name; ?></a></li>
    <li class="active"><?php echo $function_name; ?> Form</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">

            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Data Training</h3>                              
                </div>
                <form class="form-horizontal" id="px-pendaftaran-training-form" method="POST">
                    <div class="panel-body">
                        <?php echo $this->session->flashdata('success'); ?>
                        <div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
<?php echo $this->session->flashdata('failed'); ?>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-pendaftaran-training-form-pic">PIC</label>
                            <div class="col-md-6 col-xs-12">
                                <span class="form-control" name="pic" id="px-pendaftaran-training-form-pic"><?php echo $data_pendaftar->data_training->pic; ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-pendaftaran-training-form-title">Judul</label>
                            <div class="col-md-6 col-xs-12">
                                <span class="form-control" name="title" id="px-pendaftaran-training-form-title"><?php echo $data_pendaftar->data_training->title; ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Pendaftaran Dibuka</label>
                            <div class="col-md-5">
                                <div class="input-group" >
                                    <span class="form-control" name="pendaftaran_start"><?php echo $data_pendaftar->data_training->pendaftaran_start; ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Pendaftaran Ditutup</label>
                            <div class="col-md-5">
                                <div class="input-group" >
                                    <span class="form-control" name="pendaftaran_end"><?php echo $data_pendaftar->data_training->pendaftaran_end; ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Training Dimulai</label>
                            <div class="col-md-5">
                                <div class="input-group" >
                                    <span class="form-control" name="date_start"><?php echo $data_pendaftar->data_training->date_start; ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Training Selesai</label>
                            <div class="col-md-5">
                                <div class="input-group" >
                                    <span class="form-control" name="date_end"><?php echo $data_pendaftar->data_training->date_end; ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-heading">                                
                        <h3 class="panel-title">Data Peserta</h3>                              
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-pendaftaran-training-form-name">Nama Peserta</label>
                            <div class="col-md-6 col-xs-12">
                                <input type="text" class="form-control" name="name" id="px-pendaftaran-training-form-name" required value="<?php echo $data_pendaftar->name; ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-pendaftaran-training-form-company">Perusahaan</label>
                            <div class="col-md-6 col-xs-12">
                                <input type="text" class="form-control" name="company" id="px-pendaftaran-training-form-company" required value="<?php echo $data_pendaftar->company; ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-pendaftaran-training-form-position">Jabatan</label>
                            <div class="col-md-6 col-xs-12">
                                <input type="text" class="form-control" name="position" id="px-pendaftaran-training-form-position" required value="<?php echo $data_pendaftar->position; ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-pendaftaran-training-form-phone">No. Handphone</label>
                            <div class="col-md-6 col-xs-12">
                                <input type="text" class="form-control" name="phone" id="px-pendaftaran-training-form-phone" required value="<?php echo $data_pendaftar->phone; ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-pendaftaran-training-form-email">Email</label>
                            <div class="col-md-6 col-xs-12">
                                <input type="text" class="form-control" name="email" id="px-pendaftaran-training-form-email" required value="<?php echo $data_pendaftar->email; ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-pendaftaran-training-form-supervisor_name">Nama Atasan</label>
                            <div class="col-md-6 col-xs-12">
                                <input type="text" class="form-control" name="supervisor_name" id="px-pendaftaran-training-form-supervisor_name" required value="<?php echo $data_pendaftar->supervisor_name; ?>"/>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <?php if($data_pendaftar->status == 0) { ?>
                        <span class="btn btn-danger pull-left btn-processing" data-target-id="<?php echo $data_pendaftar->id; ?>" data-process="2">Reject</span>
                        <span class="btn btn-primary pull-right btn-processing" data-target-id="<?php echo $data_pendaftar->id; ?>" data-process="1">Approve</span>
                        <?php } ?>
                    </div>
                </form>
            </div>
            <!-- END DEFAULT DATATABLE -->

        </div>
    </div>                                

</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- MESSAGE BOX -->
<div id="px-info-training-message-box" class="message-box message-box-warning animated fadeIn fade">
	<div class="mb-container">
		<div class="mb-middle">
			<form action="<?php echo $controller.'/do_process_pendaftar'; ?>" method="post" id="px-info-training-message-form">
			<input type="hidden" name="id">
                        <input type="hidden" name="name">
                        <input type="hidden" name="company">
                        <input type="hidden" name="phone">
                        <input type="hidden" name="email">
                        <input type="hidden" name="position">
                        <input type="hidden" name="supervisor_name">
                        <input type="hidden" name="status">
			<div class="mb-title"><span class="fa fa-warning"></span> Warning</div>
			<div class="mb-content">
				<p id="warning-message"></p>
				<p class="msg-status"></p>                  
			</div>
			<div class="mb-footer">
				<button class="btn btn-lg pull-right" type="submit" id="button-message"></button>
				<button class="btn btn-default btn-lg pull-right mb-control-close" type="button">Cancel</button>
			</div>
			</form>
		</div>
	</div>
</div>
<!-- EOF MESSAGE BOX -->

<!-- START SCRIPTS -->               
<!-- THIS PAGE PLUGINS -->
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>  
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>    
<!-- END PAGE PLUGINS -->
<!-- START TEMPLATE -->
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>

<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
<!-- END TEMPLATE -->  
<!-- THIS PAGE JS SETTINGS -->
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/info_training/list_pendaftar_proses.js"></script>
<!--  -->
<!-- END SCRIPTS -->   