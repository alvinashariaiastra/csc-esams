<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
	<li><a href="<?php echo $controller.'/'.$function; ?>"><?php echo $function_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?> Form</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">                                
					<h3 class="panel-title">Form</h3>                              
				</div>
				<form class="form-horizontal" id="px-status-kerawanan-form" method="POST" action="<?php if($data) echo $controller.'/'.$function_edit; else echo $controller.'/'.$function_add; ?>">
				<input type="hidden" value="<?php if($data!=null) echo $data->id; ?>" name="id">
				<div class="panel-body">
					<div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
					<div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
					<div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-status-kerawanan-peta-form-peta-perusahaan">Perusahaan</label>
						<div class="col-md-6 col-xs-12">
							<select class="form-control" name="perusahaan_id" id="px-status-kerawanan-peta-form-peta-perusahaan">
								<option value="0">Pilih Perusahaan</option>
								<?php 
								if ($company) {
									foreach ($company as $data_row) {
										if ($data!=null && $data->perusahaan_id == $data_row->id) {
												$selected = "selected";
											}else{
												$selected = "";
											}
										?>
										<option value="<?php echo $data_row->id ?>"<?php echo $selected ?>><?php echo $data_row->name ?></option>
								<?php
									}
								}
								 ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-status-kerawanan-peta-form-peta-instalasi">Instalasi</label>
						<div class="col-md-6 col-xs-12">
							<select class="form-control" name="instalasi_id" id="px-status-kerawanan-peta-form-peta-instalasi">
								<option value="0">Pilih Instalasi</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-status-kerawanan-form-peta-jenisbisnis">Jenis Bisnis Instalasi</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="coordinate_x" id="px-status-kerawanan-form-peta-jenisbisnis-nm" value="<?php if($data!=null) echo $data->jenis_bisnis_nm; ?>" disabled>
							<input type="hidden" class="form-control" name="jenis_bisnis_id" id="px-status-kerawanan-form-peta-jenisbisnis-id" value="">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-status-kerawanan-form-peta-instalasi-alamat">Alamat Instalasi</label>
						<div class="col-md-6 col-xs-12">
							<textarea class="form-control" id="px-status-kerawanan-form-peta-instalasi-alamat" disabled></textarea>
						</div>
					</div>
					<!-- <div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-status-kerawanan-form-peta-koordinatx">Koordinat X (Lat)</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="coordinate_x" id="px-status-kerawanan-form-peta-koordinatx" value="<?php if($data!=null) echo $data->coordinate_y; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-status-kerawanan-form-peta-koordinaty">Koordinat Y (Long)</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="coordinate_y" id="px-status-kerawanan-form-peta-koordinaty" value="<?php if($data!=null) echo $data->coordinate_y; ?>">
						</div>
					</div> -->
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-status-kerawanan-form-peta-title">Judul</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="title" id="px-status-kerawanan-form-peta-title" value="<?php if($data!=null) echo $data->title; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-status-kerawanan-form-peta-content">Content</label>
						<div class="col-md-10 col-xs-12">
							<textarea class="form-control ignore px-summernote" name="content" id="px-status-kerawanan-form-peta-content"><?php if($data!=null) echo $data->content; ?></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-status-kerawanan-form-peta-visibility">Visibility</label>
						<div class="col-md-9 col-xs-12">
							<div class="radio">
                                <label>
                                    <input type="radio" value="1" name="visibility" class="px-status-kerawanan-form-peta-visibility" <?php if($data){ if($data->visibility == 1) echo 'checked'; } else echo 'checked'; ?>>
                                    Visible
                                </label>
                                <label>
                                    <input type="radio" value="0" name="visibility" class="px-status-kerawanan-form-peta-visibility" <?php if($data){ if($data->visibility == 0) echo 'checked'; } ?>>
                                    Hidden
                                </label>
                            </div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-status-kerawanan-form-peta-instalasi-status">Status Kerawanan</label>
						<div class="col-md-6 col-xs-12">
							<label class="radio-inline">
							  <input type="radio" name="status" id="status_rad1" value="3"> Merah
							</label>
							<label class="radio-inline">
							  <input type="radio" name="status" id="status_rad2" value="2"> Kuning
							</label>
							<label class="radio-inline">
							  <input type="radio" name="status" id="status_rad3" value="1" checked> Hijau
							</label>
						</div>
					</div>
				</div>
				<?php if(isset($data->image)){
					foreach ($data->image as $images) {
				?>
				<input type="hidden" name="images[]" value="<?php echo $images; ?>">
				<?php
					}
				} ?>
				<div class="panel-footer">
					<button class="btn btn-primary pull-right" type="submit">Save</button>
				</div>
				</form>
			</div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                                
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>  
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>    
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/status_kerawanan/peta_form.js"></script>
	<!--  -->
<!-- END SCRIPTS -->   