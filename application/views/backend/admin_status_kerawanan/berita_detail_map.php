<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
	<li><a href="<?php echo $controller.'/'.$function; ?>"><?php echo $function_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?> Form</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">                                
					<h3 class="panel-title">Form</h3>                              
				</div>
				<?php 
					if ($data->status == 1) {
            		$status_lable = '<label class="label-form label label-warning">Kuning</label>';
            	}elseif ($data->status == 2) {
            		$status_lable = '<label class="label-form label label-success">Hijau</label>';
            	}elseif ($data->status == 3) {
            		$status_lable = '<label class="label-form label label-danger">Merah</label>';
            	}
				 ?>
				<form class="form-horizontal" id="px-status-kerawanan-form" method="POST" action="<?php if($data) echo $controller.'/'.$function_edit; else echo $controller.'/'.$function_add; ?>">
				<input type="hidden" value="<?php if($data!=null) echo $data->id; ?>" name="id">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<h2><?php echo $data->title ?></h2>
							<h4><?php echo $data->date_created ?></h4>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<h2><?php echo $status_lable ?></h2>
						</div>
					</div>
					<div class="row">
						<div class="col-m-12">
							<?php echo $data->content ?>
						</div>
					</div>
				</div>
				<?php if(isset($data->image)){
					foreach ($data->image as $images) {
				?>
				<input type="hidden" name="images[]" value="<?php echo $images; ?>">
				<?php
					}
				} ?>
				<div class="btn btn-primary pull-right">
					<a href="admin_status_kerawanan" class="btn btn-primary pull-right">Back</a>
				</div>
				</form>
			</div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                                
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>  
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>    
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/status_kerawanan/berita_detail.js"></script>
	<!--  -->
<!-- END SCRIPTS -->   