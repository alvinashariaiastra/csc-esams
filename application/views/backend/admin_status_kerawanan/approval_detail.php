<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
	<li><a href="<?php echo $controller.'/'.$function; ?>"><?php echo $function_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?> Form</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">                                
					<!-- <h3 class="panel-title">Approval Status Kerawanan</h3> -->                            
				</div>
				<div class="panel-body">
				<div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
				<div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
				<div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
				<div class="row">
					<div class="col-md-4">
						<table class="table">
							<tr>
								<td>Perusahaan</td>
								<td>:</td>
								<td><?php echo $perusahaan ?></td>
							</tr>
							<tr>
								<td>Instalasi</td>
								<td>:</td>
								<td><?php echo $instalasi->name ?></td>
							</tr>
							<tr>
								<td>Alamat</td>
								<td>:</td>
								<td><?php echo $instalasi->alamat ?></td>
							</tr>
							<tr>
								<td>Telepon</td>
								<td>:</td>
								<td><?php echo $instalasi->telp ?></td>
							</tr>
							<tr>
								<td>Status</td>
								<td>:</td>
								<td><?php echo $data->status ?></td>
							</tr>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-12"><h1><?php echo $data->title ?><h1></div>
							<div class="col-md12"><?php echo $data->content ?></div>
						</div>
					</div>
				</div>
			    </div>
			    <div class="panel-footer">
			    	
			    	<?php if ($data->is_approved != 1) { ?>
			    	<form id="form-approval" class="form" action="admin_status_kerawanan/do_approve" method="POST">
			    		<input type="hidden" name="id" value="<?php echo $data->id ?>">
			    		<button type="submit" class="btn btn-info pull-right">Approve</button>
			    	</form>
			    	<?php } ?>
			    	<a href="admin_status_kerawanan/approval" class="btn btn-primary pull-right" style="margin-right:10px;">Back</a>
			    	
			    </div>

			</div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                                
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>  
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>    
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/status_kerawanan/approval_detail.js"></script>
	<!--  -->
<!-- END SCRIPTS -->   