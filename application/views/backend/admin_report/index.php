<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">

            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <?php echo $this->session->flashdata('success'); ?>
                <?php echo $this->session->flashdata('failed'); ?>
                <div class="panel-heading">                                
                    <h3 class="panel-title">Data</h3>

                </div>
                <div class="panel-body">
                    <form id="pixel-admin_report-report-form" class="form-horizontal" action="<?php echo $controller . '/search_report'; ?>" method="POST">
                    <div class="assesment-only" style="display:none;margin-bottom:20px;">
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-12 col-md-3 col-lg-2" for="admin_report-report-periode_start">Periode Start</label>
                            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
                                <select id="select2-dashboard-year-start" class="form-control" name="periode_start" style="width:200px;">
                                    <option value="0">Semua</option>
                                    <?php
                                        for ($i=2010; $i <= date("Y"); $i++) {
                                        if ($i == date("Y")) {
                                            $selected = "selected";
                                         } 
                                        echo "<option value='".$i."' ".$selected.">".$i."</option>";
                                    } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-12 col-md-3 col-lg-2" for="admin_report-report-periode_end">Periode End</label>
                            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
                                <select id="select2-dashboard-year-end" class="form-control" name="periode_end" style="width:200px;">
                                    <option value="0">Semua</option>
                                    <?php
                                        for ($i=2010; $i <= date("Y"); $i++) {
                                        if ($i == date("Y")) {
                                            $selected = "selected";
                                         } 
                                        echo "<option value='".$i."' ".$selected.">".$i."</option>";
                                    } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="non-assesment" style="display:none;margin-bottom:20px;">
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-12 col-md-3 col-lg-2" for="admin_report-report-date_start">Date Start</label>
                            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
                                <input class="datepicker form-control" type="text" name="date_start" id="datepicker">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-12 col-md-3 col-lg-2" for="admin_report-report-date_end">Date End</label>
                            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
                                <input class="datepicker form-control" type="text" name="date_end" id="datepicker2">
                            </div>
                        </div>
                    </div>
                        <div class="form-group">
                            <label class="control-label col-xs-12 col-sm-12 col-md-3 col-lg-2" for="admin_report-report-documents">Pilih Dokumen</label>
                            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-10">
                                <select id="select-docs" class="form-control" name="documents">
                                    <option value="0">Pilih Dokumen</option>
                                    <option value="1">Asesmen 2004</option>
                                    <option value="2">Asesmen 2015</option>
                                    <option value="3">Standar Kompetensi</option>
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-add pull-right" id="pixel-admin_report-report-add-button">Download</button>
                    </form>
                </div>
            </div>
            <!-- END DEFAULT DATATABLE -->
        </div>
    </div>                                
    
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- MESSAGE BOX -->
<div id="px-info-training-message-box" class="message-box message-box-warning animated fadeIn fade">
    <div class="mb-container">
        <div class="mb-middle">
            <form action="<?php echo $controller.'/'.$function_delete; ?>" method="post" id="px-info-training-message-form">
            <input type="hidden" name="id">
            <div class="mb-title"><span class="fa fa-warning"></span> Warning</div>
            <div class="mb-content">
                <p>Are you sure you want to delete this data?</p>
                <p class="msg-status"></p>                  
            </div>
            <div class="mb-footer">
                <button class="btn btn-danger btn-lg pull-right" type="submit">Delete</button>
                <button class="btn btn-default btn-lg pull-right mb-control-close" type="button">Cancel</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- EOF MESSAGE BOX -->

<!-- START SCRIPTS -->               
    <!-- THIS PAGE PLUGINS -->
    <script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/moment.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/fullcalendar/fullcalendar.min.js"></script>   
    <!-- END PAGE PLUGINS -->
    <!-- START TEMPLATE -->
    <script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
    
    <script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
    <script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
    <!-- END TEMPLATE -->  
    <!-- THIS PAGE JS SETTINGS -->
    <script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/report/report.js"></script>
    <!--  -->
<!-- END SCRIPTS -->   