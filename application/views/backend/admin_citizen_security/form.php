<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">
    <h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading"> 
					<h3 class="panel-title">Form</h3>
				</div>
				<form class="form-horizontal" id="px-citizen-security-report-form" method="POST" action="<?php echo $controller.'/cs_report_add'; ?>">
					<div class="panel-body">
						<div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
						<div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
						<div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
						<input type="hidden" id="px-citizen-security-report-form-reporter_id" name="reporter_id" value="<?php echo $this->session_admin['admin_id']; ?>">
						<div class="form-group">
							<label class="col-md-1 col-xs-12 control-label" for="#px-citizen-security-report-form-username">Username</label>
							<div class="col-md-9 col-xs-12">
								<input type="text" class="form-control" name="username" id="px-citizen-security-report-form-username" value="<?php echo $this->session_admin['username']; ?>" required disabled>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-1 col-xs-12 control-label" for="#px-citizen-security-report-form-email">Email</label>
							<div class="col-md-9 col-xs-12">
								<input type="text" class="form-control" name="email" id="px-citizen-security-report-form-email" value="<?php echo $this->session_admin['email']; ?>" required disabled>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-1 col-xs-12 control-label" for="#px-citizen-security-report-form-datetime">Date, Time</label>
							<div class="col-md-9 col-xs-12">
								<input type="text" class="form-control form_datetime" name="datetime" id="px-citizen-security-report-form-datetime" placeholder="Masukkan informasi tanggal dan waktu kejadian" required>
							</div>
						</div>
						<div class="form-group">
							<input type="hidden" id="px-citizen-security-report-form-place_id" name="place_id" value="0">
							<label class="col-md-1 col-xs-12 control-label" for="#px-citizen-security-report-form-place">Place</label>
							<div class="col-md-9 col-xs-12">
								<select class="form-control" name="place" id="px-citizen-security-report-form-place" onchange="setPlaceIdValue();">
									<option value="0" selected>-- Pilih lokasi kejadian --</option>
									<?php 
									if ($perusahaan) {
										foreach ($perusahaan->result() as $data_row) {
											?>
											<option value="<?php echo $data_row->id ?>"><?php echo $data_row->name ?></option>
									<?php
										}
									}
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-1 col-xs-12 control-label" for="#px-citizen-security-report-form-notes">Notes</label>
							<div class="col-md-9 col-xs-12">
								<textarea class="form-control" name="notes" id="px-citizen-security-report-form-notes" placeholder="Masukkan informasi catatan tambahan kejadian" required></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-1 col-xs-12 control-label">Pictures</label>
							<div class="col-md-3 col-xs-12" style="text-align: center;">
								<div class="row">
									<div class="row">
										<h5>Picture 1</h5>
									</div>
									<div class="row" id="px-citizen-security-report-form-picture1-row">
										<!-- <img src="#" alt="picture1" id="px-citizen-security-report-form-picture1-img" height="200px" width="250px" style="border-style: solid; border-color: black; border-width: 1px;"> -->
									</div>
								</div>
								<div class="row" style="padding: 10px;">
									<!-- <input type="file" onchange="" /> -->
									<input type="hidden" id="px-citizen-security-report-form-picture1-img-path" name="picture1">
									<label for="file-upload-file1" class="btn btn-primary btn-upload1" data-target="picture1" id="px-citizen-security-report-form-picture1-button">Browse</label>
								</div>
							</div>
							<div class="col-md-3 col-xs-12" style="text-align: center;">
								<div class="row">
									<div class="row">
										<h5>Picture 2</h5>
									</div>
									<div class="row" id="px-citizen-security-report-form-picture2-row">
										<!-- <img src="#" alt="picture2" id="px-citizen-security-report-form-picture2-img" height="200px" width="250px" style="border-style: solid; border-color: black; border-width: 1px;"> -->
									</div>
								</div>
								<div class="row" style="padding: 10px;">
									<!-- <input type="file" onchange="" /> -->
									<input type="hidden" id="px-citizen-security-report-form-picture2-img-path" name="picture2">
									<label for="file-upload-file2" class="btn btn-primary btn-upload2" data-target="picture2" id="px-citizen-security-report-form-picture2-button">Browse</label>
								</div>
							</div>
							<div class="col-md-3 col-xs-12" style="text-align: center;">
								<div class="row">
									<div class="row">
										<h5>Picture 3</h5>
									</div>
									<div class="row" id="px-citizen-security-report-form-picture3-row">
										<!-- <img src="#" alt="picture3" id="px-citizen-security-report-form-picture3-img" height="200px" width="250px" style="border-style: solid; border-color: black; border-width: 1px;"> -->
									</div>
								</div>
								<div class="row" style="padding: 10px;">
									<!-- <input type="file" onchange="" /> -->
									<input type="hidden" id="px-citizen-security-report-form-picture3-img-path" name="picture3">
									<label for="file-upload-file3" class="btn btn-primary btn-upload3" data-target="picture3" id="px-citizen-security-report-form-picture3-button">Browse</label>
								</div>
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<button class="btn btn-primary pull-right" type="submit">Save</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- UPLOADED PICTURE1 MODAL -->
<div class="modal fade" id="picture1modal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Picture 1</h4>
			</div>
			<div class="modal-body" id="picture1modalbody">
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- UPLOADED PICTURE2 MODAL -->
<div class="modal fade" id="picture2modal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Picture 2</h4>
			</div>
			<div class="modal-body" id="picture2modalbody">
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- UPLOADED PICTURE3 MODAL -->
<div class="modal fade" id="picture3modal" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Picture 3</h4>
			</div>
			<div class="modal-body" id="picture3modalbody">
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<!-- FORM UPLOAD -->
<form id="file-upload1" action="upload/cs_report_image1" method="POST" enctype="multipart/form-data" class="hidden">
	<input type="hidden" name="target1" id="target-file1">
	<input type="file" name="image1" id="file-upload-file1">
</form>
<form id="file-upload2" action="upload/cs_report_image2" method="POST" enctype="multipart/form-data" class="hidden">
	<input type="hidden" name="target2" id="target-file2">
	<input type="file" name="image2" id="file-upload-file2">
</form>
<form id="file-upload3" action="upload/cs_report_image3" method="POST" enctype="multipart/form-data" class="hidden">
	<input type="hidden" name="target3" id="target-file3">
	<input type="file" name="image3" id="file-upload-file3">
</form>
<!-- EOF FORM UPLOAD -->

<!-- START SCRIPTS -->  
<!-- THIS PAGE PLUGINS -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/backend_assets/css/bootstrap/bootstrap.min.css"></link>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/backend_assets/css/bootstrap/bootstrap-datetimepicker.min.css"></link>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/bootstrap/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/bootstrap/bootstrap-file-input.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/blueimp/jquery.blueimp-gallery.min.js"></script>    
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/fileupload/fileupload.min.js"></script>
<!-- END PAGE PLUGINS -->
<!-- START TEMPLATE -->
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
<!-- END TEMPLATE -->  
<!-- THIS PAGE JS SETTINGS -->
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/citizen_security/cs_report_form.js"></script>
<!--  -->
<!-- END SCRIPTS --> 