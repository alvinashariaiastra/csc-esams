<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active"><?php echo $function_name; ?> List</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">
    <h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
	<div class="row">
		<div class="col-md-12">
			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="row">
						<a class="btn btn-default pull-right btn-add" href="<?php echo 'admin_excel' . '/export_citizen_security'; ?>"><i class="fa fa-download"></i> Download Excel</a>
					</div>
				</div>
				<div class="panel-body">
					<table>
						<tr>
							<td colspan="2" style="padding: 5px;">
								<h4>Filter by submit date</h4>
							</td>
						</tr>
						<tr>
							<td style="padding: 5px;">
								<span>Start Datetime</span>
							</td>
							<td style="padding: 5px; width: 200px;">
								<input type="text" class="form_datetime" name="startdatetime" id="sc-datetime-filter-start" style="width: 100%;" placeholder="Masukkan filter tanggal awal">
							</td>
						</tr>
						<tr>
							<td style="padding: 5px;">
								<span>End Datetime</span>
							</td>
							<td style="padding: 5px; width: 200px;">
								<input type="text" class="form_datetime" name="enddatetime" id="sc-datetime-filter-end" style="width: 100%;" placeholder="Masukkan filter tanggal akhir">
							</td>
						</tr>
						<tr>
							<td colspan="2" style="padding: 5px;">
								<button id="sc-datetime-filter-apply" class="btn btn-primary">Apply</button>
							</td>
						</tr>
					</table>
					<br />
					<table class="table datatable table-bordered" id="sc-table">
						<thead>
							<tr>
								<th class="text-center">No</th>
								<th class="text-center">Username</th>
								<th class="text-center">Email</th>
								<th class="text-center">Place</th>
								<th class="text-center">Notes</th>
								<th class="text-center">Pictures</th>
								<th class="text-center">Datetime</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			<!-- END DEFAULT DATATABLE -->
		</div>
	</div>
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->
<!-- THIS PAGE PLUGINS -->
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/backend_assets/css/bootstrap/bootstrap.min.css"></link>
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/backend_assets/css/bootstrap/bootstrap-datetimepicker.min.css"></link>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/bootstrap/bootstrap-datetimepicker.min.js"></script>
<!-- END PAGE PLUGINS -->
<!-- START TEMPLATE -->
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>
<!-- END TEMPLATE -->
<!-- THIS PAGE JS SETTINGS -->
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/citizen_security/index.js"></script>
<!--  -->
<!-- END SCRIPTS --> 