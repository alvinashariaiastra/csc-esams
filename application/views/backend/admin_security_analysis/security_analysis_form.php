<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
	<li><a href="<?php echo $controller.'/'.$function; ?>"><?php echo $function_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?> Form</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">                                
					<h3 class="panel-title">Form</h3>                              
				</div>
				<form class="form-horizontal" id="px-security-analysis-form" method="POST" action="<?php if($data) echo $controller.'/'.$function_edit; else echo $controller.'/'.$function_add; ?>">
				<input type="hidden" value="<?php if($data!=null) echo $data->id; ?>" name="id">
				<div class="panel-body">
					<div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
					<div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
					<div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-security-analysis-form-news-title">Title</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="title" id="px-security-analysis-form-news-title" value="<?php if($data!=null) echo $data->title; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-security-analysis-form-news-kategori">Kategori</label>
						<div class="col-md-6 col-xs-12">
							<select class="form-control" name="kategori" id="px-security-analysis-form-news-kategori">
								<option <?php if($data!=null) { if($data->kategori == 1) echo 'selected'; } ?> value="1">Analysis</option>
								<option <?php if($data!=null) { if($data->kategori == 2) echo 'selected'; } ?> value="2">Informasi</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-security-analysis-form-news-content">Content</label>
						<div class="col-md-10 col-xs-12">
							<textarea class="form-control ignore px-summernote" name="content" id="px-security-analysis-form-news-content"><?php if($data!=null) echo $data->content; ?></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-security-analysis-form-status">Visibility</label>
						<div class="col-md-9 col-xs-12">
							<div class="radio">
                                <label>
                                    <input type="radio" value="1" name="status" class="px-security-analysis-form-status" <?php if($data){ if($data->status == 1) echo 'checked'; } else echo 'checked'; ?>>
                                    Visible
                                </label>
                                <label>
                                    <input type="radio" value="0" name="status" class="px-security-analysis-form-status" <?php if($data){ if($data->status == 0) echo 'checked'; } ?>>
                                    Hidden
                                </label>
                            </div>
						</div>
					</div>
                                        <div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-security-analysis-form-karyawan_flag">Karyawan Show Flag</label>
						<div class="col-md-9 col-xs-12">
							<div class="radio">
                                <label>
                                    <input type="radio" value="1" name="karyawan_flag" class="px-security-analysis-form-karyawan_flag" <?php if($data){ if($data->karyawan_flag == 1) echo 'checked'; } ?>>
                                    Visible
                                </label>
                                <label>
                                    <input type="radio" value="0" name="karyawan_flag" class="px-security-analysis-form-karyawan_flag" <?php if($data){ if($data->karyawan_flag == 0) echo 'checked'; } else echo 'checked'; ?>>
                                    Hidden
                                </label>
                            </div>
						</div>
					</div>
				</div>
				<?php if(isset($data->image)){
					foreach ($data->image as $images) {
				?>
				<input type="hidden" name="images[]" value="<?php echo $images; ?>">
				<?php
					}
				} ?>
				<div class="panel-footer">
					<button class="btn btn-primary pull-right" type="submit">Save</button>
				</div>
				</form>
			</div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                                
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>  
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>    
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/security_analysis/security_analysis_form.js"></script>
	<!--  -->
<!-- END SCRIPTS -->   