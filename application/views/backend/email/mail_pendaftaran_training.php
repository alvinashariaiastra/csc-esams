<html>
<head>
  
</head>
<body>
<style type="text/css">
.mail_row {
  float: left;
  width: 100%;
}
.container {
  width: 768px;
}
</style>
<div class="container">
  <div class="mail_row">
  <h2 class="page-title" style="text-align:center;">FORM PENDAFTARAN TRAINING</h2>
  </div>
  <div class="mail_row">
    <table>
      <tbody>
        <tr>
          <td>Judul Training</td>
          <td>:</td>
          <td><?php echo $training_tl ?></td>
        </tr>
        <tr>
          <td>PIC</td>
          <td>:</td>
          <td><?php echo $nama_to ?></td>
        </tr>
        <tr>
          <td>Jabatan</td>
          <td>:</td>
          <td><?php echo $jabatan ?></td>
        </tr>
        <tr>
          <td>Instalasi</td>
          <td>:</td>
          <td><?php echo $instalasi ?></td>
        </tr>
        <tr>
          <td>Tanggal Mulai Pendaftaran</td>
          <td>:</td>
          <td><?php echo $pendaftaran_start ?></td>
        </tr>
        <tr>
          <td>Tanggal Stop Pendaftaran</td>
          <td>:</td>
          <td><?php echo $pendaftaran_end ?></td>
        </tr>
        <tr>
          <td>Tanggal Mulai Training</td>
          <td>:</td>
          <td><?php echo $date_start ?></td>
        </tr>
        <tr>
          <td>Tanggal Stop Training</td>
          <td>:</td>
          <td><?php echo $date_end ?></td>
        </tr>
        <tr>
          <td>List Peserta</td>
          <td>:</td>
          <td><?php echo $content ?></td>
        </tr>
        <tr>
          <td>Tanggal Pendaftaran</td>
          <td>:</td>
          <td><?php echo $date_created ?></td>
        </tr>
      </tbody>
    </table>                  
  </div>
  <div class="mail_row" style="margin-top:20px;">
    Pembuat Form, <br />
    <br />
    <br />
    ASIS
    <?php echo $nama_from; ?>
  </div>
</div>
</body>
</html>