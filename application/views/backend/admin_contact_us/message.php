<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?> to <?php echo $user->realname; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">
			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">                                
					<h3 class="panel-title"></h3>
					<!-- <ul class="panel-controls">
						<li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
						<li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
						<li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
					</ul>  -->                               
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<div class="messages messages-img">
					    	<?php if ($message->num_rows() > 0) {
					    		foreach ($message->result() as $row) { 
					    			if ($row->type == 1) { ?>
					    				<div class="item in item-visible">
			                                <div class="image">
			                                	<img src="<?=base_url()?>assets/uploads/user/<?php echo $row->user_id.'/'.$row->photo ?>" alt="<?php echo $row->realname ?>">
			                                </div>
			                                <div class="text">
			                                    <div class="heading">
			                                        <a href="#"><?php echo $row->realname ?></a>
			                                        <span class="date"><?php echo date('d M Y H:i', strtotime($row->date_created)) ?></span></br>
                                                                <?php if($row->status == 1) { ?>
                                                                <span class="date">Date Read : <?php echo date('d M Y H:i', strtotime($row->date_read)) ?></span>
                                                                <?php } else { ?>
                                                                <span class="date">Unread</span>
                                                                <?php } ?>
			                                    </div>
			                                    <?php echo $row->message; ?>
			                                </div>
			                            </div>
					    			<?php }else{ ?>
					    				<div class="item item-visible">
			                                <div class="image">
			                                	<img src="<?=base_url()?>assets/uploads/admin/<?php echo $admin->id.'/'.$admin->photo ?>" alt="<?php echo $admin->realname ?>">
			                                </div>
			                                <div class="text">
			                                    <div class="heading">
			                                        <a href="#"><?php echo $admin->realname ?></a>
                                                                <span class="date"><?php echo date('d M Y H:i', strtotime($row->date_created)) ?></span></br>
                                                                <?php if($row->status == 1) { ?>
                                                                <span class="date">Date Read : <?php echo date('d M Y H:i', strtotime($row->date_read)) ?></span>
                                                                <?php } else { ?>
                                                                <span class="date">Unread</span>
                                                                <?php } ?>
			                                    </div>
			                                    <?php echo $row->message; ?>
			                                </div>
			                            </div>
					    			<?php } } } ?>
		                    </div>                        
		                    <div class="panel panel-default push-up-10">
		                        <div class="panel-body panel-body-search">
		                            <div class="row">
		                            	<div class="col-md-12">
			                            	<form id="message_form" class="form-horizontal">
			                            	<div class="input-group">
			                                    <input type="text" class="message_txt form-control" name="message_txt" placeholder="Your message..."/>
			                                    <div class="input-group-btn">
			                                    	<input type="hidden" name="user_id" value="<?php echo $user->id ?>">
			                                        <button type="submit" class="btn btn-default">Send</button>
			                                    </div>
			                                </div>
			                                </form>
		                                </div>
		                            </div>
		                        </div>
		                    </div>
						</div>
					</div>
				</div>
				    
			</div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                                
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>   
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/contact_us/message.js"></script> 
	<!--  -->
<!-- END SCRIPTS -->   