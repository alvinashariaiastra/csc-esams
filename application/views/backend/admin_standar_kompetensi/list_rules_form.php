<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
	<li><a href="<?php echo $controller.'/'.$function; ?>"><?php echo $function_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?> Form</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">                                
					<h3 class="panel-title">Form</h3>                              
				</div>
				<form class="form-horizontal" id="px-standarkompetensi-listrules-form" method="POST" action="<?php if($data) echo $controller.'/'.$function_edit; else echo $controller.'/'.$function_add; ?>">
				<input type="hidden" value="<?php if($data!=null) echo $data->id; ?>" name="id">
				<div class="panel-body">
					<div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
					<div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
					<div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-standarkompetensi-listrules-form-listrules-training">Training</label>
						<div class="col-md-6 col-xs-12">
							<select class="form-control" name="standar_kompetensi_training_id" id="px-standarkompetensi-listrules-form-listrules-training">
								<option value="">-- Pilih training --</option>
								<?php if ($training->num_rows() > 0) {
									foreach ($training->result() as $row) { ?>
										<option value="<?php echo $row->id ?>" <?php if($data!=NULL) echo ($data->standar_kompetensi_training_id == $row->id ? "selected" : "") ?>><?php echo $row->name ?></option>
									<?php }
								} ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-standarkompetensi-listrules-form-listrules-leveluser">Level User</label>
						<div class="col-md-6 col-xs-12">
							<select class="form-control" name="level_user_id" id="px-standarkompetensi-listrules-form-listrules-leveluser">
								<option value="1" <?php if($data!=NULL) echo ($data->level_user_id == 1 ? "selected" : "") ?>>Management</option>
								<option value="2" <?php if($data!=NULL) echo ($data->level_user_id == 2 ? "selected" : "") ?>>Middle Management</option>
								<option value="3" <?php if($data!=NULL) echo ($data->level_user_id == 3 ? "selected" : "") ?>>Operational</option>
                                                                <option value="4" <?php if($data!=NULL) echo ($data->level_user_id == 4 ? "selected" : "") ?>>Guard</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-standarkompetensi-listrules-form-listrules-stc_id">Standar Kompetensi</label>
						<div class="col-md-6 col-xs-12">
							<select class="form-control" name="stc_id" id="px-standarkompetensi-listrules-form-listrules-stc_id">
								<option value="1" <?php if($data!=NULL) echo ($data->stc_id == 1 ? "selected" : "") ?>>STC-1</option>
								<option value="2" <?php if($data!=NULL) echo ($data->stc_id == 2 ? "selected" : "") ?>>STC-2</option>
								<option value="3" <?php if($data!=NULL) echo ($data->stc_id == 3 ? "selected" : "") ?>>STC-3</option>
								<option value="4" <?php if($data!=NULL) echo ($data->stc_id == 4 ? "selected" : "") ?>>STC-4</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-standarkompetensi-listrules-form-listrules-rules">Nama</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="rules" id="px-standarkompetensi-listrules-form-listrules-rules" value="<?php if($data!=null) echo $data->rules; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label">File Panduan</label>
						<div class="col-md-9 col-xs-12">                                                                                                                                      
							<label for="file-upload-file" class="btn btn-primary btn-upload" data-target="document" id="px-site-content-document-form-upload-button">Browse</label>
							<span id="px-site-content-document-form-file-upload-progress"></span>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-offset-2 col-xs-12 col-md-9">
							<div id="preview-document">
								<?php
								if($data){
									foreach ($data->document as $document) {
								?>
							<input type="hidden" name="document_old_id[]" value="<?php echo $document->id; ?>">
							<div class="per-preview-document">
								<input class="form-control" type="hidden" name="document_old[<?php echo $document->id; ?>]" value="<?php echo $document->file; ?>">
								<i class="fa fa-file-o"></i> 
								<span><?php echo $document->file; ?></span>
								<a href="<?php echo 'assets/uploads/list_rules/'.$document->id_list_rules.'/'.$document->file; ?>" target="_blank">
									<i class="fa fa-download" title="Download"></i>
								</a>
								<i class="fa fa-times btn-delete-file" title="Delete"></i>
							</div>
							<?php		
									}
								}
							?>
								
							</div>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<button class="btn btn-primary pull-right" type="submit">Save</button>
				</div>
				</form>
			</div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                                
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- FORM UPLOAD -->
<form id="file-upload" action="upload/file" method="POST" enctype="multipart/form-data" class="hidden">
	<input type="hidden" name="target" id="target-file">
	<input type="hidden" name="old" id="old-file">
	<input type="file" name="file" id="file-upload-file" multiple>
</form>
<!-- EOF FORM UPLOAD -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>  
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>  
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/fileupload/fileupload.min.js"></script>   
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/standar_kompetensi/list_rules_form.js"></script>
	<!--  -->
<!-- END SCRIPTS -->   