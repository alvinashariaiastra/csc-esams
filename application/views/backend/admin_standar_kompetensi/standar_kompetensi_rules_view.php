<table class="table table-bordered table-score">
	<thead></thead>
	<tbody>
		<tr>
			<th class="text-center">No</th>
			<th class="text-center">Panduan</th>
			<th class="text-center">Level User</th>
			<th class="text-center">Rules</th>
			<th colspan="4" class="text-center">Score</th>
		</tr>
		<?php 
		$i = 1;
		if ($stc->num_rows() > 0) {
			foreach ($stc->result() as $row) {
			?>
		<tr>
			<td class="text-center"><?php echo $i ?></td>
			<td class="text-center">
			<?php if($row->document->row()) { ?>
			<a href="<?=base_url()?>assets/uploads/list_rules/<?php echo $row->id.'/'.$row->document->row()->file; ?>"><?php echo $row->document->row()->file; ?></a>
			<?php } else { echo '-'; } ?>
			</td>
			<td class="text-center"><?php 
			if($row->level_user_id == 1)
				echo 'Management';
			elseif ($row->level_user_id == 2)
				echo 'Middle Management';
			elseif ($row->level_user_id == 3)
				echo 'Operational'; 
			?></td>
			<td class="text-center"><?php echo $row->rules; ?></td>
			<td class="text-center"><input type="radio" name="stc<?php echo $row->stc_id; ?>_score<?php echo $i ?>" value="1" class="stc-score-<?php echo $row->stc_id; ?> pull-left" checked><label class="pull-left">1</label></td>
			<td class="text-center"><input type="radio" name="stc<?php echo $row->stc_id; ?>_score<?php echo $i ?>" value="2" class="stc-score-<?php echo $row->stc_id; ?> pull-left"><label class="pull-left">2</label></td>
			<td class="text-center"><input type="radio" name="stc<?php echo $row->stc_id; ?>_score<?php echo $i ?>" value="3" class="stc-score-<?php echo $row->stc_id; ?> pull-left"><label class="pull-left">3</label></td>
			<td class="text-center"><input type="radio" name="stc<?php echo $row->stc_id; ?>_score<?php echo $i ?>" value="4" class="stc-score-<?php echo $row->stc_id; ?> pull-left"><label class="pull-left">4</label></td>
		</tr>
		<?php $i++; } ?>
		<?php } else { ?>
		<tr><td colspan="6" class="text-center">Tidak ada rules.</td></tr>
		<?php } ?>
	</tbody>
</table>