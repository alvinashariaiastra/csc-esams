<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
	<li><a href="<?php echo $controller.'/'.$function; ?>"><?php echo $function_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?> Form</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">                                
					<h3 class="panel-title">Form Standar Kompetensi</h3>                              
				</div>
				<div class="panel-body">
			    <form id="standar-kompetensi-form" class="form-horizontal" id="px-standarkompetensi-form" action="#">
			        <div>
			            <h3>Pilih Karyawan</h3>
			            <section>
				            <div class="form-group">
								<label class="col-md-2 col-xs-12 control-label" for="#px-standarkompetensi-form-standarkompetensi-perusahaan">Perusahaan</label>
								<div class="col-md-6 col-xs-12">
									<select class="form-control" name="perusahaan_id" id="px-standarkompetensi-form-standarkompetensi-perusahaan">
										<?php 
										if ($perusahaan) {
											foreach ($perusahaan->result() as $data_row) {
												if ($data!=null && $data->perusahaan_id == $data_row->id) {
														$selected = "selected";
													}else{
														$selected = "";
													}
												?>
												<option value="<?php echo $data_row->id ?>"<?php echo $selected ?>><?php echo $data_row->name ?></option>
										<?php
											}
										}
										 ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 col-xs-12 control-label" for="#px-standarkompetensi-form-standarkompetensi-karyawan">Karyawan</label>
								<div class="col-md-6 col-xs-12">
									<select class="form-control" name="perusahaan_id" id="px-standarkompetensi-form-standarkompetensi-karyawan">
										<?php 
										if ($user) {
											foreach ($user->result() as $data_row) {
												if ($data!=null && $data->perusahaan_id == $data_row->id) {
														$selected = "selected";
													}else{
														$selected = "";
													}
												?>
												<option value="<?php echo $data_row->id ?>"<?php echo $selected ?>><?php echo $data_row->realname ?></option>
										<?php
											}
										}
										 ?>
									</select>
								</div>
							</div>
			            </section>
			            <!-- STC-1 -->
			            <h3>STC-1</h3>
			            <section>
			                <table class="table table-bordered table-score">
			                	<thead></thead>
			                	<tbody>
			                		<tr>
			                			<th class="text-center">No</th>
			                			<th class="text-center">Rules</th>
			                			<th colspan="4" class="text-center">Score</th>
			                		</tr>
			                		<?php 
			                		$i = 1;
			                		if ($stc1->num_rows() > 0) {
			                			foreach ($stc1->result() as $row) { ?>
			                		<tr>
			                			<td><?php echo $i ?></td>
			                			<td><?php echo $row->rules ?></td>
			                			<td><input type="radio" name="stc1_score<?php echo $i ?>" value="1" checked class="pull-left"><label class="pull-left">1</label></td>
			                			<td><input type="radio" name="stc1_score<?php echo $i ?>" value="2" class="pull-left"><label class="pull-left">2</label></td>
			                			<td><input type="radio" name="stc1_score<?php echo $i ?>" value="3" class="pull-left"><label class="pull-left">3</label></td>
			                			<td><input type="radio" name="stc1_score<?php echo $i ?>" value="4" class="pull-left"><label class="pull-left">4</label></td>
			                		</tr>
			                		<?php $i++; 
			                		} ?>
			                		<?php }else{ ?>
			                		<tr><td colspan="6" class="text-center">Tidak ada rules.</td></tr>
			                		<?php } ?>
			                	</tbody>
			                </table>
			            </section>
			            <!-- STC-2 -->
			            <h3>STC-2</h3>
			            <section>
			                <table class="table table-bordered table-score">
			                	<thead></thead>
			                	<tbody>
			                		<tr>
			                			<th class="text-center">No</th>
			                			<th class="text-center">Rules</th>
			                			<th colspan="4" class="text-center">Score</th>
			                		</tr>
			                		<?php 
			                		$i = 1;
			                		if ($stc2->num_rows() > 0) {
			                			foreach ($stc2->result() as $row) { ?>
			                		<tr>
			                			<td><?php echo $i ?></td>
			                			<td><?php echo $row->rules ?></td>
			                			<td><input type="radio" name="stc2_score<?php echo $i ?>" value="1" checked class="pull-left"><label class="pull-left">1</label></td>
			                			<td><input type="radio" name="stc2_score<?php echo $i ?>" value="2" class="pull-left"><label class="pull-left">2</label></td>
			                			<td><input type="radio" name="stc2_score<?php echo $i ?>" value="3" class="pull-left"><label class="pull-left">3</label></td>
			                			<td><input type="radio" name="stc2_score<?php echo $i ?>" value="4" class="pull-left"><label class="pull-left">4</label></td>
			                		</tr>
			                		<?php $i++; 
			                		} ?>
			                		<?php }else{ ?>
			                		<tr><td colspan="6" class="text-center">Tidak ada rules.</td></tr>
			                		<?php } ?>
			                	</tbody>
			                </table>
			            </section>
			            <!-- STC-3 -->
			            <h3>STC-3</h3>
			            <section>
			                <table class="table table-bordered table-score">
			                	<thead></thead>
			                	<tbody>
			                		<tr>
			                			<th class="text-center">No</th>
			                			<th class="text-center">Rules</th>
			                			<th colspan="4" class="text-center">Score</th>
			                		</tr>
			                		</tr>
			                		<?php 
			                		$i = 1;
			                		if ($stc3->num_rows() > 0) {
			                			foreach ($stc3->result() as $row) { ?>
			                		<tr>
			                			<td><?php echo $i ?></td>
			                			<td><?php echo $row->rules ?></td>
			                			<td><input type="radio" name="stc3_score<?php echo $i ?>" value="1" checked class="pull-left"><label class="pull-left">1</label></td>
			                			<td><input type="radio" name="stc3_score<?php echo $i ?>" value="2" class="pull-left"><label class="pull-left">2</label></td>
			                			<td><input type="radio" name="stc3_score<?php echo $i ?>" value="3" class="pull-left"><label class="pull-left">3</label></td>
			                			<td><input type="radio" name="stc3_score<?php echo $i ?>" value="4" class="pull-left"><label class="pull-left">4</label></td>
			                		</tr>
			                		<?php $i++; 
			                		} ?>
			                		<?php }else{ ?>
			                		<tr><td colspan="6" class="text-center">Tidak ada rules.</td></tr>
			                		<?php } ?>
			                	</tbody>
			                </table>
			            </section>
			            <!-- STC-4 -->
			            <h3>STC-4</h3>
			            <section>
			                <table class="table table-bordered table-score">
			                	<thead></thead>
			                	<tbody>
			                		<tr>
			                			<th class="text-center">No</th>
			                			<th class="text-center">Rules</th>
			                			<th colspan="4" class="text-center">Score</th>
			                		</tr>
			                		<?php 
			                		$i = 1;
			                		if ($stc4->num_rows() > 0) {
			                			foreach ($stc4->result() as $row) { ?>
			                		<tr>
			                			<td><?php echo $i ?></td>
			                			<td><?php echo $row->rules ?></td>
			                			<td><input type="radio" name="stc4_score<?php echo $i ?>" value="1" checked class="pull-left"><label class="pull-left">1</label></td>
			                			<td><input type="radio" name="stc4_score<?php echo $i ?>" value="2" class="pull-left"><label class="pull-left">2</label></td>
			                			<td><input type="radio" name="stc4_score<?php echo $i ?>" value="3" class="pull-left"><label class="pull-left">3</label></td>
			                			<td><input type="radio" name="stc4_score<?php echo $i ?>" value="4" class="pull-left"><label class="pull-left">4</label></td>
			                		</tr>
			                		<?php $i++; 
			                		} ?>
			                		<?php }else{ ?>
			                		<tr><td colspan="6" class="text-center">Tidak ada rules.</td></tr>
			                		<?php } ?>
			                	</tbody>
			                </table>
			            </section>
			          
			        </div>
			    </form>
			    </div>

			</div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                                
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>  
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>    
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/standar_kompetensi/standar_kompetensi_form.js"></script>
	<!--  -->
<!-- END SCRIPTS -->   