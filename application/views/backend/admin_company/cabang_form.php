<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
	<li><a href="<?php echo $controller.'/'.$function; ?>"><?php echo $function_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?> Form</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">                                
					<h3 class="panel-title">Form</h3>                              
				</div>
				<form class="form-horizontal" id="px-company-instalasi-form" method="POST" action="<?php if($data) echo $controller.'/'.$function_edit; else echo $controller.'/'.$function_add; ?>">
				<input type="hidden" value="<?php if($data!=null) echo $data->id; ?>" name="id">
				<div class="panel-body">
					<div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
					<div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
					<div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-instalasi-form-instalasi-businesstype">Jenis Bisnis</label>
						<div class="col-md-6 col-xs-12">
							<select class="form-control" name="jenis_bisnis_id" id="px-company-instalasi-form-instalasi-businesstype">
								<option value="0">-- Pilih Jenis Bisnis --</option>
								<?php 
								if ($business_type) {
									foreach ($business_type->result() as $data_row) {
										if ($data!=null && $data->jenis_bisnis_id == $data_row->id) {
												$selected = "selected";
											}else{
												$selected = "";
											}
										?>
										<option value="<?php echo $data_row->id ?>"<?php echo $selected ?>><?php echo $data_row->name ?></option>
								<?php
									}
								}
								 ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-instalasi-form-instalasi-perusahaan">Perusahaan</label>
						<div class="col-md-6 col-xs-12">
							<select class="form-control" name="perusahaan_id" id="px-company-instalasi-form-instalasi-perusahaan">
								<option value="0">-- Pilih Perusahaan --</option>
								<?php 
								if ($perusahaan) {
									foreach ($perusahaan->result() as $data_row) {
										if ($data!=null && $data->perusahaan_id == $data_row->id) {
												$selected = "selected";
											}else{
												$selected = "";
											}
										?>
										<option value="<?php echo $data_row->id ?>"<?php echo $selected ?>><?php echo $data_row->name ?></option>
								<?php
									}
								}
								 ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-instalasi-form-instalasi-name">Nama Instalasi</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="name" id="px-company-instalasi-form-instalasi-name" value="<?php if($data!=null) echo $data->name; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-instalasi-form-instalasi-alamat">Alamat Instalasi</label>
						<div class="col-md-6 col-xs-12">
							<textarea  class="form-control" name="alamat" id="px-company-instalasi-form-instalasi-alamat"><?php if($data!=null) echo $data->alamat; ?></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-instalasi-form-instalasi-province_id">Provinsi</label>
						<div class="col-md-6 col-xs-12">
							<select class="form-control" id="px-company-instalasi-form-instalasi-province-id" name="province_id">
								<option value="0">Pilih Salah Satu</option>
								<?php foreach ($province as $data_row) { ?>
								<option value="<?php echo $data_row->id; ?>" <?php if($data) { if($data->province_id == $data_row->id) echo 'selected'; } ?>><?php echo $data_row->name ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-instalasi-form-instalasi-koordinatx">Koordinat X (Lat)</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="coordinate_x" id="px-company-instalasi-form-instalasi-koordinatx" value="<?php if($data!=null) echo $data->coordinate_x; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-instalasi-form-instalasi-koordinaty">Koordinat Y (Long)</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="coordinate_y" id="px-company-instalasi-form-instalasi-koordinaty" value="<?php if($data!=null) echo $data->coordinate_y; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-instalasi-form-instalasi-telp">Phone</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="telp" id="px-company-instalasi-form-instalasi-telp" value="<?php if($data!=null) echo $data->telp; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-instalasi-form-instalasi-mobile_phone">Mobile Phone</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="mobile_phone" id="px-company-instalasi-form-instalasi-mobile_phone" value="<?php if($data!=null) echo $data->mobile_phone; ?>">
						</div>
					</div>
                                        <div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-companylist-form-companylist-jabatan1">Pimpinan</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="jabatan_1" id="px-company-companylist-form-companylist-jabatan1" value="<?php if($data!=null) echo $data->jabatan_1; ?>">
						</div>
					</div>
                                        <div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-companylist-form-companylist-jabatan2">Manager</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="jabatan_2" id="px-company-companylist-form-companylist-jabatan2" value="<?php if($data!=null) echo $data->jabatan_2; ?>">
						</div>
					</div>
                                        <div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-companylist-form-companylist-jabatan3">PIC</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="jabatan_3" id="px-company-companylist-form-companylist-jabatan3" value="<?php if($data!=null) echo $data->jabatan_3; ?>">
						</div>
					</div>
                                        <div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-companylist-form-companylist-jabatan4">Koordinator Satpam</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="jabatan_4" id="px-company-companylist-form-companylist-jabatan4" value="<?php if($data!=null) echo $data->jabatan_4; ?>">
						</div>
					</div>
                                        <!--
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-instalasi-form-instalasi-jabatan1">Pimpinan</label>
						<div class="col-md-6 col-xs-12">
							<select class="form-control" name="jabatan1_id" id="px-company-instalasi-form-instalasi-jabatan1">
								<option value="0">Pilih User</option>
								<?php if ($jabatan1) {
									foreach ($jabatan1->result() as $row) { 
										if ($data!=null && $data->jabatan1_id == $row->id) {
												$selected = "selected";
											}else{
												$selected = "";
											}
											?>
										<option value="<?php echo $row->id ?>" <?php echo $selected ?>><?php echo $row->realname ?></option>
									<?php }
								} ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-instalasi-form-instalasi-jabatan2">Manajer</label>
						<div class="col-md-6 col-xs-12">
							<select class="form-control" name="jabatan2_id" id="px-company-instalasi-form-instalasi-jabatan2">
								<option value="0">Pilih User</option>
								<?php if ($jabatan2) {
									foreach ($jabatan2->result() as $row) { 
										if ($data!=null && $data->jabatan2_id == $row->id) {
												$selected = "selected";
											}else{
												$selected = "";
											}
											?>
										<option value="<?php echo $row->id ?>"  <?php echo $selected ?>><?php echo $row->realname ?></option>
									<?php }
								} ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-instalasi-form-instalasi-jabatan3">PIC</label>
						<div class="col-md-6 col-xs-12">
							<select class="form-control" name="jabatan3_id" id="px-company-instalasi-form-instalasi-jabatan3">
								<option value="0">Pilih User</option>
								<?php if ($jabatan3) {
									foreach ($jabatan3->result() as $row) { 
										if ($data!=null && $data->jabatan3_id == $row->id) {
												$selected = "selected";
											}else{
												$selected = "";
											}
										?>
										<option value="<?php echo $row->id ?>"  <?php echo $selected ?>><?php echo $row->realname ?></option>
									<?php }
								} ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-instalasi-form-instalasi-jabatan4">Koordinator Satpam</label>
						<div class="col-md-6 col-xs-12">
							<select class="form-control" name="jabatan4_id" id="px-company-instalasi-form-instalasi-jabatan4">
								<option value="0">Pilih User</option>
								<?php if ($jabatan4) {
									foreach ($jabatan4->result() as $row) { 
										if ($data!=null && $data->jabatan4_id == $row->id) {
												$selected = "selected";
											}else{
												$selected = "";
											}
										?>
										<option value="<?php echo $row->id ?>"  <?php echo $selected ?>><?php echo $row->realname ?></option>
									<?php }
								} ?>
							</select>
						</div>
					</div>
                                        -->
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-instalasi-form-instalasi-jumlahkaryawantetap">Karyawan Tetap</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="jumlah_karyawan_tetap" id="px-company-instalasi-form-instalasi-jumlahkaryawantetap" value="<?php if($data!=null) echo $data->jumlah_karyawan_tetap; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-instalasi-form-instalasi-jumlahkaryawantidaktetap">Karyawan Tidak Tetap</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="jumlah_karyawan_tidak_tetap" id="px-company-instalasi-form-instalasi-jumlahkaryawantidaktetap" value="<?php if($data!=null) echo $data->jumlah_karyawan_tidak_tetap; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-instalasi-form-instalasi-jumlahpersonilsecurityorganik">Security Organik</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="jumlah_personil_security_organik" id="px-company-instalasi-form-instalasi-jumlahpersonilsecurityorganik" value="<?php if($data!=null) echo $data->jumlah_personil_security_organik; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-instalasi-form-instalasi-jumlahpersonilsecurityoutsourching">Security Outsourching</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="jumlah_personil_security_outsourching" id="px-company-instalasi-form-instalasi-jumlahpersonilsecurityoutsourching" value="<?php if($data!=null) echo $data->jumlah_personil_security_outsourching; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-instalasi-form-instalasi-namaserikatpekerja">Serikat Pekerja</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="nama_serikat_pekerja" id="px-company-instalasi-form-instalasi-namaserikatpekerja" value="<?php if($data!=null) echo $data->nama_serikat_pekerja; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-instalasi-form-instalasi-jumlahanggotaserikatpekerja">Jumlah Anggota SP</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="jumlah_anggota_serikat_pekerja" id="px-company-instalasi-form-instalasi-jumlahanggotaserikatpekerja" value="<?php if($data!=null) echo $data->jumlah_anggota_serikat_pekerja; ?>">
						</div>
					</div>

				</div>
				<div class="panel-footer">
					<button class="btn btn-primary pull-right" type="submit">Save</button>
				</div>
				</form>
			</div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                                
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>  
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>    
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/company/cabang_form.js"></script>
	<!--  -->
<!-- END SCRIPTS -->   