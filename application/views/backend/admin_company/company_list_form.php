<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
	<li><a href="<?php echo $controller.'/'.$function; ?>"><?php echo $function_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?> Form</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">                                
					<h3 class="panel-title">Form</h3>                              
				</div>
				<form class="form-horizontal" id="px-company-companylist-form" method="POST" action="<?php if($data) echo $controller.'/'.$function_edit; else echo $controller.'/'.$function_add; ?>">
				<input type="hidden" value="<?php if($data!=null) echo $data->id; ?>" name="id">
				<div class="panel-body">
					<div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
					<div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
					<div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-companylist-form-companylist-businesstype">Sub Bisnis</label>
						<div class="col-md-6 col-xs-12">
							<select class="form-control" name="jenis_bisnis_id" id="px-company-companylist-form-companylist-businesstype">
								<?php 
								if ($business_type) {
									foreach ($business_type->result() as $data_row) {
										if ($data!=null && $data->jenis_bisnis_id == $data_row->id) {
												$selected = "selected";
											}else{
												$selected = "";
											}
										?>
										<option value="<?php echo $data_row->id ?>"<?php echo $selected ?>><?php echo $data_row->name ?></option>
								<?php
									}
								}
								 ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-companylist-form-companylist-name">Nama Perusahaan</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="name" id="px-company-companylist-form-companylist-name" value="<?php if($data!=null) echo $data->name; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-companylist-form-companylist-alamat">Alamat  Perusahaan</label>
						<div class="col-md-6 col-xs-12">
							<textarea  class="form-control" name="alamat" id="px-company-companylist-form-companylist-alamat"><?php if($data!=null) echo $data->alamat; ?></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-companylist-form-companylist-telp">Phone</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="telp" id="px-company-companylist-form-companylist-telp" value="<?php if($data!=null) echo $data->telp; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-companylist-form-companylist-mobile_phone">Mobile Phone</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="mobile_phone" id="px-company-companylist-form-companylist-mobile_phone" value="<?php if($data!=null) echo $data->mobile_phone; ?>">
						</div>
					</div>
                                        <div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-companylist-form-companylist-jabatan1">Pimpinan</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="jabatan_1" id="px-company-companylist-form-companylist-jabatan1" value="<?php if($data!=null) echo $data->jabatan_1; ?>">
						</div>
					</div>
                                        <div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-companylist-form-companylist-jabatan2">Manager</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="jabatan_2" id="px-company-companylist-form-companylist-jabatan2" value="<?php if($data!=null) echo $data->jabatan_2; ?>">
						</div>
					</div>
                                        <div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-companylist-form-companylist-jabatan3">PIC</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="jabatan_3" id="px-company-companylist-form-companylist-jabatan3" value="<?php if($data!=null) echo $data->jabatan_3; ?>">
						</div>
					</div>
                                        <div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-companylist-form-companylist-jabatan4">Koordinator Satpam</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="jabatan_4" id="px-company-companylist-form-companylist-jabatan4" value="<?php if($data!=null) echo $data->jabatan_4; ?>">
						</div>
					</div>
                                        <!--
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-companylist-form-companylist-jabatan1">Pimpinan</label>
						<div class="col-md-6 col-xs-12">
							<select class="form-control" name="jabatan1_id" id="px-company-companylist-form-companylist-jabatan1">
								<option value="0">Pilih User</option>
								<?php if ($jabatan1) {
									foreach ($jabatan1->result() as $row) { 
										if ($data!=null && $data->jabatan1_id == $row->id) {
												$selected = "selected";
											}else{
												$selected = "";
											}
											?>
										<option value="<?php echo $row->id ?>" <?php echo $selected ?>><?php echo $row->realname ?></option>
									<?php }
								} ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-companylist-form-companylist-jabatan2">Manager</label>
						<div class="col-md-6 col-xs-12">
							<select class="form-control" name="jabatan2_id" id="px-company-companylist-form-companylist-jabatan2">
								<option value="0">Pilih User</option>
								<?php if ($jabatan2) {
									foreach ($jabatan2->result() as $row) { 
										if ($data!=null && $data->jabatan2_id == $row->id) {
												$selected = "selected";
											}else{
												$selected = "";
											}
											?>
										<option value="<?php echo $row->id ?>"  <?php echo $selected ?>><?php echo $row->realname ?></option>
									<?php }
								} ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-companylist-form-companylist-jabatan3">PIC</label>
						<div class="col-md-6 col-xs-12">
							<select class="form-control" name="jabatan3_id" id="px-company-companylist-form-companylist-jabatan3">
								<option value="0">Pilih User</option>
								<?php if ($jabatan3) {
									foreach ($jabatan3->result() as $row) { 
										if ($data!=null && $data->jabatan3_id == $row->id) {
												$selected = "selected";
											}else{
												$selected = "";
											}
										?>
										<option value="<?php echo $row->id ?>"  <?php echo $selected ?>><?php echo $row->realname ?></option>
									<?php }
								} ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-companylist-form-companylist-jabatan4">Koordinator Satpam</label>
						<div class="col-md-6 col-xs-12">
							<select class="form-control" name="jabatan4_id" id="px-company-companylist-form-companylist-jabatan4">
								<option value="0">Pilih User</option>
								<?php if ($jabatan4) {
									foreach ($jabatan4->result() as $row) { 
										if ($data!=null && $data->jabatan4_id == $row->id) {
												$selected = "selected";
											}else{
												$selected = "";
											}
										?>
										<option value="<?php echo $row->id ?>"  <?php echo $selected ?>><?php echo $row->realname ?></option>
									<?php }
								} ?>
							</select>
						</div>
					</div>
                                        -->
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-companylist-form-companylist-jumlahkaryawantetap">Karyawan Tetap</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="jumlah_karyawan_tetap" id="px-company-companylist-form-companylist-jumlahkaryawantetap" value="<?php if($data!=null) echo $data->jumlah_karyawan_tetap; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-companylist-form-companylist-jumlahkaryawantidaktetap">Karyawan Tidak Tetap</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="jumlah_karyawan_tidak_tetap" id="px-company-companylist-form-companylist-jumlahkaryawantidaktetap" value="<?php if($data!=null) echo $data->jumlah_karyawan_tidak_tetap; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-companylist-form-companylist-jumlahpersonilsecurityorganik">Security Organik</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="jumlah_personil_security_organik" id="px-company-companylist-form-companylist-jumlahpersonilsecurityorganik" value="<?php if($data!=null) echo $data->jumlah_personil_security_organik; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-companylist-form-companylist-jumlahpersonilsecurityoutsourching">Security Outsourching</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="jumlah_personil_security_outsourching" id="px-company-companylist-form-companylist-jumlahpersonilsecurityoutsourching" value="<?php if($data!=null) echo $data->jumlah_personil_security_outsourching; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-companylist-form-companylist-namaserikatpekerja">Serikat Pekerja</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="nama_serikat_pekerja" id="px-company-companylist-form-companylist-namaserikatpekerja" value="<?php if($data!=null) echo $data->nama_serikat_pekerja; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-companylist-form-companylist-jumlahanggotaserikatpekerja">Jumlah Anggota SP</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="jumlah_anggota_serikat_pekerja" id="px-company-companylist-form-companylist-jumlahanggotaserikatpekerja" value="<?php if($data!=null) echo $data->jumlah_anggota_serikat_pekerja; ?>">
						</div>
					</div>

				</div>
				<div class="panel-footer">
					<button class="btn btn-primary pull-right" type="submit">Save</button>
				</div>
				</form>
			</div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                                
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>  
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>    
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/company/company_list_form.js"></script>
	<!--  -->
<!-- END SCRIPTS -->   