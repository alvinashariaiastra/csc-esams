<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
	<li><a href="<?php echo $controller.'/'.$function; ?>"><?php echo $function_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">                                
					<h3 class="panel-title">Detail Instalasi</h3>                              
				</div>
				<input type="hidden" value="<?php if($data!=null) echo $data->id; ?>" name="id">
				<div class="panel-body">
					<div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
					<div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
					<div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
					<!-- NEW LAYOUT -->
					<div class="row">
						<div class="col-md-12"><h3>Data Instalasi</h3></div>
						<div class="col-md-8">
							<table class="table">
								<tr>
									<td>Jenis Bisnis</td>
									<td><?php echo $business_type ?></td>
								</tr>
								<tr>
									<td>Perusahaan</td>
									<td><?php echo $perusahaan ?></td>
								</tr>
								<tr>
									<td>Nama Instalasi</td>
									<td><?php echo $data->name ?></td>
								</tr>
								<tr>
									<td>Alamat Instalasi</td>
									<td><?php echo $data->alamat ?></td>
								</tr>
								<tr>
									<td>Provinsi</td>
									<td><?php echo $province ?></td>
								</tr>
								<tr>
									<td>Phone</td>
									<td><?php echo $data->telp ?></td>
								</tr>
								<tr>
									<td>Mobile Phone</td>
									<td><?php echo $data->mobile_phone ?></td>
								</tr>
								<tr>
									<td>Jabatan 1</td>
									<td><?php echo $jabatan1 ?></td>
								</tr>
								<tr>
									<td>Jabatan 2</td>
									<td><?php echo $jabatan2 ?></td>
								</tr>
								<tr>
									<td>Jabatan 3</td>
									<td><?php echo $jabatan3 ?></td>
								</tr>
								<tr>
									<td>Jabatan 4</td>
									<td><?php echo $jabatan4 ?></td>
								</tr>
								<tr>
									<td>Koordinat X (Lat)</td>
									<td><?php echo $data->jumlah_anggota_serikat_pekerja ?></td>
								</tr>
								<tr>
									<td>Koordinat Y (Long)</td>
									<td><?php echo $data->jumlah_anggota_serikat_pekerja ?></td>
								</tr>
							</table>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12"><h3>Data Karyawan</h3></div>
						<div class="col-md-8">
							<table class="table">
								<tr>
									<td>Karyawan Tetap</td>
									<td><?php echo $data->jumlah_karyawan_tetap ?></td>
								</tr>
								<tr>
									<td>Karyawan Tidak Tetap</td>
									<td><?php echo $data->jumlah_karyawan_tidak_tetap ?></td>
								</tr>
								<tr>
									<td>Security Organik</td>
									<td><?php echo $data->jumlah_personil_security_organik ?></td>
								</tr>
								<tr>
									<td>Security Outsourching</td>
									<td><?php echo $data->jumlah_personil_security_outsourching ?></td>
								</tr>
								<tr>
									<td>Nama Serikat Pekerja</td>
									<td><?php echo $data->nama_serikat_pekerja ?></td>
								</tr>
								<tr>
									<td>Jumlah Serikat Pekerja</td>
									<td><?php echo $data->jumlah_anggota_serikat_pekerja ?></td>
								</tr>
							</table>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12"><h3>Data Serikat Pekerja</h3></div>
						<div class="col-md-8">
							<table class="table">
								<tr>
									<td>Nama Serikat Pekerja</td>
									<td><?php echo $data->nama_serikat_pekerja ?></td>
								</tr>
								<tr>
									<td>Jumlah Serikat Pekerja</td>
									<td><?php echo $data->jumlah_anggota_serikat_pekerja ?></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<a href="admin_company/instalasi"><button class="btn btn-primary pull-right">Back</button></a>
				</div>
			</div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                                
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>  
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>    
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/company/cabang_form.js"></script>
	<!--  -->
<!-- END SCRIPTS -->   