<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
	<li><a href="<?php echo $controller.'/'.$function; ?>"><?php echo $function_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?> Form</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">                                
					<h3 class="panel-title">Form</h3>                              
				</div>
				<form class="form-horizontal" id="px-company-assessment2004-form" method="POST" action="<?php if($data) echo $controller.'/'.$function_edit; else echo $controller.'/'.$function_add; ?>">
				<input type="hidden" value="<?php if($data!=null) echo $data->id; ?>" name="id">
				<div class="panel-body">
					<div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
					<div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
					<div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-assessment2004-form-assessment2004-periode">Periode</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" name="periode" class="form-control" id="px-company-assessment2004-form-assessment2004-periode" value="<?php if($data!=null) echo $data->periode; else echo date('Y'); ?>">
						</div>
					</div>
					<div class="form-group hidden">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-assessment2004-form-assessment2004-name">Name</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="name" id="px-company-assessment2004-form-assessment2004-name" value="<?php if($data!=null) echo $data->name; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-assessment2004-form-assessment2004-businesstype">Instalasi</label>
						<div class="col-md-6 col-xs-12">
							<select class="form-control" name="instalasi_id" id="px-company-assessment2004-form-assessment2004-businesstype">
								<?php 
								if ($company) {
									foreach ($company->result() as $data_row) {
										if ($data!=null && $data->instalasi_id == $data_row->id) {
												$selected = "selected";
											}else{
												$selected = "";
											}
										?>
										<option value="<?php echo $data_row->id ?>"<?php echo $selected ?>><?php echo $data_row->name ?></option>
								<?php
									}
								}
								 ?>
							</select>
						</div>
					</div>
					<?php $el=1; foreach ($rules as $r) { ?>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-assessment2004-form-assessment2004-rules<?php echo $r->id; ?>"><?php echo $r->name; ?></label>
						<div class="col-md-6 col-xs-12">
							<input type="hidden" name="rules_id[]" value="<?php echo $r->id; ?>">
							<input type="text" class="form-control rules-element" name="rules_value[<?php echo $r->id; ?>]" id="px-company-assessment2004-form-assessment2004-rules<?php echo $el; ?>" value="<?php if($data!=null && isset($r->value)) echo $r->value; else echo 0; ?>">
						</div>
					</div>
					<?php $el++; } ?>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-assessment2004-form-assessment2004-nilai_akhir">Nilai Akhir</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="nilai_akhir" id="px-company-assessment2004-form-assessment2004-nilai_akhir" value="<?php if($data!=null) echo $data->nilai_akhir; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-assessment2004-form-assessment2004-nilai_akhir_warna">Nilai Akhir Warna</label>
						<div class="col-md-6 col-xs-12">
							<div id="nilai-akhir-colour"></div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-company-assessment2004-form-assessment2004-critical_point">Critical Point</label>
						<div class="col-md-10 col-xs-12">
							<textarea class="form-control px-summernote" name="critical_point" id="px-company-assessment2004-form-assessment2004-critical_point"><?php if($data!=null) echo $data->critical_point; ?></textarea>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<button class="btn btn-primary pull-right" type="submit">Save</button>
				</div>
				</form>
			</div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                                
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>  
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>    
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/company/assessment2004_form.js"></script>
	<!--  -->
<!-- END SCRIPTS -->   