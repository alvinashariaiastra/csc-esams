<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
	<li><a href="<?php echo $controller.'/'.$function; ?>"><?php echo $function_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?> Form</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">                                
					<h3 class="panel-title">Form</h3>                              
				</div>
				<form class="form-horizontal" id="px-knowledge-knowledge-form" method="POST" action="<?php if($data) echo $controller.'/'.$function_edit; else echo $controller.'/'.$function_add; ?>">
				<input type="hidden" value="<?php if($data!=null) echo $data->id; ?>" name="id">
				<div class="panel-body">
					<div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
					<div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
					<div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-knowledge-knowledge-form-knowledge-category">Category</label>
						<div class="col-md-6 col-xs-12">

							<select class="form-control" name="knowledge_base_category_id" id="px-knowledge-knowledge-form-knowledge-category">
								<option value="0">-- Select Category --</option>
								<?php
									if ($knowledge_category) {
										foreach ($knowledge_category->result() as $data_row) { 

											if ($data!=null && $data->knowledge_base_category_id == $data_row->id) {
												$selected = "selected";
											}else{
												$selected = "";
											}
											?>

											<option value="<?php echo $data_row->id ?>" <?php echo $selected ?>><?php echo $data_row->name ?></option>
										<?php }
									}
								 ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-knowledge-knowledge-form-knowledge-name">Name</label>
						<div class="col-md-6 col-xs-12">
							<input type="text" class="form-control" name="name" id="px-knowledge-knowledge-form-knowledge-name" value="<?php if($data!=null) echo $data->name; ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-knowledge-knowledge-form-knowledge-description">Description</label>
						<div class="col-md-6 col-xs-12">
							<textarea class="form-control" name="description" id="px-knowledge-knowledge-form-knowledge-description"><?php if($data!=null) echo $data->description; ?></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label">File</label>
						<div class="col-md-6 col-xs-12"> 

							<input type="hidden" name="old_file" value="<?php if($data!=null) echo $data->file; ?>">
							<input type="hidden" name="knowledge_file">
							<input type="hidden" name="file_src">                                             
							<label for="file-upload-file" class="btn btn-primary btn-upload" data-target="knowledge_file">Browse</label>
							<div id="preview_knowledge_file">
							<a href="<?=base_url()?>assets/uploads/knowledge/<?php if($data!=null) echo $data->id.'/'.$data->file; ?>" title="File Upload">
								<span><?php if($data!=null) echo $data->file ?></span>                             
							</a>
						</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2 col-xs-12 control-label" for="#px-knowledge-knowledge-form-knowledge-status">Visibility</label>
						<div class="col-md-9 col-xs-12">
							<div class="radio">
                                <label>
                                    <input type="radio" value="1" name="status" class="px-knowledge-knowledge-form-knowledge-status" <?php if($data){ if($data->status == 1) echo 'checked'; } else echo 'checked'; ?>>
                                    Visible
                                </label>
                                <label>
                                    <input type="radio" value="0" name="status" class="px-knowledge-knowledge-form-knowledge-status" <?php if($data){ if($data->status == 0) echo 'checked'; } ?>>
                                    Hidden
                                </label>
                            </div>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<button class="btn btn-primary pull-right" type="submit">Save</button>
				</div>
				</form>
			</div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                                
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- FORM UPLOAD -->
<form id="file-upload" action="upload/all" method="POST" enctype="multipart/form-data" class="hidden">
	<input type="hidden" name="target" id="target-file">
	<input type="hidden" name="old" id="old-file">
	<input type="file" name="file" id="file-upload-file">
</form>
<!-- EOF FORM UPLOAD -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/bootstrap/bootstrap-file-input.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>  
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/fileupload/fileupload.min.js"></script>  
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page/knowledge/knowledge_form.js"></script>
	<!--  -->
<!-- END SCRIPTS -->   