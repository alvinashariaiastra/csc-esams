<!DOCTYPE html>
<html lang="en" class="body-full-height">
	<head>     
		<!-- BASE URL -->
		<base href="<?php echo base_url(); ?>"></base>
		<!-- EOF BASE URL -->
		<!-- META SECTION -->
		<title><?php echo $app_title; ?> - <?php echo $function_name; ?></title>            
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		
		<link rel="icon" href="<?=base_url()?>assets/uploads/app_settings/<?php echo $app_favicon_logo; ?>" type="image/x-icon" />
		<!-- END META SECTION -->

		<!-- untuk captcha -->
	  	<html class="ltr yui3-js-enabled webkit js chrome chrome72 chrome72-0 win secure" dir="ltr" lang="en-US">
	  	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
	  	<meta content="initial-scale=1.0, width=device-width" name="viewport">
	  	<!-- <link class="lfr-css-file" data-senna-track="temporary" href="./forgot password - Astranet_files/aui.css" id="liferayAUICSS" rel="stylesheet" type="text/css">  -->  
	  	<!-- End -->
		
		<!-- CSS INCLUDE -->        
		<link rel="stylesheet" type="text/css" id="theme" href="<?=base_url()?>assets/backend_assets/css/theme-default.css"/>
		<!-- EOF CSS INCLUDE -->                                    
	</head>
	<body>

		<style type="text/css">   
		    .capbox(background-color:#428bca;border-width:0 12px 0 0;display:inline-block;*display:inline;zoom:1;padding:20px 10px 8px 8px)
		    .capbox-inner(font:bold 11px arial,sans-serif;color:#000;background-color:#dbf3ba;margin:5px auto 0 auto;padding:3px;-moz-border-radius:4px;-webkit-border-radius:4px;border-radius:4px)
		    #CaptchaDiv(font:bold 17px verdana,arial,sans-serif;font-style:italic;color:#000;background-color:#fff;padding:4px;-moz-border-radius:4px;-webkit-border-radius:4px;border-radius:4px)
		    #CaptchaInput(margin:1px 0 1px 0;width:90px)
		    .imgcover(background-image:url('/o/com.astra.forgot.password/portlet.jpg');background-size:cover;background-position:center;background-repeat:no-repeat)
		    .panel(border:solid .02em #3071a9;box-shadow:0 4px 10px 0 rgba(0,0,0,0.2),0 16px 20px 0 rgba(0,0,0,0.19))
		    input.parsley-error(color:#b94a48!important;background-color:#f2dede!important;border:1px solid #eed3d7!important)
		    .alert-top(position:fixed;top:250px;left:41%;width:23%)
		    .alert-warning,.portlet-msg-alert(background-color:#f8d7da)
		    .wrapper-login-astra .main-content .form-login(margin:auto 35%)
		    
		    .alert-top(position:fixed;top:250px;left:0;width:1000%)    
		  </style>
		
		<div class="login-container">
		
			<div class="login-box animated fadeInDown">
				<div class="login-logo" style="height:100px;background:rgba(0, 0, 0, 0) url('assets/uploads/app_settings/login_logo.png') no-repeat scroll center center / auto 100%;"></div>
				<div class="login-body">
					<div class="login-title"><strong>Welcome</strong>, Please login</div>
					<form action="admin/do_login" class="form-horizontal" method="post" id="login-form">
						<div role="alert" class="alert alert-success hidden">
							<strong>Success!</strong> <span>Login success, You'll be redirected.</span>
						</div>
						<div role="alert" class="alert alert-warning hidden">
							<strong>Processing!</strong> <span>Please wait ...</span>
						</div>
						<div role="alert" class="alert alert-danger hidden">
							<strong>Failed!</strong> <span>Login failed.</span>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<input type="text" class="form-control" placeholder="Username" name="username" id="username" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12">
								<input type="password" class="form-control" placeholder="Password" name="password" id="password" />
							</div>
						</div>
						<div class="form-group has-feedback">
		                    <div class="capbox">
		                        <center>                            
		                            <div id="CaptchaDiv"> 
		                              <canvas id="myCanvas" width="230" height="30" style="border:1px solid #d3d3d3; background-color:#fff">Your browser does not support the HTML5 canvas tag.</canvas>
		                            </div> 
		                        </center>
		                          <div class="capbox-inner"><br>
		                            <center> 
		                            	<h5 style="color: #e0e0e0">Enter the code above:</h5>
		                              	<div class="input-group" style="width: 92%">
		                                  	<input id="captcha" name="captcha" class="form-control" type="nama">
		                                  	<span class="input-group-addon"><i id="refresh" class="glyphicon glyphicon-refresh color-blue"></i></span> 
		                              	</div>
		                            </center>
		                        </div>
		                    </div>
		                </div>
						<div class="form-group">
							<!-- <div class="col-md-6">
								<a href="#" class="btn btn-link btn-block">Forgot your password?</a>
							</div> -->
							<div class="col-md-6 pull-right">
								<button class="btn btn-info btn-block" id="btnLogin">Log In</button>
							</div>
						</div>
					</form>
				</div>
				<div class="login-footer">
					<div class="pull-left">
					PT Astra International Tbk &copy; 2017
					</div>
					<!-- <div class="pull-right">
						<a href="#">About</a> |
						<a href="#">Privacy</a> |
						<a href="#">Contact Us</a>
					</div> -->
				</div>
			</div>
			
		</div>
	<!-- START SCRIPTS -->
		<!-- START PLUGINS -->
		<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery/jquery.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery/jquery-ui.min.js"></script>
		<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/bootstrap/bootstrap.min.js"></script>
		<!-- END PLUGINS -->
		
		<!-- THIS PAGE PLUGINS -->
		<script type='text/javascript' src='<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js'></script>
		<!-- END THIS PAGE PLUGINS -->               
		
		<script type="text/javascript">
			//captcha
			var coba;
		    $(function() {
		        Alert = {
		            show: function($div, msg) {
		              $div.find('.alert-msg').text(msg);
		              if ($div.css('display') === 'none') {
		                // fadein, fadeout.
		                $div.fadeIn(1000).delay(2000).fadeOut(2000);
		              }
		            },
		            info: function(msg) {
		              this.show($('#alert-info'), msg);
		            },
		            warn: function(msg) {
		              this.show($('#alert-warn'), msg);
		            }
		        }
		        
		        $('body').on('click', '.alert-close', function(){
		            $(this).parents('.alert').hide();
		        });   
		    });

		    var c = document.getElementById("myCanvas");
		    var ctx = c.getContext("2d");

		    ctx.beginPath();
		    ctx.moveTo(Math.floor(Math.random() * 100), 0);
		    ctx.lineTo(Math.floor(Math.random() * 300), Math.floor(Math.random() * 300));
		    ctx.stroke();

		    ctx.beginPath();
		    ctx.moveTo(Math.floor(Math.random() * 100), 0);
		    ctx.lineTo(Math.floor(Math.random() * 300), Math.floor(Math.random() * 150));
		    ctx.stroke();

		    ctx.beginPath();
		    ctx.moveTo(Math.floor(Math.random() * 100), 0);
		    ctx.lineTo(Math.floor(Math.random() * 300), Math.floor(Math.random() * 150));
		    ctx.stroke();

		    ctx.beginPath();
		    ctx.moveTo(Math.floor(Math.random() * 100), 0);
		    ctx.lineTo(Math.floor(Math.random() * 300), Math.floor(Math.random() * 150));
		    ctx.stroke();

		    ctx.beginPath();
		    ctx.moveTo(Math.floor(Math.random() * 100), 0);
		    ctx.lineTo(Math.floor(Math.random() * 300), Math.floor(Math.random() * 150));
		    ctx.stroke();
		    ctx.font = "italic bold 20px Courier New";
		    
		    var gantiCaphta = ["BulLetiN", "ineQuaLiTy", "mOuNting","coNTroVersy","iNtErval","diSCuSSion","ParANoid","eNTiRety","gRoUpIng","smaSHiNg","suPerVIsiOn","impLeMent","iNVenTor","roUTtine","average","beauty","hollow","people","agony","suspect","indicative","socket","follower","bent","chop","insurance","activity","grammar","meaning","liability","workshop","primary","offspring","hammer","trust","feeling","muddle","broadcast","safety","prediction","keyword","fairy","western","welcome","coal","flexibility","modelling","breach","rival","scope","gentle","laugh","mentality","biological","notifying","profile","monitor","horde","arrival","strain","claim","storey","prophet","appalling","announcement","highlighting","page","tense","disposal","statement","misery","blackmail"];
			//document.getElementById("demo").innerHTML = cars[Math.floor(Math.random() * 6)];
		    //var gantiCaphta = "asd";
		    coba = gantiCaphta[Math.floor(Math.random() * 73)];
		    ctx.fillText(coba,15,25);
		   // alert(coba);
		    $('#txtCaptcha').val(coba);

		    var statusCapchc = 0;

		    function validateForm()
		    {
		        if (statusCapchc == 0 )
		        {
		            return false;
		        }
		    }

		    $('#refresh').on('click',function(){
		        //var c1 = document.getElementById("myCanvas").value;
		        //alert(gantiCaphta[Math.floor(Math.random() * 73)]);
		        //alert("tes");
		        //var kosong = "";
		        ctx.clearRect(0,0,c.width,c.height);
		        //alert(this.get('responseData'));
		        //gantiCaphta = "diganti";
		        //alert(gantiCaphta);
		        ctx.beginPath();
		        ctx.moveTo(Math.floor(Math.random() * 100), 0);
		        ctx.lineTo(Math.floor(Math.random() * 300), Math.floor(Math.random() * 300));
		        ctx.stroke();

		        ctx.beginPath();
		        ctx.moveTo(Math.floor(Math.random() * 100), 0);
		        ctx.lineTo(Math.floor(Math.random() * 300), Math.floor(Math.random() * 150));
		        ctx.stroke();

		        ctx.beginPath();
		        ctx.moveTo(Math.floor(Math.random() * 100), 0);
		        ctx.lineTo(Math.floor(Math.random() * 300), Math.floor(Math.random() * 150));
		        ctx.stroke();

		        ctx.beginPath();
		        ctx.moveTo(Math.floor(Math.random() * 100), 0);
		        ctx.lineTo(Math.floor(Math.random() * 300), Math.floor(Math.random() * 150));
		        ctx.stroke();

		        ctx.beginPath();
		        ctx.moveTo(Math.floor(Math.random() * 100), 0);
		        ctx.lineTo(Math.floor(Math.random() * 300), Math.floor(Math.random() * 150));
		        ctx.stroke();
		        ctx.font = "italic bold 20px Courier New";
		        coba = gantiCaphta[Math.floor(Math.random() * 73)];
		        ctx.fillText(coba,15,25);
		        // alert(coba);
		        $('#txtCaptcha').val(coba);
		        //ctx.fillText(gantiCaphta[Math.floor(Math.random() * 73)],15,25);
		    });

		    $('#btnLogin').on('click',function() {
		      var inputCaptcha = $('#captcha').val();
		      if(inputCaptcha == ""){
		        alert('Captcha tidak boleh kosong!');
		          return false;
		      }else if(inputCaptcha != coba){
		        alert('Captcha salah!');
		          return false;
		      }
		      else if(inputCaptcha == coba){
		        return true;
		      }
		    });
		    //end captcha

			var jvalidate = $("#login-form").validate({
				ignore: [],
				rules: {                                            
					username: {
						required: true
					},
					password: {
						required: true
					}
				},
				submitHandler: function(form) {
					let username = document.getElementById("username").value;
					let password = document.getElementById("password").value;
					if(/^[a-zA-Z0-9@.]*$/.test(username) == false || /^[a-zA-Z0-9@.]*$/.test(password) == false ){
					    alert('Cannot use special characters except "@" and "."');
		          		return false;
					}
					//document.write(format.test(username));
					var target = $(form).attr('action');
					$('#login-form .alert-warning').removeClass('hidden');
					$('#login-form .alert-success').addClass('hidden');
					$('#login-form .alert-danger').addClass('hidden');
					$.ajax({
						url : target,
						type : 'POST',
						dataType : 'json',
						data : $(form).serialize(),
						success : function(response){
							$('#login-form .alert-warning').addClass('hidden');
							if(response.status == 'ok'){
								$('#login-form .alert-success').removeClass('hidden').children('span').text(response.msg);
								window.location.href = response.redirect;
							}
							else
								$('#login-form .alert-danger').removeClass('hidden').children('span').text(response.msg);	
						},
						error : function(jqXHR, textStatus, errorThrown) {
							alert(textStatus, errorThrown);
						}
					});
				}
			});                                    
		</script>
		
	<!-- END SCRIPTS --> 
		
	</body>
</html>






