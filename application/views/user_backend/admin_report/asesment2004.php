<style>@media print{.head{display:none;}}</style>
<div class="grid-body">
	<table border="1" width="100%">
    <thead>
      <tr>
        <th style="width:5%">No</th>
        <th style="width:15%">Instalasi</th>
        <th style="width:5%">Nilai SS</th>
        <th style="width:15%">Nilai SO</th>
        <th style="width:15%">Nilai SP</th>
        <th style="width:15%">Nilai SN</th>
        <th style="width:15%">Nilai Akhir</th>
        <th>Tanggal</th>
      </tr>
    </thead>
    <tbody>
    	<?php

    	foreach($list as $key=> $val) {
            
    		echo "<tr>";
    		for($i=0;$i<=7;$i++) {
    			if($i==1) echo "<td style='vertical-align:top;text-align:right;'>".$val[$i]."</td>";
    			else if($i==2) echo "<td style='vertical-align:top;'>".$val[$i]."</td>";
    			else echo "<td style='vertical-align:top;text-align:center;'>".$val[$i]."</td>";
    		}
    		echo "</tr>";
    	}
    	?>
    </tbody>
  </table>
</div>