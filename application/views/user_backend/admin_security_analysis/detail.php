<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="user">Home</a></li>                    
	<li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                
	<div class="row">
	    <div class="col-md-12">
	        <div class="panel panel-default">
	            <div class="panel-body posts">
	            	<?php if ($security_analysis->num_rows() > 0) { ?>
	            	<div class="row">
	           			<?php $row = $security_analysis->row() ?>
       			            <div class="post-item">
                                <div class="post-title">
                                    <?php echo $row->title ?>
                                </div>
                                <div class="post-date"><span class="fa fa-calendar"></span> <?php echo $row->date_created ?> / oleh <?php echo $row->realname ?></div>
                                <div class="post-text">                                            
                                    <?php echo $row->content ?>
                                </div>
                                <div class="post-row">
                                    <div class="post-info">
                                        <span class="fa fa-eye"></span> <?php echo $row->views ?>                                             
                                    </div>
                                </div>
                            </div>
	           			
	           		</div>
	            	<?php } ?>
	            </div>
	        </div>
	        
	        <!-- <ul class="pagination pagination-sm pull-right push-down-20">
	            <li class="disabled"><a href="#">«</a></li>
	            <li class="active"><a href="#">1</a></li>
	            <li><a href="#">2</a></li>
	            <li><a href="#">3</a></li>
	            <li><a href="#">4</a></li>                                    
	            <li><a href="#">»</a></li>
	        </ul>  -->                          
	        
	    </div>   
	    <!--<div class="col-md-3">
	        <div class="panel panel-default">
	            <div class="panel-body">
	                <h3>Categories</h3>
	                <div class="links">
	                    <a href="#">Front-end <span class="label label-default">195</span></a>
	                    <a href="#">Back-end <span class="label label-default">278</span></a>
	                    <a href="#">Frameworks <span class="label label-default">48</span></a>
	                    <a href="#">Programming <span class="label label-default">311</span></a>
	                    <a href="#">Management <span class="label label-default">94</span></a>
	                </div>
	            </div>
	        </div>
	        <div class="panel panel-default">
	            <div class="panel-body">
	                <h3>Recent</h3>
	                <div class="links small">
	                    <a href="#">Vestibulum porttitor neque vitae odio vulputate molestie</a>
	                    <a href="#">Etiam tellus mi, interdum id nulla in</a>
	                    <a href="#">Cras eu tincidunt quam</a>
	                    <a href="#">Donec rhoncus quam tortor, id pulvinar erat elementum eu</a>
	                    <a href="#">Nunc lorem est, suscipit bibendum</a>
	                    <a href="#">Fusce sollicitudin quis turpis eget mollis</a>
	                    <a href="#">Cras eget sagittis dui, et mollis tortor</a>
	                </div>
	            </div>
	        </div>
	        
	        <div class="panel panel-default">
	            <div class="panel-body">
	                <h3>Tags</h3>
	                <ul class="list-tags push-up-10">
	                    <li><a href="#"><span class="fa fa-tag"></span> Donec</a></li>
	                    <li><a href="#"><span class="fa fa-tag"></span> Bibendum</a></li>
	                    <li><a href="#"><span class="fa fa-tag"></span> Praesent</a></li>
	                    <li><a href="#"><span class="fa fa-tag"></span> Fusce</a></li>
	                    <li><a href="#"><span class="fa fa-tag"></span> Nam quis</a></li>
	                    <li><a href="#"><span class="fa fa-tag"></span> Maecenas</a></li>
	                    <li><a href="#"><span class="fa fa-tag"></span> Nulla facilisi</a></li>
	                    <li><a href="#"><span class="fa fa-tag"></span> Donec</a></li>
	                    <li><a href="#"><span class="fa fa-tag"></span> Nam vitae</a></li>
	                    <li><a href="#"><span class="fa fa-tag"></span> Malesuada</a></li>
	                </ul>
	            </div>
	        </div>  
	    </div>
	-->
	</div>
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- MESSAGE BOX -->
<div id="px-security-analysis-message-box" class="message-box message-box-warning animated fadeIn fade">
	<div class="mb-container">
		<div class="mb-middle">
			<form action="<?php echo $controller.'/'.$function_delete; ?>" method="post" id="px-security-analysis-message-form">
			<input type="hidden" name="id">
			<div class="mb-title"><span class="fa fa-warning"></span> Warning</div>
			<div class="mb-content">
				<p>Are you sure you want to delete this data?</p>
				<p class="msg-status"></p>                  
			</div>
			<div class="mb-footer">
				<button class="btn btn-danger btn-lg pull-right" type="submit">Delete</button>
				<button class="btn btn-default btn-lg pull-right mb-control-close" type="button">Cancel</button>
			</div>
			</form>
		</div>
	</div>
</div>
<!-- EOF MESSAGE BOX -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>   
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page_user/security_analysis/security_analysis.js"></script>
	<!--  -->
<!-- END SCRIPTS -->   