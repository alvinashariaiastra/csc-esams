<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                
	<div class="row">
		<div class="col-md-12">
			<div class="modal fade animated" id="px-dashboard-status-kerawanan-new-modal" tabindex="-1" role="dialog" aria-labelledby="px-dashboard-status-kerawanan-new-modal-label" aria-hidden="true">
				<div class="modal-dialog modal-md">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							<h4 class="modal-title" id="px-dashboard-status-kerawanan-new-modal-label">Berita Terkini</h4>
						</div>
						<div class="modal-body list-group">
							
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade animated" id="px-dashboard-status-kerawanan-news-detail-modal" tabindex="-1" role="dialog" aria-labelledby="px-dashboard-status-kerawanan-news-detail-modal-label" aria-hidden="true">
				<div class="modal-dialog modal-md">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							<h4 class="modal-title" id="px-dashboard-status-kerawanan-news-detail-modal-label">Berita Detil</h4>
						</div>
						<div class="modal-body">
							<h4></h4>
							<div class="content">
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<label>History Status Kerawanan <?php echo $this->session_user['name_instalasi']; ?></label>
						</div>
						<div class="col-md-12">
							<label class="label-form label label-success">Hijau : <?php echo $status_hijau; ?></label>
							<label class="label-form label label-warning">Kuning : <?php echo $status_kuning; ?></label>
							<label class="label-form label label-danger">Merah : <?php echo $status_merah; ?></label>
						</div>
						<div class="col-md-12">
							&nbsp;
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div id="peta-status-kerawanan" style="width: 100%; height: 400px"></div>
						</div>
					</div>
				</div>
			</div> 
		</div>                               
	</div>
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- MESSAGE BOX -->
<div id="px-security-analysis-message-box" class="message-box message-box-warning animated fadeIn fade">
	<div class="mb-container">
		<div class="mb-middle">
			<form action="<?php echo $controller.'/'.$function_delete; ?>" method="post" id="px-security-analysis-message-form">
			<input type="hidden" name="id">
			<div class="mb-title"><span class="fa fa-warning"></span> Warning</div>
			<div class="mb-content">
				<p>Are you sure you want to delete this data?</p>
				<p class="msg-status"></p>                  
			</div>
			<div class="mb-footer">
				<button class="btn btn-danger btn-lg pull-right" type="submit">Delete</button>
				<button class="btn btn-default btn-lg pull-right mb-control-close" type="button">Cancel</button>
			</div>
			</form>
		</div>
	</div>
</div>
<!-- EOF MESSAGE BOX -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>   
	<script type='text/javascript' src='<?=base_url()?>assets/backend_assets/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'></script>
	<script type='text/javascript' src='<?=base_url()?>assets/backend_assets/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'></script>  
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page_user/status_kerawanan/peta.js"></script>
	<!--  -->
<!-- END SCRIPTS -->   