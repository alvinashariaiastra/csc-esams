<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?> <?php echo $data->nama_instalasi; ?> Periode <?php echo $data->periode; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
	            <div class="panel-body posts">
	            	<div class="row">
   			            <div class="post-item">
                            <div class="post-text">
                            	<h3>Rekap Nilai</h3>
                            	<table class="table table-bordered">
									<thead>
										<tr>
											<?php foreach ($rules as $r) { ?>
											<th width="10%" class="text-center"><?php echo $r->name; ?></th>
											<?php } ?>
											<th width="10%" class="text-center">Pemenuhan System</th>
											<th width="10%" class="text-center">Security Performance</th>
											<th width="10%" class="text-center">Security Reliability</th>
											<th width="10%" class="text-center">CSI</th>
											<th width="10%" class="text-center">Nilai Akhir</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<?php foreach ($rules as $r) { ?>
											<td class="text-center"><?php echo $r->value; ?></td>
											<?php } ?>
											<td class="text-center"><?php echo $data->pemenuhan_system; ?></td>
											<td class="text-center"><?php echo $data->security_performance; ?></td>
											<td class="text-center"><?php echo $data->security_reliability; ?></td>
											<td class="text-center"><?php echo $data->csi; ?></td>
											<td class="text-center"><?php echo $data->nilai_akhir; ?></td>
										</tr>
									</tbody>
								</table> 
								<h3>Rincian Critical Point</h3>                                        
                                <p><?php echo $data->critical_point; ?></p>
                                <?php if (count($history_revise)>0) { ?>
                                	<br/><h3>History Revise</h3>
									<?php foreach ($history_revise as $row) { ?>   
										<small><?php echo date('M, d Y, (H:i)', strtotime($row->date_created)); ?></small> 
                                		<div style="background-color: #f9f9f9; padding: 10px;"><?php echo $row->revise; ?></div>
									<?php } ?>                                     
								<?php } ?>  
							</div>
                        </div>
	           		</div>
	            </div>
	        </div> 
		</div>
	</div>                                
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>   
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
<!-- END SCRIPTS -->   