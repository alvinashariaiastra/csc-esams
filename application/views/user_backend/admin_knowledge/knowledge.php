<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                
	<div class="row">
	    <div class="col-md-12">
	        <div class="panel panel-default">
	            <div class="panel-body posts">
	            	<?php 
	            	$i = 1;
	            	if ($knowledge->num_rows() > 0) { ?>
	           			<?php foreach ($knowledge->result() as $row) { 
	           				if ($i%2 > 0 || $i == 1) {
	           					echo "<div class='row'>";
	           				}
	           			?>
	           			<div class="col-md-6">
	                        <div class="post-item">
	                            <div class="post-title">
	                                <a href="user_knowledge/detail/<?php echo $row->id ?>"><?php echo $row->name ?></a>
	                            </div>
	                            <div class="post-date"><span class="fa fa-calendar"></span> <?php echo $row->date_created ?></div>
	                            <div class="post-text">
	                                <p><?php echo $row->content ?></p>                                            
	                            </div>
	                            <div class="post-row">
	                            <a href="user_knowledge/download_file/<?php echo $row->id.'/'.$row->file ?>"><label class="btn btn-primary btn-download pull-left">Download</label></a>
	                            <a href="user_knowledge/detail/<?php echo $row->id ?>"><button class="btn btn-default btn-rounded pull-right">Lebih lanjut &RightArrow;</button></a>
	                            </div>
	                        </div>
	                    </div>
	           			<?php
	           			if ($i%2 == 0 || $i == 2) {
           					echo "</div>";
           				}
	           			$i++;	
	           			} ?>
	            	<?php } ?>
	            </div>
	        </div>                        
	    </div>
	</div>
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- MESSAGE BOX -->
<div id="px-knowledge-knowledge-message-box" class="message-box message-box-warning animated fadeIn fade">
	<div class="mb-container">
		<div class="mb-middle">
			<form action="<?php echo $controller.'/'.$function_delete; ?>" method="post" id="px-knowledge-knowledge-message-form">
			<input type="hidden" name="id">
			<div class="mb-title"><span class="fa fa-warning"></span> Warning</div>
			<div class="mb-content">
				<p>Are you sure you want to delete this data?</p>
				<p class="msg-status"></p>                  
			</div>
			<div class="mb-footer">
				<button class="btn btn-danger btn-lg pull-right" type="submit">Delete</button>
				<button class="btn btn-default btn-lg pull-right mb-control-close" type="button">Cancel</button>
			</div>
			</form>
		</div>
	</div>
</div>
<!-- EOF MESSAGE BOX -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>   
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page_user/knowledge/knowledge.js"></script>
	<!--  -->
<!-- END SCRIPTS -->   