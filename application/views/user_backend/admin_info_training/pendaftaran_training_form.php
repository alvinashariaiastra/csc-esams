<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
	<li><a href="<?php echo $controller.'/'.$function; ?>"><?php echo $function_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?> Form</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Data Training</h3>                              
                </div>
                <form class="form-horizontal" id="px-pendaftaran-training-form" method="POST" action="<?php if ($training) echo $controller . '/pendaftaran_training_add';
else echo $controller . '/pendaftaran_training_add'; ?>">
                    <input type="hidden" value="<?php if ($training != null) echo $training->id; ?>" name="training_info_id">
                    <div class="panel-body">
                        <?php echo $this->session->flashdata('success'); ?>
                        <div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
<?php echo $this->session->flashdata('failed'); ?>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-pendaftaran-training-form-pic">PIC</label>
                            <div class="col-md-6 col-xs-12">
                                <span class="form-control" name="pic" id="px-pendaftaran-training-form-pic"><?php if ($training != null) echo $training->realname; ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-pendaftaran-training-form-title">Judul</label>
                            <div class="col-md-6 col-xs-12">
                                <span class="form-control" name="title" id="px-pendaftaran-training-form-title"><?php if ($training != null) echo $training->title; ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Pendaftaran Dibuka</label>
                            <div class="col-md-5">
                                <div class="input-group" >
                                    <span class="datepicker form-control" name="pendaftaran_start"><?php if ($training != null) echo date('d-m-Y', strtotime($training->pendaftaran_start)); ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Pendaftaran Ditutup</label>
                            <div class="col-md-5">
                                <div class="input-group" >
                                    <span class="datepicker form-control" name="pendaftaran_end"><?php if ($training != null) echo date('d-m-Y', strtotime($training->pendaftaran_end)); ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Training Dimulai</label>
                            <div class="col-md-5">
                                <div class="input-group" >
                                    <span class="datepicker form-control" name="date_start"><?php if ($training != null) echo date('d-m-Y', strtotime($training->date_start)); ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Training Selesai</label>
                            <div class="col-md-5">
                                <div class="input-group" >
                                    <span class="datepicker form-control" name="date_end"><?php if ($training != null) echo date('d-m-Y', strtotime($training->date_end)); ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-heading">                                
                        <h3 class="panel-title">Form Pendaftaran Peserta</h3>                              
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-pendaftaran-training-form-name">Nama Peserta</label>
                            <div class="col-md-6 col-xs-12">
                                <input type="text" class="form-control" name="name" id="px-pendaftaran-training-form-name" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-pendaftaran-training-form-company">Perusahaan</label>
                            <div class="col-md-6 col-xs-12">
                                <input type="text" class="form-control" name="company" id="px-pendaftaran-training-form-company" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-pendaftaran-training-form-position">Jabatan</label>
                            <div class="col-md-6 col-xs-12">
                                <input type="text" class="form-control" name="position" id="px-pendaftaran-training-form-position" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-pendaftaran-training-form-phone">No. Handphone</label>
                            <div class="col-md-6 col-xs-12">
                                <input type="text" class="form-control" name="phone" id="px-pendaftaran-training-form-phone" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-pendaftaran-training-form-email">Email</label>
                            <div class="col-md-6 col-xs-12">
                                <input type="text" class="form-control" name="email" id="px-pendaftaran-training-form-email" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-pendaftaran-training-form-supervisor_name">Nama Atasan</label>
                            <div class="col-md-6 col-xs-12">
                                <input type="text" class="form-control" name="supervisor_name" id="px-pendaftaran-training-form-supervisor_name" required/>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">                                  
                        <span class="text-danger pull-left">*Nama yang sudah terdaftar tidak dapat berubah</span>
                        <button class="btn btn-primary pull-right" type="submit">Submit</button>
                    </div>
                </form>
            </div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                                
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>  
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>    
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page_user/info_training/info_form.js"></script>
	<!--  -->
<!-- END SCRIPTS -->   