<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="user">Home</a></li>                    
	<li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                
	<div class="row">
	    <div class="col-md-12">
	        <div class="panel panel-default">
	            <div class="panel-body posts">
	            	<div class="row">
   			            <div class="post-item">
                            <div class="post-title">
                                <?php echo $info_training->title; ?>
                            </div>
                            <div class="post-date"><span class="fa fa-calendar"></span> Pelaksanaan Training <?php echo date('d/m/Y',strtotime($info_training->date_start)); ?> - <?php echo date('d/m/Y',strtotime($info_training->date_end)); ?></div>
                         	<div class="post-date"><span class="fa fa-calendar"></span> Pendaftaran Training <?php echo date('d/m/Y',strtotime($info_training->pendaftaran_start)); ?> - <?php echo date('d/m/Y',strtotime($info_training->pendaftaran_end)); ?></div>
                            <div class="post-text">                                            
                                <p>
                                <?php echo $info_training->content; ?>
                                </p>
                                <?php if(strtotime(date('Y-m-d',now())) <= strtotime($info_training->pendaftaran_end) && strtotime(date('Y-m-d',now())) >= strtotime($info_training->pendaftaran_start)) { ?>
                                <a class="btn btn-success" href="user_info_training/pendaftaran_training_form/<?php echo $info_training->id; ?>">Daftar Training</a>
                                <?php } else { ?>
                                <button class="btn btn-danger">Pendaftaran Telah Ditutup</button>
                                <?php } ?>
							</div>
                            <div class="post-row">
                                <div class="post-info">
                                    <span class="fa fa-eye"></span>                                            
                                </div>
                            </div>
                        </div>
	           		</div>
	            </div>
	        </div>                       
	    </div>
	</div>
</div>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>   
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>

<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script> 