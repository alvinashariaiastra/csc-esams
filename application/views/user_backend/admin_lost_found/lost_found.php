<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="user">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
    <a class="btn btn-success pull-right btn-add" href="<?php echo $controller . '/manage_lost_found'; ?>">Manage Your Data</a>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body posts">
                    <?php
                    $i = 1;
                    if ($lost_found->num_rows() > 0) {
                        ?>
                        <?php
                        foreach ($lost_found->result() as $row) {
                            if ($i % 2 > 0 || $i == 1) {
                                echo "<div class='row'>";
                            }
                            ?>
                            <div class="col-md-6">
                                <div class="post-item">
                                    <div class="post-title">
                                        <a href="user_lost_found/detail/<?php echo $row->id ?>"><?php echo $row->title ?> (<?php if ($row->type == 1) echo 'LOST';
                            else echo 'FOUND'; ?>)</a>
                                    </div>
                                    <div class="post-date"><span class="fa fa-calendar"></span> <?php echo $row->date_created ?> / oleh <?php echo $row->realname ?></div>
                                    <div class="post-image">
                                        <a href="user_lost_found/detail/<?php echo $row->id ?>"><img src="<?php echo $row->photo; ?>" /></a>
                                    </div>
                                    <div class="post-text">
                                        <p><?php echo $row->detail ?></p>                                            
                                    </div>
                                    <div class="post-row">
                                        <a href="user_lost_found/detail/<?php echo $row->id ?>"><button class="btn btn-default btn-rounded pull-right">Lebih lanjut &RightArrow;</button></a>
                                    </div>
                                </div>
                            </div>
                            <?php
                            if ($i % 2 == 0 || $i == 2) {
                                echo "</div>";
                            }
                            $i++;
                        }
                        ?>
<?php } ?>
                </div>                      

            </div>
        </div>
    </div>
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->               
<!-- THIS PAGE PLUGINS -->
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>   
<!-- END PAGE PLUGINS -->
<!-- START TEMPLATE -->
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>

<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
<!-- END TEMPLATE -->  
<!-- THIS PAGE JS SETTINGS -->
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page_user/lost_found/lost_found.js"></script>
<!--  -->
<!-- END SCRIPTS -->   