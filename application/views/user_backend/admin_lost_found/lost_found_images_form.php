<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li><a href="<?php echo $controller . '/' . $function; ?>"><?php echo $function_name; ?></a></li>
    <li class="active"><?php echo $function_name; ?> Form</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">

            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Form Input Image <?php echo $lost_found->title; ?></h3>                              
                </div>
                <form class="form-horizontal" id="px-admin-lost_found-lost_found_images-form" method="POST" action="<?php if ($data) echo $controller . '/lost_found_images_edit/';
else echo $controller . '/lost_found_images_add/' ?>">
                    <input type="hidden" value="<?php if ($data != null) echo $data->id; ?>" name="id" id="px-admin-lost_found-lost_found_images-form-lost_found_pict_id">
                    <input type="hidden" value="<?php echo $lost_found->id ?>" name="lost_found_id" id="px-admin-lost_found-lost_found_images-form-lost_found_log_id">
                    <div class="panel-body">
                        <div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
                        <div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
                        <div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label">Photo</label>
                            <div class="col-md-6 col-xs-12">  
                                <input type="hidden" name="old_photo" value="<?php if ($data != null) echo $data->photo; ?>">
                                <input type="hidden" name="photo">                                                                       
                                <label for="file-upload-file" class="btn btn-primary btn-upload" data-target="photo">Browse</label>
                            </div>
                            <div class="gallery col-md-2 col-xs-12 col-md-offset-2 no-padding" id="preview_photo">

                                <div class="image">
                                    <img src="<?=base_url()?>assets/uploads/lost_found/<?php if ($data != null) echo $data->lost_found_id . '/' . $data->photo; ?>" alt="Photo"/>                                                                                                           
                                </div>                               

                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button class="btn btn-primary pull-right" type="submit">Save</button>
                    </div>
                </form>
            </div>
            <!-- END DEFAULT DATATABLE -->

        </div>
    </div>                                

</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- FORM UPLOAD -->
<form id="file-upload" action="upload/image" method="POST" enctype="multipart/form-data" class="hidden">
    <input type="hidden" name="target" id="target-file">
    <input type="hidden" name="old" id="old-file">
    <input type="file" name="image" id="file-upload-file">
</form>
<!-- EOF FORM UPLOAD -->

<!-- START SCRIPTS -->               
<!-- THIS PAGE PLUGINS -->
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/bootstrap/bootstrap-file-input.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>  
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/fileupload/fileupload.min.js"></script>  
<!-- END PAGE PLUGINS -->
<!-- START TEMPLATE -->
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>

<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
<!-- END TEMPLATE -->  
<!-- THIS PAGE JS SETTINGS -->
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page_user/lost_found/lost_found_images_form.js"></script>
<!--  -->
<!-- END SCRIPTS -->   