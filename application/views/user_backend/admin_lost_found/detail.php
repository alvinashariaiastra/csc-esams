<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="user">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li class="active"><?php echo $function_name; ?></li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body posts">
                    <?php if ($lost_found->num_rows() > 0) { ?>
                        <div class="row">
                            <?php $row = $lost_found->row() ?>
                            <div class="post-item">
                                <div class="post-title">
                                    <?php echo $row->title ?>
                                </div>
                                <div class="post-date"><span class="fa fa-calendar"></span> <?php echo $row->date_created ?> / oleh <?php echo $row->realname ?></div>
                                <div class="row">
                                    <?php
                                    if($row->photos)
                                    {
                                    $cur_slide = 1;
                                    foreach ($row->photos->result() as $pict) {
                                        ?>
                                        <img src="<?=base_url()?>assets/uploads/lost_found/<?php echo $row->id . '/thumb' . $pict->photo ?>" style="width:200px; <?php if ($cur_slide > 1) echo "display:none;"; ?>" onclick="openModal();currentSlide(<?php echo $cur_slide ?>)" class="hover-shadow cursor" data-toggle="modal" data-target="#myModal">
                                        <?php $cur_slide++;
                                    }
                                    }
                                    ?>
                                    <br><br>*Click to view more photos
                                </div>
                                <div class="post-text">                                            
    <?php echo $row->detail ?>
                                </div>
                            </div>

                        </div>
<?php } ?>
                </div>
            </div>                         

        </div>   
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Comments</h3>
                    </div>
                </div>
                <div class="col-md-12">
                    <?php if(isset($_GET['submit_comments'])) { if($_GET['submit_comments'] == 'success') { ?>
                    <div class="alert alert-success"><strong>Komentar berhasil diinput ! </strong><span></span></div>
                    <?php } else { ?>
                    <div class="alert alert-danger"><strong>Input Gagal! </strong><span></span></div>
                    <?php }} ?>
                    <div class="panel-body">
                        <?php foreach($row->comments as $data_row) { ?>
                        <div class="list-group-item">
                            <div class="post-date"><span class="fa fa-calendar"></span> <?php echo $data_row->date_created ?> / oleh <?php echo $data_row->realname ?></div>
                            <?php echo $data_row->comments; ?>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3>Put your comments here</h3>
                    </div>
                </div>
                <div class="col-md-12">
                    <form action="user_lost_found/submit_comments" method="POST">
                        <input type="hidden" name="lost_found_id" value="<?php echo $row->id ?>" />
                        <div class="panel-body">
                                <div class="form-group">
                                    <div class="col-md-12 col-xs-12">
                                        <textarea class="form-control" name="comments" id="px-lost_found-form-comments" required></textarea>
                                    </div>
                                </div>
                        </div>
                        <div class="panel-body">
                            <button class="btn btn-primary pull-right" type="submit">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- PICTURE MODAL -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" onclick="closeModal()">&times;</button>
                <h4 class="modal-title">Activity Photos</h4>
            </div>
            <div class="modal-body">
                <?php
                if($row->photos)
                {
                $total = count($row->photos->result());
                $count = 1;
                foreach ($row->photos->result() as $pict) {
                    ?>
                    <div class="mySlides">
                        <div class="numbertext"><?php echo $count ?> / <?php echo $total ?></div>
                        <img src="<?=base_url()?>assets/uploads/lost_found/<?php echo $row->id . '/' . $pict->photo ?>" style="width:100%;">
                    </div>
                    <?php $count++;
                }
                }
                ?>

                <a class="prev2" onclick="plusSlides(-1)">&#10094;</a>
                <a class="next2" onclick="plusSlides(1)">&#10095;</a>

                <div class="caption-container">
                    <p id="caption"></p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- END PICTURE MODAL -->

<!-- START SCRIPTS -->     
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/backend_assets/page_user/lost_found/pictures_popup.css"></link>
<!-- THIS PAGE PLUGINS -->
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>   
<!-- END PAGE PLUGINS -->
<!-- START TEMPLATE -->
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>

<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>   
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page_user/lost_found/pictures_popup.js"></script>
<!-- END TEMPLATE -->  
<!-- THIS PAGE JS SETTINGS -->
<!--  -->
<!-- END SCRIPTS -->   