<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li><a href="<?php echo $controller . '/' . $function; ?>"><?php echo $function_name; ?></a></li>
    <li class="active"><?php echo $function_name; ?> Form</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">

            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Search Lost & Found</h3> 
        <a class="btn btn-success pull-right btn-add" href="<?php echo $controller.'/manage_lost_found'; ?>">Manage Your Data</a>                             
                </div>
                <form class="form-horizontal" id="px-lost_found-form" method="POST" action="user_lost_found/lost_found">
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-lost_found-form-search">Search </label>
                            <div class="col-md-9 col-xs-12">
                                <input type="text" class="form-control" name="search" id="px-lost_found-form-search" value="" placeholder="Misal : Kacamata, Handphone, etc" required>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button class="btn btn-primary pull-right" type="submit"><i class="fa fa-search"></i> Search</button>
                    </div>
                </form>
            </div>
            <!-- END DEFAULT DATATABLE -->

        </div>
    </div>                                

</div>
<div class="page-content-wrap">                
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body posts">
                    <?php
                    $i = 1;
                    if ($lost_found->num_rows() > 0) {
                        ?>
                        <?php
                        foreach ($lost_found->result() as $row) {
                            if ($i % 4 > 0 || $i == 1) {
                                echo "<div class='row'>";
                            }
                            ?>
                            <div class="col-md-3">
                                <div class="post-item">
                                    <div class="post-title">
                                        <a href="user_lost_found/detail/<?php echo $row->id ?>"  style="font-size:15px;"><?php echo $row->title ?> (<?php if ($row->type == 1) echo 'LOST';
                            else echo 'FOUND'; ?>)</a>
                                    </div>
                                    <div class="post-date"><span class="fa fa-calendar"></span> <?php echo $row->date_created ?> / oleh <?php echo $row->realname ?></div>
                                    <div class="post-image">
                                        <a href="user_lost_found/detail/<?php echo $row->id ?>"><img src="<?php echo $row->photo; ?>" style="width:200px"/></a>
                                    </div>
                                    <div class="post-text">
                                        <p><?php echo $row->detail ?></p>                                            
                                    </div>
                                    <div class="post-row">
                                        <a href="user_lost_found/detail/<?php echo $row->id ?>"><button class="btn btn-default btn-rounded pull-right">Lebih lanjut &RightArrow;</button></a>
                                    </div>
                                </div>
                            </div>
                            <?php
                            if ($i % 4 == 0 || $i == 4) {
                                echo "</div>";
                            }
                            $i++;
                        }
                        ?>
<?php } ?>
                </div>                      

            </div>
        </div>
    </div>
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->               
<!-- THIS PAGE PLUGINS -->
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>  
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>    
<!-- END PAGE PLUGINS -->
<!-- START TEMPLATE -->
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>

<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
<!-- END TEMPLATE -->
<!-- END SCRIPTS -->   