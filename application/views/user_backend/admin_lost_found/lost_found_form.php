<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="admin">Home</a></li>                    
    <li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
    <li><a href="<?php echo $controller . '/' . $function; ?>"><?php echo $function_name; ?></a></li>
    <li class="active"><?php echo $function_name; ?> Form</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
    <h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

    <div class="row">
        <div class="col-md-12">

            <!-- START DEFAULT DATATABLE -->
            <div class="panel panel-default">
                <div class="panel-heading">                                
                    <h3 class="panel-title">Form</h3>                              
                </div>
                <form class="form-horizontal" id="px-lost_found-form" method="POST" action="<?php if ($data) echo $controller . '/lost_found_edit';
else echo $controller . '/lost_found_add' ?>">
                    <input type="hidden" value="<?php if ($data != null) echo $data->id; ?>" name="id">
                    <div class="panel-body">
                        <div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
                        <div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
                        <div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-lost_found-form-type">Tipe</label>
                            <div class="col-md-9 col-xs-12">
                                <select class="form-control" name="type" id="px-lost_found-form-type" required>
                                    <option <?php if ($data != null) {
                                        if ($data->type == 1) echo 'selected';
                                    } ?> value="1">Lost</option>
                                    <option <?php if ($data != null) {
                                        if ($data->type == 2) echo 'selected';
                                    } ?> value="2">Found</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-lost_found-form-item_type">Barang</label>
                            <div class="col-md-9 col-xs-12">
                                <input type="text" class="form-control" name="item_type" id="px-lost_found-form-item_type" value="<?php if ($data != null) echo $data->item_type; ?>" placeholder="Misal : Kacamata, Handphone, etc" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-lost_found-form-color">Warna Barang</label>
                            <div class="col-md-9 col-xs-12">
                                <input type="text" class="form-control" name="color" id="px-lost_found-form-color" value="<?php if ($data != null) echo $data->color; ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-lost_found-form-title">Judul Posting</label>
                            <div class="col-md-9 col-xs-12">
                                <input type="text" class="form-control" name="title" id="px-lost_found-form-title" value="<?php if ($data != null) echo $data->title; ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 col-xs-12 control-label" for="#px-lost_found-form-detail">Detail</label>
                            <div class="col-md-9 col-xs-12">
                                <textarea class="form-control" name="detail" id="px-lost_found-form-detail" required><?php if ($data != null) echo $data->detail; ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button class="btn btn-primary pull-right" type="submit">Save</button>
                    </div>
                </form>
            </div>
            <!-- END DEFAULT DATATABLE -->

        </div>
    </div>                                

</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->               
<!-- THIS PAGE PLUGINS -->
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>  
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>    
<!-- END PAGE PLUGINS -->
<!-- START TEMPLATE -->
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>

<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
<!-- END TEMPLATE -->  
<!-- THIS PAGE JS SETTINGS -->
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page_user/lost_found/lost_found_form.js"></script>
<!--  -->
<!-- END SCRIPTS -->   