<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
	<li><a href="<?php echo $controller.'/'.$function; ?>"><?php echo $function_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?> Form</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">                                
					<h3 class="panel-title">Form Standar Kompetensi</h3>                              
				</div>
				<div class="panel-body">
				<div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
				<div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
				<div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
			    <form class="form-horizontal" id="px-standar-kompetensi-form" method="POST" action="<?php if($data) echo $controller.'/'.$function_edit; else echo $controller.'/'.$function_add; ?>">	
			    <?php 
			    	if($data)
			    		$readonly = 'readonly';
			    	else
			    		$readonly = ' ';
			    ?>
			    <input type="hidden" name="id" value="<?php if($data) echo $data->id ?>">   
			        <div>
			            <h3>Pilih Karyawan</h3>
			            <section>
			            	<div class="form-group">
								<label class="col-md-2 col-xs-12 control-label" for="#px-standarkompetensi-form-standarkompetensi-perusahaan">Perusahaan</label>
								<div class="col-md-6 col-xs-12">
									<select readonly="" class="form-control" name="perusahaan_id" id="px-standarkompetensi-form-standarkompetensi-perusahaan">
										<option value="<?php echo $perusahaan->id; ?>" selected=""><?php echo $perusahaan->name; ?></option>
									</select>
								</div>
							</div>
				            <div class="form-group">
								<label class="col-md-2 col-xs-12 control-label" for="#px-standarkompetensi-form-standarkompetensi-instalasi">Instalasi</label>
								<div class="col-md-6 col-xs-12">
									<select readonly="" class="form-control" name="instalasi" id="px-standarkompetensi-form-standarkompetensi-instalasi">
										<option value="<?php echo $instalasi->id; ?>" selected=""><?php echo $instalasi->name; ?></option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 col-xs-12 control-label" for="#px-standarkompetensi-form-standarkompetensi-user">User</label>
								<div class="col-md-6 col-xs-12">
									<select <?php echo $readonly; ?> class="form-control" name="user_id" id="px-standarkompetensi-form-standarkompetensi-user">
										<option value="0">-- Pilih User --</option>
										<?php 
										if ($user) {
											foreach ($user as $data_row) {
												if ($data!=null && $data->user_id == $data_row->id) {
														$selected = "selected";
													}else{
														$selected = "";
													}
												?>
												<option value="<?php echo $data_row->id ?>"<?php echo $selected ?> data-level="<?php echo $data_row->level_user_id; ?>"><?php echo $data_row->realname ?></option>
										<?php
											}
										}
										 ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-2 col-xs-12 control-label" for="#px-standarkompetensi-form-standarkompetensi-finalscore">Final Score</label>
								<div class="col-md-6 col-xs-12">
									<div class="input-group">
										<input readonly type="text" class="form-control" name="final_score" id="px-standarkompetensi-form-standarkompetensi-finalscore" value="<?php if($data!=NULL) echo $data->final_score ?>">
										<div class="input-group-addon">%</div>
									</div>
								</div>
							</div>
			            </section>
			            <!-- STC-1 -->
			            <h3>STC-1</h3>
			            <section data-section="section-stc1">
			                <table class="table table-bordered table-score">
			                	<thead></thead>
			                	<tbody>
			                		<tr>
			                			<th class="text-center">No</th>
			                			<th class="text-center">Panduan</th>
			                			<th class="text-center">Level User</th>
			                			<th class="text-center">Rules</th>
			                			<th colspan="4" class="text-center">Score</th>
			                		</tr>
			                		<?php 
			                		$i = 1;
			                		if ($stc1->num_rows() > 0) {
			                			foreach ($stc1->result() as $row) { 
			                				if($data){
				                				$key = array_search($row->id, array_column($result_stc1, 'standar_kompetensi_id'));
				                				if (count($key) >= 0) {
				                					$user_score = $result_stc1[$key]['score'];
					                				$checked1 = $checked2 = $checked3 = $checked4 = "";
					                				if ($user_score == 1) {
					                					$checked1 = "checked";
					                				}elseif ($user_score == 2) {
					                					$checked2 = "checked";
					                				}elseif ($user_score == 3) {
					                					$checked3 = "checked";
					                				}elseif ($user_score == 4) {
					                					$checked4 = "checked";
					                				}
				                				}else{
				                					$checked1 = $checked2 = $checked3 = $checked4 = "";
				   								}	
				   							}
				   							else
				   								$checked1 = $checked2 = $checked3 = $checked4 = "";
			                			?>
			                		<tr>
			                			<td><?php echo $i ?></td>
			                			<td class="text-center">
										<?php if($row->document->row()) { ?>
										<a href="<?=base_url()?>assets/uploads/list_rules/<?php echo $row->id.'/'.$row->document->row()->file; ?>"><?php echo $row->document->row()->file; ?></a>
										<?php } else { echo '-'; } ?>
										</td>
			                			<td class="text-center"><?php 
										if($row->level_user_id == 1)
											echo 'Management';
										elseif ($row->level_user_id == 2)
											echo 'Middle Management';
										elseif ($row->level_user_id == 3)
											echo 'Operational'; 
										?></td>
			                			<td><?php echo $row->rules ?></td>
			                			<td><input type="radio" name="stc1_score<?php echo $i ?>" value="1" <?php echo $checked1 ?> class="stc-score-1 pull-left"><label class="pull-left">1</label></td>
			                			<td><input type="radio" name="stc1_score<?php echo $i ?>" value="2" <?php echo $checked2 ?> class="stc-score-1 pull-left"><label class="pull-left">2</label></td>
			                			<td><input type="radio" name="stc1_score<?php echo $i ?>" value="3" <?php echo $checked3 ?> class="stc-score-1 pull-left"><label class="pull-left">3</label></td>
			                			<td><input type="radio" name="stc1_score<?php echo $i ?>" value="4" <?php echo $checked4 ?> class="stc-score-1 pull-left"><label class="pull-left">4</label></td>
			                		</tr>
			                		<?php $i++; 
			                		} ?>
			                		<?php }else{ ?>
			                		<tr><td colspan="6" class="text-center">Tidak ada rules.</td></tr>
			                		<?php } ?>
			                	</tbody>
			                </table>
			            </section>
			            <!-- STC-2 -->
			            <h3>STC-2</h3>
			            <section data-section="section-stc2">
			                <table class="table table-bordered table-score">
			                	<thead></thead>
			                	<tbody>
			                		<tr>
			                			<th class="text-center">No</th>
			                			<th class="text-center">Rules</th>
			                			<th colspan="4" class="text-center">Score</th>
			                		</tr>
			                		<?php 
			                		$i = 1;
			                		if ($stc2->num_rows() > 0) {
			                			foreach ($stc2->result() as $row) {
			                				if($data){
				                				$key = array_search($row->id, array_column($result_stc2, 'standar_kompetensi_id'));
				                				if (count($key) >= 0) {
				                					$user_score = $result_stc2[$key]['score'];
					                				$checked1 = $checked2 = $checked3 = $checked4 = "";
					                				if ($user_score == 1) {
					                					$checked1 = "checked";
					                				}elseif ($user_score == 2) {
					                					$checked2 = "checked";
					                				}elseif ($user_score == 3) {
					                					$checked3 = "checked";
					                				}elseif ($user_score == 4) {
					                					$checked4 = "checked";
					                				}
				                				}else{
				                					$checked1 = $checked2 = $checked3 = $checked4 = "";
				   								}
				   							} 
				   							else
				   								$checked1 = $checked2 = $checked3 = $checked4 = "";
			                			?>
			                		<tr>
			                			<td><?php echo $i ?></td>
			                			<td class="text-center">
										<?php if($row->document->row()) { ?>
										<a href="<?=base_url()?>assets/uploads/list_rules/<?php echo $row->id.'/'.$row->document->row()->file; ?>"><?php echo $row->document->row()->file; ?></a>
										<?php } else { echo '-'; } ?>
										</td>
			                			<td class="text-center"><?php 
										if($row->level_user_id == 1)
											echo 'Management';
										elseif ($row->level_user_id == 2)
											echo 'Middle Management';
										elseif ($row->level_user_id == 3)
											echo 'Operational'; 
										?></td>
			                			<td><?php echo $row->rules ?></td>
			                			<td><input type="radio" name="stc2_score<?php echo $i ?>" value="1" <?php echo $checked1 ?> class="stc-score-2 pull-left"><label class="pull-left">1</label></td>
			                			<td><input type="radio" name="stc2_score<?php echo $i ?>" value="2" <?php echo $checked2 ?> class="stc-score-2 pull-left"><label class="pull-left">2</label></td>
			                			<td><input type="radio" name="stc2_score<?php echo $i ?>" value="3" <?php echo $checked3 ?> class="stc-score-2 pull-left"><label class="pull-left">3</label></td>
			                			<td><input type="radio" name="stc2_score<?php echo $i ?>" value="4" <?php echo $checked4 ?> class="stc-score-2 pull-left"><label class="pull-left">4</label></td>
			                		</tr>
			                		<?php $i++; 
			                		} ?>
			                		<?php }else{ ?>
			                		<tr><td colspan="6" class="text-center">Tidak ada rules.</td></tr>
			                		<?php } ?>
			                	</tbody>
			                </table>
			            </section>
			            <!-- STC-3 -->
			            <h3>STC-3</h3>
			            <section data-section="section-stc3">
			                <table class="table table-bordered table-score">
			                	<thead></thead>
			                	<tbody>
			                		<tr>
			                			<th class="text-center">No</th>
			                			<th class="text-center">Rules</th>
			                			<th colspan="4" class="text-center">Score</th>
			                		</tr>
			                		</tr>
			                		<?php 
			                		$i = 1;
			                		if ($stc3->num_rows() > 0) {
			                			foreach ($stc3->result() as $row) { 
			                				if($data){
				                				$key = array_search($row->id, array_column($result_stc3, 'standar_kompetensi_id'));
				                				if (count($key) >= 0) {
				                					$user_score = $result_stc3[$key]['score'];
					                				$checked1 = $checked2 = $checked3 = $checked4 = "";
					                				if ($user_score == 1) {
					                					$checked1 = "checked";
					                				}elseif ($user_score == 2) {
					                					$checked2 = "checked";
					                				}elseif ($user_score == 3) {
					                					$checked3 = "checked";
					                				}elseif ($user_score == 4) {
					                					$checked4 = "checked";
					                				}
				                				}else{
				                					$checked1 = $checked2 = $checked3 = $checked4 = "";
				   								}
			   								}
			   								else
			   									$checked1 = $checked2 = $checked3 = $checked4 = "";
			                				?>
			                		<tr>
			                			<td><?php echo $i ?></td>
			                			<td class="text-center">
										<?php if($row->document->row()) { ?>
										<a href="<?=base_url()?>assets/uploads/list_rules/<?php echo $row->id.'/'.$row->document->row()->file; ?>"><?php echo $row->document->row()->file; ?></a>
										<?php } else { echo '-'; } ?>
										</td>
			                			<td class="text-center"><?php 
										if($row->level_user_id == 1)
											echo 'Management';
										elseif ($row->level_user_id == 2)
											echo 'Middle Management';
										elseif ($row->level_user_id == 3)
											echo 'Operational'; 
										?></td>
			                			<td><?php echo $row->rules ?></td>
			                			<td><input type="radio" name="stc3_score<?php echo $i ?>" value="1" <?php echo $checked1 ?> class="stc-score-3 pull-left"><label class="pull-left">1</label></td>
			                			<td><input type="radio" name="stc3_score<?php echo $i ?>" value="2" <?php echo $checked2 ?> class="stc-score-3 pull-left"><label class="pull-left">2</label></td>
			                			<td><input type="radio" name="stc3_score<?php echo $i ?>" value="3" <?php echo $checked3 ?> class="stc-score-3 pull-left"><label class="pull-left">3</label></td>
			                			<td><input type="radio" name="stc3_score<?php echo $i ?>" value="4" <?php echo $checked4 ?> class="stc-score-3 pull-left"><label class="pull-left">4</label></td>
			                		</tr>
			                		<?php $i++; 
			                		} ?>
			                		<?php }else{ ?>
			                		<tr><td colspan="6" class="text-center">Tidak ada rules.</td></tr>
			                		<?php } ?>
			                	</tbody>
			                </table>
			            </section>
			            <!-- STC-4 -->
			            <h3>STC-4</h3>
			            <section data-section="section-stc4">
			                <table class="table table-bordered table-score">
			                	<thead></thead>
			                	<tbody>
			                		<tr>
			                			<th class="text-center">No</th>
			                			<th class="text-center">Rules</th>
			                			<th colspan="4" class="text-center">Score</th>
			                		</tr>
			                		<?php 
			                		$i = 1;
			                		if ($stc4->num_rows() > 0) {
			                			foreach ($stc4->result() as $row) { 
			                				if($data) {
				                				$key = array_search($row->id, array_column($result_stc4, 'standar_kompetensi_id'));
				                				if (count($key) >= 0) {
				                					$user_score = $result_stc4[$key]['score'];
					                				$checked1 = $checked2 = $checked3 = $checked4 = "";
					                				if ($user_score == 1) {
					                					$checked1 = "checked";
					                				}elseif ($user_score == 2) {
					                					$checked2 = "checked";
					                				}elseif ($user_score == 3) {
					                					$checked3 = "checked";
					                				}elseif ($user_score == 4) {
					                					$checked4 = "checked";
					                				}
				                				}else{
				                					$checked1 = $checked2 = $checked3 = $checked4 = "";
				   								}
			   								}
			   								else
			   									$checked1 = $checked2 = $checked3 = $checked4 = "";
			                				?>
			                		<tr>
			                			<td><?php echo $i ?></td>
			                			<td class="text-center">
										<?php if($row->document->row()) { ?>
										<a href="<?=base_url()?>assets/uploads/list_rules/<?php echo $row->id.'/'.$row->document->row()->file; ?>"><?php echo $row->document->row()->file; ?></a>
										<?php } else { echo '-'; } ?>
										</td>
			                			<td class="text-center"><?php 
										if($row->level_user_id == 1)
											echo 'Management';
										elseif ($row->level_user_id == 2)
											echo 'Middle Management';
										elseif ($row->level_user_id == 3)
											echo 'Operational'; 
										?></td>
			                			<td><?php echo $row->rules ?></td>
			                			<td><input type="radio" name="stc4_score<?php echo $i ?>" value="1" <?php echo $checked1 ?> class="stc-score-4 pull-left"><label class="pull-left">1</label></td>
			                			<td><input type="radio" name="stc4_score<?php echo $i ?>" value="2" <?php echo $checked2 ?> class="stc-score-4 pull-left"><label class="pull-left">2</label></td>
			                			<td><input type="radio" name="stc4_score<?php echo $i ?>" value="3" <?php echo $checked3 ?> class="stc-score-4 pull-left"><label class="pull-left">3</label></td>
			                			<td><input type="radio" name="stc4_score<?php echo $i ?>" value="4" <?php echo $checked4 ?> class="stc-score-4 pull-left"><label class="pull-left">4</label></td>
			                		</tr>
			                		<?php $i++; 
			                		} ?>
			                		<?php }else{ ?>
			                		<tr><td colspan="6" class="text-center">Tidak ada rules.</td></tr>
			                		<?php } ?>
			                	</tbody>
			                </table>
			            </section>
			          
			        </div>
			    </form>
			    </div>

			</div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                                
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>  
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>    
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page_user/standar_kompetensi/standar_kompetensi_form.js"></script>
	<!--  -->
<!-- END SCRIPTS -->   