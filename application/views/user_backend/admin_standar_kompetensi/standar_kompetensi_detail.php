<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="admin">Home</a></li>                    
	<li><a href="<?php echo $controller; ?>"><?php echo $controller_name; ?></a></li>
	<li><a href="<?php echo $controller.'/'.$function; ?>"><?php echo $function_name; ?></a></li>
	<li class="active"><?php echo $function_name; ?> Form</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">                    
	<h2><?php echo $function_name; ?></h2>
</div>
<!-- END PAGE TITLE -->                

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                

	<div class="row">
		<div class="col-md-12">

			<!-- START DEFAULT DATATABLE -->
			<div class="panel panel-default">
				<div class="panel-heading">                                
					<h3 class="panel-title">Form Standar Kompetensi</h3>                              
				</div>
				<div class="panel-body">
				<div class="alert alert-success hidden"><strong>Success! </strong><span></span></div>
				<div class="alert alert-warning hidden"><strong>Processing! </strong><span>Please wait...</span></div>
				<div class="alert alert-danger hidden"><strong>Failed! </strong><span></span></div>
				<div class="row">
					<div class="col-md-4">
						<table class="table">
							<tr>
								<td>Nama</td>
								<td>:</td>
								<td><?php echo $nama ?></td>
							</tr>
							<tr>
								<td>Level</td>
								<td>:</td>
								<td><?php
								if($data->level_user_id == 1)
									$level = 'Management';
								elseif($data->level_user_id == 2)
									$level = 'Middle Management';
								else
									$level = 'Operator';
								 echo $level; ?></td>
							</tr>
							<tr>
								<td>Jabatan</td>
								<td>:</td>
								<td><?php echo $jabatan ?></td>
							</tr>
							<tr>
								<td>Instalasi</td>
								<td>:</td>
								<td><?php echo $instalasi ?></td>
							</tr>
							<tr>
								<td>Final Score</td>
								<td>:</td>
								<td><?php echo $data->final_score ?></td>
							</tr>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<table class="table table-bordered">
							<tr>
								<th class="text-center" width="1%">No</th>
								<th class="text-center" width="24%">Level</th>
								<th class="text-center" width="24%">Training</th>
								<th class="text-center" width="24%">Rules</th>
								<th class="text-center" width="24%">Score</th>
							</tr>
							<tr>
								<td colspan="5" class="text-center">STC-1</td>
							</tr>
							<?php if ($result_stc1 && $result_stc1->num_rows() > 0) {
								$i = 1;
								foreach ($result_stc1->result() as $row) {
									if($row->level_user_id == 1)
										$level = 'Management';
									elseif($row->level_user_id == 2)
										$level = 'Middle Management';
									else
										$level = 'Operator';
								?>
									<tr>
										<td><?php echo $i ?></td>
										<td><?php echo $level; ?></td>
										<td><?php echo $row->training_name; ?></td>
										<td><?php echo $row->rules ?></td>
										<td><?php echo $row->score ?></td>
									</tr>
								<?php $i++; }
							} else { ?>
								<tr><td colspan="5" class="text-center">Tidak ada rules.</td></tr>
							<?php } ?>
							<tr>
								<td colspan="5" class="text-center">STC-2</td>
							</tr>
							<?php if ($result_stc2 && $result_stc2->num_rows() > 0) {
								$i = 1;
								foreach ($result_stc2->result() as $row) {
									if($row->level_user_id == 1)
										$level = 'Management';
									elseif($row->level_user_id == 2)
										$level = 'Middle Management';
									else
										$level = 'Operator'; 
								?>
									<tr>
										<td><?php echo $i ?></td>
										<td><?php echo $level; ?></td>
										<td><?php echo $row->training_name; ?></td>
										<td><?php echo $row->rules ?></td>
										<td><?php echo $row->score ?></td>
									</tr>
								<?php $i++; }
							}else{ ?>
								<tr><td colspan="5" class="text-center">Tidak ada rules.</td></tr>
							<?php } ?>
							<tr>
								<td colspan="5" class="text-center">STC-3</td>
							</tr>
							<?php if ($result_stc3 && $result_stc3->num_rows() > 0) {
								$i = 1;
								foreach ($result_stc3->result() as $row) { 
									if($row->level_user_id == 1)
										$level = 'Management';
									elseif($row->level_user_id == 2)
										$level = 'Middle Management';
									else
										$level = 'Operator';
									?>
									<tr>
										<td><?php echo $i ?></td>
										<td><?php echo $level; ?></td>
										<td><?php echo $row->training_name; ?></td>
										<td><?php echo $row->rules ?></td>
										<td><?php echo $row->score ?></td>
									</tr>
								<?php $i++; }
							}else{ ?>
								<tr><td colspan="5" class="text-center">Tidak ada rules.</td></tr>
							<?php } ?>
							<tr>
								<td colspan="5" class="text-center">STC-4</td>
							</tr>
							<?php if ($result_stc4 && $result_stc4->num_rows() > 0) {
								$i = 1;
								foreach ($result_stc4->result() as $row) { 
									if($row->level_user_id == 1)
										$level = 'Management';
									elseif($row->level_user_id == 2)
										$level = 'Middle Management';
									else
										$level = 'Operator';
									?>
									<tr>
										<td><?php echo $i ?></td>
										<td><?php echo $level; ?></td>
										<td><?php echo $row->training_name; ?></td>
										<td><?php echo $row->rules ?></td>
										<td><?php echo $row->score ?></td>
									</tr>
								<?php $i++; }
							}else{ ?>
								<tr><td colspan="5" class="text-center">Tidak ada rules.</td></tr>
							<?php } ?>
						</table>
					</div>
				</div>
			    </div>
			    <div class="panel-footer">
			    	<a href="admin_standar_kompetensi/standar_kompetensi" class="btn btn-primary pull-right">Back</a>
			    	
			    </div>

			</div>
			<!-- END DEFAULT DATATABLE -->

		</div>
	</div>                                
	
</div>
<!-- PAGE CONTENT WRAPPER -->

<!-- START SCRIPTS -->               
	<!-- THIS PAGE PLUGINS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/jquery-validation/jquery.validate.js"></script>  
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/summernote/summernote.js"></script>    
	<!-- END PAGE PLUGINS -->
	<!-- START TEMPLATE -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>
	
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>        
	<!-- END TEMPLATE -->  
	<!-- THIS PAGE JS SETTINGS -->
	<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page_user/standar_kompetensi/standar_kompetensi_form.js"></script>
	<!--  -->
<!-- END SCRIPTS -->   