<?php $now = new DateTime(strtotime('Y-m-d', now()));
$date_modified = new DateTime($this->session_user['date_modified']);
$interval = $date_modified->diff($now);
$year = 12*$interval->format('%y');
$month = $interval->format('%m');
$total_month = $year+$month;
if($total_month >= 2)
{
?>
    <div class="alert alert-danger"><strong>Your Password expired and must be changed. <a href="user_my_profile">Click Here</a> to change your password</strong><span></span></div>
<?php } ?>
<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="#">Home</a></li>                    
	<li class="active">Dashboard</li>
</ul>
<!-- END BREADCRUMB -->                       

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                  
	<div class="row">
		<div class="col-md-3">
			<div class="widget widget-info widget-padding-sm">
				<div class="widget-big-int plugin-clock">00:00</div>                            
				<div class="widget-subtitle plugin-date">Loading...</div>                           
				<div class="widget-buttons widget-c3">
					Hallo <?php echo $this->session_user['realname']; ?>
					<div class="col hidden">
						<a href="#"><span class="fa fa-clock-o"></span></a>
					</div>
					<div class="col hidden">
						<a href="#"><span class="fa fa-bell"></span></a>
					</div>
					<div class="col hidden">
						<a href="#"><span class="fa fa-calendar"></span></a>
					</div>
				</div>                            
			</div>
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="panel-title-box">
						<h3>Info Security Division</h3>
					</div>
					<ul class="panel-controls" style="margin-top: 2px;">
						<!-- <li><span class="badge">10</span></</li>                                         -->
					</ul>
				</div>
				<div class="panel-body">
					<?php foreach ($info_training as $it) { ?>
					<div class="media">
						<div class="media-body">
							<a href="user_info_training/detail_info_training/<?php echo $it->id; ?>">
								<h6 class="media-heading"><?php echo $it->title; ?></h6> 
								<span class="text-info">
									Tanggal Pendaftaran : <br>
									<?php echo date('d-m-Y',strtotime($it->pendaftaran_start)); ?> s/d
									<?php echo date('d-m-Y',strtotime($it->pendaftaran_end)); ?>
								</span>
							</a>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
		<div class="col-md-9">
			<div class="panel">
				<div class="panel-body">
				<div id="banner-slider" class="owl-carousel">
					<?php
					foreach ($banner as $banner) {
					?>
					<div class="slider-item">
						<img src="<?=base_url()?>assets/uploads/banner/<?php echo $banner->id; ?>/<?php echo $banner->banner; ?>">
						<div class="slider-caption">
							<h5><?php echo $banner->title; ?></h5>
							<p class="hidden-xs hidden-sm"><?php echo $banner->content; ?></p>
						</div>
					</div>
					<?php
					}
					?>
				</div>
				</div>
			</div>
		</div>
	</div>               
	<div class="row">
		<div class="col-md-12" >
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="panel-title-box" style="text-align: center; float: none;">
						<h3>DO YOU SEE THE NEGLIGENCE AROUND YOU?</h3>
					</div>
				</div>
				<div class="panel-body" style="text-align: center;">
					<button class="btn btn-danger btn-lg"><i class="fa fa-exclamation-triangle"></i><a href="user_citizen_security/index"><span style="padding: 5px; color: white;">REPORT HERE!</span></a></button>				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<!-- MODAL -->
		<div class="modal fade animated" id="px-dashboard-status-kerawanan-new-modal" tabindex="-1" role="dialog" aria-labelledby="px-dashboard-status-kerawanan-new-modal-label" aria-hidden="true">
			<div class="modal-dialog modal-md">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<h4 class="modal-title" id="px-dashboard-status-kerawanan-new-modal-label">Berita Terkini</h4>
					</div>
					<div class="modal-body list-group">
						
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade animated" id="px-dashboard-status-kerawanan-news-detail-modal" tabindex="-1" role="dialog" aria-labelledby="px-dashboard-status-kerawanan-news-detail-modal-label" aria-hidden="true">
			<div class="modal-dialog modal-md">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<h4 class="modal-title" id="px-dashboard-status-kerawanan-news-detail-modal-label">Berita Detil</h4>
					</div>
					<div class="modal-body">
						<h4></h4>
						<div class="content">
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="panel panel-success">
				<div class="panel-heading">
					<div class="panel-title-box">
						<h3>Peta Status Kerawanan @ <?php echo $this->session_user['name_instalasi']; ?></h3>
					</div>    
					<div id="" class="" style="width:150px;">                                            
						<!-- <select class="select2-dropdown" style="width:100%">
							<option value="Indonesia">Indonesia</option>
							<?php foreach ($province as $data_row) { ?>
							<option value="<?php echo $data_row->name ?>"><?php echo $data_row->name ?></option>
							<?php } ?>
						</select> -->
					</div>                                   
				</div>
				<div class="panel-body">   
					<div class="row">
						<div class="col-md-12">
						<label>History Status Kerawanan <?php echo $this->session_user['name_instalasi']; ?></label>
						</div>
						<div class="col-md-12">
							<label class="label-form pull-right label label-info">Jumlah Status : <?php echo $status_merah+$status_hijau+$status_kuning; ?></label>
							<label class="label-form label label-success">Hijau : <?php echo $status_hijau; ?></label>
							<label class="label-form label label-warning">Kuning : <?php echo $status_kuning; ?></label>
							<label class="label-form label label-danger">Merah : <?php echo $status_merah; ?></label>
						</div>
						<div class="col-md-12">
							&nbsp;
						</div>
					</div>                                
					<div class="row">
						<div class="col-md-12">
							<div id="peta-status-kerawanan-dash" style="width: 100%; height: 400px"></div>
						</div>
					</div>                                    
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="panel-title-box">
						<h3>Security Analysis</h3>
					</div>
				</div>
				<div class="panel-body">
					<h4>Information</h4>
					<?php foreach ($security_analysis_info as $sai) { ?>
					<a href="user_security_analysis/detail/<?php echo $sai->id; ?>">
					<div class="list-group-item">
						<?php echo $sai->title; ?><br>
						<span class="text-muted"><?php echo date('d-m-Y H:i:s',strtotime($sai->date_created)); ?></span>
					</div>
					</a>
					<?php } ?>
					<hr>
					<h4>Analysis</h4>
					<?php foreach ($security_analysis_analyst as $saa) { ?>
					<a href="user_security_analysis/detail/<?php echo $saa->id; ?>">
					<div class="list-group-item">
						<?php echo $saa->title; ?><br>
						<span class="text-muted"><?php echo date('d-m-Y H:i:s',strtotime($saa->date_created)); ?></span>
					</div>
					</a>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-colorful">
				<div class="panel-body padding-0">
					<div class="chart-holder" id="nilai-chart"></div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-colorful">
				<div class="panel-body padding-0">
					<div class="chart-holder" id="nilai-2015-chart"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-colorful">
				<div class="panel-heading">
				<select class="form-control" id="hasil-asesmen-2004-menu">
					<?php foreach ($asesmen_2004 as $as2004) { ?>
					<option value="<?php echo $as2004->id; ?>"><?php echo 'Periode '.$as2004->periode.' - Tanggal Input '.date('d F Y, H:i:s',strtotime($as2004->date_created)); ?></option>
					<?php } ?>
				</select>
				</div>
				<div class="panel-body padding-0">
					<div class="chart-holder" id="hasil-asesmen-2004-per-instalasi-highchart"></div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-colorful">
				<div class="panel-heading">
					<select class="form-control" id="hasil-asesmen-2015-menu">
						<?php foreach ($asesmen_2015 as $as2015) { ?>
						<option value="<?php echo $as2015->id; ?>"><?php echo 'Periode '.$as2015->periode.' - Tanggal Input '.date('d F Y, H:i:s',strtotime($as2015->date_created)); ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="panel-body padding-0">
					<div class="chart-holder" id="hasil-asesmen-2015-per-instalasi-highchart"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-colorful">
                            <!--
				<div class="panel-heading">
					<select class="form-control" id="standar-kompetensi-per-orang-menu">
						<?php foreach ($stc_per_orang as $stc_po) { ?>
						<option value="<?php echo $stc_po->id; ?>"><?php echo 'Tanggal Input '.date('d F Y, H:i:s',strtotime($stc_po->date_created)); ?></option>
						<?php } ?>
					</select>
				</div>
                            -->
				<div class="panel-body padding-0">
					<div class="chart-holder" id="hasil-stc-per-orang-highchart"></div>
				</div>
			</div>
		</div>
            <div class="col-md-6">
                    <div class="panel panel-colorful">
                            <div class="panel-body padding-0">
                                    <div class="chart-holder" id="perusahaan-highchart"></div>
                            </div>
                    </div>
            </div>
            <!--
		<div class="col-md-6">
			<div class="panel panel-colorful">
				<div class="panel-body padding-0">
					<div class="chart-holder" id="hasil-stc-per-instalasi-highchart"></div>
				</div>
			</div>
		</div>
            -->
	</div>
</div>
<!-- END PAGE CONTENT WRAPPER -->                 

<!-- START SCRIPTS -->
<!-- START THIS PAGE PLUGINS--> 
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/morris/raphael-min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/morris/morris.min.js"></script>       
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/rickshaw/d3.v3.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/rickshaw/rickshaw.min.js"></script>
<script type='text/javascript' src='<?=base_url()?>assets/backend_assets/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'></script>
<script type='text/javascript' src='<?=base_url()?>assets/backend_assets/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'></script>                
<script type='text/javascript' src='<?=base_url()?>assets/backend_assets/js/plugins/bootstrap/bootstrap-datepicker.js'></script>                
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/owl/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/moment.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/daterangepicker/daterangepicker.js"></script>
<!-- END THIS PAGE PLUGINS-->        

<!-- START TEMPLATE -->
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>

<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>

<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/dashboard.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page_user/dashboard/dashboard-asis.js"></script>

<!-- END TEMPLATE -->
<!-- END SCRIPTS -->  