<?php $now = new DateTime(strtotime('Y-m-d', now()));
$date_modified = new DateTime($this->session_user['date_modified']);
$interval = $date_modified->diff($now);
$year = 12*$interval->format('%y');
$month = $interval->format('%m');
$total_month = $year+$month;
if($total_month >= 2)
{
?>
    <div class="alert alert-danger"><strong>Your Password expired and must be changed. <a href="user_my_profile">Click Here</a> to change your password</strong><span></span></div>
<?php } ?>
<!-- START BREADCRUMB -->
<ul class="breadcrumb">
	<li><a href="#">Home</a></li>                    
	<li class="active">Dashboard</li>
</ul>
<!-- END BREADCRUMB -->                       

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">                  
	<div class="row">
		<div class="col-md-3">
			<div class="widget widget-info widget-padding-sm">
				<div class="widget-big-int plugin-clock">00:00</div>                            
				<div class="widget-subtitle plugin-date">Loading...</div>                           
				<div class="widget-buttons widget-c3">
					Hallo <?php echo $this->session_user['realname']; ?>
					<div class="col hidden">
						<a href="#"><span class="fa fa-clock-o"></span></a>
					</div>
					<div class="col hidden">
						<a href="#"><span class="fa fa-bell"></span></a>
					</div>
					<div class="col hidden">
						<a href="#"><span class="fa fa-calendar"></span></a>
					</div>
				</div>                            
			</div>
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="panel-title-box">
						<h3>Knowledge Share</h3>
					</div>
					<ul class="panel-controls" style="margin-top: 2px;">
						<!-- <li><span class="badge">10</span></</li>                                         -->
					</ul>
				</div>
				<div class="panel-body">
					<?php foreach ($knowledge_base as $it) { ?>
					<div class="media">
						<div class="media-body">
							<a href="user_knowledge/detail/<?php echo $it->id; ?>">
								<h6 class="media-heading"><?php echo $it->name; ?></h6> 
								<span class="text-info">
									Date Upload :
									<?php echo date('d-m-Y',strtotime($it->date_created)); ?>
								</span>
							</a>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
		<div class="col-md-9">
			<div class="panel">
				<div class="panel-body">
				<div id="banner-slider" class="owl-carousel">
					<?php
					foreach ($banner as $banner) {
					?>
					<div class="slider-item">
						<img src="<?=base_url()?>assets/uploads/banner/<?php echo $banner->id; ?>/<?php echo $banner->banner; ?>">
						<div class="slider-caption">
							<h5><?php echo $banner->title; ?></h5>
							<p class="hidden-xs hidden-sm"><?php echo $banner->content; ?></p>
						</div>
					</div>
					<?php
					}
					?>
				</div>
				</div>
			</div>
		</div>
	</div>                 
	<div class="row">
		<div class="col-md-12" >
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="panel-title-box" style="text-align: center; float: none;">
						<h3>DO YOU SEE THE NEGLIGENCE AROUND YOU?</h3>
					</div>
				</div>
				<div class="panel-body" style="text-align: center;">
					<button class="btn btn-danger btn-lg"><i class="fa fa-exclamation-triangle"></i><a href="user_citizen_security/index"><span style="padding: 5px; color: white;">REPORT HERE!</span></a></button>				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="panel-title-box">
						<h3>Security Analysis</h3>
					</div>
				</div>
                            <div class="col-md-6">
				<div class="panel-body">
					<h4>Information</h4>
					<?php foreach ($security_analysis_info as $sai) { ?>
					<a href="user_security_analysis/detail/<?php echo $sai->id; ?>">
					<div class="list-group-item">
						<?php echo $sai->title; ?><br>
						<span class="text-muted"><?php echo date('d-m-Y H:i:s',strtotime($sai->date_created)); ?></span>
					</div>
					</a>
					<?php } ?>
				</div>
                            </div>
                            <div class="col-md-6">
                                <div class="panel-body">
					<h4>Analysis</h4>
					<?php foreach ($security_analysis_analyst as $saa) { ?>
					<a href="user_security_analysis/detail/<?php echo $saa->id; ?>">
					<div class="list-group-item">
						<?php echo $saa->title; ?><br>
						<span class="text-muted"><?php echo date('d-m-Y H:i:s',strtotime($saa->date_created)); ?></span>
					</div>
					</a>
					<?php } ?>
				</div>
                            </div>
			</div>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT WRAPPER -->                 

<!-- START SCRIPTS -->
<!-- START THIS PAGE PLUGINS--> 
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/morris/raphael-min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/morris/morris.min.js"></script>       
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/rickshaw/d3.v3.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/rickshaw/rickshaw.min.js"></script>
<script type='text/javascript' src='<?=base_url()?>assets/backend_assets/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'></script>
<script type='text/javascript' src='<?=base_url()?>assets/backend_assets/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'></script>                
<script type='text/javascript' src='<?=base_url()?>assets/backend_assets/js/plugins/bootstrap/bootstrap-datepicker.js'></script>                
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/owl/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/moment.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins/daterangepicker/daterangepicker.js"></script>
<!-- END THIS PAGE PLUGINS-->        

<!-- START TEMPLATE -->
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/settings.js"></script>

<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/plugins.js"></script>        
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/actions.js"></script>

<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/js/dashboard.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/backend_assets/page_user/dashboard/dashboard-asis.js"></script>

<!-- END TEMPLATE -->
<!-- END SCRIPTS -->  