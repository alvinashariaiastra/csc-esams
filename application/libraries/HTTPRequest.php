<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class HTTPRequest
{
    public static function get($url, $auth=[], $contenttype=0)
    {
        $thisClass = new HTTPRequest();
        return $thisClass->curl($url, 'GET', $auth, $contenttype);
    }

    public static function post($url, $auth=[], $body=[], $contenttype=0)
    {
        $thisClass = new HTTPRequest();
        return $thisClass->curl($url, 'POST', $auth, $body, $contenttype);
    }

    function curl($url, $method, $auth, $body=[], $contenttype=0)
    {
        // Set Header
        $header = [];
        $header[] = 'Cache-Control: no-cache';
        if ($contenttype==0) { 
            $header[] = 'Content-Type: application/json'; 
        } else
        if ($contenttype==1) { 
            $header[] = 'Content-Type: application/x-www-form-urlencoded';
        }
        foreach ($auth['header'] as $key => $value)
            $header[] = $key.': '.$value;

        // Init cURL
        $curl = curl_init();

        // Set Option cURL
        $curlopt = [];
        $curlopt[CURLOPT_URL] = $url;
        $curlopt[CURLOPT_HTTPAUTH] = CURLAUTH_BASIC;
        // $curlopt[CURLOPT_HTTPAUTH] = CURLAUTH_ANY;
        $curlopt[CURLOPT_RETURNTRANSFER] = true;
        $curlopt[CURLOPT_ENCODING] = '';
        $curlopt[CURLOPT_MAXREDIRS] = 10;
        $curlopt[CURLOPT_TIMEOUT] = 60;
        $curlopt[CURLOPT_SSL_VERIFYHOST] = false; // SSL Host Verify
        $curlopt[CURLOPT_SSL_VERIFYPEER] = false; // SSL Cert Verify
        // $curlopt[CURLOPT_SSH_PRIVATE_KEYFILE] = './assets/ssl/wso2devs.jks';
        // $curlopt[CURLOPT_SSLCERTTYPE] = "PEM";
        // $curlopt[CURLOPT_SSLCERT] = './assets/ssl/wso2.pem';
        // $curlopt[CURLOPT_SSLCERTPASSWD] = 'wso2dev19';
        $curlopt[CURLOPT_HTTP_VERSION] = CURL_HTTP_VERSION_1_1;
        $curlopt[CURLOPT_CUSTOMREQUEST] = $method;
        if ($method=='POST') {
            $curlopt[CURLOPT_POST] = true;
            if ($contenttype==0) {
                $curlopt[CURLOPT_POSTFIELDS] = json_encode($body);
            } else
            if ($contenttype==1) {
                $bodys = [];
                foreach ($body as $key => $value)
                    $bodys[] = $key.'='.$value;
                $bodys = implode('&', $bodys);
                $curlopt[CURLOPT_POSTFIELDS] = $bodys;
            }
        }
        $curlopt[CURLOPT_HTTPHEADER] = $header;
        if (isset($auth['username']) and isset($auth['password']))
            $curlopt[CURLOPT_USERPWD] = $auth['username'].':'.$auth['password'];
        // $curlopt[CURLINFO_HEADER_OUT] = true;

        // Send Request cURL
        curl_setopt_array($curl, $curlopt);

        // Execute cURL
        $response = curl_exec($curl);
        // Get Error Execute
        $err = curl_error($curl);
        // Close cURL
        curl_close($curl);

        // Response
        if ($err)
            $response = $err;

        return $response;
    }
}