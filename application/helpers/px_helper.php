<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('date_indo'))
{
	function date_indo($format, $date)
	{
		$timestamp = strtotime($date);
		$l = array('', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu', 'Minggu');
		$F = array('', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
		$return = '';
		if(is_null($timestamp))
		{ 
			$timestamp = mktime(); 
		}
		for($i = 0, $len = strlen($format); $i < $len; $i++) 
		{
			switch($format[$i])
			{
				case '\\' :
					$i++;
					$return .= isset($format[$i]) ? $format[$i] : '';
					break;
				case 'l' :
					$return .= $l[date('N', $timestamp)];
					break;
				case 'F' :
					$return .= $F[date('n', $timestamp)];
					break;
				default :
					$return .= date($format[$i], $timestamp);
					break;
			}
		}
		return $return;
	}
}
if ( ! function_exists('limit_words'))
{
	function limit_words($paragraph,$limit){
		$data = implode(' ', array_slice(explode(' ', strip_tags($paragraph)), 0, $limit));
		return $data;
	}
}
if (! function_exists('array_column')) {
    function array_column(array $input, $columnKey, $indexKey = null) {
        $array = array();
        foreach ($input as $value) {
            if ( ! isset($value[$columnKey])) {
                trigger_error("Key \"$columnKey\" does not exist in array");
                return false;
            }
            if (is_null($indexKey)) {
                $array[] = $value[$columnKey];
            }
            else {
                if ( ! isset($value[$indexKey])) {
                    trigger_error("Key \"$indexKey\" does not exist in array");
                    return false;
                }
                if ( ! is_scalar($value[$indexKey])) {
                    trigger_error("Key \"$indexKey\" does not contain scalar value");
                    return false;
                }
                $array[$value[$indexKey]] = $value[$columnKey];
            }
        }
        return $array;
    }
}