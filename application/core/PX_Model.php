<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class PX_Model extends CI_Model {

    function __construct() {
        parent::__construct();
		// DEFAULT TIME ZONE
		date_default_timezone_set('Asia/Jakarta');
		//Admin Table
        $this->tbl_prefix = 'px_';
        $this->tbl_adm_config = $this->tbl_prefix.'adm_config';
        $this->tbl_admin = $this->tbl_prefix.'admin';
        $this->tbl_admin_log = $this->tbl_prefix.'admin_log';
        $this->tbl_master_data = $this->tbl_prefix.'master_data';
        $this->tbl_menu = $this->tbl_prefix.'menu';
        $this->tbl_useraccess = $this->tbl_prefix.'useraccess';
        $this->tbl_usergroup = $this->tbl_prefix.'usergroup';
        $this->tbl_maintain_email = $this->tbl_prefix.'maintain_email';
        //user table
        $this->tbl_user = $this->tbl_prefix.'user';
        $this->tbl_user_log = $this->tbl_prefix.'user_log';
        $this->tbl_user_jabatan = $this->tbl_prefix.'user_jabatan';
		//citizen security table
		$this->tbl_citizen_security_report = $this->tbl_prefix.'citizen_security_report';
        //security analysis table
        $this->tbl_security_analysis = $this->tbl_prefix.'security_analysis';
        //standar kompetensi table
        $this->tbl_level_user = $this->tbl_prefix.'level_user';
        $this->tbl_list_rules_file = $this->tbl_prefix.'list_rules_file';
        $this->tbl_standar_kompetensi = $this->tbl_prefix.'standar_kompetensi';
        $this->tbl_standar_kompetensi_result = $this->tbl_prefix.'standar_kompetensi_result';
        $this->tbl_standar_kompetensi_user = $this->tbl_prefix.'standar_kompetensi_user';
        //training info table
        $this->tbl_training_info = $this->tbl_prefix.'training_info';
        $this->tbl_training_pendaftaran = $this->tbl_prefix.'training_pendaftaran';
        //knowledge base table
        $this->tbl_knowledge_base = $this->tbl_prefix.'knowledge_base';
        $this->tbl_knowledge_base_category = $this->tbl_prefix.'knowledge_base_category';
        //status kerawanan table
        $this->tbl_status_kerawanan = $this->tbl_prefix.'status_kerawanan';
        $this->tbl_status_kerawanan_news = $this->tbl_prefix.'status_kerawanan_news';
        $this->tbl_status_kerawanan_news_photo = $this->tbl_prefix.'status_kerawanan_news_photo';
        //asesmen table
        $this->tbl_jenis_bisnis = $this->tbl_prefix.'asesmen_jenis_bisnis';
        $this->tbl_sub_bisnis = $this->tbl_prefix.'asesmen_sub_bisnis';
        $this->tbl_perusahaan = $this->tbl_prefix.'asesmen_perusahaan';
        $this->tbl_cabang = $this->tbl_prefix.'asesmen_cabang';
        $this->tbl_instalasi = $this->tbl_prefix.'asesmen_instalasi';
        $this->tbl_assessment2004 = $this->tbl_prefix.'asesmen_2004';
        $this->tbl_assessment2015 = $this->tbl_prefix.'asesmen_2015';
        //lost and found table
        $this->tbl_lost_found = $this->tbl_prefix.'lost_found';
        //other table
        $this->tbl_static_content = $this->tbl_prefix.'static_content';
        $this->tbl_province = $this->tbl_prefix.'status_kerawanan_province';
        // Esams - Master Data
        $this->tbl_building = $this->tbl_prefix.'building';
        $this->tbl_shift = $this->tbl_prefix.'shift';
        $this->tbl_group_security = $this->tbl_prefix.'group_security';
        $this->tbl_group_detail = $this->tbl_prefix.'group_security_detail';
        // Esams - Assignent
        $this->tbl_assignment = $this->tbl_prefix.'assignment';
        $this->tbl_assignment_detail = $this->tbl_prefix.'assignment_detail';
        $this->tbl_assignment_people = $this->tbl_prefix.'assignment_people';
        $this->tbl_assignment_attachment = $this->tbl_prefix.'assignment_attachment';
        $this->tbl_assignment_report = $this->tbl_prefix.'assignment_report';
        $this->tbl_assignment_report_attachment = $this->tbl_prefix.'assignment_report_attachment';
        $this->tbl_assignment_people_checkin = $this->tbl_prefix.'assignment_people_checkin';
        
    }
}
