<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PX_Controller extends CI_Controller {

	// const wso2_url = 'https://wso2devb.astra.co.id:9443/oauth2'; // dev
	const wso2_url = 'https://devproxy.astra.co.id/wso2dev/oauth2'; // dev
	// const wso2_url = 'https://accounts.astra.co.id/oauth2'; // prod
	const wso2_expired_time = 3600; // 3600
	const wso2_env = 'development'; // development
	//const wso2_env = 'production'; //production

	public function __construct() {
		parent::__construct();
		// Load Library HTTP Requast
		$this->load->library('HTTPRequest', 'httprequest');
		// DEFAULT TIME ZONE
		date_default_timezone_set('Asia/Jakarta');
		//Admin Table
		$this->tbl_prefix = 'px_';
		$this->tbl_adm_config = $this->tbl_prefix.'adm_config';
        $this->tbl_admin = $this->tbl_prefix.'admin';
        $this->tbl_admin_log = $this->tbl_prefix.'admin_log';
		$this->tbl_master_data = $this->tbl_prefix.'master_data';
		$this->tbl_menu = $this->tbl_prefix.'menu';
		$this->tbl_menu_user = $this->tbl_prefix.'menu_user';
		$this->tbl_useraccess = $this->tbl_prefix.'useraccess';
		$this->tbl_useraccess_user = $this->tbl_prefix.'useraccess_user';
		$this->tbl_usergroup = $this->tbl_prefix.'usergroup';
		$this->tbl_usergroup_user = $this->tbl_prefix.'usergroup_user';
		$this->tbl_maintain_email = $this->tbl_prefix.'maintain_email';
        //user table
        $this->tbl_user = $this->tbl_prefix.'user';
        $this->tbl_user_log = $this->tbl_prefix.'user_log';
        $this->tbl_user_jabatan = $this->tbl_prefix.'user_jabatan';
		//citizen security table
		$this->tbl_citizen_security = $this->tbl_prefix.'citizen_security';
        //security analysis table
        $this->tbl_security_analysis = $this->tbl_prefix.'security_analysis';
        //standar kompetensi table
        $this->tbl_level_user = $this->tbl_prefix.'level_user';
        $this->tbl_list_rules_file = $this->tbl_prefix.'list_rules_file';
        $this->tbl_standar_kompetensi = $this->tbl_prefix.'standar_kompetensi';
        $this->tbl_standar_kompetensi_result = $this->tbl_prefix.'standar_kompetensi_result';
        $this->tbl_standar_kompetensi_user = $this->tbl_prefix.'standar_kompetensi_user';
        $this->tbl_standar_kompetensi_training = $this->tbl_prefix.'standar_kompetensi_training';
        //training info table
        $this->tbl_training_info = $this->tbl_prefix.'training_info';
        $this->tbl_training_pendaftaran = $this->tbl_prefix.'training_pendaftaran';
        //knowledge base table
        $this->tbl_knowledge_base = $this->tbl_prefix.'knowledge_base';
        $this->tbl_knowledge_base_category = $this->tbl_prefix.'knowledge_base_category';
        //status kerawanan table
        $this->tbl_status_kerawanan = $this->tbl_prefix.'status_kerawanan';
        $this->tbl_status_kerawanan_status = $this->tbl_prefix.'status_kerawanan_status';
        $this->tbl_status_kerawanan_news = $this->tbl_prefix.'status_kerawanan_news';
        $this->tbl_status_kerawanan_news_photo = $this->tbl_prefix.'status_kerawanan_news_photo';
        //asesmen table
        $this->tbl_jenis_bisnis = $this->tbl_prefix.'asesmen_jenis_bisnis';
        $this->tbl_sub_bisnis = $this->tbl_prefix.'asesmen_sub_bisnis';
        $this->tbl_perusahaan = $this->tbl_prefix.'asesmen_perusahaan';
        $this->tbl_cabang = $this->tbl_prefix.'asesmen_cabang';
        $this->tbl_instalasi = $this->tbl_prefix.'asesmen_instalasi';
        $this->tbl_assessment2004 = $this->tbl_prefix.'asesmen_2004';
        $this->tbl_assessment2015 = $this->tbl_prefix.'asesmen_2015';
        $this->tbl_assessment2015_history = $this->tbl_prefix.'asesmen_2015_history'; // Add by: Alvin (atsalvin0017), Date: 2019-08-09 10:52 AM
        $this->tbl_rules_asms_2004 = $this->tbl_prefix.'rules_asms_2004';
        $this->tbl_rules_asms_2015 = $this->tbl_prefix.'rules_asms_2015';
        $this->tbl_asesmen_2004_value = $this->tbl_prefix.'asesmen_2004_value';
        $this->tbl_asesmen_2015_value = $this->tbl_prefix.'asesmen_2015_value';
        $this->tbl_asesmen_scoring_colour = $this->tbl_prefix.'asesmen_scoring_colour';
        $this->tbl_asesmen_scoring_colour_config = $this->tbl_prefix.'asesmen_scoring_colour_config';
        //lost and found table
        $this->tbl_lost_found = $this->tbl_prefix.'lost_found';
        //other table
        $this->tbl_static_content = $this->tbl_prefix.'static_content';
        $this->tbl_province = $this->tbl_prefix.'status_kerawanan_province';
        $this->tbl_banner = $this->tbl_prefix.'banner';
        $this->tbl_message = $this->tbl_prefix.'message';
        $this->tbl_visitor = $this->tbl_prefix.'visitor';
        // Esams - Master Data
        $this->tbl_building = $this->tbl_prefix.'building';
        $this->tbl_detail_building = $this->tbl_prefix.'building_detail';
        $this->tbl_checkpoint = $this->tbl_prefix.'checkpoint';
        $this->tbl_cctv = $this->tbl_prefix.'cctv_csc';
        $this->tbl_pattern = $this->tbl_prefix.'pattern';
        $this->tbl_pattern_detail = $this->tbl_prefix.'pattern_detail';
        $this->tbl_checklist = $this->tbl_prefix.'checklist';
        $this->tbl_checklist_detail = $this->tbl_prefix.'checklist_detail';
        $this->tbl_shift = $this->tbl_prefix.'shift';
        $this->tbl_group_security = $this->tbl_prefix.'group_security';
        $this->tbl_group_detail = $this->tbl_prefix.'group_security_detail';
        // Esams - Assignent
        $this->tbl_assignment = $this->tbl_prefix.'assignment';
        $this->tbl_assignment_detail = $this->tbl_prefix.'assignment_detail';
        $this->tbl_assignment_people = $this->tbl_prefix.'assignment_people';
        $this->tbl_assignment_attachment = $this->tbl_prefix.'assignment_attachment';
        $this->tbl_assignment_report = $this->tbl_prefix.'assignment_report';
        $this->tbl_assignment_report_attachment = $this->tbl_prefix.'assignment_report_attachment';
        $this->tbl_assignment_people_checkin = $this->tbl_prefix.'assignment_people_checkin';
        // Esams - Patrol
        $this->tbl_patrol = $this->tbl_prefix.'patrol';
        $this->tbl_patrol_user = $this->tbl_prefix.'patrol_user';
        $this->tbl_patrol_pattern = $this->tbl_prefix.'patrol_pattern';
        $this->tbl_patrol_report = $this->tbl_prefix.'patrol_report';
        $this->tbl_patrol_report_checkpoint = $this->tbl_prefix.'patrol_report_checkpoint';
        $this->tbl_patrol_report_attachment = $this->tbl_prefix.'patrol_report_attachment';
        $this->tbl_patrol_checkin = $this->tbl_prefix.'patrol_checkin';
        // Esams - Issue
        $this->tbl_issue = $this->tbl_prefix.'issue';
        $this->tbl_issue_assign = $this->tbl_prefix.'issue_assign';
        $this->tbl_issue_attachment = $this->tbl_prefix.'issue_attachment';
        $this->tbl_issue_report = $this->tbl_prefix.'issue_report';
        $this->tbl_issue_report_type = $this->tbl_prefix.'issue_report_type';
        $this->tbl_issue_report_attachment = $this->tbl_prefix.'issue_report_attachment';
        // 
        $this->tbl_esams_log_wso2_token = $this->tbl_prefix.'esams_log_wso2_token';
        $this->tbl_esams_log_api = $this->tbl_prefix.'esams_log_api';
                
		// MODELS
		$this->load->model('model_basic');
		$this->load->model('model_chart');
		$this->load->model('model_menu');
		$this->load->model('model_menu_user');
		$this->load->model('model_admin_user');
		$this->load->model('model_useraccess');
		$this->load->model('model_useraccess_user');
		$this->load->model('model_usergroup');
		$this->load->model('model_usergroup_user');
		$this->load->model('model_master');
		$this->load->model('model_table');
                $this->load->model('model_activity');
		$this->load->model('model_assessment2004');
		$this->load->model('model_assessment2015');
		$this->load->model('model_user_list');
		$this->load->model('model_user_jabatan');
		$this->load->model('model_info_training');
		$this->load->model('model_instalasi');
		$this->load->model('model_pendaftaran_training');
		$this->load->model('model_standar_kompetensi');
		$this->load->model('model_message');
		$this->load->model('model_standar_kompetensi_training');
		$this->load->model('model_status_kerawanan');
                $this->load->model('model_history');
		$this->load->model('model_citizen_security');
		$this->load->model('model_maintain_email');
		//esams
		$this->load->model('model_group_security');
		$this->load->model('model_assignment');

		// sessions
		if($this->session->userdata('admin') != FALSE)
			$this->session_admin = $this->session->userdata('admin');
		elseif($this->session->userdata('admin') == FALSE || $this->session->userdata('user') != FALSE){
			$this->session_admin = array(
				'admin_id' => 0,
				'username' => 'GUEST',
				'password' => ' ',
				'realname' => 'GUEST',
				'email' => 'GUEST@LOCAL.DEV',
				'id_usergroup' => 0,
				'name_usergroup' => 'GUEST',
				'photo' => 'THUMB.png'
				);
		}
		if($this->session->userdata('user') != FALSE)
			$this->session_user = $this->session->userdata('user');
		elseif($this->session->userdata('admin') != FALSE || $this->session->userdata('user') == FALSE){
			$this->session_user = array(
				'user_id' => 0,
				'username' => 'GUEST',
				'password' => ' ',
				'realname' => 'GUEST',
				'email' => 'GUEST@LOCAL.DEV',
				'id_usergroup' => 0,
				'id_level' => 0,
				'name_usergroup' => 'GUEST',
				'photo' => 'THUMB.png',
				'id_instalasi' => 0
				);
		}
	}
	function get_app_settings() {
		$d_row = $this->model_basic->select_all_limit($this->tbl_adm_config,1)->row();
		$data['app_id'] = $d_row->id;
		$data['app_title'] = $d_row->title;
		$data['app_desc'] = $d_row->desc;
		$data['app_login_logo'] = $d_row->login_logo;
		$data['app_mini_logo'] = $d_row->mini_logo;
		$data['app_single_logo'] = $d_row->single_logo;
		$data['app_mini_logo'] = $d_row->mini_logo;
		$data['app_favicon_logo'] = $d_row->favicon_logo;
                $data['unread_message_admin'] = $this->model_basic->select_where_array($this->tbl_message, array('type' => 1, 'status' =>0))->num_rows();
                $data['app_user_log_count'] = $this->model_basic->select_where($this->tbl_user_log, 'read_flag', 0)->num_rows();
                $user_log = $this->model_basic->select_where($this->tbl_user_log, 'read_flag', 0);
                foreach($user_log->result() as $data_row)
                {
                    if($data_row->access_type == 0)
                    {
                        $user = $this->model_basic->select_where($this->tbl_user, 'id', $data_row->user_id);
                        $access = 'User';
                    }
                    else
                    {
                        $user = $this->model_basic->select_where($this->tbl_admin, 'id', $data_row->user_id);
                        $access = 'Admin';
                    }
                    if($user->num_rows() == 1)
                    {
                        $data_row->user = $user->row()->realname.' ('.$access.')';
                    }
                    else
                    {
                        $data_row->user = 'Unknown';
                    }
                }
                $data['app_user_log_data'] = $user_log->result();
                if($this->session->userdata('user') != FALSE)
                    $data['unread_message_user'] = $this->model_basic->select_where_array($this->tbl_message, array('type' => 0, 'status' => 0, 'user_id' => $this->session_user['admin_id']))->num_rows();
		return $data;
	}
	function get_function($name,$function){
		$data['function_name'] = $name;
		$data['function'] = $function;
		$data['function_form'] = $function.'_form';
		$data['function_add'] = $function.'_add';
		$data['function_edit'] = $function.'_edit';
		$data['function_delete'] = $function.'_delete';
		$data['function_get'] = $function.'_get';
		$menu_id = $this->model_basic->select_where($this->tbl_menu,'target',$function)->row();
		if($menu_id)
			$data['function_id'] = $menu_id->id;
		else
			$data['function_id'] = 0;
		return $data;
	}
	function get_function_user($name,$function){
		$data['function_name'] = $name;
		$data['function'] = $function;
		$data['function_form'] = $function.'_form';
		$data['function_add'] = $function.'_add';
		$data['function_edit'] = $function.'_edit';
		$data['function_delete'] = $function.'_delete';
		$data['function_get'] = $function.'_get';
		$menu_id = $this->model_basic->select_where($this->tbl_menu_user,'target',$function)->row();
		if($menu_id)
			$data['function_id'] = $menu_id->id;
		else
			$data['function_id'] = 0;
		return $data;
	}
	/*
		Updated by: Alvin (atsalvin0017)
		Date: 2019-08-13 10:35 AM
		Action: Add object badge for notification information
    */
	function get_menu()
	{
		$menu = $this->model_menu->get_menu_bar($this->session_admin['id_usergroup']);
		$submenu = $this->model_menu->get_sub_menu($this->session_admin['id_usergroup']);
		$data = array();
		foreach ($menu as $m) {
			// start:: add by Alvin (atsalvin0017) Date: 2019-08-13 10:35 AM
			$badge = null;
			if ($m->target=='admin_request') {
				$user_id = null;
				if ($this->session_admin['id_usergroup']!=1)
					$user_id = $this->session->userdata['admin']['admin_id'];
				else
				if ($this->session_admin['id_usergroup']==1)
					$user_id = 1;
				$request_assessment2015 = $this->model_assessment2015->get_count_by_status($this->tbl_assessment2015, 3, $user_id);
				if ($request_assessment2015>0)
					$badge = $request_assessment2015;
				// add by Alvin (atsalvin0017) Date: 2019-08-15 11:25 AM
				// if ($this->session->userdata['admin']['name_usergroup']=='Admin Asesor')
					// $m->name='Inbox';
			} else
			if ($m->target=='admin_inbox') {
				$status = 3;
				if ($this->session_admin['id_usergroup']==19)
					$status = 2;
				else
				if ($this->session_admin['id_usergroup']==1)
					$status = [2, 3];
				$request_assessment2015 = $this->model_assessment2015->get_count_by_status($this->tbl_assessment2015, $status);
				if ($request_assessment2015>0)
					$badge = $request_assessment2015;
			}
			$m->badge = $badge;
			// end::
			$data[$m->id_menu] = $m;
			$m->submenu = array();
		}
		foreach ($submenu as $sm) {
			// start:: add by Alvin (atsalvin0017) Date: 2019-08-29 17:40 PM
			$sm->submenu = array();
			if ($sm->target=='esams/masterdata') {
				$sm->submenu = [
					(object)[
						'name' => 'Shift',
						'target' => $sm->target.'/shift'
					],
					(object)[
						'name' => 'Group Security',
						'target' => $sm->target.'/group_security'
					],
					(object)[
						'name' => 'Detail Group',
						'target' => $sm->target.'/group_detail'
					],
					(object)[
						'name' => 'Building',
						'target' => $sm->target.'/building'
					],
					(object)[
						'name' => 'Detail Building',
						'target' => $sm->target.'/detail_building'
					],
					(object)[
						'name' => 'Pattern',
						'target' => $sm->target.'/pattern'
					],
					(object)[
						'name' => 'Detail Pattern',
						'target' => $sm->target.'/detail_pattern'
					],
					(object)[
						'name' => 'Checkpoint',
						'target' => $sm->target.'/checkpoint'
					],
					(object)[
						'name' => 'CCTV',
						'target' => $sm->target.'/cctv'
					],
					(object)[
						'name' => 'Checklist',
						'target' => $sm->target.'/checklist'
					],
					(object)[
						'name' => 'Issue Report Type',
						'target' => $sm->target.'/issue_report_type'
					]
				];
			}
			// end::
			$data[$sm->id_parent]->submenu[] = $sm;
		}
		$data['menu'] = $data;
		return $data;
	}
	function user_get_menu()
	{
		$menu = $this->model_menu_user->get_menu_bar($this->session_user['id_usergroup']);
		$submenu = $this->model_menu_user->get_sub_menu($this->session_user['id_usergroup']);
		$data = array();
		foreach ($menu as $m) {
			// start:: add by Alvin (atsalvin0017) Date: 2019-08-13 10:35 AM
			$badge = null;
			if ($m->target=='user_request') {
				$request_assessment2015 = $this->model_assessment2015->get_count_by_status($this->tbl_assessment2015, [2,3], $this->session_user['admin_id']);
				if ($request_assessment2015>0)
					$badge = $request_assessment2015;
			}
			$m->badge = $badge;
			// end::
			$data[$m->id_menu] = $m;
			$m->submenu = array();
		}
		foreach ($submenu as $sm) {
			$data[$sm->id_parent]->submenu[] = $sm;
		}
		$data['menu'] = $data;
		return $data;
	}
	function get_all_menu()
	{
		$menu = $this->model_basic->select_where_order($this->tbl_menu,'id_parent','0','orders','ASC')->result();
		$submenu = $this->model_basic->select_where_order($this->tbl_menu,'id_parent >','0','orders','ASC')->result();
		$data = array();
		foreach ($menu as $m) {
			$data[$m->id] = $m;
			$m->submenu = array();
		}
		foreach ($submenu as $sm) {
			$data[$sm->id_parent]->submenu[] = $sm;
		}
		return $data;
	}
	function user_get_all_menu()
	{
		$menu = $this->model_basic->select_where_order($this->tbl_menu_user,'id_parent','0','orders','ASC')->result();
		$submenu = $this->model_basic->select_where_order($this->tbl_menu_user,'id_parent >','0','orders','ASC')->result();
		$data = array();
		foreach ($menu as $m) {
			$data[$m->id] = $m;
			$m->submenu = array();
		}
		foreach ($submenu as $sm) {
			$data[$sm->id_parent]->submenu[] = $sm;
		}
		return $data;
	}
	//admin
	function check_login(){
		if($this->session->userdata('admin') == FALSE){
			redirect('admin');
		}
		else
			return true;
	}

	function check_userakses($menu_id, $function, $user = 'admin')
	{
		if($user == 'admin')
			$group_id = $this->session_admin['id_usergroup'];
		if($user == 'member')
			$group_id = $this->session->userdata['member']['group_id'];
		$access = $this->model_useraccess->get_useraccess($group_id, $menu_id);
		switch ($function)
		{
			case 1:
				if($access->act_read == 1)
					return TRUE;
				else
					redirect('admin');
				break;
			case 2:
				if($access->act_create == 1)
					return TRUE;
				else
					redirect('admin');
				break;
			case 3:
				if($access->act_update == 1)
					return TRUE;
				else
					redirect('admin');
				break;
			case 4:
				if($access->act_delete == 1)
					return TRUE;
				else
					redirect('admin');
				break;
		}
	}
        
        function get_submenu($parent)
    {
        $parent_id = $this->model_basic->select_where($this->tbl_menu, 'target', $parent)->row()->id;
        $submenu = $this->model_basic->select_where_order($this->tbl_menu, 'id_parent', $parent_id, 'orders', 'ASC')->result();
        return $submenu;
    }

	//user
	function user_check_login(){
		if($this->session->userdata('user') == FALSE){
			redirect('user');
		}
		else
			return true;
	}

	function check_userakses_user($menu_id, $function, $user = 'user')
	{
		$group_id = $this->session_user['id_usergroup'];
		$access = $this->model_useraccess_user->get_useraccess($group_id, $menu_id);

		switch ($function)
		{
			case 1:
				if($access->act_read == 1)
					return TRUE;
				else
					redirect('user');
				break;
			case 2:
				if($access->act_create == 1)
					return TRUE;
				else
					redirect('user');
				break;
			case 3:
				if($access->act_update == 1)
					return TRUE;
				else
					redirect('user');
				break;
			case 4:
				if($access->act_delete == 1)
					return TRUE;
				else
					redirect('user');
				break;
		}
	}
	function delete_temp($folder){
		if($this->session->userdata($folder) != FALSE){
			$temp_folder = $this->session->userdata($folder);
			$files = glob(FCPATH . "assets/uploads/temp/".$temp_folder."/{,.}*", GLOB_BRACE);
			foreach($files as $file){ 
				if(is_file($file))
					@unlink($file);
			}
			@rmdir(FCPATH . "assets/uploads/temp/".$temp_folder);
			$this->session->unset_userdata($folder);
		}
	}
	function delete_folder($folder){
		$files = glob(FCPATH . "assets/uploads/".$folder."/{,.}*", GLOB_BRACE);
		foreach($files as $file){ 
			if(is_file($file))
				@unlink($file);
		}
		@rmdir(FCPATH . "assets/uploads/".$folder);
	}
	function save_log_admin($action, $log_description){
        $admin_id = $this->session->userdata['admin']['admin_id'];
        $data = array('user_id' => $admin_id,
                      'type_id' => $action,
                      'access_type' => 1,
                      'log_description' => $log_description,
                      'read_flag' => 0, // Add by: Alvin (atsalvin0017), Date: 2019-08-09 10:52 AM
                      'delete_flag' => 0, // Add by: Alvin (atsalvin0017), Date: 2019-08-09 10:52 AM
                      'date_created' => date('Y-m-d H:i:s', now()));
		if($this->model_basic->insert_all($this->tbl_user_log,$data))
			return true;
		else
			return false;
	}
    function save_log_user($action, $log_description){
        $user_id = $this->session->userdata['user']['admin_id'];
        $data = array('user_id' => $user_id,
                      'type_id' => $action,
                      'log_description' => $log_description,
                      'read_flag' => 0, // Add by: Alvin (atsalvin0017), Date: 2019-08-12 13:59 PM
                      'delete_flag' => 0, // Add by: Alvin (atsalvin0017), Date: 2019-08-12 13:59 PM
                      'date_created' => date('Y-m-d H:i:s', now()));
		if($this->model_basic->insert_all($this->tbl_user_log,$data))
			return true;
		else
			return false;
	}
	function check_colour($value,$type){
		$scoring_warna = $this->model_basic->select_where($this->tbl_asesmen_scoring_colour,'name',$type)->row();
		$scoring_warna_config = $this->model_basic->select_where($this->tbl_asesmen_scoring_colour_config,'id_scoring_colour',$scoring_warna->id);
		$warna = $value;
		$i = 1;
		foreach ($scoring_warna_config->result() as $swc) {
			if ($i == $scoring_warna_config->num_rows()) {
				if($swc->min <= $value && $value <= $swc->max){
					$warna = '<div class="bg-'.$swc->name.' text-center">'.$value.'</div>';
				}
			}else{
				if($swc->min < $value && $value <= $swc->max){
					$warna = '<div class="bg-'.$swc->name.' text-center">'.$value.'</div>';
				}
			}
			$i++;
		}
		return $warna;
	}
	function check_colour_text($value,$type){
		$scoring_warna = $this->model_basic->select_where($this->tbl_asesmen_scoring_colour,'name',$type)->row();
		$scoring_warna_config = $this->model_basic->select_where($this->tbl_asesmen_scoring_colour_config,'id_scoring_colour',$scoring_warna->id);
		$warna = $value;
		$i = 1;
		foreach ($scoring_warna_config->result() as $swc) {
			if ($i == $scoring_warna_config->num_rows()) {
				if($swc->min <= $value && $value <= $swc->max){
					$warna = '<div class="bg-'.$swc->name.' text-center">'.$value.'</div>';
				}
			}else{
				if($swc->min < $value && $value <= $swc->max){
					$warna = '<div class="bg-'.$swc->name.' text-center">'.$value.'</div>';
				}
			}
			$i++;
		}
		return $warna;
	}
	function check_colour_text_dashboard($value,$type){
		$scoring_warna = $this->model_basic->select_where($this->tbl_asesmen_scoring_colour,'name',$type)->row();
		$scoring_warna_config = $this->model_basic->select_where($this->tbl_asesmen_scoring_colour_config,'id_scoring_colour',$scoring_warna->id);
		$warna = $value;
		$i = 1;
		foreach ($scoring_warna_config->result() as $swc) {
			if ($i == $scoring_warna_config->num_rows()) {
				if($swc->min <= $value && $value <= $swc->max){
					$warna = strtolower($swc->name);
				}
			}else{
				if($swc->min < $value && $value <= $swc->max){
					$warna = strtolower($swc->name);
				}
			}
			$i++;
		}
		return $warna;
	}
	function check_colour_hexa($warna_text){
		if($warna_text == 'hitam')
			return '#000';
		if($warna_text == 'merah')
			return '#ED1A23';
		if($warna_text == 'biru')
			return '#417EC1';
		if($warna_text == 'hijau')
			return '#23B14D';
		if($warna_text == 'emas')
			return '#D4AF37';
	}
	function returnJson($msg){
		echo json_encode($msg);
		exit;
	}
	function getUserJenisBisnis($user_id) {
		$user_instalasi_id = $this->model_basic->select_where($this->tbl_user_jabatan, 'user_id', $user_id)->row()->instalasi_id;
		$user_jenis_bisnis_id = $this->model_basic->select_where($this->tbl_instalasi, 'id', $user_instalasi_id)->row()->jenis_bisnis_id;

		return $user_jenis_bisnis_id;
	}
        
        function insert_visitory_history($visitor_id, $type)
        {
            $this->load->library('user_agent');
            $ip = $this->input->ip_address();
            $browser = $this->agent->agent_string();
            
            $data_insert = array(
                'type' => $type,
                'visitor_id' => $visitor_id,
                'browser' => $browser,
                'ip_address' => $ip,
                'date_login' => date('Y-m-d H:i:s', now()));
            if(!$this->model_basic->insert_all($this->tbl_visitor, $data_insert))
                    return FALSE;
            return TRUE;
        }
        
        function send_email($email_data)
        {
            $this->load->library('email');
            $this->email->to($email_data->receiver);
            $this->email->from('noreply@secinform.com', 'ASIS Auto-Reply');
            $this->email->subject($email_data->subject);
            $this->email->message($email_data->content);
            $this->email->send();
        }
        
        function email_pendaftaran($pendaftar_id) {
        $p = $this->model_basic->select_where($this->tbl_training_pendaftaran, 'id', $pendaftar_id);
        if($p->num_rows() == 1)
        {
            $pendaftar = $p->row();
            $data_email = new stdClass();
            $data_training = $this->model_basic->select_where($this->tbl_training_info, 'id', $pendaftar->training_info_id)->row();
            $data_pic = $this->model_basic->select_where($this->tbl_admin, 'id', $data_training->id_pic)->row();
            $data_email->receiver = $data_pic->email;
            $data_email->subject = 'Terdapat Data Baru Pendaftar Training '.$data_training->title;
            $data_email->content = '<html><head></head><body>'
                    . '<p><strong>Hallo '.$data_pic->realname.'</strong></p>'
                    . '<p></p>'
                    . '<p>Training '.$data_training->title.' ada pendaftar baru dengan data sbb :</p>'
                    . '<table border="0">'
                    . '<tr>'
                    . '<td>Nama Peserta</td>'
                    . '<td>:</td>'
                    . '<td>'.$pendaftar->name.'</td>'
                    . '</tr>'
                    . '<tr>'
                    . '<td>Perusahaan</td>'
                    . '<td>:</td>'
                    . '<td>'.$pendaftar->company.'</td>'
                    . '</tr>'
                    . '<tr>'
                    . '<td>Jabatan</td>'
                    . '<td>:</td>'
                    . '<td>'.$pendaftar->position.'</td>'
                    . '</tr>'
                    . '<tr>'
                    . '<td>No Handphone</td>'
                    . '<td>:</td>'
                    . '<td>'.$pendaftar->phone.'</td>'
                    . '</tr>'
                    . '<tr>'
                    . '<td>Email</td>'
                    . '<td>:</td>'
                    . '<td>'.$pendaftar->email.'</td>'
                    . '</tr>'
                    . '<tr>'
                    . '<td>Atasan</td>'
                    . '<td>:</td>'
                    . '<td>'.$pendaftar->supervisor_name.'</td>'
                    . '</tr>'
                    . '</table>'
                    . '<p></p>'
                    . '<p>Untuk Approve/Reject pendaftar, dapat mengakses ke <strong><a href="http://secinform.com/admin">Halaman Administrator ASIS</a></strong></p>'
                    . '<p></p>'
                    . '<p>Terima Kasih</p>'
                    . '<p><strong>Administrator ASIS</strong></p>'
                    . '</body></html>';
            $this->send_email($data_email);
            return TRUE;
        }
        else
            return FALSE;
    }
    
    function password_checker($password)
    {
        $uppercase = preg_match('@[A-Z]@', $password);
        $lowercase = preg_match('@[a-z]@', $password);
        $number    = preg_match('@[0-9]@', $password);
        $result = new stdClass();
        if(strlen($password) < 8)
        {
            $result->status = FALSE;
            $result->msg = 'Password must have minimum 8 characters';
        }
        else
        {
            if(!$uppercase)
            {
                $result->status = FALSE;
                $result->msg = 'Password must have one Uppercase Letter';
            }
            else
            {
                if(!$lowercase)
                {
                    $result->status = FALSE;
                    $result->msg = 'Password must have one Lowercase Letter';
                }
                else
                {
                    if(!$number)
                    {
                        $result->status = FALSE;
                        $result->msg = 'Password must have one Number';
                    }
                    else
                    {
                        $result->status = TRUE;
                    }
                }
            }
        }
        return $result;
    }
    
    function makeThumbnails($updir, $img, $width, $height) {
        $thumbnail_width = $width;
        $thumbnail_height = $height;
        $thumb_beforeword = "thumb";
        $arr_image_details = getimagesize("$updir" . "$img"); // pass id to thumb name
        $original_width = $arr_image_details[0];
        $original_height = $arr_image_details[1];
        if ($original_width > $original_height) {
            $new_width = $thumbnail_width;
            $new_height = intval($original_height * $new_width / $original_width);
        } else {
            $new_height = $thumbnail_height;
            $new_width = intval($original_width * $new_height / $original_height);
        }
        $dest_x = intval(($thumbnail_width - $new_width) / 2);
        $dest_y = intval(($thumbnail_height - $new_height) / 2);
        if ($arr_image_details[2] == 1) {
            $imgt = "ImageGIF";
            $imgcreatefrom = "ImageCreateFromGIF";
        }
        if ($arr_image_details[2] == 2) {
            $imgt = "ImageJPEG";
            $imgcreatefrom = "ImageCreateFromJPEG";
        }
        if ($arr_image_details[2] == 3) {
            $imgt = "ImagePNG";
            $imgcreatefrom = "ImageCreateFromPNG";
        }
        if ($imgt) {
            $old_image = $imgcreatefrom("$updir" . "$img");
            $new_image = imagecreatetruecolor($thumbnail_width, $thumbnail_height);
            imagecopyresized($new_image, $old_image, $dest_x, $dest_y, 0, 0, $new_width, $new_height, $original_width, $original_height);
            $imgt($new_image, "$updir" . "$thumb_beforeword" . "$img");
        }
    }

	function send_email_telnet($recipient)
	{
		$this->load->library('email');
		
		$smtpadrs = "smtpor.astra.co.id";
		$smtpport = 25;
		
		ini_set("SMTP", "smtpor.astra.co.id");
		ini_set("smtp_port", 25);
		
		$sender = "secd@ai.astra.co.id";
		
		if (array_key_exists('user', $recipient))
		{
			if ($recipient['user'] != "")
			{
				$subject = "[SECD] Citizen Security Submission ".$recipient['ticket_number'];
				$body = "Dear Mr. / Ms. ".$recipient['user_realname'].", \n\n";
				$body .= "You have created a new Citizen Security submission with ticket number ".$recipient['ticket_number'].". \n\n";
				$body .= "Submission Description: \n";
				$body .= "Username: ".$recipient['user_username']." \n";
				$body .= "Email   : ".$recipient['user_email']." \n";
				$body .= "Place   : ".$recipient['place']." \n";
				$body .= "Notes   : ".$recipient['notes']." \n";
				$body .= "Datetime: ".$recipient['datetime']." \n";
				$body .= " \n";
				$body .= "Your submission will be followed up soon. You can check the progress status at Citizen Security Menu. \n\n";
				$body .= "Regards, \n\n";
				$body .= "Security Information \n";
				$body .= "Email: security.information@ai.astra.co.id \n";
				$body .= "Ext.512\n\n";
				$body .= "*This is an automated email sent by system. \n";
				$body .= "*PLEASE DO NOT REPLY TO THIS EMAIL. \n\n";
				$body .= "======================================================================================== \n\n";
				$body .= "Dear Bapak / Ibu ".$recipient['user_realname'].", \n\n";
				$body .= "Anda telah membuat laporan Citizen Security baru dengan nomor tiket ".$recipient['ticket_number'].". \n\n";
				$body .= "Deskripsi Laporan: \n";
				$body .= "Username     : ".$recipient['user_username']." \n";
				$body .= "Email        : ".$recipient['user_email']." \n";
				$body .= "Lokasi       : ".$recipient['place']." \n";
				$body .= "Catatan      : ".$recipient['notes']." \n";
				$body .= "Tanggal Waktu: ".$recipient['datetime']." \n";
				$body .= " \n";
				$body .= "Laporan Anda akan segera ditindaklanjuti. Anda dapat memonitor status laporan melalui menu Citizen Security. \n";
				$body .= "Regards, \n\n";
				$body .= "Security Information \n";
				$body .= "Email: security.information@ai.astra.co.id \n";
				$body .= "Ext.512\n\n";
				$body .= "*Ini adalah email otomatis dari sistem. \n";
				$body .= "*MOHON TIDAK MEMBALAS EMAIL INI. \n\n";
				
				$fp = fsockopen("smtpor.astra.co.id", 25, $errno, $errstr, 30);
				if (!$fp) {
					// echo "$errstr($errno)";die();
				} else {
					$out = "telnet " . $smtpadrs . " " . $smtpport . "\r\n";
					$out .= "ehlo " . $smtpadrs . "\r\n";
					$out .= "mail from: " . $sender . "\r\n";
					$out .= "rcpt to: " . $recipient['user'] . "\r\n";
					$out .= "data\r\n";
					$out .= "To: <" . $recipient['user'] . ">\r\n";
					$out .= "Subject: " . $subject . "\r\n";
					$out .= $body . "\r\n\r\n";
					$out .= ".\r\n";
					$out .= "quit\r\n";
					
					fwrite($fp, $out);
					while (!feof($fp)) {
						fgets($fp,128);
					}
					fclose($fp);
				}
			}
		}
		
		if (array_key_exists('admin', $recipient))
		{
			if (count($recipient['admin']) > 0)
			{
				foreach ($recipient['admin'] as $admin_recipient)
				{
					$subject = "[SECD] Citizen Security Submission ".$recipient['ticket_number'];
					$body = "Dear Mr. / Ms. Administrator, \n\n";
					$body .= "This is an automated email to inform you that \n";
					$body .= "there is a new Citizen Security submission with details as follows. \n\n";
					$body .= "Submission Description: \n";
					$body .= "Ticket Number: ".$recipient['ticket_number']." \n";
					$body .= "Username     : ".$recipient['user_username']." \n";
					$body .= "Email        : ".$recipient['user_email']." \n";
					$body .= "Place        : ".$recipient['place']." \n";
					$body .= "Notes        : ".$recipient['notes']." \n";
					$body .= "Datetime     : ".$recipient['datetime']." \n";
					$body .= " \n";
					$body .= "You can see the full list and details of Citizen Security submission \n";
					$body .= "in the Citizen Security menu. \n\n";
					$body .= "Regards, \n\n";
					$body .= "Security Information \n";
					$body .= "Email: security.information@ai.astra.co.id \n";
					$body .= "Ext.512\n\n";
					$body .= "*This is an automated email sent by system. \n";
					$body .= "*PLEASE DO NOT REPLY TO THIS EMAIL. \n\n";
					$body .= "======================================================================================== \n\n";
					$body .= "Ini adalah email otomatis untuk memberitahukan bahwa \n";
					$body .= "terdapat laporan Citizen Security baru dengan detail sebagai berikut. \n\n";
					$body .= "Deskripsi Laporan: \n";
					$body .= "Nomor Tiket  : ".$recipient['ticket_number']." \n";
					$body .= "Username     : ".$recipient['user_username']." \n";
					$body .= "Email        : ".$recipient['user_email']." \n";
					$body .= "Lokasi       : ".$recipient['place']." \n";
					$body .= "Catatan      : ".$recipient['notes']." \n";
					$body .= "Tanggal Waktu: ".$recipient['datetime']." \n";
					$body .= " \n";
					$body .= "Anda bisa melihat daftar lengkap dan detail laporan Citizen Submission \n";
					$body .= "di menu Citizen Security. \n\n";
					$body .= "Regards, \n\n";
					$body .= "Security Information \n";
					$body .= "Email: security.information@ai.astra.co.id \n";
					$body .= "Ext.512\n\n";
					$body .= "*Ini adalah email otomatis dari sistem. \n";
					$body .= "*MOHON TIDAK MEMBALAS EMAIL INI. \n\n";
					
					$fp = fsockopen("smtpor.astra.co.id", 25, $errno, $errstr, 30);
					if (!$fp) {
						// echo "$errstr($errno)";die();
					} else {
						$out = "telnet " . $smtpadrs . " " . $smtpport . "\r\n";
						$out .= "ehlo " . $smtpadrs . "\r\n";
						$out .= "mail from: " . $sender . "\r\n";
						$out .= "rcpt to: " . $admin_recipient->email . "\r\n";
						$out .= "data\r\n";
						$out .= "To: <" . $admin_recipient->email . ">\r\n";
						$out .= "Subject: " . $subject . "\r\n";
						$out .= $body . "\r\n\r\n";
						$out .= ".\r\n";
						$out .= "quit\r\n";
						
						fwrite($fp, $out);
						while (!feof($fp)) {
							fgets($fp,128);
						}
						fclose($fp);
					}
				}
			}
		}
	}

	//send email dev
	function send_email_lostfound($recipient)
	{
		$this->load->library('email');
		
		$smtpadrs = "smtpor.astra.co.id";
		$smtpport = 25;
		
		ini_set("SMTP", "smtpor.astra.co.id");
		ini_set("smtp_port", 25);
		
		$sender = "secd@ai.astra.co.id";
		
		if (array_key_exists('admin', $recipient))
		{
			if (count($recipient['admin']) > 0)
			{
				foreach ($recipient['admin'] as $admin_recipient)
				{
					if($recipient['type']==1)
					{
						$type = "Lost";
					}
					else
					{
						$type = "Found";
					}
					$subject = "[SECD] Lost And Found Submission ".$recipient['ticket_number'];
					$body = "Dear Mr. / Ms. Administrator, \n\n";
					$body .= "This is an automated email to inform you that there is a new Lost And Found \n";
					$body .= "Submission with details as follows. \n\n";
					$body .= "Submission Description: \n";
					$body .= "Ticket Number  : ".$recipient['ticket_number']." \n";
					$body .= "Username       : ".$recipient['user_username']." \n";
					$body .= "Email          : ".$recipient['user_email']." \n";
					$body .= "Type           : ".$type." \n";
					$body .= "Things         : ".$recipient['item_type']." \n";
					$body .= "Color of Things: ".$recipient['color']." \n";
					$body .= "Title          : ".$recipient['title']." \n";
					$body .= "Detail         : ".$recipient['detail']." \n";
					$body .= " \n";
					$body .= "You can see the full list and details of Lost And Found submission in the Lost And Found menu. \n\n";
					$body .= "Regards, \n\n";
					$body .= "Security Information \n";
					$body .= "Email: security.information@ai.astra.co.id \n";
					$body .= "Ext.512\n\n";
					$body .= "*This is an automated email sent by system. \n";
					$body .= "*PLEASE DO NOT REPLY TO THIS EMAIL. \n\n";
					$body .= "======================================================================================== \n\n";
					$body .= "Ini adalah email otomatis untuk memberitahukan bahwa terdapat laporan Lost \n";
					$body .= "And Found baru dengan detail sebagai berikut.\n\n";
					$body .= "Deskripsi Laporan: \n";
					$body .= "Nomor Tiket  : ".$recipient['ticket_number']." \n";
					$body .= "Username     : ".$recipient['user_username']." \n";
					$body .= "Email        : ".$recipient['user_email']." \n";
					$body .= "Tipe         : ".$type." \n";
					$body .= "Barang       : ".$recipient['item_type']." \n";
					$body .= "Warna Barang : ".$recipient['color']." \n";
					$body .= "Judul        : ".$recipient['title']." \n";
					$body .= "Detail       : ".$recipient['detail']." \n";
					$body .= " \n";
					$body .= "Anda bisa melihat daftar lengkap dan detail laporan Lost And Found di menu Lost And Found. \n\n";
					$body .= "Regards, \n\n";
					$body .= "Security Information \n";
					$body .= "Email: security.information@ai.astra.co.id \n";
					$body .= "Ext.512\n\n";
					$body .= "*Ini adalah email otomatis dari sistem. \n";
					$body .= "*MOHON TIDAK MEMBALAS EMAIL INI. \n\n";

					$fp = fsockopen("smtpor.astra.co.id", 25, $errno, $errstr, 30);
					if (!$fp) {
						//echo "Error email".$errstr. "\n";die();
					} else {
						$out = "telnet " . $smtpadrs . " " . $smtpport . "\r\n";
						$out .= "ehlo " . $smtpadrs . "\r\n";
						$out .= "mail from: " . $sender . "\r\n";
						$out .= "rcpt to: " . $admin_recipient->email . "\r\n";
						$out .= "data\r\n";
						$out .= "To: <" . $admin_recipient->email . ">\r\n";
						$out .= "Subject: " . $subject . "\r\n";
						$out .= $body . "\r\n\r\n";
						$out .= ".\r\n";
						$out .= "quit\r\n";
						

						fwrite($fp, $out);
						//print_r($errstr);die();
						while (!feof($fp)) {
							fgets($fp,128);
						}
						fclose($fp);
					}
				}
			}
		}
	}

	function send_email_lostfound_acc($recipient)
	{
		$this->load->library('email');
		
		$smtpadrs = "smtpor.astra.co.id";
		$smtpport = 25;
		
		ini_set("SMTP", "smtpor.astra.co.id");
		ini_set("smtp_port", 25);
		
		$sender = "secd@ai.astra.co.id";
		
		if (array_key_exists('user', $recipient))
		{
			if ($recipient['user'] != "")
			{
				foreach ($recipient['data'] as $data_recipient)
				{
					foreach ($recipient['user'] as $user_recipient)
					{
						if($recipient['type']==1)
						{
							$type = "Lost";
						}
						else
						{
							$type = "Found";
						}
						$subject = "[SECD] Lost And Found Submission ".$data_recipient->ticket_number;
						$body = "Dear Mr. / Ms. ".$user_recipient->realname.", \n\n";
						$body .= "You have successful created a new Lost and Found submission with ticket number ".$data_recipient->ticket_number.". \n\n";
						$body .= "Submission Description: \n";
						$body .= "Username       : ".$user_recipient->username." \n";
						$body .= "Email          : ".$user_recipient->email." \n";
						$body .= "Type           : ".$type." \n";
						$body .= "Things         : ".$data_recipient->item_type." \n";
						$body .= "Color of Things: ".$data_recipient->color." \n";
						$body .= "Title          : ".$data_recipient->title." \n";
						$body .= "Detail         : ".$data_recipient->detail." \n";
						$body .= "\n";
						$body .= "Your submission will be followed up soon.\nYou can check the progress status at Lost and Found menu. \n\n";
						$body .= "Regards, \n\n";
						$body .= "Security Information \n";
						$body .= "Email: security.information@ai.astra.co.id \n";
						$body .= "Ext.512\n\n";
						$body .= "*This is an automated email sent by system. \n";
						$body .= "*PLEASE DO NOT REPLY TO THIS EMAIL. \n\n";
						$body .= "======================================================================================== \n\n";
						$body .= "Dear Bapak/Ibu ".$user_recipient->realname.", \n\n";
						$body .= "Anda telah berhasil membuat laporan Lost and Found baru dengan nomor tiket".$data_recipient->ticket_number.". \n\n";
						$body .= "Deskripsi Laporan: \n";
						$body .= "Username    : ".$user_recipient->username." \n";
						$body .= "Email       : ".$user_recipient->email." \n";
						$body .= "Tipe        : ".$type." \n";
						$body .= "Barang      : ".$data_recipient->item_type." \n";
						$body .= "Warna Barang: ".$data_recipient->color." \n";
						$body .= "Judul       : ".$data_recipient->title." \n";
						$body .= "Detail      : ".$data_recipient->detail." \n";
						$body .= "\n";
						$body .= "Laporan Anda akan segera ditindaklanjuti.\n";
						$body .= "Anda dapat memonitor status laporan melalui menu Lost and Found. \n\n";
						$body .= "Regards, \n\n";
						$body .= "Security Information \n";
						$body .= "Email: security.information@ai.astra.co.id \n";
						$body .= "Ext.512\n\n";
						$body .= "*Ini adalah email otomatis dari sistem. \n";
						$body .= "*MOHON TIDAK MEMBALAS EMAIL INI. \n\n";

						$fp = fsockopen("smtpor.astra.co.id", 25, $errno, $errstr, 30);
						if (!$fp) {
							//echo "$errstr($errno)";die();
							echo "Error email".$errstr. "\n";die();
						} else {
							$out = "telnet " . $smtpadrs . " " . $smtpport . "\r\n";
							$out .= "ehlo " . $smtpadrs . "\r\n";
							$out .= "mail from: " . $sender . "\r\n";
							$out .= "rcpt to: " . $user_recipient->email . "\r\n";
							$out .= "data\r\n";
							$out .= "To: <" . $user_recipient->email . ">\r\n";
							$out .= "Subject: " . $subject . "\r\n";
							$out .= $body . "\r\n\r\n";
							$out .= ".\r\n";
							$out .= "quit\r\n";

							fwrite($fp, $out);
							while (!feof($fp)) 
							{
								fgets($fp,128);
							}
							fclose($fp);
						}
					}
				}
			}
			else
			{
				echo'<script type="text/javascript">
				    alert("Email not send");
				</script>';
			}
		}
	}

	function send_email_lostfound_rjc($recipient)
	{
		$this->load->library('email');
		
		$smtpadrs = "smtpor.astra.co.id";
		$smtpport = 25;
		
		ini_set("SMTP", "smtpor.astra.co.id");
		ini_set("smtp_port", 25);
		
		$sender = "secd@ai.astra.co.id";
		
		if (array_key_exists('user', $recipient))
		{
			if ($recipient['user'] != "")
			{
				foreach ($recipient['data'] as $data_recipient)
				{
					foreach ($recipient['user'] as $user_recipient)
					{
						if($recipient['type']==1)
						{
							$type = "Lost";
						}
						else
						{
							$type = "Found";
						}
						$subject = "[SECD] Lost And Found Submission ".$data_recipient->ticket_number;
						$body = "Dear Mr. / Ms. ".$user_recipient->realname.", \n\n";
						$body .= "Your Lost and Found submission with ticket number ".$data_recipient->ticket_number." is rejected because the data is not appropriate. \n\n";
						$body .= "Submission Description: \n";
						$body .= "Username       : ".$user_recipient->username." \n";
						$body .= "Email          : ".$user_recipient->email." \n";
						$body .= "Type           : ".$type." \n";
						$body .= "Things         : ".$data_recipient->item_type." \n";
						$body .= "Color of Things: ".$data_recipient->color." \n";
						$body .= "Title          : ".$data_recipient->title." \n";
						$body .= "Detail         : ".$data_recipient->detail." \n";
						$body .= "\n";
						$body .= "Regards, \n\n";
						$body .= "Security Information \n";
						$body .= "Email: security.information@ai.astra.co.id \n";
						$body .= "Ext.512\n\n";
						$body .= "*This is an automated email sent by system. \n";
						$body .= "*PLEASE DO NOT REPLY TO THIS EMAIL. \n\n";
						$body .= "======================================================================================== \n\n";
						$body .= "Dear Bapak/Ibu ".$user_recipient->realname.", \n\n";
						$body .= "Laporan Lost and Found dengan nomor tiket ".$data_recipient->ticket_number." ditolak karena data tidak sesuai. \n\n";
						$body .= "Deskripsi Laporan: \n";
						$body .= "Username    : ".$user_recipient->username." \n";
						$body .= "Email       : ".$user_recipient->email." \n";
						$body .= "Tipe        : ".$type." \n";
						$body .= "Barang      : ".$data_recipient->item_type." \n";
						$body .= "Warna Barang: ".$data_recipient->color." \n";
						$body .= "Judul       : ".$data_recipient->title." \n";
						$body .= "Detail      : ".$data_recipient->detail." \n";
						$body .= "\n";
						$body .= "Regards, \n\n";
						$body .= "Security Information \n";
						$body .= "Email: security.information@ai.astra.co.id \n";
						$body .= "Ext.512\n\n";
						$body .= "*Ini adalah email otomatis dari sistem. \n";
						$body .= "*MOHON TIDAK MEMBALAS EMAIL INI. \n\n";

						$fp = fsockopen("smtpor.astra.co.id", 25, $errno, $errstr, 30);
						if (!$fp) {
							//echo "$errstr($errno)";die();
							echo "Error email".$errstr. "\n";die();
						} else {
							$out = "telnet " . $smtpadrs . " " . $smtpport . "\r\n";
							$out .= "ehlo " . $smtpadrs . "\r\n";
							$out .= "mail from: " . $sender . "\r\n";
							$out .= "rcpt to: " . $user_recipient->email . "\r\n";
							$out .= "data\r\n";
							$out .= "To: <" .$user_recipient->email. ">\r\n";
							$out .= "Subject: " . $subject . "\r\n";
							$out .= $body . "\r\n\r\n";
							$out .= ".\r\n";
							$out .= "quit\r\n";

							fwrite($fp, $out);
							while (!feof($fp)) 
							{
								fgets($fp,128);
							}
							fclose($fp);
						}
					}
				}
			}
			else
			{
				echo'<script type="text/javascript">
				    alert("Email not send");
				</script>';
			}
		}
	}

	function send_email_lostfound_cmp($recipient)
	{
		$this->load->library('email');
		
		$smtpadrs = "smtpor.astra.co.id";
		$smtpport = 25;
		
		ini_set("SMTP", "smtpor.astra.co.id");
		ini_set("smtp_port", 25);
		
		$sender = "secd@ai.astra.co.id";
		
		if (array_key_exists('user', $recipient))
		{
			if ($recipient['user'] != "")
			{
				foreach ($recipient['data'] as $data_recipient)
				{
					foreach ($recipient['user'] as $user_recipient)
					{
						if($recipient['type']==1)
						{
							$type = "Lost";
						}
						else
						{
							$type = "Found";
						}
						$subject = "[SECD] Lost And Found Submission ".$data_recipient->ticket_number;
						$body = "Dear Mr. / Ms. ".$user_recipient->realname.", \n\n";
						$body .= "Your Lost and Found submission with ticket number ".$data_recipient->ticket_number." is completed. \n\n";
						$body .= "Submission Description: \n";
						$body .= "Username       : ".$user_recipient->username." \n";
						$body .= "Email          : ".$user_recipient->email." \n";
						$body .= "Type           : ".$type." \n";
						$body .= "Things         : ".$data_recipient->item_type." \n";
						$body .= "Color of Things: ".$data_recipient->color." \n";
						$body .= "Title          : ".$data_recipient->title." \n";
						$body .= "Detail         : ".$data_recipient->detail." \n";
						$body .= "\n";
						$body .= "Regards, \n\n";
						$body .= "Security Information \n";
						$body .= "Email: security.information@ai.astra.co.id \n";
						$body .= "Ext.512\n\n";
						$body .= "*This is an automated email sent by system. \n";
						$body .= "*PLEASE DO NOT REPLY TO THIS EMAIL. \n\n";
						$body .= "======================================================================================== \n\n";
						$body .= "Dear Bapak/Ibu ".$user_recipient->realname.", \n\n";
						$body .= "Laporan Lost and Found dengan nomor tiket ".$data_recipient->ticket_number." sudah selesai diproses. \n\n";
						$body .= "Deskripsi Laporan: \n";
						$body .= "Username    : ".$user_recipient->username." \n";
						$body .= "Email       : ".$user_recipient->email." \n";
						$body .= "Tipe        : ".$type." \n";
						$body .= "Barang      : ".$data_recipient->item_type." \n";
						$body .= "Warna Barang: ".$data_recipient->color." \n";
						$body .= "Judul       : ".$data_recipient->title." \n";
						$body .= "Detail      : ".$data_recipient->detail." \n";
						$body .= "\n";
						$body .= "Regards, \n\n";
						$body .= "Security Information \n";
						$body .= "Email: security.information@ai.astra.co.id \n";
						$body .= "Ext.512\n\n";
						$body .= "*Ini adalah email otomatis dari sistem. \n";
						$body .= "*MOHON TIDAK MEMBALAS EMAIL INI. \n\n";

						$fp = fsockopen("smtpor.astra.co.id", 25, $errno, $errstr, 30);
						if (!$fp) {
							//echo "$errstr($errno)";die();
							echo "Error email".$errstr. "\n";die();
						} else {
							$out = "telnet " . $smtpadrs . " " . $smtpport . "\r\n";
							$out .= "ehlo " . $smtpadrs . "\r\n";
							$out .= "mail from: " . $sender . "\r\n";
							$out .= "rcpt to: " . $user_recipient->email . "\r\n";
							$out .= "data\r\n";
							$out .= "To: <" .$user_recipient->email. ">\r\n";
							$out .= "Subject: " . $subject . "\r\n";
							$out .= $body . "\r\n\r\n";
							$out .= ".\r\n";
							$out .= "quit\r\n";

							fwrite($fp, $out);
							while (!feof($fp)) 
							{
								fgets($fp,128);
							}
							fclose($fp);
						}
					}
				}
			}
			else
			{
				echo'<script type="text/javascript">
				    alert("Email not send");
				</script>';
			}
		}
	}

	/* 
		Created by Alvin (atsalvin0017), 
		Date: 2019-08-25 10:50 AM 
		Notes: for send mail to division head case URF AISS Assessment 2015
	*/
	function send_email_division_head($recipient)
	{
		$this->load->library('email');
		
		$smtpadrs = "smtpor.astra.co.id";
		$smtpport = 25;
		
		ini_set("SMTP", "smtpor.astra.co.id");
		ini_set("smtp_port", 25);
		
		$sender = "secd@ai.astra.co.id";

		$subject = 'You have new inbox in Aiss (Astra information security system)';
		$body = "Dear Mr./Ms. ".$recipient['name'].", \n";
		$body .= "\n";
		$body .= "You have new request for approval in Astra Information Security System (AISS) \n";
		$body .= "\n";
		$body .= "Please check new request by follow the link below: \n";
		$body .= "\n";
		$body .= base_url()." \n";
		$body .= "\n";
		$body .= "You can check the data for further detail in inbox Astra Information Security System (AISS) \n";
		$body .= "\n";
		$body .= "Regards, \n";
		$body .= "\n";
		$body .= "============================================================= \n";
		$body .= "\n";
		$body .= "Dear Bapak/Ibu. ".$recipient['name'].", \n";
		$body .= "\n";
		$body .= "Anda mendapat permintaan untuk persetujuan di Astra Information Security System (AISS) \n";
		$body .= "\n";
		$body .= "Silakan memeriksa permintaan anda dengan melalui link di bawah ini: \n";
		$body .= "\n";
		$body .= base_url()." \n";
		$body .= "\n";
		$body .= "Anda dapat memeriksa detail data lebih lanjut di inbox Astra Information Security System (AISS) \n";
		$body .= "\n";
		$body .= "Regards,";

		$fp = fsockopen("smtpor.astra.co.id", 25, $errno, $errstr, 30);
		if (!$fp) {
			echo "Error email".$errstr. "\n";
			die();
		} else {
			$out = "telnet " . $smtpadrs . " " . $smtpport . "\r\n";
			$out .= "ehlo " . $smtpadrs . "\r\n";
			$out .= "mail from: " . $sender . "\r\n";
			$out .= "rcpt to: " . $recipient['email'] . "\r\n";
			$out .= "data\r\n";
			$out .= "To: <" . $recipient['email'] . ">\r\n";
			$out .= "Subject: " . $subject . "\r\n";
			$out .= $body . "\r\n\r\n";
			$out .= ".\r\n";
			$out .= "quit\r\n";

			fwrite($fp, $out);
			//print_r($errstr);die();
			while (!feof($fp)) {
				fgets($fp,128);
			}
			fclose($fp);
		}
	}

	/* 
		Created by Alvin (atsalvin0017), 
		Date: 2019-09-03 15:26 PM 
		Notes: for pagination library custom
	*/
	function pagination_render($sql, $columns=array()) 
	{
        $filter_a = $this->input->post('filter_a');
        if ($filter_a) {
	        $show_r = $this->input->post('show_r');
			$sort_c = $this->input->post('sort_c');
	        $sort_t =  $this->input->post('sort_t');
	        $search_t =  $this->input->post('search_t');
	    }
		$render = [];

		// get parameter from url 
		// $base_url = base_url().self::modul.'/'.$data['controller'].'/'.$data['function'];
		// $uri_segment = explode('/', $base_url);
        // $uri_segment = count($uri_segment)-3;
        $base_url = $_SERVER['REQUEST_URI'];
        $base_url = explode('/', $base_url);
        if (is_numeric($base_url[(count($base_url)-1)])==true) unset($base_url[(count($base_url)-1)]);
        unset($base_url[0]);
        unset($base_url[1]);
        $uri_segment = count($base_url)+1; 
        $base_url = implode('/', $base_url);
        $base_url = base_url().$base_url;

        // Generate Show per page
        $per_page = 10; // show data per page
        if (isset($show_r)) $per_page = $show_r;
        $render['show_r'] = $per_page;

		// Generate Query Sql
		// $sql = 'SELECT * FROM ('.$sql.') z WHERE 1=1 ';
		// Generate Order
        if (isset($sort_c) and $sort_c!='') {
        	$render['sort_c'] = $sort_c;
        	$sort_c = $columns[$sort_c];
        } else {
        	$sort_c = 'created_date';
        	$render['sort_c'] = null;
        }
        if (!isset($sort_t) or $sort_t=='') {
        	$sort_t = 'desc';
        	$render['sort_t'] = null;
        } else $render['sort_t'] = $sort_t;
        // Generate Search
        $swhere = '';
        if (isset($search_t)) {
            $swhere .= 'AND (';
            foreach ($columns as $k => $v) {
                if ($k>0) $swhere .= ' OR ';
                $swhere .= $v.' LIKE "%'.$search_t.'%"';
            } 
            $swhere .= ' )';
        	$render['search_t'] = $search_t;
        } else $render['search_t'] = null;
        $sql = '
            SELECT * FROM (
                '.$sql.'
            ) z 
            WHERE 1=1 '.$swhere.'
            ORDER BY z.'.$sort_c.' '.$sort_t.'
        ';

		// Get total data on table
        $total_rows = $this->model_basic->select_count_by_query($sql);
        // $total_rows = $this->model_basic->get_count($table);

        if (isset($show_r) and $show_r=='All') $per_page = $total_rows;

        // Generate Pagination by library
		$this->load->library('pagination');
        $config['base_url'] = $base_url;
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $per_page;
        $config['uri_segment'] = $uri_segment;

        // $config['num_links'] = 2;
        $config['first_link'] = '<<';
        $config['last_link'] = '>>';
        $config['next_link'] = '<span class="btn btn-sm btn-light">&gt;</span>';
        $config['prev_link'] = '<span class="btn btn-sm btn-light">&lt;</span>';
        $config['cur_tag_open'] = '<b class="btn btn-sm btn-light">';
        $config['cur_tag_close'] = '</b>';  
        // $config['_attributes'] = '';
        $config['full_tag_open'] = '<p>';
        $config['full_tag_close'] ='</p>';
        $config['num_tag_open'] = '<span class="btn btn-sm btn-light">';
        $config['num_tag_close'] = '</span>';
        // $config['next_tag_open'] = '';
        // $config['next_tag_close'] = '';
        $config['prev_tag_open'] = '';
        $config['prev_tag_close'] = '';
        $config['first_tag_open'] = '<span class="btn btn-sm btn-light">';
        $config['first_tag_close'] = '</span>';
        $config['last_tag_open'] = '<span class="btn btn-sm btn-light">';
        $config['last_tag_close'] = '</span>';
        $this->pagination->initialize($config);

        // render pagination number
        $render['links'] = $this->pagination->create_links(); 
        // start get data from
        $offset = $this->uri->segment($uri_segment); 
        if (!$offset) $offset = 0; else if ($offset%$per_page>0) redirect($base_url);
        // get data table by limit
        $sql .= 'LIMIT '.$offset.', '.$per_page;
        $gdata = $this->model_basic->select_all_by_query($sql);
        foreach ($gdata as $r) {
        	if (isset($r->created_date)) $r->created_date = date('M d, Y, (H:i)', strtotime($r->created_date));
        	if (isset($r->modified_date)) $r->modified_date = date('M d, Y, (H:i)', strtotime($r->modified_date));
        }
        $render['data'] = $gdata;
        // $render['data'] = $this->model_basic->get_paging($table, $per_page, $offset, 'created_date', 'desc');

        // Generate Sorting column
        for ($i=0; $i < count($columns); $i++) $columns[$i] = ucwords(str_replace('_', ' ', $columns[$i]), ' ');
        $render['sort'] = $columns;

        return $render;
	}

	/* 
		Created by Alvin (atsalvin0017), 
		Date: 2019-08-21 14:14 
		Notes: for development API
	*/
	function res($param=[])
	{
		$_p = $param;
		if (is_array($param)==false) {
			$res = $param;
			$param = [];
			$param['data'] = $res;
		}
		if (!isset($param['data']) and $_p!==null)
			$param['data'] = $param;
		$this->data = $param;

		return $this;
	}

	function success($http_code = 200)
	{
		if (OAUTH_TYPE!='mobile') {
			// Update token expire in
			$class_name = $this->router->fetch_class(); // oauth
			if ($class_name!='oauth' and !empty($this->tokenkey)) {
				$token = $this->get_token_wso2_by_tokenkey($this->tokenkey, true);
		        $update = [];
		        $update['expires_in'] = time()+self::wso2_expired_time;
				
				// start::Request to wso2
				$response_wso2_request = json_decode($token->response_wso2_request);

		        $auth = [];
		        if (self::wso2_env=='development') {
		            $auth['header']['Authorization'] = 'Basic b1JBRDJJbkpoWW5yM2JPZTJpQ3dpYVh6VTk4YTpLUE9EU0l2VW92UXV1S3VTMkxxWXlYZk1uNk1h';
		            $auth['username'] = 'oRAD2InJhYnr3bOe2iCwiaXzU98a';
		            $auth['password'] = 'KPODSIvUovQuuKuS2LqYyXfMn6Ma';
		        } else
		        if (self::wso2_env=='production') {
		            $auth['header']['Authorization'] = 'Basic TkNwWjVBejVZc1h5TWRkNlVXUGwxSldqclVVYTpPejdvTG0wN0lTX2FlYTl2YlR2N2h2ZDZOeW9h';
		            $auth['username'] = 'NCpZ5Az5YsXyMdd6UWPl1JWjrUUa';
		            $auth['password'] = 'Oz7oLm07IS_aea9vbTv7hvd6Nyoa';
		        }

	            $body = [];
	            $body['grant_type'] = 'refresh_token';
	            $body['refresh_token'] = $response_wso2_request->refresh_token;

	            $request = httprequest::post(self::wso2_url.'/token', $auth, $body, 1);
	            $response = json_decode($request);
	            if ($response) {
					if (isset($response->access_token)) {
						// $response->expires_in = time()+$response->expires_in; // not used
						// $update['expires_in'] = $response->expires_in; // not used
						// $update['expires_in'] = time()+$response->expires_in; // original 3600 seconds
						$update['response_wso2_request'] = json_encode($response);
					}
	            }
				// ::end

		        $this->model_basic->update($this->tbl_esams_log_wso2_token,$update,'id',$token->id);
		    }
		}

		return $this->output
		        	->set_content_type('application/json')
		        	->set_status_header($http_code)
		        	->set_output(json_encode($this->_response($this->data, $http_code)));
	} 

	function error($http_code = 500)
	{
		return $this->output
		        	->set_content_type('application/json')
		        	// ->set_status_header($http_code)
		        	->set_status_header(200)
		        	->set_output(json_encode($this->_response($this->data, $http_code)));
	} 

	function notfound($http_code = 404)
	{
		return $this->output
		        	->set_content_type('application/json')
		        	->set_status_header($http_code)
		        	->set_output(json_encode($this->_response($this->data, $http_code)));
	} 

	function forbidden($http_code = 403)
	{
		return $this->output
		        	->set_content_type('application/json')
		        	->set_status_header($http_code)
		        	->set_output(json_encode($this->_response($this->data, $http_code)));
	}

	function unauthorized($http_code = 401)
	{
		return $this->output
		        	->set_content_type('application/json')
		        	->set_status_header($http_code)
		        	->set_output(json_encode($this->_response($this->data, $http_code)));
	}

	function _response($data, $http_code)
	{
		$_response = [];
		// $_response['status'] = $http_code==200?true:false;
		$_response['Acknowledge'] = $http_code==200?1:0;
		// $_response['http_code'] = $http_code;
		// $_response['elapsed_time'] = $this->convert_elapsed_time(microtime(true)-CI_START);
		// $_response['memory_usage'] = $this->convert_memory_usage(memory_get_usage(true));
		// $_response['message'] = (isset($data['message'])?$data['message']:($http_code==200?'Success':'Error'));
		$_response['Message'] = (isset($data['message'])?$data['message']:($http_code==200?'Success':'Error'));
		$_response['Version'] = APP_VERSION;
		$_response['Build'] = APP_BUILD;
		$_response['Data'] = (isset($data['message']))?(isset($data['data_error'])?$data['data_error']:null):$data['data'];

		$insert = [];
		$insert['method'] = $_SERVER['REQUEST_METHOD'];
		$insert['uri'] = $_SERVER['REQUEST_URI'];
		$insert['token_key'] = $this->tokenkey;
		$insert['http_code'] = $http_code;
		$insert['request_post'] = json_encode($_POST);
		$insert['request_get'] = json_encode($_GET);
		$insert['message'] = $_response['Message'];
		$insert['response'] = json_encode($_response['Data']);
		$insert['date_created'] = date('Y-m-d H:i:s');
		$this->model_basic->insert_all($this->tbl_esams_log_api,$insert);

		return $_response;
	}

	function convert_elapsed_time($time)
    {
        return number_format($time, 3).' seconds';
    }

    function convert_memory_usage($size)
    {
        $unit=array('Byte','KB','MB','GB','TB','PB');
        return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
    }

    function validation_msg($msg) 
    {
    	$msg = trim(strip_tags($msg));
        $msg = str_replace("\n",'',$msg);
        $msg = explode('.', $msg);
        unset($msg[(count($msg)-1)]);
        if (is_array($msg)) {
        	if (count($msg)>0) $msg = $msg[0];
        }
        return $msg;
    }

    function check_token($tokenkey)
    {
    	$tokenkey = explode(' ', $tokenkey);
    	if (count($tokenkey) < 2 or $tokenkey[0] != 'Bearer') 
    		show_403_api('Please provide correct token!');
    	$tokenkey = $tokenkey[1];
    	return $tokenkey;
    }

    function get_token_wso2_by_tokenkey_old($access_token, $skip=false)
    {
    	// $sql = 'SELECT id, user_id, JSON_EXTRACT(access_token, "$.expires_in") as expires_in FROM '.$this->tbl_esams_log_wso2_token.' WHERE 1=1 AND JSON_EXTRACT(access_token, "$.access_token") = "'.$access_token.'" AND delete_flag = 0';
    	$sql = 'SELECT * FROM '.$this->tbl_esams_log_wso2_token.' WHERE 1=1 AND access_token = "'.$access_token.'" AND delete_flag = 0';
        $token = $this->model_basic->select_row_by_query($sql);
        if ($token && $token->user_id==null) {
        	$user = $this->model_basic->select_where($this->tbl_user, 'email', $token->email)->row();
        		if ($user) $token->user_id = $user->id;
        }
        
        if ($skip==true)
        	return $token;

    	// start::WSO2
    	$class_name = $this->router->fetch_class(); // oauth
    	if ($class_name!='oauth') {
        	$auth = [];
            $auth['header']['Authorization'] = 'Basic U3lzRGlnaXRhbFBhdHJvbDpVc2VyU3lzdGVtQDEyMw==';
            $auth['username'] = 'SysDigitalPatrol';
            $auth['password'] = 'UserSystem@123';

            $body = [];
            if (OAUTH_TYPE=='mobile') $body['token'] = $access_token;
            else $body['token'] = json_decode($token->response_wso2_request)->access_token;

            $request = httprequest::post(self::wso2_url.'/introspect', $auth, $body, 1);
            $response = json_decode($request);
            if (!$response) return null;
            if ($response->active==false) return null;
        }
        // ::end

        return $token;
    }

    // EHC for mobile
    function get_token_wso2_by_tokenkey($access_token)
    {
    	$auth = [];
        $auth['header']['Authorization'] = 'Bearer '.$access_token;

        $request = httprequest::get(self::wso2_url.'/userinfo?scope=openid', $auth, 1);
        $response = json_decode($request);
        if (!$response) return null;
        if (!isset($response->sub)) return null; // return $response->error_description;

        $email = $response->email_address;

        // Get User
        $token = $this->model_basic->select_where($this->tbl_esams_log_wso2_token, 'email', $email)->row();
        if (!$token) return null;
        $user = $this->model_basic->select_where($this->tbl_user, 'email', $email)->row();
        if (!$user) return null;

        $token->user_id = $user->id;
        $token->access_token = $access_token;

        return $token;
    }

    function do_upload_image($name, $path, $fieldname='userfile')
	{	
		$this->load->library('upload');

		$config = [];
		$config['overwrite'] = TRUE;
		$config['upload_path'] = $path;
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['max_size'] = 100000;
		$config['max_width'] = 7200;
		$config['max_height'] = 3600;
		$config['file_name'] = url_title($name);
		
		$this->upload->initialize($config);
		
		if(!$this->upload->do_upload($fieldname))
			return $this->upload->display_errors();

		return $this->upload->data();
	}

	function populate_post() 
	{
		$post = $this->input->post(NULL, TRUE);
        if ($post===false) {
            $_POST = json_decode(file_get_contents('php://input'), true);
            $post = $_POST;
        }
        return $post;
	}

}
