ALTER TABLE `px_admin` CHANGE `id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `px_admin` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;

--
-- Database: `csc`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_assignment`
--

CREATE TABLE `px_assignment` (
  `id` int(11) NOT NULL,
  `assignment_title` varchar(255) NOT NULL,
  `assignment_type` enum('single','series') NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_assignment_attachment`
--

CREATE TABLE `px_assignment_attachment` (
  `id` int(11) NOT NULL,
  `assignment_detail_id` int(11) NOT NULL,
  `image_name` varchar(500) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_assignment_detail`
--

CREATE TABLE `px_assignment_detail` (
  `id` int(11) NOT NULL,
  `task_title` varchar(255) DEFAULT NULL,
  `assignment_id` int(11) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `description` text NOT NULL,
  `location` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_assignment_people`
--

CREATE TABLE `px_assignment_people` (
  `id` int(11) NOT NULL,
  `assignment_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `group_security_id` int(11) DEFAULT NULL,
  `status` enum('done','active','failed') NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_assignment_people_checkin`
--

CREATE TABLE `px_assignment_people_checkin` (
  `id` int(11) NOT NULL,
  `assignment_detail_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `location` text NOT NULL,
  `image_name` varchar(500) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_assignment_report`
--

CREATE TABLE `px_assignment_report` (
  `id` int(11) NOT NULL,
  `assignment_detail_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `report_text` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_assignment_report_attachment`
--

CREATE TABLE `px_assignment_report_attachment` (
  `id` int(11) NOT NULL,
  `assignment_report_id` int(11) NOT NULL,
  `image_name` varchar(500) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_building`
--

CREATE TABLE `px_building` (
  `id` int(11) NOT NULL,
  `instalasi_id` int(11) NOT NULL,
  `building_name` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_building_detail`
--

CREATE TABLE `px_building_detail` (
  `id` int(11) NOT NULL,
  `building_id` int(11) NOT NULL,
  `floor` varchar(50) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_checklist`
--

CREATE TABLE `px_checklist` (
  `id` int(11) NOT NULL,
  `checklist_name` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_checkpoint`
--

CREATE TABLE `px_checkpoint` (
  `id` int(11) NOT NULL,
  `building_detail_id` int(11) NOT NULL,
  `checkpoint_name` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_group_security`
--

CREATE TABLE `px_group_security` (
  `id` int(11) NOT NULL,
  `groupsecurity_name` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  `modified_date` datetime NOT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_group_security_detail`
--

CREATE TABLE `px_group_security_detail` (
  `id` int(11) NOT NULL,
  `group_security_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_issue`
--

CREATE TABLE `px_issue` (
  `id` int(11) NOT NULL,
  `building_id` int(11) NOT NULL,
  `floor` varchar(255) DEFAULT NULL,
  `area` text,
  `description` text NOT NULL,
  `action` enum('dispatch','escalate') NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_issue_assign`
--

CREATE TABLE `px_issue_assign` (
  `id` int(11) NOT NULL,
  `issue_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_issue_report_attachment`
--

CREATE TABLE `px_issue_report_attachment` (
  `id` int(11) NOT NULL,
  `issue_id` int(11) NOT NULL,
  `image_name` varchar(500) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_patrol`
--

CREATE TABLE `px_patrol` (
  `id` int(11) NOT NULL,
  `building_id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_patrol_checkin`
--

CREATE TABLE `px_patrol_checkin` (
  `id` int(11) NOT NULL,
  `patrol_id` int(11) NOT NULL,
  `image_name` varchar(500) NOT NULL,
  `checkin_type` enum('cctv','manual') NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_patrol_patern`
--

CREATE TABLE `px_patrol_patern` (
  `id` int(11) NOT NULL,
  `patrol_id` int(11) NOT NULL,
  `checkpoint_id` int(11) NOT NULL,
  `sequence` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_patrol_report`
--

CREATE TABLE `px_patrol_report` (
  `id` int(11) NOT NULL,
  `patrol_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `report_text` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_patrol_report_attachment`
--

CREATE TABLE `px_patrol_report_attachment` (
  `id` int(11) NOT NULL,
  `patrol_report_id` int(11) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_patrol_report_checkpoint`
--

CREATE TABLE `px_patrol_report_checkpoint` (
  `id` int(11) NOT NULL,
  `patrol_report_id` int(11) NOT NULL,
  `checkpoint_id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_patrol_user`
--

CREATE TABLE `px_patrol_user` (
  `id` int(11) NOT NULL,
  `patrol_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` enum('done','active','failed') NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_pattern`
--

CREATE TABLE `px_pattern` (
  `id` int(11) NOT NULL,
  `pattern_name` varchar(255) NOT NULL,
  `checkpoint_id` int(11) NOT NULL,
  `sequence` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_shift`
--

CREATE TABLE `px_shift` (
  `id` int(11) NOT NULL,
  `shift_name` varchar(255) NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `px_assignment`
--
ALTER TABLE `px_assignment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `px_assignment_ibfk_1` (`created_by`),
  ADD KEY `px_assignment_ibfk_2` (`modified_by`);

--
-- Indexes for table `px_assignment_attachment`
--
ALTER TABLE `px_assignment_attachment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assignment_detail_id` (`assignment_detail_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_assignment_detail`
--
ALTER TABLE `px_assignment_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assignment_id` (`assignment_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_assignment_people`
--
ALTER TABLE `px_assignment_people`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assignment_id` (`assignment_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `group_security_id` (`group_security_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_assignment_people_checkin`
--
ALTER TABLE `px_assignment_people_checkin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assignment_detail_id` (`assignment_detail_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_assignment_report`
--
ALTER TABLE `px_assignment_report`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assignment_detail_id` (`assignment_detail_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_assignment_report_attachment`
--
ALTER TABLE `px_assignment_report_attachment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assignment_report_id` (`assignment_report_id`);

--
-- Indexes for table `px_building`
--
ALTER TABLE `px_building`
  ADD PRIMARY KEY (`id`),
  ADD KEY `instalasi_id` (`instalasi_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_building_detail`
--
ALTER TABLE `px_building_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `building_id` (`building_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_checklist`
--
ALTER TABLE `px_checklist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_checkpoint`
--
ALTER TABLE `px_checkpoint`
  ADD PRIMARY KEY (`id`),
  ADD KEY `building_detail_id` (`building_detail_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_group_security`
--
ALTER TABLE `px_group_security`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_group_security_detail`
--
ALTER TABLE `px_group_security_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_security_id` (`group_security_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_issue`
--
ALTER TABLE `px_issue`
  ADD PRIMARY KEY (`id`),
  ADD KEY `building_id` (`building_id`);

--
-- Indexes for table `px_issue_assign`
--
ALTER TABLE `px_issue_assign`
  ADD PRIMARY KEY (`id`),
  ADD KEY `issue_id` (`issue_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_issue_report_attachment`
--
ALTER TABLE `px_issue_report_attachment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `issue_id` (`issue_id`);

--
-- Indexes for table `px_patrol`
--
ALTER TABLE `px_patrol`
  ADD PRIMARY KEY (`id`),
  ADD KEY `building_id` (`building_id`),
  ADD KEY `shift_id` (`shift_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_patrol_checkin`
--
ALTER TABLE `px_patrol_checkin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patrol_id` (`patrol_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_patrol_patern`
--
ALTER TABLE `px_patrol_patern`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patrol_id` (`patrol_id`),
  ADD KEY `location_id` (`checkpoint_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_patrol_report`
--
ALTER TABLE `px_patrol_report`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patrol_id` (`patrol_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `px_patrol_report_attachment`
--
ALTER TABLE `px_patrol_report_attachment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patrol_report_id` (`patrol_report_id`);

--
-- Indexes for table `px_patrol_report_checkpoint`
--
ALTER TABLE `px_patrol_report_checkpoint`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patrol_report_id` (`patrol_report_id`),
  ADD KEY `checkpoint_id` (`checkpoint_id`);

--
-- Indexes for table `px_patrol_user`
--
ALTER TABLE `px_patrol_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `px_pattern`
--
ALTER TABLE `px_pattern`
  ADD PRIMARY KEY (`id`),
  ADD KEY `checkpoint_id` (`checkpoint_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_shift`
--
ALTER TABLE `px_shift`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `px_assignment`
--
ALTER TABLE `px_assignment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_assignment_attachment`
--
ALTER TABLE `px_assignment_attachment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_assignment_detail`
--
ALTER TABLE `px_assignment_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_assignment_people`
--
ALTER TABLE `px_assignment_people`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_assignment_people_checkin`
--
ALTER TABLE `px_assignment_people_checkin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_assignment_report`
--
ALTER TABLE `px_assignment_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_assignment_report_attachment`
--
ALTER TABLE `px_assignment_report_attachment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_building`
--
ALTER TABLE `px_building`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_building_detail`
--
ALTER TABLE `px_building_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_checklist`
--
ALTER TABLE `px_checklist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_checkpoint`
--
ALTER TABLE `px_checkpoint`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_group_security`
--
ALTER TABLE `px_group_security`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_group_security_detail`
--
ALTER TABLE `px_group_security_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_issue`
--
ALTER TABLE `px_issue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_issue_assign`
--
ALTER TABLE `px_issue_assign`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_issue_report_attachment`
--
ALTER TABLE `px_issue_report_attachment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_patrol`
--
ALTER TABLE `px_patrol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_patrol_checkin`
--
ALTER TABLE `px_patrol_checkin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_patrol_patern`
--
ALTER TABLE `px_patrol_patern`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_patrol_report`
--
ALTER TABLE `px_patrol_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_patrol_report_attachment`
--
ALTER TABLE `px_patrol_report_attachment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_patrol_report_checkpoint`
--
ALTER TABLE `px_patrol_report_checkpoint`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_patrol_user`
--
ALTER TABLE `px_patrol_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_pattern`
--
ALTER TABLE `px_pattern`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_shift`
--
ALTER TABLE `px_shift`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `px_assignment`
--

ALTER TABLE `px_assignment` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_assignment_attachment` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_assignment_detail` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_assignment_people` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_assignment_people_checkin` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_assignment_report` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_assignment_report_attachment` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_checklist` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_checkpoint` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_group_security` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_group_security_detail` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_patrol` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_patrol_checkin` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_patrol_patern` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_patrol_report` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_patrol_report_attachment` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_patrol_report_checkpoint` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_patrol_user` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_pattern` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_shift` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;

RENAME TABLE `px_patrol_patern` TO `px_patrol_pattern`;

/*baru*/
ALTER TABLE `px_building` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_building_detail` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;

ALTER TABLE `px_shift` CHANGE `start_time` `start_time` TIME NULL DEFAULT NULL, CHANGE `end_time` `end_time` TIME NULL DEFAULT NULL;

ALTER TABLE `px_group_security` CHANGE `modified_by` `modified_by` INT(11) NULL, CHANGE `modified_date` `modified_date` DATETIME NULL;

CREATE TABLE `px_esams_log_wso2_token` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`user_id` Int( 11 ) NOT NULL,
	`access_token` Text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
	`delete_flag` Int( 1 ) NOT NULL DEFAULT 0,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;

CREATE INDEX `index_user_id` USING BTREE ON `px_esams_log_wso2_token`( `user_id` );

ALTER TABLE `px_assignment_detail` ADD `radius` INT NOT NULL AFTER `location`;

ALTER TABLE `px_esams_log_wso2_token` ADD `expires_in` INT NOT NULL AFTER `access_token`;

RENAME TABLE px_pattern TO px_pattern_detail;
ALTER TABLE `px_pattern_detail` CHANGE `pattern_name` `pattern_id` INT(11) NOT NULL;
ALTER TABLE `px_pattern_detail` ADD INDEX `pattern_id` (`pattern_id`);

CREATE TABLE `px_pattern` (
  `id` int(11) NOT NULL,
  `pattern_name` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `delete_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `px_pattern` ADD PRIMARY KEY (`id`);
ALTER TABLE `px_pattern` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

CREATE TABLE `px_cctv_csc` (
  `id` int(11) NOT NULL,
  `checkpoint_id` int(11) NOT NULL,
  `channel` varchar(255) NOT NULL,
  `location` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `delete_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `px_cctv_csc` ADD PRIMARY KEY (`id`);
ALTER TABLE `px_cctv_csc` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `px_cctv_csc` ADD INDEX `checkpoint_id` (`checkpoint_id`);

ALTER TABLE `px_esams_log_wso2_token` ADD `response_wso2_request` TEXT NULL AFTER `expires_in`;
ALTER TABLE `px_esams_log_wso2_token` ADD `response_wso2_introspect` TEXT NULL AFTER `expires_in`;
ALTER TABLE `px_esams_log_wso2_token` ADD `email` varchar(255) NULL AFTER `user_id`;
ALTER TABLE `px_esams_log_wso2_token` MODIFY `user_id` int(11) NULL;

ALTER TABLE `px_patrol_report` CHANGE `patrol_id` `patrol_pattern_id` INT(11) NOT NULL;
ALTER TABLE `px_patrol_checkin` CHANGE `patrol_id` `patrol_pattern_id` INT(11) NOT NULL;
ALTER TABLE `px_patrol_checkin` ADD `user_id` INT NOT NULL AFTER `id`;
ALTER TABLE `px_patrol_checkin` ADD INDEX `user_id` (`user_id`);
ALTER TABLE `px_patrol_report_checkpoint` ADD `checklist_id` INT NOT NULL AFTER `checkpoint_id`;
ALTER TABLE `px_patrol_report_checkpoint` ADD INDEX `checklist_id` (`checklist_id`);

ALTER TABLE `px_patrol_pattern` ADD `reason_manual_checkin` TEXT NULL AFTER `sequence`;

ALTER TABLE `px_issue` MODIFY COLUMN `action` enum('dispatch','escalate') NULL DEFAULT NULL;
ALTER TABLE `px_issue` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_issue_assign` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_issue_report_attachment` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;

ALTER TABLE `px_issue_report_attachment` CHANGE `issue_id` `issue_report_id` INT(11) NOT NULL;
ALTER TABLE `px_issue_report_attachment` DROP INDEX `issue_id`;
ALTER TABLE `px_issue_report_attachment` ADD INDEX `issue_report_id` (`issue_report_id`);
ALTER TABLE `px_issue_assign` ADD `status` enum('done','active','failed') NOT NULL DEFAULT 'active' AFTER `user_id`;

CREATE TABLE `px_issue_attachment` (
  `id` int(11) NOT NULL,
  `issue_id` int(11) NOT NULL,
  `image_name` varchar(500) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `delete_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `px_issue_attachment` ADD PRIMARY KEY (`id`);
ALTER TABLE `px_issue_attachment` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `px_issue_attachment` ADD INDEX `issue_id` (`issue_id`);

CREATE TABLE `px_issue_report` (
  `id` int(11) NOT NULL,
  `issue_assign_id` int(11) NOT NULL,
  `report_text` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `delete_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `px_issue_report` ADD PRIMARY KEY (`id`);
ALTER TABLE `px_issue_report` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `px_issue_report` ADD INDEX `issue_assign_id` (`issue_assign_id`);

-- ALTER TABLE `px_issue` ADD `ticket_number` VARCHAR(15) NOT NULL AFTER `id`;

ALTER TABLE `px_issue` ADD `ticket_number` varchar(255) NULL DEFAULT NULL AFTER `id`;

ALTER TABLE `px_issue` ADD `status` ENUM('active','done','failed') NOT NULL DEFAULT 'active' AFTER `action`;

ALTER TABLE `px_issue` ADD `note_admin` TEXT NOT NULL AFTER `status`;

TRUNCATE TABLE `px_building`;
INSERT INTO `px_building` 
	(`id`, `instalasi_id`, `building_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	(NULL, '754', 'Amdi A', '18', '2019-10-25 10:00:00', NULL, NULL, '0'), 
	(NULL, '754', 'Amdi B', '18', '2019-10-25 10:00:00', NULL, NULL, '0'), 
	(NULL, '754', 'Amdi C', '18', '2019-10-25 10:00:00', NULL, NULL, '0'), 
	(NULL, '754', 'Amdi D', '18', '2019-10-25 10:00:00', NULL, NULL, '0');

SET FOREIGN_KEY_CHECKS=0;
TRUNCATE TABLE `px_building_detail`;
SET FOREIGN_KEY_CHECKS=1;
INSERT INTO `px_building_detail` 
	(`id`, `building_id`, `floor`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	(NULL, '1', '1', '18', '2019-10-25 10:00:00', NULL, NULL, '0'), 
	(NULL, '1', '2', '18', '2019-10-25 10:00:00', NULL, NULL, '0'), 
	(NULL, '1', '3', '18', '2019-10-25 10:00:00', NULL, NULL, '0'), 
	(NULL, '1', '4', '18', '2019-10-25 10:00:00', NULL, NULL, '0'),
	(NULL, '1', '5', '18', '2019-10-25 10:00:00', NULL, NULL, '0'), 
	(NULL, '1', '6', '18', '2019-10-25 10:00:00', NULL, NULL, '0'), 
	(NULL, '1', '7', '18', '2019-10-25 10:00:00', NULL, NULL, '0'), 
	(NULL, '1', '8', '18', '2019-10-25 10:00:00', NULL, NULL, '0'),
	(NULL, '2', '1', '18', '2019-10-25 10:00:00', NULL, NULL, '0'), 
	(NULL, '2', '2', '18', '2019-10-25 10:00:00', NULL, NULL, '0'), 
	(NULL, '2', '3', '18', '2019-10-25 10:00:00', NULL, NULL, '0'), 
	(NULL, '2', '4', '18', '2019-10-25 10:00:00', NULL, NULL, '0'),
	(NULL, '2', '5', '18', '2019-10-25 10:00:00', NULL, NULL, '0'), 
	(NULL, '2', '6', '18', '2019-10-25 10:00:00', NULL, NULL, '0'), 
	(NULL, '2', '7', '18', '2019-10-25 10:00:00', NULL, NULL, '0'), 
	(NULL, '2', '8', '18', '2019-10-25 10:00:00', NULL, NULL, '0'),
	(NULL, '3', '1', '18', '2019-10-25 10:00:00', NULL, NULL, '0'), 
	(NULL, '3', '2', '18', '2019-10-25 10:00:00', NULL, NULL, '0'), 
	(NULL, '3', '3', '18', '2019-10-25 10:00:00', NULL, NULL, '0'), 
	(NULL, '3', '4', '18', '2019-10-25 10:00:00', NULL, NULL, '0'),
	(NULL, '3', '5', '18', '2019-10-25 10:00:00', NULL, NULL, '0'), 
	(NULL, '4', '1', '18', '2019-10-25 10:00:00', NULL, NULL, '0'), 
	(NULL, '4', '2', '18', '2019-10-25 10:00:00', NULL, NULL, '0'), 
	(NULL, '4', '3', '18', '2019-10-25 10:00:00', NULL, NULL, '0'),
	(NULL, '4', '4', '18', '2019-10-25 10:00:00', NULL, NULL, '0'), 
	(NULL, '4', '5', '18', '2019-10-25 10:00:00', NULL, NULL, '0');

CREATE TABLE `px_issue_report_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `delete_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `px_issue_report_type` ADD PRIMARY KEY (`id`);
ALTER TABLE `px_issue_report_type` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

INSERT INTO `px_issue_report_type` 
	(`id`, `name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	(NULL, 'Patroli', '18', '2019-10-25 10:00:00', NULL, NULL, '0');

ALTER TABLE `px_issue` ADD `issue_report_type_id` int(11) NULL DEFAULT NULL AFTER `id`;
ALTER TABLE `px_issue` ADD INDEX `issue_report_type_id` (`issue_report_type_id`);

ALTER TABLE `px_issue` ADD `fact` TEXT NULL DEFAULT NULL AFTER `description`;
ALTER TABLE `px_issue` ADD `analysis` TEXT NULL DEFAULT NULL AFTER `description`;
ALTER TABLE `px_issue` ADD `summary` TEXT NULL DEFAULT NULL AFTER `description`;

ALTER TABLE `px_esams_log_wso2_token` CHANGE `expires_in` `expires_in` INT(11) NULL;
ALTER TABLE `px_esams_log_wso2_token` ADD `imei` VARCHAR(255) NULL DEFAULT NULL AFTER `access_token`;

ALTER TABLE `px_cctv_csc` CHANGE `checkpoint_id` `checkpoint_id` INT(11) NULL;
ALTER TABLE `px_cctv_csc` ADD `ip4_address` VARCHAR(255) NULL DEFAULT NULL AFTER `checkpoint_id`;
ALTER TABLE `px_cctv_csc` ADD `port` VARCHAR(255) NULL DEFAULT NULL AFTER `checkpoint_id`;
ALTER TABLE `px_cctv_csc` ADD `status` VARCHAR(255) NULL DEFAULT NULL AFTER `checkpoint_id`;
ALTER TABLE `px_cctv_csc` ADD `notes` TEXT NULL DEFAULT NULL AFTER `location`;
TRUNCATE TABLE `px_cctv_csc`;
INSERT INTO `px_cctv_csc` 
	(`channel`, `status`, `ip4_address`, `port`, `location`, `created_by`, `created_date`, `delete_flag`) 
VALUES 
	('1', 'Active', '192.168.2.176', '8000', 'JEMBATAN GD. B DAN C', '18', '2019-10-25 10:00:00', '0'), 
	('2', 'Active', '192.168.2.213', '8000', 'DEPOT', '18', '2019-10-25 10:00:00', '0'), 
	('3', 'Active', '192.168.2.214', '8000', 'LOBBY LIFT LT. 1A', '18', '2019-10-25 10:00:00', '0'), 
	('4', 'Active', '192.168.2.215', '8000', 'PINTU SELATAN GD A', '18', '2019-10-25 10:00:00', '0'),
	('5', 'Active', '192.168.2.216', '8000', 'PINTU TEMBUS LT. 5A', '18', '2019-10-25 10:00:00', '0'),
	('6', 'Active', '192.168.2.217', '8000', 'DEPAN LIFT LT 7A', '18', '2019-10-25 10:00:00', '0'),
	('7', 'Active', '192.168.2.218', '8000', 'STRONG ROOM LT 1 A', '18', '2019-10-25 10:00:00', '0'),
	('8', 'Active', '192.168.2.219', '8000', 'UPS 1', '18', '2019-10-25 10:00:00', '0'),
	('9', 'Active', '192.168.2.220', '8000', 'ENGINERING', '18', '2019-10-25 10:00:00', '0'),
	('10', 'Active', '192.168.2.221', '8000', 'RECEPTIONIST LOBBY A', '18', '2019-10-25 10:00:00', '0'),
	('11', 'Active', '192.168.2.222', '8000', 'LOBBY LIFT LT.8 A', '18', '2019-10-25 10:00:00', '0'),
	('12', 'Active', '192.168.2.223', '8000', 'PINTU BARAT GD A', '18', '2019-10-25 10:00:00', '0'),
	('13', 'Active', '192.168.2.224', '8000', 'LOBBY LIFT LT 1 D', '18', '2019-10-25 10:00:00', '0'),
	('14', 'Active', '192.168.2.225', '8000', 'LOBBY LIFT LT. 6A', '18', '2019-10-25 10:00:00', '0'),
	('15', 'Active', '192.168.2.226', '8000', 'RECEPTIONIST LOBBY B', '18', '2019-10-25 10:00:00', '0'),
	('16', 'Active', '192.168.2.227', '8000', 'PINTU TIMUR LT. 1C', '18', '2019-10-25 10:00:00', '0');

ALTER TABLE `px_cctv_csc` DROP `checkpoint_id`;

ALTER TABLE `px_checkpoint` ADD `cctv_csc_id` INT NOT NULL AFTER `building_detail_id`;

ALTER TABLE `px_checklist` ADD `checkpoint_id` INT NOT NULL AFTER `id`;

TRUNCATE TABLE `px_checkpoint`;

INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('1', '8', '11', 'LOBBY LIFT', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('2', '8', '11', 'R.AHU ZONE A', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('3', '8', '11', 'TANGGA DARURAT LT.9 ZONE A', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('4', '8', '11', 'FITNESS', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('5', '8', '11', 'WILLIAM SURJAYA', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('6', '8', '11', 'EKSEKUTIF LOUNGE', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('7', '8', '11', 'R. AHU ZONE B', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('8', '8', '11', 'TOILET', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('9', '8', '11', 'TANGGA DARURAT LT.9 ZONE B', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('10', '7', '6', 'LOBBY LIFT', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('11', '7', '6', 'GUDANG BP VAM', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('12', '7', '6', 'IT UTAMA', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('13', '7', '6', 'RUNGGU SERVER', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('14', '7', '6', 'SERVER', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('15', '7', '6', 'GUDANG AGOS', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('16', '7', '6', 'R. AHU ZONE A', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('17', '7', '6', 'R. AHU ZONE B', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('18', '7', '6', 'TOILET', '18', '2019-11-19 13:46:52', NULL, NULL, '0');

CREATE TABLE `px_checklist_detail` (
  `id` int(11) NOT NULL,
  `checklist_id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `delete_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `px_checklist_detail` ADD PRIMARY KEY (`id`);
ALTER TABLE `px_checklist_detail` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

UPDATE `px_checkpoint` SET `building_detail_id` = '7' WHERE `px_checkpoint`.`id` = 10; 
UPDATE `px_checkpoint` SET `building_detail_id` = '7' WHERE `px_checkpoint`.`id` = 11; 
UPDATE `px_checkpoint` SET `building_detail_id` = '7' WHERE `px_checkpoint`.`id` = 12; 
UPDATE `px_checkpoint` SET `building_detail_id` = '7' WHERE `px_checkpoint`.`id` = 13; 
UPDATE `px_checkpoint` SET `building_detail_id` = '7' WHERE `px_checkpoint`.`id` = 14; 
UPDATE `px_checkpoint` SET `building_detail_id` = '7' WHERE `px_checkpoint`.`id` = 15; 
UPDATE `px_checkpoint` SET `building_detail_id` = '7' WHERE `px_checkpoint`.`id` = 16; 
UPDATE `px_checkpoint` SET `building_detail_id` = '7' WHERE `px_checkpoint`.`id` = 17; 
UPDATE `px_checkpoint` SET `building_detail_id` = '7' WHERE `px_checkpoint`.`id` = 18;

INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('19', '5', '5', 'RUANG ESR', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('20', '5', '5', 'RUANG MEETING', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('21', '5', '5', 'RUANG FOTOCOPY', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('22', '5', '5', 'RUANG MAKAN', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('23', '5', '5', 'RUANG SECD', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('24', '5', '5', 'GUDANG GA', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('25', '5', '5', 'GUDANG SECD', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('26', '5', '5', 'GUDANG AIR', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('27', '5', '5', 'RUANG NURSERY', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('28', '5', '5', 'R. AHU ZONE A', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('29', '5', '5', 'R. AHU ZONE B', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('30', '5', '5', 'TOILET', '18', '2019-11-19 13:46:52', NULL, NULL, '0');

INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('31', '6', '14', 'Ruang Utama BOD/BOC', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('32', '6', '14', 'Ruang Sinergy', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('33', '6', '14', 'Ruang Makan', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('34', '6', '14', 'Ruang Dapur', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('35', '6', '14', 'LOBBY LIFT', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('36', '6', '14', 'R. AHU ZONE A', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('37', '6', '14', 'R. AHU ZONE B', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('38', '6', '14', 'TOILET', '18', '2019-11-19 13:46:52', NULL, NULL, '0');

INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('39', '1', '10', 'LOBBY A', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('40', '1', '10', 'TOILET', '18', '2019-11-19 13:46:52', NULL, NULL, '0');

/*20 november*/
INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('41', '4', '5', 'R.401', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('42', '4', '5', 'R.402', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('43', '4', '5', 'R.403', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('44', '4', '5', 'R.404', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('45', '4', '5', 'R.405', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('46', '4', '5', 'R.406', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('47', '4', '5', 'R.407', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('48', '4', '5', 'R.408', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('49', '4', '5', 'R.409', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('50', '4', '5', 'R.410', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('51', '4', '5', 'R.411', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('52', '4', '5', 'R.412', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('53', '4', '5', 'R.413', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('54', '4', '5', 'R.414', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('55', '4', '5', 'R.415', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('56', '4', '5', 'R.416', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('57', '4', '5', 'R.AHU ZONE A', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('58', '4', '5', 'R.AHU ZONE B', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('59', '4', '5', 'TOILET WANITA', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('60', '4', '5', 'TOILET PRIA', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('62', '3', '5', 'AUDITORIUM', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('63', '3', '5', 'R.301', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('64', '3', '5', 'R.302', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('65', '3', '5', 'R.303', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('66', '3', '5', 'R.304', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('67', '3', '5', 'R.305', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('68', '3', '5', 'R.306', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('69', '3', '5', 'R.307', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('70', '3', '5', 'R.308', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('71', '3', '5', 'R.309', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('72', '3', '5', 'R.310', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('73', '3', '5', 'R.311', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('74', '3', '5', 'R.312', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('75', '3', '5', 'R.313', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('76', '3', '5', 'R.314', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('77', '3', '5', 'R.315', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('78', '3', '5', 'R.316', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('79', '3', '5', 'R.AHU ZONE A', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('80', '3', '5', 'R.AHU ZONE B', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('81', '3', '5', 'TOILET WANITA', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('82', '3', '5', 'TOILET PRIA', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('83', '3', '5', 'HRD', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('84', '3', '5', 'GAD', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('85', '3', '5', 'AMDI', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('86', '3', '5', 'R.204', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('87', '3', '5', 'R.206', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('88', '3', '5', 'R.207', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('89', '3', '5', 'R.AHU ZONE A', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('90', '3', '5', 'R.AHU ZONE B', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('91', '3', '5', 'TOILET', '18', '2019-11-20 11:46:52', NULL, NULL, '0');


INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('92', '1', '3', 'R.AHU ZONE A', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('93', '1', '3', 'R.AHU ZONE B', '18', '2019-11-20 13:46:52', NULL, NULL, '0');

INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('94', '1', '7', 'STROONG ROOM', '18', '2019-11-20 13:46:52', NULL, NULL, '0');

INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('95', '1', '4', 'ARC', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('96', '1', '4', 'PUREL', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('97', '1', '4', 'STROONG ROOM', '18', '2019-11-20 13:46:52', NULL, NULL, '0');

INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('98', '1', '12', 'KLINIK', '18', '2019-11-20 13:46:52', NULL, NULL, '0');


/*Gedung B*/

INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('99', '9', '15', 'LOBBY', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('100', '9', '15', 'HSO', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('101', '9', '15', 'BANK PERMATA', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('102', '9', '15', 'PINTU DARURAT', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('103', '9', '15', 'TOILET', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('104', '10', '15', 'HSO', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('105', '11', '15', 'R.301', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('106', '11', '15', 'R.302', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('107', '11', '15', 'R.303', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('108', '11', '15', 'R.304', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('109', '11', '15', 'R.305', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('110', '11', '15', 'R.306', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('111', '11', '15', 'R.307', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('112', '11', '15', 'R.308', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('113', '11', '15', 'R.309', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('114', '11', '15', 'R.310', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('115', '11', '15', 'R.311', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('116', '11', '15', 'R.312', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('117', '11', '15', 'R.313', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('118', '11', '15', 'RUANG FOTO COPY', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('119', '11', '15', 'TOILET', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('120', '12', '15', 'R.401', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('121', '12', '15', 'R.402', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('122', '12', '15', 'R.403', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('123', '12', '15', 'R.404', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('124', '12', '15', 'R.405', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('125', '12', '15', 'R.406', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('126', '12', '15', 'R.407', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('127', '12', '15', 'TOILET', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('128', '13', '15', 'SEKRETARIAT POLMAN', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('129', '13', '15', 'TOILET', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('130', '13', '15', 'R.KARYAWAN', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('131', '14', '15', '601 Ruang Meeting 1', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('132', '14', '15', '602 Ruang Meeting 2', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('133', '14', '15', '604 Ruang File', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('134', '14', '15', 'R IT Suport', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('135', '14', '15', 'R.Bussines', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('136', '14', '15', 'R.Photo Copy', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('137', '14', '15', 'TOILET', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('138', '15', '15', 'R.KARYAWAN', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('139', '15', '15', '701 RUANG MEETING 1', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('140', '15', '15', '703 RUANG MEETING 2', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('141', '15', '15', 'NURSERY ROOM', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('142', '15', '15', 'TOILET', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('143', '16', '15', 'R. Karyawan', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('144', '16', '15', '801 Ruang Meeting 1', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('145', '16', '15', '802 Ruang Meeting 2', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('146', '16', '15', '803 Ruang File', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('147', '16', '15', '806 Ruang File', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('148', '16', '15', 'NURSERY ROOM', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('149', '16', '15', 'TOILET', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('150', '16', '15', 'TANGGA DARURAT', '18', '2019-11-20 13:46:52', NULL, NULL, '0');

INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('151', '17', '16', '101 Ruang Instruktur TPM', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('152', '17', '16', '103 Ruang Bengkel', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('153', '17', '16', 'R 104', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('154', '17', '16', '109 Kamar Alat', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('155', '17', '16', 'Ruang Sa Karu', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('156', '17', '16', 'Ruang Instruktur TO', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('157', '17', '16', 'Lab Pengecatan', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('158', '17', '16', 'Kamar Alat', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('159', '17', '16', 'Pintu Kaca GD C', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('160', '17', '16', 'Rolling Door I', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('161', '17', '16', 'Roling Door II', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('162', '17', '16', 'Tangga Darurat ZONE A', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('163', '17', '16', 'Tangga Darurat ZONE B', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('164', '17', '16', 'TOILET PRIA/WANITA', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('165', '18', '16', '201 Ruang Kelas ', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('166', '18', '16', '202 Lab UPT', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('167', '18', '16', '203 Ruang Kelas', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('168', '18', '16', '204 Ruang Kelas', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('169', '18', '16', '207 Ruang Teori Praktek TO', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('170', '18', '16', '208 Lab TMS ( Time Motion Study )', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('171', '18', '16', '209 Ruang Teori Praktek TO', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('172', '18', '16', '210 Ruang Training', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('173', '18', '16', '211 Ruang Kelas', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('174', '18', '16', '212 Ruang Kelas ', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('175', '18', '16', '213 Ruang Kelas ', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('176', '18', '16', 'MUSHOLLA', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('178', '18', '16', 'TOILET PRIA/WANITA', '18', '2019-11-20 13:46:52', NULL, NULL, '0');

INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('179', '19', '1', '301 Ruang Assesment CBT/CBA', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('180', '19', '1', '302 Ruang Instruktur P4 TK1', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('181', '19', '1', '303 Ruang Instruktur P4 TK2', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('182', '19', '1', '304 Ruang Instruktur P4 TK3', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('183', '19', '1', '305 Ruang Mesin', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('184', '19', '1', '306 Ruang Instruktur Produksi', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('185', '19', '1', '307  Ruang Maintenance, UPT B', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('186', '19', '1', '308 Locker Karyawan / Instruktur', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('187', '19', '1', '309 Gudang Training & Marketing', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('188', '19', '1', 'R 313', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('189', '19', '1', 'R 314', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('190', '19', '1', '315-316 Lab Pengukuran', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('191', '19', '1', '317 Lab Perlakuan Panas', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('192', '19', '1', '318 Ruang Tools', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('193', '19', '1', '319 Ruang Product', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('194', '19', '1', '320 Ruang Alat-alat', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('195', '19', '1', '321 Gudang Material', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('196', '19', '1', '322 Gudang Material', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('197', '19', '1', 'Pintu Tembus Parkir Lt IV', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('198', '20', '1', 'SPORT HALL', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('199', '20', '1', 'Ruang Musik Polman', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('200', '20', '1', 'Ruang Musik AI-HO', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('201', '20', '1', 'Toilet Pria / Wanita', '18', '2019-11-20 13:46:52', NULL, NULL, '0');

/*gedung D*/
INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('202', '22', '9', 'R. GENSET', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('203', '22', '9', 'Musholla', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('204', '22', '9', 'Lab ESR', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('205', '22', '9', 'Parkir Motor', '18', '2019-11-20 13:46:52', NULL, NULL, '0');

INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('206', '22', '13', 'Ruang Locker', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('207', '22', '13', 'TOILET', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('208', '23', '13', 'Parkir Mobil VIP', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('209', '23', '13', 'Ruang Pool', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('210', '23', '13', 'Ruang Locker', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('211', '23', '13', 'TOILET', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('212', '24', '13', 'Parkir Mobil', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('213', '24', '13', 'Ruang Locker', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('214', '24', '13', 'TOILET', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('215', '25', '13', 'Parkir Mobil', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('216', '25', '13', 'Mobil Titipan', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('217', '25', '13', 'Ruang Locker', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('218', '25', '13', 'TOILET', '18', '2019-11-20 13:46:52', NULL, NULL, '0');

INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('219', '26', '8', 'Parkir Mobil', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('220', '26', '8', 'Mobil Titipan', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('221', '26', '8', 'Ruang Locker', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('222', '26', '8', 'UPS', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('223', '26', '8', 'TOILET', '18', '2019-11-20 13:46:52', NULL, NULL, '0');

/*INSERT CHECKPOINT*/
INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('224', '27', '8', 'Parkir Mobil POOL', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('225', '27', '8', 'Parkir Mobil', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('226', '27', '8', 'Ruang Locker', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('227', '27', '8', 'TOILET ', '18', '2019-11-20 13:46:52', NULL, NULL, '0');

/*insert checkpoint*/
INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('228', '28', '2', 'OUTDOR', '18', '2019-11-20 13:46:52', NULL, NULL, '0');

/*INSERT BUILDING DETAIL*/
INSERT INTO `px_building_detail` 
	(`id`, `building_id`, `floor`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('27', '4', '6', '18', '2019-11-20 00:00:00', NULL, NULL, '0');

/*insert building*/
INSERT INTO `px_building` 
	(`id`, `instalasi_id`, `building_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('5', '754', 'OUTDOOR', '18', '2019-11-20 00:00:00', NULL, NULL, '0');

/*insert building detail*/
INSERT INTO `px_building_detail` 
	(`id`, `building_id`, `floor`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('28', '5', '1', '18', '2019-11-20 00:00:00', NULL, NULL, '0');

ALTER TABLE `px_esams_log_wso2_token` CHANGE `access_token` `access_token` TEXT NULL;

ALTER TABLE `px_issue` CHANGE `description` `description` TEXT NULL;
ALTER TABLE `px_issue` CHANGE `note_admin` `note_admin` TEXT NULL;

INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('229', '2', '5', 'HRD', '18', '2019-11-26 08:00:52', NULL, NULL, '0'),
	('230', '2', '5', 'GAD', '18', '2019-11-26 08:00:52', NULL, NULL, '0'),
	('231', '2', '5', 'AMDI', '18', '2019-11-26 08:00:52', NULL, NULL, '0'),
	('232', '2', '5', 'R.204', '18', '2019-11-26 08:00:52', NULL, NULL, '0'),
	('233', '2', '5', 'R.206', '18', '2019-11-26 08:00:52', NULL, NULL, '0'),
	('234', '2', '5', 'R.207', '18', '2019-11-26 08:00:52', NULL, NULL, '0'),
	('235', '2', '5', 'R. AHU ZONE A', '18', '2019-11-26 08:00:52', NULL, NULL, '0'),
	('236', '2', '5', 'R. AHU ZONE B', '18', '2019-11-26 08:00:52', NULL, NULL, '0'),
	('237', '2', '5', 'TOILET', '18', '2019-11-26 08:00:52', NULL, NULL, '0');

CREATE TABLE `px_esams_log_api` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`method` Varchar( 225 ) NULL DEFAULT NULL,
	`uri` Varchar( 225 ) NULL DEFAULT NULL,
	`token_key` Varchar( 225 ) NULL DEFAULT NULL,
	`http_code` Varchar( 225 ) NULL DEFAULT NULL,
	`request_post` TEXT NULL DEFAULT NULL,
	`request_get` TEXT NULL DEFAULT NULL,
	`message` TEXT NULL DEFAULT NULL,
	`response` TEXT NULL DEFAULT NULL,
	`date_created` DateTime NOT NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 0;