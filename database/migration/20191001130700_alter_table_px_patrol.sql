ALTER TABLE `px_patrol_report` CHANGE `patrol_id` `patrol_pattern_id` INT(11) NOT NULL;
ALTER TABLE `px_patrol_checkin` CHANGE `patrol_id` `patrol_pattern_id` INT(11) NOT NULL;
ALTER TABLE `px_patrol_checkin` ADD `user_id` INT NOT NULL AFTER `id`;
ALTER TABLE `px_patrol_checkin` ADD INDEX `user_id` (`user_id`);
ALTER TABLE `px_patrol_report_checkpoint` ADD `checklist_id` INT NOT NULL AFTER `checkpoint_id`;
ALTER TABLE `px_patrol_report_checkpoint` ADD INDEX `checklist_id` (`checklist_id`);