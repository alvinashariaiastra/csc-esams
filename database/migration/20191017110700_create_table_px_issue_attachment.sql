CREATE TABLE `px_issue_attachment` (
  `id` int(11) NOT NULL,
  `issue_id` int(11) NOT NULL,
  `image_name` varchar(500) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `delete_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `px_issue_attachment` ADD PRIMARY KEY (`id`);
ALTER TABLE `px_issue_attachment` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `px_issue_attachment` ADD INDEX `issue_id` (`issue_id`);