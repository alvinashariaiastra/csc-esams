ALTER TABLE `px_issue_report_attachment` CHANGE `issue_id` `issue_report_id` INT(11) NOT NULL;
ALTER TABLE `px_issue_report_attachment` DROP INDEX `issue_id`;
ALTER TABLE `px_issue_report_attachment` ADD INDEX `issue_report_id` (`issue_report_id`);
ALTER TABLE `px_issue_assign` ADD `status` enum('done','active','failed') NOT NULL DEFAULT 'active' AFTER `user_id`;