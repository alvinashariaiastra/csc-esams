ALTER TABLE px_assignment DROP FOREIGN KEY px_assignment_ibfk_1;
ALTER TABLE px_assignment DROP FOREIGN KEY px_assignment_ibfk_2;

ALTER TABLE px_assignment_attachment DROP FOREIGN KEY px_assignment_attachment_ibfk_1;
ALTER TABLE px_assignment_attachment DROP FOREIGN KEY px_assignment_attachment_ibfk_2;
ALTER TABLE px_assignment_attachment DROP FOREIGN KEY px_assignment_attachment_ibfk_3;

ALTER TABLE px_assignment_detail DROP FOREIGN KEY px_assignment_detail_ibfk_1;
ALTER TABLE px_assignment_detail DROP FOREIGN KEY px_assignment_detail_ibfk_2;
ALTER TABLE px_assignment_detail DROP FOREIGN KEY px_assignment_detail_ibfk_3;

ALTER TABLE px_assignment_people DROP FOREIGN KEY px_assignment_people_ibfk_1;
ALTER TABLE px_assignment_people DROP FOREIGN KEY px_assignment_people_ibfk_2;
ALTER TABLE px_assignment_people DROP FOREIGN KEY px_assignment_people_ibfk_3;
ALTER TABLE px_assignment_people DROP FOREIGN KEY px_assignment_people_ibfk_4;
ALTER TABLE px_assignment_people DROP FOREIGN KEY px_assignment_people_ibfk_5;

ALTER TABLE px_assignment_people_checkin DROP FOREIGN KEY px_assignment_people_checkin_ibfk_1;
ALTER TABLE px_assignment_people_checkin DROP FOREIGN KEY px_assignment_people_checkin_ibfk_2;
ALTER TABLE px_assignment_people_checkin DROP FOREIGN KEY px_assignment_people_checkin_ibfk_3;
ALTER TABLE px_assignment_people_checkin DROP FOREIGN KEY px_assignment_people_checkin_ibfk_4;

ALTER TABLE px_assignment_report DROP FOREIGN KEY px_assignment_report_ibfk_1;
ALTER TABLE px_assignment_report DROP FOREIGN KEY px_assignment_report_ibfk_2;
ALTER TABLE px_assignment_report DROP FOREIGN KEY px_assignment_report_ibfk_3;
ALTER TABLE px_assignment_report DROP FOREIGN KEY px_assignment_report_ibfk_4;

ALTER TABLE px_assignment_report_attachment DROP FOREIGN KEY px_assignment_report_attachment_ibfk_1;

ALTER TABLE px_building DROP FOREIGN KEY px_building_ibfk_1;
ALTER TABLE px_building DROP FOREIGN KEY px_building_ibfk_2;
ALTER TABLE px_building DROP FOREIGN KEY px_building_ibfk_3;

ALTER TABLE px_building_detail DROP FOREIGN KEY px_building_detail_ibfk_1;
ALTER TABLE px_building_detail DROP FOREIGN KEY px_building_detail_ibfk_2;
ALTER TABLE px_building_detail DROP FOREIGN KEY px_building_detail_ibfk_3;

/*ALTER TABLE px_cctv DROP FOREIGN KEY px_cctv_ibfk_1;
ALTER TABLE px_cctv DROP FOREIGN KEY px_cctv_ibfk_2;
ALTER TABLE px_cctv DROP FOREIGN KEY px_cctv_ibfk_3;*/

ALTER TABLE px_checklist DROP FOREIGN KEY px_checklist_ibfk_1;
ALTER TABLE px_checklist DROP FOREIGN KEY px_checklist_ibfk_2;

ALTER TABLE px_checkpoint DROP FOREIGN KEY px_checkpoint_ibfk_1;
ALTER TABLE px_checkpoint DROP FOREIGN KEY px_checkpoint_ibfk_2;
ALTER TABLE px_checkpoint DROP FOREIGN KEY px_checkpoint_ibfk_3;

ALTER TABLE px_group_security DROP FOREIGN KEY px_group_security_ibfk_1;
ALTER TABLE px_group_security DROP FOREIGN KEY px_group_security_ibfk_2;

ALTER TABLE px_group_security_detail DROP FOREIGN KEY px_group_security_detail_ibfk_1;
ALTER TABLE px_group_security_detail DROP FOREIGN KEY px_group_security_detail_ibfk_2;
ALTER TABLE px_group_security_detail DROP FOREIGN KEY px_group_security_detail_ibfk_3;
ALTER TABLE px_group_security_detail DROP FOREIGN KEY px_group_security_detail_ibfk_4;

ALTER TABLE px_issue DROP FOREIGN KEY px_issue_ibfk_1;

ALTER TABLE px_issue_assign DROP FOREIGN KEY px_issue_assign_ibfk_1;
ALTER TABLE px_issue_assign DROP FOREIGN KEY px_issue_assign_ibfk_2;
ALTER TABLE px_issue_assign DROP FOREIGN KEY px_issue_assign_ibfk_3;
ALTER TABLE px_issue_assign DROP FOREIGN KEY px_issue_assign_ibfk_4;

ALTER TABLE px_issue_report_attachment DROP FOREIGN KEY px_issue_report_attachment_ibfk_1;

ALTER TABLE px_patrol DROP FOREIGN KEY px_patrol_ibfk_1;
ALTER TABLE px_patrol DROP FOREIGN KEY px_patrol_ibfk_2;
ALTER TABLE px_patrol DROP FOREIGN KEY px_patrol_ibfk_3;
ALTER TABLE px_patrol DROP FOREIGN KEY px_patrol_ibfk_4;

ALTER TABLE px_patrol_checkin DROP FOREIGN KEY px_patrol_checkin_ibfk_1;
ALTER TABLE px_patrol_checkin DROP FOREIGN KEY px_patrol_checkin_ibfk_2;
ALTER TABLE px_patrol_checkin DROP FOREIGN KEY px_patrol_checkin_ibfk_3;

ALTER TABLE px_patrol_pattern DROP FOREIGN KEY px_patrol_pattern_ibfk_1;
ALTER TABLE px_patrol_pattern DROP FOREIGN KEY px_patrol_pattern_ibfk_2;
ALTER TABLE px_patrol_pattern DROP FOREIGN KEY px_patrol_pattern_ibfk_3;
ALTER TABLE px_patrol_pattern DROP FOREIGN KEY px_patrol_pattern_ibfk_4;

ALTER TABLE px_patrol_report DROP FOREIGN KEY px_patrol_report_ibfk_1;
ALTER TABLE px_patrol_report DROP FOREIGN KEY px_patrol_report_ibfk_2;

ALTER TABLE px_patrol_report_attachment DROP FOREIGN KEY px_patrol_report_attachment_ibfk_1;

ALTER TABLE px_patrol_report_checkpoint DROP FOREIGN KEY px_patrol_report_checkpoint_ibfk_1;
ALTER TABLE px_patrol_report_checkpoint DROP FOREIGN KEY px_patrol_report_checkpoint_ibfk_2;

ALTER TABLE px_pattern DROP FOREIGN KEY px_pattern_ibfk_1;
ALTER TABLE px_pattern DROP FOREIGN KEY px_pattern_ibfk_2;
ALTER TABLE px_pattern DROP FOREIGN KEY px_pattern_ibfk_3;

ALTER TABLE px_shift DROP FOREIGN KEY px_shift_ibfk_1;
ALTER TABLE px_shift DROP FOREIGN KEY px_shift_ibfk_2;


