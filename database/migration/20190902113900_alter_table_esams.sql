ALTER TABLE `px_assignment` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_assignment_attachment` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_assignment_detail` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_assignment_people` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_assignment_people_checkin` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_assignment_report` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_assignment_report_attachment` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_checklist` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_checkpoint` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_group_security` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_group_security_detail` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_patrol` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_patrol_checkin` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_patrol_patern` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_patrol_report` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_patrol_report_attachment` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_patrol_report_checkpoint` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_patrol_user` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_pattern` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_shift` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;

RENAME TABLE `px_patrol_patern` TO `px_patrol_pattern`;

/*baru*/
ALTER TABLE `px_building` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_building_detail` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;