CREATE TABLE `px_asesmen_2015_history` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`asesmen_2015_id` Int( 11 ) NOT NULL DEFAULT 0,
	`user_id` Int( 11 ) NOT NULL,
	`user_status` Varchar( 255 ) NOT NULL DEFAULT 'Assessor',
	`action` Varchar( 255 ) NOT NULL DEFAULT 'Submit',
	`date_created` DateTime NOT NULL,
	`status` Int( 1 ) NOT NULL DEFAULT 1,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 0;