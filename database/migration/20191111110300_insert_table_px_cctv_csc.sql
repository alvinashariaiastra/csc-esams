ALTER TABLE `px_cctv_csc` CHANGE `checkpoint_id` `checkpoint_id` INT(11) NULL;
ALTER TABLE `px_cctv_csc` ADD `ip4_address` VARCHAR(255) NULL DEFAULT NULL AFTER `checkpoint_id`;
ALTER TABLE `px_cctv_csc` ADD `port` VARCHAR(255) NULL DEFAULT NULL AFTER `checkpoint_id`;
ALTER TABLE `px_cctv_csc` ADD `status` VARCHAR(255) NULL DEFAULT NULL AFTER `checkpoint_id`;
ALTER TABLE `px_cctv_csc` ADD `notes` TEXT NULL DEFAULT NULL AFTER `location`;
TRUNCATE TABLE `px_cctv_csc`;
INSERT INTO `px_cctv_csc` 
	(`channel`, `status`, `ip4_address`, `port`, `location`, `created_by`, `created_date`, `delete_flag`) 
VALUES 
	('1', 'Active', '192.168.2.176', '8000', 'JEMBATAN GD. B DAN C', '18', '2019-10-25 10:00:00', '0'), 
	('2', 'Active', '192.168.2.213', '8000', 'DEPOT', '18', '2019-10-25 10:00:00', '0'), 
	('3', 'Active', '192.168.2.214', '8000', 'LOBBY LIFT LT. 1A', '18', '2019-10-25 10:00:00', '0'), 
	('4', 'Active', '192.168.2.215', '8000', 'PINTU SELATAN GD A', '18', '2019-10-25 10:00:00', '0'),
	('5', 'Active', '192.168.2.216', '8000', 'PINTU TEMBUS LT. 5A', '18', '2019-10-25 10:00:00', '0'),
	('6', 'Active', '192.168.2.217', '8000', 'DEPAN LIFT LT 7A', '18', '2019-10-25 10:00:00', '0'),
	('7', 'Active', '192.168.2.218', '8000', 'STRONG ROOM LT 1 A', '18', '2019-10-25 10:00:00', '0'),
	('8', 'Active', '192.168.2.219', '8000', 'UPS 1', '18', '2019-10-25 10:00:00', '0'),
	('9', 'Active', '192.168.2.220', '8000', 'ENGINERING', '18', '2019-10-25 10:00:00', '0'),
	('10', 'Active', '192.168.2.221', '8000', 'RECEPTIONIST LOBBY A', '18', '2019-10-25 10:00:00', '0'),
	('11', 'Active', '192.168.2.222', '8000', 'LOBBY LIFT LT.8 A', '18', '2019-10-25 10:00:00', '0'),
	('12', 'Active', '192.168.2.223', '8000', 'PINTU BARAT GD A', '18', '2019-10-25 10:00:00', '0'),
	('13', 'Active', '192.168.2.224', '8000', 'LOBBY LIFT LT 1 D', '18', '2019-10-25 10:00:00', '0'),
	('14', 'Active', '192.168.2.225', '8000', 'LOBBY LIFT LT. 6A', '18', '2019-10-25 10:00:00', '0'),
	('15', 'Active', '192.168.2.226', '8000', 'RECEPTIONIST LOBBY B', '18', '2019-10-25 10:00:00', '0'),
	('16', 'Active', '192.168.2.227', '8000', 'PINTU TIMUR LT. 1C', '18', '2019-10-25 10:00:00', '0');