ALTER TABLE `px_esams_log_wso2_token` ADD `response_wso2_request` TEXT NULL AFTER `expires_in`;
ALTER TABLE `px_esams_log_wso2_token` ADD `response_wso2_introspect` TEXT NULL AFTER `expires_in`;
ALTER TABLE `px_esams_log_wso2_token` ADD `email` varchar(255) NULL AFTER `user_id`;
ALTER TABLE `px_esams_log_wso2_token` MODIFY `user_id` int(11) NULL;