ALTER TABLE `px_issue` ADD `fact` TEXT NULL DEFAULT NULL AFTER `description`;
ALTER TABLE `px_issue` ADD `analysis` TEXT NULL DEFAULT NULL AFTER `description`;
ALTER TABLE `px_issue` ADD `summary` TEXT NULL DEFAULT NULL AFTER `description`;