INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('19', '5', '5', 'RUANG ESR', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('20', '5', '5', 'RUANG MEETING', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('21', '5', '5', 'RUANG FOTOCOPY', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('22', '5', '5', 'RUANG MAKAN', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('23', '5', '5', 'RUANG SECD', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('24', '5', '5', 'GUDANG GA', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('25', '5', '5', 'GUDANG SECD', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('26', '5', '5', 'GUDANG AIR', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('27', '5', '5', 'RUANG NURSERY', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('28', '5', '5', 'R. AHU ZONE A', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('29', '5', '5', 'R. AHU ZONE B', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('30', '5', '5', 'TOILET', '18', '2019-11-19 13:46:52', NULL, NULL, '0');

INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('31', '6', '14', 'Ruang Utama BOD/BOC', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('32', '6', '14', 'Ruang Sinergy', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('33', '6', '14', 'Ruang Makan', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('34', '6', '14', 'Ruang Dapur', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('35', '6', '14', 'LOBBY LIFT', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('36', '6', '14', 'R. AHU ZONE A', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('37', '6', '14', 'R. AHU ZONE B', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('38', '6', '14', 'TOILET', '18', '2019-11-19 13:46:52', NULL, NULL, '0');

INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('39', '1', '10', 'LOBBY A', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('40', '1', '10', 'TOILET', '18', '2019-11-19 13:46:52', NULL, NULL, '0');

/*20 november*/
INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('41', '4', '5', 'R.401', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('42', '4', '5', 'R.402', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('43', '4', '5', 'R.403', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('44', '4', '5', 'R.404', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('45', '4', '5', 'R.405', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('46', '4', '5', 'R.406', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('47', '4', '5', 'R.407', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('48', '4', '5', 'R.408', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('49', '4', '5', 'R.409', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('50', '4', '5', 'R.410', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('51', '4', '5', 'R.411', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('52', '4', '5', 'R.412', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('53', '4', '5', 'R.413', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('54', '4', '5', 'R.414', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('55', '4', '5', 'R.415', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('56', '4', '5', 'R.416', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('57', '4', '5', 'R.AHU ZONE A', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('58', '4', '5', 'R.AHU ZONE B', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('59', '4', '5', 'TOILET WANITA', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('60', '4', '5', 'TOILET PRIA', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('62', '3', '5', 'AUDITORIUM', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('63', '3', '5', 'R.301', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('64', '3', '5', 'R.302', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('65', '3', '5', 'R.303', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('66', '3', '5', 'R.304', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('67', '3', '5', 'R.305', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('68', '3', '5', 'R.306', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('69', '3', '5', 'R.307', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('70', '3', '5', 'R.308', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('71', '3', '5', 'R.309', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('72', '3', '5', 'R.310', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('73', '3', '5', 'R.311', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('74', '3', '5', 'R.312', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('75', '3', '5', 'R.313', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('76', '3', '5', 'R.314', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('77', '3', '5', 'R.315', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('78', '3', '5', 'R.316', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('79', '3', '5', 'R.AHU ZONE A', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('80', '3', '5', 'R.AHU ZONE B', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('81', '3', '5', 'TOILET WANITA', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('82', '3', '5', 'TOILET PRIA', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('83', '3', '5', 'HRD', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('84', '3', '5', 'GAD', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('85', '3', '5', 'AMDI', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('86', '3', '5', 'R.204', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('87', '3', '5', 'R.206', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('88', '3', '5', 'R.207', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('89', '3', '5', 'R.AHU ZONE A', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('90', '3', '5', 'R.AHU ZONE B', '18', '2019-11-20 11:46:52', NULL, NULL, '0'),
	('91', '3', '5', 'TOILET', '18', '2019-11-20 11:46:52', NULL, NULL, '0');


INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('92', '1', '3', 'R.AHU ZONE A', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('93', '1', '3', 'R.AHU ZONE B', '18', '2019-11-20 13:46:52', NULL, NULL, '0');

INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('94', '1', '7', 'STROONG ROOM', '18', '2019-11-20 13:46:52', NULL, NULL, '0');

INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('95', '1', '4', 'ARC', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('96', '1', '4', 'PUREL', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('97', '1', '4', 'STROONG ROOM', '18', '2019-11-20 13:46:52', NULL, NULL, '0');

INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('98', '1', '12', 'KLINIK', '18', '2019-11-20 13:46:52', NULL, NULL, '0');


/*Gedung B*/

INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('99', '9', '15', 'LOBBY', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('100', '9', '15', 'HSO', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('101', '9', '15', 'BANK PERMATA', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('102', '9', '15', 'PINTU DARURAT', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('103', '9', '15', 'TOILET', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('104', '10', '15', 'HSO', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('105', '11', '15', 'R.301', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('106', '11', '15', 'R.302', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('107', '11', '15', 'R.303', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('108', '11', '15', 'R.304', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('109', '11', '15', 'R.305', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('110', '11', '15', 'R.306', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('111', '11', '15', 'R.307', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('112', '11', '15', 'R.308', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('113', '11', '15', 'R.309', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('114', '11', '15', 'R.310', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('115', '11', '15', 'R.311', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('116', '11', '15', 'R.312', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('117', '11', '15', 'R.313', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('118', '11', '15', 'RUANG FOTO COPY', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('119', '11', '15', 'TOILET', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('120', '12', '15', 'R.401', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('121', '12', '15', 'R.402', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('122', '12', '15', 'R.403', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('123', '12', '15', 'R.404', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('124', '12', '15', 'R.405', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('125', '12', '15', 'R.406', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('126', '12', '15', 'R.407', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('127', '12', '15', 'TOILET', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('128', '13', '15', 'SEKRETARIAT POLMAN', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('129', '13', '15', 'TOILET', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('130', '13', '15', 'R.KARYAWAN', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('131', '14', '15', '601 Ruang Meeting 1', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('132', '14', '15', '602 Ruang Meeting 2', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('133', '14', '15', '604 Ruang File', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('134', '14', '15', 'R IT Suport', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('135', '14', '15', 'R.Bussines', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('136', '14', '15', 'R.Photo Copy', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('137', '14', '15', 'TOILET', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('138', '15', '15', 'R.KARYAWAN', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('139', '15', '15', '701 RUANG MEETING 1', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('140', '15', '15', '703 RUANG MEETING 2', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('141', '15', '15', 'NURSERY ROOM', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('142', '15', '15', 'TOILET', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('143', '16', '15', 'R. Karyawan', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('144', '16', '15', '801 Ruang Meeting 1', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('145', '16', '15', '802 Ruang Meeting 2', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('146', '16', '15', '803 Ruang File', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('147', '16', '15', '806 Ruang File', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('148', '16', '15', 'NURSERY ROOM', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('149', '16', '15', 'TOILET', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('150', '16', '15', 'TANGGA DARURAT', '18', '2019-11-20 13:46:52', NULL, NULL, '0');

INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('151', '17', '16', '101 Ruang Instruktur TPM', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('152', '17', '16', '103 Ruang Bengkel', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('153', '17', '16', 'R 104', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('154', '17', '16', '109 Kamar Alat', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('155', '17', '16', 'Ruang Sa Karu', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('156', '17', '16', 'Ruang Instruktur TO', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('157', '17', '16', 'Lab Pengecatan', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('158', '17', '16', 'Kamar Alat', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('159', '17', '16', 'Pintu Kaca GD C', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('160', '17', '16', 'Rolling Door I', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('161', '17', '16', 'Roling Door II', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('162', '17', '16', 'Tangga Darurat ZONE A', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('163', '17', '16', 'Tangga Darurat ZONE B', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('164', '17', '16', 'TOILET PRIA/WANITA', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('165', '18', '16', '201 Ruang Kelas ', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('166', '18', '16', '202 Lab UPT', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('167', '18', '16', '203 Ruang Kelas', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('168', '18', '16', '204 Ruang Kelas', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('169', '18', '16', '207 Ruang Teori Praktek TO', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('170', '18', '16', '208 Lab TMS ( Time Motion Study )', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('171', '18', '16', '209 Ruang Teori Praktek TO', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('172', '18', '16', '210 Ruang Training', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('173', '18', '16', '211 Ruang Kelas', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('174', '18', '16', '212 Ruang Kelas ', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('175', '18', '16', '213 Ruang Kelas ', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('176', '18', '16', 'MUSHOLLA', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('178', '18', '16', 'TOILET PRIA/WANITA', '18', '2019-11-20 13:46:52', NULL, NULL, '0');

INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('179', '19', '1', '301 Ruang Assesment CBT/CBA', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('180', '19', '1', '302 Ruang Instruktur P4 TK1', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('181', '19', '1', '303 Ruang Instruktur P4 TK2', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('182', '19', '1', '304 Ruang Instruktur P4 TK3', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('183', '19', '1', '305 Ruang Mesin', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('184', '19', '1', '306 Ruang Instruktur Produksi', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('185', '19', '1', '307  Ruang Maintenance, UPT B', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('186', '19', '1', '308 Locker Karyawan / Instruktur', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('187', '19', '1', '309 Gudang Training & Marketing', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('188', '19', '1', 'R 313', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('189', '19', '1', 'R 314', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('190', '19', '1', '315-316 Lab Pengukuran', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('191', '19', '1', '317 Lab Perlakuan Panas', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('192', '19', '1', '318 Ruang Tools', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('193', '19', '1', '319 Ruang Product', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('194', '19', '1', '320 Ruang Alat-alat', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('195', '19', '1', '321 Gudang Material', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('196', '19', '1', '322 Gudang Material', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('197', '19', '1', 'Pintu Tembus Parkir Lt IV', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('198', '20', '1', 'SPORT HALL', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('199', '20', '1', 'Ruang Musik Polman', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('200', '20', '1', 'Ruang Musik AI-HO', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('201', '20', '1', 'Toilet Pria / Wanita', '18', '2019-11-20 13:46:52', NULL, NULL, '0');

/*gedung D*/
INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('202', '22', '9', 'R. GENSET', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('203', '22', '9', 'Musholla', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('204', '22', '9', 'Lab ESR', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('205', '22', '9', 'Parkir Motor', '18', '2019-11-20 13:46:52', NULL, NULL, '0');

INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('206', '22', '13', 'Ruang Locker', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('207', '22', '13', 'TOILET', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('208', '23', '13', 'Parkir Mobil VIP', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('209', '23', '13', 'Ruang Pool', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('210', '23', '13', 'Ruang Locker', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('211', '23', '13', 'TOILET', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('212', '24', '13', 'Parkir Mobil', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('213', '24', '13', 'Ruang Locker', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('214', '24', '13', 'TOILET', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('215', '25', '13', 'Parkir Mobil', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('216', '25', '13', 'Mobil Titipan', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('217', '25', '13', 'Ruang Locker', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('218', '25', '13', 'TOILET', '18', '2019-11-20 13:46:52', NULL, NULL, '0');

INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('219', '26', '8', 'Parkir Mobil', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('220', '26', '8', 'Mobil Titipan', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('221', '26', '8', 'Ruang Locker', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('222', '26', '8', 'UPS', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('223', '26', '8', 'TOILET', '18', '2019-11-20 13:46:52', NULL, NULL, '0');

/*INSERT CHECKPOINT*/
INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('224', '27', '8', 'Parkir Mobil POOL', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('225', '27', '8', 'Parkir Mobil', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('226', '27', '8', 'Ruang Locker', '18', '2019-11-20 13:46:52', NULL, NULL, '0'),
	('227', '27', '8', 'TOILET ', '18', '2019-11-20 13:46:52', NULL, NULL, '0');

/*insert checkpoint*/
INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('228', '28', '2', 'OUTDOR', '18', '2019-11-20 13:46:52', NULL, NULL, '0');


