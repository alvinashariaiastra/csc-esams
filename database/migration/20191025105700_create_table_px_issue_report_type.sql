CREATE TABLE `px_issue_report_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `delete_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `px_issue_report_type` ADD PRIMARY KEY (`id`);
ALTER TABLE `px_issue_report_type` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

INSERT INTO `px_issue_report_type` 
	(`id`, `name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	(NULL, 'Patroli', '18', '2019-10-25 10:00:00', NULL, NULL, '0');

ALTER TABLE `px_issue` ADD `issue_report_type_id` int(11) NULL DEFAULT NULL AFTER `id`;
ALTER TABLE `px_issue` ADD INDEX `issue_report_type_id` (`issue_report_type_id`);