CREATE TABLE `px_checklist_detail` (
  `id` int(11) NOT NULL,
  `checklist_id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `delete_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `px_checklist_detail` ADD PRIMARY KEY (`id`);
ALTER TABLE `px_checklist_detail` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
