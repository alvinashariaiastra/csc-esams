TRUNCATE TABLE `px_checkpoint`;

INSERT INTO `px_checkpoint` 
	(`id`, `building_detail_id`, `cctv_csc_id`, `checkpoint_name`, `created_by`, `created_date`, `modified_by`, `modified_date`, `delete_flag`) 
VALUES 
	('1', '8', '11', 'LOBBY LIFT', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('2', '8', '11', 'R.AHU ZONE A', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('3', '8', '11', 'TANGGA DARURAT LT.9 ZONE A', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('4', '8', '11', 'FITNESS', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('5', '8', '11', 'WILLIAM SURJAYA', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('6', '8', '11', 'EKSEKUTIF LOUNGE', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('7', '8', '11', 'R. AHU ZONE B', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('8', '8', '11', 'TOILET', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('9', '8', '11', 'TANGGA DARURAT LT.9 ZONE B', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('10', '7', '6', 'LOBBY LIFT', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('11', '7', '6', 'GUDANG BP VAM', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('12', '7', '6', 'IT UTAMA', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('13', '7', '6', 'RUNGGU SERVER', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('14', '7', '6', 'SERVER', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('15', '7', '6', 'GUDANG AGOS', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('16', '7', '6', 'R. AHU ZONE A', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('17', '7', '6', 'R. AHU ZONE B', '18', '2019-11-19 13:46:52', NULL, NULL, '0'),
	('18', '7', '6', 'TOILET', '18', '2019-11-19 13:46:52', NULL, NULL, '0');


