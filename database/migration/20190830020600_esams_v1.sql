--
-- Database: `csc`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_assignment`
--

CREATE TABLE `px_assignment` (
  `id` int(11) NOT NULL,
  `assignment_title` varchar(255) NOT NULL,
  `assignment_type` enum('single','series') NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_assignment_attachment`
--

CREATE TABLE `px_assignment_attachment` (
  `id` int(11) NOT NULL,
  `assignment_detail_id` int(11) NOT NULL,
  `image_name` varchar(500) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_assignment_detail`
--

CREATE TABLE `px_assignment_detail` (
  `id` int(11) NOT NULL,
  `task_title` varchar(255) DEFAULT NULL,
  `assignment_id` int(11) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `description` text NOT NULL,
  `location` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_assignment_people`
--

CREATE TABLE `px_assignment_people` (
  `id` int(11) NOT NULL,
  `assignment_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `group_security_id` int(11) DEFAULT NULL,
  `status` enum('done','active','failed') NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_assignment_people_checkin`
--

CREATE TABLE `px_assignment_people_checkin` (
  `id` int(11) NOT NULL,
  `assignment_detail_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `location` text NOT NULL,
  `image_name` varchar(500) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_assignment_report`
--

CREATE TABLE `px_assignment_report` (
  `id` int(11) NOT NULL,
  `assignment_detail_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `report_text` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_assignment_report_attachment`
--

CREATE TABLE `px_assignment_report_attachment` (
  `id` int(11) NOT NULL,
  `assignment_report_id` int(11) NOT NULL,
  `image_name` varchar(500) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_building`
--

CREATE TABLE `px_building` (
  `id` int(11) NOT NULL,
  `instalasi_id` int(11) NOT NULL,
  `building_name` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_building_detail`
--

CREATE TABLE `px_building_detail` (
  `id` int(11) NOT NULL,
  `building_id` int(11) NOT NULL,
  `floor` varchar(50) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_checklist`
--

CREATE TABLE `px_checklist` (
  `id` int(11) NOT NULL,
  `checklist_name` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_checkpoint`
--

CREATE TABLE `px_checkpoint` (
  `id` int(11) NOT NULL,
  `building_detail_id` int(11) NOT NULL,
  `checkpoint_name` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_group_security`
--

CREATE TABLE `px_group_security` (
  `id` int(11) NOT NULL,
  `groupsecurity_name` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  `modified_date` datetime NOT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_group_security_detail`
--

CREATE TABLE `px_group_security_detail` (
  `id` int(11) NOT NULL,
  `group_security_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_issue`
--

CREATE TABLE `px_issue` (
  `id` int(11) NOT NULL,
  `building_id` int(11) NOT NULL,
  `floor` varchar(255) DEFAULT NULL,
  `area` text,
  `description` text NOT NULL,
  `action` enum('dispatch','escalate') NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_issue_assign`
--

CREATE TABLE `px_issue_assign` (
  `id` int(11) NOT NULL,
  `issue_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_issue_report_attachment`
--

CREATE TABLE `px_issue_report_attachment` (
  `id` int(11) NOT NULL,
  `issue_id` int(11) NOT NULL,
  `image_name` varchar(500) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_patrol`
--

CREATE TABLE `px_patrol` (
  `id` int(11) NOT NULL,
  `building_id` int(11) NOT NULL,
  `shift_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_patrol_checkin`
--

CREATE TABLE `px_patrol_checkin` (
  `id` int(11) NOT NULL,
  `patrol_id` int(11) NOT NULL,
  `image_name` varchar(500) NOT NULL,
  `checkin_type` enum('cctv','manual') NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_patrol_patern`
--

CREATE TABLE `px_patrol_patern` (
  `id` int(11) NOT NULL,
  `patrol_id` int(11) NOT NULL,
  `checkpoint_id` int(11) NOT NULL,
  `sequence` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_patrol_report`
--

CREATE TABLE `px_patrol_report` (
  `id` int(11) NOT NULL,
  `patrol_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `report_text` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_patrol_report_attachment`
--

CREATE TABLE `px_patrol_report_attachment` (
  `id` int(11) NOT NULL,
  `patrol_report_id` int(11) NOT NULL,
  `image_name` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_patrol_report_checkpoint`
--

CREATE TABLE `px_patrol_report_checkpoint` (
  `id` int(11) NOT NULL,
  `patrol_report_id` int(11) NOT NULL,
  `checkpoint_id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_patrol_user`
--

CREATE TABLE `px_patrol_user` (
  `id` int(11) NOT NULL,
  `patrol_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` enum('done','active','failed') NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_pattern`
--

CREATE TABLE `px_pattern` (
  `id` int(11) NOT NULL,
  `pattern_name` varchar(255) NOT NULL,
  `checkpoint_id` int(11) NOT NULL,
  `sequence` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `px_shift`
--

CREATE TABLE `px_shift` (
  `id` int(11) NOT NULL,
  `shift_name` varchar(255) NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `deleted_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `px_assignment`
--
ALTER TABLE `px_assignment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `px_assignment_ibfk_1` (`created_by`),
  ADD KEY `px_assignment_ibfk_2` (`modified_by`);

--
-- Indexes for table `px_assignment_attachment`
--
ALTER TABLE `px_assignment_attachment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assignment_detail_id` (`assignment_detail_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_assignment_detail`
--
ALTER TABLE `px_assignment_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assignment_id` (`assignment_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_assignment_people`
--
ALTER TABLE `px_assignment_people`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assignment_id` (`assignment_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `group_security_id` (`group_security_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_assignment_people_checkin`
--
ALTER TABLE `px_assignment_people_checkin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assignment_detail_id` (`assignment_detail_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_assignment_report`
--
ALTER TABLE `px_assignment_report`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assignment_detail_id` (`assignment_detail_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_assignment_report_attachment`
--
ALTER TABLE `px_assignment_report_attachment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `assignment_report_id` (`assignment_report_id`);

--
-- Indexes for table `px_building`
--
ALTER TABLE `px_building`
  ADD PRIMARY KEY (`id`),
  ADD KEY `instalasi_id` (`instalasi_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_building_detail`
--
ALTER TABLE `px_building_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `building_id` (`building_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_checklist`
--
ALTER TABLE `px_checklist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_checkpoint`
--
ALTER TABLE `px_checkpoint`
  ADD PRIMARY KEY (`id`),
  ADD KEY `building_detail_id` (`building_detail_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_group_security`
--
ALTER TABLE `px_group_security`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_group_security_detail`
--
ALTER TABLE `px_group_security_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_security_id` (`group_security_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_issue`
--
ALTER TABLE `px_issue`
  ADD PRIMARY KEY (`id`),
  ADD KEY `building_id` (`building_id`);

--
-- Indexes for table `px_issue_assign`
--
ALTER TABLE `px_issue_assign`
  ADD PRIMARY KEY (`id`),
  ADD KEY `issue_id` (`issue_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_issue_report_attachment`
--
ALTER TABLE `px_issue_report_attachment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `issue_id` (`issue_id`);

--
-- Indexes for table `px_patrol`
--
ALTER TABLE `px_patrol`
  ADD PRIMARY KEY (`id`),
  ADD KEY `building_id` (`building_id`),
  ADD KEY `shift_id` (`shift_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_patrol_checkin`
--
ALTER TABLE `px_patrol_checkin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patrol_id` (`patrol_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_patrol_patern`
--
ALTER TABLE `px_patrol_patern`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patrol_id` (`patrol_id`),
  ADD KEY `location_id` (`checkpoint_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_patrol_report`
--
ALTER TABLE `px_patrol_report`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patrol_id` (`patrol_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `px_patrol_report_attachment`
--
ALTER TABLE `px_patrol_report_attachment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patrol_report_id` (`patrol_report_id`);

--
-- Indexes for table `px_patrol_report_checkpoint`
--
ALTER TABLE `px_patrol_report_checkpoint`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patrol_report_id` (`patrol_report_id`),
  ADD KEY `checkpoint_id` (`checkpoint_id`);

--
-- Indexes for table `px_patrol_user`
--
ALTER TABLE `px_patrol_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `px_pattern`
--
ALTER TABLE `px_pattern`
  ADD PRIMARY KEY (`id`),
  ADD KEY `checkpoint_id` (`checkpoint_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `px_shift`
--
ALTER TABLE `px_shift`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `px_assignment`
--
ALTER TABLE `px_assignment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_assignment_attachment`
--
ALTER TABLE `px_assignment_attachment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_assignment_detail`
--
ALTER TABLE `px_assignment_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_assignment_people`
--
ALTER TABLE `px_assignment_people`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_assignment_people_checkin`
--
ALTER TABLE `px_assignment_people_checkin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_assignment_report`
--
ALTER TABLE `px_assignment_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_assignment_report_attachment`
--
ALTER TABLE `px_assignment_report_attachment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_building`
--
ALTER TABLE `px_building`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_building_detail`
--
ALTER TABLE `px_building_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_checklist`
--
ALTER TABLE `px_checklist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_checkpoint`
--
ALTER TABLE `px_checkpoint`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_group_security`
--
ALTER TABLE `px_group_security`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_group_security_detail`
--
ALTER TABLE `px_group_security_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_issue`
--
ALTER TABLE `px_issue`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_issue_assign`
--
ALTER TABLE `px_issue_assign`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_issue_report_attachment`
--
ALTER TABLE `px_issue_report_attachment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_patrol`
--
ALTER TABLE `px_patrol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_patrol_checkin`
--
ALTER TABLE `px_patrol_checkin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_patrol_patern`
--
ALTER TABLE `px_patrol_patern`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_patrol_report`
--
ALTER TABLE `px_patrol_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_patrol_report_attachment`
--
ALTER TABLE `px_patrol_report_attachment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_patrol_report_checkpoint`
--
ALTER TABLE `px_patrol_report_checkpoint`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_patrol_user`
--
ALTER TABLE `px_patrol_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_pattern`
--
ALTER TABLE `px_pattern`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `px_shift`
--
ALTER TABLE `px_shift`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `px_assignment`
--
ALTER TABLE `px_assignment`
  ADD CONSTRAINT `px_assignment_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `px_admin` (`id`),
  ADD CONSTRAINT `px_assignment_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `px_admin` (`id`);

--
-- Ketidakleluasaan untuk tabel `px_assignment_attachment`
--
ALTER TABLE `px_assignment_attachment`
  ADD CONSTRAINT `px_assignment_attachment_ibfk_1` FOREIGN KEY (`assignment_detail_id`) REFERENCES `px_assignment_detail` (`id`),
  ADD CONSTRAINT `px_assignment_attachment_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `px_admin` (`id`),
  ADD CONSTRAINT `px_assignment_attachment_ibfk_3` FOREIGN KEY (`modified_by`) REFERENCES `px_admin` (`id`);

--
-- Ketidakleluasaan untuk tabel `px_assignment_detail`
--
ALTER TABLE `px_assignment_detail`
  ADD CONSTRAINT `px_assignment_detail_ibfk_1` FOREIGN KEY (`assignment_id`) REFERENCES `px_assignment` (`id`),
  ADD CONSTRAINT `px_assignment_detail_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `px_user` (`id`),
  ADD CONSTRAINT `px_assignment_detail_ibfk_3` FOREIGN KEY (`modified_by`) REFERENCES `px_user` (`id`);

--
-- Ketidakleluasaan untuk tabel `px_assignment_people`
--
ALTER TABLE `px_assignment_people`
  ADD CONSTRAINT `px_assignment_people_ibfk_1` FOREIGN KEY (`assignment_id`) REFERENCES `px_assignment` (`id`),
  ADD CONSTRAINT `px_assignment_people_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `px_user` (`id`),
  ADD CONSTRAINT `px_assignment_people_ibfk_3` FOREIGN KEY (`group_security_id`) REFERENCES `px_group_security` (`id`),
  ADD CONSTRAINT `px_assignment_people_ibfk_4` FOREIGN KEY (`created_by`) REFERENCES `px_admin` (`id`),
  ADD CONSTRAINT `px_assignment_people_ibfk_5` FOREIGN KEY (`modified_by`) REFERENCES `px_admin` (`id`);

--
-- Ketidakleluasaan untuk tabel `px_assignment_people_checkin`
--
ALTER TABLE `px_assignment_people_checkin`
  ADD CONSTRAINT `px_assignment_people_checkin_ibfk_1` FOREIGN KEY (`assignment_detail_id`) REFERENCES `px_assignment_detail` (`id`),
  ADD CONSTRAINT `px_assignment_people_checkin_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `px_user` (`id`),
  ADD CONSTRAINT `px_assignment_people_checkin_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `px_admin` (`id`),
  ADD CONSTRAINT `px_assignment_people_checkin_ibfk_4` FOREIGN KEY (`modified_by`) REFERENCES `px_admin` (`id`);

--
-- Ketidakleluasaan untuk tabel `px_assignment_report`
--
ALTER TABLE `px_assignment_report`
  ADD CONSTRAINT `px_assignment_report_ibfk_1` FOREIGN KEY (`assignment_detail_id`) REFERENCES `px_assignment_detail` (`id`),
  ADD CONSTRAINT `px_assignment_report_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `px_user` (`id`),
  ADD CONSTRAINT `px_assignment_report_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `px_admin` (`id`),
  ADD CONSTRAINT `px_assignment_report_ibfk_4` FOREIGN KEY (`modified_by`) REFERENCES `px_admin` (`id`);

--
-- Ketidakleluasaan untuk tabel `px_assignment_report_attachment`
--
ALTER TABLE `px_assignment_report_attachment`
  ADD CONSTRAINT `px_assignment_report_attachment_ibfk_1` FOREIGN KEY (`assignment_report_id`) REFERENCES `px_assignment_report` (`id`);

--
-- Ketidakleluasaan untuk tabel `px_building`
--
ALTER TABLE `px_building`
  ADD CONSTRAINT `px_building_ibfk_1` FOREIGN KEY (`instalasi_id`) REFERENCES `px_asesmen_instalasi` (`id`),
  ADD CONSTRAINT `px_building_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `px_admin` (`id`),
  ADD CONSTRAINT `px_building_ibfk_3` FOREIGN KEY (`modified_by`) REFERENCES `px_admin` (`id`);

--
-- Ketidakleluasaan untuk tabel `px_building_detail`
--
ALTER TABLE `px_building_detail`
  ADD CONSTRAINT `px_building_detail_ibfk_1` FOREIGN KEY (`building_id`) REFERENCES `px_building` (`id`),
  ADD CONSTRAINT `px_building_detail_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `px_admin` (`id`),
  ADD CONSTRAINT `px_building_detail_ibfk_3` FOREIGN KEY (`modified_by`) REFERENCES `px_admin` (`id`);

--
-- Ketidakleluasaan untuk tabel `px_checklist`
--
ALTER TABLE `px_checklist`
  ADD CONSTRAINT `px_checklist_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `px_admin` (`id`),
  ADD CONSTRAINT `px_checklist_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `px_admin` (`id`);

--
-- Ketidakleluasaan untuk tabel `px_checkpoint`
--
ALTER TABLE `px_checkpoint`
  ADD CONSTRAINT `px_checkpoint_ibfk_1` FOREIGN KEY (`building_detail_id`) REFERENCES `px_building_detail` (`id`),
  ADD CONSTRAINT `px_checkpoint_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `px_admin` (`id`),
  ADD CONSTRAINT `px_checkpoint_ibfk_3` FOREIGN KEY (`modified_by`) REFERENCES `px_admin` (`id`);

--
-- Ketidakleluasaan untuk tabel `px_group_security`
--
ALTER TABLE `px_group_security`
  ADD CONSTRAINT `px_group_security_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `px_admin` (`id`),
  ADD CONSTRAINT `px_group_security_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `px_admin` (`id`);

--
-- Ketidakleluasaan untuk tabel `px_group_security_detail`
--
ALTER TABLE `px_group_security_detail`
  ADD CONSTRAINT `px_group_security_detail_ibfk_1` FOREIGN KEY (`group_security_id`) REFERENCES `px_group_security` (`id`),
  ADD CONSTRAINT `px_group_security_detail_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `px_user` (`id`),
  ADD CONSTRAINT `px_group_security_detail_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `px_admin` (`id`),
  ADD CONSTRAINT `px_group_security_detail_ibfk_4` FOREIGN KEY (`modified_by`) REFERENCES `px_admin` (`id`);

--
-- Ketidakleluasaan untuk tabel `px_issue`
--
ALTER TABLE `px_issue`
  ADD CONSTRAINT `px_issue_ibfk_1` FOREIGN KEY (`building_id`) REFERENCES `px_building` (`id`);

--
-- Ketidakleluasaan untuk tabel `px_issue_assign`
--
ALTER TABLE `px_issue_assign`
  ADD CONSTRAINT `px_issue_assign_ibfk_1` FOREIGN KEY (`issue_id`) REFERENCES `px_issue` (`id`),
  ADD CONSTRAINT `px_issue_assign_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `px_user` (`id`),
  ADD CONSTRAINT `px_issue_assign_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `px_admin` (`id`),
  ADD CONSTRAINT `px_issue_assign_ibfk_4` FOREIGN KEY (`modified_by`) REFERENCES `px_admin` (`id`);

--
-- Ketidakleluasaan untuk tabel `px_issue_report_attachment`
--
ALTER TABLE `px_issue_report_attachment`
  ADD CONSTRAINT `px_issue_report_attachment_ibfk_1` FOREIGN KEY (`issue_id`) REFERENCES `px_issue` (`id`);

--
-- Ketidakleluasaan untuk tabel `px_patrol`
--
ALTER TABLE `px_patrol`
  ADD CONSTRAINT `px_patrol_ibfk_1` FOREIGN KEY (`building_id`) REFERENCES `px_building` (`id`),
  ADD CONSTRAINT `px_patrol_ibfk_2` FOREIGN KEY (`shift_id`) REFERENCES `px_shift` (`id`),
  ADD CONSTRAINT `px_patrol_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `px_admin` (`id`),
  ADD CONSTRAINT `px_patrol_ibfk_4` FOREIGN KEY (`modified_by`) REFERENCES `px_admin` (`id`);

--
-- Ketidakleluasaan untuk tabel `px_patrol_checkin`
--
ALTER TABLE `px_patrol_checkin`
  ADD CONSTRAINT `px_patrol_checkin_ibfk_1` FOREIGN KEY (`patrol_id`) REFERENCES `px_patrol` (`id`),
  ADD CONSTRAINT `px_patrol_checkin_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `px_admin` (`id`),
  ADD CONSTRAINT `px_patrol_checkin_ibfk_3` FOREIGN KEY (`modified_by`) REFERENCES `px_admin` (`id`);

--
-- Ketidakleluasaan untuk tabel `px_patrol_patern`
--
ALTER TABLE `px_patrol_patern`
  ADD CONSTRAINT `px_patrol_patern_ibfk_1` FOREIGN KEY (`patrol_id`) REFERENCES `px_patrol` (`id`),
  ADD CONSTRAINT `px_patrol_patern_ibfk_2` FOREIGN KEY (`checkpoint_id`) REFERENCES `px_checkpoint` (`id`),
  ADD CONSTRAINT `px_patrol_patern_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `px_admin` (`id`),
  ADD CONSTRAINT `px_patrol_patern_ibfk_4` FOREIGN KEY (`modified_by`) REFERENCES `px_admin` (`id`);

--
-- Ketidakleluasaan untuk tabel `px_patrol_report`
--
ALTER TABLE `px_patrol_report`
  ADD CONSTRAINT `px_patrol_report_ibfk_1` FOREIGN KEY (`patrol_id`) REFERENCES `px_patrol` (`id`),
  ADD CONSTRAINT `px_patrol_report_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `px_user` (`id`);

--
-- Ketidakleluasaan untuk tabel `px_patrol_report_attachment`
--
ALTER TABLE `px_patrol_report_attachment`
  ADD CONSTRAINT `px_patrol_report_attachment_ibfk_1` FOREIGN KEY (`patrol_report_id`) REFERENCES `px_patrol_report` (`id`);

--
-- Ketidakleluasaan untuk tabel `px_patrol_report_checkpoint`
--
ALTER TABLE `px_patrol_report_checkpoint`
  ADD CONSTRAINT `px_patrol_report_checkpoint_ibfk_1` FOREIGN KEY (`patrol_report_id`) REFERENCES `px_patrol_report` (`id`),
  ADD CONSTRAINT `px_patrol_report_checkpoint_ibfk_2` FOREIGN KEY (`checkpoint_id`) REFERENCES `px_checkpoint` (`id`);

--
-- Ketidakleluasaan untuk tabel `px_pattern`
--
ALTER TABLE `px_pattern`
  ADD CONSTRAINT `px_pattern_ibfk_1` FOREIGN KEY (`checkpoint_id`) REFERENCES `px_checkpoint` (`id`),
  ADD CONSTRAINT `px_pattern_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `px_admin` (`id`),
  ADD CONSTRAINT `px_pattern_ibfk_3` FOREIGN KEY (`modified_by`) REFERENCES `px_admin` (`id`);

--
-- Ketidakleluasaan untuk tabel `px_shift`
--
ALTER TABLE `px_shift`
  ADD CONSTRAINT `px_shift_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `px_admin` (`id`),
  ADD CONSTRAINT `px_shift_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `px_admin` (`id`);
