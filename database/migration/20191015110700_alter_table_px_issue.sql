ALTER TABLE `px_issue` MODIFY COLUMN `action` enum('dispatch','escalate') NULL DEFAULT NULL;
ALTER TABLE `px_issue` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_issue_assign` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;
ALTER TABLE `px_issue_report_attachment` CHANGE `deleted_flag` `delete_flag` INT(11) NOT NULL;