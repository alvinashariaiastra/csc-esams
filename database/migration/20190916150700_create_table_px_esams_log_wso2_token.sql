CREATE TABLE `px_esams_log_wso2_token` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`user_id` Int( 11 ) NOT NULL,
	`access_token` Text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
	`delete_flag` Int( 1 ) NOT NULL DEFAULT 0,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;

CREATE INDEX `index_user_id` USING BTREE ON `px_esams_log_wso2_token`( `user_id` );