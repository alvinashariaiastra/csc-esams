CREATE TABLE `px_esams_log_api` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`method` Varchar( 225 ) NULL DEFAULT NULL,
	`uri` Varchar( 225 ) NULL DEFAULT NULL,
	`token_key` Varchar( 225 ) NULL DEFAULT NULL,
	`http_code` Varchar( 225 ) NULL DEFAULT NULL,
	`request_post` TEXT NULL DEFAULT NULL,
	`request_get` TEXT NULL DEFAULT NULL,
	`message` TEXT NULL DEFAULT NULL,
	`response` TEXT NULL DEFAULT NULL,
	`date_created` DateTime NOT NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB
AUTO_INCREMENT = 0;