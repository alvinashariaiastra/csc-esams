RENAME TABLE px_pattern TO px_pattern_detail;
ALTER TABLE `px_pattern_detail` CHANGE `pattern_name` `pattern_id` INT(11) NOT NULL;
ALTER TABLE `px_pattern_detail` ADD INDEX `pattern_id` (`pattern_id`);