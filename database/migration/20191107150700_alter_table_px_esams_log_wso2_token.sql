ALTER TABLE `px_esams_log_wso2_token` CHANGE `expires_in` `expires_in` INT(11) NULL;
ALTER TABLE `px_esams_log_wso2_token` ADD `imei` VARCHAR(255) NULL DEFAULT NULL AFTER `access_token`;