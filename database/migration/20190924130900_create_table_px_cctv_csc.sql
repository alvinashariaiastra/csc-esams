CREATE TABLE `px_cctv_csc` (
  `id` int(11) NOT NULL,
  `checkpoint_id` int(11) NOT NULL,
  `channel` varchar(255) NOT NULL,
  `location` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `delete_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `px_cctv_csc` ADD PRIMARY KEY (`id`);
ALTER TABLE `px_cctv_csc` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `px_cctv_csc` ADD INDEX `checkpoint_id` (`checkpoint_id`);