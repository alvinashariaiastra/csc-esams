CREATE TABLE `px_issue_report` (
  `id` int(11) NOT NULL,
  `issue_assign_id` int(11) NOT NULL,
  `report_text` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `delete_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `px_issue_report` ADD PRIMARY KEY (`id`);
ALTER TABLE `px_issue_report` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `px_issue_report` ADD INDEX `issue_assign_id` (`issue_assign_id`);